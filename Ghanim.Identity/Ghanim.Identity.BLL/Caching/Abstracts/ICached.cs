﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace Ghanim.BLL.Caching.Abstracts
{

    public interface ICached<T> where T : IRepoistryBaseEntity //: IClone<T>
    {
        T CachedGetByID(int nId);
        List<T> CachedGetAll();
        List<T> CachedGetAllWithBlock();
        List<T> CachedGetByIdsWithBlocked(List<int> ids);
        List<T> CachedGetByIds(List<int> ids);


        
        void ClearCashed();
        
    }

    public interface ICloneable //where T : class
    {
        T Clone<T>();
    }
}
