﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.BLL.Services.FlixyApi
{
    public class OrderStatusChangedFlixyDto
    {
        public OrderStatusChangedFlixyDto()
        {
            TechnicianOrder= new OrderStatusChangedFlixyDto_TechnicianOrder();
        }
        public string ProfileTechnicianNameEnglish { get; set; }
        public string ProfileTechnicianNameArabic { get; set; }
        public string ProfilePicture { get; set; }
        public string TechnicianEmployeeID { get; set; }
        public string Cancellationreason { get; set; }
        public OrderStatusChangedFlixyDto_TechnicianOrder TechnicianOrder { get; set; }
    }

    public class OrderStatusChangedFlixyDto_TechnicianOrder
    {
        public string OrderSAPId { get; set; }
        public string PlateNumber { get; set; }
        public string Remainingvalue { get; set; }
        public string OrderCoordinates { get; set; }
        public string SAPStatus { get; set; }
    }
}
