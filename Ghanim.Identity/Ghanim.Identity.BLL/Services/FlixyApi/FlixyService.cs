﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Ghanim.API.ServiceCommunications.FlixyApi;

using Ghanim.Models.Entities;
using Newtonsoft.Json;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.Services.FlixyApi
{
    public class FlixyService : IFlixyService
    {
        FlixyApiSettings _settings;

        public FlixyService(IOptions<FlixyApiSettings> obj)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<string>> OrderStatusChanged(OrderStatusChangedFlixyDto dataToSend)
        {

            if (!_settings.enabled)
                return new ProcessResultViewModel<string>("the service was disabled from Amanah");

            string baseUrl = _settings.changeStatusUrl;


            var json = JsonConvert.SerializeObject(dataToSend);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            string result = null;


            //await PostBasicAsync(dataToSend);


            //using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            using (var client = new HttpClient(new HttpClientHandler()))
            {
                //client.DefaultRequestHeaders.Add("Authorization", _settings.authKey);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(_settings.authKey);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //client.BaseAddress = new Uri(baseUrl);
                //HttpResponseMessage response = await client.GetAsync(url);
                ////response.EnsureSuccessStatusCode();
                //if (response.IsSuccessStatusCode)
                //{
                //    return ProcessResultViewModelHelper.Succedded<dynamic>(await response.Content.ReadAsAsync<dynamic>());
                //}
                //else
                //{
                //    return ProcessResultViewModelHelper.Failed<dynamic>(null, "error while trying to get location");
                //}
                var response = await client.PostAsync(_settings.changeStatusUrl, data);
                result =await response.Content.ReadAsStringAsync();
            }
            return new ProcessResultViewModel<string>(result);
        }

        //private async Task PostBasicAsync(OrderStatusChangedFlixyDto dataToSend)
        //{
        //    string baseUrl = _settings.changeStatusUrl;


        //    var json = JsonConvert.SerializeObject(dataToSend);
        //    var data = new StringContent(json, Encoding.UTF8, "application/json");


        //    using (var client = new HttpClient())
        //    using (
        //        var request = new HttpRequestMessage
        //        {
        //            RequestUri = new Uri("http://localhost:49846/api/Order/GetOrderActionTypes"),
        //            Method = HttpMethod.Get,
        //            Headers = {
        //                //{ "X-Version", "1" } // HERE IS HOW TO ADD HEADERS,
        //                { HttpRequestHeader.Authorization.ToString(), _settings.authKey },
        //                { HttpRequestHeader.ContentType.ToString(), "application/json" },//use this content type if you want to send more than one content type
        //            },
        //            Content = new MultipartContent { // Just example of request sending multipart request
        //                new ObjectContent<OrderStatusChangedFlixyDto>(
        //                    dataToSend,
        //                    new JsonMediaTypeFormatter(), "application/json"), // this will add 'Content-Type' header for the first part of request
        //            },
        //        }

        //        )
        //    {
        //        using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
        //        {
        //            request.Content = stringContent;

        //            using (var response = await client
        //                .SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
        //                .ConfigureAwait(false))
        //            {
        //                var tresp = response.EnsureSuccessStatusCode();
        //            }
        //        }
        //    }
        //}

    }
}
