﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ServiceCommunications.FlixyApi
{
    public class FlixyApiSettings
    {
        public bool enabled { get; set; }
        public string authKey { get; set; }
        public string changeStatusUrl { get; set; }
    }
}
