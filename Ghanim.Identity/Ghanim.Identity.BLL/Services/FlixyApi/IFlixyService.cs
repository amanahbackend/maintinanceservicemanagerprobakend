﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.ServiceCommunications.FlixyApi;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.Services.FlixyApi
{
    public interface IFlixyService
    {
        Task<ProcessResultViewModel<string>> OrderStatusChanged(OrderStatusChangedFlixyDto dataToSend);
    }
}
