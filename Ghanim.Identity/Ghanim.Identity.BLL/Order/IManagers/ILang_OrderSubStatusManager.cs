﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_OrderSubStatusManager : IRepository<Lang_OrderSubStatus>
    {
        ProcessResult<List<Lang_OrderSubStatus>> GetAllLanguagesByOrderSubStatusId(int OrderSubStatusId);
        ProcessResult<bool> UpdateByOrderSubStatus(List<Lang_OrderSubStatus> entities);
    }
}