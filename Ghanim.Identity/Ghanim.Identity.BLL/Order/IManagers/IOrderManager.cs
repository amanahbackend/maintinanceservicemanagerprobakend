﻿using CommonEnum;
using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing.Charts;
using Ghanim.BLL.Services.FlixyApi;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IOrderManager : IRepository<OrderObject>
    {
        ProcessResult<bool> SetAcceptence(int? orderId, bool acceptenceFlag, int? rejectionReasonId, string rejectionReason, int? initialStatusId, string initialStatusName, int? initialSubStatusId, string initialSubStatusName);
        ProcessResult<bool> AcceptOrderAndSetOnTravel(int? orderId, string statusName, int statusId, string subStatusName, int subStatusId);


        ProcessResult<bool> BulkUnAssign(List<int> orderIds, int statusId);
        OrderObject Assign(int orderId, int teamId);


        ProcessResult<bool> UnAssign(int orderId);
        //ProcessResult<bool> ChangeOrderRank(List<OrderObject> teamOrders, Dictionary<int, int> orders);
        ProcessResult<bool> ChangeOrderRank2(List<OrderObject> teamOrders, Dictionary<int, int> orders, int OnTravelNo, int NotOnTravelNo);

        ProcessResult<List<OrderObject>> Add(List<OrderObject> ordersLst);
        //Task<OrderObject> Update(OrderObject order);
        ProcessResult<List<OrderObject>> GetDispatcherOrders(int SupervisorId, int state);
        ProcessResult<OrderObject> AssignDispatcher(int orderId, int dispatcherId);
        ProcessResult<bool> UnAssignDispatcher(int orderId);
        ProcessResult<bool> DeleteAll();
        string ChoosePin(int onHoldCount, string orderType, DateTime orderDate, int? problemId);
        string ChooseFolder(int onHoldCount, string problem);
        string ChooseFolder(int onHoldCount, int? problemId);
        void AddActionForDispatcherForBuilkAssign(OrderObject order, OrderActionType actionType);


        Dictionary<int?, string> GetDefaultTechniciansForOrders();
        Dictionary<int?, ApplicationUser> GetDefaultTechnicianUsersForOrders();
        ApplicationUser GetDefaultTechniciansForTeam(int teamId);


        OrderStatusChangedFlixyDto GetOrderStatusChangedFlixyDto(int orderId,int newStatusId);

        decimal GetOrderRemainingValue(OrderObject order);
        //ProcessResult<bool> SendIdeleBreakAlert();
    }
}

