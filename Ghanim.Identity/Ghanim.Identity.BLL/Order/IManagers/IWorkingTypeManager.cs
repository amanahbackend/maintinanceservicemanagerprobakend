﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IWorkingTypeManager : ICashableRepository<WorkingType, WorkingTypeMiniViewModel>
    {
        ProcessResult<WorkingType> CheckWorkingTypeNoDoublication(string name,   int? Id = null);
    }
}
