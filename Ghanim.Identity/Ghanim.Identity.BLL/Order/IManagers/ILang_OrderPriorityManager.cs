﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_OrderPriorityManager : IRepository<Lang_OrderPriority>
    {
        ProcessResult<List<Lang_OrderPriority>> GetAllLanguagesByOrderPriorityId(int OrderPriorityId);
        ProcessResult<bool> UpdateByOrderPriority(List<Lang_OrderPriority> entities);
    }
}
