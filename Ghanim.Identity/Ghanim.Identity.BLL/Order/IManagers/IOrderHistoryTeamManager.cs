﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.IManagers
{
    public interface IOrderHistoryTeamManager : IRepository<OrderHistoryTeam>
    {
    }
}
