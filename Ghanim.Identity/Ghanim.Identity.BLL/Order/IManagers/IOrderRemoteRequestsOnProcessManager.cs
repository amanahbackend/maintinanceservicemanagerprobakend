﻿using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.IManagers
{ 
    public interface IOrderRemoteRequestsOnProcessManager : IRepository<OrderRemoteRequestsOnProcess>
    {
    }
}
