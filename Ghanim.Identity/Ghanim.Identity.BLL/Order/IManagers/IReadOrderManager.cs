﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace Ghanim.BLL.IManagers
{
    public interface IReadOrderManager
    {
        ProcessResult<List<OrderObject>> Process(string path, UploadFile Uploadfile);
        void ReadFile();
    }
}