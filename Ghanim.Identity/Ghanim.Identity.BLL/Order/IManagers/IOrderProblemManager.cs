﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Ghanim.API;

namespace Ghanim.BLL.IManagers
{
    public interface IOrderProblemManager : ICashableRepository<OrderProblem,OrderProblemViewModel>
    {
        ProcessResult<OrderProblem> CheckOrderProblemNoDoublication(string name, string code, int? Id = null);
    }
}
