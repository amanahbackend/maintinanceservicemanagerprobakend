﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IOrderStatusManager : ICashableRepository<OrderStatus, OrderStatusViewModel>
    {
        ProcessResult<List<OrderStatus>> GetAllExclude(List<int> excludedStatuseIds);
          ProcessResult<bool> LogicalDeleteById(params object[] id);
        ProcessResult<OrderStatus> CheckOrderStatusNoDoublication(string name, string code, int? Id = null);
        bool CheckCanUpdateOrBlock(int Id);
        //{
        //    OrderStatus  = manger._set.Find(id);
        //    return LogicalDelete(entity);
        //}
    }
}
