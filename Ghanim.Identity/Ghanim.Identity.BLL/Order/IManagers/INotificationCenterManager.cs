﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface INotificationCenterManager : IRepository<NotificationCenter>
    {
        ProcessResult<List<NotificationCenter>> FilterByDate(DateTime? startDate, DateTime? endDate);
        ProcessResult<bool> MarkAllAsRead(string recieverId);
       // ProcessResult<List<NotificationCenter>> FilterByDate(List<int> orderIds, DateTime? endDate);
    }
}