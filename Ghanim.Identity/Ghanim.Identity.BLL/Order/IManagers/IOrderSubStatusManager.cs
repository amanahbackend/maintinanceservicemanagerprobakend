﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IOrderSubStatusManager : IRepository<OrderSubStatus>
    {
        ProcessResult<List<OrderSubStatus>> GetByStatusId(int statusId);
        ProcessResult<List<OrderSubStatus>> GetByStatusIdWithBlocked(int statusId);

        ProcessResult<OrderSubStatus> CheckOrderSubStatusNoDoublication(string subStatusName, int? Id = null,
            string code = "");

        //ProcessResult<OrderSubStatus> CheckOrderSubStatusNoDoublication(string subStatusName, int? Id = null, string Code = "");
    }
}
