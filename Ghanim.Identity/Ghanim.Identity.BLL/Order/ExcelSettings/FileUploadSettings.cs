﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.Models.Context;

namespace Ghanim.BLL.ExcelSettings
{
    public static class FileUploadSettings
    {
        public static string SourceFilePath { get; set; }
        public static string TargetFilePath { get; set; }
        public static string HostName { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static string FileExtention { get; set; }
        public static string ReadingPeriod { get; set; }
        public static string ConnectionString { get; set; }
        public static string DBOptions { get; set; }

        public static string proxyUrl { get; set; }
        public static string paciServiceUrl { get; set; }
        public static string paciNumberFieldName { get; set; }
        public static string blockServiceUrl { get; set; }
        public static string blockNameFieldNameBlockService { get; set; }
        public static string nhoodNameFieldName { get; set; }
        public static string streetServiceUrl { get; set; }
        public static string blockNameFieldNameStreetService { get; set; }
        public static string streetNameFieldName { get; set; }

        public static ApplicationDbContext GetDbContext()
        {
            var optsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optsBuilder.UseSqlServer(ConnectionString);
           return new ApplicationDbContext(optsBuilder.Options);
        }
    }
}
