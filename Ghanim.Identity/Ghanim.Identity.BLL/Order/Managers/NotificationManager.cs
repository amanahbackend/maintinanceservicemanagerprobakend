﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;
namespace Ghanim.BLL.Managers
{
    public class NotificationManager : Repository<Notification>, INotificationManager
    {
        public NotificationManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
