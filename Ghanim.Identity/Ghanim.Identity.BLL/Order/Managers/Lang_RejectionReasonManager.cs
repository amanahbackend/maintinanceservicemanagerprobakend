﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{
    public class Lang_RejectionReasonManager : Repository<Lang_RejectionReason>, ILang_RejectionReasonManager
    {
        public Lang_RejectionReasonManager(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}