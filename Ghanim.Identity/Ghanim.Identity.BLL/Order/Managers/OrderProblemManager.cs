﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.BLL.Managers
{
    public class OrderProblemManager : CashableRepository<OrderProblem, OrderProblemViewModel>, IOrderProblemManager
    {
        public OrderProblemManager(ApplicationDbContext context, ICached<OrderProblemViewModel> cash)
            : base(context, cash)
        {
        }

        public override ProcessResult<OrderProblem> Add(OrderProblem model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<OrderProblem>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckOrderProblemNoDoublication(model.Name, model.Code, null);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed(model, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);

            return base.Add(model);
        }

        public override ProcessResult<bool> Update(OrderProblem model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckOrderProblemNoDoublication(model.Name, model.Code, model.Id);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue); 
            return base.Update(model);
        }


        public ProcessResult<OrderProblem> CheckOrderProblemNoDoublication(string name, string code, int? Id = null)
        {
            try
            {
                OrderProblem result = null;
                //if (string.IsNullOrEmpty(Code) && Id != null)
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() && x.Id != Id.Value)?.Data;
                //else
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() ||
                //                 x.Code == Code)?.Data;
                //if (string.IsNullOrEmpty(code) && Id != null)
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() && x.Id != Id.Value)?.Data;
                //else if (!string.IsNullOrEmpty(code) && Id == null)
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() || x.Code == code)?.Data;
                //else if (!string.IsNullOrEmpty(code) && Id != null)
                result = Get(x => x.Id != Id && (x.Name.ToLower() == name.ToLower() || x.Code.ToLower() == code.ToLower())).Data;

                if (result == null)
                    return ProcessResultHelper.Succedded(result, (string)null, ProcessResultStatusCode.Succeded, "CheckOrderProblemNoDoublication");
                else
                    return ProcessResultHelper.Failed(result, null, message: string.Format(SharedResource.General_Duplicate, SharedResource.Entity_Problem), ProcessResultStatusCode.InvalidValue, "CheckOrderProblemNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<OrderProblem>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckOrderProblemNoDoublication");
            }
        }




        public override ProcessResult<bool> LogicalDelete(OrderProblem model)
        {
            string blockingMsg;
            if (!CheckCanBlock(model.Id,out blockingMsg))
                return ProcessResultHelper.Failed<bool>
                    (false, null, message: blockingMsg, ProcessResultStatusCode.InvalidValue);
            return base.LogicalDelete(model);
        } 
        public bool CheckCanBlock(int Id,out string msg)
        {
            bool valid = true;
            var obj = base.Get(Id); 
            if (obj.Data.DispatcherSettings != null && obj.Data.DispatcherSettings.Count >0 )
            {
                valid = false;
                msg = SharedResource.Problem_Dispatching_Block;
                return valid;
            } 
            if (obj.Data.Orders != null )
            {
                List<int> status = new List<int>() { 1,2,3,4,5,6 };
                if (obj.Data.Orders.Any(x => status.Contains( x.StatusId) ))
                {
                    msg = SharedResource.Problem_Ongoing_Block;
                    return valid = false;
                }
            }
            msg = "";
            return valid;
        }
    }
}
