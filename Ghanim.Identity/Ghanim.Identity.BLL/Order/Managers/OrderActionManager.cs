﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class OrderActionManager : Repository<OrderAction>, IOrderActionManager
    {
        private DbContext _context;

        public OrderActionManager(ApplicationDbContext context)
            : base(context)
        {
            _context = context;
        }

        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                _context.Database.ExecuteSqlCommand("TRUNCATE TABLE [OrderActions]");
                return ProcessResultHelper.Succedded<bool>(true);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}
