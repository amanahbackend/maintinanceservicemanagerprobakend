﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Ghanim.API;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.BLL.Managers
{
    public class OrderStatusManager : CashableRepository<OrderStatus,OrderStatusViewModel>, IOrderStatusManager
    {
 
        private ApplicationDbContext _context;
        public OrderStatusManager(ApplicationDbContext context, ICached<OrderStatusViewModel>  cash)
            : base(context, cash)
        {
            _context = context;
 
        }

        public ProcessResult<List<OrderStatus>> GetAllExclude(List<int> excludedStatuseIds)
        {
            return GetAll(x => !excludedStatuseIds.Contains(x.Id));
        }
        public virtual ProcessResult<bool> LogicalDeleteById(params object[] id)
        {
            // int num = (int)Convert.ChangeType(id, TypeCode.Int32);
            
           // OrderStatus orderStatus = new OrderStatus();
            var entity = _context.OrderStatuses.Find(id);
            return LogicalDelete(entity);
        }
        //public override ProcessResult<bool> DeleteById(params object[] id)
        //{
        //    try
        //    {
        //        var entity = _context.JunkUsers.Find(id);
        //        return Delete(entity);
        //    }
        //    catch (Exception ex)
        //    {   
        //        return ProcessResultHelper.Failed(false, ex);
        //    }
        //}
        //public virtual ProcessResult<bool> LogicalDelete(OrderStatus entity)
        //{
        //    bool data = false;
        //    try
        //    {
        //        entity.IsDeleted = !entity.IsDeleted;
        //        data = (this.Context as ApplicationDbContext).SaveChanges() > 0;
        //        return ProcessResultHelper.Succedded(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessResultHelper.Failed(data, ex);
        //    }
        //}

        public override ProcessResult<bool> LogicalDelete(OrderStatus model)
        {
               if (!CheckCanUpdateOrBlock(model.Id))
                return ProcessResultHelper.Failed<bool>
                    (false, null, message: SharedResource.OnHoldCancellations_Block, ProcessResultStatusCode.InvalidValue);

            return base.LogicalDelete(model);
        }

        public override ProcessResult<OrderStatus> Add(OrderStatus model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<OrderStatus>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);           
            var checkNoDubArea = CheckOrderStatusNoDoublication(model.Name,  model.Code,null);
            if (!checkNoDubArea.IsSucceeded)
                return checkNoDubArea; 
            return base.Add(model);
         }

        public override ProcessResult<bool> Update(OrderStatus model)
        {  
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);
            if(!CheckCanUpdateOrBlock(model.Id))
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.OnHoldCancellations_Block, ProcessResultStatusCode.InvalidValue);

            var checkNoDublication = CheckOrderStatusNoDoublication(model.Name, model.Code, model.Id);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            return base.Update(model);
        }

        public ProcessResult<OrderStatus> CheckOrderStatusNoDoublication(string name, string code, int? Id = null)
        {
            try
            {
                OrderStatus result = null;
                if (name != null && code != null && Id != null)
                    result = Get(x => (x.Name.ToLower() == name.ToLower() || x.Code == code)
                                        && x.Id != Id.Value)?.Data;
                else if (name != null && code != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() ||
                         x.Code == code)?.Data;
                else if (name != null && code == null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                else if (name != null && code == null && Id != null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() && x.Id != Id.Value)?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<OrderStatus>(result, null, message: SharedResource.OnHoldCancellations_Duplicate, ProcessResultStatusCode.InvalidValue, "CheckOrderStatusNoDoublication");
                else
                    return ProcessResultHelper.Succedded<OrderStatus>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckOrderStatusNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<OrderStatus>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckOrderStatusNoDoublication");
            }
        }

        public bool CheckCanUpdateOrBlock(int Id)
        {
            return Id > 8 ? true : false; 
        }

    }
}