﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;

namespace Ghanim.BLL.Managers
{
    public class Lang_NotificationManager : Repository<Lang_Notification>, ILang_NotificationManager
    {
        public Lang_NotificationManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
