﻿using CommonEnum;
using CommonEnums;
using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Ghanim.API.Helper;
using Ghanim.API.ServiceCommunications.FlixyApi;
using Ghanim.BLL.Services.FlixyApi;
using Ghanim.BLL.Settings;
using Ghanim.Models.Context;
using Ghanim.Models.Order.Enums;
using Microsoft.Extensions.Options;
using Utilites;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Identity;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class OrderManager : Repository<OrderObject>, IOrderManager
    {
        public IOrderActionManager actionManager;
        public IOrderTypeManager orderTypeManager;

        private readonly IAPIHelper _helper;
        private readonly IFlixyService _flixService;
        private readonly StaticFilesSettings _filesSettings;
        private UserManager<ApplicationUser> _identityUserManager;

        public OrderManager(ApplicationDbContext context,
            IOrderActionManager _actionManager, IAPIHelper helper, IFlixyService flixService, IOptions<StaticFilesSettings> filesSettings)//IOrderTypeManager _orderTypeManager
           : base(context)
        {
            actionManager = _actionManager;
            _helper = helper;
            _flixService = flixService;
            _filesSettings = filesSettings.Value;
            //   orderTypeManager = _orderTypeManager;
        }



        public override ProcessResult<PaginatedItems<OrderObject>> GetAllPaginated(PaginatedItems<OrderObject> paginatedItems, Expression<Func<OrderObject, bool>> predicate, params Expression<Func<OrderObject, object>>[] include)
        {
            try
            {
                string orderTypeCode;
                ProcessResult<PaginatedItems<OrderObject>> result = null;
                if (predicate == null)
                {
                    result = base.GetAllPaginated(paginatedItems);
                }
                else
                {
                    if (paginatedItems == null)
                    {
                        paginatedItems = new PaginatedItems<OrderObject>();
                    }
                    if (paginatedItems.PageNo == 0)
                        paginatedItems.PageNo = 1;
                    if (paginatedItems.PageSize == 0)
                        paginatedItems.PageSize = 10;
                    var count = Context.Orders.Where(x => x.IsDeleted == false).Where(predicate).AsNoTracking().Count();
                    paginatedItems.Count = count;
                    var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                    var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
                    if (paginatedItems.PageNo >= 0)
                    {
                        if (count > paginatedItems.PageSize)
                        {
                            if (takeCount > count)
                            {
                                if (skipCount < count)
                                {
                                    paginatedItems.Data = Context.Orders.Where(x => x.IsDeleted == false).Where(predicate).AsNoTracking().Skip(skipCount).Take(count - skipCount).ToList();
                                }
                                //else
                                //{
                                //    data = null;
                                //}
                            }
                            else
                            {
                                paginatedItems.Data = Context.Orders.Where(x => x.IsDeleted == false).Where(predicate).AsNoTracking().Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                            }
                        }
                        else
                        {
                            if (skipCount >= count)
                            {
                                paginatedItems.Data = Context.Orders.Where(x => x.IsDeleted == false).Where(predicate).AsNoTracking().Skip(skipCount).Take(paginatedItems.PageSize).ToList();

                                //return empty -set
                                paginatedItems.Data = Context.Orders.Where(x => x.IsDeleted == false).Skip(Context.Orders.Count()).AsNoTracking().ToList();
                            }
                            else
                            {
                                paginatedItems.Data = Context.Orders.Where(x => x.IsDeleted == false).Where(predicate).AsNoTracking().Skip(skipCount).Take(count - skipCount).ToList();
                                //foreach (var item in paginatedItems.Data)
                                //{
                                //   // var OrderTypeRes=orderTypeManager.
                                //    // item.TypeName= paginatedItems.Data.FirstOrDefault(x=>x.TypeId==)
                                //}
                            }
                        }
                    }
                    result = ProcessResultHelper.Succedded(paginatedItems);

                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }
        public ProcessResult<bool> SetAcceptence(int? orderId, bool acceptenceFlag, int? rejectionReasonId, string rejectionReason, int? initialStatusId, string initialStatusName, int? initialSubStatusId, string initialSubStatusName)
        {
            try
            {
                var order = Get(x => x.Id == orderId);
                if (order.IsSucceeded == true)
                {

                    if (acceptenceFlag)
                    {
                        if (order.Data != null)
                        {
                            order.Data.AcceptanceFlag = AcceptenceType.Accepted;
                        }
                    }
                    else
                    {
                        if (rejectionReasonId < 1)
                        {
                            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.RequiredRejectionReason);
                        }
                        order.Data.AcceptanceFlag = AcceptenceType.Rejected;
                        order.Data.RejectionReason = rejectionReason;
                        order.Data.RejectionReasonId = (int)rejectionReasonId;
                        order.Data.StatusId = (int)initialStatusId;
                        order.Data.SubStatusId = (int)initialSubStatusId;
                        //order.Data.StatusName = initialStatusName;
                        //order.Data.SubStatusName = initialSubStatusName;

                        order.Data.PrevTeamId = order.Data.TeamId ?? order.Data.PrevTeamId;
                        order.Data.TeamId = null;
                        order.Data.RankInTeam = 0;


                        //order team history
                        OrderHistoryTeam ordHistory = new OrderHistoryTeam();
                        ordHistory.OrderId = order.Data.Id;
                        ordHistory.Code = order.Data.Code;
                        ordHistory.PrevTeamId = order.Data.PrevTeamId;
                        ordHistory.TeamId = order.Data.TeamId;
                        Context.OrderHistoryTeams.Add(ordHistory);
                        Context.SaveChanges();
                    }
                    var updateResult = Update(order.Data);





                    if (updateResult.IsSucceeded == true)
                    {
                        //if (acceptenceFlag)
                        //{
                        //    AddAction(order.Data, OrderActionType.Acceptence);
                        //}
                        //else
                        //{
                        //    AddAction(order.Data, OrderActionType.Rejection);
                        //}

                        return ProcessResultHelper.Succedded<bool>(true);
                    }
                    else
                    {
                        return ProcessResultHelper.Failed<bool>(false, null, updateResult.Status.Message);
                    }
                }
                return ProcessResultHelper.Failed<bool>(false, null, order.Status.Message);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }

        public ProcessResult<bool> AcceptOrderAndSetOnTravel(int? orderId, string statusName, int statusId, string subStatusName, int subStatusId)
        {
            try
            {
                var order = Get(x => x.Id == orderId);
                if (order.IsSucceeded == true)
                {
                    order.Data.AcceptanceFlag = AcceptenceType.Accepted;
                    order.Data.StatusId = statusId;
                    order.Data.SubStatusId = subStatusId;
                    //order.Data.StatusName = statusName;
                    //order.Data.SubStatusName = subStatusName;

                    var updateResult = Update(order.Data);
                    if (updateResult.IsSucceeded == true)
                    {
                        // AddAction(order.Data, OrderActionType.FirstOrderWork);
                        return ProcessResultHelper.Succedded<bool>(true);
                    }
                    else
                    {
                        return ProcessResultHelper.Failed<bool>(false, null, updateResult.Status.Message);
                    }
                }
                return ProcessResultHelper.Failed<bool>(false, null, order.Status.Message);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }


        public OrderObject Assign(int orderId, int teamId)
        {

            int maxRankOrderForThatTeam = Context.Orders.Where(x => x.TeamId == teamId).Max(o => o.RankInTeam) ?? 0;

            var dispatcher = Context.Teams.FirstOrDefault(x => x.Id == teamId).Dispatcher;
            OrderObject tempOrder = Context.Orders.FirstOrDefault(x => x.Id == orderId);

            tempOrder.RankInTeam = ++maxRankOrderForThatTeam;
            tempOrder.StatusId = (int)OrderStatusEnum.Dispatched;


            if (tempOrder.PrevTeamId != null && tempOrder.PrevTeamId != 0)
            {
                //tempOrder.StatusName = OrderStatusEnum.Dispatched.ToString();
                tempOrder.SubStatusId = (int)OrderSubstatusEnum.Re_Scheduled;
                //tempOrder.SubStatusName = OrderSubstatusEnum.Re_Scheduled.ToString();
            }
            else
            {
                tempOrder.StatusId = (int)OrderStatusEnum.Dispatched;
                //tempOrder.StatusName = OrderStatusEnum.Dispatched.ToString();
                tempOrder.SubStatusId = (int)OrderSubstatusEnum.Scheduled;
                //tempOrder.SubStatusName = OrderSubstatusEnum.Scheduled.ToString();
            }

            //Get Team Orders
            //int maxOrderWithRanks;
            //var teamOrders = Context.Orders.Where(c => c.TeamId == teamId&&c.IsDeleted!=true&&c.AcceptanceFlag.ToString()!="1");
            //if (teamOrders.ToList()!=null &&teamOrders.ToList().Count>0)
            //{
            //   maxOrderWithRanks = teamOrders.Max(x => x.RankInTeam ?? 0);
            //}
            //else
            //{
            //    maxOrderWithRanks = -1;
            //}
            int maxOrderWithRanks = Context.Orders.Max(x => x.RankInTeam ?? 0);
            maxOrderWithRanks++;
            tempOrder.PrevTeamId = tempOrder.TeamId ?? tempOrder.PrevTeamId;
            tempOrder.TeamId = teamId;
            tempOrder.RankInTeam = maxOrderWithRanks;
            tempOrder.AcceptanceFlag = AcceptenceType.NoAction;
            tempOrder.DispatcherId = dispatcher.Id;
            tempOrder.SupervisorId = dispatcher.SupervisorId;

            var updatedOrder = Update(tempOrder);

            //order team history
            OrderHistoryTeam ordHistory = new OrderHistoryTeam();
            ordHistory.OrderId = tempOrder.Id;
            ordHistory.Code = tempOrder.Code;
            ordHistory.PrevTeamId = tempOrder.PrevTeamId;
            ordHistory.TeamId = tempOrder.TeamId;
            Context.OrderHistoryTeams.Add(ordHistory);
            return tempOrder;
        }
        //public ProcessResult<bool> SendIdeleBreakAlert()
        //{
        //    try
        //    {
        //        //var order = Get(x => x.Id == orderId);
        //        return ProcessResultHelper.Succedded<bool>(false, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessResultHelper.Failed<bool>(false, ex);
        //    }           
        //}

        public ProcessResult<bool> UnAssign(int orderId)
        {
            var tempOrder = Get(x => x.Id == orderId);
            if (tempOrder.IsSucceeded && tempOrder.Data != null)
            {
                tempOrder.Data.PrevTeamId = tempOrder?.Data?.TeamId ?? tempOrder.Data.PrevTeamId;
                tempOrder.Data.TeamId = null;
                // check status before un assign
                // in case it was Dispatched
                // in the below check it will be Open
                //if (tempOrder.Data.StatusId == nextStatusId + 1)
                //{
                tempOrder.Data.StatusId = (int)OrderStatusEnum.Open;
                //tempOrder.Data.StatusName = OrderStatusEnum.Open.ToString();
                tempOrder.Data.SubStatusId = (int)OrderSubstatusEnum.Released;
                //tempOrder.Data.SubStatusName = OrderSubstatusEnum.Released.ToString();

                tempOrder.Data.AcceptanceFlag = AcceptenceType.NoAction;
                tempOrder.Data.RankInTeam = 0;

                //}
                // here we need to make it Hold ==> and sub status as the same reason
                // when it was hold and in the team column

                var updatedOrder = Update(tempOrder.Data);
                //order team history
                OrderHistoryTeam ordHistory = new OrderHistoryTeam();
                ordHistory.OrderId = tempOrder.Data.Id;
                ordHistory.Code = tempOrder.Data.Code;
                ordHistory.PrevTeamId = tempOrder.Data.PrevTeamId;
                ordHistory.TeamId = tempOrder.Data.TeamId;
                Context.OrderHistoryTeams.Add(ordHistory);
                return updatedOrder;
            }
            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen, ProcessResultStatusCode.Failed, "Assign Manager");
        }

        public ProcessResult<bool> BulkUnAssign(List<int> orderIds, int statusId)
        {
            var tempOrders = GetAll(x => orderIds.Contains(x.Id));
            if (tempOrders.IsSucceeded && tempOrders.Data.Count > 0)
            {
                for (int i = 0; i < tempOrders.Data.Count; i++)
                {
                    tempOrders.Data[i].TeamId = null;
                    tempOrders.Data[i].StatusId = statusId;
                    //tempOrders.Data[i].StatusName = statusName;
                }
                var updatedOrders = Update(tempOrders.Data);
                if (updatedOrders.IsSucceeded && updatedOrders.Data == true)
                {
                    for (int i = 0; i < tempOrders.Data.Count; i++)
                    {
                        AddActionForDispatcherForBuilkAssign(tempOrders.Data[i], OrderActionType.BulkUnAssign);
                    }
                }

                return updatedOrders;
            }
            return ProcessResultHelper.Failed<bool>(false, null,SharedResource.General_WrongWhileGet , Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "BulkUnAssign Manager");
        }




        public ProcessResult<bool> ChangeOrderRank2(List<OrderObject> teamOrders, Dictionary<int, int> orders, int OnTravelNo, int NotOnTravelNo)
        {
            try
            {
                //var c=0;
                //var z=0;
                var oldValue = 0;
                var NewValue = 0;
                //for (int i = 0; i < teamOrders.Count; i++)
                //{
                //    if (orders.ElementAt(i).Value == 0)
                //        c = orders.ElementAt(i).Key;
                //    if (orders.ElementAt(i).Value == 1)
                //        z = orders.ElementAt(i).Key;
                //}
                for (int i = 0; i < teamOrders.Count; i++)
                {
                    oldValue = teamOrders[i].RankInTeam.Value;
                    NewValue = oldValue + 1;
                    teamOrders[i].RankInTeam = NewValue;
                    if (teamOrders[i].Id == NotOnTravelNo)
                        teamOrders[i].RankInTeam = 1;
                    if (teamOrders[i].Id == OnTravelNo)
                        teamOrders[i].RankInTeam = 0;
                    //teamOrders[i].RankInTeam = orders[teamOrders[i].Id];

                    //    teamOrders[1].RankInTeam = z;
                    //teamOrders[teamOrders.Count-1].RankInTeam = c;

                }
                var updatedOrders = Update(teamOrders);

                if (updatedOrders.IsSucceeded && updatedOrders.Data == true)
                {
                    for (int i = 0; i < teamOrders.Count; i++)
                    {
                        //  AddAction(teamOrders[i], OrderActionType.ChangeTeamRank);
                    }
                }
                return updatedOrders;

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex, SharedResource.General_SomethingWrongHappen, Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "BulkUnAssign Manager");
            }
        }

        public void AddActionForDispatcherForBuilkAssign(OrderObject order, OrderActionType actionType)
        {
            string usetrId = _helper.CurrentUserId();
            OrderAction orderAction = new OrderAction()
            {
                ActionDate = DateTime.Now,
                ActionDay = DateTime.Now.Date,
                ActionTime = DateTime.Now.TimeOfDay,
                StatusId = order.StatusId,
                //StatusName = order.StatusName,
                OrderId = order.Id,
                SubStatusId = order.SubStatusId,
                //SubStatusName = order.SubStatusName,
                ActionTypeId = (int)actionType,
                ActionTypeName = EnumManager<OrderActionType>.GetName(actionType),
                SupervisorId = order.SupervisorId,
                DispatcherId = order?.DispatcherId,
                //DispatcherName = order.DispatcherName,
                //SupervisorName = order.SupervisorName(),
                TeamId = order?.TeamId,
                CurrentUserId = usetrId,
                FK_CreatedBy_Id = usetrId,
            };
            actionManager.Add(orderAction);
        }

        public ProcessResult<List<OrderObject>> Add(List<OrderObject> ordersLst, string excuterUserId)
        {
            try
            {
                for (int i = 0; i < ordersLst.Count; i++)
                {
                    // Check for repeated call
                    DateTime currentTime = DateTime.Now;

                    var customerOrders = GetAll(x => x.CustomerCode == ordersLst[i].CustomerCode && (currentTime - x.CreatedDate).TotalHours <= 24);
                    if (customerOrders.IsSucceeded && customerOrders.Data.Count > 0)
                    {
                        ordersLst[i].IsRepeatedCall = true;
                    }
                }
                var addedOrders = base.Add(ordersLst);
                if (addedOrders.IsSucceeded && addedOrders.Data.Count > 0)
                {

                    for (int i = 0; i < addedOrders.Data.Count; i++)
                    {
                        AddActionForDispatcherForBuilkAssign(addedOrders.Data[i], OrderActionType.AddOrder);
                    }
                }
                return addedOrders;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ProcessResult<List<OrderObject>> GetDispatcherOrders(int SupervisorId, int state)
        {
            var orders = GetAll(x => x.SupervisorId == SupervisorId && x.DispatcherId != 0 && x.StatusId == state);
            return orders;
        }

        public ProcessResult<OrderObject> AssignDispatcher(int orderId, int dispatcherId)
        {
            var orders = Get(x => x.Id == orderId);

            if (orders.Data != null)
            {
                orders.Data.DispatcherId = dispatcherId;
                orders.Data.TeamId = null;
                //orders.Data.DispatcherName = DispacherName;
                Update(orders.Data);
                return orders;
            }
            return null;
        }

        public ProcessResult<bool> UnAssignDispatcher(int orderId)
        {
            var orders = Get(x => x.Id == orderId);

            if (orders.Data != null)
            {
                orders.Data.DispatcherId = null;
                orders.Data.TeamId = null;
                //orders.Data.DispatcherName = "";
                var updatedOrder = Update(orders.Data);

                return updatedOrder;
            }
            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen, Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "Assign Manager");
        }


        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                Context.Database.ExecuteSqlCommand("delete from Orders");
                return ProcessResultHelper.Succedded<bool>(true);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }

        public string ChoosePin(int onHoldCount, string orderType, DateTime orderDate, int? problemId)
        {
            var pinsDomain = ChooseFolder(onHoldCount, problemId);
            string externalColor = string.Empty;
            string internalColor;
            // check order type
            if (orderType?.ToLower() == "Preventive maintenance order".ToLower())
            {
                internalColor = "blue";
            }
            else if (orderType?.ToLower() == "Cash compressor".ToLower())
            {
                internalColor = "orange";
            }
            else if (orderType?.ToLower() == "Cash".ToLower() || orderType?.ToLower() == "Cash Call order".ToLower())
            {
                internalColor = "darkgreen";
            }
            else
            {
                internalColor = "white";
            }
            // check order day
            //var date = new Date(orderDate.toLowerCase());
            var d = orderDate.DayOfWeek;
            switch (d)
            {
                case DayOfWeek.Sunday: //sunday
                    externalColor = "pink/pink";
                    break;
                case DayOfWeek.Monday: // monday
                    externalColor = "lightgreen/lightgreen";
                    break;
                case DayOfWeek.Tuesday: // tuesday
                    externalColor = "lightblue/lightblue";
                    break;
                case DayOfWeek.Wednesday: // wednesday
                    externalColor = "purple/purple";
                    break;
                case DayOfWeek.Thursday: // thursday
                    externalColor = "turquoise/turquoise";
                    break;
                case DayOfWeek.Friday: // friday
                    externalColor = "gray/gray";
                    break;
                case DayOfWeek.Saturday: // saturday
                    externalColor = "yellow/yellow";
                    break;
            }
            // reset pin
            string pin = pinsDomain + externalColor + "." + internalColor + ".png";
            if (onHoldCount != 0)
            {
                pin = pinsDomain + externalColor + "." + internalColor + ".onhold.png";
            }
            return pin;
        }

        public string ChooseFolder(int onHoldCount, string problem)
        {
            string baseURL;
            if (onHoldCount != 0)
            {
                baseURL = "assets/Images/on-hold-pins/";
            }
            else
            {
                baseURL = "assets/Images/pins/";
            }

            dynamic result = new System.Dynamic.ExpandoObject();
            result.baseURL = baseURL;
            result.name = string.Empty;

            switch (problem)
            {
                case "Commercial CAC":
                    result.baseURL += "CAC/";
                    result.name = "CAC";
                    break;
                case "Chillers":
                    result.baseURL += "CHI/";
                    result.name = "CHI";
                    break;
                case "CHS":
                    result.baseURL += "CHS/";
                    result.name = "CHS";
                    break;
                case "Cleaning Equipment":
                    result.baseURL += "/";
                    result.name = "";
                    break;
                case "Electrical":
                    result.baseURL += "ELEC/";
                    result.name = "ELEC";
                    break;
                case "Elevators":
                    result.baseURL += "ZELE/";
                    result.name = "ZELE";
                    break;
                case "Fire Fighting":
                    result.baseURL += "ZFIR/";
                    result.name = "ZFIR";
                    break;
                case "Fork Lifts":
                    result.name = "FLT";
                    result.baseURL += "FLT/";
                    break;
                case "Government Service":
                    result.name = "GAC";
                    result.baseURL += "GAC/";
                    break;
                case "Non Res Non Std":
                    result.name = "NSD";
                    result.baseURL += "NSD/";
                    break;
                case "Plumbing":
                    result.name = "PLB";
                    result.baseURL += "PLB/";
                    break;
                case "Residential CAC":
                    result.name = "RAC";
                    result.baseURL += "RAC/";
                    break;
                case "Residential Electrical Service":
                    result.name = "REL";
                    result.baseURL += "REL/";
                    break;
                case "Residential Plumbing Service":
                    result.name = "RPB";
                    result.baseURL += "RPB/";
                    break;
                case "Residential Swimming Pool Serv":
                    result.name = "RSW";
                    result.baseURL += "RSW/";
                    break;
                case "Security Audio Visual":
                    result.name = "SAV";
                    result.baseURL += "SAV/";
                    break;
                case "SRAC Service":
                    result.name = "SRA";
                    result.baseURL += "SRA/";
                    break;
                case "Non Res Standard":
                    result.name = "STD";
                    result.baseURL += "STD/";
                    break;
                case "Stationed operator":
                    result.name = "STO";
                    result.baseURL += "STO/";
                    break;
                case "Subcontractor":
                    result.name = "SUB";
                    result.baseURL += "SUB/";
                    break;
                case "Technology":
                    result.name = "TEC";
                    result.baseURL += "TEC/";
                    break;
                case "Video Data Network":
                    result.name = "VDN";
                    result.baseURL += "VDN/";
                    break;
                case "Water Cooler":
                    result.name = "WTC";
                    result.baseURL += "WTC/";
                    break;
                default:
                    result.baseURL += "PINS";
                    result.name = "PINS/";
                    break;
            }
            return baseURL + result.name + "/";
        }


        public string ChooseFolder(int onHoldCount, int? problemId)
        {
            string baseURL;
            if (onHoldCount != 0)
            {
                baseURL = "assets/Images/on-hold-pins/";
            }
            else
            {
                baseURL = "assets/Images/pins/";
            }

            dynamic result = new System.Dynamic.ExpandoObject();
            result.baseURL = baseURL;
            result.name = string.Empty;

            switch (problemId)
            {
                case 1:
                    result.baseURL += "CAC/";
                    result.name = "CAC";
                    break;
                case 2:
                    result.baseURL += "CHI/";
                    result.name = "CHI";
                    break;
                case 3:
                    result.baseURL += "CHS/";
                    result.name = "CHS";
                    break;
                case 4:
                    result.baseURL += "/";
                    result.name = "";
                    break;
                case 5:
                    result.baseURL += "ELEC/";
                    result.name = "ELEC";
                    break;
                case 6:
                    result.baseURL += "ZELE/";
                    result.name = "ZELE";
                    break;
                case 7:
                    result.baseURL += "ZFIR/";
                    result.name = "ZFIR";
                    break;
                case 8:
                    result.name = "FLT";
                    result.baseURL += "FLT/";
                    break;
                case 9:
                    result.name = "GAC";
                    result.baseURL += "GAC/";
                    break;
                case 10:
                    result.name = "NSD";
                    result.baseURL += "NSD/";
                    break;
                case 11:
                    result.name = "PLB";
                    result.baseURL += "PLB/";
                    break;
                case 12:
                    result.name = "RAC";
                    result.baseURL += "RAC/";
                    break;
                case 13:
                    result.name = "REL";
                    result.baseURL += "REL/";
                    break;
                case 14:
                    result.name = "RPB";
                    result.baseURL += "RPB/";
                    break;
                case 15:
                    result.name = "RSW";
                    result.baseURL += "RSW/";
                    break;
                case 16:
                    result.name = "SAV";
                    result.baseURL += "SAV/";
                    break;
                case 17:
                    result.name = "SRA";
                    result.baseURL += "SRA/";
                    break;
                case 18:
                    result.name = "STD";
                    result.baseURL += "STD/";
                    break;
                case 19:
                    result.name = "STO";
                    result.baseURL += "STO/";
                    break;
                case 20:
                    result.name = "SUB";
                    result.baseURL += "SUB/";
                    break;
                case 21:
                    result.name = "TEC";
                    result.baseURL += "TEC/";
                    break;
                case 22:
                    result.name = "VDN";
                    result.baseURL += "VDN/";
                    break;
                case 23:
                    result.name = "WTC";
                    result.baseURL += "WTC/";
                    break;
                default:
                    result.baseURL += "PINS";
                    result.name = "PINS/";
                    break;
            }
            return baseURL + result.name + "/";
        }


        public override ProcessResult<bool> Update(OrderObject entity)
        {
            int lastStatus = Context.Orders.FirstOrDefault(x => x.Id == entity.Id).StatusId;


            //if the order wasnt onhold and became onhold then add onhold count
            if (lastStatus != (int)OrderStatusEnum.On_Hold && entity.StatusId == (int)OrderStatusEnum.On_Hold)
                entity.OnHoldCount++;

            //get current user and check if he is tech then update the last tech userid in order

            string AppUserId = _helper.CurrentUserId();
            var User = Context.Users.Where(x => x.Id == AppUserId).FirstOrDefault();
            if (User != null && (User.UserTypeId == UserTypesEnum.Technician || User.UserTypeId == UserTypesEnum.Driver))
            {
                entity.LastActorTechUserId = AppUserId;
            }
            if (lastStatus != entity.StatusId)
            {
                var inpt = GetOrderStatusChangedFlixyDto(entity.Id, entity.StatusId);
                var result = _flixService.OrderStatusChanged(inpt).Result;
            }

            return base.Update(entity);
        }




        public OrderStatusChangedFlixyDto GetOrderStatusChangedFlixyDto(int orderId, int newStatusId)
        {
            OrderObject order = Get(orderId).Data;
            OrderStatus newStatus = Context.OrderStatuses.FirstOrDefault(x => x.Id == newStatusId);

            int? teamId = order.TeamId;
            ApplicationUser techUser;

            //get Current tech for order by its team
            if (teamId != null)
            {
                techUser = GetDefaultTechniciansForTeam(order.TeamId.Value);
            }
            else
            {
                var lastOrderActionThatExcutedByTech = actionManager.GetAllQuerable().Data
                    .Include(x => x.ExcuterUser.Technician)
                    .Where(x => x.OrderId == orderId).OrderBy(x => x.Id)
                    .LastOrDefault(x => x.ExcuterUser.Technician != null);

                teamId = lastOrderActionThatExcutedByTech?.TeamId;
                techUser = lastOrderActionThatExcutedByTech?.ExcuterUser;
            }

            TeamMember vehiclesThatWasUsed = Context.Members.FirstOrDefault(x => !x.IsDeleted && x.TeamId == teamId && x.MemberType == MemberType.Vehicle);

            var result = new OrderStatusChangedFlixyDto()
            {
                ProfileTechnicianNameEnglish = techUser?.FirstName ?? "NA",
                ProfileTechnicianNameArabic = techUser?.LastName ?? "NA",
                TechnicianEmployeeID = techUser?.PF ?? "NA",
                Cancellationreason = order?.CancellationReason,
                ProfilePicture = _filesSettings.defaultProfilePicture,
                TechnicianOrder = new OrderStatusChangedFlixyDto_TechnicianOrder()
                {
                    OrderSAPId = order.Code,
                    OrderCoordinates = $"{order.Lat}, {order.Long}",
                    PlateNumber = vehiclesThatWasUsed?.MemberParentName,
                    SAPStatus = newStatus?.Code,
                    Remainingvalue = GetOrderRemainingValue(order).ToString(),
                },
            };

            return result;
        }

        // <summary>
        // All amount of money that client payed
        //total done Payments On Order =all Payments On Order(done by tech or disp or sup) + order.AmountPaid(from csv);
        //what should client pay after discount
        //what Should Client Pay = order.ServiceTotalAmount(from csv) - (order.ServiceTotalAmount(from csv) * order.OfferDiscount(from csv));
        //result = what Should Client Pay - total done Payments On Order
        // </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public decimal GetOrderRemainingValue(OrderObject order)
        {
            //All amount of money that client payed
            decimal allPaymentsOnOrder = Context.PaymentTransactions.Where(x => x.OrderId == order.Id)
                .Select(x => x.Payment).Sum();
            allPaymentsOnOrder += order.AmountPaid;

            //what should client pay after discount
            decimal whatShouldClientPay = order.ServiceTotalAmount - (order.ServiceTotalAmount * (decimal)order.OfferDiscount); //(order.AmountPaid );

            return whatShouldClientPay - allPaymentsOnOrder;
        }




        public Dictionary<int?, string> GetDefaultTechniciansForOrders()
            => GetDefaultTechnicianUsersForOrders().ToDictionary(x => x.Key, x => x.Value.FullName);


        public Dictionary<int?, ApplicationUser> GetDefaultTechnicianUsersForOrders()
        {
            Dictionary<int?, ApplicationUser> teamIdDefaultTechName = Context.Technicians.Include(x => x.User)
                .Where(x => x.TeamId != null && !x.IsDriver).DistinctBy(x => x.TeamId).ToDictionary(x => x.TeamId, x => x.User);
            return teamIdDefaultTechName;
        }

        public ApplicationUser GetDefaultTechniciansForTeam(int teamId)
        {
            ApplicationUser result;
            var teamIdDefaultTechName = GetDefaultTechnicianUsersForOrders();
            teamIdDefaultTechName.TryGetValue(teamId, out result);
            return result;
        }
    }
}