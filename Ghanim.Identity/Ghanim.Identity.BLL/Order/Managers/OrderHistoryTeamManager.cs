﻿using Ghanim.API.Helper;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class OrderHistoryTeamManager : Repository<OrderHistoryTeam> , IOrderHistoryTeamManager
    {
        private readonly IAPIHelper _helper;
        public OrderHistoryTeamManager( ApplicationDbContext context, IAPIHelper helper)
          : base(context)
        {
            _helper = helper;
        }
         public override ProcessResult<OrderHistoryTeam> Add(OrderHistoryTeam entity)
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.FK_CreatedBy_Id = _helper.CurrentUserId();
            return base.Add(entity);    
        }
    
    }
}
