﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class OrderSubStatusManager : Repository<OrderSubStatus>, IOrderSubStatusManager
    {

        public OrderSubStatusManager(ApplicationDbContext context)
            : base(context)
        {

        }

        public ProcessResult<List<OrderSubStatus>> GetByStatusId(int statusId)
        {
            return GetAll(x => x.StatusId == statusId);
        }

        public ProcessResult<List<OrderSubStatus>> GetByStatusIdWithBlocked(int statusId)
        {
            return GetAllWithBlocked(x => x.StatusId == statusId);
        }

        public ProcessResult<OrderSubStatus> AddOrderSubStatus(OrderSubStatus entity)
        {
            try
            {
                List<OrderSubStatus> orders = new List<OrderSubStatus>();
                if (entity.Code != null)
                {
                    var checkNoDub = CheckOrderSubStatusNoDoublication(entity.Name, null, entity.Code);
                    if (!checkNoDub.IsSucceeded)
                        return checkNoDub;
                }
                else
                {
                    return ProcessResultHelper.Failed(entity, null, SharedResource.NoNullStatusCode);
                }

                entity.IsDeleted = false;
                var result = base.Add(entity);
                return ProcessResultHelper.Succedded(entity);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(entity, ex);
            }
        }

        public override ProcessResult<OrderSubStatus> Add(OrderSubStatus model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<OrderSubStatus>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var checkNoDubArea = CheckOrderSubStatusNoDoublication(model.Name, null, model.Code);
            if (!checkNoDubArea.IsSucceeded)
                return ProcessResultHelper.Failed<OrderSubStatus>(model, null, message: checkNoDubArea.Status.Message, ProcessResultStatusCode.InvalidValue);

            return base.Add(model);
        }


        public override ProcessResult<bool> Update(OrderSubStatus model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);
            var checkNoDublication = CheckOrderSubStatusNoDoublication(model.Name, model.Id, model.Code);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            return base.Update(model);
        }
        /// <summary>
        /// you must add atleast two param from this three
        /// </summary>
        /// <param name="subStatusName"></param>
        /// <param name="Id"></param>
        /// <param name="Code"></param>
        /// <returns></returns>

        public ProcessResult<OrderSubStatus> CheckOrderSubStatusNoDoublication(string subStatusName, int? Id = null, string code = "")
        {
            try
            {
                OrderSubStatus result = null;
                //if (string.IsNullOrEmpty(Code) && Id != null)
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() && x.Id != Id.Value)?.Data;
                //else
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() ||
                //                 x.Code == Code)?.Data;
                //if (string.IsNullOrEmpty(code) && Id != null)
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() && x.Id != Id.Value)?.Data;
                //else if (!string.IsNullOrEmpty(code) && Id == null)
                //    result = Get(x => x.Name.ToLower() == subStatusName.ToLower() || x.Code == code)?.Data;
                //else if (!string.IsNullOrEmpty(code) && Id != null)
                result = Get(x => x.Id != Id && (x.Name.ToLower() == subStatusName.ToLower() || x.Code.ToLower() == code.ToLower())).Data;

                if (result == null)
                    return ProcessResultHelper.Succedded(result, (string)null, ProcessResultStatusCode.Succeded, "CheckOrderSubStatusNoDoublication");
                else
                    return ProcessResultHelper.Failed(result, null, message: SharedResource.OnHoldCancellations_Duplicate, ProcessResultStatusCode.InvalidValue, "CheckAreaDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<OrderSubStatus>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckOrderSubStatusNoDoublication");
            }
        }

    }
}