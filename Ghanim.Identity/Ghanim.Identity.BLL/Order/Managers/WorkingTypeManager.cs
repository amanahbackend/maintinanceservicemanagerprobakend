﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class WorkingTypeManager : CashableRepository<WorkingType, WorkingTypeMiniViewModel>, IWorkingTypeManager
    {
        public WorkingTypeManager(ApplicationDbContext context, ICached<WorkingTypeMiniViewModel> cash)
            : base(context, cash)
        {
 
        }
        public override ProcessResult<WorkingType> Add(WorkingType model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<WorkingType>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckWorkingTypeNoDoublication(model.Name, null);
            if (!checkNoDublication.IsSucceeded)
                return checkNoDublication;

            return base.Add(model);
        }

        public override ProcessResult<bool> Update(WorkingType model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckWorkingTypeNoDoublication(model.Name , model.Id);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            return base.Update(model);
        }
        public ProcessResult<WorkingType> CheckWorkingTypeNoDoublication(string name, int? Id = null)
        {
            try
            {
                WorkingType result = null;
                if (name != null  && Id != null)
                    result = Get(x =>  x.Name.ToLower() == name.ToLower()  
                                        && x.Id != Id.Value)?.Data; 
                else if (name != null   && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() )?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<WorkingType>(result, null, message: SharedResource.IdleBreak_Duplicate, ProcessResultStatusCode.InvalidValue, "CheckWorkingTypeNoDoublication");
                else
                    return ProcessResultHelper.Succedded<WorkingType>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckWorkingTypeNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<WorkingType>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckWorkingTypeNoDoublication");
            }
        }
    }
}
