﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{
    public class RejectionReasonManager : Repository<RejectionReason>, IRejectionReasonManager
    {
        public RejectionReasonManager(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}