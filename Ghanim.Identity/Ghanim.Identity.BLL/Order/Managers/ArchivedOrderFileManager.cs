﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class ArchivedOrderFileManager : Repository<ArchivedOrderFile>, IArchivedOrderFileManager
    {
        private readonly ApplicationDbContext _context;
        public ArchivedOrderFileManager(ApplicationDbContext context)
            : base(context)
        {
            _context = context;
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                _context.Database.ExecuteSqlCommand("TRUNCATE TABLE [ArchivedOrderFile]");
                return ProcessResultHelper.Succedded<bool>(true);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }

        public override ProcessResult<bool> Delete(List<ArchivedOrderFile> entity)
        {
            bool data = false;
            try
            {
                _context.RemoveRange(entity);
                data = _context.SaveChanges() > 0;
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }

        public async Task<ProcessResult<ArchivedOrderFile>> AddFileName(ArchivedOrderFile entity)
        {
            try
            {
                if (Validator.IsValid(entity))
                {
                    entity.IsDeleted = false;
                    await _context.AddAsync(entity);
                    await _context.SaveChangesAsync();
                    return ProcessResultHelper.Succedded(entity);
                }
                else
                {
                    return ProcessResultHelper.Failed<ArchivedOrderFile>(null, null, "error while adding record");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<ArchivedOrderFile>(null, ex, "error while adding record");
            }
        }
    }
}
