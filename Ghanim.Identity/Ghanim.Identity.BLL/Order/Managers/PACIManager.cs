﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;
using Utilites.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class PACIManager : Repository<PACI>, IPACIManager
    {
        private readonly ApplicationDbContext _context;
        public PACIManager(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public ProcessResult<PACI> AddRow(PACI entity)
        {
            try
            {
                if (Validator.IsValid(entity))
                {
                    entity.IsDeleted = false;
                    _context.Add(entity);
                    _context.SaveChanges();
                    return ProcessResultHelper.Succedded(entity);
                }
                else
                {
                    return ProcessResultHelper.Failed<PACI>(null, null,SharedResource.General_SomethingWrongHappen );
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<PACI>(null, ex, SharedResource.General_SomethingWrongHappen);
            }
        }
    }
}
