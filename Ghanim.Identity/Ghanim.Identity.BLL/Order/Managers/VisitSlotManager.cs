﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{ 
    public class VisitSlotManager : Repository<VisitSlot>, IVisitSlotManager
    {
        public VisitSlotManager(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}
