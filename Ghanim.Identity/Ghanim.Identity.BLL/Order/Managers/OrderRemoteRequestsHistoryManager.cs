﻿using Ghanim.BLL.IManagers;
using Ghanim.BLL.Managers;
 
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.Order.Managers
{ 
    public class OrderRemoteRequestsHistoryManager : Repository<OrderRemoteRequestsHistory>, IOrderRemoteRequestsHistoryManager
    {
        public OrderRemoteRequestsHistoryManager(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}
