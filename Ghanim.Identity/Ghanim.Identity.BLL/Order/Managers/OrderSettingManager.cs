﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{
    public class OrderSettingManager : Repository<OrderSetting>, IOrderSettingManager
    {
        public OrderSettingManager(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}
