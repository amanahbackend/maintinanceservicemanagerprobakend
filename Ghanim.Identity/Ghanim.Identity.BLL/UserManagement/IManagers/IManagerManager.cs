﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IManagerManager :IRepository<Manager>
    {
       ProcessResult<Manager> GetByUserId(string userId);
       ProcessResult<bool> DeleteAll();
    }
}
