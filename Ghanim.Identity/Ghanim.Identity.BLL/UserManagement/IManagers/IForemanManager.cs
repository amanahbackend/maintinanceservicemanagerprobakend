﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IForemanManager : IRepository<Foreman>
    {
        ProcessResult<Foreman> GetByUserId(string userId);
        ProcessResult<bool> DeleteAll();
    }
}