﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IDispatcherSettingsManager : IRepository<DispatcherSettings>
    {
        ProcessResult<List<DispatcherSettings>> GetByDispatcherId(int dispatcherId);
        ProcessResult<List<DispatcherSettings>> GetbyGroupId(int groupId);
        ProcessResult<bool> DeleteAll();
    }
}