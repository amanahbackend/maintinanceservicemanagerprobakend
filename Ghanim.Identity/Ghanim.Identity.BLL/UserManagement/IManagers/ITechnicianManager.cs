﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ITechnicianManager : IRepository<Technician>
    {
         ProcessResult<Technician> GetByUserId(string userId);
         ProcessResult<List<Technician>> Search(string word);
         ProcessResult<bool> DeleteAll();
    }
}
