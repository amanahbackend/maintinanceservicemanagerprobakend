﻿using CommonEnums;
using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Ghanim.BLL.NotMappedModels;
namespace Ghanim.BLL.IManagers
{
    public interface ITeamMemberManager : IRepository<TeamMember>
    {
        ProcessResult<List<TeamMember>> GetUnassignedMember();
        ProcessResult<IQueryable<TeamMember>> GetUnassignedMemberByDivisionId(int divisionId);
        ProcessResult<bool> AssignedMemberToTeam(int teamId, int memberId, bool isDefaultTech);
        ProcessResult<List<TeamMember>> GetUnassignedVehicleMember();
        ProcessResult<TeamMember> GetByMemberType_TeamId(int teamId, int memberType);
        ProcessResult<TeamMember> GetByMemberParentId(int memberParentId);
        ProcessResult<List<TeamMember>> GetByTeamId(int teamId);
        ProcessResult<bool> SortMemeber(int teamId, List<TeamMemberRanks> teamMembers);
        ProcessResult<List<TeamMember>> SearchVec(string word);
        ProcessResult<List<TeamMember>> SearchTech(string word, int division);
        ProcessResult<bool> DeleteAll();
    }
}