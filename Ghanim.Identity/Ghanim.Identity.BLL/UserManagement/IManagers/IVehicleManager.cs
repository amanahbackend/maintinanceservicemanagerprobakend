﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IVehicleManager : IRepository<Vehicle>
    {
         ProcessResult<List<Vehicle>> Search(string word);
         ProcessResult<bool> DeleteAll();
    }
}
