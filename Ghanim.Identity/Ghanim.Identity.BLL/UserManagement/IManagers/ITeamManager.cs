﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ghanim.BLL.Managers;
using Utilites.ProcessingResult;
using Ghanim.BLL.UserManagement.NotMappedModels;

namespace Ghanim.BLL.IManagers
{
    public interface ITeamManager : IRepository<Team>
    {
      Task<  ProcessResult<List<Team>>> GetByDispatcherId(int dispatcherId);
        ProcessResult<List<Team>> GetByPreviosId(int PreviousId);

        ProcessResult<List<DispatcherWithTeamsViewModel>> GetByDispatchersId(List<int> dispatcherIds);
        ProcessResult<List<Team>> GetByFormansId(List<int?> formansId);
        ProcessResult<List<Team>> GetByFormansIdWithBlocked(List<int?> formansId);
        ProcessResult<List<Team>> SearchByTeamName(string word);
        ProcessResult<bool> DeleteAll();
        Task<bool> DeleteAsync(int id);
        Task<List<TeamWithoutMembersViewModel>> GetTeamsWithOutMembers();
    }
}