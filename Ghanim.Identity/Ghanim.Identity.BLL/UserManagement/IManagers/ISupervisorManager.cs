﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
   public interface ISupervisorManager : IRepository<Supervisor>
    {
        ProcessResult<Supervisor> GetByUserId(string userId);
        ProcessResult<Supervisor> GetById(int id);
        ProcessResult<bool> DeleteAll();
    }
}
