﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IEngineerManager : IRepository<Engineer>
    {
        ProcessResult<Engineer> GetByUserId(string userId);
        ProcessResult<bool> DeleteAll();
    }
}
