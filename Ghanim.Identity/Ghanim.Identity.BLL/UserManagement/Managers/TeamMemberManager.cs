﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using CommonEnums;
using Ghanim.Models.Context;
using Ghanim.BLL.NotMappedModels;

namespace Ghanim.BLL.Managers
{
    public class TeamMemberManager : Repository<TeamMember>, ITeamMemberManager
    {
        IServiceProvider serviceProvider;
        ITeamMembersHistoryManager _ITeamMembersHistoryManager;
        public TeamMemberManager(IServiceProvider _serviceProvider, ITeamMembersHistoryManager ITeamMembersHistoryManager ,
            ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
            _ITeamMembersHistoryManager = ITeamMembersHistoryManager;
        }

        public ProcessResult<List<TeamMember>> GetUnassignedMember()
        {
            List<TeamMember> input = null;
            try
            {
                input = GetAll(x => x.TeamId == null && x.MemberType != MemberType.Vehicle).Data.ToList();
                return ProcessResultHelper.Succedded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetUnassignedMember");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetUnassignedMember");
            }
        }

        public ProcessResult<IQueryable<TeamMember>> GetUnassignedMemberByDivisionId(int divisionId)
        {
            IQueryable<TeamMember> data = null;
            try
            {
                data = GetAllQuerable().Data.Where(x => (x.TeamId == 0 && x.MemberType != MemberType.Vehicle && x.DivisionId != null && x.DivisionId == divisionId) || (x.TeamId == null && x.MemberType != MemberType.Vehicle && x.DivisionId != null && x.DivisionId == divisionId));
                return ProcessResultHelper.Succedded(data, (string)null, ProcessResultStatusCode.Succeded, "GetUnassignedMember");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<IQueryable<TeamMember>>(null, ex, (string)null, ProcessResultStatusCode.Failed, "GetUnassignedMember");
            }
        }

        public ProcessResult<List<TeamMember>> GetUnassignedVehicleMember()
        {
            List<TeamMember> input = null;
            try
            {
                input = GetAll(x => (x.TeamId == null && x.MemberType == MemberType.Vehicle) || (x.TeamId == 0 && x.MemberType == MemberType.Vehicle)).Data.ToList();
                return ProcessResultHelper.Succedded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetUnassignedMember");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetUnassignedMember");
            }
        }

        public ProcessResult<bool> AssignedMemberToTeam(int teamId, int MemberId,bool isDefaultTech = false)
        {
            bool result = false;
            try
            {
                var teamMemberRes = Get(MemberId).Data;
                if (teamMemberRes != null)
                {
                    
                    teamMemberRes.IsDefault = isDefaultTech;
                    #region LogTeamMember
                    TeamMembersHistory logMember = new TeamMembersHistory();
                    logMember.PrevTeamId = teamMemberRes.TeamId;
                    logMember.TeamId = teamId;
                    logMember.UserId = teamMemberRes.UserId; 
                    logMember.UserDetailId = teamMemberRes.MemberParentId;
                    logMember.UserTypeId = teamMemberRes.UserTypeId;
                    if (teamMemberRes.MemberType == MemberType.Vehicle)
                    {
                        logMember.VehicleId = teamMemberRes.MemberParentId;
                    }
                    _ITeamMembersHistoryManager.Add(logMember);
                    #endregion
                    teamMemberRes.TeamId = teamId;
                }
                result = Update(teamMemberRes).Data;

                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "assignedMemberToTeam");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "assignedMemberToTeam");
            }
        }

        public ProcessResult<TeamMember> GetByMemberType_TeamId(int teamId, int memberType)
        {
            TeamMember input = null;
            try
            {
                input = Get(x => (int)x.MemberType == memberType && x.TeamId == teamId).Data;
                return ProcessResultHelper.Succedded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMemberType_TeamId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMemberType_TeamId");
            }
        }

        public ProcessResult<TeamMember> GetByMemberParentId(int memberParentId)
        {
            TeamMember input = null;
            try
            {
                input = Get(x => x.MemberParentId == memberParentId).Data;
                return ProcessResultHelper.Succedded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMemberParentId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<TeamMember>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMemberParentId");
            }
        }

        public ProcessResult<List<TeamMember>> GetByTeamId(int teamId)
        {
            List<TeamMember> input = null;
            try
            {
                input = GetAll(x => x.TeamId == teamId).Data.ToList();
                return ProcessResultHelper.Succedded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMemberType_TeamId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMemberType_TeamId");
            }
        }

        public ProcessResult<bool> SortMemeber(int teamId, List<TeamMemberRanks> teamMembers)
        {
            bool result = false;
            try
            {
                foreach (var item in teamMembers)
                {
                    var teamMemberRes = Get(item.Id).Data;
                    teamMemberRes.Rank = item.Rank;
                    result = Update(teamMemberRes).Data;
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "SortMemeber");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "SortMemeber");
            }

        }

        public ProcessResult<List<TeamMember>> SearchTech(string word, int division)
        {
            List<TeamMember> input = null;
            try
            {
                if (!string.IsNullOrEmpty(word))
                {
                    input = GetAll(x => x.TeamId == null && x.DivisionId == division && x.MemberType != MemberType.Vehicle && x.User.PF.ToLower().Contains(word.ToLower()), x => x.User,x=> x.Division).Data.ToList();
                }
                else
                {
                    input = GetAll(x => x.TeamId == null  && x.DivisionId == division&& x.MemberType != MemberType.Vehicle ,x => x.User, x => x.Division).Data.ToList();
                }
                return ProcessResultHelper.Succedded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }

        public ProcessResult<List<TeamMember>> SearchVec(string word)
        {
            List<TeamMember> input = null;
            try
            {
                if (!string.IsNullOrEmpty(word))
                {
                    input = GetAll(x => x.MemberParentName.Contains(word) && x.MemberType == MemberType.Vehicle).Data.Take(100).ToList();
                }
                else
                {
                    input = GetAllQuerable().Data.Take(100).ToList();
                }
                return ProcessResultHelper.Succedded<List<TeamMember>>(input, (string)null, ProcessResultStatusCode.Succeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<TeamMember>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var teamMembers = GetAll();
                var deleteResult = Delete(teamMembers.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }


        public override ProcessResult<TeamMember> Add(TeamMember model)
        {
            if (model.UserTypeId == 0)
            {
                var User = Context.Users.Where(x => x.Id == model.UserId).FirstOrDefault();
                model.UserTypeId = User.UserTypeId;
            }

            #region LogTeamMember
            TeamMembersHistory logMember = new TeamMembersHistory();
            logMember.PrevTeamId = null;
            logMember.TeamId = model.TeamId;
            logMember.UserId = model.UserId;
            logMember.UserDetailId = model.MemberParentId;
            logMember.UserTypeId = model.UserTypeId;
            _ITeamMembersHistoryManager.Add(logMember);
            #endregion
            return base.Add(model);
        }

        }
}