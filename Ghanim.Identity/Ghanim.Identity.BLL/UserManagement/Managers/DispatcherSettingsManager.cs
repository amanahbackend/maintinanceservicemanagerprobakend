﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
   public class DispatcherSettingsManager : Repository<DispatcherSettings>, IDispatcherSettingsManager
    {
        IServiceProvider serviceProvider;
        public DispatcherSettingsManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<List<DispatcherSettings>> GetByDispatcherId(int dispatcherId)
        {
            List<DispatcherSettings> input = null;
            try
            {                                                                 
                input = GetAll(x => x.DispatcherId == dispatcherId,x => x.OrderProblem,x=> x.Dispatcher,x=> x.Dispatcher.User).Data.ToList();
                return ProcessResultHelper.Succedded<List<DispatcherSettings>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<DispatcherSettings>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }
        public ProcessResult<List<DispatcherSettings>> GetbyGroupId(int groupId)
        {
            List<DispatcherSettings> input = null;
            try
            {
                input = GetAll(x => x.GroupId == groupId).Data.ToList();
                return ProcessResultHelper.Succedded<List<DispatcherSettings>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<DispatcherSettings>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var dispatcherSettings = GetAll();
                var deleteResult = Delete(dispatcherSettings.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}