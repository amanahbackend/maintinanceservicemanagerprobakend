﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Ghanim.Models.Context;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class ManagerManager :Repository<Manager> ,IManagerManager
    {
        IServiceProvider serviceProvider;
        public ManagerManager(IServiceProvider _serviceProvidor, ApplicationDbContext context) :base(context)
        {
            serviceProvider = _serviceProvidor;
        }

        public ProcessResult<Manager> GetByUserId(string userId)
        {
            Manager input = null;
            try
            {
                input = Get(x => x.UserId == userId).Data;
                return ProcessResultHelper.Succedded<Manager>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<Manager>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var managers = GetAll();
                var deleteResult = Delete(managers.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}
