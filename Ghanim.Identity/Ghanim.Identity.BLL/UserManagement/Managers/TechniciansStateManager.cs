﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class TechniciansStateManager : Repository<TechniciansState>, ITechniciansStateManager
    {
        IServiceProvider serviceProvider;
        public TechniciansStateManager(IServiceProvider _serviceProvidor, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvidor;
        }

        public ProcessResult<TechniciansState> GetLastStateByTechnicianId(int technicianId)
        {
            var technicianStates = base.GetAll(x => x.TechnicianId == technicianId).Data.LastOrDefault();
            return ProcessResultHelper.Succedded<TechniciansState>(technicianStates, (string)null, ProcessResultStatusCode.Succeded, "GetLastStateByTechnicianId");
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var techniciansState = GetAll();
                var deleteResult = Delete(techniciansState.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}
