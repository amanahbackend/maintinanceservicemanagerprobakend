﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class SupervisorManager : Repository<Supervisor>, ISupervisorManager
    {
        IServiceProvider serviceProvider;
        private ApplicationDbContext currentConext;
        public SupervisorManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
            currentConext = context;
        }

        public ProcessResult<Supervisor> GetById(int id)
        {
            var input = currentConext.Supervisors.FirstOrDefault(x => x.Id == id);
            //var input = Get(x => x.Id  == id).Data;
           //var input = currentConext.Supervisors
           //    .Include(x => x.Division)
           //    .Include(x => x.CostCenter)
           //    .FirstOrDefault(x => x.Id == id);

            return ProcessResultHelper.Succedded<Supervisor>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");

        }

        public ProcessResult<Supervisor> GetByUserId(string userId)
        {
            Supervisor input = null;
            try
            {
                input = Get(x => x.UserId == userId).Data;
                return ProcessResultHelper.Succedded<Supervisor>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<Supervisor>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var supervisors = GetAll();
                var deleteResult = Delete(supervisors.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}