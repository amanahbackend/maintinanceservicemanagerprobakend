﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{
    public class AttendenceManager : Repository<Attendence>, IAttenenceManager
    {
        IServiceProvider serviceProvider;
        public AttendenceManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }
    }
}