﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Ghanim.BLL.UserManagement.NotMappedModels;
using Ghanim.Models.Order.Enums;

namespace Ghanim.BLL.Managers
{
    public class TeamManager : Repository<Team>, ITeamManager
    {
        IServiceProvider serviceProvider;
        public TeamManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<List<Team>> GetByPreviosId(int Id)
        {
            List<Team> input = null;
            try
            {
                input = GetAll(x => x.Id == Id).Data.ToList();
                return ProcessResultHelper.Succedded<List<Team>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }
        public async Task<ProcessResult<List<Team>>> GetByDispatcherId(int dispatcherId)
        {
            List<Team> input = null;
            try
            {
                var result = await GetAllAsync(x => x.DispatcherId == dispatcherId && x.Members.Any());
                input = result.Data.ToList();
                return ProcessResultHelper.Succedded<List<Team>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByDispatcherId");
            }

            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }

        public ProcessResult<List<DispatcherWithTeamsViewModel>> GetByDispatchersId(List<int> dispatcherIds)
        {
            try
            {
                //refactor issue
                var result = GetAll(x => dispatcherIds.Contains(x.DispatcherId)).Data.GroupBy(x => new { x.DispatcherId }, (key, group) => new DispatcherWithTeamsViewModel { DispatcherId = key.DispatcherId, Teams = group }).ToList();
                return ProcessResultHelper.Succedded<List<DispatcherWithTeamsViewModel>>(result, (string)null, ProcessResultStatusCode.Succeded, "GetByDispatchersId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<DispatcherWithTeamsViewModel>>(new List<DispatcherWithTeamsViewModel>(), ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatchersId");
            }
        }

        public ProcessResult<List<Team>> GetByFormansId(List<int?> formansId)
        {
            formansId.Remove(0);
            try
            {
                var result = GetAll(x => formansId.Contains(x.ForemanId)).Data.ToList();
                return ProcessResultHelper.Succedded<List<Team>>(result, (string)null, ProcessResultStatusCode.Succeded, "GetByFormansId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(null, ex, (string)null, ProcessResultStatusCode.Failed, "GetByFormansId");
            }
        }
        public ProcessResult<List<Team>> GetByFormansIdWithBlocked(List<int?> formansId)
        {
            formansId.Remove(0);
            try
            {
                var result = GetAllWithBlocked(x => formansId.Contains(x.ForemanId)).Data.ToList();
                return ProcessResultHelper.Succedded<List<Team>>(result, (string)null, ProcessResultStatusCode.Succeded, "GetByFormansId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(null, ex, (string)null, ProcessResultStatusCode.Failed, "GetByFormansId");
            }
        }

        public ProcessResult<List<Team>> SearchByTeamName(string word)
        {
            List<Team> input = null;
            try
            {
                if (!string.IsNullOrEmpty(word))
                {
                    input = GetAll(x => x.Name.ToLower().Contains(word.ToLower())).Data.ToList();
                }
                else
                {
                    input = GetAllQuerable().Data.ToList();
                }
                return ProcessResultHelper.Succedded<List<Team>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByDispatcherId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Team>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDispatcherId");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var teams = GetAll();
                var deleteResult = Delete(teams.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
        public async Task<bool> DeleteAsync(int id)
        {
            Team input = null;
            input = GetAll(x => x.Id == id).Data.FirstOrDefault();
            if (input != null)
            {
                return await DeleteAsync(input.Id);
            }
            return false;
        }

        public async Task<List<TeamWithoutMembersViewModel>> GetTeamsWithOutMembers()
        {
            var teams = GetAllQuerable().Data.Where(x => !x.IsDeleted);
            return teams.Select(t => new TeamWithoutMembersViewModel
            {
                Id = t.Id,
                Name = t.Name,
                DivisionId = t.DivisionId,
                DivisionName = t.Division.Name,
                DispatcherId = t.DispatcherId,
                SupervisorId = t.SupervisorId,
                ForemanId = t.ForemanId,
                StatusId = t.StatusId,
                StatusName = t.Status.Name,
                EngineerId = t.EngineerId,
                ShiftId = t.ShiftId,
                OrdersCount = t.Orders.Count(x => x.TeamId == t.Id
                         && x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete),

            }).ToList();
        }
    }
}