﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class MaterialControllerManager :Repository<MaterialController>,IMaterialControllerManager
    {
        IServiceProvider serviceProvider;
        public MaterialControllerManager(IServiceProvider _serviceProvider, ApplicationDbContext context):base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var materialControllers = GetAll();
                var deleteResult = Delete(materialControllers.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }

    }
}
