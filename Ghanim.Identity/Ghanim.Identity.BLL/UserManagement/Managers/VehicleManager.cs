﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class VehicleManager : Repository<Vehicle>, IVehicleManager
    {
        IServiceProvider serviceProvider;
        public VehicleManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<List<Vehicle>> Search(string word)
        {
            List<Vehicle> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Plate_no.Contains(word)).Take(100).ToList();
                return ProcessResultHelper.Succedded<List<Vehicle>>(input, (string)null, ProcessResultStatusCode.Succeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Vehicle>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var vehicles = GetAll();
                var deleteResult = Delete(vehicles.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}