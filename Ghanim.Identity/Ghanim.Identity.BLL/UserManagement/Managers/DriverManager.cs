﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.Managers
{
   public class DriverManager : Repository<Driver>, IDriverManager
    {
        IServiceProvider serviceProvider;
        public DriverManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var drivers = GetAll();
                var deleteResult = Delete(drivers.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }

    }
}