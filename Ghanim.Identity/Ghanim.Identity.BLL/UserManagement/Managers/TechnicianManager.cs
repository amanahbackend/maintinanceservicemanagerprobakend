﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class TechnicianManager : Repository<Technician>, ITechnicianManager
    {
        IServiceProvider serviceProvider;
        public TechnicianManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<Technician> GetByUserId(string userId)
        {
            Technician input = null;
            try
            {
                input = Get(x => x.UserId == userId).Data;
                return ProcessResultHelper.Succedded<Technician>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<Technician>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
        }

        public ProcessResult<List<Technician>> Search(string word)
        {
            List<Technician> input = null;
            try
            {
                input = Context.Technicians.Where(x => !x.IsDeleted && $"{x.User.FirstName} {x.User.LastName}".Contains(word)).Take(100).ToList();
                return ProcessResultHelper.Succedded<List<Technician>>(input, (string)null, ProcessResultStatusCode.Succeded, "Search");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Technician>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "Search");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var technicians = GetAll();
                var deleteResult = Delete(technicians.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }



    }
}