﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
   public class DispatcherManager : Repository<Dispatcher>, IDispatcherManager
    {
        IServiceProvider serviceProvider;
        public DispatcherManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }
        public ProcessResult<Dispatcher> GetByUserId(string userId)
        {
            Dispatcher input = null;
            try
            {
                input = Get(x => x.UserId == userId).Data;
                return ProcessResultHelper.Succedded<Dispatcher>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<Dispatcher>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var dispatchers = GetAll();
                var deleteResult = Delete(dispatchers.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }


    }
}