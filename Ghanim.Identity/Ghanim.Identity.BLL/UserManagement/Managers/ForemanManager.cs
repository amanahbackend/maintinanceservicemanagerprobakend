﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class ForemanManager : Repository<Foreman>, IForemanManager
    {
        IServiceProvider serviceProvider;
        public ForemanManager(IServiceProvider _serviceProvider, ApplicationDbContext context) : base(context)
        {
            serviceProvider = _serviceProvider;
        }

        public ProcessResult<Foreman> GetByUserId(string userId)
        {
            Foreman input = null;
            try
            {
                input = Get(x => x.UserId == userId).Data;
                return ProcessResultHelper.Succedded<Foreman>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<Foreman>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByUserId");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var foreman = GetAll();
                var deleteResult = Delete(foreman.Data);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }
    }
}