﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.NotMappedModels
{
  public class Problems
    {
        public int ProblemId { get; set; }
        public string ProblemName { get; set; }
    }
}
