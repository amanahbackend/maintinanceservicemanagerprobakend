﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.NotMappedModels
{
    public class AreasWithOrderProblems
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public List<AreasVmForDispatcherSettings> Areas { get; set; }
        public List<Problems> OrderProblems { get; set; }
        public int GroupId { get; set; }

    }
}