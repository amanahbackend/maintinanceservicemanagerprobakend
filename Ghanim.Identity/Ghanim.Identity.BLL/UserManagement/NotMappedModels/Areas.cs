﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.NotMappedModels
{
    public class AreasVmForDispatcherSettings
    {
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
    }
}
