﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.UserManagement.NotMappedModels
{
    public class TeamWithoutMembersViewModel
    {
        public int Id { get; set; }

        public int? DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }

        public int? ForemanId { get; set; }
        public int Phone { get; set; }
        public int? EngineerId { get; set; }
        public int? DispatcherId { get; set; }
        public int? SupervisorId { get; set; }
        public string Name { get; set; }
        public int? ShiftId { get; set; }
        public int? OrdersCount { get; set; }
        public bool HasOrders => (OrdersCount ?? 0) > 0;

    }
}
