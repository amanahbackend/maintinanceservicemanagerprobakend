﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.NotMappedModels
{
   public class TeamMemberRanks
    {
        public int Id { get; set; }
        public int Rank { get; set; }
    }
}
