﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
   public class ApplicationUserHistoryManager : Repository<ApplicationUserHistory>, IApplicationUserHistoryManager
    {
        public ApplicationUserHistoryManager(ApplicationDbContext applicationDbContext)
            : base(applicationDbContext)
        {
        }
        public ProcessResult<List<ApplicationUserHistory>> GetLoginUser(string userId,string deviceId)
        {
            List<ApplicationUserHistory> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.UserId== userId && x.LogoutDate == null&&x.DeveiceId== deviceId).ToList();
                return ProcessResultHelper.Succedded<List<ApplicationUserHistory>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetLoginUser");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<ApplicationUserHistory>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetLoginUser");
            }
        }
        public async Task<ProcessResult<List<ApplicationUserHistory>>> GetLoginUsers()
        {
            List<ApplicationUserHistory> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x=> x.LogoutDate == null).ToList();
                return ProcessResultHelper.Succedded<List<ApplicationUserHistory>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetLoginUsers");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<ApplicationUserHistory>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetLoginUsers");
            }
        }

        public ProcessResult<List<ApplicationUserHistory>> IsTokenExpired(string token)
        {
            List<ApplicationUserHistory> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Token == token && x.LogoutDate != null).ToList();
                return ProcessResultHelper.Succedded<List<ApplicationUserHistory>>(input, (string)null, ProcessResultStatusCode.Succeded, "IsTokenExpired");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<ApplicationUserHistory>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "IsTokenExpired");
            }
        }
        public ProcessResult<bool> DeleteAll()
        {
            try
            {
                var users = GetAll().Data.Where(x => x.UserType != "Admin").ToList();
                var deleteResult = Delete(users);
                if (deleteResult.IsSucceeded)
                {
                    return ProcessResultHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultHelper.Failed<bool>(false, null, deleteResult.Status.Message);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex);
            }
        }

    }
}
