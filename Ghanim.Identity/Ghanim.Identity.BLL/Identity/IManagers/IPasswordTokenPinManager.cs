﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.IManagers
{
    public interface IPasswordTokenPinManager 
    {
        PasswordTokenPin Add(PasswordTokenPin entity);
        PasswordTokenPin GetByToken(string token);
        PasswordTokenPin GetByPin(string pin);
        bool Delete(PasswordTokenPin entity);
        bool IsPinExist(string pin);
    }
}
