﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IUserDeviceManager
    {
        void AddIfNotExist(UserDevice userDevice);
        Task AddIfNotExistAzureAsync(UserDevice userDevice);
        void DeleteAzure(string deviceId);
        void Delete(string deviceId);
        List<UserDevice> GetByUserId(string userId);
        bool DeleteAll();
    }
}
