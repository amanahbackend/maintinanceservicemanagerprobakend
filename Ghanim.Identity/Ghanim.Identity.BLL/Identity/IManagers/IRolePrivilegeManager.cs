﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ghanim.BLL.IManagers
{
    public interface IRolePrivilegeManager
    {
        RolePrivilege Add(RolePrivilege entity);
        List<RolePrivilege> GetByRoleId(string roleId);
    }
}
