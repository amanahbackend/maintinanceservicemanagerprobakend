﻿using Ghanim.Models.Entities;
using DispatchProduct.RepositoryModule;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IJunkUserManager : IIdentityRepository<JunkUser>
    {
        ProcessResult<bool> DeleteByUsername(string username);
        ProcessResult<JunkUser> GetJunkUser(ApplicationUser applicationUser);
    }
}
