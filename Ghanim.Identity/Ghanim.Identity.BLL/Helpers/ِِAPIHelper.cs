﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Helper
{
    public class APIHelper : IAPIHelper
    {
        IHttpContextAccessor _httpContextAccessor { set; get; }

        public APIHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetUserIdFromToken(string token)
        {
            JwtSecurityToken jwtTokenObj = new JwtSecurityToken(token);
            string userId = jwtTokenObj.Payload.Jti;
            return userId;
        }

        public string GetAuthHeader(HttpRequest request)
        {
            try
            {
                string authHeader = request.Headers["Authorization"];
                if (!string.IsNullOrEmpty(authHeader))
                {
                    authHeader = authHeader.Replace("bearer ", "");
                    authHeader = authHeader.Replace("bearer ", "");
                }

                return authHeader;
            }
            catch (Exception e)
            {
                return CurrentUserId();
            }
        }

        public string GetLanguageIdFromHeader()
        {
            string languageId = _httpContextAccessor.HttpContext.Request.Headers["LanguageId"];

            return languageId;
        }


        public string CurrentUserId ()
        {
            try
            {
                //return "7e1717f7-d3ac-40b1-8787-e2065b3d008e";
                //todo undo this for testinggggggggggggggggggggggggggggggggggggggggggggggggggggggggg

                string header = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                header = header.Replace("bearer ", "");

                string currentUserId = GetUserIdFromToken(header);
                return currentUserId;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }

}
