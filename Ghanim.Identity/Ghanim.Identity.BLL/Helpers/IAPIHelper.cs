﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Helper
{
   public interface IAPIHelper
    {
        string GetUserIdFromToken(string token);
        string GetAuthHeader(HttpRequest request);
        string CurrentUserId();
        string GetLanguageIdFromHeader();

    }
}
