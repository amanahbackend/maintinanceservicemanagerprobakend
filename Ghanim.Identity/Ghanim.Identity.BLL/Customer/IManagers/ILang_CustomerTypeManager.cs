﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_CustomerTypeManager : IRepository<Lang_CustomerType>
    {
        ProcessResult<List<Lang_CustomerType>> GetAllLanguagesByCustomerTypeId(int CsutomerTypeId);
        ProcessResult<List<Lang_CustomerType>> GetAllLanguagesByCustomerTypeIdWithBlocked(int CsutomerTypeId);

    }
}
