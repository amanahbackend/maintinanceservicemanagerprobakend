﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_MachineTypeManager : IRepository<Lang_MachineType>
    {
        ProcessResult<List<Lang_MachineType>> GetAllLanguagesByMachineTypeId(int CsutomerTypeId);
        ProcessResult<List<Lang_MachineType>> GetAllLanguagesByMachineTypeIdWithBlocked(int CsutomerTypeId);

    }
}
