﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_PhoneTypeManager : IRepository<Lang_PhoneType>
    {
        ProcessResult<List<Lang_PhoneType>> GetAllLanguagesByPhoneTypeId(int CsutomerTypeId);
        ProcessResult<List<Lang_PhoneType>> GetAllLanguagesByPhoneTypeIdWithBlocked(int CsutomerTypeId);

    }
}
