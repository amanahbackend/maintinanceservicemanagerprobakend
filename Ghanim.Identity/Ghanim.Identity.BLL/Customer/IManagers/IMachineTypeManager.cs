﻿using Ghanim.API;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IMachineTypeManager : ICashableRepository<MachineType, MachineTypeViewModel>
    {
        ProcessResult<MachineType> CheckMachineTypeNoDoublication(string name, int? Id = null);
        bool CheckCanUpdateOrBlock(int Id);

    }
}
