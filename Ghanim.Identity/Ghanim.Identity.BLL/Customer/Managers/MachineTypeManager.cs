﻿using Ghanim.API;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using Ghanim.ResourceLibrary.Resources;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class MachineTypeManager : CashableRepository<MachineType, MachineTypeViewModel>, IMachineTypeManager
    {
        public MachineTypeManager(ApplicationDbContext context, ICached<MachineTypeViewModel> cash)
            : base(context, cash)
        {
        }
        public ProcessResult<MachineType> CheckMachineTypeNoDoublication(string name, int? Id = null)
        {
            try
            {
                MachineType result = null;
                if (name != null && Id != null)
                    result = Get(x => (x.Name.ToLower() == name.ToLower())
                                        && x.Id != Id.Value)?.Data;
                else if (name != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                else if (name != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                else if (name != null && Id != null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() && x.Id != Id.Value)?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<MachineType>(result, null, message: string.Format(SharedResource.General_DuplicateWithoutCode, SharedResource.Entity_MachineType), ProcessResultStatusCode.InvalidValue, "CheckMachineTypeNoDoublication");
                else
                    return ProcessResultHelper.Succedded<MachineType>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckMachineTypeNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<MachineType>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckMachineTypeNoDoublication");
            }
        }

        public bool CheckCanUpdateOrBlock(int Id)
        {
            return true;
        }
    }
}
