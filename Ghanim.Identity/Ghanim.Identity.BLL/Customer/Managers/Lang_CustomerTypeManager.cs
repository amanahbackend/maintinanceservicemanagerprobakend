﻿using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;
using System.Linq;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class Lang_CustomerTypeManager : Repository<Lang_CustomerType>, ILang_CustomerTypeManager
    {
        IServiceProvider _serviceprovider;
        ApplicationDbContext _context;
        ISupportedLanguagesManager supportedLanguagesmanager;
        public Lang_CustomerTypeManager(IServiceProvider serviceprovider, ApplicationDbContext context, ISupportedLanguagesManager _supportedLanguagesmanager)
            : base(context)
        {
            _serviceprovider = serviceprovider;
            _context = context;
            supportedLanguagesmanager = _supportedLanguagesmanager;
        }
        //private ISupportedLanguagesManager supportedLanguagesmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ISupportedLanguagesManager>();
        //    }
        //}

        public ProcessResult<List<Lang_CustomerType>> GetAllLanguagesByCustomerTypeId(int CsutomerTypeId)
        {
            List<Lang_CustomerType> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_CustomerType_ID == CsutomerTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_CustomerType { FK_CustomerType_ID = CsutomerTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_CustomerType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByOrderStatusId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_CustomerType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderStatusId");
            }
        }


        public ProcessResult<List<Lang_CustomerType>> GetAllLanguagesByCustomerTypeIdWithBlocked(int CsutomerTypeId)
        {
            List<Lang_CustomerType> input = null;
            try
            {//blocked language must not included 
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerableWithBlocked().Data.Where(x => x.FK_CustomerType_ID == CsutomerTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_CustomerType { FK_CustomerType_ID = CsutomerTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_CustomerType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByAreaId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_CustomerType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByAreaId");
            }
        }
    }
}
