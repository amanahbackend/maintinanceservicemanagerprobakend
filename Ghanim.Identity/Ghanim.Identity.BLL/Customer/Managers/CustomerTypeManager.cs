﻿using Ghanim.API;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using Ghanim.ResourceLibrary.Resources;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class CustomerTypeManager: CashableRepository<CustomerType, CustomerTypeViewModel>, ICustomerTypeManager
    {
        public CustomerTypeManager(ApplicationDbContext context, ICached<CustomerTypeViewModel> cash)
            : base(context, cash)
        { }

        public ProcessResult<CustomerType> CheckCustomerTypeNoDoublication(string name ,int? Id = null)
        {
            try
            {
                CustomerType result = null;
                if (name != null  && Id != null)
                    result = Get(x => (x.Name.ToLower() == name.ToLower() )
                                        && x.Id != Id.Value)?.Data;
                else if (name != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                else if (name != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                else if (name != null  && Id != null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() && x.Id != Id.Value)?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<CustomerType>(result, null, message: string.Format(SharedResource.General_DuplicateWithoutCode, SharedResource.Entity_CustomerType), ProcessResultStatusCode.InvalidValue, "CheckCustomerTypeNoDoublication");
                else
                    return ProcessResultHelper.Succedded<CustomerType>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckCustomerTypeNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<CustomerType>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckCustomerTypeNoDoublication");
            }
        }

        public bool CheckCanUpdateOrBlock(int Id)
        {
            return true;
        }
    }
}
