﻿using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.BLL.Managers
{
    public class LocationManager : Repository<Location>, ILocationManager
    {
        public LocationManager(ApplicationDbContext context)
           : base(context)
        {
        }
    }
}
