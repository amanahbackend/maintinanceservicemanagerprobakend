﻿using Ghanim.API;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Managers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using Ghanim.ResourceLibrary.Resources;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
     public class PhoneTypeManager : CashableRepository<PhoneType, PhoneTypeViewModel>, IPhoneTypeManager
    {
        public PhoneTypeManager(ApplicationDbContext context, ICached<PhoneTypeViewModel> cash)
            : base(context, cash)
        {
        }
        public ProcessResult<PhoneType> CheckPhoneTypeNoDoublication(string name, int? Id = null)
        {
            try
            {
                PhoneType result = null;
                if (name != null && Id != null)
                    result = Get(x => (x.Name.ToLower() == name.ToLower())
                                        && x.Id != Id.Value)?.Data;
                else if (name != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                else if (name != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                else if (name != null && Id != null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() && x.Id != Id.Value)?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<PhoneType>(result, null, message: string.Format(SharedResource.General_DuplicateWithoutCode, SharedResource.Entity_PhoneType), ProcessResultStatusCode.InvalidValue, "CheckPhoneTypeNoDoublication");
                else
                    return ProcessResultHelper.Succedded<PhoneType>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckPhoneTypeNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<PhoneType>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckPhoneTypeNoDoublication");
            }
        }

        public bool CheckCanUpdateOrBlock(int Id)
        {
            return true;
        }
    }
}
