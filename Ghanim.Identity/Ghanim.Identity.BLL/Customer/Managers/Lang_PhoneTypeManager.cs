﻿using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class Lang_PhoneTypeManager : Repository<Lang_PhoneType>, ILang_PhoneTypeManager
    {
        IServiceProvider _serviceprovider;
        ApplicationDbContext _context;
        ISupportedLanguagesManager supportedLanguagesmanager;
        public Lang_PhoneTypeManager(IServiceProvider serviceprovider, ApplicationDbContext context, ISupportedLanguagesManager _supportedLanguagesmanager)
            : base(context)
        {
            _serviceprovider = serviceprovider;
            _context = context;
            supportedLanguagesmanager = _supportedLanguagesmanager;
        }


        public ProcessResult<List<Lang_PhoneType>> GetAllLanguagesByPhoneTypeId(int PhoneTypeId)
        {
            List<Lang_PhoneType> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_PhoneType_ID == PhoneTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_PhoneType { FK_PhoneType_ID = PhoneTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_PhoneType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByOrderStatusId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_PhoneType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderStatusId");
            }
        }


        public ProcessResult<List<Lang_PhoneType>> GetAllLanguagesByPhoneTypeIdWithBlocked(int PhoneTypeId)
        {
            List<Lang_PhoneType> input = null;
            try
            {//blocked language must not included 
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerableWithBlocked().Data.Where(x => x.FK_PhoneType_ID == PhoneTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_PhoneType { FK_PhoneType_ID = PhoneTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_PhoneType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByAreaId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_PhoneType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByAreaId");
            }
        }
    }
}
