﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.BLL.Caching.Abstracts;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.API
{
  public class AreasViewModel : RepoistryBaseEntity , ICloneable , IRepoistryBaseEntity
    {
       public int Area_No { get; set; }
       public string Name { get; set; }
       public int? Fk_Governorate_Id { get; set; }


        public AreasViewModel Clone<AreasViewModel>() => (AreasViewModel)MemberwiseClone();

    }
}
