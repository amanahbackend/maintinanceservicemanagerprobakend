﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.API
{
    public class DivisionViewModel : RepoistryBaseEntity, ICloneable, IRepoistryBaseEntity
    {
        public string Name { get; set; }
        public string code { get; set; }
        public DivisionViewModel Clone<DivisionViewModel>() => (DivisionViewModel)MemberwiseClone();
    }
}
