﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.API
{
    public class OrderStatusViewModel :BaseEntity, ICloneable, IRepoistryBaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public OrderStatusViewModel Clone<OrderStatusViewModel>() => (OrderStatusViewModel)MemberwiseClone();
    }
}
