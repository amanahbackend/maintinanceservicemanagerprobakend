﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.API
{
    public  class MapAdminRelatedViewModel : RepoistryBaseEntity, ICloneable, IRepoistryBaseEntity
    {
        public string Mobile_Api_Key { get; set; }
        public string Web_Api_Key { get; set; }
        public int Refresh_Rate { get; set; }


        public MapAdminRelatedViewModel Clone<MapAdminRelatedViewModel>() => (MapAdminRelatedViewModel)MemberwiseClone();

    }
}
