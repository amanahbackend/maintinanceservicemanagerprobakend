﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.API
{
    public class WorkingTypeMiniViewModel : BaseEntity, ICloneable, IRepoistryBaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int CategoryCode { get; set; }
        public WorkingTypeMiniViewModel Clone<WorkingTypeMiniViewModel>() => (WorkingTypeMiniViewModel)MemberwiseClone();
    }
}
