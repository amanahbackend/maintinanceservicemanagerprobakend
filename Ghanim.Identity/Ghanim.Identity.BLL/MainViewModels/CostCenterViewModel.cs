﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.API
{
    public class CostCenterViewModel : RepoistryBaseEntity, ICloneable, IRepoistryBaseEntity
    {
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public CostCenterViewModel Clone<CostCenterViewModel>() => (CostCenterViewModel)MemberwiseClone();
    }
}
