﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;


namespace Ghanim.API
{
    public class PhoneTypeViewModel : RepoistryBaseEntity, ICloneable, IRepoistryBaseEntity
    {
        public string Name { get; set; }
        public bool IsDefault { set; get; }

        public PhoneTypeViewModel Clone<PhoneTypeViewModel>() => (PhoneTypeViewModel)MemberwiseClone();


    }
}
