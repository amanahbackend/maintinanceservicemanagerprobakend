﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.API
{
   public class ContractTypesViewModel : RepoistryBaseEntity, ICloneable, IRepoistryBaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }

        public ContractTypesViewModel Clone<ContractTypesViewModel>() => (ContractTypesViewModel)MemberwiseClone();
    }
}
