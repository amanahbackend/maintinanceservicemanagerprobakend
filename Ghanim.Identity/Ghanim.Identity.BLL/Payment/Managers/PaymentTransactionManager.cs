﻿using CommonEnum;
using CommonEnums;
using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Ghanim.API.Helper;
using Ghanim.Models.Context;
using Ghanim.Models.Order.Enums;
using Ghanim.Models.Payment.Entities;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Identity;

namespace Ghanim.BLL.Managers
{
    public class PaymentTransactionManager : Repository<PaymentTransaction>, IPaymentTransactionManager
    {
        public IOrderActionManager actionManager;
        public IOrderTypeManager orderTypeManager;

        private readonly IAPIHelper _helper;
        private UserManager<ApplicationUser> _identityUserManager;

        public PaymentTransactionManager(ApplicationDbContext context, IAPIHelper helper)
           : base(context)
        {
            _helper = helper;
        }

       


    }
}