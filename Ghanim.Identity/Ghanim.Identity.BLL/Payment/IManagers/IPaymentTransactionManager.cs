﻿using CommonEnum;
using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing.Charts;
using Ghanim.Models.Payment.Entities;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IPaymentTransactionManager : IRepository<PaymentTransaction>
    {
      
    }
}

