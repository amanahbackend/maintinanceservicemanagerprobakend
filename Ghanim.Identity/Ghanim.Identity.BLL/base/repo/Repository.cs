﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System.Linq.Expressions;
using System.Reflection;
using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilites.PaginatedItems;

namespace Ghanim.BLL.Managers
{
    public class Repository<T> : IDisposable, IRepository<T> where T : class, IBaseEntity
    {
        private ApplicationDbContext _context;
        private bool _disposed;
        public ApplicationDbContext Context
        {
            get { return _context; }
        }
        private DbSet<T> _set;
        private IQueryable _query;
        public Repository(ApplicationDbContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }

        protected void Include(ref IQueryable<T> query, params Expression<Func<T, object>>[] include)
        {
            foreach (Expression<Func<T, object>> navigationProperty in include)
                query = query.Include<T, object>(navigationProperty);
        }


        public virtual ProcessResult<IQueryable<T>> GetAllQuerable()
        {
            IQueryable<T> data = null;
            try
            {
                data = _set.Where(x => x.IsDeleted == false);
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<IQueryable<T>> GetAllQuerableWithBlocked()
        {
            IQueryable<T> data = null;
            try
            {
                data = _set;
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public ProcessResult<int> Count()
        {
            try
            {
                var data = GetAllQuerable().Data.Count();
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(0, ex);
            }
        }
        public ProcessResult<int> CountWithBlocked()
        {
            try
            {
                var data = GetAllQuerableWithBlocked().Data.Count();
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(0, ex);
            }
        }

        public virtual async Task< ProcessResult<List<T>>> GetAllAsync(Expression<Func<T, bool>> predicate,
          params Expression<Func<T, object>>[] include)
        {
            IQueryable<T> query = null;
            try
            {
                query = _set.Where(x => x.IsDeleted == false).Where(predicate);
                Include(ref query, include);
                return ProcessResultHelper.Succedded(await  query.ToListAsync());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(await  query.ToListAsync(), ex);
            }
        }
        public virtual ProcessResult<List<T>> GetAll(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] include)
        {
            IQueryable<T> query = null;
            try
            {
                query = _set.Where(x => x.IsDeleted == false).Where(predicate);
                Include(ref query, include);
                return ProcessResultHelper.Succedded(query.ToList());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(query.ToList(), ex);
            }
        }
      
        public virtual ProcessResult<List<T>> GetAll(params Expression<Func<T, object>>[] include)
        {
            IQueryable<T> query = null;
            try
            {
                query = _set.Where(x => x.IsDeleted == false);
                Include(ref query, include);
                return ProcessResultHelper.Succedded(query.ToList());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(query.ToList(), ex);
            }
        }
        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginated(PaginatedItems<T> paginatedItems, params Expression<Func<T, object>>[] include)
        {
            var query = _set.Where(x => x.IsDeleted == false);
            Include(ref query, include);

            try
            {
                if (paginatedItems == null)
                {
                    paginatedItems = new PaginatedItems<T>();
                }
                var count = query.Count();
                paginatedItems.Count = count;
                var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
                if (paginatedItems.PageNo >= 0)
                {
                    if (count > paginatedItems.PageSize)
                    {
                        if (takeCount > count)
                        {
                            if (skipCount < count)
                            {
                                paginatedItems.Data = query.Skip(skipCount).Take(count - skipCount).ToList();
                            }
                            //else
                            //{
                            //    data = null;
                            //}
                        }
                        else
                        {
                            paginatedItems.Data = query.Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                        }
                    }
                    else
                    {
                        paginatedItems.Data = query.ToList();
                    }
                }


                return ProcessResultHelper.Succedded(paginatedItems);
                //var data= new PaginatedItemsViewModel<WorkOrderReportResult>(pageIndex, paginatedItems.PageSize, count, orders);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }

        ///old and not used if so add include to it
        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginated<TKey>(PaginatedItems<T> paginatedItems, Func<T, TKey> orderDescExpr)
        {
            try
            {
                if (paginatedItems == null)
                {
                    paginatedItems = new PaginatedItems<T>();
                }
                var count = _set.Where(x => x.IsDeleted == false).Count();
                paginatedItems.Count = count;
                var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
                if (paginatedItems.PageNo >= 0)
                {
                    if (count > paginatedItems.PageSize)
                    {
                        if (takeCount > count)
                        {
                            if (skipCount < count)
                            {
                                paginatedItems.Data = _set.Where(x => x.IsDeleted == false).OrderByDescending(orderDescExpr).Skip(skipCount).Take(count - skipCount).ToList();
                            }
                            //else
                            //{
                            //    data = null;
                            //}
                        }
                        else
                        {
                            paginatedItems.Data = _set.Where(x => x.IsDeleted == false).OrderByDescending(orderDescExpr).Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                        }
                    }
                    else
                    {
                        paginatedItems.Data = _set.Where(x => x.IsDeleted == false).OrderByDescending(orderDescExpr).ToList();
                    }
                }


                return ProcessResultHelper.Succedded(paginatedItems);
                //var data= new PaginatedItemsViewModel<WorkOrderReportResult>(pageIndex, paginatedItems.PageSize, count, orders);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }
        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginated(PaginatedItems<T> paginatedItems, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include)
        {
            try
            {
                ProcessResult<PaginatedItems<T>> result = null;
                if (predicate == null)
                {
                    result = this.GetAllPaginated(paginatedItems, include);
                }
                else
                {
                    if (paginatedItems == null)
                    {
                        paginatedItems = new PaginatedItems<T>();
                    }
                    if (paginatedItems.PageNo == 0)
                        paginatedItems.PageNo = 1;
                    if (paginatedItems.PageSize == 0)
                        paginatedItems.PageSize = 10;
                    var query = _set.Where(x => x.IsDeleted == false).Where(predicate);
                    int count;
                    paginatedItems.Count = count = query.Count();
                    var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                    var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;

                    Include(ref query, include);

                    if (paginatedItems.PageNo >= 0)
                    {
                        if (count > paginatedItems.PageSize)
                        {
                            if (takeCount > count)
                            {
                                if (skipCount < count)
                                {
                                    paginatedItems.Data = query.Skip(skipCount).Take(count - skipCount).ToList();
                                }
                                //else
                                //{
                                //    data = null;
                                //}
                            }
                            else
                            {
                                paginatedItems.Data = query.Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                            }
                        }
                        else
                        {
                            if (skipCount >= count)
                            {
                                skipCount = count;
                                paginatedItems.Data = query.Skip(skipCount).Take(paginatedItems.PageSize).ToList();

                                //return empty -set
                                //paginatedItems.Data = _set.Where(x => x.IsDeleted == false).Skip(_set.Count()).ToList();
                            }
                            else
                            {
                                paginatedItems.Data = query.Skip(skipCount).Take(count - skipCount).ToList();
                            }
                        }
                    }
                    result = ProcessResultHelper.Succedded(paginatedItems);

                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }
        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginated<TKey>(PaginatedItems<T> paginatedItems, Expression<Func<T, bool>> predicate, Func<T, TKey> orderDescExpr, params Expression<Func<T, object>>[] include)
        {
            try
            {
                ProcessResult<PaginatedItems<T>> result = null;
                if (predicate == null)
                {
                    result = this.GetAllPaginated(paginatedItems, include);
                }
                else
                {
                    var query = _set.Where(x => x.IsDeleted == false).Where(predicate);


                    if (paginatedItems == null)
                    {
                        paginatedItems = new PaginatedItems<T>();
                    }
                    if (paginatedItems.PageNo == 0)
                        paginatedItems.PageNo = 1;
                    if (paginatedItems.PageSize == 0)
                        paginatedItems.PageSize = 10;
                    int count;
                    paginatedItems.Count = count = query.Count();
                    var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                    var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;


                    Include(ref query, include);
                    if (paginatedItems.PageNo >= 0)
                    {
                        if (count > paginatedItems.PageSize)
                        {
                            if (takeCount > count)
                            {
                                if (skipCount < count)
                                {
                                    paginatedItems.Data = query.Skip(skipCount).Take(count - skipCount).ToList();
                                }
                                //else
                                //{
                                //    data = null;
                                //}
                            }
                            else
                            {
                                paginatedItems.Data = query.Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                            }
                        }
                        else
                        {
                            if (skipCount >= count)
                            {
                                skipCount = count;
                                paginatedItems.Data = query.Skip(skipCount).Take(paginatedItems.PageSize).ToList();

                                ////return empty -set
                                //paginatedItems.Data = _set.Where(x => x.IsDeleted == false).Skip(_set.Count()).OrderByDescending(orderDescExpr).ToList();
                            }
                            else
                            {
                                paginatedItems.Data = query.Skip(skipCount).Take(count - skipCount).ToList();
                            }
                        }
                    }
                    result = ProcessResultHelper.Succedded(paginatedItems);

                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }
        public virtual ProcessResult<List<T>> GetAllWithoutBlocked(params Expression<Func<T, object>>[] include)
        {
            IQueryable<T> query = null;

            try
            {
                query = _set.Where(x => x.IsDeleted == false);
                Include(ref query, include);

                return ProcessResultHelper.Succedded(query.ToList());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(query.ToList(), ex);
            }
        }
        public virtual ProcessResult<List<T>> GetAllWithBlocked(params Expression<Func<T, object>>[] include)
        {
            IQueryable<T> query = null;

            try
            {
                query = _set;
                Include(ref query, include);

                return ProcessResultHelper.Succedded(query.ToList());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(query.ToList(), ex);
            }
        }
        public virtual ProcessResult<List<T>> GetAllWithBlocked(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include)
        {
            IQueryable<T> query = null;
            try
            {
                query = _set.Where(predicate);
                Include(ref query, include);
                return ProcessResultHelper.Succedded(query.ToList());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(query.ToList(), ex);
            }
        }
        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginatedWithBlocked(PaginatedItems<T> paginatedItems, params Expression<Func<T, object>>[] include)
        {
            try
            {
                if (paginatedItems == null)
                {
                    paginatedItems = new PaginatedItems<T>();
                }

                IQueryable<T> query = _set;
                int count;
                paginatedItems.Count = count = query.Count();
                var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
                if (count >= 0)
                {
                    if (count > paginatedItems.PageSize)
                    {
                        if (takeCount > count)
                        {
                            if (skipCount < count)
                            {
                                query = query.Skip(skipCount).Take(count - skipCount).AsQueryable();
                            }
                            //else
                            //{
                            //    data = null;
                            //}
                        }
                        else
                        {
                            query = query.Skip(skipCount).Take(paginatedItems.PageSize).AsQueryable();
                        }
                    }
                }

                Include(ref query, include);
                paginatedItems.Data = query.ToList();
                return ProcessResultHelper.Succedded(paginatedItems);
                //var data= new PaginatedItemsViewModel<WorkOrderReportResult>(pageIndex, paginatedItems.PageSize, count, orders);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }

        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginatedWithBlocked<TKey>(PaginatedItems<T> paginatedItems, Func<T, TKey> orderDescExpr, params Expression<Func<T, object>>[] include)
        {
            try
            {
                if (paginatedItems == null)
                {
                    paginatedItems = new PaginatedItems<T>();
                }
                IQueryable<T> query = _set;
                int count;
                paginatedItems.Count = count = query.Count();
                var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;

                query = query.OrderByDescending(orderDescExpr).AsQueryable();


                if (paginatedItems.PageNo >= 0)
                {
                    if (count > paginatedItems.PageSize)
                    {
                        if (takeCount > count)
                        {
                            if (skipCount < count)
                            {
                                query = query.Skip(skipCount).Take(count - skipCount);
                            }
                            //else
                            //{
                            //    data = null;
                            //}
                        }
                        else
                        {
                            query = query.Skip(skipCount).Take(paginatedItems.PageSize);
                        }
                    }
                    Include(ref query, include);
                    paginatedItems.Data = query.ToList();
                }
                return ProcessResultHelper.Succedded(paginatedItems);
                //var data= new PaginatedItemsViewModel<WorkOrderReportResult>(pageIndex, paginatedItems.PageSize, count, orders);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }
        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginatedWithBlocked(PaginatedItems<T> paginatedItems, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include)
        {
            try
            {
                ProcessResult<PaginatedItems<T>> result = null;
                if (predicate == null)
                {
                    result = this.GetAllPaginatedWithBlocked(paginatedItems, include);
                }
                else
                {
                    if (paginatedItems == null)
                    {
                        paginatedItems = new PaginatedItems<T>();
                    }
                    if (paginatedItems.PageNo == 0)
                        paginatedItems.PageNo = 1;
                    if (paginatedItems.PageSize == 0)
                        paginatedItems.PageSize = 10;

                    var query = _set.Where(predicate);
                    Include(ref query, include);

                    int count;
                    paginatedItems.Count = count = query.Count();
                    var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                    var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;
                    if (paginatedItems.PageNo >= 0)
                    {
                        if (count > paginatedItems.PageSize)
                        {
                            if (takeCount > count)
                            {
                                if (skipCount < count)
                                {
                                    paginatedItems.Data = query.Skip(skipCount).Take(count - skipCount).ToList();
                                }
                                //else
                                //{
                                //    data = null;
                                //}
                            }
                            else
                            {
                                paginatedItems.Data = query.Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                            }
                        }
                        else
                        {
                            if (skipCount >= count)
                            {
                                skipCount = count;
                                paginatedItems.Data = query.Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                            }
                            else
                            {
                                paginatedItems.Data = query.Skip(skipCount).Take(count - skipCount).ToList();
                            }
                        }
                    }
                    result = ProcessResultHelper.Succedded(paginatedItems);

                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }
        public virtual ProcessResult<PaginatedItems<T>> GetAllPaginatedWithBlocked<TKey>(PaginatedItems<T> paginatedItems, Expression<Func<T, bool>> predicate, Func<T, TKey> orderDescExpr, params Expression<Func<T, object>>[] include)
        {
            try
            {
                ProcessResult<PaginatedItems<T>> result = null;
                if (predicate == null)
                {
                    result = this.GetAllPaginatedWithBlocked(paginatedItems, include);
                }
                else
                {
                    if (paginatedItems == null)
                    {
                        paginatedItems = new PaginatedItems<T>();
                    }
                    if (paginatedItems.PageNo == 0)
                        paginatedItems.PageNo = 1;
                    if (paginatedItems.PageSize == 0)
                        paginatedItems.PageSize = 10;
                    var query = _set.Where(predicate);
                    int count;
                    paginatedItems.Count = count = query.Count();
                    var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                    var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;

                    Include(ref query, include);


                    if (paginatedItems.PageNo >= 0)
                    {
                        if (count > paginatedItems.PageSize)
                        {
                            if (takeCount > count)
                            {
                                if (skipCount < count)
                                {
                                    paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(count - skipCount).ToList();
                                }
                                //else
                                //{
                                //    data = null;
                                //}
                            }
                            else
                            {
                                paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(paginatedItems.PageSize).ToList();
                            }
                        }
                        else
                        {
                            if (skipCount >= count)
                            {
                                skipCount = count;
                                paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(paginatedItems.PageSize).ToList();

                                ////return empty -set
                                //paginatedItems.Data = _set.Skip(_set.Count()).OrderByDescending(orderDescExpr).ToList();
                            }
                            else
                            {
                                paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(count - skipCount).ToList();
                            }
                        }
                    }
                    result = ProcessResultHelper.Succedded(paginatedItems);

                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }

        //TODO refactor this method add include to this method ===================
        public virtual ProcessResult<T> Get(params object[] id)
        {
            T data = null;
            try
            {
                data = _set.Find(id);
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }


        public ProcessResult<T> Get(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include)
        {
            var query = _set.AsQueryable();
            Include(ref query, include);
            T data = null;
            try
            {
                data = query.FirstOrDefault(predicate);
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<T> Add(T entity)
        {
            T data = null;
            try
            {
                if (Validator.IsValid(entity))
                {
                    entity.IsDeleted = false;
                    entity = _set.Add(entity).Entity;
                    if (SaveChanges() > 0)
                    {
                        data = entity;
                    }
                    return ProcessResultHelper.Succedded(entity);
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    var ex = new Exception(exceptionMsgs.ToString());
                    return ProcessResultHelper.Failed(data, ex);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<List<T>> Add(List<T> entityLst)
        {
            List<T> data = null;
            try
            {
                for (int i = 0; i < entityLst.Count; i++)
                {
                    var addresult = Add(entityLst[i]);
                    if (addresult.IsSucceeded)
                    {
                        entityLst[i] = addresult.Data;
                    }
                }
                data = entityLst;
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> Update(T entity)
        {
            bool data = false;
            try
            {
                if (Validator.IsValid(entity))
                {
                    DetachLocal(entity);
                    _context.Entry<T>(entity).State = EntityState.Modified;
                    if (SaveChanges() > 0)
                    {
                        data = true;
                    }
                    return ProcessResultHelper.Succedded(data);
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    var ex = new Exception(exceptionMsgs.ToString());
                    return ProcessResultHelper.Failed(data, ex);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> Update(List<T> entityLst)
        {
            bool data = false;
            try
            {
                foreach (var entity in entityLst)
                {
                    if (!Validator.IsValid(entity))
                    {
                        StringBuilder exceptionMsgs = new StringBuilder();
                        List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                        foreach (var errmsg in errorMsgs)
                        {
                            exceptionMsgs.Append(errmsg);
                            exceptionMsgs.Append("/n");
                        }
                        var ex = new Exception(exceptionMsgs.ToString());
                        return ProcessResultHelper.Failed(data, ex);
                    }
                }
                if (SaveChanges() > 0)
                {
                    data = true;
                }
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }


        public virtual ProcessResult<bool> Delete(T entity)
        {
            bool data = false;
            try
            {
                _context.Entry<T>(entity).State = EntityState.Deleted;
                data = _context.SaveChanges() > 0;
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> LogicalDelete(T entity)
        {
            bool data = false;
            try
            {
                entity.IsDeleted = !entity.IsDeleted;
                data = _context.SaveChanges() > 0;
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }

        public virtual ProcessResult<bool> DeleteById(params object[] id)
        {
            T entity = _set.Find(id);
            return Delete(entity);
        }

        public virtual ProcessResult<bool> LogicalDeleteById(params object[] id)
        {
            T entity = _set.Find(id);
            return LogicalDelete(entity);
        }

        public virtual ProcessResult<bool> Delete(List<T> entitylst)
        {
            bool data = false;
            try
            {
                if (entitylst != null && entitylst.Count > 0)
                {
                    foreach (var entity in entitylst)
                    {
                        var deleteResult = Delete(entity);
                        if (!deleteResult.IsSucceeded)
                        {
                            return deleteResult;
                        }
                    }
                    data = true;
                }
                else
                {
                    data = true;
                }
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> LogicalDelete(List<T> entitylst)
        {
            bool data = false;
            try
            {
                if (entitylst != null && entitylst.Count > 0)
                {
                    foreach (var entity in entitylst)
                    {
                        var deleteResult = LogicalDelete(entity);
                        if (!deleteResult.IsSucceeded)
                        {
                            return deleteResult;
                        }
                    }
                    data = true;
                }
                else
                {
                    data = true;
                }
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual int SaveChanges()
        {
            // this method handle any exception so no need to put it in try
            BaseEntityManager.AddAuditingData(_context.ChangeTracker.Entries());
            return _context.SaveChanges();
        }
        public virtual ProcessResult<bool> IsChanged(T entity, string propName)
        {
            bool data = false;
            var original = Context.Entry(entity).GetDatabaseValues()[propName];
            var current = Context.Entry(entity).CurrentValues[propName];
            if (original.Equals(current))
            {
                data = false;
            }
            else
            {
                data = true;
            }
            return ProcessResultHelper.Succedded(data);

        }
        public T Get(string property, object value)
        {
            var lambda = CreateEqualSingleExpression(property, value);

            return _set.SingleOrDefault(lambda);
        }
        public virtual ProcessResult<List<T>> GetByIds(List<int> ids, params Expression<Func<T, object>>[] include)
        {

            var query = _set.Where(x => x.IsDeleted == false && ids.Contains(x.Id));
            Include(ref query, include);

            try
            {
                return ProcessResultHelper.Succedded(query.ToList());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed((List<T>)null, ex);
            }
        }
        public virtual ProcessResult<List<T>> GetByIdsWithBlocked(List<int> ids, params Expression<Func<T, object>>[] include)
        {
            var query = _set.Where(x => ids.Contains(x.Id));
            Include(ref query, include);
            try
            {
                return ProcessResultHelper.Succedded(query.ToList());
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed((List<T>)null, ex);
            }
        }
        public Expression<Func<T, bool>> CreateEqualSingleExpression(string property, object value)
        {

            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            var propertyExpression = Expression.Property(p, property);

            //p.Property == value
            var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(value));

            //p => p.Property == value
            return Expression.Lambda<Func<T, bool>>(equalsExpression, p);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        public void DetachLocal(T entity)
        {
            var local = _set.Local.FirstOrDefault(entry => entry.Id.Equals(entity.Id));
            if (local != null)
            {
                _context.Entry(local).State = EntityState.Detached;
            }
        }


        public bool Any(Expression<Func<T, bool>> predicate)
        {
           return _set.Any(predicate);
        }
        

        #region asyncpagination
        public virtual async Task< ProcessResult<PaginatedItems<T>>> GetAllPaginatedAsync<TKey>(PaginatedItems<T> paginatedItems, Expression<Func<T, bool>> predicate, Func<T, TKey> orderDescExpr, params Expression<Func<T, object>>[] include)
        {
            try
            {
                ProcessResult<PaginatedItems<T>> result = null;
                if (predicate == null)
                {
                    result = this.GetAllPaginated(paginatedItems, include);
                }
                else
                {
                    var query = _set.Where(x => x.IsDeleted == false).Where(predicate);

                  
                    if (paginatedItems == null)
                    {
                        paginatedItems = new PaginatedItems<T>();
                    }
                    if (paginatedItems.PageNo == 0)
                        paginatedItems.PageNo = 1;
                    if (paginatedItems.PageSize == 0)
                        paginatedItems.PageSize = 10;
                    int count;
                    paginatedItems.Count = count = query.Count();
                    var skipCount = (paginatedItems.PageNo - 1) * paginatedItems.PageSize;
                    var takeCount = paginatedItems.PageNo * paginatedItems.PageSize;


                    Include(ref query, include);
                    if (paginatedItems.PageNo >= 0)
                    {
                        if (count > paginatedItems.PageSize)
                        {
                            if (takeCount > count)
                            {
                                if (skipCount < count)
                                {
                                    if(orderDescExpr != null)
                                    {
                                        paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(count - skipCount).ToList();

                                    }
                                    else
                                    {
                                        paginatedItems.Data = await query.Skip(skipCount).Take(count - skipCount).ToListAsync();

                                    }
                                    
                                }
                                //else
                                //{
                                //    data = null;
                                //}
                            }
                            else
                            {
                                if(orderDescExpr != null)
                                {
                                    paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(paginatedItems.PageSize).ToList();

                                }
                                else
                                {
                                    paginatedItems.Data = await query.Skip(skipCount).Take(paginatedItems.PageSize).ToListAsync();

                                }
                                
                            }
                        }
                        else
                        {
                            if (skipCount >= count)
                            {
                                skipCount = count;
                                if(orderDescExpr != null)
                                {
                                    paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(paginatedItems.PageSize).ToList();

                                }
                                else
                                {
                                    paginatedItems.Data = await query.Skip(skipCount).Take(paginatedItems.PageSize).ToListAsync();
                                }
                               

                                ////return empty -set
                                //paginatedItems.Data = _set.Where(x => x.IsDeleted == false).Skip(_set.Count()).OrderByDescending(orderDescExpr).ToList();
                            }
                            else
                            {
                                if (orderDescExpr != null)
                                {
                                    paginatedItems.Data = query.OrderByDescending(orderDescExpr).Skip(skipCount).Take(count - skipCount).ToList();
                                }
                                else
                                {
                                    paginatedItems.Data = await query.Skip(skipCount).Take(count - skipCount).ToListAsync();

                                }
                                    
                            }
                        }
                    }
                    result = ProcessResultHelper.Succedded(paginatedItems);

                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(paginatedItems, ex);
            }
        }

        #endregion


    }


}
