﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IRepository<TEntity> where TEntity : class, IBaseEntity
    {


        ProcessResult<IQueryable<TEntity>> GetAllQuerable();
        ProcessResult<IQueryable<TEntity>> GetAllQuerableWithBlocked();
        ProcessResult<List<TEntity>> GetAll(params Expression<Func<TEntity, object>>[] include);
        ProcessResult<List<TEntity>> GetAll(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] include);
       Task< ProcessResult<List<TEntity>>> GetAllAsync(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] include);
        ProcessResult<List<TEntity>> GetAllWithBlocked(params Expression<Func<TEntity, object>>[] include);
        ProcessResult<List<TEntity>> GetAllWithoutBlocked(params Expression<Func<TEntity, object>>[] include);        
        ProcessResult<List<TEntity>> GetAllWithBlocked(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked(PaginatedItems<TEntity> paginatedItems, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked<TKey>(PaginatedItems<TEntity> paginatedItems, Func<TEntity, TKey> orderDescExpr, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked<TKey>(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> orderDescExpr, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<TEntity> Get(params object[] id);
        ProcessResult<int> Count();
        ProcessResult<int> CountWithBlocked();
        ProcessResult<List<TEntity>> GetByIds(List<int> ids, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<List<TEntity>> GetByIdsWithBlocked(List<int> ids, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<TEntity> Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<TEntity> Add(TEntity entity);
        ProcessResult<List<TEntity>> Add(List<TEntity> entityLst);
        ProcessResult<bool> Update(TEntity entity);
        ProcessResult<bool> Update(List<TEntity> entityLst);
        ProcessResult<bool> Delete(TEntity entity);
        ProcessResult<bool> LogicalDelete(TEntity entity);
        ProcessResult<bool> Delete(List<TEntity> entitylst);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated(PaginatedItems<TEntity> paginatedItems, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated<TKey>(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> orderDescExpr, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);
       Task< ProcessResult<PaginatedItems<TEntity>>> GetAllPaginatedAsync<TKey>(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> orderDescExpr, params Expression<Func<TEntity, object>>[] include);
        ProcessResult<bool> DeleteById(params object[] id);
        ProcessResult<bool> LogicalDeleteById(params object[] id);
        bool Any(Expression<Func<TEntity, bool>> predicate);
        int SaveChanges();
      
















    }
}
