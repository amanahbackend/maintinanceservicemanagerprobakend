﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.BLL.IManagers
{
    public interface ICashableRepository<TEntity, TViewModel> : IRepository<TEntity>
        where TEntity : class, IBaseEntity
    where TViewModel : class, IRepoistryBaseEntity
    {


    }
}
