﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System.Linq.Expressions;
using System.Reflection;
using DispatchProduct.RepositoryModule;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilites.PaginatedItems;
using ICloneable = Ghanim.BLL.Caching.Abstracts.ICloneable;

namespace Ghanim.BLL.Managers
{
    public class CashableRepository<T, VT> : Repository<T>, IDisposable, ICashableRepository<T, VT>
        where T : class, IBaseEntity
        where VT : class, IRepoistryBaseEntity
    {
        private ApplicationDbContext _context;
        private bool _disposed;
        private ICached<VT> _cashed;

        public ApplicationDbContext Context
        {
            get { return _context; }
        }
        private DbSet<T> _set;
        private IQueryable _query;
        public CashableRepository(ApplicationDbContext context, ICached<VT> cashed) : base(context)
        {
            _context = context;
            _set = _context.Set<T>();
            _cashed = cashed;
        }


        public override async Task<ProcessResult<List<T>>> GetAllAsync(Expression<Func<T, bool>> predicate,
          params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return await base.GetAllAsync(predicate, include);
        }
        public override ProcessResult<List<T>> GetAll(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetAll(predicate, include);
        }

        public override ProcessResult<List<T>> GetAll(params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetAll(include);
        }


        public override ProcessResult<List<T>> GetAllWithoutBlocked(params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetAllWithoutBlocked(include);
        }
        public override ProcessResult<List<T>> GetAllWithBlocked(params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetAllWithBlocked(include);
        }
        public override ProcessResult<List<T>> GetAllWithBlocked(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetAllWithBlocked(predicate, include);
        }
        public override ProcessResult<PaginatedItems<T>> GetAllPaginatedWithBlocked(PaginatedItems<T> paginatedItems, params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetAllPaginatedWithBlocked(paginatedItems, include);
        }


        //TODO refactor this method add include to this method =======================================================================================================================================================================================================================================================
        public override ProcessResult<T> Get(params object[] id)
        {
            T data = null;
            try
            {
                data = _set.Find(id);
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }

        public ProcessResult<T> Get(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.Get(predicate, include);
        }
        public override ProcessResult<T> Add(T entity)
        {
            var result = base.Add(entity);
            _cashed.ClearCashed();
            return result;
        }
        public override ProcessResult<List<T>> Add(List<T> entityLst)
        {
            var result = base.Add(entityLst);
            _cashed.ClearCashed();
            return result;
        }
        public override ProcessResult<bool> Update(T entity)
        {
            var result = base.Update(entity);
            _cashed.ClearCashed();
            return result;
        }
        public override ProcessResult<bool> Update(List<T> entityLst)
        {
            var result = base.Update(entityLst);
            _cashed.ClearCashed();
            return result;
        }


        public override ProcessResult<bool> Delete(T entity)
        {
            var result = LogicalDelete(entity);
            _cashed.ClearCashed();
            return result;
        }
        public override ProcessResult<bool> LogicalDelete(T entity)
        {
            var result = base.LogicalDelete(entity);
            _cashed.ClearCashed();
            return result;
        }

        public override ProcessResult<bool> DeleteById(params object[] id)
        {
            var result = base.LogicalDeleteById(id);
            _cashed.ClearCashed();
            return result;
        }

        public override ProcessResult<bool> LogicalDeleteById(params object[] id)
        {
            var result = base.LogicalDeleteById(id);
            _cashed.ClearCashed();
            return result;
        }

        public override ProcessResult<bool> Delete(List<T> entitylst)
        {
            var result = base.LogicalDelete(entitylst);
            _cashed.ClearCashed();
            return result;
        }
        public override ProcessResult<bool> LogicalDelete(List<T> entitylst)
        {
            var result = base.LogicalDelete(entitylst);
            _cashed.ClearCashed();
            return result;
        }


        public override ProcessResult<List<T>> GetByIds(List<int> ids, params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetByIds(ids, include);
        }
        public override ProcessResult<List<T>> GetByIdsWithBlocked(List<int> ids, params Expression<Func<T, object>>[] include)
        {
            if (include == null || include.Length == 0)
            {

            }
            return base.GetByIdsWithBlocked(ids, include);
        }
    }


}
