﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_GovernoratesManager : IRepository<Lang_Governorates>
    {
        ProcessResult<List<Lang_Governorates>> GetAllLanguagesByGovernorateId(int GovernoratesId);
        ProcessResult<bool> UpdateByGovernorate(List<Lang_Governorates> entities);
    }
}
