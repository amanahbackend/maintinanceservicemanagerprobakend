﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_CompanyCodeManager : IRepository<Lang_CompanyCode>
    {
        ProcessResult<List<Lang_CompanyCode>> GetAllLanguagesByCompanyCodeId(int CompanyCodeId);
        ProcessResult<bool> UpdateByCompanyCode(List<Lang_CompanyCode> entities);
    }
}
