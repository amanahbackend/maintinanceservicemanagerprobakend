﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface ILang_AreasManager : IRepository<Lang_Areas>
    {
        ProcessResult<List<Lang_Areas>> GetAllLanguagesByAreaId(int AreaId);
        ProcessResult<List<Lang_Areas>> GetAllLanguagesByAreaIdWithBlocked(int AreaId);
        ProcessResult<bool> UpdateByArea(List<Lang_Areas> entities);
    }
}
