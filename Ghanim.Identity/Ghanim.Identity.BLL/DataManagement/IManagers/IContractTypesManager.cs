﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
    public interface IContractTypesManager : IRepository<ContractTypes>
    {
        ProcessResult<ContractTypes>  CheckContractTypesNoDoublication(string name, string code, int? Id = null);
    }
}
