﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;

namespace Ghanim.BLL.IManagers
{
    public interface IMapAdminRelatedManager : ICashableRepository<MapAdminRelated,MapAdminRelatedViewModel>
    {
    }
}
