﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.BLL.IManagers
{
   public interface ILang_CostCenterManager : IRepository<Lang_CostCenter>
    {
        ProcessResult<List<Lang_CostCenter>> GetAllLanguagesByCostCenterId(int CostCenterId);
        ProcessResult<bool> UpdateByCostCenter(List<Lang_CostCenter> entities);
    }
}
