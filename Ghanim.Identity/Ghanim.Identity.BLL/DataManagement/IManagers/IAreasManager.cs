﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;
using Utilites.ProcessingResult;
namespace Ghanim.BLL.IManagers
{
    public interface IAreasManager : ICashableRepository<Areas, AreasViewModel> //: IRepository<Areas>
    {
        ProcessResult<List<Areas>> GetAreasByGovId(int govId);
        ProcessResult<Areas> GetAreasByArea_No(int AreaNo);
        ProcessResult<Areas> GetByName(string name); 
        ProcessResult<Areas> CheckAreaNoDoublication(string areaName, int? Id = null, int? areaNo = null);
    }
}
