﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class AvailabilityManager : Repository<Availability>, IAvailabilityManager
    {
        public AvailabilityManager(ApplicationDbContext context)
            : base(context)
        {
        }

        public override ProcessResult<Availability> Add(Availability model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<Availability>(model, null, message: SharedResource.General_NoVal_Body , ProcessResultStatusCode.InvalidValue);
             
            var checkNoDublication = CheckAvailabilityNoDoublication(model.Name , null);
            if (!checkNoDublication.IsSucceeded)
                return checkNoDublication;

            return base.Add(model);
        }

        public override ProcessResult<bool> Update(Availability model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckAvailabilityNoDoublication(model.Name,   model.Id);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            return base.Update(model);
        }
        public ProcessResult<Availability> CheckAvailabilityNoDoublication(string name,  int? Id = null)
        {
            try
            {
                Availability result = null;
                if (name != null   && Id != null)
                    result = Get(x =>  x.Name.ToLower() == name.ToLower()  
                                        && x.Id != Id.Value)?.Data;
                else if (name != null   && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() )?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<Availability>(result, null, message: string.Format(SharedResource.General_DuplicateWithoutCode, SharedResource.Entity_Availability), ProcessResultStatusCode.InvalidValue, "CheckAvailabilityNoDoublication");
                else
                    return ProcessResultHelper.Succedded<Availability>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckAvailabilityNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<Availability>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckAvailabilityNoDoublication");
            }
        }
    }
}
