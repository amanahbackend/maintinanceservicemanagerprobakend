﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ghanim.API;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.Extensions.Localization;

namespace Ghanim.BLL.Managers
{
    public class AreasManager : CashableRepository<Areas,AreasViewModel>, IAreasManager
    {
        IServiceProvider _serviceprovider;
        public ISupportedLanguagesManager supportedLanguagesManager;

        public AreasManager(ApplicationDbContext context, ICached<AreasViewModel> cashed)
            : base(context, cashed)
        {
            
        }

        //public AreasManager(IServiceProvider serviceprovider, LookUpDbContext context)
        //  : base(context)
        //{
        //    _serviceprovider = serviceprovider;
        //}

        //private ILang_AreasManager lang_AreasManager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ILang_AreasManager>();
        //    }
        //}
        
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        public ProcessResult<List<Areas>> GetAreasByGovId(int govId)
        {
            List<Areas> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Fk_Governorate_Id == govId).ToList();
                return ProcessResultHelper.Succedded<List<Areas>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAreasByGovId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Areas>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
            }
        }

        public override ProcessResult<List<Areas>> Add(List<Areas> entityLst)
        {


            ProcessResult<List<Areas>> result = new ProcessResult<List<Areas>>() { Data = null };
            List<Lang_Areas> Lang_Areas = new List<Lang_Areas>();
            try
            {
                var supportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.ToList();
                result = base.Add(entityLst);
                foreach (var item in result.Data)
                {
                    foreach (var supportedLanguage in supportedLanguagesRes)
                    {
                        if (supportedLanguage.Name == "English")
                        {
                            Lang_Areas.Add(new Lang_Areas { FK_Area_ID = item.Id, FK_SupportedLanguages_ID = supportedLanguage.Id, Name = item.Name });
                        }
                        else
                        {
                            Lang_Areas.Add(new Lang_Areas { FK_Area_ID = item.Id, FK_SupportedLanguages_ID = supportedLanguage.Id, Name = null });
                        }
                    }
                }

                //var lang_AreasRes = lang_AreasManager.Add(Lang_Areas);

                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Areas>>(result.Data, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
            }
        }

        public ProcessResult<Areas> GetAreasByArea_No(int AreaNo) {
            Areas input = null;
            try
            {
                input = GetAll().Data.Find(x => x.Area_No == AreaNo);
                return ProcessResultHelper.Succedded<Areas>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAreasByGovId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<Areas>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
            }
        }
   
        public ProcessResult<Areas> GetByName(string name)
        {
            var entityResult = Get(x => x.Name == name);
            return entityResult;
        }


        public override ProcessResult<Areas> Add(Areas model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<Areas>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var checkNoDubArea = CheckAreaNoDoublication(model.Name,null, model.Area_No);
            if (!checkNoDubArea.IsSucceeded) 
                return checkNoDubArea;

            return base.Add(model);
        }


        public override ProcessResult<bool> Update(Areas model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);
            var checkNoDublication = CheckAreaNoDoublication(model.Name,  model.Id, model.Area_No);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);        
            return base.Update(model);
        }

        public override ProcessResult<bool> LogicalDelete(Areas model)
        {
            if (!CheckCanBlock(model.Id))
                return ProcessResultHelper.Failed<bool>
                    (false, null, message: string.Format(SharedResource.General_Block, SharedResource.Entity_Area, SharedResource.General_HasDispatchingSetting), ProcessResultStatusCode.InvalidValue);

            return base.LogicalDelete(model);
        }

        /// <summary>
        /// you must add atleast two param from this three 
        /// </summary>
        /// <param name="areaName"></param>
        /// <param name="Id"></param>
        /// <param name="areaNo"></param>
        /// <returns></returns>

        public ProcessResult<Areas> CheckAreaNoDoublication(string areaName ,int? Id=null ,int? areaNo=null)
        {
            try
            {
                Areas result = null;
                if (areaNo == null && Id != null)
                    result = Get(x =>   x.Name.ToLower() == areaName.ToLower()  
                                        && x.Id != Id.Value)?.Data;
                else if(areaNo != null && Id == null)  
                            result = Get(x =>  x.Name.ToLower() == areaName.ToLower() ||
                                 x.Area_No == areaNo  )?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<Areas>(result, null, message: string.Format(SharedResource.General_Duplicate, SharedResource.Entity_Area) , ProcessResultStatusCode.InvalidValue, "CheckAreaDoublication");
                else
                    return ProcessResultHelper.Succedded<Areas>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckAreaDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<Areas>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckAreaDoublication");
            }
        }

        public bool CheckCanBlock(int Id)
        {
            bool valid = true;
            var obj = base.Get(Id);
            if (obj.Data.DispatcherSettings != null && obj.Data.DispatcherSettings.Count > 0)
            {
                valid = false;
                return valid;
            }

           
            return valid;
        }



    }
}
