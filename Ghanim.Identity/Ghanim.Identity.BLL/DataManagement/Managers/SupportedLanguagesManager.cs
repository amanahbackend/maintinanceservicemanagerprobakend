﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class SupportedLanguagesManager : Repository<SupportedLanguages>, ISupportedLanguagesManager
    {
         public SupportedLanguagesManager(ApplicationDbContext context )
            : base(context)
        {
           
        }

       public  ProcessResult<SupportedLanguages> GetLanguageByName(string name)
        {
            SupportedLanguages input = null;
            try
            {
                input = GetAll().Data.Find(x => x.Name == name);
                return ProcessResultHelper.Succedded<SupportedLanguages>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAreasByGovId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<SupportedLanguages>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAreasByGovId");
            }
        }


        public override ProcessResult<SupportedLanguages> Add(SupportedLanguages model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<SupportedLanguages>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var checkNoDubArea = CheckSupportedLanguagesNoDoublication(model.Name, null , model.Code);
            if (!checkNoDubArea.IsSucceeded)
                return checkNoDubArea;

            return base.Add(model);
        }


        public override ProcessResult<bool> Update(SupportedLanguages model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);
            if (!CheckCanBlockOrUpdate(model.Id))
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.Language_Cant_Update, ProcessResultStatusCode.InvalidValue);
            var checkNoDublication = CheckSupportedLanguagesNoDoublication(model.Name, model.Id ,model.Code);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            return base.Update(model);
        }





        public ProcessResult<SupportedLanguages> CheckSupportedLanguagesNoDoublication(string name, int? Id = null, string code = null)
        {
            try
            {
                SupportedLanguages result = null;
                if ( string.IsNullOrEmpty(code) && Id != null)
                    result = Get(x => x.Name.ToLower() == name.ToLower()
                                        && x.Id != Id.Value)?.Data;
                else if (!string.IsNullOrEmpty( code ) && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() ||
                         x.Code == code)?.Data;
                else if (!string.IsNullOrEmpty(code) && Id != null)
                    result = Get(x => (x.Name.ToLower() == name.ToLower() ||
                         x.Code == code )&& x.Id != Id.Value)?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<SupportedLanguages>(result, null, message: string.Format(SharedResource.General_Duplicate, SharedResource.Entity_Languages), ProcessResultStatusCode.InvalidValue, "CheckSupportedLanguagesNoDoublication");
                else
                    return ProcessResultHelper.Succedded<SupportedLanguages>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckSupportedLanguagesNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<SupportedLanguages>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckSupportedLanguagesNoDoublication");
            }
        }


        public override ProcessResult<bool> LogicalDelete(SupportedLanguages model)
        {
            if (!CheckCanBlockOrUpdate(model.Id))
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.Language_Cant_Block, ProcessResultStatusCode.InvalidValue);

            return base.LogicalDelete(model);
        }


        public bool CheckCanBlockOrUpdate(int Id)
        { 
            if (Id == 2)
                return false;
            else
                return true; 
        }

    }
}
