﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class ShiftManager : Repository<Shift> , IShiftManager
    {
        public ShiftManager(ApplicationDbContext context)
            : base(context)
        {
        }

        public override ProcessResult<bool> LogicalDelete(Shift model)
        {
            if (!CheckCanBlock(model.Id))
                return ProcessResultHelper.Failed<bool>
                    (false, null, message: string.Format(SharedResource.General_Block, SharedResource.Entity_Shifts, SharedResource.General_CurrentUsedInTeams), ProcessResultStatusCode.InvalidValue);
            return base.LogicalDelete(model);
        }
        public bool CheckCanBlock(int Id)
        {
            bool valid = true;
            var obj = base.Get(Id);
            if (obj.Data.Teams != null && obj.Data.Teams.Count >0)
            {
                valid = false;
            }
            return valid;
        }

 
    }
}
