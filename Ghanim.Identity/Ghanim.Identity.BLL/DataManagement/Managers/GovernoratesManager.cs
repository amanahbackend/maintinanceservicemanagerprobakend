﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{
    public class GovernoratesManager : Repository<Governorates>, IGovernoratesManager
    {
        public GovernoratesManager(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}
