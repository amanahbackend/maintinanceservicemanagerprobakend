﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.BLL.Managers
{
    public class Lang_ContractTypesManager : Repository<Lang_ContractTypes>, ILang_ContractTypesManager
    {
        IServiceProvider _serviceprovider;
        public Lang_ContractTypesManager(IServiceProvider serviceprovider, ApplicationDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IContractTypesManager contractTypesmanager
        {
            get
            {
                return _serviceprovider.GetService<IContractTypesManager>();
            }
        }

        public ProcessResult<List<Lang_ContractTypes>> GetAllLanguagesByContractTypesId(int ContractTypesId)
        {
            List<Lang_ContractTypes> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_ContractTypes_ID == ContractTypesId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_ContractTypes { FK_ContractTypes_ID = ContractTypesId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_ContractTypes>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByContractTypesId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_ContractTypes>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByContractTypesId");
            }
        }

        public ProcessResult<bool> UpdateByContractTypes(List<Lang_ContractTypes> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var contractTypeRes = contractTypesmanager.Get(item.FK_ContractTypes_ID);
                        if (contractTypeRes != null && contractTypeRes.Data != null)
                        {
                            contractTypeRes.Data.Name = item.Name;
                            var updateRes = contractTypesmanager.Update(contractTypeRes.Data);
                        }
                    }
                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangContractTypes = GetByContractTypesIdAndsupprotedLangId(item.FK_ContractTypes_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangContractTypes != null && prevLangContractTypes.Data != null && prevLangContractTypes.Data.Count == 0)
                        {
                            result = Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByContractTypesId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByContractTypesId");
            }
        }

        private ProcessResult<List<Lang_ContractTypes>> GetByContractTypesIdAndsupprotedLangId(int ContractTypesId, int supprotedLangId)
        {
            List<Lang_ContractTypes> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_ContractTypes_ID == ContractTypesId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_ContractTypes>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByContractTypesIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_ContractTypes>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByContractTypesIdAndsupprotedLangId");
            }
        }
    }
}
