﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class ContractTypesManager : Repository<ContractTypes>, IContractTypesManager
    {
        public ContractTypesManager(ApplicationDbContext context)
            : base(context)
        {

        }

        public override ProcessResult<ContractTypes> Add(ContractTypes model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<ContractTypes>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var checkNoDublication = CheckContractTypesNoDoublication(model.Name,model.Code, null);
            if (!checkNoDublication.IsSucceeded)
                return checkNoDublication;

            return base.Add(model);
        }

        public override ProcessResult<bool> Update(ContractTypes model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckContractTypesNoDoublication(model.Name,model.Code, model.Id);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            return base.Update(model);
        }



        

        public ProcessResult<ContractTypes> CheckContractTypesNoDoublication(string name, string code, int? Id = null)
        {
            try
            {  

                    ContractTypes result = null;
                if (name != null && code != null && Id != null)
                    result = Get(x => (x.Name.ToLower() == name.ToLower() || x.Code == code)  && x.Id != Id.Value)?.Data;

                else if (name != null && code != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() || x.Code == code)?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<ContractTypes>(result, null, message: string.Format(SharedResource.General_Duplicate, SharedResource.Entity_ContractTypes)  , ProcessResultStatusCode.InvalidValue, "CheckContractTypesNoDoublication");
                else
                    return ProcessResultHelper.Succedded<ContractTypes>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckContractTypesNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<ContractTypes>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckContractTypesNoDoublication");
            }
        }
    }
}
