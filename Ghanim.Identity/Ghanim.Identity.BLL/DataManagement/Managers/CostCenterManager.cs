﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class CostCenterManager : Repository<CostCenter>, ICostCenterManager
    {
         
        public CostCenterManager(ApplicationDbContext context )
            : base(context)
        {
            
        }
        public  ProcessResult<bool> costCenterIsExit(string name)
        {
            List<CostCenter> input = null;
            bool result = false;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.Name == name).ToList();
                if (input!=null&&input.Count()>0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "costCenterIsExit");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(result, ex, (string)null, ProcessResultStatusCode.Failed, "costCenterIsExit");
            }
        }

        public override ProcessResult<CostCenter> Add(CostCenter model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<CostCenter>(model, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var checkNoDublication = CheckCostCenterNoDoublication(model.Name, null);
            if (!checkNoDublication.IsSucceeded)
                return checkNoDublication;

            return base.Add(model);
        }

        public override ProcessResult<bool> Update(CostCenter model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckCostCenterNoDoublication(model.Name, model.Id);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            return base.Update(model);
        }
        public ProcessResult<CostCenter> CheckCostCenterNoDoublication(string name, int? Id = null)
        {
            try
            {
                CostCenter result = null;
                if (name != null && Id != null)
                    result = Get(x => x.Name.ToLower() == name.ToLower()
                                        && x.Id != Id.Value)?.Data;
                else if (name != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower())?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<CostCenter>(result, null, message: string.Format(SharedResource.General_Duplicate, SharedResource.Entity_CostCenter), ProcessResultStatusCode.InvalidValue, "CheckCostCenterNoDoublication");
                else
                    return ProcessResultHelper.Succedded<CostCenter>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckCostCenterNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<CostCenter>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckContractTypesNoDoublication");
            }
        }
        public override ProcessResult<bool> LogicalDelete(CostCenter model)
        {
            if (!CheckCanBlock(model.Id))
                return ProcessResultHelper.Failed<bool>
                    (false, null, message: string.Format(SharedResource.General_Block, SharedResource.Entity_CostCenter, SharedResource.General_UsedUserManagement), ProcessResultStatusCode.InvalidValue);

            return base.LogicalDelete(model);
        }

        public bool CheckCanBlock(int Id)
        {
            bool valid = true;
            var obj = base.Get(Id);
            if ( obj.Data.Managers?.Count > 0 || obj.Data.Supervisors?.Count > 0 || obj.Data.Dispatchers?.Count() >0
                ||obj.Data.Technicians?.Count > 0 || obj.Data.Foremans?.Count > 0 || obj.Data.Engineers?.Count() > 0)
            {
                valid = false;
                return valid;
            }


            return valid;
        }

    }
}
