﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using System.Linq;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.BLL.Managers
{
    public class DivisionManager : Repository<Division>, IDivisionManager
    {
      
        public DivisionManager(ApplicationDbContext context )
            : base(context)
        {
            
        }

        public override ProcessResult<Division> Add(Division model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<Division>(model, null, message: SharedResource.General_NoVal_Body , ProcessResultStatusCode.InvalidValue);


            var checkNoDublication = CheckDivisionNoDoublication(model.Name,  model.code,null );
            if (!checkNoDublication.IsSucceeded)
                return checkNoDublication;

            return base.Add(model);
        }

        public override ProcessResult<bool> Update(Division model)
        {
            if (model == null)
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.General_NoVal_Body , ProcessResultStatusCode.InvalidValue);
            var checkNoDublication = CheckDivisionNoDoublication(model.Name,  model.code, model.Id);
            if (!checkNoDublication.IsSucceeded)
                return ProcessResultHelper.Failed<bool>(false, null, message: checkNoDublication.Status.Message, ProcessResultStatusCode.InvalidValue);
            //if (CheckNameChanged(model))
            //    return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.Division_NotAllowChangeName , ProcessResultStatusCode.InvalidValue, "Update");
            if (CheckCodeChanged(model))
                return ProcessResultHelper.Failed<bool>(false, null, message: SharedResource.Division_NotAllowChangeCode, ProcessResultStatusCode.InvalidValue, "Update");
            return base.Update(model);
        } 
        public ProcessResult<Division> CheckDivisionNoDoublication(string name,  string code ,int? Id = null )
        {
            try
            {
                Division result = null;
                if (name != null &&code != null && Id != null)
                    result = Get(x => (x.Name.ToLower() == name.ToLower() || x.code == code)
                                        && x.Id != Id.Value)?.Data;
                else if (name != null && code != null && Id == null)
                    result = Get(x => x.Name.ToLower() == name.ToLower() ||
                         x.code == code)?.Data;
                if (result != null)
                    return ProcessResultHelper.Failed<Division>(result, null, message: string.Format(SharedResource.General_Duplicate, SharedResource.Entity_Division), ProcessResultStatusCode.InvalidValue, "CheckDivisionNoDoublication");
                else
                    return ProcessResultHelper.Succedded<Division>(result, (string)null, ProcessResultStatusCode.Succeded, "CheckDivisionNoDoublication");

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<Division>(null, ex, (string)null, ProcessResultStatusCode.Failed, "CheckDivisionNoDoublication");
            }
        }

        public override ProcessResult<bool> LogicalDelete(Division model)
        {
            if (!CheckCanBlock(model.Id))
                return ProcessResultHelper.Failed<bool>
                    (false, null, message: string.Format(SharedResource.General_Block, SharedResource.Entity_Division, SharedResource.General_HasOngoingOrders), ProcessResultStatusCode.InvalidValue); 
            return base.LogicalDelete(model);
        }
        public bool CheckCanBlock(int Id)
        {
            bool valid = true;
            var obj = base.Get(Id); 
            if (obj.Data.Orders != null)
            {
                List<int> status = new List<int>() { 1, 2, 3, 4, 5, 6 };
                if (obj.Data.Orders.Any(x => status.Contains(x.StatusId)))
                {
                    valid = false;
                }
            }
            return valid;
        }
        //public  bool CheckNameChanged(Division model)
        //{
        //    var result = base.Get(model.Id);
        //    if (result.Data.Name != model.Name)
        //        return true;
        //    else
        //        return false;
        // }

        public bool CheckCodeChanged(Division model)
        {
            var result = base.Get(model.Id);
            if (result.Data.code != model.code)
                return true;
            else
                return false;
        }
    }
}
