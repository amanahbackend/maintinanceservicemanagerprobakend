﻿using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.BLL.Managers;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.BLL.IManagers
{
    public class Lang_DivisionManager : Repository<Lang_Division>, ILang_DivisionManager
    {
        IServiceProvider _serviceprovider;
        public Lang_DivisionManager(IServiceProvider serviceprovider, ApplicationDbContext context)
            : base(context)
        {
            _serviceprovider = serviceprovider;
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        private IDivisionManager divisionManager
        {
            get
            {
                return _serviceprovider.GetService<IDivisionManager>();
            }
        }

        public ProcessResult<List<Lang_Division>> GetAllLanguagesByDivisionId(int DivisionId)
        {
            List<Lang_Division> input = null;
            try
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetAll().Data.OrderBy(x => x.Id).ToList();
                input = GetAllQuerable().Data.Where(x => x.FK_Division_ID == DivisionId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_Division { FK_Division_ID = DivisionId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_Division>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByDivisionId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Division>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByDivisionId");
            }
        }

        public ProcessResult<bool> UpdateByDivision(List<Lang_Division> entities)
        {
            bool result = false;
            try
            {
                List<Division> neededDivisions = divisionManager.GetByIds(entities.Select(x => x.FK_Division_ID).ToList()).Data;
                //List<Lang_Division> Dblang_Devisions = GetByDivisionIdAndsupprotedLangIdList(entities).Data;
                Division currentDevision = null;
                foreach (var item in entities)
                {
                    var supportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    //if language exist update it
                    if (supportedLanguageRes?.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        //todo
                        currentDevision = neededDivisions.FirstOrDefault(x => x.Id == item.FK_Division_ID);
                        if (currentDevision != null)
                            currentDevision.Name = item.Name;
                    }
                }

                result = Update(entities.Where(x => x.Id != 0).ToList()).IsSucceeded;
                result = result && Add(entities.Where(x => x.Id == 0).ToList()).IsSucceeded;
                //Update List division
                var updateRes = divisionManager.Update(neededDivisions);
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByDivisionId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByDivisionId");
            }
        }

        private ProcessResult<List<Lang_Division>> GetByDivisionIdAndsupprotedLangId(int DivisionId, int supprotedLangId)
        {
            List<Lang_Division> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Division_ID == DivisionId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_Division>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByDivisionIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Division>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDivisionIdAndsupprotedLangId");
            }
        }

        /// <summary>
        /// pass tuble of devisionId && FK_SupportedLanguages_ID to get a list
        /// </summary>
        /// <param name="inputTuble"></param>
        /// <returns></returns>
        private ProcessResult<List<Lang_Division>> GetByDivisionIdAndsupprotedLangIdList(List<Lang_Division> dataIbput)
        {
            List<Lang_Division> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => dataIbput.Any(i => i.FK_Division_ID == x.FK_Division_ID && i.FK_SupportedLanguages_ID == x.FK_SupportedLanguages_ID)).ToList();
                return ProcessResultHelper.Succedded<List<Lang_Division>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByDivisionIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Division>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByDivisionIdAndsupprotedLangId");
            }
        }
    }
}
