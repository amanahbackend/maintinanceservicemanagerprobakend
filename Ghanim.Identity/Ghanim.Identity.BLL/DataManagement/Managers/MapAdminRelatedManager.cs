﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{
    public class MapAdminRelatedManager : CashableRepository<MapAdminRelated, MapAdminRelatedViewModel>, IMapAdminRelatedManager
    {
        public MapAdminRelatedManager(ApplicationDbContext context, ICached<MapAdminRelatedViewModel> cash) : base(context, cash)
        {
        }
    }
}
