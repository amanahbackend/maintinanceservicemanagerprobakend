﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.Models.Context;

namespace Ghanim.BLL.Managers
{
    public class CompanyCodeManager : CashableRepository<CompanyCode, CompanyCodeViewModel>, ICompanyCodeManager
    {
        public CompanyCodeManager(ApplicationDbContext context, ICached<CompanyCodeViewModel> cash)
            : base(context, cash)
        {
        }
    }
}
