﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ghanim.Models.Context;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.BLL.Managers
{
    public class Lang_ShiftManager : Repository<Lang_Shift>, ILang_ShiftManager
    {
        public Lang_ShiftManager(ApplicationDbContext context)
            : base(context)
        {
        }
        public ProcessResult<List<Lang_Shift>> GetAllLanguagesByShiftId(int ShiftId)
        {
            List<Lang_Shift> input = null;
            try
            {
                input = GetAllQuerable().Data.Where(x => x.FK_Shift_ID == ShiftId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_Shift>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByShiftId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_Shift>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByShiftId");
            }
        }

        public ProcessResult<bool> UpdateByShift(List<Lang_Shift> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    result = Update(item).Data;
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByShiftId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByShiftId");
            }
        }
    }
}
