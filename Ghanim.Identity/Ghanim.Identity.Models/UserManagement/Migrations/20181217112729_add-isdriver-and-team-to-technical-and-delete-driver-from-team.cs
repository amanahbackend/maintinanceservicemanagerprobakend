﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.UserManagement.EFCore.MSSQL.Migrations
{
    public partial class addisdriverandteamtotechnicalanddeletedriverfromteam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DispatcherId",
                table: "Technicians");

            migrationBuilder.DropColumn(
                name: "DispatcherName",
                table: "Technicians");

            migrationBuilder.DropColumn(
                name: "ForemanId",
                table: "Technicians");

            migrationBuilder.DropColumn(
                name: "ForemanName",
                table: "Technicians");

            migrationBuilder.DropColumn(
                name: "SupervisorName",
                table: "Technicians");

            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "DriverName",
                table: "Teams");

            migrationBuilder.RenameColumn(
                name: "SupervisorId",
                table: "Technicians",
                newName: "TeamId");

            migrationBuilder.AddColumn<bool>(
                name: "IsDriver",
                table: "Technicians",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDriver",
                table: "Technicians");

            migrationBuilder.RenameColumn(
                name: "TeamId",
                table: "Technicians",
                newName: "SupervisorId");

            migrationBuilder.AddColumn<int>(
                name: "DispatcherId",
                table: "Technicians",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DispatcherName",
                table: "Technicians",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ForemanId",
                table: "Technicians",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ForemanName",
                table: "Technicians",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SupervisorName",
                table: "Technicians",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "Teams",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DriverName",
                table: "Teams",
                nullable: true);
        }
    }
}
