﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.UserManagement.EFCore.MSSQL.Migrations
{
    public partial class _addmanagerandmaterialcontrollers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Managers",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrentUserId = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DivisionId = table.Column<int>(nullable: false),
                    DivisionName = table.Column<string>(nullable: true),
                    CostCenterId = table.Column<int>(nullable: false),
                    CostCenterName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Managers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MaterialControllers",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrentUserId = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DivisionId = table.Column<int>(nullable: false),
                    DivisionName = table.Column<string>(nullable: true),
                    CostCenterId = table.Column<int>(nullable: false),
                    CostCenterName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialControllers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Managers");

            migrationBuilder.DropTable(
                name: "MaterialControllers");
        }
    }
}
