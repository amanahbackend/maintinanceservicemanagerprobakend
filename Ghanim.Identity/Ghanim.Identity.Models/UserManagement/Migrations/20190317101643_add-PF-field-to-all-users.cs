﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.UserManagement.EFCore.MSSQL.Migrations
{
    public partial class addPFfieldtoallusers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Supervisors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "MaterialControllers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Managers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Foremans",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Engineers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Dispatchers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PF",
                table: "Supervisors");

            migrationBuilder.DropColumn(
                name: "PF",
                table: "MaterialControllers");

            migrationBuilder.DropColumn(
                name: "PF",
                table: "Managers");

            migrationBuilder.DropColumn(
                name: "PF",
                table: "Foremans");

            migrationBuilder.DropColumn(
                name: "PF",
                table: "Engineers");

            migrationBuilder.DropColumn(
                name: "PF",
                table: "Dispatchers");
        }
    }
}
