﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
   public interface IForeman : IBaseEntity
    {
        string UserId { get; set; }
        string Name { get; set; }
        string PF { get; set; }
        int DivisionId { get; set; }
        int CostCenterId { get; set; }
        int? DispatcherId { get; set; }
        int? EngineerId { get; set; }
    }
}
