﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
   public interface IDispatcherSettings : IBaseEntity
    {
        int DispatcherId { get; set; }
        int OrderProblemId { get; set; } 
        int? DivisionId { get; set; }
        int? AreaId { get; set; }
        int GroupId { get; set; }
    }
}
