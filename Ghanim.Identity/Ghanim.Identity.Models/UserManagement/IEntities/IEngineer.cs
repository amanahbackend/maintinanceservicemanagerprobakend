﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
   public interface IEngineer : IBaseEntity
    {
        string UserId { get; set; }
        int DivisionId { get; set; }
        int CostCenterId { get; set; }
    }
}
