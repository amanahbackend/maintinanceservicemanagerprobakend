﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
   public interface IAttendance :IBaseEntity
    {
        string AttendedId { get; set; }
        string AttendedName { get; set; }
        int StatusId { get; set; }
        int StatusName { get; set; }
        DateTime AttendedDate { get; set; }
    }
}
