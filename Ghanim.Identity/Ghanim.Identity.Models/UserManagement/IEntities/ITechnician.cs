﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ITechnician : IBaseEntity
    {
        string UserId { get; set; }
        //string Name { get; set; }
        //string PF { get; set; }
        int DivisionId { get; set; }
        int CostCenterId { get; set; }
        int? TeamId { get; set; }
        //Team Team { get; set; }
        bool IsDriver { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        SupportedLanguages SupportedLanguages { get; set; }
    }
}