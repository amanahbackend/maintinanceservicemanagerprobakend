﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
   public interface IVehicle : IBaseEntity
    {
        string Plate_no { get; set; }
        bool Isassigned { get; set; }
        int? TeamId { get; set; }
        Team Team { get; set; }
    }
}

