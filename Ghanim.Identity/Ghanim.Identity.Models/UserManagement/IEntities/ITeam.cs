﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ITeam : IBaseEntity
    {
        string VehicleNo { get; set; }
        //string DriverName { get; set; }
        //int DriverId { get; set; }
        int? DivisionId { get; set; }
        int? ForemanId { get; set; }
        int? EngineerId { get; set; }
        int DispatcherId { get; set; }
        int? SupervisorId { get; set; }
        int StatusId { get; set; }
        int ShiftId { get; set; }
    }
}