﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class DispatcherSettingsTypeConfiguration : BaseEntityTypeConfiguration<DispatcherSettings>
    {
        public override void Configure(EntityTypeBuilder<DispatcherSettings> DispatcherSettings)
        {
            base.Configure(DispatcherSettings);

            DispatcherSettings.HasOne(x => x.Division)
                .WithMany(x => x.DispatcherSettings)
                .HasForeignKey(x => x.DivisionId);
               

            DispatcherSettings.HasOne(x => x.Dispatcher)
                .WithMany(x => x.DispatcherSettings)
                .HasForeignKey(x => x.DispatcherId);

            DispatcherSettings.HasOne(x => x.OrderProblem)
                .WithMany(x => x.DispatcherSettings)
                .HasForeignKey(x => x.OrderProblemId);


            DispatcherSettings.HasOne(x => x.Area)
                .WithMany(x => x.DispatcherSettings)
                .HasForeignKey(x => x.AreaId);

            DispatcherSettings.ToTable("DispatcherSettings");
            DispatcherSettings.HasKey(o => o.Id);
            DispatcherSettings.Property(o => o.Id).ForSqlServerUseSequenceHiLo("DispatcherSettingsseq");
            DispatcherSettings.Property(o => o.AreaId).IsRequired();
            //DispatcherSettings.Property(o => o.AreaName).IsRequired();
            DispatcherSettings.Property(o => o.DispatcherId).IsRequired();
            //DispatcherSettings.Property(o => o.DispatcherName).IsRequired();
            DispatcherSettings.Property(o => o.OrderProblemId).IsRequired();
            DispatcherSettings.Property(o => o.DivisionId).IsRequired();
            //DispatcherSettings.Property(o => o.OrderProblemName).IsRequired();
        }
    }
}