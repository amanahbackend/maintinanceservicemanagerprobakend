﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class SupervisorTypeConfiguration : BaseEntityTypeConfiguration<Supervisor>
    {
        public void Configure(EntityTypeBuilder<Supervisor> builder)
        {
            base.Configure(builder);
            builder.HasOne(x => x.User)
                .WithOne(x => x.Supervisor);

            builder.HasOne(x => x.Division)
                .WithMany(x => x.Supervisors)
                .HasForeignKey(x => x.DivisionId);


            builder.HasOne(x => x.CostCenter)
                .WithMany(x => x.Supervisors)
                .HasForeignKey(x => x.CostCenterId);


            builder.HasOne(x => x.Manager)
                .WithMany(x => x.Supervisors)
                .HasForeignKey(x => x.ManagerId);



            builder.ToTable("Supervisor");
        }
    }
}