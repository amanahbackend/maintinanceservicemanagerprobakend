﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class TeamTypeConfiguration : BaseEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            base.Configure(builder);
            builder.HasOne(x => x.Division)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.DivisionId);

            builder.HasOne(x => x.Engineer)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.EngineerId);


            builder.HasOne(x => x.Dispatcher)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.DispatcherId);

            builder.HasOne(x => x.Supervisor)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.SupervisorId);


            builder.HasOne(x => x.Status)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.StatusId);

            builder.HasOne(x => x.Shift)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.ShiftId);


            builder.HasOne(x => x.Foreman)
                .WithMany(x => x.Teams)
                .HasForeignKey(x => x.ForemanId);


            builder.ToTable("Team");
        }
    }
}