﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class DispatcherTypeConfiguration : BaseEntityTypeConfiguration<Dispatcher>
    {
        public void Configure(EntityTypeBuilder<Dispatcher> builder)
        {
            base.Configure(builder);
            builder.HasOne(x => x.User)
                .WithOne(x => x.Dispatcher);

            builder.HasOne(x => x.Division)
                .WithMany(x => x.Dispatchers)
                .HasForeignKey(x=> x.DivisionId);


            builder.HasOne(x => x.CostCenter)
                .WithMany(x => x.Dispatchers)
                .HasForeignKey(x => x.CostCenterId);

            builder.HasOne(x => x.Supervisor)
                .WithMany(x => x.Dispatchers)
                .HasForeignKey(x => x.SupervisorId);

            //.HasForeignKey(x => x.);




            builder.ToTable("Dispatcher");


            //builder.Entity<UserCustomer>().HasKey(bc => new { bc.AppUserId, bc.CustomerId });

            //builder.Entity<UserCustomer>()
            //    .HasOne(bc => bc.AppUser)
            //    .WithMany(b => b.UserCustomers)
            //    .HasForeignKey(bc => bc.AppUserId);
        }
    }
}