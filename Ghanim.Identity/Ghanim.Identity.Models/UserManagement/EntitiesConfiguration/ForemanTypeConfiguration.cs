﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class ForemanTypeConfiguration : BaseEntityTypeConfiguration<Foreman>
    {
        public void Configure(EntityTypeBuilder<Foreman> builder)
        {
            base.Configure(builder);

            builder.HasOne(x => x.User)
                .WithOne(x => x.Foreman);
                //.HasForeignKey(x => x.UserId);


        builder.HasOne(x => x.Division)
                .WithMany(x => x.Foremans)
                .HasForeignKey(x => x.DivisionId);


            builder.HasOne(x => x.CostCenter)
                .WithMany(x => x.Foremans)
                .HasForeignKey(x => x.CostCenterId);


            builder.HasOne(x => x.Dispatcher)
                .WithMany(x => x.Foremans)
                .HasForeignKey(x => x.DispatcherId);



            builder.HasOne(x => x.Engineer)
                .WithMany(x => x.Foremans)
                .HasForeignKey(x => x.EngineerId);


            //has parent one engineer 
            builder.ToTable("Foreman");
        }
    }
}