﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class MaterialControllerTypeConfiguration : BaseEntityTypeConfiguration<MaterialController>
    {
        public void Configure(EntityTypeBuilder<MaterialController> builder)
        {
            base.Configure(builder);
            builder.ToTable("MaterialController");
        }
    }
}
