﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class TechniciansStateTypeConfiguration : BaseEntityTypeConfiguration<TechniciansState>
    {
        public void Configure(EntityTypeBuilder<TechniciansState> builder)
        {
            base.Configure(builder);
            builder.ToTable("TechniciansState");
        }
    }
}
