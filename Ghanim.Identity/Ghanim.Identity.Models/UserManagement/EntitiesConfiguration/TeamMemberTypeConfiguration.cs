﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class TeamMemberTypeConfiguration : BaseEntityTypeConfiguration<TeamMember>
    {
        public void Configure(EntityTypeBuilder<TeamMember> builder)
        {
            base.Configure(builder);

            builder.HasOne(x => x.User)
                .WithOne(x => x.TeamMember);
            builder.HasOne(x => x.Team).WithMany(x => x.Members).HasForeignKey(x => x.TeamId);
            builder.HasOne(x => x.Division).WithMany(x => x.Members).HasForeignKey(x => x.DivisionId);

            builder.ToTable("TeamMember");
            builder.Property(o => o.TeamId).IsRequired(false);
        }
    }
}