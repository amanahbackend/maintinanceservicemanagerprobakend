﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class ManagerTypeConfiguration : BaseEntityTypeConfiguration<Manager>
    {
        public void Configure(EntityTypeBuilder<Manager> builder)
        {
            base.Configure(builder);


            builder.HasOne(x => x.User)
                .WithOne(x => x.Manager);

            builder.HasOne(x => x.CostCenter)
                .WithMany(x => x.Managers)
                .HasForeignKey(x => x.CostCenterId);


            builder.ToTable("Manager");
        }
    }
}
