﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class TechnicianTypeConfiguration : BaseEntityTypeConfiguration<Technician>
    {
        public void Configure(EntityTypeBuilder<Technician> builder)
        {
            base.Configure(builder);


            builder.HasOne(x => x.User)
                .WithOne(x => x.Technician);
            //.HasForeignKey(x => x.DivisionId);

            builder.HasOne(x => x.Division)
                .WithMany(x => x.Technicians)
            .HasForeignKey(x => x.DivisionId);

            builder.HasOne(x => x.CostCenter)
                .WithMany(x => x.Technicians)
                .HasForeignKey(x => x.CostCenterId);

            builder.HasOne(x => x.Team)
                .WithMany(x => x.Technicians)
                .HasForeignKey(x => x.TeamId);

            builder.ToTable("Technician");
        }
    }
}