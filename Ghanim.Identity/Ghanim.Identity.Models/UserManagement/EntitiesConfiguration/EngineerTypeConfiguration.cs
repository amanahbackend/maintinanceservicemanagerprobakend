﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class EngineerTypeConfiguration : BaseEntityTypeConfiguration<Engineer>
    {
        public void Configure(EntityTypeBuilder<Engineer> builder)
        {
            base.Configure(builder);

            builder.HasOne(x => x.User)
                .WithOne(x => x.Engineer);

            builder.HasOne(x => x.Division)
                .WithMany(x => x.Engineers)
                .HasForeignKey(x => x.DivisionId);


            builder.HasOne(x => x.CostCenter)
                .WithMany(x => x.Engineers)
                .HasForeignKey(x => x.CostCenterId);

            builder.HasOne(x => x.Dispatcher)
                .WithMany(x => x.Engineers)
                .HasForeignKey(x => x.DispatcherId);


            builder.ToTable("Engineer");
        }
    }
}