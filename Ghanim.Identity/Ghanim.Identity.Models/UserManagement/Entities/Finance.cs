﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
   public class Finance : BaseEntity
    {
        public string Name { get; set; }
        public string PF { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }


    }
}