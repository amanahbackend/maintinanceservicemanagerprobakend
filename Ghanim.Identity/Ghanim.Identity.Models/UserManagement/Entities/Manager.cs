﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;

namespace Ghanim.Models.Entities
{
    public class Manager: BaseEntity , IManager
    {
        public string Name { get; set; }
        public string PF { get; set; }
        
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }
        //public string CostCenterName { get; set; }

        public virtual ICollection<Supervisor> Supervisors { get; set; }
    }
}
