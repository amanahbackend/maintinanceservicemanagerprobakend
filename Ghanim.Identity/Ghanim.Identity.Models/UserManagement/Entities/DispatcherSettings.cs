﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class DispatcherSettings : BaseEntity, IDispatcherSettings
    {
        public int? DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public int DispatcherId { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }

        public int OrderProblemId { get; set; }
        public virtual OrderProblem OrderProblem { get; set; }

        public int? AreaId { get; set; }
        public virtual Areas Area { get; set; }

        public int GroupId { get; set; }

    }
}
