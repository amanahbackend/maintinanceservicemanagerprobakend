﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
    public class Technician : BaseEntity, ITechnician
    {
        //public string Name { get; set; }
        //public string PF { get; set; }
        //public bool IsDriver { get; set; }

        //public string UserId { get; set; }


        //public int DivisionId { get; set; }

        //public int CostCenterId { get; set; }

        //public int? TeamId { get; set; }
        //public int FK_SupportedLanguages_ID { get; set; }
        //[NotMapped]
        //public SupportedLanguages SupportedLanguages { get; set; }

        //public string DivisionName { get; set; }
        //public string CostCenterName { get; set; }


        public bool IsDriver { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }


        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }


        public int FK_SupportedLanguages_ID { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }

        public virtual ICollection<OrderAction> OrderActions { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }

        //public Team Team { get; set; }

        //public int SupervisorId { get; set; }
        //public string SupervisorName { get; set; }
        //public int DispatcherId { get; set; }
        //public string DispatcherName { get; set; }
        //public int ForemanId { get; set; }
        //public string ForemanName { get; set; }
    }
}
