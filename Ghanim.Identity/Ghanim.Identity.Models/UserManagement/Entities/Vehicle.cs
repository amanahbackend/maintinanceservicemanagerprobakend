﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class Vehicle : BaseEntity, IVehicle
    {
        public string Plate_no { get; set; }
        //TODO remove this prop and check is assigned or not only from teamId if it has a value then its assigned
        public bool Isassigned { get; set; }
        public int? TeamId { get; set; }
        [NotMapped]
        public Team Team { get; set; }
    }
}
