﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
    public class Supervisor : BaseEntity, ISupervisor
    {
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int? CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public int? DivisionId { get; set; }
        public virtual Division Division { get; set; }


        public int? ManagerId { get; set; }
        public virtual Manager Manager { get; set; }


        public virtual ICollection<Dispatcher> Dispatchers { get; set; }
        public virtual ICollection<Team> Teams { get; set; }
        //public string DivisionName { get; set; }
        //public string CostCenterName { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }
        public virtual ICollection<OrderAction> OrderActions { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }
    }
}
