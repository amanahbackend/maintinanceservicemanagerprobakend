﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Team : BaseEntity, ITeam
    {
        public string Name { get; set; }

        public string VehicleNo { get; set; }
        //public string DriverName { get; set; }
        //public int DriverId { get; set; }
        public int? DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public int? EngineerId { get; set; }
        public virtual Engineer Engineer { get; set; }


        public int DispatcherId { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }

        public int? SupervisorId { get; set; }
        public virtual Supervisor Supervisor { get; set; }


        public int StatusId { get; set; }
        public virtual Availability Status { get; set; }


        public int ShiftId { get; set; }
        public virtual Shift Shift { get; set; }


        public int? ForemanId { get; set; }
        public virtual Foreman Foreman { get; set; }

        public virtual ICollection<TeamMember> Members { get; set; }
        public virtual ICollection<Technician> Technicians { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }
        public virtual ICollection<OrderAction> OrderActions { get; set; }
    }
}
