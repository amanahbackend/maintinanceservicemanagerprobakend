﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
  public  class Engineer : BaseEntity, IEngineer
    {
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }
        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public int? DispatcherId { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }

        public virtual ICollection<Foreman> Foremans { get; set; }
        public virtual ICollection<Team> Teams { get; set; }


    }
}