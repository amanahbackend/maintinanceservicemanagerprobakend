﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CommonEnums;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Ghanim.Models.Order.Enums;

namespace Ghanim.Models.Entities
{
    public class TeamMember : BaseEntity//, ITeamMember
    {


        public int MemberParentId { get; set; }
        public string MemberParentName { get; set; }
        public MemberType MemberType { get; set; }

        public int Rank { get; set; }
        public  bool IsDefault { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }


        public int? DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public UserTypesEnum UserTypeId { get; set; }

    }
}
