﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
    public class Dispatcher : BaseEntity, IDispatcher
    {
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public int SupervisorId { get; set; }
        public virtual Supervisor Supervisor { get; set; }

        public virtual ICollection<Foreman> Foremans { get; set; }
        public virtual ICollection<Engineer> Engineers { get; set; }
        public virtual ICollection<DispatcherSettings> DispatcherSettings { get; set; }
        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }
        public virtual ICollection<OrderAction> OrderActions { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }
    }
}