﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
   public class Foreman : BaseEntity, IForeman
    {
        public string Name { get; set; }
        public string PF { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public int? DispatcherId { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }

        public int? EngineerId { get; set; }
        public virtual Engineer Engineer { get; set; }

        public int SupervisorId { get; set; }

        public virtual ICollection<Team> Teams { get; set; }

        public virtual ICollection<OrderAction> OrderActions { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }


    }
}