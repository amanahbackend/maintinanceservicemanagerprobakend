﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Contracts.Entities
{
    public class ContractLocation : BaseEntity
    {

        public int FK_Contract_Id { get; set; }

        public int FK_Location_Id { get; set; }
        public Contract Contract { get; set; }
        //public Location Location { get; set; }

        public int? FK_OrderProblem_Id { get; set; }
        public int? FK_OrderPriority_Id { get; set; }
        public string FK_Technician_Id { get; set; }
        public string PmTasks { get; set; }


        public List<LocationPMConfiguration> LocationPMConfiguration { get; set; }
    }
}
