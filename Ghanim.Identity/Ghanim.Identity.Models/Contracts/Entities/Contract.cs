﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Contracts.Entities
{
    public class Contract : BaseEntity
    {
        public string ContractID { get; set; }
        public double ContractValue { get; set; }

        [ForeignKey("ContractTypes")]
        public int ContractTypeId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ContractManagerId { get; set; }
        public string CollectorId { get; set; }
        [ForeignKey("Division")]
        public int DivisionId { get; set; }
        public string Remarks { get; set; }
        public bool HasPreventiveMaintenance { get; set; }
       
            
        public int CustomerId { get; set; }

    }
}
