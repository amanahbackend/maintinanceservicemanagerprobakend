﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace Ghanim.Models.Context.Base
{
    public class BaseEntityDb
    {
        [Key]
        public  int Id { get; set; }

    }
}
