﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class OrderSubStatus : BaseEntity, IOrderSubStatus
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int StatusId { get; set; }
        public int StatusName { get; set; }

        public virtual ICollection<OrderObject> Orders { get; set; }
        public virtual ICollection<OrderAction> OrderActions { get; set; }
        
    }
}
