﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Notification : BaseEntity, INotification
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
