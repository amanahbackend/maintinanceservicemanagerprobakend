﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Order.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class OrderRemoteRequestsOnProcess : BaseEntity
    {
        public string Code { get; set; }
        public DateTime? VisitDate { get; set; } 
        public string VisitCode { get; set; }
        public OrderRequestTypeEnum OrderRequestTypeId { get; set; }
    }
}
