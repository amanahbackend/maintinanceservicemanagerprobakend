﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class NotificationCenter : BaseEntity, INotificationCenter
    {
        public string RecieverId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Data { get; set; }
        public string NotificationType { get; set; }
        public bool IsRead { get; set; }
        public int? TeamId { get; set; }
        public bool? IsActionTaken { get; set; }
        public string OrderCode { get; set; }

    }
}
