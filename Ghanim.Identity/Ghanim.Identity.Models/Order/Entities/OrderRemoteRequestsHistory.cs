﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Order.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class OrderRemoteRequestsHistory : BaseEntity
    {
      public string Code { get; set; }
      public string RequestBody { get; set; }
      public OrderRequestTypeEnum OrderRequestTypeId { get; set; }
    }
}
