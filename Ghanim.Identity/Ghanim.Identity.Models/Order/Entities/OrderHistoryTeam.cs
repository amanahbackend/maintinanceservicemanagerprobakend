﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class OrderHistoryTeam : BaseEntity, IOrderHistoryTeam
    {
        public int OrderId { get; set; }
        public string Code { get; set; }
        public int? TeamId { get; set; }
        public int? PrevTeamId { get; set; }

    }
}
