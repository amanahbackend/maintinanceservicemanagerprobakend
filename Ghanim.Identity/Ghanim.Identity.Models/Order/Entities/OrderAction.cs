﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using CommonEnums;
using Ghanim.Models.Order.Enums;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Ghanim.Models.Entities
{
    public class OrderAction : BaseEntity//, IOrderAction
    {
        public int? OrderId { get; set; }
        public virtual OrderObject Order { get; set; }

        [ForeignKey("Status")]
        public int? StatusId { get; set; }
        [ForeignKey("StatusId")]
        public virtual OrderStatus Status { get; set; }

        public int? SubStatusId { get; set; }
        //TODO in MR and Printout need to be replaced with the object
        //public string SubStatusName { get; set; }
        public virtual OrderSubStatus SubStatus { get; set; }

        public DateTime ActionDate { get; set; }
        public DateTime ActionDay { get; set; }
        public TimeSpan? ActionTime { get; set; }
        public int? ActionTimeDays { get; set; }



        public int? CreatedUserId { get; set; }//currect user
        public string CreatedUser { get; set; }//Techniccian only


        //public int? TechnicianId { get; set; }//tecgnician
        //public virtual Technician Technician { get; set; }//tecgnician

        [ForeignKey("ExcuterUser")]
        public override string FK_CreatedBy_Id { get; set; }
        [ForeignKey("FK_CreatedBy_Id")]
        public virtual ApplicationUser ExcuterUser { get; set; }//tecgnician


        [ForeignKey("Technician")]
        public int? TechnicianId { get; set; }
        [ForeignKey("TechnicianId")]
        public virtual Technician Technician { get; set; }



        public int? ActionTypeId { get; set; }
        public string ActionTypeName { get; set; }//has no tables
        public int? CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public int? WorkingTypeId { get; set; }
        //TODO in MR and Printout need to be replaced with the object
        //public string WorkingTypeName { get; set; }
        public virtual WorkingType WorkingType { get; set; }


        public string Reason { get; set; }
        public string ServeyReport { get; set; }

        // assigning 
        public double ActionDistance { get; set; }
        public int? SupervisorId { get; set; }
        public virtual Supervisor Supervisor { get; set; }

        public int? DispatcherId { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }

        public int? ForemanId { get; set; }
        public virtual Foreman Foreman { get; set; }

        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }

        public PlatformType PlatformTypeSource { get; set; }



        public OrderAction Clone() => (OrderAction)MemberwiseClone();

        //new Prop
        public string WorkingSubReason { get; set; }
        public int? WorkingSubReasonId { get; set; }


        //public string PF { get; set; }
        //public int DriverId { get; set; }
        //public string DriverName { get; set; }
        //public string DriverPFNumber { get; set; }
    }
}
