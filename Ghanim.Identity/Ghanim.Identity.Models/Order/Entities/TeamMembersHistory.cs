﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using Ghanim.Models.Order.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class TeamMembersHistory : BaseEntity, ITeamMembersHistory
    {
        public string UserId { get; set; }
        public int? UserDetailId { get; set; }//tech or dispatcher or driver table id 
        public int? TeamId { get; set; }
        public int? PrevTeamId { get; set; }
        public UserTypesEnum UserTypeId { get; set; }
        public int? VehicleId { get; set; }
        
    }
}
