﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class OrderProblem : BaseEntity, IOrderProblem
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int ExceedHours { get; set; }
        public virtual ICollection<DispatcherSettings> DispatcherSettings { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }
    }
}
