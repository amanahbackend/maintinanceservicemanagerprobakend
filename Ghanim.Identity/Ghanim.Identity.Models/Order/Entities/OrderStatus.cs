﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
    public class OrderStatus : BaseEntity, IOrderStatus
    {
        public string Name { get; set; }
        public string Code { get; set; }


        public virtual ICollection<OrderObject> OrderObjects { get; set; }
        public virtual ICollection<OrderAction> OrderActions { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }

        
    }
}
