﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class ArchivedOrderFile : BaseEntity, IArchivedOrderFile
    {
        public string FileName { get; set; }
        public int Trials { get; set; }
    }
}