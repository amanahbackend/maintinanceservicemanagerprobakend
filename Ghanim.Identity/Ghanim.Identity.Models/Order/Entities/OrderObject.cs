﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Ghanim.Models.Context.Base;
using Ghanim.Models.Payment.Entities;

//    SELECT *
//    --TypeId
//    --StatusId
//    --SubStatusId
//    --ProblemId
//    --PriorityId
//    --CompanyCodeId
//    --DivisionId

//    --StreetId
//    --BlockId
//    --AreaId
//    --GovId

//    --BuildingTypeId
//    --ContractTypeId

//    --SupervisorId
//    --DispatcherId
//    --TeamId
//    --PrevTeamId
//    --RejectionReasonId
//    FROM[CodeFirst - db].[dbo].[Orders]
//where RejectionReasonId = 0;


//--0
//--DivisionId  
//--ProblemId
//------------StreetId
//------------BlockId
//------------GovId
//--BuildingTypeId
//--ContractTypeId
//--DispatcherId
//--TeamId
//--PrevTeamId
//--RejectionReasonId



//-- mapped 
//--DivisionId  
//--ProblemId
//--BuildingTypeId
//--ContractTypeId
//--DispatcherId
//--TeamId
//--PrevTeamId


namespace Ghanim.Models.Entities
{
    public class OrderObject : BaseEntity//, IOrderObject
    {
        //public string FK_CreatedBy_Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public string FK_UpdatedBy_Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public string FK_DeletedBy_Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public DateTime CreatedDate { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public DateTime UpdatedDate { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public DateTime DeletedDate { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public bool IsDeleted { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public string CurrentUserId { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }


        // Order infow
        public string Code { get; set; }
        public int? TypeId { get; set; }
        public virtual OrderType Type { get; set; }
        //TODO in MR and Printout need to be replaced with the object
        //public string TypeName { get; set; }


        [ForeignKey("Status")]
        public int StatusId { get; set; }
        [ForeignKey("StatusId")]
        public virtual OrderStatus Status { get; set; }

        
        public int? SubStatusId { get; set; }
        public virtual OrderSubStatus SubStatus { get; set; }
        //TODO in MR and Printout need to be replaced with the object
        //public string SubStatusName { get; set; }

        public int? ProblemId { get; set; }
        public virtual OrderProblem Problem { get; set; }

        public int? PriorityId { get; set; }
        public virtual OrderPriority Priority { get; set; }
        //TODO in MR and Printout need to be replaced with the object
        //public string PriorityName { get; set; }

        public int CompanyCodeId { get; set; }
        public virtual CompanyCode CompanyCode { get; set; }
        //TODO in MR and Printout need to be replaced with the object
        //public string CompanyCodeName { get; set; }

        public int? DivisionId { get; set; }
        public virtual Division Division { get; set; }

        public string ICAgentNote { get; set; }
        public string DispatcherNote { get; set; }
        public string CancellationReason { get; set; }
        public string GeneralNote { get; set; }
        public DateTime SAP_CreatedDate { get; set; }
        public string OrderDescription { get; set; }
        public string ServeyReport { get; set; }
        public string OrderTypeCode { get; set; }

        // Customer info
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string PhoneOne { get; set; }
        public string PhoneTwo { get; set; }
        public string Caller_ID { get; set; }
        // Address info
        //// SAP address
        public string SAP_PACI { get; set; }
        public string SAP_HouseKasima { get; set; }
        public string SAP_Floor { get; set; }
        public string SAP_AppartmentNo { get; set; }
        public string SAP_StreetName { get; set; }
        public string SAP_BlockName { get; set; }
        public string SAP_AreaName { get; set; }
        public string SAP_GovName { get; set; }
        //// PACI address
        public string PACI { get; set; }
        public string FunctionalLocation { get; set; }
        public string HouseKasima { get; set; }
        public string Floor { get; set; }
        public string AppartmentNo { get; set; }

        public int StreetId { get; set; }
        public string StreetName { get; set; }

        public int BlockId { get; set; }
        public string BlockName { get; set; }

        public int AreaId { get; set; }
        public virtual Areas Area { get; set; }
        //TODO in MR and Printout need to be replaced with the object
        //public string AreaName { get; set; }

        public int GovId { get; set; }
        public string GovName { get; set; }

        public string AddressNote { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
        public int? BuildingTypeId { get; set; }
        public virtual BuildingTypes BuildingType { get; set; }
        // Contract info
        public string ContractCode { get; set; }
        public int? ContractTypeId { get; set; }
        public virtual ContractTypes ContractType { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractExpiryDate { get; set; }
        // Reading info
        public DateTime InsertionDate { get; set; }
        public string FileName { get; set; }
        // Assigning info
        public int? SupervisorId { get; set; }
        public virtual Supervisor Supervisor { get; set; }

        public int? DispatcherId { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }

        //public string SupervisorName() =>
        //    this.DispatcherId == null ? Supervisor?.User?.FullName : Dispatcher?.Supervisor?.User?.FullName;


        public int? RankInDispatcher { get; set; }
        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }
        public int? PrevTeamId { get; set; }
        //public virtual Team PrevTeam { get; set; }
        public int? RankInTeam { get; set; }
        public AcceptenceType AcceptanceFlag { get; set; }
        public int? RejectionReasonId { get; set; }
        public string RejectionReason { get; set; }
        public AccomplishType IsAccomplish { get; set; }
        public int OnHoldCount { get; set; }
        // Repeated call
        public bool IsRepeatedCall { get; set; }
        // Is Exceed time
        public bool IsExceedTime { get; set; }

        public string LastActorTechUserId { get; set; }
        public virtual ApplicationUser LastActorTechUser { get; set; }



        #region Payment Props

        public string CustomerEnglishName { get; set; }
        public DateTime? Visitdate { get; set; }

        public int? VisitSlotId { get; set; }
        public virtual VisitSlot VisitSlot { get; set; }



        public string TimeSlot { get; set; }
        public string CreatedByOnSAP { get; set; }
        public decimal ServiceCharges { get; set; }
        public decimal AmountPaid  { get; set; }
        public float OfferDiscount  { get; set; } // 20%  or.2 
        public decimal Count  { get; set; }
        public decimal ExpressCharges  { get; set; }
        public decimal ServiceTotalAmount { get; set; }
        public string CustomerMail { get; set; }
        #endregion



        //Sub canselatio
        //public string SubCancellationReason { get; set; }
        //public DateTime? OrderExpiredDate { get; set; }










        public virtual ICollection<OrderAction> OrderActions { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }



        public OrderObject Clone() => (OrderObject)MemberwiseClone();
    }
}
