﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class WorkingType : BaseEntity,IWorkingType
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int CategoryCode { get; set; }
        public virtual ICollection<OrderAction> OrderActions { get; set; }

    }
}