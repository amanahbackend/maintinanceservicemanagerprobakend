﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
namespace Ghanim.Models.Entities
{
    public class VisitSlot : BaseEntity
    {
        public string Code { get; set; }
        public string SlotDescription { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }

    }
}
