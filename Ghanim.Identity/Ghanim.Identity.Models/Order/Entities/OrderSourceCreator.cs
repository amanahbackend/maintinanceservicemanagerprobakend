﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;
namespace Ghanim.Models.Entities
{
    public class OrderSourceCreator : BaseEntity
    {
        public string UserName { get; set; }
        public string ImgPath { get; set; }
    }
}
