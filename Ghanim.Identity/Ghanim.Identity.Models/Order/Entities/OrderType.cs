﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class OrderType : BaseEntity, IOrderType
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }

    }
}
