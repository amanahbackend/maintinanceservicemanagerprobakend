﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class PACI : BaseEntity, IPACI
    {
        public string PACINumber { get; set; }
        public string AreaName { get; set; }
        public int AreaId { get; set; }
        public string GovernorateName { get; set; }
        public int GovernorateId { get; set; }
        public string BlockName { get; set; }
        public int BlockId { get; set; }
        public string StreetName { get; set; }
        public int StreetId { get; set; }
        public decimal Longtiude { get; set; }
        public decimal Latitude { get; set; }
        public string AppartementNumber { get; set; }
        public string FloorNumber { get; set; }
        public string BuildingNumber { get; set; }
    }
}
