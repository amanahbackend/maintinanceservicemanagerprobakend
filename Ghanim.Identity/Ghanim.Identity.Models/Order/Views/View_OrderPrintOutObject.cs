﻿using Ghanim.Models.Order.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Views
{
    

    [Table("View_OrderPrintOut")]
    public class View_OrderPrintOutObject
    {  // Order info

        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int SubStatusId { get; set; }
        public string SubStatusName { get; set; }
        public int? ProblemId { get; set; }
        public string ProblemName { get; set; }
        public int PriorityId { get; set; }
        public string PriorityName { get; set; }
        public int CompanyCodeId { get; set; }
        public string CompanyCodeName { get; set; }
        public string OrderTypeCode { get; set; }
        public int? DivisionId { get; set; }

        public string DivisionName { get; set; }
        public string ICAgentNote { get; set; }
        public string DispatcherNote { get; set; }
        public string CancellationReason { get; set; }
        public string GeneralNote { get; set; }
        public Nullable<DateTime> SAP_CreatedDate { get; set; }
        public string OrderDescription { get; set; }
        public string ServeyReport { get; set; }

        // Customer info
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string PhoneOne { get; set; }
        public string PhoneTwo { get; set; }
        public string Caller_ID { get; set; }
        // Address info
        //// SAP address
        public string SAP_PACI { get; set; }
        public string SAP_HouseKasima { get; set; }
        public string SAP_Floor { get; set; }
        public string SAP_AppartmentNo { get; set; }
        public string SAP_StreetName { get; set; }
        public string SAP_BlockName { get; set; }
        public string SAP_AreaName { get; set; }
        public string SAP_GovName { get; set; }
        //// PACI address
        public string PACI { get; set; }
        public string FunctionalLocation { get; set; }
        public string HouseKasima { get; set; }
        public string Floor { get; set; }
        public string AppartmentNo { get; set; }
        public int StreetId { get; set; }
        public string StreetName { get; set; }
        public int BlockId { get; set; }
        public string BlockName { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public int GovId { get; set; }
        public string GovName { get; set; }
        public string AddressNote { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
        public int? BuildingTypeId { get; set; }
        public string BuildingTypeName { get; set; }
        // Contract info
        public string ContractCode { get; set; }
        public int? ContractTypeId { get; set; }
        public string ContractTypeName { get; set; }
        public Nullable<DateTime> ContractStartDate { get; set; }
        public Nullable<DateTime> ContractExpiryDate { get; set; }
        // Reading info
        public Nullable<DateTime> InsertionDate { get; set; }
        public string FileName { get; set; }
        // Assigning info
        public int? SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int? DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int? RankInDispatcher { get; set; }
        public int? TeamId { get; set; }
        public int? PrevTeamId { get; set; }
        public int? RankInTeam { get; set; }


        //public int? TechId { get; set; }
        //public string TechName { get; set; }
        ////public int? TeamId { get; set; }
        //public string TeamName { get; set; }
        //public string TechNotes { get; set; }
        //public string Pin { get; set; }


        //orderaction 

        public string acFK_CreatedBy_Id { get; set; }
        public PlatformType? acPlatformTypeSource { get; set; }
        public int? acId { get; set; }



    }





}
