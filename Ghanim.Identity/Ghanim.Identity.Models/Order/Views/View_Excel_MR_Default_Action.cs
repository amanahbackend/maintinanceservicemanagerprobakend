﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ghanim.Models.Entities;
using Ghanim.Models.Order.Enums;

namespace Ghanim.Models.Views
{

    [Table("View_Excel_MR_DefaultAction")]
    public class View_Excel_MR_DefaultAction
    {
        [Key]
        public virtual int Acid { get; set; }

        public int Id { get; set; }

        public string Order_ID { get; set; }
        public DateTime? Created_Date { get; set; }

        public string Current_Order_Status { get; set; }
        public string Order_Type { get; set; }
        public string Dispatcher_Notes { get; set; }
        public string IC_Agent_Notes { get; set; }
        public string Order_Description { get; set; }
        public string Problem_Type { get; set; }
        public string Division { get; set; }
        public string Current_Supervisor_Name { get; set; }
        public string Current_Dispatcher { get; set; }
        public string Current_Foreman_Name { get; set; }
        public string Company_Code { get; set; }
        public string Customer_ID { get; set; }
        public string Customer_Tel_1 { get; set; }
        public string Customer_Tel_2 { get; set; }
        public string Customer_Caller_Phone { get; set; }

        public string Functional_Location { get; set; }
        public string PACI_No { get; set; }
        public string PACI_Governorate { get; set; }
        public string PACI_Area { get; set; }
        public string PACI_Street { get; set; }
        public string PACI_Block { get; set; }
        public string SAP_Building { get; set; }
        public string SAP_Floor { get; set; }
        public string SAP_Apartment { get; set; }
        public string Contract_Number { get; set; }
        public string Current_Technician { get; set; }


        public string Contract_Type { get; set; }
        public DateTime? Contract_Creation_Date { get; set; }
        public DateTime? Contract_Expiration_Date { get; set; }
        public DateTime? Closed_Date { get; set; }
        [NotMapped]
        public string Closed_Time { get; set; }
        public DateTime? Progress_Date_History { get; set; }
        [NotMapped]
        public string Progress_Time_History { get; set; }
        public string Executor_History { get; set; }
        public string Action_Type_Name_History { get; set; }
        public string Progress_Status_History { get; set; }
        public string Progress_Sub_Status_History { get; set; }
        public string Technician_Name_History { get; set; }
        public string Dispatcher_Name_History { get; set; }
        public string Survey_Report_History { get; set; }
        public int? AreaId { get; set; }
        public int? DispatcherId { get; set; }
        public int? DivisionId { get; set; }
        public int TypeId { get; set; }
        public int? ProblemId { get; set; }
        public int StatusId { get; set; }
        public int? acStatusId { get; set; }
        public int CompanyCodeId { get; set; }
        public int? WorkingTypeId { get; set; }
        public bool? acTechnicianIsDriver { get; set; }
        public int? acTechnicianId { get; set; }
        public PlatformType? PlatformTypeSource { get; set; }
        public int? LastActionID { get; set; }
        public int? DefaultActionID { get; set; }
    }




}
