﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderSubStatusConfiguration : BaseEntityTypeConfiguration<OrderSubStatus>, IEntityTypeConfiguration<OrderSubStatus>
    {
        public override void Configure(EntityTypeBuilder<OrderSubStatus> builder)
        {
            base.Configure(builder);
            builder.ToTable("OrderSubStatus");
        }
    }
}
