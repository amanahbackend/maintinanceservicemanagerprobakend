﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_WorkingTypeConfiguration : BaseEntityTypeConfiguration<Lang_WorkingType>, IEntityTypeConfiguration<Lang_WorkingType>
    {
        public override void Configure(EntityTypeBuilder<Lang_WorkingType> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_WorkingType");
        }
    }
}