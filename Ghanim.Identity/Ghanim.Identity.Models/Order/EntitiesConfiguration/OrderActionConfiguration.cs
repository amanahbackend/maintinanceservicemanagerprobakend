﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderActionConfiguration : BaseEntityTypeConfiguration<OrderAction>
    {
        public void Configure(EntityTypeBuilder<OrderAction> builder)
        {
            base.Configure(builder);

            builder.HasOne(x => x.Order)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.OrderId);

            builder.HasOne(x => x.Supervisor)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.SupervisorId);

            builder.HasOne(x => x.Dispatcher)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.DispatcherId);

            builder.HasOne(x => x.Foreman)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.ForemanId);

            builder.HasOne(x => x.Team)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.TeamId);

            builder.HasOne(x => x.ExcuterUser)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.FK_CreatedBy_Id);


            builder.HasOne(x => x.CostCenter)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.CostCenterId);

            builder.HasOne(x => x.Status)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.StatusId);


            builder.HasOne(x => x.Technician)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.TechnicianId);



            builder.HasOne(x => x.SubStatus)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.SubStatusId);

            builder.HasOne(x => x.WorkingType)
                .WithMany(x => x.OrderActions)
                .HasForeignKey(x => x.WorkingTypeId);


            builder.ToTable("OrderAction");
        }
    }
}
