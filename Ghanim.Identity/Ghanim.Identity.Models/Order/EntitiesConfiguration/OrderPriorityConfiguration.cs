﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderPriorityConfiguration : BaseEntityTypeConfiguration<OrderPriority>, IEntityTypeConfiguration<OrderPriority>
    {
        public override void Configure(EntityTypeBuilder<OrderPriority> builder)
        {
            base.Configure(builder);
            builder.ToTable("OrderPriority");
        }
    }
}