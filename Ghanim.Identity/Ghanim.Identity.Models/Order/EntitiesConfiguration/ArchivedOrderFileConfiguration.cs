﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class ArchivedOrderFileConfiguration : BaseEntityTypeConfiguration<ArchivedOrderFile>, IEntityTypeConfiguration<ArchivedOrderFile>
    {
        public override void Configure(EntityTypeBuilder<ArchivedOrderFile> builder)
        {
            base.Configure(builder);
            builder.HasIndex(x => x.FileName).IsUnique();
            builder.ToTable("ArchivedOrderFile");
        }
    }
}
