﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class PACIConfiguration : BaseEntityTypeConfiguration<PACI>, IEntityTypeConfiguration<PACI>
    {
        public override void Configure(EntityTypeBuilder<PACI> builder)
        {
            base.Configure(builder);
            builder.HasIndex(x => x.PACINumber).IsUnique();
            builder.Property(x => x.Latitude).HasColumnType("decimal(18,6)");
            builder.Property(x => x.Longtiude).HasColumnType("decimal(18,6)");
            builder.ToTable("PACI");
        }
    }
}
