﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderConfiguration : BaseEntityTypeConfiguration<OrderObject>
    {
        public void Configure(EntityTypeBuilder<OrderObject> builder)
        {
            base.Configure(builder);

            builder.HasOne(x => x.Problem)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.ProblemId);

            builder.HasOne(x => x.Division)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.DivisionId);

            builder.HasOne(x => x.BuildingType)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.BuildingTypeId);

            builder.HasOne(x => x.ContractType)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.ContractTypeId);

            builder.HasOne(x => x.Supervisor)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.SupervisorId);

            builder.HasOne(x => x.Dispatcher)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.DispatcherId);


            builder.HasOne(x => x.Team)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.TeamId);

            builder.HasOne(x => x.Status)
                .WithMany(x => x.OrderObjects)
                .HasForeignKey(x => x.StatusId);


            builder.HasOne(x => x.LastActorTechUser)
                .WithMany(x => x.LastExcutedOrdersByTech)
                .HasForeignKey(x => x.LastActorTechUserId);

            builder.HasOne(x => x.Type)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.TypeId);

            builder.HasOne(x => x.SubStatus)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.SubStatusId);


            builder.HasOne(x => x.Priority)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.PriorityId);


            builder.HasOne(x => x.CompanyCode)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.CompanyCodeId);


            builder.HasOne(x => x.VisitSlot)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.VisitSlotId);

            //builder.Property(x => x.Lat).HasColumnType("decimal(18,6)");
            //builder.Property(x => x.Long).HasColumnType("decimal(18,6)");


            builder.HasIndex(x => x.Code).IsUnique();
            builder.ToTable("Order");
        }
    }
}