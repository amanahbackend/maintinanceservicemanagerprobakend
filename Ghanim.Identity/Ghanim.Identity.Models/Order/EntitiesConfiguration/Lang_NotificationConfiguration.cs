﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_NotificationConfiguration : BaseEntityTypeConfiguration<Lang_Notification>, IEntityTypeConfiguration<Lang_Notification>
    {
        public override void Configure(EntityTypeBuilder<Lang_Notification> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_Notification");
        }
    }
}
