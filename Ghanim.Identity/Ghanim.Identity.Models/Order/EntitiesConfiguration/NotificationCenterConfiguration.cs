﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class NotificationCenterConfiguration : BaseEntityTypeConfiguration<NotificationCenter>
    {
        public void Configure(EntityTypeBuilder<NotificationCenter> builder)
        {
            builder.ToTable("NotificationCenter");
            base.Configure(builder);
        }
    }
}