﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class OrderSettingConfiguration : BaseEntityTypeConfiguration<OrderSetting>
    {
        public void Configure(EntityTypeBuilder<OrderSetting> builder)
        {
            builder.ToTable("Setting");
            base.Configure(builder);
        }
    }
}