﻿
namespace Ghanim.Models.Order.Enums
{
    public enum PlatformType
    {
        Web = 0,
        Mobile = 2,
        Ftp = 1,
    }
}
