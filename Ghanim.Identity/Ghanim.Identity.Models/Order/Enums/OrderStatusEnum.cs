﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Models.Order.Enums
{
    //TODO IMPORTATNT numbers must match the status ids on the database 
    public enum OrderStatusEnum
    {
        Open = 1,
        Dispatched = 2,
        On_Travel = 3,
        Reached = 4,
        Started = 5,
        On_Hold = 6,
        Compelete = 7,
        Cancelled = 8,
    }
}
