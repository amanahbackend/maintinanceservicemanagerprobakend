﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Order.Enums
{

    public enum UserTypesEnum
    {
        Anonymous = 0,
        Admin = 1,
        Supervisor =2,
        Foreman=3,
        Dispatcher=4 ,
        Engineer=5,
        Technician =6,
        Driver=7,
        Manager=8,
        Finance=9
    }
}
