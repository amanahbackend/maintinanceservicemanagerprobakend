﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Models.Order.Enums
{
    public  enum OrderSubstatusEnum
    {
        Released = 1,
        Scheduled = 2,
        Re_Scheduled = 3,
        On_Travel = 4,
        Reached = 5,
        Started = 6,
    }



}
