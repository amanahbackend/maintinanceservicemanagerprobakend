﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface IPACI : IBaseEntity
    {
        string PACINumber { get; set; }
        string AreaName { get; set; }
        int AreaId { get; set; }
        string GovernorateName { get; set; }
        int GovernorateId { get; set; }
        string BlockName { get; set; }
        int BlockId { get; set; }
        string StreetName { get; set; }
        int StreetId { get; set; }
        decimal Longtiude { get; set; }
        decimal Latitude { get; set; }
        string AppartementNumber { get; set; }
        string FloorNumber { get; set; }
        string BuildingNumber { get; set; }
    }
}
