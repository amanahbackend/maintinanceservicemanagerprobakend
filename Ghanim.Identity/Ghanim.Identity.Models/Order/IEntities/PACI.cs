﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface PACI
    {
        string PACINumber { get; set; }
        string Area { get; set; }
        string Block { get; set; }
        string Street { get; set; }
        decimal Longtiude { get; set; }
        decimal Latitude { get; set; }
        string AppartementNumber { get; set; }
        string FloorNumber { get; set; }
    }
}
