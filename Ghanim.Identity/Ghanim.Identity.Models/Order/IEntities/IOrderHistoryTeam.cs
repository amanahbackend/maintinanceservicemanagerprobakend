﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface IOrderHistoryTeam : IBaseEntity 
    {
          int OrderId { get; set; }
          string Code { get; set; }
          int? TeamId { get; set; }
          int? PrevTeamId { get; set; }
    }
}
