﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;


namespace Ghanim.Models.Order.IEntities
{
    public interface IVisitSlot : IBaseEntity
    {   
        string Code { get; set; }
        string SlotDescription { get; set; }

    }
}
