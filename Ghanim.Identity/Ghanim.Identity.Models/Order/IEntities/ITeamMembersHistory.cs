﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Order.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ITeamMembersHistory : IBaseEntity
    {
          string UserId { get; set; }
          int? UserDetailId { get; set; }//tech or dispatcher or driver table id 
          int? TeamId { get; set; }
          int? PrevTeamId { get; set; }
          UserTypesEnum UserTypeId { get; set; }
    }
}