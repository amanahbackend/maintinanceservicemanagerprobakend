﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ILang_RejectionReason : IBaseEntity
    {
        int FK_RejectionReason_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        string Name { get; set; }
    }
}