﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;


namespace Ghanim.Models.Order.IEntities
{
    public interface IOrderSourceCreator : IBaseEntity
    {
        string UserName { get; set; }
        string ImgPath { get; set; }
    }
}
