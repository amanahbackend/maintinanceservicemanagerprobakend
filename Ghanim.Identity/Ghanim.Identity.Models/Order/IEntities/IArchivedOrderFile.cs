﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface IArchivedOrderFile : IBaseEntity
    {
        string FileName { get; set; }
         int Trials { get; set; }

    }
}
