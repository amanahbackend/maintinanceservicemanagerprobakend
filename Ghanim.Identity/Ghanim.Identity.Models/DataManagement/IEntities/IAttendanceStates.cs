﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
   public interface IAttendanceStates : IBaseEntity
    {
         string Name { get; set; }
    }
}
