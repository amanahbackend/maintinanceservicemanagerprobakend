﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface IBuildingTypes : IBaseEntity
    {
        string Name { get; set; }
        string Code { get; set; }
    }
}
