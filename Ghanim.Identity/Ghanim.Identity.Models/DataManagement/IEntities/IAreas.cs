﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface IAreas : IBaseEntity
    {
        int  Area_No { get; set; }
        string Name { get; set; }
        int? Fk_Governorate_Id { get; set; }
        Governorates Governorates { get; set; }
    }
}
