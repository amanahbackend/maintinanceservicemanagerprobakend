﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface IGovernorates : IBaseEntity
    {
        string Name { get; set; }
        int Gov_No { get; set; }
        ICollection<Areas> Areas { get; set; }
    }
}
