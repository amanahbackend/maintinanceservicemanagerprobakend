﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ILang_Availability : IBaseEntity
    {
        int FK_Availability_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        string Name { get; set; }
        Availability Availability { get; set; }
        SupportedLanguages SupportedLanguages { get; set; }
    }
}
