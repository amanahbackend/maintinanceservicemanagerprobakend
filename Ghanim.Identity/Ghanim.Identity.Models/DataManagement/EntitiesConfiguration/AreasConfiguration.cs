﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
   public class AreasConfiguration : BaseEntityTypeConfiguration<Areas>, IEntityTypeConfiguration<Areas>
    {
        public override void Configure(EntityTypeBuilder<Areas> AreasConfiguration)
        {
            base.Configure(AreasConfiguration);

            AreasConfiguration.ToTable("Areas");
            AreasConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            AreasConfiguration.Property(o => o.Area_No).IsRequired();
            AreasConfiguration.Property(o => o.Name).IsRequired();
            AreasConfiguration.Property(o => o.Fk_Governorate_Id).IsRequired(false);
            //AreasConfiguration.Ignore(o => o.Governorates);
        }
    }
}
