﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
   public class AvailabilityConfiguration : BaseEntityTypeConfiguration<Availability>, IEntityTypeConfiguration<Availability>
    {
        public override void Configure(EntityTypeBuilder<Availability> AvailabilityConfiguration)
        {
            base.Configure(AvailabilityConfiguration);

            AvailabilityConfiguration.ToTable("Availability");
            AvailabilityConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            AvailabilityConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
