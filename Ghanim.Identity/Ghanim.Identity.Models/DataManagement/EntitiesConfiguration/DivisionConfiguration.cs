﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
  public class DivisionConfiguration : BaseEntityTypeConfiguration<Division>, IEntityTypeConfiguration<Division>
    {
        public override void Configure(EntityTypeBuilder<Division> DivisionConfiguration)
        {
            base.Configure(DivisionConfiguration);

            DivisionConfiguration.ToTable("Division");
            DivisionConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            DivisionConfiguration.Property(o => o.code).IsRequired();
            DivisionConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
