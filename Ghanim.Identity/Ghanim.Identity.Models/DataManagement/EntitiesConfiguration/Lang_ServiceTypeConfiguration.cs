﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_ServiceTypeConfiguration : BaseEntityTypeConfiguration<Lang_ServiceType>, IEntityTypeConfiguration<Lang_ServiceType>
    {
        public override void Configure(EntityTypeBuilder<Lang_ServiceType> Lang_ServiceTypeConfiguration)
        {
            base.Configure(Lang_ServiceTypeConfiguration);


            Lang_ServiceTypeConfiguration.HasOne(x => x.ServiceType)
                .WithMany(x => x.Lang_ServiceTypes)
                .HasForeignKey(x => x.FK_ServiceType_Id);

            Lang_ServiceTypeConfiguration.HasOne(x => x.SupportedLanguages)
                .WithMany(x => x.Lang_ServiceTypes)
                .HasForeignKey(x => x.FK_SupportedLanguages_ID);


            Lang_ServiceTypeConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_ServiceTypeConfiguration.Property(o => o.Name).IsRequired();
            Lang_ServiceTypeConfiguration.Property(o => o.FK_ServiceType_Id).IsRequired();
            Lang_ServiceTypeConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();

            Lang_ServiceTypeConfiguration.ToTable("Lang_ServiceType");

        }
    }
}
