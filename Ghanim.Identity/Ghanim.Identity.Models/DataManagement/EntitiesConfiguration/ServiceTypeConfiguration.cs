﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
   public class ServiceTypeConfiguration : BaseEntityTypeConfiguration<ServiceType>, IEntityTypeConfiguration<ServiceType>
    {
        public override void Configure(EntityTypeBuilder<ServiceType> ServiceTypeConfiguration)
        {
            base.Configure(ServiceTypeConfiguration);

            ServiceTypeConfiguration.ToTable("ServiceType");
            ServiceTypeConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            ServiceTypeConfiguration.Property(o => o.Name).IsRequired();
            //ServiceTypeConfiguration.Ignore(o => o.Governorates);
        }
    }
}
