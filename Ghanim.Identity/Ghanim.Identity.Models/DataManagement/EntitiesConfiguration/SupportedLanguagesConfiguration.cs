﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
  public  class SupportedLanguagesConfiguration : BaseEntityTypeConfiguration<SupportedLanguages>, IEntityTypeConfiguration<SupportedLanguages>
    {
        public override void Configure(EntityTypeBuilder<SupportedLanguages> SupportedLanguagesConfiguration)
        {
            base.Configure(SupportedLanguagesConfiguration);

            SupportedLanguagesConfiguration.ToTable("SupportedLanguages");
            SupportedLanguagesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            SupportedLanguagesConfiguration.Property(o => o.Code).IsRequired();
            SupportedLanguagesConfiguration.Property(o => o.Name).IsRequired();

        }
    }
}
