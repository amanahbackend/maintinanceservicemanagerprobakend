﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class MapAdminRelatedConfiguration : BaseEntityTypeConfiguration<MapAdminRelated>, IEntityTypeConfiguration<MapAdminRelated>
    {
        public override void Configure(EntityTypeBuilder<MapAdminRelated> MapAdminRelatedConfiguration)
        {
            base.Configure(MapAdminRelatedConfiguration);

            MapAdminRelatedConfiguration.ToTable("MapAdminRelated");
        }

    }
}
