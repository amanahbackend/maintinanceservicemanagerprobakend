﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
  public  class Lang_GovernoratesConfiguration : BaseEntityTypeConfiguration<Lang_Governorates>, IEntityTypeConfiguration<Lang_Governorates>
    {
        public override void Configure(EntityTypeBuilder<Lang_Governorates> Lang_GovernoratesConfiguration)
        {
            base.Configure(Lang_GovernoratesConfiguration);

            Lang_GovernoratesConfiguration.ToTable("Lang_Governorates");
            Lang_GovernoratesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_GovernoratesConfiguration.Property(o => o.Name).IsRequired();
            Lang_GovernoratesConfiguration.Property(o => o.FK_Governorates_ID).IsRequired();
            Lang_GovernoratesConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
        }
    }
}
