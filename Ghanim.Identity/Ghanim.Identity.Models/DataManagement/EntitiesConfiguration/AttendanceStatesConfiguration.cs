﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
   public class AttendanceStatesConfiguration : BaseEntityTypeConfiguration<AttendanceStates>, IEntityTypeConfiguration<AttendanceStates>
    {
        public override void Configure(EntityTypeBuilder<AttendanceStates> AttendanceStatesConfiguration)
        {
            base.Configure(AttendanceStatesConfiguration);

            AttendanceStatesConfiguration.ToTable("AttendanceStates");
            AttendanceStatesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            AttendanceStatesConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
