﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
   public class Lang_DivisionConfiguration : BaseEntityTypeConfiguration<Lang_Division>, IEntityTypeConfiguration<Lang_Division>
    {
        public override void Configure(EntityTypeBuilder<Lang_Division> Lang_DivisionConfiguration)
        {
            base.Configure(Lang_DivisionConfiguration);

            Lang_DivisionConfiguration.ToTable("Lang_Division");
            Lang_DivisionConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_DivisionConfiguration.Property(o => o.Name).IsRequired();
            Lang_DivisionConfiguration.Property(o => o.FK_Division_ID).IsRequired();
            Lang_DivisionConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();
        }
    }
}
