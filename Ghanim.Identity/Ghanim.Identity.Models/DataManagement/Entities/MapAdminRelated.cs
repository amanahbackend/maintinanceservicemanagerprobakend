﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class MapAdminRelated : BaseEntity, IMapAdminRelated
    {
        public string Mobile_Api_Key { get; set; }

        public string Web_Api_Key { get; set; }

        public int Refresh_Rate { get; set; }

    }
}
