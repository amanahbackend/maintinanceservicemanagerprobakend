﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class Shift :BaseEntity// , IShift
    {
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
    }
}
