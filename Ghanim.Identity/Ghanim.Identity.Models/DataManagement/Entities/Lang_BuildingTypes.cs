﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Lang_BuildingTypes : BaseEntity, ILang_BuildingTypes
    {
        public string Name { get; set; }

        public int FK_BuildingTypes_ID { get; set; }
        public virtual BuildingTypes BuildingTypes { get; set; }

        public int FK_SupportedLanguages_ID { get; set; }
        public virtual SupportedLanguages SupportedLanguages { get; set; }
    }
}
