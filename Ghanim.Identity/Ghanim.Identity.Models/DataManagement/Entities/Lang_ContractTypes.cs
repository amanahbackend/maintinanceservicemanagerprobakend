﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Lang_ContractTypes : BaseEntity, ILang_ContractTypes
    {
        public string Name { get; set; }

        public int FK_ContractTypes_ID { get; set; }
        public virtual ContractTypes ContractTypes { get; set; }

        public int FK_SupportedLanguages_ID { get; set; }
        public virtual SupportedLanguages SupportedLanguages { get; set; }
    }
}
