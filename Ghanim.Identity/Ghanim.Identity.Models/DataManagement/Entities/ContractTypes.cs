﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class ContractTypes : BaseEntity, IContractTypes
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual ICollection<Lang_ContractTypes> Lang_ContractTypes { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }
    }
}
