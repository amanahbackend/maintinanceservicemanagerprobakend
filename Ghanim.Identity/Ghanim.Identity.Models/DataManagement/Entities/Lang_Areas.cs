﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Lang_Areas : BaseEntity, ILang_Areas
    {
        public string Name { get; set; }

        public int FK_Area_ID { get; set; }
        public virtual Areas Areas { get; set; }

        public int FK_SupportedLanguages_ID { get; set; }
        public virtual SupportedLanguages SupportedLanguages { get; set; }
    }
}
