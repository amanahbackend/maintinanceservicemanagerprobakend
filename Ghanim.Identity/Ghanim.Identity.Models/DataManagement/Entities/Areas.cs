﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Areas : BaseEntity, IAreas
    {
        public int Area_No { get; set; }
        public string Name { get; set; }

        public int? Fk_Governorate_Id { get; set; }
        [ForeignKey("Fk_Governorate_Id")]
        public virtual Governorates Governorates { get; set; }

        public virtual ICollection<DispatcherSettings> DispatcherSettings { get; set; }
        public virtual ICollection<Lang_Areas> Lang_Areas { get; set; }
    }
}
