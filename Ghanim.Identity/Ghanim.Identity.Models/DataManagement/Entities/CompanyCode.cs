﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class CompanyCode :BaseEntity , ICompanyCode
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual ICollection<Lang_CompanyCode> Lang_CompanyCodes { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }



        
    }
}
