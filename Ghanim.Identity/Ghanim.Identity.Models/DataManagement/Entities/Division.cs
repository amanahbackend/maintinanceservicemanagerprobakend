﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Division : BaseEntity, IDivision
    {
        public string Name { get; set; }
        public string code { get; set; }

        public virtual ICollection<Supervisor> Supervisors { get; set; }
        public virtual ICollection<Dispatcher> Dispatchers { get; set; }
        public virtual ICollection<Foreman> Foremans { get; set; }
        public virtual ICollection<Engineer> Engineers { get; set; }
        public virtual ICollection<DispatcherSettings> DispatcherSettings { get; set; }
        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<Technician> Technicians { get; set; }

        public virtual ICollection<Lang_Division> Lang_Divisions { get; set; }
        public virtual ICollection<OrderObject> Orders { get; set; }
        public virtual ICollection<TeamMember> Members { get; set; }
    }
}
