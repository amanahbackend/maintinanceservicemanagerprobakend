﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class CostCenter : BaseEntity, ICostCenter
    {
        public string Name { get; set; }

        public virtual ICollection<Supervisor> Supervisors { get; set; }
        public virtual ICollection<Dispatcher> Dispatchers { get; set; }
        public virtual ICollection<Foreman> Foremans { get; set; }
        public virtual ICollection<Engineer> Engineers { get; set; }
        public virtual ICollection<Manager> Managers { get; set; }
        public virtual ICollection<Technician> Technicians { get; set; }
        public virtual ICollection<OrderAction> OrderActions { get; set; }

        public virtual ICollection<Lang_CostCenter> Lang_CostCenters { get; set; }

        
    }

}
