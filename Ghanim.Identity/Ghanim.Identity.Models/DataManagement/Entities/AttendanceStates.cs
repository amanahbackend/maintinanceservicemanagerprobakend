﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class AttendanceStates : BaseEntity, IAttendanceStates
    {
        public string Name { get; set; }

        public virtual ICollection<Lang_AttendanceStates> Lang_AttendanceStates { get; set; }
    }
}
