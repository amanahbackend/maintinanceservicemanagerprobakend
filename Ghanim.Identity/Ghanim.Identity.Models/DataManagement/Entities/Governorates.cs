﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Governorates : BaseEntity , IGovernorates
    {
        public string Name { get; set; }
        public int Gov_No { get; set; }
        public virtual ICollection<Areas> Areas {get; set; }
        public virtual ICollection<Lang_Governorates> Lang_Governorates { get; set; }
    }
}
