﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
    public class ServiceType : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }

        public virtual ICollection<Lang_ServiceType> Lang_ServiceTypes { get; set; }
    }
}
