﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Lang_Shift : BaseEntity, ILang_Shift
    {
        public int FK_Shift_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }


        [NotMapped]
        public TimeSpan FromTime
        {
            get => FromTimeValue - DateTime.MinValue;
            set => FromTimeValue = DateTime.MinValue + value;
        }

        [NotMapped]
        public TimeSpan ToTime
        {
            get => ToTimeValue - DateTime.MinValue;
            set => ToTimeValue = DateTime.MinValue + value;
        }

        public DateTime FromTimeValue { get; set; }
        public DateTime ToTimeValue { get; set; }


        [NotMapped]
        public Shift Shift { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
