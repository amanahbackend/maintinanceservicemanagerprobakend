﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
  public  class SupportedLanguages : BaseEntity , ISupportedLanguages
    {
        public string Name { get; set; }
        public string Code { get; set; }


        public virtual ICollection<Lang_Areas> Lang_Areas { get; set; }
        public virtual ICollection<Lang_AttendanceStates> Lang_AttendanceStates { get; set; }
        public virtual ICollection<Lang_Availability> Lang_Availability { get; set; }
        public virtual ICollection<Lang_BuildingTypes> Lang_BuildingTypes { get; set; }
        public virtual ICollection<Lang_CompanyCode> Lang_CompanyCodes { get; set; }
        public virtual ICollection<Lang_ContractTypes> Lang_ContractTypes { get; set; }
        public virtual ICollection<Lang_CostCenter> Lang_CostCenters { get; set; }
        public virtual ICollection<Lang_Division> Lang_Divisions { get; set; }
        public virtual ICollection<Lang_Governorates> Lang_Governorates { get; set; }
        public virtual ICollection<Lang_ServiceType> Lang_ServiceTypes { get; set; }
        
        
    }
}
