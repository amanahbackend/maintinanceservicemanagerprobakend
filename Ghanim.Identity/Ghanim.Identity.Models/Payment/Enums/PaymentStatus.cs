﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Payment.Enums
{
    public enum PaymentStatusEnum
    {
        Pending = 0,
        Completed = 1
    }
}
