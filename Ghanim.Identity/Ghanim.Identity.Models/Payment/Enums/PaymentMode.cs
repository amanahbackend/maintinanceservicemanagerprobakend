﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Payment.Enums
{
    public enum PaymentModeEnum
    {
        Cash = 1,
        Online = 2
    }
}
