﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using Ghanim.Models.Payment.Enums;

namespace Ghanim.Models.Payment.Entities
{
    public class PaymentTransaction : BaseEntity
    {
        public PaymentModeEnum PaymentMode { get; set; }
        //Status in case online: (the online payment status to come from KNPAY)
        public PaymentStatusEnum PaymentStatus { get; set; }
        public string PaymentMethod { get; set; }

        //the money that customer payed on this payment
        public decimal Payment  { get; set; }


        public string Receipt { get; set; }

        //Manual receipt number* (entered manually by technician)
        public string ManualReceiptNumber { get; set; }


        public decimal OfferDiscount { get; set; }


        public string KNPAYPaymentRefNumber { get; set; }
        public string KNPAYAuthorizationCode { get; set; }
        public string KNPAYTransactionID { get; set; }
        public string KNPAYTrackID { get; set; }


        public int? OrderId { get; set; }
        public virtual OrderObject Order { get; set; }
   


        public int OrderHistoryStatusId { get; set; }
        public virtual OrderStatus OrderHistoryStatus { get; set; }


        //exuter userId
        //ExcuterUser // created by
        [ForeignKey("FK_CreatedBy")]
        public override string FK_CreatedBy_Id { get; set; }
        [ForeignKey("FK_CreatedBy_Id")]
        public virtual ApplicationUser FK_CreatedBy { get; set; }


        //Dispatcher id during payment
        public int? CurrentDispatcherId { get; set; }
        public virtual Dispatcher CurrentDispatcher { get; set; }


        //supervisor id during payment
        public int? CurrentSupervisorId { get; set; }
        public virtual Supervisor CurrentSupervisor { get; set; }

        //last tech had interaction with order before payment
        public int? CurrentTechnicianId { get; set; }
        public virtual Technician CurrentTechnician { get; set; } 

        //foreman at payment time
        public int? CurrentForemanId { get; set; }
        public virtual Foreman CurrentForeman { get; set; }
        //master data
        public int ServiceTypeId { get; set; }
        public virtual ServiceType ServiceType { get; set; } 
      
    }
}
