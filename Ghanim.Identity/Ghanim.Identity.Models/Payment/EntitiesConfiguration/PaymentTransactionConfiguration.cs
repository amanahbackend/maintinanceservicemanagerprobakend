﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    class PaymentTransactionConfiguration : BaseEntityTypeConfiguration<PaymentTransaction>
    {
        public void Configure(EntityTypeBuilder<PaymentTransaction> builder)
        {
            builder.HasOne(x => x.Order)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.OrderId);

            builder.HasOne(x => x.OrderHistoryStatus)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.OrderHistoryStatusId);


            builder.HasOne(x => x.FK_CreatedBy)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.FK_CreatedBy_Id);

            builder.HasOne(x => x.CurrentDispatcher)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.CurrentDispatcherId);


            builder.HasOne(x => x.CurrentSupervisor)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.CurrentSupervisorId);


            builder.HasOne(x => x.CurrentTechnician)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.CurrentTechnicianId);


            builder.HasOne(x => x.CurrentForeman)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.CurrentForemanId);


            builder.HasOne(x => x.ServiceType)
                .WithMany(x => x.PaymentItems)
                .HasForeignKey(x => x.ServiceTypeId);

            builder.ToTable("PaymentItem");
            base.Configure(builder);
        }
    }
}
