﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Order.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Ghanim.Models.Payment.Entities;

namespace Ghanim.Models.Entities
{
    public class ApplicationUser : IdentityUser, IIdentityBaseEntity
    {
        public ApplicationUser() : base()
        {

        }
        public ApplicationUser(string username) : base(username)
        {

        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PF { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool Deactivated { get; set; }
        public string PicturePath { get; set; }
        public bool IsFirstLogin { get; set; }
        //public int? LanguageName { get; set; }
        public int LanguageId { get; set; }
        [NotMapped]
        public List<string> RoleNames { get; set; }
        [NotMapped]
        public string Picture { get; set; }
        public UserTypesEnum UserTypeId { get; set; } 
        public virtual Supervisor Supervisor { get; set; }
        public virtual Manager Manager { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }
        public virtual Foreman Foreman { get; set; }
        public virtual Engineer Engineer { get; set; }
        public virtual Technician Technician { get; set; }
        public virtual TeamMember TeamMember { get; set; }
        public virtual Finance Finance { get; set; }

        


        public virtual ICollection<OrderAction> OrderActions { get; set; }
        public virtual ICollection<OrderObject> LastExcutedOrdersByTech { get; set; }
        public virtual ICollection<PaymentTransaction> PaymentItems { get; set; }



        //public virtual ICollection<Contract> SBMUserContracts { get; set; }


        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

    }
}
