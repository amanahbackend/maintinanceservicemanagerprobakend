﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class PasswordTokenPin
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Pin { get; set; }
    }
}
