﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Identity.Static
{
    public static class StaticRoles
    {
        public const string Engineer = "Engineer";
        public const string Supervisor = "Supervisor";
        public const string Admin = "Admin";
        public const string Foreman = "Foreman";
        public const string Technician = "Technician";
        public const string Dispatcher = "Dispatcher";
        public const string Manager = "Manager";

        public const string Supervisor_or_Dispatcher = Supervisor + "," + Dispatcher;
        public const string Closed = "Closed";
    }
}
