﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;

namespace Ghanim.Models.EntitiesConfiguration
{
    public class ApplicationUserTypeConfiguration
        : IdentityBaseEntityTypeConfiguration<ApplicationUser>, IEntityTypeConfiguration<ApplicationUser>
    {
        public new void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.FirstName).IsRequired(true);
            builder.Property(u => u.LastName).IsRequired(true);
            builder.Property(u => u.Phone1).IsRequired(false);
            builder.Property(u => u.Phone2).IsRequired(false);
            builder.Property(u => u.PicturePath).IsRequired(false);
            //builder.Property(u => u.IsFirstLogin).IsRequired(false);
           // builder.Property(u => u.LanguageName).IsRequired(false);
        }
    }
}
