﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_MachineTypeConfiguration : BaseEntityTypeConfiguration<Lang_MachineType>, IEntityTypeConfiguration<Lang_MachineType>
    {
        public override void Configure(EntityTypeBuilder<Lang_MachineType> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_MachineType");
        }
    }
}
