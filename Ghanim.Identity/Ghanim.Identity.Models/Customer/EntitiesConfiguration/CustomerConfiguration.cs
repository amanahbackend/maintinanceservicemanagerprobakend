﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class CustomerConfiguration : BaseEntityTypeConfiguration<CustomerObject>
    {
        public void Configure(EntityTypeBuilder<CustomerObject> builder)
        {
            base.Configure(builder);
            builder.ToTable("Customer");

            builder.HasOne(x => x.CustomerType)
               .WithMany(x => x.customers)
               .HasForeignKey(x => x.FK_CustomerType_Id);

            builder.HasOne(x => x.MachineType)
               .WithMany(x => x.Customers)
               .HasForeignKey(x => x.FK_MachineType_Id);

        }
    }
}
