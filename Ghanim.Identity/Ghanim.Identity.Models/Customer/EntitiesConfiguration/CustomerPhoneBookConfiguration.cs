﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class CustomerPhoneBookConfiguration : BaseEntityTypeConfiguration<CustomerPhoneBook>
    {
        public void Configure(EntityTypeBuilder<CustomerPhoneBook> builder)
        {
            base.Configure(builder);
            builder.ToTable("CustomerPhoneBook");

            builder.HasOne(x => x.Customer)
               .WithMany(x => x.CustomerPhoneBook)
               .HasForeignKey(x => x.FK_Customer_Id);

            builder.HasOne(x => x.PhoneType)
               .WithMany(x => x.CustomerPhoneBooks)
               .HasForeignKey(x => x.FK_PhoneType_Id);

        }
    }
}
