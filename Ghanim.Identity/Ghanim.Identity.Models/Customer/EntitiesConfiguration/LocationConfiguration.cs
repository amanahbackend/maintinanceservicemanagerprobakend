﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class LocationConfiguration : BaseEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            base.Configure(builder);
            builder.ToTable("Location");

            builder.HasOne(x => x.Customer)
               .WithMany(x => x.Locations)
               .HasForeignKey(x => x.Fk_Customer_Id);

        }
    }
}
