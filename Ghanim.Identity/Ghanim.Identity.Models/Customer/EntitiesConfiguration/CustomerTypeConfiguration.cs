﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class CustomerTypeConfiguration : BaseEntityTypeConfiguration<CustomerType>
    {
        public void Configure(EntityTypeBuilder<CustomerType> builder)
        {
            base.Configure(builder);
            builder.ToTable("CustomerType");

        }
    }
}

