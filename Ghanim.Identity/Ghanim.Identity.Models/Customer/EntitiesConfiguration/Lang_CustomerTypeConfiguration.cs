﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_CustomerTypeConfiguration : BaseEntityTypeConfiguration<Lang_CustomerType>, IEntityTypeConfiguration<Lang_CustomerType>
    {
        public override void Configure(EntityTypeBuilder<Lang_CustomerType> builder)
        {
            base.Configure(builder);
            builder.ToTable("Lang_CustomerType");
        }
    }
}
