﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class CustomerObject : BaseEntity, ICustomer
    {

        public string CuatomerId { get; set; }

        public string Name { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public string Email { get; set; }

        public string AccountNo { get; set; }

        public string CustomerSiteName { get; set; }

        public string SiteCode { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public int FK_MachineType_Id { get; set; }


        public virtual MachineType MachineType { get; set; }

        public virtual CustomerType CustomerType { get; set; }


        public virtual ICollection<CustomerPhoneBook> CustomerPhoneBook { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

    }
}
