﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class PhoneType : BaseEntity, IPhoneType
    {
        public string Name { get; set; }
        public bool IsDefault { set; get; }
        public virtual ICollection<CustomerPhoneBook> CustomerPhoneBooks { get; set; }
    }
}
