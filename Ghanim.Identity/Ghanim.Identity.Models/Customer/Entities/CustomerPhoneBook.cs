﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
   public class CustomerPhoneBook : BaseEntity, ICustomerPhoneBook
    {
        public int FK_Customer_Id { get; set; }

        public string Phone { get; set; }

        public int FK_PhoneType_Id { get; set; }

        public virtual PhoneType PhoneType { get; set; }

        public virtual CustomerObject Customer { get; set; }
    }
}
