﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class Location : BaseEntity, ILocation
    {
        public string PACINumber { get; set; }
        public string Title { get; set; }

        public string Governorate { get; set; }

        public string Area { get; set; }

        public string Block { get; set; }

        public string Street { get; set; }

        public string AddressNote { get; set; }

        public string Building { set; get; }

        public string AppartmentNo { set; get; }

        public string FloorNo { set; get; }

        public string HouseNo { set; get; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string FileNo { get; set; }
        public string CusSiteNo { get; set; }
        public int Fk_Customer_Id { get; set; }

        public virtual CustomerObject Customer { get; set; }

    }
}
