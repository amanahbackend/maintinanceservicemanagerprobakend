﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.Entities
{
    public class CustomerType : BaseEntity, ICustomerType
    {
        public string Name { get; set; }
        public virtual ICollection<CustomerObject> customers { get; set; }


    }
}
