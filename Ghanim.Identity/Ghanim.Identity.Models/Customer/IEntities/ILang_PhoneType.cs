﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ILang_PhoneType : IBaseEntity
    {
        int FK_PhoneType_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        string Name { get; set; }

    }
}
