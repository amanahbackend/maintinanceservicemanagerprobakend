﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ILocation : IBaseEntity
    {

        string PACINumber { get; set; }
        string Title { get; set; }

        string Governorate { get; set; }

        string Area { get; set; }

        string Block { get; set; }

        string Street { get; set; }

        string AddressNote { get; set; }

        string Building { set; get; }

        string AppartmentNo { set; get; }

        string FloorNo { set; get; }

        string HouseNo { set; get; }

        double Latitude { get; set; }

        double Longitude { get; set; }

         string FileNo { get; set; }
         string CusSiteNo { get; set; }
        int Fk_Customer_Id { get; set; }


    }
}
