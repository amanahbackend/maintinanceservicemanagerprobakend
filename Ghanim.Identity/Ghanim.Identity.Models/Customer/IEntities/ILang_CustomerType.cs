﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ILang_CustomerType : IBaseEntity
    {
        int FK_CustomerType_ID { get; set; }
        int FK_SupportedLanguages_ID { get; set; }
        string Name { get; set; }
    }
}
