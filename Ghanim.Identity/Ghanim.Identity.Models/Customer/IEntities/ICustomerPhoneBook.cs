﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
    public interface ICustomerPhoneBook : IBaseEntity
    {
        int FK_Customer_Id { get; set; }

        string Phone { get; set; }

        int FK_PhoneType_Id { get; set; }
    }
}
