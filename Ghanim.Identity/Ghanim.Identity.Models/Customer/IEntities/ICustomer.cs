﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Models.IEntities
{
     public interface ICustomer : IBaseEntity
    {
        string CuatomerId { get; set; }

        string Name { get; set; }


        string CivilId { get; set; }

        string Remarks { get; set; }

        string Email { get; set; }

        string AccountNo { get; set; }

        string CustomerSiteName { get; set; }
        
        string SiteCode { get; set; }

        int FK_CustomerType_Id { get; set; }

        int FK_MachineType_Id { get; set; }

    //    MachineType MachineType { get; set; }

    //    CustomerType CustomerType { get; set; }



    //    List<Complain> Complains { get; set; }

    //    List<CustomerPhoneBook> CustomerPhoneBook { get; set; }

    //    List<Location> Locations { get; set; }
    }
}
