﻿using DispatchProduct.RepositoryModule;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Managers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using IdentityServer4.Services;
using Ghanim.API.Services;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using DnsClient;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Text;
//using BuildingBlocks.ServiceDiscovery;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Ghanim.API.Schedulers.TaskFlow;
using Ghanim.API.Schedulers;
using Ghanim.API;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Managers;
using Ghanim.Models.Entities;
using Ghanim.Models.IEntities;
using Ghanim.API.Helper;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.Order;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Managers;
using Ghanim.Models.Entities;
using Ghanim.Models.IEntities;
using Ghanim.API.Controllers;
using Ghanim.API.Helper;
using Ghanim.API.Schedulers;
using Ghanim.API.Schedulers.Scheduling;
using Ghanim.API.Schedulers.TaskFlow;
using Ghanim.API.ServiceCommunications;
using Ghanim.API.ServiceCommunications.DataManagementService;
using Ghanim.API.ServiceCommunications.DataManagementService.BuildingTypesService;
using Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using Ghanim.API.ServiceCommunications.DataManagementService.ContractTypesService;
using Ghanim.API.ServiceCommunications.DataManagementService.DivisionService;
using Ghanim.API.ServiceCommunications.DataManagementService.GovernoratesService;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.DataManagementService.Problem;
using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Ghanim.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Dispatching.Location.API;
using Dispatching.Location.API.ServiceCommunications.Order;
using Ghanim.API.Caching;
using Ghanim.API.Caching.Abstracts;
using Ghanim.API.Caching.BL;
using Ghanim.API.Caching.CachedTablesContainers;
using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.API.ServiceCommunications.FlixyApi;
using Ghanim.API.ServiceCommunications.GoogleApi;
using Ghanim.API.Utilities.HangFireServices;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.ExcelSettings;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Managers;
using Ghanim.BLL.Services.FlixyApi;
using Ghanim.BLL.Settings;
using Ghanim.Models.Entities;
using Ghanim.Models.IEntities;
using Hangfire;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Logging;
using Ghanim.ResourceLibrary;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Ghanim.API.ServiceCommunications.Payment;
using Ghanim.BLL.Order.Managers;

namespace Ghanim.API
{
    public class Startup
    {
        public IServiceCollection _services { get; set; }

        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;
        public Startup(IHostingEnvironment env)
        {

            _env = env;
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;
            StaticAppSettings.ConnectionString = Configuration.GetSection("ConnectionString").Get<string>();
            StaticAppSettings.ExcludedStatusIds = Configuration.GetSection("ExcludeOrderActions:ExcludedIds").Get<List<int>>();
            StaticAppSettings.ExcludedStatusNames = Configuration.GetSection("ExcludeOrderActions:ExcludedNames").Get<List<string>>();

            //localization
            services.AddLocalization(o => { o.ResourcesPath = "Resources"; });
            services.Configure<RequestLocalizationOptions>(options =>
            {
                CultureInfo[] supportedCultures = new[]
                {
                    new CultureInfo("en"),
                     
                }; 
                options.DefaultRequestCulture = new RequestCulture("en");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures; 
            });
            //end localization

            services.Configure<SMSKey>(Configuration.GetSection("SMS"));

            services.Configure<GoogleApiSettings>(Configuration.GetSection("ServiceCommunications:GoogleApiService"));
            services.Configure<FlixyApiSettings>(Configuration.GetSection("ServiceCommunications:FlixyApiSettings"));
            services.Configure<StaticFilesSettings>(Configuration.GetSection("StaticFiles"));

            


            // End Service communication section for user management

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddSingleton(provider => Configuration);

            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options
                    .UseLazyLoadingProxies()
                    .ConfigureWarnings(warnings => warnings.Ignore(CoreEventId.DetachedLazyLoadingWarning))
                    .UseSqlServer(Configuration["ConnectionString"],
                    sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.EFCore.MSSQL").CommandTimeout(300)));

            services.AddIdentity<ApplicationUser, ApplicationRole>(opt =>
            {
                opt.User.RequireUniqueEmail = true;
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 1;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAutoMapper(typeof(Startup));
            //Mapper.AssertConfigurationIsValid();

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });


            #region Data & Managers For UserManagement
            services.AddScoped<DbContext, ApplicationDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(ICashableRepository<,>), typeof(CashableRepository<,>));
            
            //services.AddScoped(typeof(ICashableRepository<,>), typeof(CashableRepository<,>));

            services.AddScoped(typeof(IProcessResultMapper), typeof(ProcessResultMapper));
            services.AddScoped(typeof(IProcessResultPaginatedMapper), typeof(ProcessResultPaginatedMapper));
            services.AddScoped(typeof(IPaginatedItems<>), typeof(PaginatedItems<>));

            services.AddScoped(typeof(ISupervisor), typeof(Supervisor));
            services.AddScoped(typeof(IDispatcher), typeof(Dispatcher));
            services.AddScoped(typeof(IDispatcherSettings), typeof(DispatcherSettings));
            services.AddScoped(typeof(IForeman), typeof(Foreman));
            services.AddScoped(typeof(ITechnician), typeof(Technician));
            services.AddScoped(typeof(IDriver), typeof(Driver));
            services.AddScoped(typeof(IEngineer), typeof(Engineer));
            services.AddScoped(typeof(ITeamMember), typeof(TeamMember));
            services.AddScoped(typeof(ITeam), typeof(Team));
            services.AddScoped(typeof(IAttendance), typeof(Attendence));
            services.AddScoped(typeof(IManager), typeof(Manager));
            services.AddScoped(typeof(IMaterialController), typeof(MaterialController));
            services.AddScoped(typeof(ITechniciansState), typeof(TechniciansState));
            services.AddScoped(typeof(IVehicle), typeof(Vehicle));

            services.AddScoped(typeof(ISupervisorManager), typeof(SupervisorManager));
            services.AddScoped(typeof(IDispatcherManager), typeof(DispatcherManager));
            services.AddScoped(typeof(IForemanManager), typeof(ForemanManager));
            services.AddScoped(typeof(ITechnicianManager), typeof(TechnicianManager));
            services.AddScoped(typeof(IDriverManager), typeof(DriverManager));
            services.AddScoped(typeof(IFinanceManager), typeof(FinanceManager));
            services.AddScoped(typeof(IEngineerManager), typeof(EngineerManager));
            services.AddScoped(typeof(ITeamMemberManager), typeof(TeamMemberManager));
            services.AddScoped(typeof(ITeamManager), typeof(TeamManager));
            services.AddScoped(typeof(IAttenenceManager), typeof(AttendenceManager));
            services.AddScoped(typeof(IDispatcherSettingsManager), typeof(DispatcherSettingsManager));
            services.AddScoped(typeof(IManagerManager), typeof(ManagerManager));
            services.AddScoped(typeof(IMaterialControllerManager), typeof(MaterialControllerManager));
            services.AddScoped(typeof(ITechniciansStateManager), typeof(TechniciansStateManager));
            services.AddScoped(typeof(IVehicleManager), typeof(VehicleManager));
            services.AddScoped(typeof(IAPIHelper), typeof(APIHelper));
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IFlixyService, FlixyService>();
            services.AddScoped<IPACIService, PACIService>();

            //services.AddScoped(typeof(IOrderService), typeof(DispatcherManager));            
            #endregion








            //services.AddIdentityServer();
            //services.Configure<VehicleAppSettings>(Configuration);


            services.AddScoped(typeof(ApplicationRole), typeof(ApplicationRole));
            services.AddScoped(typeof(PasswordTokenPin), typeof(PasswordTokenPin));

            services.AddScoped(typeof(IApplicationRoleManager), typeof(ApplicationRoleManager));
            services.AddScoped(typeof(IApplicationUserManager), typeof(ApplicationUserManager));
            services.AddScoped(typeof(IApplicationUserHistoryManager), typeof(ApplicationUserHistoryManager));
            services.AddScoped(typeof(IPasswordTokenPinManager), typeof(PasswordTokenPinManager));
            services.AddScoped(typeof(IJunkUserManager), typeof(JunkUserManager));
            services.AddScoped(typeof(IPrivilegeManager), typeof(PrivilegeManager));
            services.AddScoped(typeof(IRolePrivilegeManager), typeof(RolePrivilegeManager));
            services.AddScoped(typeof(IUserDeviceManager), typeof(UserDeviceManager));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddScoped(typeof(IUploadImageFileManager), typeof(UploadImageFileManager));
            //services.AddScoped(typeof(IHttpContextAccessor), typeof(HttpContextAccessor));
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<LogoutFlow>();


            services.AddHostedService<LogoutTask>();
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration["ConnectionString"]));

            // injection for datamanagement
            services.AddScoped(typeof(IAreas), typeof(Areas));
            services.AddTransient(typeof(IAttendanceStates), typeof(AttendanceStates));
            services.AddTransient(typeof(IAvailability), typeof(Availability));
            services.AddScoped(typeof(IBuildingTypes), typeof(BuildingTypes));
            services.AddScoped(typeof(ICompanyCode), typeof(CompanyCode));
            services.AddScoped(typeof(Expression<>), typeof(Expression<>));
            services.AddScoped(typeof(Func<>), typeof(Func<>));
            services.AddScoped(typeof(IContractTypes), typeof(ContractTypes));
            services.AddTransient(typeof(IDivision), typeof(Division));
            services.AddScoped(typeof(IGovernorates), typeof(Governorates));
            services.AddScoped(typeof(ILang_Areas), typeof(Lang_Areas));
            services.AddScoped(typeof(ILang_AttendanceStates), typeof(Lang_AttendanceStates));
            services.AddScoped(typeof(ILang_Availability), typeof(Lang_Availability));
            services.AddScoped(typeof(ILang_BuildingTypes), typeof(Lang_BuildingTypes));
            services.AddScoped(typeof(ILang_CompanyCode), typeof(Lang_CompanyCode));
            services.AddScoped(typeof(ILang_ContractTypes), typeof(Lang_ContractTypes));
            services.AddScoped(typeof(ILang_Division), typeof(Lang_Division));
            services.AddScoped(typeof(ILang_Governorates), typeof(Lang_Governorates));
            services.AddScoped(typeof(ILang_Shift), typeof(Lang_Shift));
            services.AddScoped(typeof(IShift), typeof(Shift));
            services.AddScoped(typeof(ISupportedLanguages), typeof(SupportedLanguages));
            services.AddScoped(typeof(ILang_CostCenter), typeof(Lang_CostCenter));


            services.AddScoped(typeof(IMapAdminRelated), typeof(MapAdminRelated));


            services.AddScoped(typeof(IAreasManager), typeof(AreasManager));
            services.AddScoped(typeof(IAttendanceStatesManager), typeof(AttendanceStatesManager));
            services.AddScoped(typeof(IAvailabilityManager), typeof(AvailabilityManager));
            services.AddScoped(typeof(IBuildingTypesManager), typeof(BuildingTypesManager));
            services.AddScoped(typeof(ICompanyCodeManager), typeof(CompanyCodeManager));
            services.AddScoped(typeof(IContractTypesManager), typeof(ContractTypesManager));
            services.AddScoped(typeof(IDivisionManager), typeof(DivisionManager));
            services.AddScoped(typeof(IGovernoratesManager), typeof(GovernoratesManager));
            services.AddScoped(typeof(ILang_AreasManager), typeof(Lang_AreasManager));
            services.AddScoped(typeof(ILang_AttendanceStatesManager), typeof(Lang_AttendanceStatesManager));
            services.AddScoped(typeof(ILang_AvailabilityManager), typeof(Lang_AvailabilityManager));
            services.AddScoped(typeof(ILang_BuildingTypesManager), typeof(Lang_BuildingTypesManager));
            services.AddScoped(typeof(ILang_CompanyCodeManager), typeof(Lang_CompanyCodeManager));
            services.AddScoped(typeof(ILang_ContractTypesManager), typeof(Lang_ContractTypesManager));
            services.AddScoped(typeof(ILang_DivisionManager), typeof(Lang_DivisionManager));
            services.AddScoped(typeof(ILang_GovernoratesManager), typeof(Lang_GovernoratesManager));
            services.AddScoped(typeof(ILang_ShiftManager), typeof(Lang_ShiftManager));
            services.AddScoped(typeof(IShiftManager), typeof(ShiftManager));
            services.AddScoped(typeof(ISupportedLanguagesManager), typeof(SupportedLanguagesManager));
            services.AddScoped(typeof(ICostCenterManager), typeof(CostCenterManager));
            services.AddScoped(typeof(ILang_CostCenterManager), typeof(Lang_CostCenterManager));
            services.AddScoped(typeof(IPaymentTransactionManager), typeof(PaymentTransactionManager));
            services.AddScoped(typeof(IPaymentTransactionService), typeof(PaymentTransactionService));


            



            services.AddScoped(typeof(IMapAdminRelatedManager), typeof(MapAdminRelatedManager));


            services.AddScoped<IDispatcherService, DispatcherService>();
            services.AddScoped<IDispatcherSettingsService, DispatcherSettingsService>();
            services.AddScoped<ISupervisorService, SupervisorService>();



            services.AddHangfire(x => x.UseSqlServerStorage(Configuration["ConnectionString"]));



            services.AddHttpContextAccessor();
            //end in jection for data management


            services.AddIdentityServer()
               .AddDeveloperSigningCredential()
               // this adds the config data from DB (clients, resources)
               .AddAspNetIdentity<ApplicationUser>()
               .AddConfigurationStore(options =>
               {
                   options.ConfigureDbContext = builder =>
                       builder.UseSqlServer(Configuration["ConnectionString"],
                           sql => sql.MigrationsAssembly("Ghanim.EFCore.MSSQL"));
               })
               // this adds the operational data from DB (codes, tokens, consents)
               .AddOperationalStore(options =>
               {
                   options.ConfigureDbContext = builder =>
                       builder.UseSqlServer(Configuration["ConnectionString"],
                           sql => sql.MigrationsAssembly("Ghanim.EFCore.MSSQL"));

                   // this enables automatic token cleanup. this is optional.
                   options.EnableTokenCleanup = true;
                   options.TokenCleanupInterval = 3600;

               })
               .Services.AddTransient<IProfileService, IdentityProfileService>();
            var key = Encoding.ASCII.GetBytes("2dfcdfebc9f0037512964af4f0939bc715616e6609574bf43665e19f9cbc1ecd");
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters()
                {
                    //ValidIssuer = Configuration["JwtSecurityToken:Issuer"],
                    //ValidAudience = Configuration["JwtSecurityToken:Audience"],
                    //ValidateIssuerSigningKey = true,
                    ////IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityToken:Key"])),
                    //IssuerSigningKey = new SymmetricSecurityKey(key),
                    //ValidateLifetime = true

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            //////////////////// commented the part of ServiceRegisteryPort  ////////////////////////////
            //// Dns Query for consul - service discovery
            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            //services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));
            ///////////////////////////////////////////////////////////////////////////////////////


            //services.AddApiVersioning(o => o.ReportApiVersions = true);

            //services.AddScoped(typeof(IIdentityService), typeof(IdentityService));



            services.AddSession();

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });


            FileUploadSettings.FileExtention = Configuration.GetSection("OrderFileUpload:FileExtention").Value;
            FileUploadSettings.ReadingPeriod = Configuration.GetSection("OrderFileUpload:ReadingPeriod").Value;
            FileUploadSettings.SourceFilePath = Configuration.GetSection("OrderFileUpload:SourceFilePath").Value;
            FileUploadSettings.TargetFilePath = Configuration.GetSection("OrderFileUpload:TargetFilePath").Value;
            FileUploadSettings.HostName = Configuration.GetSection("OrderFileUpload:HostName").Value;
            FileUploadSettings.Username = Configuration.GetSection("OrderFileUpload:Username").Value;
            FileUploadSettings.Password = Configuration.GetSection("OrderFileUpload:Password").Value;
            FileUploadSettings.ConnectionString = Configuration.GetSection("ConnectionString").Value;
            FileUploadSettings.proxyUrl = Configuration.GetSection("Location:ProxyUrl").Value;
            FileUploadSettings.paciServiceUrl = Configuration.GetSection("Location:PACIService:ServiceUrl").Value;
            FileUploadSettings.paciNumberFieldName = Configuration.GetSection("Location:PACIService:PACIFieldName").Value;
            FileUploadSettings.nhoodNameFieldName = Configuration.GetSection("Location:BlockService:AreaNameFieldName").Value;
            FileUploadSettings.blockServiceUrl = Configuration.GetSection("Location:BlockService:ServiceUrl").Value;
            FileUploadSettings.blockNameFieldNameBlockService = Configuration.GetSection("Location:BlockService:BlockNameFieldName").Value;
            FileUploadSettings.streetServiceUrl = Configuration.GetSection("Location:StreetService:ServiceUrl").Value;
            FileUploadSettings.blockNameFieldNameStreetService = Configuration.GetSection("Location:StreetService:BlockNameFieldName").Value;
            FileUploadSettings.streetNameFieldName = Configuration.GetSection("Location:StreetService:StreetFieldName").Value;
            StaticAppSettings.ArchivedStatusId = Configuration.GetSection("OrderActionsPrerequisite:ArchivedStatusId").Get<List<int>>();
            StaticAppSettings.TravellingStatusId = Configuration.GetSection("OrderActionsPrerequisite:TravellingStatusId").Get<List<int>>();

            StaticAppSettings.WorkingStatusId = Configuration.GetSection("OrderActionsPrerequisite:WorkingStatusId").Get<List<int>>();

            StaticAppSettings.AssignedStatusId = Configuration.GetSection("OrderActionsPrerequisite:AssignedStatusId").Get<List<int>>();
            StaticAppSettings.UnassignedStatusId = Configuration.GetSection("OrderActionsPrerequisite:UnassignedStatusId").Get<List<int>>();
            StaticAppSettings.CanUnassignStatusId = Configuration.GetSection("OrderActionsPrerequisite:CanUnassignStatusId").Get<List<int>>();
            StaticAppSettings.DispatchedSubStatusIds = Configuration.GetSection("SubStatusNames:DispatchedIds").Get<List<int>>();
            StaticAppSettings.ArchivedStatusName = Configuration.GetSection("OrderActionsPrerequisite:ArchivedStatusName").Get<List<string>>();
            StaticAppSettings.TravellingStatusName = Configuration.GetSection("OrderActionsPrerequisite:TravellingStatusName").Get<List<string>>();
            StaticAppSettings.AssignedStatusName = Configuration.GetSection("OrderActionsPrerequisite:AssignedStatusName").Get<List<string>>();
            StaticAppSettings.UnassignedStatusName = Configuration.GetSection("OrderActionsPrerequisite:UnassignedStatusName").Get<List<string>>();
            StaticAppSettings.CanUnassignStatusName = Configuration.GetSection("OrderActionsPrerequisite:CanUnassignStatusName").Get<List<string>>();
            StaticAppSettings.DispatchedSubStatusNames = Configuration.GetSection("SubStatusNames:DispatchedNames").Get<List<string>>();
            StaticAppSettings.ConnectionString = Configuration.GetSection("ConnectionString").Get<string>();
            StaticAppSettings.ExceedTimeHours = Configuration.GetSection("OrderSettingsColumns:ExceedTimeHours").Get<string>();
            StaticAppSettings.ArchiveAfterDays = Configuration.GetSection("OrderSettingsColumns:ArchiveAfterDays").Get<string>();
            StaticAppSettings.InitialStatus = Configuration.GetSection("StatusControl:InitialStatus").Get<KeyValue>();
            StaticAppSettings.InitialSubStatus = Configuration.GetSection("StatusControl:InitialSubStatus").Get<KeyValue>();
            StaticAppSettings.DispatchedStatus = Configuration.GetSection("StatusControl:DispatchedStatus").Get<KeyValue>();
            StaticAppSettings.OnHoldStatus = Configuration.GetSection("StatusControl:OnHoldStatus").Get<KeyValue>();
            StaticAppSettings.OnTravelSubStatus = Configuration.GetSection("OrderActionsPrerequisite:OnTravelSubStatus").Get<KeyValue>();
            StaticAppSettings.SMSContent = Configuration.GetSection("SMSContent").Get<string>();
            StaticAppSettings.WorkingTypes = Configuration.GetSection("WorkingTypes").Get<List<KeyValue>>();
            StaticAppSettings.IdleHandling = Configuration.GetSection("IdleHandling").Get<List<KeyValue>>();
            StaticAppSettings.WorkingTypes = Configuration.GetSection("WorkingTypes").Get<List<KeyValue>>();
            StaticAppSettings.ExcludedStatusIds = Configuration.GetSection("ExcludeOrderActions:ExcludedIds").Get<List<int>>();
            StaticAppSettings.ExcludedStatusNames = Configuration.GetSection("ExcludeOrderActions:ExcludedNames").Get<List<string>>();


            //Location config settings 
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddSingleton(provider => Configuration);

            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("Ghanim.Order.EFCore.MSSQL")));

            //location config
            PACIConfig.proxyUrl = Configuration.GetSection("Location:ProxyUrl").Value;
            PACIConfig.paciServiceUrl = Configuration.GetSection("Location:PACIService:ServiceUrl").Value;
            PACIConfig.paciNumberFieldName = Configuration.GetSection("Location:PACIService:PACIFieldName").Value;
            PACIConfig.nhoodNameFieldName = Configuration.GetSection("Location:BlockService:AreaNameFieldName").Value;
            PACIConfig.blockServiceUrl = Configuration.GetSection("Location:BlockService:ServiceUrl").Value;
            PACIConfig.blockNameFieldNameBlockService = Configuration.GetSection("Location:BlockService:BlockNameFieldName").Value;
            PACIConfig.streetServiceUrl = Configuration.GetSection("Location:StreetService:ServiceUrl").Value;
            PACIConfig.blockNameFieldNameStreetService = Configuration.GetSection("Location:StreetService:BlockNameFieldName").Value;
            PACIConfig.streetNameFieldName = Configuration.GetSection("Location:StreetService:StreetFieldName").Value;



            services.AddAutoMapper(typeof(Startup));
            //Mapper.AssertConfigurationIsValid();

            //Hangfire Settings
            services.AddHangfire(x => x.UseSqlServerStorage(Configuration["ConnectionString"]));

            //Mapper.AssertConfigurationIsValid();
            //Mapper.Initialize(cfg =>
            //{
            //    cfg.AddProfile<MappingProfile>();
            //});

            //services.AddIdentityServer();
            //services.Configure<VehicleAppSettings>(Configuration);
            #region Data & Managers
            services.AddScoped(typeof(IOrderRowData), typeof(OrderRowData));
            services.AddScoped(typeof(IOrderObject), typeof(OrderObject));
            services.AddScoped(typeof(IOrderSetting), typeof(OrderSetting));
            services.AddScoped(typeof(IArchivedOrder), typeof(ArchivedOrder));
            services.AddScoped(typeof(IOrderAction), typeof(OrderAction));
            services.AddScoped(typeof(INotificationCenter), typeof(NotificationCenter));
            services.AddScoped(typeof(INotification), typeof(Notification));
            services.AddScoped(typeof(IPACI), typeof(Ghanim.Models.Entities.PACI));
            services.AddScoped(typeof(IOrderStatus), typeof(OrderStatus));
            services.AddScoped(typeof(IOrderSubStatus), typeof(OrderSubStatus));
            services.AddScoped(typeof(IOrderPriority), typeof(OrderPriority));
            services.AddScoped(typeof(IOrderType), typeof(OrderType));
            services.AddScoped(typeof(IOrderProblem), typeof(OrderProblem));
            services.AddScoped(typeof(ILang_OrderProblem), typeof(Lang_OrderProblem));
            services.AddScoped(typeof(ILang_OrderType), typeof(Lang_OrderType));
            services.AddScoped(typeof(ILang_OrderPriority), typeof(Lang_OrderPriority));
            services.AddScoped(typeof(ILang_OrderSubStatus), typeof(Lang_OrderSubStatus));
            services.AddScoped(typeof(ILang_OrderStatus), typeof(Lang_OrderStatus));
            services.AddScoped(typeof(IRejectionReason), typeof(RejectionReason));
            services.AddScoped(typeof(ILang_RejectionReason), typeof(Lang_RejectionReason));
            services.AddScoped(typeof(IWorkingType), typeof(WorkingType));
            services.AddScoped(typeof(ILang_WorkingType), typeof(Lang_WorkingType));
            services.AddScoped(typeof(ILang_Notification), typeof(Lang_Notification));


            services.AddScoped(typeof(ICustomer), typeof(CustomerObject));
            services.AddScoped(typeof(ICustomerPhoneBook), typeof(CustomerPhoneBook));
            services.AddScoped(typeof(ILocation), typeof(Location));
            services.AddScoped(typeof(ICustomerType), typeof(CustomerType));
            services.AddScoped(typeof(ILang_CustomerType), typeof(Lang_CustomerType));
            services.AddScoped(typeof(IMachineType), typeof(MachineType));
            services.AddScoped(typeof(ILang_MachineType), typeof(Lang_MachineType));
            services.AddScoped(typeof(IPhoneType), typeof(PhoneType));
            services.AddScoped(typeof(ILang_PhoneType), typeof(Lang_PhoneType));




            //services.AddScoped(typeof(IReadOrderManager), typeof(ReadOrderManager));
            services.AddScoped(typeof(IArchivedOrderFileManager), typeof(ArchivedOrderFileManager));
            services.AddScoped(typeof(IOrderRowDataManager), typeof(OrderRowDataManager));
            services.AddScoped(typeof(IOrderManager), typeof(OrderManager));
            services.AddScoped(typeof(IOrderSettingManager), typeof(OrderSettingManager));
            services.AddScoped(typeof(IArchivedOrderManager), typeof(ArchivedOrderManager));
            services.AddScoped(typeof(IOrderActionManager), typeof(OrderActionManager));
            services.AddScoped(typeof(IOrderStatusManager), typeof(OrderStatusManager));
            services.AddScoped(typeof(IOrderSubStatusManager), typeof(OrderSubStatusManager));
            services.AddScoped(typeof(IOrderProblemManager), typeof(OrderProblemManager));
            services.AddScoped(typeof(IOrderTypeManager), typeof(OrderTypeManager));
            services.AddScoped(typeof(IOrderPriorityManager), typeof(OrderPriorityManager));
            services.AddScoped(typeof(ILang_OrderSubStatusManager), typeof(_lang_WorkingTypeManager));
            services.AddScoped(typeof(ILang_OrderStatusManager), typeof(_lang_WorkingTypeManager));
            services.AddScoped(typeof(ILang_OrderTypeManager), typeof(Lang_OrderTypeManager));
            services.AddScoped(typeof(ILang_OrderPriorityManager), typeof(Lang_OrderPriorityManager));
            services.AddScoped(typeof(ILang_OrderProblemManager), typeof(Lang_OrderProblemManager));
            services.AddScoped(typeof(IRejectionReasonManager), typeof(RejectionReasonManager));
            services.AddScoped(typeof(ILang_RejectionReasonManager), typeof(Lang_RejectionReasonManager));
            services.AddScoped(typeof(IWorkingTypeManager), typeof(WorkingTypeManager));
            services.AddScoped(typeof(ILang_WorkingTypeManager), typeof(Lang_WorkingTypeManager));
            services.AddScoped(typeof(ILang_NotificationManager), typeof(Lang_NotificationManager));
            services.AddScoped(typeof(INotificationCenterManager), typeof(NotificationCenterManager));
            services.AddScoped(typeof(INotificationManager), typeof(NotificationManager));
            services.AddScoped(typeof(IPACIManager), typeof(PACIManager));
            services.AddScoped(typeof(ITeamMembersService), typeof(TeamMembersService));
            services.AddScoped(typeof(ISMSService), typeof(SMSService));
            services.AddScoped(typeof(IProblemService), typeof(ProblemService));
            services.AddScoped(typeof(IIdentityService), typeof(IdentityService));
            services.AddScoped(typeof(IForemanService), typeof(ForemanService));
            services.AddScoped(typeof(IEngineerService), typeof(EngineerService));
            services.AddScoped(typeof(IDivisionService), typeof(DivisionService));
            //services.AddScoped(typeof(IAreaService), typeof(AreaService));
            services.AddScoped(typeof(IBuildingTypesService), typeof(BuildingTypesService));
            services.AddScoped(typeof(ICompanyCodeService), typeof(CompanyCodeService));
            services.AddScoped(typeof(IContractTypesService), typeof(ContractTypesService));
            services.AddScoped(typeof(IGovernoratesService), typeof(GovernoratesService));
            services.AddScoped(typeof(IManagerService), typeof(ManagerService));
            services.AddScoped(typeof(ITechnicianService), typeof(TechnicianService));
            services.AddScoped(typeof(IDispatcherService), typeof(DispatcherService));
            services.AddScoped(typeof(IGeoLocationService), typeof(GeoLocationService));
            services.AddScoped(typeof(ITeamService), typeof(TeamService));
            services.AddScoped(typeof(ISupervisorService), typeof(SupervisorService));
            services.AddScoped(typeof(IDispatcherSettingsService), typeof(DispatcherSettingsService));
            services.AddScoped(typeof(ISupportedLanguageService), typeof(SupportedLanguageService));
            services.AddScoped(typeof(ITechnicianStateService), typeof(TechnicianStateService));
            services.AddScoped(typeof(ILang_OrderStatusManager), typeof(Lang_OrderStatusManager));
            //
            services.AddScoped(typeof(ISupportedLanguagesManager), typeof(SupportedLanguagesManager));
            services.AddScoped(typeof(IOrderHistoryTeamManager), typeof(OrderHistoryTeamManager));
            services.AddScoped(typeof(ITeamMembersHistoryManager), typeof(TeamMembersHistoryManager)); 
            services.AddScoped(typeof(IOrderSourceCreatorManager), typeof( OrderSourceCreatorManager)); 
            services.AddScoped(typeof(IVisitSlotManager), typeof(VisitSlotManager));
            services.AddScoped(typeof(IOrderRemoteRequestsOnProcessManager), typeof(OrderRemoteRequestsOnProcessManager));
            services.AddScoped(typeof(IOrderRemoteRequestsHistoryManager), typeof(OrderRemoteRequestsHistoryManager));
            

            /////// to call trl from start up
            services.AddTransient(typeof(IHangFireJobService), typeof(HangFireJobService));


            services.AddScoped(typeof(IAPIHelper), typeof(APIHelper));
            services.AddScoped(typeof(ILang_WorkingTypeManager), typeof(Lang_WorkingTypeManager));
            services.AddScoped(typeof(ILang_WorkingType), typeof(Lang_WorkingType));



            services.AddScoped(typeof(ICustomerManager), typeof(CustomerManager));
            services.AddScoped(typeof(ILocationManager), typeof(LocationManager));
            services.AddScoped(typeof(ICustomerPhoneBookManager), typeof(CustomerPhoneBookManager));
            services.AddScoped(typeof(ICustomerTypeManager), typeof(CustomerTypeManager));
            services.AddScoped(typeof(ILang_CustomerTypeManager), typeof(Lang_CustomerTypeManager));
            services.AddScoped(typeof(IMachineTypeManager), typeof(MachineTypeManager));
            services.AddScoped(typeof(ILang_MachineTypeManager), typeof(Lang_MachineTypeManager));
            services.AddScoped(typeof(IPhoneTypeManager), typeof(PhoneTypeManager));
            services.AddScoped (typeof(ILang_PhoneTypeManager), typeof(Lang_PhoneTypeManager)) ;






            //disable reading orderTasks from api !!
            //services.AddHostedService<ReadingOrderTask>();
            //services.AddScoped<ReadOrdersWorkFlow>();

            //services.AddScoped<ArchiveOrderFlow>();
            //services.AddScoped<TimeExceedFlow>();

            //services.AddHostedService<ArchiveOrderTask>();
            //services.AddHostedService<TimeExccedTask>();
            #endregion






            #region Server Cashing Injections
            services.AddSingleton(typeof(ICashedDataContainer), typeof(CashedDataContainer));
            services.AddScoped(typeof(ICachedTables), typeof(CachedTables));
            services.AddScoped(typeof(ICached<AreasViewModel>), typeof(AreaCached));
            services.AddScoped(typeof(ICached<OrderStatusViewModel>), typeof(OrderStatusCached));
            services.AddScoped(typeof(ICached<OrderProblemViewModel>), typeof(OrderProblemCached));
            services.AddScoped(typeof(ICached<OrderTypeViewModel>), typeof(OrderTypeCached));
            services.AddScoped(typeof(ICached<DivisionViewModel>), typeof(DivisionCached));
            services.AddScoped(typeof(ICached<CostCenterViewModel>), typeof(CostCenterCached));
            services.AddScoped(typeof(ICached<MapAdminRelatedViewModel>), typeof(MapAdminRelatedCached));
            services.AddScoped(typeof(ICached<CompanyCodeViewModel>), typeof(CompanyCodeCached));
            services.AddScoped(typeof(ICached<ContractTypesViewModel>), typeof(ContractTypeCached));

            services.AddScoped(typeof(ICached<CustomerTypeViewModel>), typeof(CustomerTypeCached));
            services.AddScoped(typeof(ICached<MachineTypeViewModel>), typeof(MachineTypeCached));
            services.AddScoped(typeof(ICached<PhoneTypeViewModel>), typeof(PhoneTypeCached));







            services.AddScoped(typeof(ICached<WorkingTypeMiniViewModel>), typeof(WorkingTypeCached));


            #endregion
















            //   services.AddDbContext<DataContext>(options =>
            //options.UseSqlServer(Configuration["ConnectionString"],
            // sqlOptions => sqlOptions.MigrationsAssembly("Sprintoo.UserManagement.EFCore.MSSQL")));

            //////////////////// commented the part of ServiceRegisteryPort  ////////////////////////////
            //// Dns Query for consul - service discovery
            //string consulContainerName = Configuration.GetSection("ServiceRegisteryContainerName").Value;
            //int consulPort = Convert.ToInt32(Configuration.GetSection("ServiceRegisteryPort").Value);
            //IPAddress ip = Dns.GetHostEntry(consulContainerName).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First();
            //services.AddSingleton<IDnsQuery>(new LookupClient(ip, consulPort));
            //services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));
            ////////////////////////////////////////////////////////////////////////////////
            services.AddDirectoryBrowser();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "Order API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);
                options.CustomSchemaIds(x => x.FullName);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                // Assign scope requirements to operations based on AuthorizeAttribute
                // options.OperationFilter<SecurityRequirementsOperationFilter>();
            });


            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });



            //new Microsoft.Extensions.DependencyInjection.ServiceCollection();
            //IServiceProvider servicesProvider = ServiceCollection();
            //IHangFireJobService hngfirSrvc = services.BuildServiceProvider().GetRequiredService<IHangFireJobService>();
            //hngfirSrvc.SendIdeleBreakAlert();
            //todo
            //services.AddSingleton<OrderController, OrderController>((ctx) =>
            //{
            //    OrderController svc = ctx.GetService<OrderController>();
            //    svc.SendIdeleBreakAlert();
            //    return svc;
            //});


            //var context = HttpContext.RequestServices.GetService<IOrderActionManager>;

            ////var asdas =  (IOrderActionManager) HttpContext.ReqiuestServices.GetServices

            //OrderController order = new OrderController();
            //order.SendIdeleBreakAlert();
            _services = services;

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //app.UseCors("AllowAll");
            app.UseCors(x => x
               .AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader());

            app.UseAuthentication();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }


            //localization
            app.UseRequestLocalization();
            //end localization


            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //app.UseStaticFiles();
            // Autoregister using server.Features (does not work in reverse proxy mode)
            //app.UseConsulRegisterService();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                //options.InjectOnCompleteJavaScript("/swagger/ui/abp.js");
                //options.InjectOnCompleteJavaScript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Identity API V1");
            }); // URL: /swagger





















            app.UseCors("AllowAll");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            //app.Use(async (context, next) =>
            //{
            //    await next();
            //    if (context.Response.StatusCode == 200)
            //    {
            //        context.Request.Path = "/Order/SendIdeleBreakAlert"; // <==== Url.Action("", "")...
            //        await next();
            //    }
            //});
            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Order}/{action=SendIdeleBreakAlert}/{id?}");
            //});




            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Home}/{action=Index}/{id?}");
            //});
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\ExcelSheet")),
                RequestPath = new PathString("/ExcelSheet")
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\ExcelSheet")),
                RequestPath = new PathString("/ExcelSheet")
            });


            // Autoregister using server.Features (does not work in reverse proxy mode)
            //app.UseConsulRegisterService();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                //options.InjectOnCompleteJavaScript("/swagger/ui/abp.js");
                //options.InjectOnCompleteJavaScript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Order API V1");
            }); // URL: /swagger 


            //HangFire
            app.UseHangfireDashboard();
            app.UseHangfireServer();

            //var services = new ServiceCollection();
            IServiceProvider servicesProvider = _services.BuildServiceProvider();
            IHangFireJobService hngfirSrvc = servicesProvider.GetRequiredService<IHangFireJobService>();
            ////var jobId = BackgroundJob.Schedule(
            ////   () => hngfirSrvc.Hangfire(), TimeSpan.FromSeconds(2));
            ////var jobId = BackgroundJob.Schedule(
            //// () => Console.WriteLine("Hello Hangfire"), TimeSpan.FromSeconds(2));            
            ////var jobId = BackgroundJob.Schedule(() => hngfirSrvc.RegisterIdleBreakNotifications(),
            ////TimeSpan.FromMinutes(20));
            ////RecurringJob.AddOrUpdate(() => hngfirSrvc.RegisterIdleBreakNotifications(), Cron.MinuteInterval(20));
            ////hngfirSrvc.SendIdeleBreakAlert();
            ////For Stopping IdelNotifications
            //RecurringJob.AddOrUpdate("IdleAndBreakJobId", () => hngfirSrvc.RegisterIdleBreakNotifications(), Cron.MinuteInterval(20));
            ////New Solution
            ////var startTimeSpan = TimeSpan.Zero;
            ////var periodTimeSpan = TimeSpan.FromMinutes(5);

            ////var timer = new System.Threading.Timer((e) =>
            ////{
            ////    hngfirSrvc.RegisterIdleBreakNotifications();
            ////}, null, startTimeSpan, periodTimeSpan);
        }
    }
}
