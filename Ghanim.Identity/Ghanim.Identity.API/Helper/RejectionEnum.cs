﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Helper
{
  
    public enum RejectionEnum
    {
        NoAction = 1,
        Accepted = 2,
        Rejected = 3,
        All = 4
    }
}
