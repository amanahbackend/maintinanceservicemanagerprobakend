﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Helper
{
    public enum OrderStatusEnumForFilterInMap
    {
        Assigned=1,
        Unassigned=2,
        Both=3
    }
}
