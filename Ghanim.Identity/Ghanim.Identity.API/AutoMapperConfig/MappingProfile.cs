﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonEnums;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;
using Utilites.PACI;
using Ghanim.Models.Entities;
using Ghanim.API.ViewModels.Order;
using Ghanim.BLL.NotMappedModels;

namespace Ghanim.API
{
    public partial class MappingProfile : Profile
    {
        public MappingProfile()
        {

            MapForManagementReport();
            MapForOrderPrintOut();
            MappingProfilePayment();
            MapForCashing();
            
            CreateMap<ApplicationRoleViewModel, ApplicationRole>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedName, opt => opt.Ignore());

            CreateMap<ApplicationRole, ApplicationRoleViewModel>();

            CreateMap<ApplicationUserViewModel, ApplicationUser>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.Deactivated, opt => opt.Ignore())
                .ForMember(dest => dest.Phone1, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Phone2, opt => opt.MapFrom(src => src.SecondPhone))
                .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                // .ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));



            CreateMap<ApplicationUser, ApplicationUserViewModel>()
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone1))
                .ForMember(dest => dest.SecondPhone, opt => opt.MapFrom(src => src.Phone2))
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.PictureUrl, opt => opt.Ignore())
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
                .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                 //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                 .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));

            CreateMap<Ghanim.API.JunkUserViewModel, JunkUser>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.Deactivated, opt => opt.Ignore());
            CreateMap<JunkUser, JunkUserViewModel>();

            CreateMap<ApplicationUserHistory, ApplicationUserHistoryViewModel>();
            CreateMap<ApplicationUserHistoryViewModel, ApplicationUserHistory>()
                .IgnoreBaseEntityProperties();

            CreateMap<Privilege, PrivilegeViewModel>();
            CreateMap<PrivilegeViewModel, Privilege>();

            //CreateMap<Lkp_CountryViewModel, Lkp_Country>()
            //    .IgnoreBaseEntityProperties();
            //CreateMap<Lkp_Country, Lkp_CountryViewModel>();

            CreateMap<ApplicationUser, UserSearchResultViewModel>()
                .ForMember(dest => dest.RoleNames, opt => opt.MapFrom(u => string.Join(",", u.RoleNames.ToArray())));


            //mapping for  DataManagement
            CreateMap<Areas, AreasViewModel>();
            CreateMap<AreasViewModel, Areas>()
                .IgnoreBaseEntityProperties(); ;

            CreateMap<AttendanceStates, AttendanceStatesViewModel>();
            CreateMap<AttendanceStatesViewModel, AttendanceStates>()
                .IgnoreBaseEntityProperties();
            CreateMap<NotificationCenter, NotificationCenterViewModel>();
            CreateMap<NotificationCenterViewModel, NotificationCenter>();
            CreateMap<Availability, AvailabilityViewModel>();
            CreateMap<AvailabilityViewModel, Availability>()
                .IgnoreBaseEntityProperties();

            CreateMap<BuildingTypes, BuildingTypesViewModel>();
            CreateMap<BuildingTypesViewModel, BuildingTypes>()
                     .IgnoreBaseEntityProperties();

            CreateMap<CompanyCode, CompanyCodeViewModel>();
            CreateMap<CompanyCodeViewModel, CompanyCode>()
                .IgnoreBaseEntityProperties();

            CreateMap<ContractTypes, ContractTypesViewModel>();
            CreateMap<ContractTypesViewModel, ContractTypes>()
                .IgnoreBaseEntityProperties();

            CreateMap<Division, DivisionViewModel>();
            CreateMap<DivisionViewModel, Division>()
                .IgnoreBaseEntityProperties();

            CreateMap<Governorates, GovernoratesViewModel>();
            CreateMap<GovernoratesViewModel, Governorates>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_Areas, Lang_AreasViewModel>();
            CreateMap<Lang_AreasViewModel, Lang_Areas>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_AttendanceStates, Lang_AttendanceStatesViewModel>();
            CreateMap<Lang_AttendanceStatesViewModel, Lang_AttendanceStates>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_Availability, Lang_AvailabilityViewModel>();
            CreateMap<Lang_AvailabilityViewModel, Lang_Availability>()
                .IgnoreBaseEntityProperties();

            CreateMap<Lang_BuildingTypes, Lang_BuildingTypesViewModel>();
            CreateMap<Lang_BuildingTypesViewModel, Lang_BuildingTypes>()
            .IgnoreBaseEntityProperties();

            CreateMap<Lang_CompanyCode, Lang_CompanyCodeViewModel>();
            CreateMap<Lang_CompanyCodeViewModel, Lang_CompanyCode>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_ContractTypes, Lang_ContractTypesViewModel>();
            CreateMap<Lang_ContractTypesViewModel, Lang_ContractTypes>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_CostCenter, Lang_CostCenterViewModel>();
            CreateMap<Lang_CostCenterViewModel, Lang_CostCenter>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Division, Lang_DivisionViewModel>();
            CreateMap<Lang_DivisionViewModel, Lang_Division>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Governorates, Lang_GovernoratesViewModel>();
            CreateMap<Lang_GovernoratesViewModel, Lang_Governorates>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Shift, Lang_ShiftViewModel>();
            CreateMap<Lang_ShiftViewModel, Lang_Shift>()
           .IgnoreBaseEntityProperties();

            CreateMap<CostCenter, CostCenterViewModel>();
            CreateMap<CostCenterViewModel, CostCenter>()
           .IgnoreBaseEntityProperties();

            CreateMap<Shift, ShiftViewModel>();


            CreateMap<ShiftViewModel, Shift>()
           //.ForMember(e => e.Name, c => c.Ignore())
           .IgnoreBaseEntityProperties();

            CreateMap<SupportedLanguages, SupportedLanguagesViewModel>();
            CreateMap<SupportedLanguagesViewModel, SupportedLanguages>()
           .IgnoreBaseEntityProperties();

            CreateMap<CustomerType, CustomerTypeViewModel>();
            CreateMap<CustomerTypeViewModel, CustomerType>()
                .IgnoreBaseEntityProperties();

            CreateMap<MachineType, MachineTypeViewModel>();
            CreateMap<MachineTypeViewModel, MachineType>()
                .IgnoreBaseEntityProperties();


            CreateMap<PhoneType, PhoneTypeViewModel>();
            CreateMap<PhoneTypeViewModel, PhoneType>()
                .IgnoreBaseEntityProperties();

            CreateMap(typeof(PaginatedItems<>), typeof(PaginatedItemsViewModel<>));
            CreateMap(typeof(PaginatedItemsViewModel<>), typeof(PaginatedItems<>));

            CreateMap<MapAdminRelated, MapAdminRelatedViewModel>();
            CreateMap<MapAdminRelatedViewModel, MapAdminRelated>()
           .IgnoreBaseEntityProperties();


            CreateMap<Supervisor, SupervisorViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => $"{src.User.FirstName} {src.User.FirstName} "))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name));
            //.IgnoreIdentityBaseEntityProperties();


            CreateMap<SupervisorViewModel, Supervisor>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Dispatcher, DispatcherViewModel>()
             .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
             .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FullName))
             .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FullName))
             .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
             .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
             .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
             .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
             .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
             .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
             .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User == null ? "" : src.User.FullName))
             .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor == null ? "" : (src.Supervisor.User == null ? "" : src.Supervisor.User.FullName)))


            //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
            ;


            CreateMap<Dispatcher, DispatcherViewModelWithCounts>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FullName))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FullName))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User == null ? "" : src.User.FullName))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor == null ? "" : (src.Supervisor.User == null ? "" : src.Supervisor.User.FullName)))

                .ForMember(dest => dest.ForeManCount, opt => opt.MapFrom(src => src.Foremans.Count(f => !f.IsDeleted)))
                .ForMember(dest => dest.OrderCount, opt => opt.MapFrom(src => src.Orders.Count()))
                ;

            //CreateMap<Dispatcher, DispatcherViewModel>()
            //    .Include<Dispatcher, DispatcherViewModelWithCounts>();



            CreateMap<DispatcherViewModel, Dispatcher>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Technician, TechnicianViewModel>()
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
               .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FullName))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FullName))
               .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
               //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
               .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
               //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
               //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
               //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
               //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
               //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
               //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
               .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
               .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
               .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver));

            //.ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.ForemanName));

            CreateMap<TechnicianViewModel, Technician>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
                //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
               .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver))
                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Foreman, ForemanViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User == null ? "" : src.User.FullName))

                //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                ;

            //CreateMap<Foreman, ForemanViewModel>()
            //    .Include<Foreman, ForemanViewModelWithTeamsCount>()
            //    ;


            CreateMap<Foreman, ForemanViewModelWithTeamsCount>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FullName))
                .ForMember(dest => dest.TeamsCount, opt => opt.MapFrom(src => src.Teams.Count(t => !t.IsDeleted)))
                ;






            CreateMap<ForemanViewModel, Foreman>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))

                .IgnoreIdentityBaseEntityProperties();

            CreateMap<Engineer, EngineerViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FullName))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
              .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
            .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User == null ? "" : src.User.FullName));

            CreateMap<EngineerViewModel, Engineer>()
                   .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                   //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                   //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                   .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                   //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                   .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                   //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.Division.Name))
                   .IgnoreIdentityBaseEntityProperties();




            CreateMap<Finance, FinanceViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FullName))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                //.ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                //.ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User == null ? "" : src.User.FullName));

            CreateMap<FinanceViewModel, Finance>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                //.ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                //.ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.Division.Name))
                .IgnoreIdentityBaseEntityProperties();




            CreateMap<Driver, DriverViewModel>()
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
               .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
               .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
               .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
               .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<DriverViewModel, Driver>()
                   .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                   .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                   .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                   .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                   .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                   .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                   .IgnoreIdentityBaseEntityProperties();

            CreateMap<Dispatcher, DispatcherUserViewModel>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => $"{src.User.FirstName} {src.User.FirstName} "))
                    .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                    .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                    ;

            CreateMap<DispatcherUserViewModel, Dispatcher>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, DispatcherUserViewModel>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                    .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                    .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                    //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                    .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
                    .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<DispatcherUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                 .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));

            CreateMap<ApplicationUserViewModel, SupervisorUserViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<SupervisorUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                 .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                 //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                 .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));


            CreateMap<Supervisor, SupervisorUserViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => $"{src.User.FirstName } {src.User.LastName }"))
                //.ForMember(dest => dest.DispUserCount, opt => opt.MapFrom(src => src.Dispatchers.Count))
                //.ForMember(dest => dest.OrdersCount, opt => opt.MapFrom(src =>src.Orders.Count))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
              .ForMember(dest => dest.DispUserCount, opt => opt.Ignore())
              .ForMember(dest => dest.OrdersCount, opt => opt.Ignore())
            ;

            CreateMap<SupervisorUserViewModel, Supervisor>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, DriverUserViewModel>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
              .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
              .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
               //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
               .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
              .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<DriverUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                 .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                 //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                 .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));


            CreateMap<Driver, DriverUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                 .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                 .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 .ForMember(dest => dest.VehicleNo, opt => opt.MapFrom(src => src.VehicleNo))
                 .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<DriverUserViewModel, Driver>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.VehicleNo, opt => opt.MapFrom(src => src.VehicleNo))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, EngineerUserViewModel>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
              .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
              .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
               //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
               .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
              .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<EngineerUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                  .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                 .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                 //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                 .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));

            CreateMap<Engineer, EngineerUserViewModel>()
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.FullName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                 .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                 .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                 .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                 ;

            CreateMap<EngineerUserViewModel, Engineer>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .IgnoreIdentityBaseEntityProperties();








            CreateMap<ApplicationUserViewModel, FinanceUserViewModel>()
            .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
            .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
            .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
            .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
            .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
            .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
             //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
             .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
            .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<FinanceUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                  .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                 .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                 //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                 .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));

            CreateMap<Finance, FinanceUserViewModel>()
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.FullName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                 //.ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                 //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                 //.ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                 //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                 ;

            CreateMap<FinanceUserViewModel, Finance>()
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    //.ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    //.ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .IgnoreIdentityBaseEntityProperties();





            CreateMap<ApplicationUserViewModel, ForemanUserViewModel>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
              .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
               .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
              .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<ForemanUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                  .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                   //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                   .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));

            CreateMap<Foreman, ForemanUserViewModel>()
                     .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                     .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                     .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                     .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                     .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                     .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                     .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                     .ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenter.Name))
                     .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                     //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                     .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                     //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                     ;

            CreateMap<ForemanUserViewModel, Foreman>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                    .ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                    //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                    .IgnoreIdentityBaseEntityProperties();

            CreateMap<ApplicationUserViewModel, TechnicianUserViewModel>()
             .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
             .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
             .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
             .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
             .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
             //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
             .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
             .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<TechnicianUserViewModel, ApplicationUserViewModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                  .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                 .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
                 //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
                 .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId));


            CreateMap<Technician, TechnicianUserViewModel>()
                     .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                     .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                     .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.FullName))
                     .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                     //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                     .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                     //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                     .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
                     .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                     .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver));
            //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
            //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
            //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
            //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
            //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
            //.ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.ForemanName));

            CreateMap<TechnicianUserViewModel, Technician>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                    //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.DivisionId))
                    //.ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.DivisionName))
                    .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId))
                    //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName))
                    .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
                    .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.IsDriver))
                    //.ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                    //.ForMember(dest => dest.SupervisorId, opt => opt.MapFrom(src => src.SupervisorId))
                    //.ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.SupervisorName))
                    //.ForMember(dest => dest.DispatcherId, opt => opt.MapFrom(src => src.DispatcherId))
                    //.ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.DispatcherName))
                    //.ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
                    //.ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.ForemanName))
                    .IgnoreIdentityBaseEntityProperties();
            CreateMap<DispatcherSettings, DispatcherSettingsViewModel>();
            CreateMap<DispatcherSettingsViewModel, DispatcherSettings>()
                .IgnoreBaseEntityProperties();
            CreateMap<Areas, AreasVmForDispatcherSettings>()
                .ForMember(dest => dest.AreaId, opt => opt.MapFrom(src => src.Id))
                 .ForMember(dest => dest.AreaName, opt => opt.MapFrom(src => src.Name));
            CreateMap<AreasVmForDispatcherSettings, Areas>();
            
                 CreateMap<Areas, AreasWithAssignedFlag>()
                .ForMember(dest => dest.AreaId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.Name));


            CreateMap<Vehicle, VehicleViewModel>();
            CreateMap<VehicleViewModel, Vehicle>()
                .IgnoreBaseEntityProperties();
            CreateMap<TeamMember, TeamMemberViewModel>()

                 .ForMember(dest => dest.IsDriver, opt => opt.MapFrom(src => src.MemberType == MemberType.Driver))
                 .ForMember(dest => dest.MemberTypeName, opt => opt.MapFrom(src => src.MemberType.ToString()))
                 .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FullName))
                 .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                 .ForMember(dest => dest.DivisionId, opt => opt.MapFrom(src => src.Division.Id))
                 .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.User.PF))
                 .ForMember(dest => dest.MemberName, opt => opt.MapFrom(src => src.User.FirstName))
                 .ForMember(dest => dest.MemberPhone, opt => opt.MapFrom(src => src.User.Phone1))
                 .ForMember(dest => dest.MemberType, opt => opt.MapFrom(src => (int)src.MemberType))

                 ;
            CreateMap<TeamMemberViewModel, TeamMember>()


                ;
            CreateMap<TeamMember, TeamMemberUserViewModel>();
            CreateMap<TeamMemberUserViewModel, TeamMember>();
            CreateMap<Team, TeamViewModel>()
                .ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.Foreman.User.FullName))
                ;
            CreateMap<Team, TeamViewModelMini>();

            CreateMap<Team, TeamIdForemanIdNameViewModel>()
                .ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.Foreman.User.FullName))
                ;
            CreateMap<TeamViewModel, Team>();
            CreateMap<Manager, ManagerViewModel>();
            CreateMap<Manager, ManagerUserViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
                .ForMember(dest => dest.CostCenterId, opt => opt.MapFrom(src => src.CostCenterId));
            //.ForMember(dest => dest.CostCenterName, opt => opt.MapFrom(src => src.CostCenterName));

            CreateMap<ApplicationUserViewModel, ManagerUserViewModel>()
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
               .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
               .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
               .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
               .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
               .ForMember(dest => dest.IsFirstLogin, opt => opt.MapFrom(src => src.IsFirstLogin))
               //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))
               .ForMember(dest => dest.LanguageId, opt => opt.MapFrom(src => src.LanguageId))
               .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<ApplicationUserViewModel, MaterialControllerUserViewModel>()
       .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
       .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
       .ForMember(dest => dest.PF, opt => opt.MapFrom(src => src.PF))
       .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
       .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
       .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
       //.ForMember(dest => dest.LanguageName, opt => opt.MapFrom(src => src.LanguageName))       
       .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<ManagerViewModel, Manager>();
            CreateMap<MaterialController, MaterialControllerViewModel>();
            CreateMap<MaterialController, MaterialControllerUserViewModel>();
            CreateMap<MaterialControllerViewModel, MaterialController>();
            CreateMap<TechniciansState, TechniciansStateViewModel>();
            CreateMap<TechniciansStateViewModel, TechniciansState>();








            //Order Mapping models and view models

            CreateMap<OrderObject, OrderViewModel>()
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name))
                .ForMember(dest => dest.RejectionReason, opt => opt.MapFrom(src => src.RejectionReason))
                .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.Dispatcher.User.FullName))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor.User.FullName))
                .ForMember(dest => dest.ContractTypeName, opt => opt.MapFrom(src => src.ContractType.Name))
                .ForMember(dest => dest.BuildingTypeName, opt => opt.MapFrom(src => src.BuildingType.Name))
                .ForMember(dest => dest.DivisionName, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.ProblemName, opt => opt.MapFrom(src => src.Problem.Name))

                .ForMember(dest => dest.TypeName, opt => opt.MapFrom(src => src.Type.Name))
                .ForMember(dest => dest.SubStatusName, opt => opt.MapFrom(src => src.SubStatus.Name))
                .ForMember(dest => dest.PriorityName, opt => opt.MapFrom(src => src.Priority.Name))
                .ForMember(dest => dest.AreaName, opt => opt.MapFrom(src => src.Area.Name))
               
                ;
            CreateMap<OrderViewModel, OrderObject>()
                .IgnoreBaseEntityProperties();
            /////

            CreateMap<OrderObject, TeamOrdersForMobileViewModel>()

            .ForMember(dest => dest.RejectionReason, opt => opt.MapFrom(src => src.RejectionReason))
            .ForMember(dest => dest.ContractTypeName, opt => opt.MapFrom(src => src.ContractType.Name))
            .ForMember(dest => dest.ProblemName, opt => opt.MapFrom(src => src.Problem.Name));

            CreateMap<TeamOrdersForMobileViewModel, OrderObject>()
                .IgnoreBaseEntityProperties();
            ///
            CreateMap<AssignDispatcherOrderViewModel, OrderObject>()
           .IgnoreBaseEntityProperties();
            /////
            CreateMap<OrderObject, ArchivedOrder>()
               .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.Id, opt => opt.Ignore());


            CreateMap<ArchivedOrder, OrderObject>()
                .IgnoreBaseEntityProperties();
            CreateMap<OrderProblem, OrderProblemViewModel>();
            CreateMap<OrderProblemViewModel, OrderProblem>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderType, OrderTypeViewModel>();
            CreateMap<OrderTypeViewModel, OrderType>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderStatus, OrderStatusViewModel>();
            CreateMap<OrderStatusViewModel, OrderStatus>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderPriority, OrderPriorityViewModel>();
            CreateMap<OrderPriorityViewModel, OrderPriority>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderSubStatus, OrderSubStatusViewModel>();
            CreateMap<OrderSubStatusViewModel, OrderSubStatus>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_OrderStatus, Lang_OrderStatusViewModel>();
            CreateMap<Lang_OrderStatusViewModel, Lang_OrderStatus>()
           .IgnoreBaseEntityProperties();
            CreateMap<OrderProblem, OrderProblemViewModel>();
            CreateMap<OrderProblemViewModel, OrderProblem>()
           .IgnoreBaseEntityProperties();
            CreateMap<Lang_OrderProblem, Lang_OrderProblemViewModel>();
            CreateMap<Lang_OrderProblemViewModel, Lang_OrderProblem>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_OrderType, Lang_OrderTypeViewModel>();
            CreateMap<Lang_OrderTypeViewModel, Lang_OrderType>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_WorkingType, Lang_WorkingTypeViewModel>();
            CreateMap<Lang_WorkingTypeViewModel, Lang_WorkingType>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_OrderPriority, Lang_OrderPriorityViewModel>();
            CreateMap<Lang_OrderPriorityViewModel, Lang_OrderPriority>()
           .IgnoreBaseEntityProperties();

            CreateMap<Lang_Notification, Lang_NotificationViewModel>();
            CreateMap<Lang_NotificationViewModel, Lang_Notification>()
           .IgnoreBaseEntityProperties();

            CreateMap<Notification, NotificationViewModel>();
            CreateMap<NotificationViewModel, Notification>()
           .IgnoreBaseEntityProperties();

            CreateMap<WorkingViewModel, TechniciansStateViewModel>();

            CreateMap<OrderObject, OrdersMapViewModel>()
                .ForMember(dest => dest.Pin, opt => opt.Ignore());

            CreateMap<TeamViewModel, TeamOrdersViewModel>()
                .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ForemanId, opt => opt.MapFrom(src => src.ForemanId))
                .ForMember(dest => dest.ForemanName, opt => opt.MapFrom(src => src.ForemanName))
                .ForMember(dest => dest.Orders, opt => opt.Ignore());


            CreateMap<PACIHelper.DropPACI, Dispatching.Location.API.ViewModels.LocationItemViewModel>()
                .ForMember(src => src.Id, opt => opt.MapFrom(dest => dest.Id))
                .ForMember(src => src.Name, opt => opt.MapFrom(dest => dest.Name));


            CreateMap<PACIHelper.PACIInfo, Dispatching.Location.API.ViewModels.LocationViewModel>()
                .ForMember(src => src.Area, opt => opt.MapFrom(dest => dest.Area))
                .ForMember(src => src.Block, opt => opt.MapFrom(dest => dest.Block))
                .ForMember(src => src.Governorate, opt => opt.MapFrom(dest => dest.Governorate))
                .ForMember(src => src.Street, opt => opt.MapFrom(dest => dest.Street))
                .ForMember(src => src.Lat, opt => opt.MapFrom(dest => dest.Latitude))
                .ForMember(src => src.Lng, opt => opt.MapFrom(dest => dest.Longtiude))
                .ForMember(src => src.AppartementNumber, opt => opt.MapFrom(dest => dest.AppartementNumber))
                .ForMember(src => src.FloorNumber, opt => opt.MapFrom(dest => dest.FloorNumber))
                .ForMember(src => src.BuildingNumber, opt => opt.MapFrom(dest => dest.BuildingNumber));







            CreateMap<OrderObject, OrderManagementReportItem>()
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.SAP_AreaName))
                .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.SAP_BlockName))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.SAP_StreetName))
                .ForMember(dest => dest.PACINum, opt => opt.MapFrom(src => src.SAP_PACI))
                .ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.CompanyCode.Code))
                .ForMember(dest => dest.DispatcherNotes, opt => opt.MapFrom(src => src.DispatcherNote))
                .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => src.CustomerCode))
                .ForMember(dest => dest.CustomerFirstPhone, opt => opt.MapFrom(src => src.PhoneOne))
                .ForMember(dest => dest.CustomerSecondPhone, opt => opt.MapFrom(src => src.PhoneTwo))
                .ForMember(dest => dest.CustomerCallerPhone, opt => opt.MapFrom(src => src.Caller_ID))
                .ForMember(dest => dest.GovName, opt => opt.MapFrom(src => src.GovName))
                .ForMember(dest => dest.FunctionalLocation, opt => opt.MapFrom(src => src.FunctionalLocation))
                .ForMember(dest => dest.SAPBuilding, opt => opt.MapFrom(src => src.SAP_HouseKasima))
                .ForMember(dest => dest.SAPFloor, opt => opt.MapFrom(src => src.SAP_Floor))
                .ForMember(dest => dest.SAPApartmentNo, opt => opt.MapFrom(src => src.SAP_AppartmentNo))
                .ForMember(dest => dest.ContractNo, opt => opt.MapFrom(src => src.ContractCode))
                .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.ContractType.Name))
                .ForMember(dest => dest.ContractCreationDate, opt => opt.MapFrom(src => src.ContractStartDate))
                .ForMember(dest => dest.ContractExpirationDate, opt => opt.MapFrom(src => src.ContractExpiryDate))
                .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Dispatcher.User.FullName))
                .ForMember(dest => dest.Supervisor, opt => opt.MapFrom(src => src.Supervisor.User.FullName))
                .ForMember(dest => dest.ForeMan, opt => opt.MapFrom(src => src.Team.Foreman.User.FullName))
                .ForMember(dest => dest.ICAgentNotes, opt => opt.MapFrom(src => src.ICAgentNote))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Division.Name))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Name))
                .ForMember(dest => dest.OrderDesc, opt => opt.MapFrom(src => src.OrderDescription))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.Type.Name))
                .ForMember(dest => dest.ProblemType, opt => opt.MapFrom(src => src.Problem.Name))
                .ForMember(dest => dest.ClosedDate,
                    opt => opt.MapFrom(src =>
                        src.StatusId == 7 || src.StatusId == 8 ? src.UpdatedDate : (DateTime?)null))
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => src.SAP_CreatedDate == null ? default(DateTime) : src.SAP_CreatedDate))
                .ForMember(dest => dest.UpdatedDate,
                    opt => opt.MapFrom(src =>
                        src.UpdatedDate == DateTime.MinValue ? src.SAP_CreatedDate : src.UpdatedDate));



            ///mapping values from order as usual as apove
            CreateMap<OrderAction, OrderManagementReportItem>()
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Order.Id))
                .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.Order.Code))
                .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Order.SAP_AreaName))
                .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.Order.SAP_BlockName))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.Order.SAP_StreetName))
                .ForMember(dest => dest.PACINum, opt => opt.MapFrom(src => src.Order.SAP_PACI))
                .ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Order.CompanyCode.Code))
                .ForMember(dest => dest.DispatcherNotes, opt => opt.MapFrom(src => src.Order.DispatcherNote))
                .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => src.Order.CustomerCode))
                .ForMember(dest => dest.CustomerFirstPhone, opt => opt.MapFrom(src => src.Order.PhoneOne))
                .ForMember(dest => dest.CustomerSecondPhone, opt => opt.MapFrom(src => src.Order.PhoneTwo))
                .ForMember(dest => dest.CustomerCallerPhone, opt => opt.MapFrom(src => src.Order.Caller_ID))
                .ForMember(dest => dest.GovName, opt => opt.MapFrom(src => src.Order.GovName))
                .ForMember(dest => dest.FunctionalLocation, opt => opt.MapFrom(src => src.Order.FunctionalLocation))
                .ForMember(dest => dest.SAPBuilding, opt => opt.MapFrom(src => src.Order.SAP_HouseKasima))
                .ForMember(dest => dest.SAPFloor, opt => opt.MapFrom(src => src.Order.SAP_Floor))
                .ForMember(dest => dest.SAPApartmentNo, opt => opt.MapFrom(src => src.Order.SAP_AppartmentNo))
                .ForMember(dest => dest.ContractNo, opt => opt.MapFrom(src => src.Order.ContractCode))
                .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.Order.ContractType.Name))
                .ForMember(dest => dest.ContractCreationDate, opt => opt.MapFrom(src => src.Order.ContractStartDate))
                .ForMember(dest => dest.ContractExpirationDate, opt => opt.MapFrom(src => src.Order.ContractExpiryDate))
                .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Order.Dispatcher.User.FullName))
                .ForMember(dest => dest.Supervisor, opt => opt.MapFrom(src => src.Order.Supervisor.User.FullName))
                .ForMember(dest => dest.ForeMan, opt => opt.MapFrom(src => src.Order.Team.Foreman.User.FullName))
                .ForMember(dest => dest.ICAgentNotes, opt => opt.MapFrom(src => src.Order.ICAgentNote))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Order.Division.Name))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status != null ? src.Status.Name : string.Empty))
                .ForMember(dest => dest.OrderDesc, opt => opt.MapFrom(src => src.Order.OrderDescription))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.Order.Type.Name))
                .ForMember(dest => dest.ProblemType, opt => opt.MapFrom(src => src.Order.Problem.Name))
                .ForMember(dest => dest.ClosedDate,
                    opt => opt.MapFrom(src =>
                        src.Order.StatusId == 7 || src.Order.StatusId == 8 ? src.Order.UpdatedDate : (DateTime?)null))
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src =>
                        src.Order.SAP_CreatedDate == null ? default(DateTime) : src.Order.SAP_CreatedDate))
                .ForMember(dest => dest.UpdatedDate,
                    opt => opt.MapFrom(src =>
                        src.Order.UpdatedDate == DateTime.MinValue ? src.Order.SAP_CreatedDate : src.Order.UpdatedDate))



                .ForMember(dest => dest.ActionTypeName, opt => opt.MapFrom(src => src.ActionTypeName))
                .ForMember(dest => dest.ProgressDate, opt => opt.MapFrom(src => src.ActionDate))
                .ForMember(dest => dest.Executer, opt => opt.MapFrom(src => src.CreatedUser))//TODO Excuter is not entered
                .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.Status.Name))
                .ForMember(dest => dest.ProgressSubStatus, opt => opt.MapFrom(src => src.SubStatus.Name))
                .ForMember(dest => dest.ProgressTime,
                    opt => opt.MapFrom(src =>
                        src.ActionTimeDays == 0 || src.ActionTimeDays == null
                            ? src.ActionTime
                            : src.ActionTime.Value.Add(TimeSpan.FromDays(src.ActionTimeDays.Value))))
                .ForMember(dest => dest.TechnicianNotes, opt => opt.MapFrom(src => src.ServeyReport))
                .ForMember(dest => dest.TechnicianName, opt => opt.MapFrom(src => src.Technician.User.FullName))
                .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.Dispatcher.User.FullName))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor.User.FullName))
                ;




            CreateMap<VisitSlot, VisitSlotViewModel>();
            CreateMap<VisitSlotViewModel, VisitSlot>()
           .IgnoreBaseEntityProperties();

            CreateMap<OrderSourceCreator, OrderSourceCreatorViewModel>();
            CreateMap<OrderSourceCreatorViewModel, OrderSourceCreator>()
           .IgnoreBaseEntityProperties();


            CreateMap<OrderRemoteRequestsOnProcess, OrderRemoteRequestsOnProcessViewModel>();
            CreateMap<OrderRemoteRequestsOnProcessViewModel, OrderRemoteRequestsOnProcess>()
           .IgnoreBaseEntityProperties();


            CreateMap<OrderRemoteRequestsHistory, OrderRemoteRequestsHistoryViewModel>();
            CreateMap<OrderRemoteRequestsHistoryViewModel, OrderRemoteRequestsHistory>()
           .IgnoreBaseEntityProperties();





        }
    }
}
