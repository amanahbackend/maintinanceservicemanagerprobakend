﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CommonEnums;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;
using Utilities.Utilites;
using Utilites.PACI;
using Ghanim.Models.Entities;
using Ghanim.Models.Order.Enums;
using Ghanim.Models.Views;
using Utilites;

namespace Ghanim.API
{
    public partial class MappingProfile
    {

        public void MapForManagementReport()
        {
            #region Depricated Views

            #region Mapping 1 Export with history 

            CreateMap<View_Excel_MRObject, OrderManagementReportExportOutputViewModelWithProgreassColomns>()
                //for order
                .ForMember(dest => dest.Created_Date,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("dd-MM-yyyy") ?? ""))
                .ForMember(dest => dest.Created_Time,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("hh:mm tt") ?? ""))


                .ForMember(dest => dest.IC_Agent_Notes,
                    opt => opt.MapFrom(src =>
                        src.IC_Agent_Notes != null ? Regex.Replace(src.IC_Agent_Notes, @"\t|\n|\r|,", " ") : ""))


                //.ForMember(dest => dest.Customer_Caller_Phone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "")}{(src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))

                .ForMember(dest => dest.Customer_Caller_Phone, opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))





                .ForMember(dest => dest.Contract_Creation_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Creation_Date == null ? "" : ((DateTime)src.Contract_Creation_Date).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Contract_Expiration_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Expiration_Date == null ? "" : ((DateTime)src.Contract_Expiration_Date).ToString("dd-MM-yyyy") ?? ""))

                .ForMember(dest => dest.Closed_Date,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("dd-MM-yyyy") ?? "" : ""))


                .ForMember(dest => dest.Closed_Time,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("hh:mm tt") ?? "" : ""))



                .ForMember(dest => dest.Progress_Date_History,
                    opt => opt.MapFrom(src =>
                        src.Progress_Date_History == null ? "" : (((DateTime)src.Progress_Date_History).AddHours(3)).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Progress_Time_History,
                    opt => opt.MapFrom(src =>
                        src.Progress_Date_History == null ? "" : (((DateTime)src.Progress_Date_History).AddHours(3)).ToString("hh:mm tt") ?? ""))

                .ForMember(dest => dest.PACI_Governorate, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.SAP_Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.SAP_Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.SAP_Block, opt => opt.MapFrom(src => src.PACI_Block))

                //current techniciaaaaaaaan



                ;


            #endregion


            #region Mapping 1 Export with out history 

            CreateMap<View_Excel_MRObject, OrderManagementReportExportOutputViewModel>()
                //for order
                .ForMember(dest => dest.Created_Date,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("dd-MM-yyyy") ?? ""))
                .ForMember(dest => dest.Created_Time,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("hh:mm tt") ?? ""))


                .ForMember(dest => dest.IC_Agent_Notes,
                    opt => opt.MapFrom(src =>
                        src.IC_Agent_Notes != null ? Regex.Replace(src.IC_Agent_Notes, @"\t|\n|\r|,", " ") : ""))


                //.ForMember(dest => dest.Customer_Caller_Phone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "")}{(src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))


                //.ForMember(dest => dest.Customer_Tel_1,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_1) && !string.IsNullOrWhiteSpace(src.Customer_Tel_1)) ? "(+)" : "")}{src.Customer_Tel_1 ?? ""}"))

                //.ForMember(dest => dest.Customer_Tel_2,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_2) && !string.IsNullOrWhiteSpace(src.Customer_Tel_2)) ? "(+)" : "")}{src.Customer_Tel_2 ?? ""}"))



                .ForMember(dest => dest.Customer_Caller_Phone,
                    opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))

                .ForMember(dest => dest.Customer_Tel_1,
                    opt => opt.MapFrom(src => src.Customer_Tel_1.Replace("+", "(+)")))

                .ForMember(dest => dest.Customer_Tel_2,
                    opt => opt.MapFrom(src => src.Customer_Tel_2.Replace("+", "(+)")))


                .ForMember(dest => dest.PACI_Governorate, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.SAP_Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.SAP_Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.SAP_Block, opt => opt.MapFrom(src => src.PACI_Block))



                .ForMember(dest => dest.Contract_Creation_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Creation_Date == null
                            ? ""
                            : ((DateTime)src.Contract_Creation_Date).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Contract_Expiration_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Expiration_Date == null
                            ? ""
                            : ((DateTime)src.Contract_Expiration_Date).ToString("dd-MM-yyyy") ?? ""))

                .ForMember(dest => dest.Closed_Date,
                    opt => opt.MapFrom(src =>

                        (src.StatusId == (int)OrderStatusEnum.Cancelled ||
                         src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("dd-MM-yyyy") ?? ""
                            : ""))





                .ForMember(dest => dest.Closed_Time,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled ||
                         src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("hh:mm tt") ?? ""
                            : ""))

                ;


            #endregion

            #region Mapping 1 View for Filter


            /////mapping values from order as usual as apove
            CreateMap<View_Excel_MRObject, OrderManagementReportItem>()
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.Order_ID))
                .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.PACI_Block))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.PACINum, opt => opt.MapFrom(src => src.PACI_No))
                .ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Company_Code))
                .ForMember(dest => dest.DispatcherNotes, opt => opt.MapFrom(src => src.Dispatcher_Notes))
                .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => src.Customer_ID))

                //.ForMember(dest => dest.CustomerFirstPhone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_1) && !string.IsNullOrWhiteSpace(src.Customer_Tel_1)) ? "(+)" : "")}{  (src.Customer_Tel_1 ?? "").Replace('+',' ')}"))

                //.ForMember(dest => dest.CustomerSecondPhone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_2) && !string.IsNullOrWhiteSpace(src.Customer_Tel_2)) ? "(+)" : "")}{ (src.Customer_Tel_2 ?? "").Replace('+',' ')}"))

                //.ForMember(dest => dest.CustomerCallerPhone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "") }{ (src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))


                .ForMember(dest => dest.CustomerFirstPhone, opt => opt.MapFrom(src => src.Customer_Tel_1.Replace("+", "(+)")))

                .ForMember(dest => dest.CustomerSecondPhone, opt => opt.MapFrom(src => src.Customer_Tel_2.Replace("+", "(+)")))


                .ForMember(dest => dest.CustomerCallerPhone, opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))











                .ForMember(dest => dest.GovName, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.FunctionalLocation, opt => opt.MapFrom(src => src.Functional_Location))
                .ForMember(dest => dest.SAPBuilding, opt => opt.MapFrom(src => src.SAP_Building))
                .ForMember(dest => dest.SAPFloor, opt => opt.MapFrom(src => src.SAP_Floor))
                .ForMember(dest => dest.SAPApartmentNo, opt => opt.MapFrom(src => src.SAP_Apartment))
                .ForMember(dest => dest.ContractNo, opt => opt.MapFrom(src => src.Contract_Number))
                .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.Contract_Type))
                .ForMember(dest => dest.ContractCreationDate, opt => opt.MapFrom(src => src.Contract_Creation_Date))
                .ForMember(dest => dest.ContractExpirationDate, opt => opt.MapFrom(src => src.Contract_Expiration_Date))
                .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Current_Dispatcher))
                .ForMember(dest => dest.Supervisor, opt => opt.MapFrom(src => src.Current_Supervisor_Name))
                .ForMember(dest => dest.ForeMan, opt => opt.MapFrom(src => src.Current_Foreman_Name))
                .ForMember(dest => dest.ICAgentNotes, opt => opt.MapFrom(src => src.IC_Agent_Notes))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Division))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Current_Order_Status))
                .ForMember(dest => dest.OrderDesc, opt => opt.MapFrom(src => src.Order_Description))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.Order_Type))
                .ForMember(dest => dest.ProblemType, opt => opt.MapFrom(src => src.Problem_Type))


                .ForMember(dest => dest.ClosedDate,
                    opt => opt.MapFrom(src => (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) ? src.Closed_Date : null))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Closed_Date))


                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => src.Created_Date))

                .ForMember(dest => dest.ActionTypeName, opt => opt.MapFrom(src => src.Action_Type_Name_History))
                .ForMember(dest => dest.ProgressDate, opt => opt.MapFrom(src => src.Progress_Date_History))
                .ForMember(dest => dest.Executer, opt => opt.MapFrom(src => src.Executor_History))//TODO Excuter is not entered
                .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.Progress_Status_History))
                .ForMember(dest => dest.ProgressSubStatus, opt => opt.MapFrom(src => src.Progress_Sub_Status_History))
                .ForMember(dest => dest.ProgressTime,
                    opt => opt.MapFrom(src => src.Progress_Time_History))
                .ForMember(dest => dest.TechnicianNotes, opt => opt.MapFrom(src => src.Survey_Report_History != null ? Regex.Replace(src.Survey_Report_History, @"\t|\n|\r|,", " ") : ""))


                .ForMember(dest => dest.Technician, opt => opt.MapFrom(src => src.Current_Technician))
                .ForMember(dest => dest.TechnicianName, opt => opt.MapFrom(src => src.Technician_Name_History))
                .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.Dispatcher_Name_History))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Current_Supervisor_Name))

                ;
            //current techniciaaaaaaaan

            #endregion







            #region View_Excel_MRObject_Action
            #endregion

            #region Mapping 1 Export with history 

            CreateMap<View_Excel_MR_Last_Action, OrderManagementReportExportOutputViewModelWithProgreassColomns>()
                //for order
                .ForMember(dest => dest.Created_Date,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("dd-MM-yyyy") ?? ""))
                .ForMember(dest => dest.Created_Time,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("hh:mm tt") ?? ""))


                .ForMember(dest => dest.IC_Agent_Notes,
                    opt => opt.MapFrom(src =>
                        src.IC_Agent_Notes != null ? Regex.Replace(src.IC_Agent_Notes, @"\t|\n|\r|,", " ") : ""))


                //.ForMember(dest => dest.Customer_Caller_Phone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "")}{(src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))

                .ForMember(dest => dest.Customer_Caller_Phone, opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))





                .ForMember(dest => dest.Contract_Creation_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Creation_Date == null ? "" : ((DateTime)src.Contract_Creation_Date).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Contract_Expiration_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Expiration_Date == null ? "" : ((DateTime)src.Contract_Expiration_Date).ToString("dd-MM-yyyy") ?? ""))

                .ForMember(dest => dest.Closed_Date,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("dd-MM-yyyy") ?? "" : ""))


                .ForMember(dest => dest.Closed_Time,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("hh:mm tt") ?? "" : ""))



                .ForMember(dest => dest.Progress_Date_History,
                    opt => opt.MapFrom(src =>
                        src.Progress_Date_History == null ? "" : (((DateTime)src.Progress_Date_History).AddHours(3)).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Progress_Time_History,
                    opt => opt.MapFrom(src =>
                        src.Progress_Date_History == null ? "" : (((DateTime)src.Progress_Date_History).AddHours(3)).ToString("hh:mm tt") ?? ""))

                .ForMember(dest => dest.PACI_Governorate, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.SAP_Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.SAP_Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.SAP_Block, opt => opt.MapFrom(src => src.PACI_Block))

                //current techniciaaaaaaaan



                ;


            #endregion


            #region Mapping 1 Export with out history 

            CreateMap<View_Excel_MR_Last_Action, OrderManagementReportExportOutputViewModel>()
                //for order
                .ForMember(dest => dest.Created_Date,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("dd-MM-yyyy") ?? ""))
                .ForMember(dest => dest.Created_Time,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("hh:mm tt") ?? ""))


                .ForMember(dest => dest.IC_Agent_Notes,
                    opt => opt.MapFrom(src =>
                        src.IC_Agent_Notes != null ? Regex.Replace(src.IC_Agent_Notes, @"\t|\n|\r|,", " ") : ""))


                //.ForMember(dest => dest.Customer_Caller_Phone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "")}{(src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))


                //.ForMember(dest => dest.Customer_Tel_1,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_1) && !string.IsNullOrWhiteSpace(src.Customer_Tel_1)) ? "(+)" : "")}{src.Customer_Tel_1 ?? ""}"))

                //.ForMember(dest => dest.Customer_Tel_2,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_2) && !string.IsNullOrWhiteSpace(src.Customer_Tel_2)) ? "(+)" : "")}{src.Customer_Tel_2 ?? ""}"))



                .ForMember(dest => dest.Customer_Caller_Phone,
                    opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))

                .ForMember(dest => dest.Customer_Tel_1,
                    opt => opt.MapFrom(src => src.Customer_Tel_1.Replace("+", "(+)")))

                .ForMember(dest => dest.Customer_Tel_2,
                    opt => opt.MapFrom(src => src.Customer_Tel_2.Replace("+", "(+)")))


                .ForMember(dest => dest.PACI_Governorate, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.SAP_Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.SAP_Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.SAP_Block, opt => opt.MapFrom(src => src.PACI_Block))



                .ForMember(dest => dest.Contract_Creation_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Creation_Date == null
                            ? ""
                            : ((DateTime)src.Contract_Creation_Date).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Contract_Expiration_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Expiration_Date == null
                            ? ""
                            : ((DateTime)src.Contract_Expiration_Date).ToString("dd-MM-yyyy") ?? ""))

                .ForMember(dest => dest.Closed_Date,
                    opt => opt.MapFrom(src =>

                        (src.StatusId == (int)OrderStatusEnum.Cancelled ||
                         src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("dd-MM-yyyy") ?? ""
                            : ""))





                .ForMember(dest => dest.Closed_Time,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled ||
                         src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("hh:mm tt") ?? ""
                            : ""))

                ;


            #endregion

            #region Mapping 1 View for Filter


            /////mapping values from order as usual as apove
            CreateMap<View_Excel_MR_Last_Action, OrderManagementReportItem>()
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.Order_ID))
                .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.PACI_Block))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.PACINum, opt => opt.MapFrom(src => src.PACI_No))
                .ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Company_Code))
                .ForMember(dest => dest.DispatcherNotes, opt => opt.MapFrom(src => src.Dispatcher_Notes))
                .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => src.Customer_ID))

                //.ForMember(dest => dest.CustomerFirstPhone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_1) && !string.IsNullOrWhiteSpace(src.Customer_Tel_1)) ? "(+)" : "")}{  (src.Customer_Tel_1 ?? "").Replace('+',' ')}"))

                //.ForMember(dest => dest.CustomerSecondPhone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_2) && !string.IsNullOrWhiteSpace(src.Customer_Tel_2)) ? "(+)" : "")}{ (src.Customer_Tel_2 ?? "").Replace('+',' ')}"))

                //.ForMember(dest => dest.CustomerCallerPhone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "") }{ (src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))


                .ForMember(dest => dest.CustomerFirstPhone, opt => opt.MapFrom(src => src.Customer_Tel_1.Replace("+", "(+)")))

                .ForMember(dest => dest.CustomerSecondPhone, opt => opt.MapFrom(src => src.Customer_Tel_2.Replace("+", "(+)")))


                .ForMember(dest => dest.CustomerCallerPhone, opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))











                .ForMember(dest => dest.GovName, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.FunctionalLocation, opt => opt.MapFrom(src => src.Functional_Location))
                .ForMember(dest => dest.SAPBuilding, opt => opt.MapFrom(src => src.SAP_Building))
                .ForMember(dest => dest.SAPFloor, opt => opt.MapFrom(src => src.SAP_Floor))
                .ForMember(dest => dest.SAPApartmentNo, opt => opt.MapFrom(src => src.SAP_Apartment))
                .ForMember(dest => dest.ContractNo, opt => opt.MapFrom(src => src.Contract_Number))
                .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.Contract_Type))
                .ForMember(dest => dest.ContractCreationDate, opt => opt.MapFrom(src => src.Contract_Creation_Date))
                .ForMember(dest => dest.ContractExpirationDate, opt => opt.MapFrom(src => src.Contract_Expiration_Date))
                .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Current_Dispatcher))
                .ForMember(dest => dest.Supervisor, opt => opt.MapFrom(src => src.Current_Supervisor_Name))
                .ForMember(dest => dest.ForeMan, opt => opt.MapFrom(src => src.Current_Foreman_Name))
                .ForMember(dest => dest.ICAgentNotes, opt => opt.MapFrom(src => src.IC_Agent_Notes))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Division))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Current_Order_Status))
                .ForMember(dest => dest.OrderDesc, opt => opt.MapFrom(src => src.Order_Description))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.Order_Type))
                .ForMember(dest => dest.ProblemType, opt => opt.MapFrom(src => src.Problem_Type))


                .ForMember(dest => dest.ClosedDate,
                    opt => opt.MapFrom(src => (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) ? src.Closed_Date : null))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Closed_Date))


                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => src.Created_Date))

                .ForMember(dest => dest.ActionTypeName, opt => opt.MapFrom(src => src.Action_Type_Name_History))
                .ForMember(dest => dest.ProgressDate, opt => opt.MapFrom(src => src.Progress_Date_History))
                .ForMember(dest => dest.Executer, opt => opt.MapFrom(src => src.Executor_History))//TODO Excuter is not entered
                .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.Progress_Status_History))
                .ForMember(dest => dest.ProgressSubStatus, opt => opt.MapFrom(src => src.Progress_Sub_Status_History))
                .ForMember(dest => dest.ProgressTime,
                    opt => opt.MapFrom(src => src.Progress_Time_History))
                .ForMember(dest => dest.TechnicianNotes, opt => opt.MapFrom(src => src.Survey_Report_History != null ? Regex.Replace(src.Survey_Report_History, @"\t|\n|\r|,", " ") : ""))


                .ForMember(dest => dest.Technician, opt => opt.MapFrom(src => src.Current_Technician))
                .ForMember(dest => dest.TechnicianName, opt => opt.MapFrom(src => src.Technician_Name_History))
                .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.Dispatcher_Name_History))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Current_Supervisor_Name))

                ;
            //current techniciaaaaaaaan

            #endregion
                    

            #endregion





            #region View_Excel_MR_DefaultAction
            #endregion

            #region Mapping 1 Export with history 

            CreateMap<View_Excel_MR_DefaultAction, OrderManagementReportExportOutputViewModelWithProgreassColomns>()
                //for order
                .ForMember(dest => dest.Created_Date,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("dd-MM-yyyy") ?? ""))
                .ForMember(dest => dest.Created_Time,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("hh:mm tt") ?? ""))


                .ForMember(dest => dest.IC_Agent_Notes,
                    opt => opt.MapFrom(src =>
                        src.IC_Agent_Notes != null ? Regex.Replace(src.IC_Agent_Notes, @"\t|\n|\r|,", " ") : ""))


                //.ForMember(dest => dest.Customer_Caller_Phone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "")}{(src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))

                .ForMember(dest => dest.Customer_Caller_Phone, opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))





                 .ForMember(dest => dest.Contract_Creation_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Creation_Date == null ? "" : ((DateTime)src.Contract_Creation_Date).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Contract_Expiration_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Expiration_Date == null ? "" : ((DateTime)src.Contract_Expiration_Date).ToString("dd-MM-yyyy") ?? ""))

                .ForMember(dest => dest.Closed_Date,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("dd-MM-yyyy") ?? "" : ""))


                .ForMember(dest => dest.Closed_Time,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("hh:mm tt") ?? "" : ""))



                .ForMember(dest => dest.Progress_Date_History,
                    opt => opt.MapFrom(src =>
                        src.Progress_Date_History == null ? "" : (((DateTime)src.Progress_Date_History).AddHours(3)).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Progress_Time_History,
                    opt => opt.MapFrom(src =>
                        src.Progress_Date_History == null ? "" : (((DateTime)src.Progress_Date_History).AddHours(3)).ToString("hh:mm tt") ?? ""))

                .ForMember(dest => dest.PACI_Governorate, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.SAP_Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.SAP_Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.SAP_Block, opt => opt.MapFrom(src => src.PACI_Block))
                .ForMember(dest => dest.Survey_Report_History, opt => opt.MapFrom(src => src.Survey_Report_History != null ? Regex.Replace(src.Survey_Report_History, @"\t|\n|\r|,", " ") : ""))

                //current techniciaaaaaaaan



                ;


            #endregion


            #region Mapping 1 Export with out history 

            CreateMap<View_Excel_MR_DefaultAction, OrderManagementReportExportOutputViewModel>()
                //for order
                .ForMember(dest => dest.Created_Date,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("dd-MM-yyyy") ?? ""))
                .ForMember(dest => dest.Created_Time,
                    opt => opt.MapFrom(src =>
                        src.Created_Date == null ? "" : ((DateTime)src.Created_Date).ToString("hh:mm tt") ?? ""))


                .ForMember(dest => dest.IC_Agent_Notes,
                    opt => opt.MapFrom(src =>
                        src.IC_Agent_Notes != null ? Regex.Replace(src.IC_Agent_Notes, @"\t|\n|\r|,", " ") : ""))


                //.ForMember(dest => dest.Customer_Caller_Phone,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "")}{(src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))


                //.ForMember(dest => dest.Customer_Tel_1,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_1) && !string.IsNullOrWhiteSpace(src.Customer_Tel_1)) ? "(+)" : "")}{src.Customer_Tel_1 ?? ""}"))

                //.ForMember(dest => dest.Customer_Tel_2,
                //    opt => opt.MapFrom(src =>
                //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_2) && !string.IsNullOrWhiteSpace(src.Customer_Tel_2)) ? "(+)" : "")}{src.Customer_Tel_2 ?? ""}"))



                .ForMember(dest => dest.Customer_Caller_Phone,
                    opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))

                .ForMember(dest => dest.Customer_Tel_1,
                    opt => opt.MapFrom(src => src.Customer_Tel_1.Replace("+", "(+)")))

                .ForMember(dest => dest.Customer_Tel_2,
                    opt => opt.MapFrom(src => src.Customer_Tel_2.Replace("+", "(+)")))


                .ForMember(dest => dest.PACI_Governorate, opt => opt.MapFrom(src => src.PACI_Governorate))
                .ForMember(dest => dest.SAP_Area, opt => opt.MapFrom(src => src.PACI_Area))
                .ForMember(dest => dest.SAP_Street, opt => opt.MapFrom(src => src.PACI_Street))
                .ForMember(dest => dest.SAP_Block, opt => opt.MapFrom(src => src.PACI_Block))



                .ForMember(dest => dest.Contract_Creation_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Creation_Date == null
                            ? ""
                            : ((DateTime)src.Contract_Creation_Date).ToString("dd-MM-yyyy") ?? ""))



                .ForMember(dest => dest.Contract_Expiration_Date,
                    opt => opt.MapFrom(src =>
                        src.Contract_Expiration_Date == null
                            ? ""
                            : ((DateTime)src.Contract_Expiration_Date).ToString("dd-MM-yyyy") ?? ""))

                .ForMember(dest => dest.Closed_Date,
                    opt => opt.MapFrom(src =>

                        (src.StatusId == (int)OrderStatusEnum.Cancelled ||
                         src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("dd-MM-yyyy") ?? ""
                            : ""))





                .ForMember(dest => dest.Closed_Time,
                    opt => opt.MapFrom(src =>
                        (src.StatusId == (int)OrderStatusEnum.Cancelled ||
                         src.StatusId == (int)OrderStatusEnum.Compelete) && src.Closed_Date != null
                            ? ((DateTime)src.Closed_Date).AddHours(3).ToString("hh:mm tt") ?? ""
                            : ""))

                ;


            #endregion

            #region Mapping 1 View for Filter


            /////mapping values from order as usual as apove
            CreateMap<View_Excel_MR_DefaultAction, OrderManagementReportItem>()
            .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.Order_ID))
            .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.PACI_Area))
            .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.PACI_Block))
            .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.PACI_Street))
            .ForMember(dest => dest.PACINum, opt => opt.MapFrom(src => src.PACI_No))
            .ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Company_Code))
            .ForMember(dest => dest.DispatcherNotes, opt => opt.MapFrom(src => src.Dispatcher_Notes))
            .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => src.Customer_ID))

            //.ForMember(dest => dest.CustomerFirstPhone,
            //    opt => opt.MapFrom(src =>
            //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_1) && !string.IsNullOrWhiteSpace(src.Customer_Tel_1)) ? "(+)" : "")}{  (src.Customer_Tel_1 ?? "").Replace('+',' ')}"))

            //.ForMember(dest => dest.CustomerSecondPhone,
            //    opt => opt.MapFrom(src =>
            //        $"{((!string.IsNullOrEmpty(src.Customer_Tel_2) && !string.IsNullOrWhiteSpace(src.Customer_Tel_2)) ? "(+)" : "")}{ (src.Customer_Tel_2 ?? "").Replace('+',' ')}"))

            //.ForMember(dest => dest.CustomerCallerPhone,
            //    opt => opt.MapFrom(src =>
            //        $"{((!string.IsNullOrEmpty(src.Customer_Caller_Phone) && !string.IsNullOrWhiteSpace(src.Customer_Caller_Phone)) ? "(+)" : "") }{ (src.Customer_Caller_Phone ?? "").Replace('+',' ')}"))


            .ForMember(dest => dest.CustomerFirstPhone, opt => opt.MapFrom(src => src.Customer_Tel_1.Replace("+", "(+)")))

            .ForMember(dest => dest.CustomerSecondPhone, opt => opt.MapFrom(src => src.Customer_Tel_2.Replace("+", "(+)")))


            .ForMember(dest => dest.CustomerCallerPhone, opt => opt.MapFrom(src => src.Customer_Caller_Phone.Replace("+", "(+)")))











            .ForMember(dest => dest.GovName, opt => opt.MapFrom(src => src.PACI_Governorate))
            .ForMember(dest => dest.FunctionalLocation, opt => opt.MapFrom(src => src.Functional_Location))
            .ForMember(dest => dest.SAPBuilding, opt => opt.MapFrom(src => src.SAP_Building))
            .ForMember(dest => dest.SAPFloor, opt => opt.MapFrom(src => src.SAP_Floor))
            .ForMember(dest => dest.SAPApartmentNo, opt => opt.MapFrom(src => src.SAP_Apartment))
            .ForMember(dest => dest.ContractNo, opt => opt.MapFrom(src => src.Contract_Number))
            .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.Contract_Type))
            .ForMember(dest => dest.ContractCreationDate, opt => opt.MapFrom(src => src.Contract_Creation_Date))
            .ForMember(dest => dest.ContractExpirationDate, opt => opt.MapFrom(src => src.Contract_Expiration_Date))
            .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Current_Dispatcher))
            .ForMember(dest => dest.Supervisor, opt => opt.MapFrom(src => src.Current_Supervisor_Name))
            .ForMember(dest => dest.ForeMan, opt => opt.MapFrom(src => src.Current_Foreman_Name))
            .ForMember(dest => dest.ICAgentNotes, opt => opt.MapFrom(src => src.IC_Agent_Notes))
            .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Division))
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Current_Order_Status))
            .ForMember(dest => dest.OrderDesc, opt => opt.MapFrom(src => src.Order_Description))
            .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.Order_Type))
            .ForMember(dest => dest.ProblemType, opt => opt.MapFrom(src => src.Problem_Type))


            .ForMember(dest => dest.ClosedDate,
                opt => opt.MapFrom(src => (src.StatusId == (int)OrderStatusEnum.Cancelled || src.StatusId == (int)OrderStatusEnum.Compelete) ? src.Closed_Date : null))
            .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Closed_Date))


            .ForMember(dest => dest.CreatedDate,
                opt => opt.MapFrom(src => src.Created_Date))

            .ForMember(dest => dest.ActionTypeName, opt => opt.MapFrom(src => src.Action_Type_Name_History))
            .ForMember(dest => dest.ProgressDate, opt => opt.MapFrom(src => src.Progress_Date_History))
            .ForMember(dest => dest.Executer, opt => opt.MapFrom(src => src.Executor_History))//TODO Excuter is not entered
            .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.Progress_Status_History))
            .ForMember(dest => dest.ProgressSubStatus, opt => opt.MapFrom(src => src.Progress_Sub_Status_History))
            .ForMember(dest => dest.ProgressTime,
                opt => opt.MapFrom(src => src.Progress_Time_History))
            .ForMember(dest => dest.TechnicianNotes, opt => opt.MapFrom(src => src.Survey_Report_History != null ? Regex.Replace(src.Survey_Report_History, @"\t|\n|\r|,", " ") : ""))


            .ForMember(dest => dest.Technician, opt => opt.MapFrom(src => src.Current_Technician))
            .ForMember(dest => dest.TechnicianName, opt => opt.MapFrom(src => src.Technician_Name_History))
            .ForMember(dest => dest.DispatcherName, opt => opt.MapFrom(src => src.Dispatcher_Name_History))
            .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Current_Supervisor_Name))

            ;
            //current techniciaaaaaaaan

            #endregion


        }





    }
}
