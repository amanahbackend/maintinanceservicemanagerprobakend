﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CommonEnums;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;
using Utilites.PACI;
using Ghanim.Models.Entities;
using Ghanim.Models.Views;
using Utilites;

namespace Ghanim.API
{
    public partial class MappingProfile
    {
        public void MapForOrderPrintOut()
        {


            CreateMap<View_OrderPrintOutObject, OrderViewModel>()
                    .ForMember(dest => dest.RejectionReason, opt => opt.Ignore())
                    .ForMember(dest => dest.RejectionReasonId, opt => opt.Ignore())
                    .ForMember(dest => dest.AcceptanceFlag, opt => opt.Ignore())
                    .ForMember(dest => dest.IsAccomplish, opt => opt.Ignore())
                    .ForMember(dest => dest.OnHoldCount, opt => opt.Ignore())
                    .ForMember(dest => dest.IsRepeatedCall, opt => opt.Ignore())
                    .ForMember(dest => dest.IsExceedTime, opt => opt.Ignore())
                    .ForMember(dest => dest.TechId, opt => opt.Ignore())
                    .ForMember(dest => dest.TechName, opt => opt.Ignore())
                    .ForMember(dest => dest.TeamName, opt => opt.Ignore())
                    .ForMember(dest => dest.TechNotes, opt => opt.Ignore())
                    .ForMember(dest => dest.Pin, opt => opt.Ignore())
                    .ForMember(dest => dest.CurrentUserId, opt => opt.Ignore())
                    .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
                    .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
                    .ForMember(dest => dest.FK_DeletedBy_Id, opt => opt.Ignore())
                    .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                    .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore())
                    .ForMember(dest => dest.DeletedDate, opt => opt.Ignore())
                    .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
                    ;

            //CreateMap<View_OrderPrintOutObject,View_OrderPrintOutViewModel>()
            //    .ForMember(dest => dest.TechName, opt => opt.Ignore());
            

        }





    }
}
