﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CommonEnums;
using Ghanim.API.ViewModels.PaymentTransaction;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.Utilites;
using Utilites.PACI;
using Ghanim.Models.Entities;
using Ghanim.Models.Payment.Entities;
using Ghanim.Models.Views;
using Utilites;

namespace Ghanim.API
{
    public partial class MappingProfile
    {
        public void MappingProfilePayment()
        {


            CreateMap<PaymentTransactionViewModel, PaymentTransaction>()
                    .ForMember(dest => dest.Order, opt => opt.Ignore())
                    .ForMember(dest => dest.OrderHistoryStatus, opt => opt.Ignore())
                    .ForMember(dest => dest.FK_CreatedBy, opt => opt.Ignore())
                    .ForMember(dest => dest.CurrentDispatcher, opt => opt.Ignore())
                    .ForMember(dest => dest.CurrentSupervisor, opt => opt.Ignore())
                    .ForMember(dest => dest.CurrentTechnician, opt => opt.Ignore())
                    .ForMember(dest => dest.CurrentForeman, opt => opt.Ignore())
                    .ForMember(dest => dest.ServiceType, opt => opt.Ignore())
                    .ForMember(dest => dest.CurrentUserId, opt => opt.MapFrom(src => src.FK_CreatedBy_Id))


                    ;

            CreateMap<PaymentTransaction, PaymentTransactionViewModel>()
                .ForMember(dest => dest.OrderHistoryStatus, opt => opt.MapFrom(src => src.OrderHistoryStatus.Name))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.FK_CreatedBy_Id == null ? null : src.FK_CreatedBy.FullName))
                .ForMember(dest => dest.CurrentDispatcher, opt => opt.MapFrom(src => src.CurrentDispatcherId == null ? null : src.CurrentDispatcher.User.FullName))
                .ForMember(dest => dest.CurrentSupervisor, opt => opt.MapFrom(src => src.CurrentSupervisorId == null ? null : src.CurrentSupervisor.User.FullName))
                .ForMember(dest => dest.CurrentTechnician, opt => opt.MapFrom(src => src.CurrentTechnicianId == null ? null : src.CurrentTechnician.User.FullName))
                .ForMember(dest => dest.CurrentForeman, opt => opt.MapFrom(src => src.CurrentForemanId == null ? null : src.CurrentForeman.User.FullName))
                .ForMember(dest => dest.ServiceType, opt => opt.MapFrom(src => src.ServiceType.Name))
                .ForMember(dest => dest.OrderHistoryStatus, opt => opt.MapFrom(src => src.OrderHistoryStatus.Name))

                .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.Order.Code))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.Order.Type.Code))
                .ForMember(dest => dest.OrderContractNom, opt => opt.MapFrom(src => src.Order.ContractCode))
                .ForMember(dest => dest.OrderContractType, opt => opt.MapFrom(src => src.Order.ContractTypeId == null ? null : src.Order.ContractType.Name))
                .ForMember(dest => dest.OrderAreaNumber, opt => opt.MapFrom(src => src.Order.Area.Area_No))
                .ForMember(dest => dest.OrderAreaDescription, opt => opt.MapFrom(src => src.Order.Area.Name))
                .ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Order.CompanyCodeId))
                .ForMember(dest => dest.CompanySAPId, opt => opt.MapFrom(src => src.Order.CompanyCodeId))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Order.CompanyCode.Name))


                .ForMember(dest => dest.OrderCustomerName, opt => opt.MapFrom(src => src.Order.CustomerName))
                .ForMember(dest => dest.OrderCustomerPhone, opt => opt.MapFrom(src => src.Order.PhoneOne))
                .ForMember(dest => dest.OrderCustomerSAPID, opt => opt.MapFrom(src => src.Order.CustomerCode))
                .ForMember(dest => dest.PaymentSource, opt => opt.MapFrom(src => "TechApp"))
                .ForMember(dest => dest.OrderProblemCode, opt => opt.MapFrom(src => src.Order.Problem.Code))
                .ForMember(dest => dest.OrderProblemName, opt => opt.MapFrom(src => src.Order.Problem.Name))
                ;




            CreateMap<PaymentTransaction, PaymentTransactionExcellViewModel>()
                .ForMember(dest => dest.Order_Id, opt => opt.MapFrom(src => src.Order.Code))
                .ForMember(dest => dest.Amount_Collected, opt => opt.MapFrom(src => src.Payment.ToString()))
                .ForMember(dest => dest.System_Receipt_No, opt => opt.MapFrom(src => src.Receipt ?? ""))
                .ForMember(dest => dest.Manual_Receipt_No, opt => opt.MapFrom(src => src.ManualReceiptNumber ?? ""))
                .ForMember(dest => dest.Order_Creation_Date,
                    opt => opt.MapFrom(src => src.Order.SAP_CreatedDate.ToString(" dd-MM-yyyy")))

                .ForMember(dest => dest.Order_Type, opt => opt.MapFrom(src => src.Order.Type.Name))


                .ForMember(dest => dest.Contract_No, opt => opt.MapFrom(src => src.Order.ContractCode))
                .ForMember(dest => dest.Contract_Type, opt => opt.MapFrom(src => src.Order.ContractType.Name))
                .ForMember(dest => dest.Order_Expiry_Date,
                    opt => opt.MapFrom(src =>
                        src.Order.ContractExpiryDate == null
                            ? ""
                            : ((DateTime)src.Order.ContractExpiryDate).ToString(" dd-MM-yyyy")))
                .ForMember(dest => dest.Collector_Name,
                    opt => opt.MapFrom(src => src.FK_CreatedBy_Id == null ? "" : src.FK_CreatedBy.FullName))
                .ForMember(dest => dest.Collector_Pf,
                    opt => opt.MapFrom(src => src.FK_CreatedBy_Id == null ? "" : src.FK_CreatedBy.PF))
                .ForMember(dest => dest.Collector_User_Type,
                    opt => opt.MapFrom(src =>
                        src.FK_CreatedBy_Id == null ? "" : src.FK_CreatedBy.UserTypeId.ToString()))

                .ForMember(dest => dest.Payment_Mode, opt => opt.MapFrom(src => src.PaymentMode.ToString()))
                .ForMember(dest => dest.Payment_Method, opt => opt.MapFrom(src => src.PaymentMethod.ToString()))
                .ForMember(dest => dest.Payment_Date, opt => opt.MapFrom(src => src.CreatedDate.ToString(" dd-MM-yyyy")))
                .ForMember(dest => dest.Order_Area_Number, opt => opt.MapFrom(src => src.Order.Area.Area_No))
                .ForMember(dest => dest.Order_Area_Description, opt => opt.MapFrom(src => src.Order.Area.Name))
                .ForMember(dest => dest.Service_Type, opt => opt.MapFrom(src => src.ServiceType.Name))
                ;
            //CreateMap<View_OrderPrintOutObject,View_OrderPrintOutViewModel>()
            //    .ForMember(dest => dest.TechName, opt => opt.Ignore());


        }





    }
}
