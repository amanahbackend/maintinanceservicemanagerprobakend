﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.IdentityService
{
    public interface IIdentityService
    {
        Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string userId);

        Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserNames(List<string> usernames);
        Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAll();

        Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIds(List<string> ids);


        Task<ProcessResultViewModel<bool>> ResetPassword(ResetUsersPasswordViewModel model);
        Task<ProcessResultViewModel<LoginResultViewModel>> Login(LoginViewModel loginViewModel);

        ProcessResultViewModel<bool> Logout(string userId, string deviceId);
        Task<ProcessResultViewModel<bool>> ToggleActivation(string id);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> GetByUserName(string username);

        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUser(
          ApplicationUserViewModel userViewModel);
        Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIdsWithBlocked(List<string> ids);
    }
}