﻿using AutoMapper;
using Ghanim.API.Utilities;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.GenericListToExcel;
using Utilites;
using System.IO;
using Ghanim.API.Enums;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Models.Context;

namespace Ghanim.API.ServiceCommunications.UserManagementService.IdentityService
{
    public class IdentityService : IIdentityService
    {
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IApplicationUserManager _applicationUserManager;
        private readonly IApplicationUserHistoryManager _applicationUserHistoryManager;
        private readonly IConfigurationRoot _configuration;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IPasswordTokenPinManager _passwordTokenPinManager;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IUploadImageFileManager _imageManager;
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly ApplicationDbContext _applicationDbContext;
        private IApplicationUserHistoryManager ApplicationUserHistorymanager
        {
            get
            {
                return _serviceprovider.GetService<IApplicationUserHistoryManager>();
            }
        }

        IServiceProvider _serviceprovider;

        public IdentityService(IServiceProvider serviceprovider, IApplicationUserManager applicationUserManager,
            IHostingEnvironment hostingEnv, IConfigurationRoot configuration,
            IPasswordTokenPinManager passwordTokenPinManager, IPasswordHasher<ApplicationUser> passwordHasher,
            IUploadImageFileManager imageManager,
            IProcessResultMapper processResultMapper,
            IUserDeviceManager userDeviceManager,
            IApplicationUserHistoryManager applicationUserHistoryManager,
            ApplicationDbContext applicationDbContext
        )
        {
            _serviceprovider = serviceprovider;
            _passwordHasher = passwordHasher;
            _passwordTokenPinManager = passwordTokenPinManager;
            _configuration = configuration;
            _hostingEnv = hostingEnv;
            _applicationUserManager = applicationUserManager;
            _imageManager = imageManager;
            _userDeviceManager = userDeviceManager;
            _processResultMapper = processResultMapper;
            _applicationUserHistoryManager = applicationUserHistoryManager;
            _applicationDbContext = applicationDbContext;
        }
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string userId)
        {
            var user = await _applicationUserManager.Get(userId);
            if (user != null)
            {
                var userModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);
                return ProcessResultViewModelHelper.Succedded(userModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "User can not be found");
            }
        }

        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserNames(List<string> usernames)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = await _applicationUserManager.GetByUsernames(usernames);
                var models = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAll()
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = await _applicationUserManager.GetAll();
                if (users.Count > 0)
                {
                    List<ApplicationUserViewModel> userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, "No users found");
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIds(List<string> ids)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = await _applicationUserManager.GetByIds(ids);
                var models = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                result = ProcessResultViewModelHelper.Succedded(models);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }

            return result;
        }



        public async Task<ProcessResultViewModel<bool>> ResetPassword(ResetUsersPasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                for (int i = 0; i < model.Usernames.Count(); i++)
                {
                    var isReset = await ResetUserPassword(model.Usernames[i], model.NewPassword);
                    result = ProcessResultViewModelHelper.Succedded(isReset);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }
        private Task<bool> ResetUserPassword(string userName, string newPassword)
        {
            Task<bool> result = null;
            try
            {
                result = _applicationUserManager.ResetPassword(userName, newPassword);
            }
            catch (Exception ex)
            {
                result = Task.FromResult(false);
            }
            return result;
        }
        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login([FromBody]LoginViewModel loginViewModel)
        {
            try
            {
                var response = await TokenManager.GetToken(loginViewModel, _applicationUserManager, _passwordHasher, _userDeviceManager);
                var UserId = response.Item1.Id;
                var OldDevicesForLoggedUser = _applicationDbContext.UserDevices.Where(x => x.Fk_AppUser_Id == UserId);


                //if (OldDevicesForLoggedUser.Any())
                //{
                //    _applicationDbContext.UserDevices.RemoveRange(OldDevicesForLoggedUser);
                //}
                if (response != null && response.Item2 != null && response.Item1 != null)
                {
                    LoginResultViewModel loginResult = new LoginResultViewModel()
                    {
                        Token = new JwtSecurityTokenHandler().WriteToken(response.Item2),
                        UserName = response.Item1.UserName,
                        Name = response.Item1.FirstName + " " + response.Item1.LastName + (!string.IsNullOrEmpty(response.Item1.PF) ? " - " + response.Item1.PF : string.Empty),
                        Roles = response.Item1.RoleNames,
                        UserId = response.Item1.Id
                    };
                    ApplicationUserHistoryViewModel ApplicationUserHistoryRes = new ApplicationUserHistoryViewModel()
                    {
                        Token = loginResult.Token,
                        LoginDate = DateTime.UtcNow,
                        UserId = response.Item1.Id,
                        UserType = loginResult.Roles.FirstOrDefault(),
                        DeveiceId = loginViewModel.DeviceId
                    };

                    ApplicationUserHistory userHistory = Mapper.Map<ApplicationUserHistoryViewModel, ApplicationUserHistory>(ApplicationUserHistoryRes);
                    var ApplicationUserHistoryResult = ApplicationUserHistorymanager.Add(userHistory);

                    return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult);
                }
                else if (response == null && await _applicationUserManager.IsUserDeactivated(loginViewModel.Username))
                {
                    return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "User Was Blocked");
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "Username or password is invalid");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, ex.Message);
            }
        }

        public ProcessResultViewModel<bool> Logout(string userId, string deviceId)
        {
            var loginUserRes = ApplicationUserHistorymanager.GetLoginUser(userId, deviceId);
            if (loginUserRes != null && loginUserRes.Data.Count > 0)
            {
                foreach (var item in loginUserRes.Data)
                {
                    item.LogoutDate = DateTime.UtcNow;
                    var logoutUserRes = ApplicationUserHistorymanager.Update(item);
                }
                //_userDeviceManager.DeleteAzure(deviceId);
                //return ProcessResultViewModelHelper.Succedded(true);
            }
            //else
            //{
            //    return ProcessResultViewModelHelper.Failed<bool>(false, "This User can't logout. ");
            //}

            _userDeviceManager.DeleteAzure(deviceId);
            return ProcessResultViewModelHelper.Succedded(true);
        }

        public async Task<ProcessResultViewModel<bool>> ToggleActivation(string id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isToggled = await _applicationUserManager.ToggleActivation(id);
                result = ProcessResultViewModelHelper.Succedded(isToggled);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }


        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetByUserName(string username)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            try
            {
                ApplicationUser user = await _applicationUserManager.GetBy(username);
                if (user != null)
                {
                    //user.Picture = ProfilePictureManager.GetProfilePictureBase64(username, _hostingEnv);
                    var userModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    userModel = ProfilePictureManager.BindFileURL(userModel, _configuration);

                    result = ProcessResultViewModelHelper.Succedded(userModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "User not found");
                }

            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, ex.Message);
            }
            return result;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUser(ApplicationUserViewModel userViewModel)
        {
            if (userViewModel.Picture != null)
            {
                userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                    userViewModel.UserName, _imageManager, _hostingEnv);
            }
            var user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel);
            var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
            if (updateUserResult.IsSucceeded)
            {
                user = updateUserResult.Data;
                userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                return ProcessResultViewModelHelper.Succedded(userViewModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, updateUserResult.Exception.Message);
            }
        }
        public Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIdsWithBlocked(List<string> ids)
        {
            throw new NotImplementedException();
        }
    }
}
