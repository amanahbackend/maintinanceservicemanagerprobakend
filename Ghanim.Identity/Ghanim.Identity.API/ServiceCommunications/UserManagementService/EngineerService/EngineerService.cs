﻿using AutoMapper;
using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.EngineerService
{
    public class EngineerService : IEngineerService
    {
        private readonly IEngineerManager manger;
        private readonly IMapper mapper;

        public EngineerService(IEngineerManager _manger, IMapper _mapper)
        {
            manger = _manger;
            mapper = _mapper;
        }

        public ProcessResultViewModel<EngineerViewModel> GetEngineerByUserId(string userId)
        {
            try
            {
                Engineer entityResult = manger.GetByUserId(userId)?.Data;
                EngineerViewModel result = mapper.Map<Engineer, EngineerViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<EngineerViewModel>(null, ex.Message);
            }
        }
    }
}
