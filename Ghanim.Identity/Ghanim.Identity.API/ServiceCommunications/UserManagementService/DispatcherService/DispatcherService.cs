﻿using AutoMapper;
using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService
{
    public class DispatcherService : IDispatcherService
    {
        private readonly IDispatcherManager manger;
        private readonly IMapper mapper;
        private readonly IIdentityService identityUserService;
        IProcessResultMapper _processResultMapper;


        public DispatcherService(IDispatcherManager _manger, IMapper _mapper, IIdentityService _identityUserService, IProcessResultMapper processResultMapper)
        {
            manger = _manger;
            mapper = _mapper;
            identityUserService = _identityUserService;
            _processResultMapper = _processResultMapper;
        }

        public ProcessResultViewModel<List<DispatcherViewModel>> GetDispatchersByDivision(int divisonId)
        {
            try
            {
                //Filtering by divisionId
                List<Dispatcher> dispatchersTemp = manger.GetAll(x => x.DivisionId == divisonId).Data.ToList();
                if (dispatchersTemp.Count > 0)
                {
                    //List<string> userIds = dispatchersTemp.Select(x => x.UserId).ToList();
                    //ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<DispatcherViewModel> result = mapper.Map<List<Dispatcher>, List<DispatcherViewModel>>(dispatchersTemp);
                    //// YARAB SAM7NY .. Fawzy
                    //for (int i = 0; i < result.Count; i++)
                    //{
                    //    mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatchersTemp[i], result[i]);
                    //}
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<DispatcherViewModel>(), "no dispatchers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherViewModel>>(null, ex.Message);
            }
        }

        public ProcessResultViewModel<DispatcherViewModel> GetDispatcherById(int id)
        {
            var dispatcher =  manger.Get(id)?.Data;
            if (dispatcher != null)
            {
                var userModel = mapper.Map<Dispatcher, DispatcherViewModel>(dispatcher);
                return ProcessResultViewModelHelper.Succedded(userModel);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<DispatcherViewModel>(null, "Dispatcher can not be found");
            }
        }


       
        public ProcessResultViewModel<DispatcherViewModel> GetDispatcherByUserId(string userId)
        {
            try
            {
                var entityResult = manger.GetAll(x => x.UserId == userId).Data.FirstOrDefault();
                DispatcherViewModel result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<DispatcherViewModel>(null, ex.Message);
            }
        }

        public async Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetAll()
        {
            try
            {

                // implement the base Get() method
                var dispatchersentites = manger.GetAllQuerable()?.Data.ProjectTo<DispatcherViewModel>().ToList();
                //List<DispatcherViewModel> result = mapper.Map<List<Dispatcher>, List<DispatcherViewModel>>(dispatchersentites);

                return ProcessResultViewModelHelper.Succedded(dispatchersentites);

            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherViewModel>>(null, ex.Message);
            }
        }

        public ProcessResultViewModel<List<DispatcherViewModel>> GetDispatchersByIds(List<int> ids)
        {
            try
            {
                var entityResult = manger.GetByIds(ids)?.Data;
                List<DispatcherViewModel> result = mapper.Map<List<Dispatcher>, List<DispatcherViewModel>>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherViewModel>>(null, ex.Message);
            }            
        }

        public async Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetDispatchersBySupervisorId(int id)
        {
            try
            {
                //TODO refactor Projection example
                //Get dispatchers from Dispatcher table
                List<DispatcherViewModel> dispatchersTemp = manger.GetAll(x => x.SupervisorId == id,
                    x => x.User,
                    x => x.CostCenter,
                    x => x.Division
                ).Data.Select(d=> mapper.Map<Dispatcher, DispatcherViewModel>(d)).ToList();
                if (dispatchersTemp.Count > 0)
                {
                    //List<string> userIds = dispatchersTemp.Select(x => x.UserId).ToList();
                    //ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    //List<DispatcherViewModel> result = mapper.Map<List<Dispatcher>, List<DispatcherViewModel>>(dispatchersTemp);

                    ////Get dispatchers from AspNetUsers table (identity service)
                    //var identity = await identityUserService.GetByUserIds(result.Select(x => x.UserId).ToList());

                    ////Map FullName and Phone from data we retreived from identity service
                    //foreach (var item in result)
                    //{
                    //    var user = identity.Data?.FirstOrDefault(x => x.Id == item.UserId);
                    //    item.FullName = user?.FullName;
                    //    item.Phone = user?.Phone;
                    //}

                    //// YARAB SAM7NY .. Fawzy
                    //for (int i = 0; i < result.Count; i++)
                    //{
                    //    mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatchersTemp[i], result[i]);
                    //}
                    return ProcessResultViewModelHelper.Succedded(dispatchersTemp);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<DispatcherViewModel>(), "no dispatchers found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherViewModel>>(null, ex.Message);
            }
        }
        //public  ProcessResultViewModel<DispatcherViewModel> AddDispatcher(DispatcherViewModel model)
        //{
        //    var entityModel = mapper.Map<DispatcherViewModel, Dispatcher>(model);
        //    var entityResult = manger.Add(entityModel);
        //    return _processResultMapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
        //}


    }
}