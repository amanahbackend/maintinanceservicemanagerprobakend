﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService
{
    public interface IDispatcherService 
    {
        ProcessResultViewModel<List<DispatcherViewModel>> GetDispatchersByDivision(int divisonId);
        ProcessResultViewModel<DispatcherViewModel> GetDispatcherById(int id);
        ProcessResultViewModel<DispatcherViewModel> GetDispatcherByUserId(string userId);
        Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetAll();
        ProcessResultViewModel<List<DispatcherViewModel>> GetDispatchersByIds(List<int> ids);
        Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetDispatchersBySupervisorId(int id);
       // Task<ProcessResultViewModel<bool>> AddDispatcher(DispatcherViewModel model);
    }
}