﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.TeamService
{
    public interface ITeamService
    {
       Task< ProcessResultViewModel<List<TeamViewModel>>> GetTeamsByDispatcherId(int dispatcherId);
        ProcessResultViewModel<TeamViewModel> GetTeamById(int Id);
        ProcessResultViewModel<List<TeamViewModel>> GetTeamsByIds(List<int> ids);
        ProcessResultViewModel<List<TeamIdForemanIdNameViewModel>> GetTeamsWithBlockedByIds(List<int> ids);
        Task<ProcessResultViewModel<List<TeamViewModel>>> GetTeamsByDispatcherIdusingTracking(int dispatcherId);
    }
}
