﻿
using DnsClient;

using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Google;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;
using Utilities.ProcessingResult;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace Ghanim.API.ServiceCommunications.UserManagementService.TeamService
{
    public class TeamService : ITeamService
    {
        private readonly ITeamManager _manager;
        private readonly IProcessResultMapper _processResultMapper;
        private ApplicationDbContext _context;
        public readonly  IMapper mapper;


        public TeamService(ITeamManager manager, IProcessResultMapper processResultMapper, IMapper _mapper )
        {
            _manager = manager;
            _processResultMapper = processResultMapper;
            mapper = _mapper;


        }

        public async Task< ProcessResultViewModel<List<TeamViewModel>>> GetTeamsByDispatcherId(int dispatcherId)
        {
            try
            {
                var entityResult =  await  _manager.GetByDispatcherId(dispatcherId);
                var result = mapper.Map<List<Team>, List<TeamViewModel>>(entityResult.Data);
                return new ProcessResultViewModel<List<TeamViewModel>>() { Data = result, IsSucceeded = true };


                //var result = _processResultMapper.Map<List<Team>, List<TeamViewModel>>entityResult(entityResult);
               // return result;

            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamViewModel>>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTeamsByDispatcherId;
            //    var url = $"{baseUrl}/{requestedAction}/{dispatcherId}";
            //    return await GetByUriCustomized<List<TeamViewModel>>(url);
            //}
            //return null;
        }
        public async Task<ProcessResultViewModel<List<TeamViewModel>>> GetTeamsByDispatcherIdusingTracking(int dispatcherId)
        {
            try
            {
                var entityResult = await _context.Teams.AsNoTracking()
                    .Where( x=> x.DispatcherId == dispatcherId && x.Members.Any())
                    .ToListAsync();
                var result = mapper.Map<List< Team>,List< TeamViewModel>>(entityResult);
                // return result ;              // ProcessResultHelper.Succedded<List<TeamViewModel>>(result);
                return ProcessResultViewModelHelper.Succedded<List<TeamViewModel>>(result, "");
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamViewModel>>(null, ex.Message);
            }
           
        }
        public ProcessResultViewModel<TeamViewModel> GetTeamById(int Id)
        {
            try
            {
                var entityResult = _manager.Get(Id);
                var result = _processResultMapper.Map<Team, TeamViewModel>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TeamViewModel>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTeamById;
            //    var url = $"{baseUrl}/{requestedAction}/{Id}";
            //    return await GetByUriCustomized<TeamViewModel>(url);
            //}
            //return null;
        }

        public ProcessResultViewModel<List<TeamViewModel>> GetTeamsByIds(List<int> ids)
        {
            try
            {
                var entityResult = _manager.GetByIds(ids);
                var result = _processResultMapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamViewModel>>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTeamsByIds;
            //    var url = $"{baseUrl}/{requestedAction}";
            //    return await PostCustomize<List<int>, List<TeamViewModel>>(url, ids);
            //}
            //return null;
        }
        public ProcessResultViewModel<List<TeamIdForemanIdNameViewModel>> GetTeamsWithBlockedByIds(List<int> ids)
        {
            try
            {
                var entityResult = _manager.GetAllQuerableWithBlocked().Data.Where(x => ids.Contains(x.Id))
                    .ProjectTo<TeamIdForemanIdNameViewModel>();
                var result = ProcessResultViewModelHelper.Succedded(entityResult.ToList());
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamIdForemanIdNameViewModel>>(null, ex.Message);
            }

        }

    }
}
