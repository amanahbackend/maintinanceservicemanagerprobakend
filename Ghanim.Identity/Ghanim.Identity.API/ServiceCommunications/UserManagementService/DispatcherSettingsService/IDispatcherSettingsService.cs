﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.DispatcherSettingsService
{
    public interface IDispatcherSettingsService 
    {
        ProcessResultViewModel<List<DispatcherSettingsViewModel>> GetDispatcherSettingsByOrder(SettingsViewModel model);
    }
}
