﻿using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using AutoMapper;
using Ghanim.Models.Entities;
using Ghanim.BLL.IManagers;

namespace Ghanim.API.ServiceCommunications.UserManagementService.DispatcherSettingsService
{
    public class DispatcherSettingsService : IDispatcherSettingsService
    {
        private readonly IDispatcherSettingsManager manager;
        private readonly IMapper mapper;

        public DispatcherSettingsService(IDispatcherSettingsManager _manager, IMapper _mapper)
        {
            manager = _manager;
            mapper = _mapper;
        }

        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> GetDispatcherSettingsByOrder(SettingsViewModel settings)
        {
            try
            {
                var dispatcherSettings = manager.GetAll(x => x.DivisionId == settings.DivisionId && x.AreaId == settings.AreaId && x.OrderProblemId == settings.ProblemId).Data;
                if (dispatcherSettings != null && dispatcherSettings.Count > 0)
                {
                    List<DispatcherSettingsViewModel> result = mapper.Map<List<DispatcherSettings>, List<DispatcherSettingsViewModel>>(dispatcherSettings);
                    return ProcessResultViewModelHelper.Succedded<List<DispatcherSettingsViewModel>>(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded<List<DispatcherSettingsViewModel>>(null, "no dispatchers found");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
