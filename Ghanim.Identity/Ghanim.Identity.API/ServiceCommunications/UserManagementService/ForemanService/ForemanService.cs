﻿using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using Ghanim.BLL.Managers;
using Ghanim.Models.Entities;
using AutoMapper;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.BLL.IManagers;
using System.Linq;
using AutoMapper.QueryableExtensions;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Microsoft.EntityFrameworkCore;

namespace Ghanim.API.ServiceCommunications.UserManagementService.ForemanService
{
    public class ForemanService : IForemanService
    {
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IForemanManager manager;
        private readonly ITeamManager teamManager;
        private readonly IIdentityService identityService;
        private readonly ITeamService teamService;
        private readonly IMapper mapper;

        public ForemanService(IProcessResultMapper processResultMapper, ITeamManager _teamManager, ITeamService _teamService, IIdentityService _identityService, IForemanManager _manager, IMapper _mapper)
        {
            identityService = _identityService;
            manager = _manager;
            mapper = _mapper;
            teamService = _teamService;
            teamManager = _teamManager;
            _processResultMapper = processResultMapper;
        }

        public async Task<ProcessResultViewModel<List<ForemanViewModel>>> GetAll()
        {
            try
            {
                // implement the base Get() method
                var formans = manager.GetAllQuerable().Data;
                var formansViewModels = formans.ProjectTo<ForemanViewModel>();
                return ProcessResultViewModelHelper.Succedded(await formansViewModels.ToListAsync());
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<ForemanViewModel>>(null, ex.Message);
            }
        }

        public ProcessResultViewModel<ForemanViewModel> GetForemanByUserId(string userId)
        {
            try
            {
                var entityResult = manager.Get(x => x.UserId == userId).Data;
                ForemanViewModel result = mapper.Map<Foreman, ForemanViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ForemanViewModel>(null, ex.Message);
            }
        }
        public ProcessResultViewModel<ForemanViewModel> GetForemanById(int Id)
        {
            try
            {
                var entityResult = manager.Get(Id)?.Data;
                ForemanViewModel result = mapper.Map<Foreman, ForemanViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ForemanViewModel>(null, ex.Message);
            }

            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetForemanById;
            //    var url = $"{baseUrl}/{requestedAction}/{userId}";
            //    return await GetByUriCustomized<ForemanViewModel>(url);
            //}
            //return null;
        }

        public ProcessResultViewModel<List<ForemanViewModel>> GetForemanByIds(List<int> ids)
        {
            try
            {
                var entityResult = manager.GetByIds(ids)?.Data;
                List<ForemanViewModel> result = mapper.Map<List<Foreman>, List<ForemanViewModel>>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<ForemanViewModel>>(null, ex.Message);
            }

            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetForemansByIds;
            //    var url = $"{baseUrl}/{requestedAction}";
            //    return await PostCustomize<List<int>, List<ForemanViewModel>>(url, ids);
            //}
            //return null;
        }

        public ProcessResultViewModel<List<ForemanViewModel>> GetForeManByTeamsIds(List<int> ids)
        {
            try
            {
                ids = ids.Distinct().ToList();
                var teams = teamManager.GetByIds(ids)?.Data;
                var entityResult = manager.GetAll(f => teams.Any(x => x.ForemanId == f.Id))?.Data?.Distinct().ToList();
                List<ForemanViewModel> result = mapper.Map<List<Foreman>, List<ForemanViewModel>>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<ForemanViewModel>>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetForemansByIds;
            //    var url = $"{baseUrl}/{requestedAction}";
            //    return await PostCustomize<List<int>, List<ForemanViewModel>>(url, ids);
            //}
            //return null;
        }

        public ProcessResult<bool> Put(ForemanViewModel model)
        {
            try
            {
                ProcessResult<bool> result = null;
                // retrive old data for foremen to check pf number and Division
                var oldForemenDat = GetForemanById(model.Id);

                if (oldForemenDat.Data.PF != model.PF)
                {
                    return ProcessResultHelper.Failed<bool>(false, null);
                }
                else
                {
                    //Implementing the base Put method from inherited class
                    var EntityResult = mapper.Map<ForemanViewModel, Foreman>(model);
                    result = manager.Update(EntityResult);
                    return result;
                }

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, null);
            }
        }

    }
}
