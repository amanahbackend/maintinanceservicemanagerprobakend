﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.ForemanService
{
    public interface IForemanService
    {
        ProcessResultViewModel<ForemanViewModel> GetForemanByUserId(string userId);
        ProcessResultViewModel<ForemanViewModel> GetForemanById(int userId);
        Task<ProcessResultViewModel<List<ForemanViewModel>>> GetAll();
        ProcessResultViewModel<List<ForemanViewModel>> GetForemanByIds(List<int> ids);
        ProcessResultViewModel<List<ForemanViewModel>> GetForeManByTeamsIds(List<int> list);
        ProcessResult<bool> Put(ForemanViewModel model);

    }
}