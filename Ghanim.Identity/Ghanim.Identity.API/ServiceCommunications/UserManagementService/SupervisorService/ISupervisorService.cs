﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService
{
    public interface ISupervisorService 
    {
        ProcessResultViewModel<List<SupervisorViewModel>> GetSupervisorsByDivisionId(int divisonId);
        ProcessResultViewModel<SupervisorViewModel> GetSupervisorByDivisionId(int divisonId);
        ProcessResultViewModel<SupervisorViewModel> GetSupervisorByUserId(string userId);
        ProcessResultViewModel<List<SupervisorViewModel>> GetSupervisorsByIds(List<int> ids);

    }
}