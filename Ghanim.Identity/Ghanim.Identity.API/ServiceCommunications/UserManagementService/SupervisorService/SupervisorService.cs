﻿using AutoMapper;
using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Order.Enums;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService
{
    public class SupervisorService : ISupervisorService
    {
        private readonly IProcessResultMapper _processResultMapper;
        private readonly ISupervisorManager _manager;
        private readonly IMapper _mapper;

        public SupervisorService(IProcessResultMapper processResultMapper, ISupervisorManager manager, IMapper mapper)
        {
            _manager = manager;
            _mapper = mapper;
            _processResultMapper = processResultMapper;
        }

        public ProcessResultViewModel<SupervisorViewModel> GetSupervisorByDivisionId(int divisonId)
        {
            try
            {
                var supervisorTemp = _manager.Get(x => x.DivisionId == divisonId);
                if (supervisorTemp != null)
                {
                    var result = _processResultMapper.Map<Supervisor, SupervisorViewModel>(supervisorTemp);
                    return result;
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded<SupervisorViewModel>(null, "no supervisor found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<SupervisorViewModel>(null, ex.Message);
            }
            //string baseUrl =  _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetSupervisorByDivisionId;
            //    var url = $"{baseUrl}/{requestedAction}/{divisonId}";
            //    return await GetByUriCustomized<SupervisorViewModel>(url);
            //}
            //return null;
        }


        public ProcessResultViewModel<List<SupervisorViewModel>> GetSupervisorsByIds(List<int> ids)
        {
            try
            {
                var supervisorTemp = _manager.GetByIds(ids);
                if (supervisorTemp != null)
                {
                    var result = _processResultMapper.Map<List<Supervisor>, List<SupervisorViewModel>>(supervisorTemp);
                    return result;
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded<List<SupervisorViewModel>>(null, "no supervisor found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<SupervisorViewModel>>(null, ex.Message);
            }

            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetSupervisorsByIds;
            //    var url = $"{baseUrl}/{requestedAction}";
            //    return await PostCustomize<List<int>, List<SupervisorViewModel>>(url, ids);
            //}
            //return null;
        }

        public ProcessResultViewModel<List<SupervisorViewModel>> GetSupervisorsByDivisionId(int divisonId)
        {
            try
            {
                var supervisorsTemp = _manager.GetAll(x => x.DivisionId == divisonId);
                var count = supervisorsTemp?.Data?.ToList()?.Count;
                if (supervisorsTemp != null && count > 0)
                {
                    var result = _processResultMapper.Map<List<Supervisor>, List<SupervisorViewModel>>(supervisorsTemp);
                    return result;
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded<List<SupervisorViewModel>>(null, "no supervisor found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<SupervisorViewModel>>(null, ex.Message);
            }

            //string baseUrl =  _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetSupervisorsByDivisionId;
            //    var url = $"{baseUrl}/{requestedAction}/{divisonId}";
            //    return await GetByUriCustomized<List<SupervisorViewModel>>(url);
            //}
            //return null;
        }

        public ProcessResultViewModel<SupervisorViewModel> GetSupervisorByUserId(string userId)
        {
            try
            {
                var entityResult = _manager.Get(x => x.UserId == userId);
                var result = _processResultMapper.Map<Supervisor, SupervisorViewModel>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<SupervisorViewModel>(null, ex.Message);
            }

            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetSupervisorByUserId;
            //    var url = $"{baseUrl}/{requestedAction}/{userId}";
            //    return await GetByUriCustomized<SupervisorViewModel>(url);
            //}
            //return null;
        }

        //Update
        //public async Task<ProcessResultViewModel<bool>> UpdateSuper(SupervisorViewModel model)
        //{
        //    ProcessResultViewModel<bool> resultViewModel = new ProcessResultViewModel<bool>();
        //    try
        //    {
        //       var Entity=Mapper.Map<SupervisorViewModel, Supervisor>(model);

        //        resultViewModel= _manager.Update(Entity);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
        //    }
        //}
    }
}
