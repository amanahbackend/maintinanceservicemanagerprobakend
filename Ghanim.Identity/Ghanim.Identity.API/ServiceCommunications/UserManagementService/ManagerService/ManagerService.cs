﻿using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.ManagerService
{
    public class ManagerService : IManagerService
    {

        private readonly IManagerManager _manager;
        private readonly IProcessResultMapper _processResultMapper;

        public ManagerService(IManagerManager manager, IProcessResultMapper ProcessResultMapper) 
        {
            _manager = manager;
            _processResultMapper = ProcessResultMapper;
        }

        public ProcessResultViewModel<ManagerViewModel> GetManagerByUserId(string userId)
        {

            try
            {
                var entityResult = _manager.Get(x => x.UserId == userId);
                var result = _processResultMapper.Map<Manager, ManagerViewModel>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ManagerViewModel>(null, ex.Message);
            }
            //string baseUrl =  _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetManagerByUserId;
            //    var url = $"{baseUrl}/{requestedAction}/{userId}";
            //    return await GetByUriCustomized<ManagerViewModel>(url);
            //}
            //return null;
        }
    }
}
