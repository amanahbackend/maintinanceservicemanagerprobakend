﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;

using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.ManagerService
{
    public interface IManagerService
    {
        ProcessResultViewModel<ManagerViewModel> GetManagerByUserId(string userId);
    }
}