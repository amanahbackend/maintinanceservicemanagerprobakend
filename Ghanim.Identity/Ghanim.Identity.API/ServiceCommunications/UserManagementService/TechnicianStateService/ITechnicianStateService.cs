﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService
{
    public interface ITechnicianStateService 
    {
        ProcessResultViewModel<TechniciansStateViewModel> Add(TechniciansStateViewModel model);
    }
}