﻿using AutoMapper;
using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;

using Ghanim.BLL.IManagers;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Ghanim.Models.Entities;
namespace Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService
{
    public class TechnicianStateService : ITechnicianStateService
    {
        private readonly ITechniciansStateManager _manager;
        private readonly IMapper _mapper;
        private readonly IProcessResultMapper _processResultMapper;

        public TechnicianStateService(IProcessResultMapper processResultMapper, IMapper mapper, ITechniciansStateManager manager)
        {
            _manager = manager;
            _mapper = mapper;
            _processResultMapper = processResultMapper;
        }

        public ProcessResultViewModel<TechniciansStateViewModel> Add(TechniciansStateViewModel model)
        {
            try
            {
                model.ActionDate = DateTime.UtcNow;
                var entity = _mapper.Map<TechniciansStateViewModel, TechniciansState>(model);
                var resultViewModel = _manager.Add(entity);
                var result = _processResultMapper.Map<TechniciansState, TechniciansStateViewModel>(resultViewModel);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechniciansStateViewModel>(null, ex.Message);
            }
            //    string baseUrl = _settings.Uri;
            //    while (baseUrl.EndsWith('/'))
            //    {
            //        baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //    }
            //    if (!String.IsNullOrEmpty(baseUrl))
            //    {
            //        string requestedAction = _settings.AddTechnicianState;
            //        var url = $"{baseUrl}/{requestedAction}";
            //        return await PostCustomize<TechniciansStateViewModel,TechniciansStateViewModel>(url, model);
            //    }
            //    return null;
            //}
        }

    }
}
