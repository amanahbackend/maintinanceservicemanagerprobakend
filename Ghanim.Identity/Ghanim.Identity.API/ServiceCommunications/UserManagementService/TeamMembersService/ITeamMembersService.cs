﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;

using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService
{
    public interface ITeamMembersService
    {
        ProcessResultViewModel<List<TeamMemberViewModel>> GetMembersByTeamId(int teamId);
        Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMemberUsersByTeamId(int teamId);
        ProcessResultViewModel<List<TeamMemberViewModel>> GetTeamIdByTechnicianMember(int Id);
        ProcessResultViewModel<bool> UnassignedMembers(List<int> membersIds);
        ProcessResultViewModel<TeamMemberViewModel> GetDeafualtTeamMemberIdByTeamId(int Id);
    }
}