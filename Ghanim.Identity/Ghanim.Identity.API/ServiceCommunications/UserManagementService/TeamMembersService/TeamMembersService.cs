﻿using DnsClient;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Utilites.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using CommonEnums;

namespace Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService
{
    public class TeamMembersService : ITeamMembersService
    {
        private readonly ITeamMemberManager _teamMemberManager;
        private readonly IIdentityService _identityUserService;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly ITechnicianManager _technicianManager;
        private readonly IVehicleManager _vehicleManager;
        private readonly ITeamMembersHistoryManager _ITeamMembersHistoryManager;
         

        public TeamMembersService(ITechnicianManager technicianManager, IIdentityService identityUserService, ITeamMemberManager teamMemberManager, IProcessResultMapper processResultMapper, IVehicleManager vehicleManager,
            ITeamMembersHistoryManager ITeamMembersHistoryManager)
        {
            _teamMemberManager = teamMemberManager;
            _processResultMapper = processResultMapper;
            _identityUserService = identityUserService;
            _technicianManager = technicianManager;
            _vehicleManager = vehicleManager;
            _ITeamMembersHistoryManager = ITeamMembersHistoryManager;
        }

        public ProcessResultViewModel<List<TeamMemberViewModel>> GetMembersByTeamId(int teamId)
        {
            try
            {
                ProcessResult<List<TeamMember>> teamMembers = _teamMemberManager.GetByTeamId(teamId);
                if (teamMembers != null)
                {
                    var result = _processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(teamMembers);
                    return result;
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded<List<TeamMemberViewModel>>(null, "no team members found");
                }

            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamMemberViewModel>>(null, ex.Message);
            }
            //string baseUrl =  _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetMembersByTeamId;
            //    var url = $"{baseUrl}/{requestedAction}/{teamId}";
            //    return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            //}
            //return null;
        }

        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMemberUsersByTeamId(int teamId)
        {
            try
            {
                var entityResult = _teamMemberManager.GetByTeamId(teamId);

                var result = _processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);

                if (result.IsSucceeded && result.Data.Count > 0)
                {
                    if (result.Data.Count > 0)
                    {
                        result = await _bindingTechnicianDataToMember(result, entityResult);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamMemberViewModel>>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetMemberUsersByTeamId;
            //    var url = $"{baseUrl}/{requestedAction}/{teamId}";
            //    return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            //}
            //return null;
        }

        public ProcessResultViewModel<List<TeamMemberViewModel>> GetTeamIdByTechnicianMember(int Id)
        {
            try
            {
                var entityResult = _teamMemberManager.GetAll(x => x.MemberType == MemberType.Technician && x.MemberParentId == Id);
                var result = _processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamMemberViewModel>>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTeamIdByTechnicianMember;
            //    var url = $"{baseUrl}/{requestedAction}/{Id}";
            //    return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            //}
            //return null;
        }
        public ProcessResultViewModel<TeamMemberViewModel> GetDeafualtTeamMemberIdByTeamId(int Id)
        {
            try
            {
                var entityResult = _teamMemberManager.GetAllQuerable().Data.ProjectTo<TeamMemberViewModel>().FirstOrDefault(x => x.TeamId == Id && x.IsDefault);
                return ProcessResultViewModelHelper.Succedded(entityResult);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TeamMemberViewModel>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTeamIdByTechnicianMember;
            //    var url = $"{baseUrl}/{requestedAction}/{Id}";
            //    return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            //}
            //return null;
        }

        private async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> _bindingTechnicianDataToMember(ProcessResultViewModel<List<TeamMemberViewModel>> models, ProcessResult<List<TeamMember>> enitityModel)
        {
            var userIds = models.Data.Select(x => x.UserId).ToList();
            var identity = await _identityUserService.GetByUserIds(userIds);

            for (int i = 0; i < models.Data.Count; i++)
            {
                if (models.Data[i].MemberTypeName == "Technician")
                {
                    var technician = _technicianManager.Get(enitityModel.Data[i].MemberParentId);
                    var identityUser = identity.Data.FirstOrDefault(x => x.Id == technician.Data.UserId);

                    if (technician.IsSucceeded && technician.Data != null)
                    {
                        //todo mapping infects erorrs
                        //models.Data[i].CostCenter = technician.Data.CostCenterName;
                        models.Data[i].CostCenterId = technician.Data.CostCenterId;
                        models.Data[i].DivisionId = technician.Data.DivisionId;
                        //models.Data[i].DivisionName = technician.Data.DivisionName;
                        models.Data[i].PF = technician.Data.User.PF;
                        models.Data[i].UserId = technician.Data.UserId;
                        models.Data[i].MemberName = identityUser?.FirstName;
                        models.Data[i].MemberPhone = identityUser?.Phone;
                        models.Data[i].FullName = identityUser?.FullName;
                    }
                }
            }
            return models;
        }



        public ProcessResultViewModel<bool> UnassignedMembers(List<int> membersIds)
        {
            ProcessResultViewModel<bool> result = null;
            ProcessResult<bool> entityResult = new ProcessResult<bool> { Data = false };
            foreach (var memberId in membersIds)
            {
                var memberRes = _teamMemberManager.Get(memberId);

                #region LogTeamMember
                TeamMembersHistory logMember = new TeamMembersHistory();
                logMember.PrevTeamId = memberRes.Data.TeamId;
                logMember.TeamId = null;
                logMember.UserId = memberRes.Data.UserId;
                logMember.UserDetailId = memberRes.Data.MemberParentId;
                logMember.UserTypeId = memberRes.Data.UserTypeId;
                if (memberRes.Data.MemberType == MemberType.Vehicle)
                {
                    logMember.VehicleId = memberRes.Data.MemberParentId;
                }
                _ITeamMembersHistoryManager.Add(logMember);
                #endregion
                if (memberRes != null && memberRes.Data != null)
                {
                    memberRes.Data.TeamId = null;
                    memberRes.Data.IsDefault = false;
                    if (memberRes.Data.MemberType == MemberType.Technician || memberRes.Data.MemberType == MemberType.Driver)
                    {
                        var technicianMemberRes = _technicianManager.Get(memberRes.Data.MemberParentId);
                        if (technicianMemberRes != null && technicianMemberRes.Data != null)
                        {
                            technicianMemberRes.Data.TeamId = null;
                            technicianMemberRes.Data.TeamId = null;
                            var vehicleResult = _technicianManager.Update(technicianMemberRes.Data);
                        }
                    }
                    else
                    {
                        memberRes.Data.TeamId = null;
                        var vehicleMemberRes = _vehicleManager.Get(memberRes.Data.MemberParentId);

                        vehicleMemberRes.Data.Isassigned = false;
                        vehicleMemberRes.Data.TeamId = null;
                        var vehicleResult = _vehicleManager.Update(vehicleMemberRes.Data);
                    }
                
                    entityResult = _teamMemberManager.Update(memberRes.Data);
                }
                result = _processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            }
            return result;
        }



    }
}