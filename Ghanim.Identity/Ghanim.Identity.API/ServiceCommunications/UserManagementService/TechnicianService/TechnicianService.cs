﻿
using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;

using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using System.Linq;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using CommonEnums;
using AutoMapper;
using Ghanim.Models.Order.Enums;

namespace Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService
{
    public class TechnicianService : ITechnicianService
    {
        private readonly ITechnicianManager _manager;
        private readonly ITeamMemberManager _teamMemberManager;        
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IIdentityService _identityService;

        public TechnicianService(ITeamMemberManager teamMemberManager,IIdentityService identityService, ITechnicianManager manager, IProcessResultMapper processResultMapper)
        {
            _teamMemberManager = teamMemberManager;
            _manager = manager;
            _processResultMapper = processResultMapper;
            _identityService = identityService;
        }

        public ProcessResultViewModel<TechnicianViewModel> GetTechnicianByUserId(string userId)
        {
            try
            {
                var entityResult = _manager.Get(x => x.UserId == userId);
                var result = _processResultMapper.Map<Technician, TechnicianViewModel>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianViewModel>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTechnicianByUserId;
            //    var url = $"{baseUrl}/{requestedAction}/{userId}";
            //    return await GetByUriCustomized<TechnicianViewModel>(url);
            //}
            //return null;
        }

        public ProcessResultViewModel<TechnicianViewModel> GetTechnicianById(int Id)
        {
            try
            {
                var entityResult = _manager.Get(Id);
                var result = _processResultMapper.Map<Technician, TechnicianViewModel>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianViewModel>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTechnicianById;
            //    var url = $"{baseUrl}/{requestedAction}/{id}";
            //    return await GetByUriCustomized<TechnicianViewModel>(url);
            //}
            //return null;
        }

        public ProcessResultViewModel<List<TechnicianViewModel>> GetTechniciansByIds(List<int> ids)
        {
            try
            {
                var entityResult = _manager.GetByIds(ids);
                var result = _processResultMapper.Map<List<Technician>, List<TechnicianViewModel>>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TechnicianViewModel>>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTechniciansByIds;
            //    var url = $"{baseUrl}/{requestedAction}";
            //    return await PostCustomize<List<int>, List<TechnicianViewModel>>(url, ids);
            //}
            //return null;
        }

        public ProcessResultViewModel<List<TechnicianViewModel>> GetTechniciansByTeamIds(List<int> teamsId)
        {
            try
            {
                var entityResult = _manager.GetAll(x =>x.TeamId!=null &&  teamsId.Contains(x.TeamId??0));
                var result = _processResultMapper.Map<List<Technician>, List<TechnicianViewModel>>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TechnicianViewModel>>(null, ex.Message);
            }
            //string baseUrl = _settings.Uri;
            //while (baseUrl.EndsWith('/'))
            //{
            //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            //}
            //if (!String.IsNullOrEmpty(baseUrl))
            //{
            //    string requestedAction = _settings.GetTechniciansByTeamsId;
            //    var url = $"{baseUrl}/{requestedAction}";
            //    return await PostCustomize<List<int>, List<TechnicianViewModel>>(url, ids);
            //}
            //return null;
        }
        public async Task<ProcessResultViewModel<List<TechnicianViewModel>>> GetAllTechnicians()
        {
            try
            {
                var users = _manager.GetAll();
                var identity = await _identityService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());
                var result = _processResultMapper.Map<List<Technician>, List<TechnicianViewModel>>(users);

                foreach (var item in result.Data)
                    item.FullName = identity.Data.FirstOrDefault(x => x.Id == item.UserId)?.FullName;

                return result;
            }
            catch(Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TechnicianViewModel>>(null, ex.Message);
            }







        //item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();

        //result.Data = result.Data.Select(x => {
        //    x.FullName = identity.Data.FirstOrDefault(u => u.Id == x.UserId)?.FullName;
        //    return x;
        //}).ToList();
        //string baseUrl = _settings.Uri;
        //while (baseUrl.EndsWith('/'))
        //{
        //    baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
        //}
        //if (!String.IsNullOrEmpty(baseUrl))
        //{
        //    string requestedAction = _settings.GetAllTechnicians;
        //    var url = $"{baseUrl}/{requestedAction}";
        //    return await GetByUriCustomized<List<TechnicianViewModel>>(url);
        //}
        //return null;
    }

        public ProcessResult<bool> Put(TechnicianViewModel model)
        {
            ProcessResult<bool> result = null;
            model.TeamId = model.TeamId == 0 ? null : model.TeamId;
            try
            {
                // retrive old data for technician to check pf number & division
                var oldForemenDat = GetTechnicianByUserId(model.UserId);
                               
                    //else
                    //{
                        var EntityResult = Mapper.Map<TechnicianViewModel, Technician>(model);
                        result = _manager.Update(EntityResult);
                        if (result.Data)
                        {
                            var resultUpdate = _teamMemberManager.Update(new TeamMember()
                            {
                                Id = _teamMemberManager.Get(x => x.MemberParentId == model.Id).Data.Id,
                                MemberParentId = model.Id,
                                MemberParentName = model.Name,
                                MemberType = model.IsDriver == false ? MemberType.Technician : MemberType.Driver,
                                //MemberType = MemberType.Technician,
                                UserId = model.UserId,
                                CreatedDate = DateTime.Now,
                                DivisionId = model.DivisionId,
                                TeamId=model.TeamId,
                                UserTypeId= model.IsDriver == false ? UserTypesEnum.Technician : UserTypesEnum.Driver,
                                  
                            });

                        }

                        return result;
                   // }
                
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, null);
            }
        }
        
        public  ProcessResult<Technician> Add(TechnicianViewModel model)
        {
            model.TeamId = model.TeamId == 0 ? null : model.TeamId;

            try
            {
                var EntityResult = Mapper.Map<TechnicianViewModel,Technician>(model);
                ProcessResult<Technician> resultViewModel = _manager.Add(EntityResult);
                _teamMemberManager.Add(new TeamMember()
                {
                    MemberParentId = resultViewModel.Data.Id,
                    MemberParentName = resultViewModel.Data.User.PF,
                    MemberType = model.IsDriver==false? MemberType.Technician :MemberType.Driver,
                    UserId = resultViewModel.Data.UserId,
                    TeamId = model.TeamId,
                    DivisionId = resultViewModel.Data.DivisionId,
                });

                return resultViewModel;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<Technician>(null);
            }
        }

    }
}
