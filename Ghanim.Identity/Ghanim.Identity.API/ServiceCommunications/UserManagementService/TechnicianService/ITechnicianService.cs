﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService
{
    public interface ITechnicianService
    {
        ProcessResultViewModel<TechnicianViewModel> GetTechnicianByUserId(string userId);
        ProcessResultViewModel<TechnicianViewModel> GetTechnicianById(int id);
        ProcessResultViewModel<List<TechnicianViewModel>> GetTechniciansByIds(List<int> ids);
        ProcessResultViewModel<List<TechnicianViewModel>> GetTechniciansByTeamIds(List<int> ids);
        Task<ProcessResultViewModel<List<TechnicianViewModel>>> GetAllTechnicians();
        ProcessResult<bool> Put(TechnicianViewModel model);
        ProcessResult<Technician> Add(TechnicianViewModel model);
    }
}