﻿using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

namespace Ghanim.API.ServiceCommunications.DataManagementService.DivisionService
{
    public class DivisionService :      IDivisionService
    {
        IDivisionManager _divisionsManager { set; get; }
        IProcessResultMapper _processResultMapper { set; get; }

        public DivisionService( IProcessResultMapper processResultMapper, IDivisionManager divisionsManager)
        {
            _divisionsManager =  divisionsManager;
            _processResultMapper = processResultMapper;
        }

        public async Task<ProcessResultViewModel<DivisionViewModel>> GetByName(string name)
        {
            var entityResult = _divisionsManager.Get(x => x.Name == name);
            var result = _processResultMapper.Map<Division, DivisionViewModel>(entityResult);
            return result;
        }
        
    }
}
