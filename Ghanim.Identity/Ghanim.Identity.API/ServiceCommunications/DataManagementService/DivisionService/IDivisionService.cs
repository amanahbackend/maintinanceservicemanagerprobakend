﻿using Ghanim.API.ServiceCommunications.UserManagementService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.DivisionService
{
    public interface IDivisionService 
    {
        Task<ProcessResultViewModel<DivisionViewModel>> GetByName(string name);
    }
}