﻿using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using Ghanim.Models.Entities;
using Ghanim.BLL.IManagers;

namespace Ghanim.API.ServiceCommunications.DataManagementService.GovernoratesService
{
    public class GovernoratesService:       IGovernoratesService
    {
        IGovernoratesManager _governoratesManager { set; get; }
        IProcessResultMapper _processResultMapper { set; get; }

        public GovernoratesService( IProcessResultMapper processResultMapper, IGovernoratesManager governoratesManager) 
        {
            _governoratesManager = governoratesManager;
            _processResultMapper = processResultMapper;
        }

        public async Task<ProcessResultViewModel<GovernoratesViewModel>> GetByName(string name)
        {
            var entityResult = _governoratesManager.Get(x => x.Name == name);
            var result = _processResultMapper.Map<Governorates, GovernoratesViewModel>(entityResult);
            return result;
        }
        
    }
}
