﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.Problem
{
    public interface IProblemService
    {
        Task<ProcessResultViewModel<List<OrderProblemViewModel>>> GetProblems();
    }
}