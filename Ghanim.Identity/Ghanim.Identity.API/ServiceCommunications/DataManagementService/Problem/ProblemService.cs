﻿using DnsClient;
using Ghanim.API.Helper;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.BLL.Caching.Abstracts;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.Problem
{
    public class ProblemService : IProblemService
    {
        IProcessResultMapper _processResultMapper { set; get; }
        IOrderProblemManager _orderProblem_manager { set; get; }
        IAPIHelper _APIhelper { set; get; }
        ILang_OrderProblemManager _Lang_OrderProblemManager { set; get; }
        private ICached<OrderProblemViewModel> _cash;
        public ProblemService(IProcessResultMapper processResultMapper, IOrderProblemManager orderProblem_manager , IAPIHelper APIhelper , ILang_OrderProblemManager Lang_OrderProblemManager, ICached<OrderProblemViewModel> cash)
        {
            _processResultMapper = processResultMapper;
            _orderProblem_manager = orderProblem_manager;
            _APIhelper = APIhelper;
            _Lang_OrderProblemManager = Lang_OrderProblemManager;
            _cash = cash;
        }
        public async Task<ProcessResultViewModel<List<OrderProblemViewModel>>> GetProblems()
        {

            //var entityResult = manger.GetAll();
            string languageIdValue = _APIhelper.GetLanguageIdFromHeader();
            //languageIdValue = "1";
            var actions = _cash.CachedGetAll();
            //FillCurrentUser(entityResult);
            if (!string.IsNullOrEmpty(languageIdValue))
            {
                foreach (var item in actions)
                {
                    var orderProblemLocalizesRes = _Lang_OrderProblemManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderProblem_ID == item.Id);
                    if (orderProblemLocalizesRes?.Data != null)
                    {
                        item.Name = orderProblemLocalizesRes.Data.Name;
                    }
                }
            }
            return ProcessResultViewModelHelper.Succedded(actions);
        }
    }
}
