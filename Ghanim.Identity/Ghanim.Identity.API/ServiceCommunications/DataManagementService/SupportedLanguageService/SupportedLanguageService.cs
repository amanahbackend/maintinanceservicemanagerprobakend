﻿using DnsClient;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

namespace Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService
{
    public class SupportedLanguageService:  ISupportedLanguageService
    {
        ISupportedLanguagesManager _supportedLanguageManager { set; get; }
        IProcessResultMapper _processResultMapper { set; get; }
        public SupportedLanguageService( ISupportedLanguagesManager supportedLanguageManager , IProcessResultMapper processResultMapper)
        {
            _supportedLanguageManager = supportedLanguageManager;
            _processResultMapper = processResultMapper;
        }

        public async Task<ProcessResultViewModel<List<SupportedLanguagesViewModel>>> GetAll()
        {
            var entityResult = _supportedLanguageManager.GetAll();
            //FillCurrentUser(entityResult);
            return _processResultMapper.Map<List<SupportedLanguages>, List<SupportedLanguagesViewModel>>(entityResult);
        }

        public async Task<ProcessResultViewModel<SupportedLanguagesViewModel>> Get(int id)
        {
            var entityResult = _supportedLanguageManager.Get(x=>x.Id == id );
            var result = _processResultMapper.Map<SupportedLanguages, SupportedLanguagesViewModel>(entityResult);
            return result;

        }

        public async Task<ProcessResultViewModel<SupportedLanguagesViewModel>> GetByName(string name)
        {
            try
            {
                var entityResult = _supportedLanguageManager.GetLanguageByName(name);
                var result = _processResultMapper.Map<SupportedLanguages, SupportedLanguagesViewModel>(entityResult);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public async Task<ProcessResultViewModel<List<SupportedLanguagesViewModel>>> GetAllWithBlocked()
        {
            var entityResult = _supportedLanguageManager.GetAll();
            //FillCurrentUser(entityResult);
            return _processResultMapper.Map<List<SupportedLanguages>, List<SupportedLanguagesViewModel>>(entityResult);
        }

    }
}
