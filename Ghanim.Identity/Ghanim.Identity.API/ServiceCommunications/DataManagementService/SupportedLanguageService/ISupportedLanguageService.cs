﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService
{
    public interface ISupportedLanguageService 
    {
        Task<ProcessResultViewModel<List<SupportedLanguagesViewModel>>> GetAll();
        Task<ProcessResultViewModel<SupportedLanguagesViewModel>> Get(int id);
        Task<ProcessResultViewModel<SupportedLanguagesViewModel>> GetByName(string name);
        Task<ProcessResultViewModel<List<SupportedLanguagesViewModel>>> GetAllWithBlocked();
    }
}