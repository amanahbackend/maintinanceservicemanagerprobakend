﻿using DnsClient;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.ContractTypesService
{
    public class ContractTypesService : IContractTypesService
    {
        IContractTypesManager _contractTypeManager { set; get; }
        IProcessResultMapper _processResultMapper { set; get; }

        public ContractTypesService( IContractTypesManager  contractTypeManager , IProcessResultMapper processResultMapper) 
        {
            _contractTypeManager = contractTypeManager;
            _processResultMapper = processResultMapper;
        }

        public async Task<ProcessResultViewModel<ContractTypesViewModel>> GetByName(string name)
        {
            var entityResult = _contractTypeManager.Get(x => x.Name == name);
            var result = _processResultMapper.Map<ContractTypes, ContractTypesViewModel>(entityResult);
            return result;
        }
        public async Task<ProcessResultViewModel<ContractTypesViewModel>> GetById(int? id)
        {
            var entityResult = _contractTypeManager.Get(x => x.Id == id);
            var result = _processResultMapper.Map<ContractTypes, ContractTypesViewModel>(entityResult);
            return result;
        }

    }
}
