﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.ContractTypesService
{
    public interface IContractTypesService 
    {
          Task<ProcessResultViewModel<ContractTypesViewModel>> GetByName(string name);
          Task<ProcessResultViewModel<ContractTypesViewModel>> GetById(int? id);
    }
}
