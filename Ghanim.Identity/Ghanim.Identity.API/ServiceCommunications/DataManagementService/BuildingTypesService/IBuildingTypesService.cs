﻿using Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.BuildingTypesService
{
    public interface IBuildingTypesService 
    {
        Task<ProcessResultViewModel<BuildingTypesViewModel>> GetByName(string name);
        Task<ProcessResultViewModel<BuildingTypesViewModel>> GetById(int? id);
    }
}