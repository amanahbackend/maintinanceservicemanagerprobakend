﻿using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

namespace Ghanim.API.ServiceCommunications.DataManagementService.BuildingTypesService
{
    public class BuildingTypesService :IBuildingTypesService
    {
        IBuildingTypesManager _buldingTypeManager { set; get; }
        IProcessResultMapper _processResultMapper { set; get; }

        public BuildingTypesService( IBuildingTypesManager buldingTypeManager, IProcessResultMapper processResultMapper) 
        {
            _buldingTypeManager = buldingTypeManager;
            _processResultMapper = processResultMapper;
        }

        public async Task<ProcessResultViewModel<BuildingTypesViewModel>> GetByName(string name)
        {
            try {      
                var entityResult = _buldingTypeManager.Get(x => x.Name == name);
                var result = _processResultMapper.Map<BuildingTypes, BuildingTypesViewModel>(entityResult);


                return result;
            }
            catch(Exception e)
            {
                return null;
            }
       }

        public async Task<ProcessResultViewModel<BuildingTypesViewModel>> GetById(int? id)
        {
            try
            {
                var entityResult = _buldingTypeManager.Get(x => x.Id == id);
                var result = _processResultMapper.Map<BuildingTypes, BuildingTypesViewModel>(entityResult);


                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
