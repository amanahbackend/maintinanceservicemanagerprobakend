﻿using Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService
{
    public interface ICompanyCodeService 
    {
        Task<ProcessResultViewModel<CompanyCodeViewModel>> GetByCode(string code);
    }
}