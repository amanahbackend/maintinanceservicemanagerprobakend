﻿using DnsClient;
using Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService
{
    public class CompanyCodeService : ICompanyCodeService
    {
        private readonly ICompanyCodeManager _manager;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly ICached<CompanyCodeViewModel> _cash;


        public CompanyCodeService(ICompanyCodeManager manager, IProcessResultMapper processResultMapper, ICached<CompanyCodeViewModel> cash)
        {
            _manager = manager;
            _processResultMapper = processResultMapper;
            _cash = cash;
        }

        public async Task<ProcessResultViewModel<CompanyCodeViewModel>> GetByCode(string code)
        {
            var result = _cash.CachedGetAll().FirstOrDefault(x => x.Code == code);
            return ProcessResultViewModelHelper.Succedded(result);
        }
    }
}
