﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API;
using Utilites.ProcessingResult;

namespace Dispatching.Location.API.ServiceCommunications.Order
{
    public interface IPACIService
    {
        Task<ProcessResultViewModel<PACIViewModel>> Add(PACIViewModel paciViewModel);
        ProcessResult<PACIViewModel> GetPaciByNumber(string paciNumber);
    }
}
