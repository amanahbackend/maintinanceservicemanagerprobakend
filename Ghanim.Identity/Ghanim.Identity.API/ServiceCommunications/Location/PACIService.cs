﻿
using System;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.API;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.Configuration;
using Utilites.PACI;
using Utilites.ProcessingResult;

namespace Dispatching.Location.API.ServiceCommunications.Order
{
    public class PACIService :IPACIService
    {

        public IPACIManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;

        private IConfigurationRoot _configurationRoot;


        public PACIService(
            IPACIManager _manger,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IConfigurationRoot configurationRoot,
            IServiceProvider serviceProvider
            )
        {
            manager = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            //this is form location PASI COntroller fporm the miicro service 
            _configurationRoot = configurationRoot;

            PACIHelper.Intialization(
                PACIConfig.proxyUrl,
                PACIConfig.paciServiceUrl,
                PACIConfig.paciNumberFieldName,
                PACIConfig.blockServiceUrl,
                PACIConfig.blockNameFieldNameBlockService,
                PACIConfig.nhoodNameFieldName,
                PACIConfig.streetServiceUrl,
                PACIConfig.blockNameFieldNameStreetService,
                PACIConfig.streetNameFieldName);

        }



        public async Task<ProcessResultViewModel<PACIViewModel>> Add(PACIViewModel paciViewModel)
        {
            //FillCurrentUser(model);
            var entityModel = mapper.Map<PACIViewModel, PACI>(paciViewModel);
            var entityResult = manager.Add(entityModel);
            return processResultMapper.Map<PACI, PACIViewModel>(entityResult);
        }

        //test
        public ProcessResult<PACIViewModel> GetPaciByNumber(string paciNumber)
        {
            try
            {
                var paci = manager.Get(x => x.PACINumber == paciNumber);
                var result = mapper.Map<PACI, PACIViewModel>(paci.Data);

                if (paci?.Data != null)
                {
                    return ProcessResultHelper.Succedded<PACIViewModel>(result);
                }
                else
                {
                    return ProcessResultHelper.Failed<PACIViewModel>(null, null, "paci number not found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<PACIViewModel>(null, ex);
            }
        }
    }
}
