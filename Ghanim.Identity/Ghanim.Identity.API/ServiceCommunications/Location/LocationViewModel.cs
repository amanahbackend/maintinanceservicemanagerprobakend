﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Location.API.ViewModels
{
    public class LocationViewModel
    {
        public LocationItemViewModel Governorate { get; set; }
        public LocationItemViewModel Area { get; set; }
        public LocationItemViewModel Block { get; set; }
        public LocationItemViewModel Street { get; set; }
        public decimal? Lat { get; set; }
        public decimal? Lng { get; set; }
        public string AppartementNumber { get; set; }
        public string FloorNumber { get; set; }
        public string BuildingNumber { get; set; }

    }
}
