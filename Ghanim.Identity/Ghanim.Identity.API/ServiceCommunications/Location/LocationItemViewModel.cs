﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Location.API.ViewModels
{
    public class LocationItemViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
