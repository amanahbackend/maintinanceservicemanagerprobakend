﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.GoogleApi
{
    public interface IGeoLocationService
    {
        Task<ProcessResultViewModel<dynamic>> GetGeoLocation(string address);
    }
}
