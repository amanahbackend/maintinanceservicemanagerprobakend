﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ServiceCommunications.GoogleApi
{
    public class GoogleApiSettings
    {
        public string GetGeoLocation { get; set; }
        public string ApiKey { get; set; }

        public virtual string Uri
        {
            get; set;
        }
        public virtual string Version
        {
            get; set;
        }
        public virtual string Controller
        {
            get; set;
        }
        public bool UsingIIS { get; set; }
     
        public string ServiceName { get; set; }
        public virtual string GetVerb
        {
            get
            {
                return "Get";
            }
        }
        public virtual string GetByIdsVerb
        {
            get
            {
                return "GetByIds";
            }
        }
        public virtual string GetAllVerb
        {
            get
            {
                return "GetAll";
            }
        }
        public virtual string PutVerb
        {
            get
            {
                return "Update";
            }
        }
        public virtual string PostVerb
        {
            get
            {
                return "Add";
            }
        }
        public virtual string DeleteVerb
        {
            get
            {
                return "Delete";
            }
        }
        public virtual string PatchVerb
        {
            get
            {
                return "";
            }
        }
    }
}
