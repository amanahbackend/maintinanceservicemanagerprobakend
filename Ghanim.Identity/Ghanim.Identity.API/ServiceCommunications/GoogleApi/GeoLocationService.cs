﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.GoogleApi
{
    public class GeoLocationService :  IGeoLocationService
    {
        GoogleApiSettings _settings;

        public GeoLocationService(IOptions<GoogleApiSettings> obj)
        {
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<dynamic>> GetGeoLocation(string address)
        {
            string baseUrl = _settings.Uri;
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetGeoLocation;
                string apiKey = _settings.ApiKey;
                var url = $"{requestedAction}?address={address}&key={apiKey}";

                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(baseUrl);
                    HttpResponseMessage response = await client.GetAsync(url);
                    //response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        return ProcessResultViewModelHelper.Succedded<dynamic>(await response.Content.ReadAsAsync<dynamic>());
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<dynamic>(null, "error while trying to get location");
                    }
                }
            }
            return null;
        }
    }
}
