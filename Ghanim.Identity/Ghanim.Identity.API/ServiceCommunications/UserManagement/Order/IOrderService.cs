﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonEnum;
using Ghanim.API.Models;
using Ghanim.API.ServiceCommunications.FlixyApi;
using Ghanim.API.ViewModels.Order;
using Ghanim.Models.Entities;
using Ghanim.Models.Order.Enums;
using Microsoft.AspNetCore.Http;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.Order
{

    public interface IOrderService
    {

        Task<ProcessResultViewModel<bool>> SetWorkingState(WorkingViewModel model, HttpRequest Request);

        Task<ProcessResultViewModel<bool>> SetWorkingAction(WorkingViewModel model, HttpRequest Request);
        Task<ProcessResultViewModel<bool>> RegisterEndWork(WorkingViewModel model);



        ////this method is dummy
        //Task<ProcessResult<List<OrderAction>>> AddAction(OrderObject order, HttpRequest Request, int? TeamId,
        //    OrderActionType actionType, int? workingTypeId, string workingTypeName, string subFlag, string userId,
        //    string reason = null, string StateFlag = null);



        Task<ProcessResult<List<OrderAction>>> AddAction(OrderObject order, HttpRequest Request,
            OrderActionType actionType, int? workingTypeId, string workingTypeName, int? inputTeamId = null,
            string inputNewServeyReport = null, int? WorkingSubReasonId = null, string WorkingSubReason = null, string reason = null, PlatformType platformTypeSource = PlatformType.Web);


        Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewStatus(ChangeStatusViewModel model,
           HttpRequest Request);


        Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderAcceptence(AcceptenceViewModel model, HttpRequest Request);


        Task<ProcessResult<bool>> ChangeAcceptence(AcceptenceViewModel model, HttpRequest Request);


        ProcessResult<List<OrderViewModel>> GetTeamOrdersForWeb(int teamId);
        ProcessResult<bool> BulkAssignForDispatcher(int orderId, bool isTeam, int? teamId, int? dispatcherId, string dispatcherName, int? supervisorId, string supervisorName, int statusId, string statusName, string excuter);

        Task<ProcessResult<bool>> BulkAssignForTeam(int orderId, bool isTeam, int? teamId, int? dispatcherId,
            string dispatcherName, int? supervisorId, string supervisorName, int statusId, string statusName,
            string excuter, HttpRequest Request);


        Task<ProcessResult<bool>> ChangeOrderRankAsync(List<OrderObject> teamOrders, Dictionary<int, int> orders, int? orderId);
        Task<ProcessResult<bool>> ChangeOrderRankAsyncForApproveOrder(List<OrderObject> teamOrders, Dictionary<int, int> orders, int? orderId);
        Task<ProcessResult<DispatcherAllOrdersViewModel>> GetDisptcherOrders(PaginatedUnAssignedOrdersForDispatcherFilterViewModel model);
        Task<PaginatedItemsViewModel<OrderCardViewModel>> GetUnAssignedOrdersForDisptcher(PaginatedUnAssignedOrdersForDispatcherFilterViewModel model);
        Task<List<DispatcherAssignedOrdersViewModel>> GetAssignedOrdersForDisptcher(PaginatedUnAssignedOrdersForDispatcherFilterViewModel model);
        Task<List<FormanTeamsViewModel>> GetDispatchersTeam(int dispatcherId);
        Task<Tuple<List<OrderCardViewModel>, int>> GetTeamOrders(PaginatedTeamOrdersViewModel model);


    }
}
