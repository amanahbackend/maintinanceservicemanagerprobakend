﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using Ghanim.API.Helper;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnums;
using DocumentFormat.OpenXml.Spreadsheet;
using Ghanim.API.Utilities.HangFireServices;
using Ghanim.Models.Context;
using Ghanim.Models.Order.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Utilities.ProcessingResult;
using DocumentFormat.OpenXml.EMMA;
using Utilites.PaginatedItemsViewModel;
using Ghanim.API.ViewModels.Order;
using Utilites.PaginatedItems;
using DispatchProduct.RepositoryModule;
using Ghanim.API.Models;
using System.Diagnostics;
using AutoMapper.QueryableExtensions;
using Ghanim.API.ServiceCommunications.FlixyApi;

namespace Ghanim.API.ServiceCommunications.Order
{

    public class OrderService : IOrderService
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<ApplicationRole> _identityRoleManager;
        private readonly UserManager<ApplicationUser> _identityUserManager;
        public IHangFireJobService hangFireJobService;
        public IOrderManager manager;
        public ITeamMemberManager _teamMemberManager;
        public IOrderSettingManager settingManager;
        public IOrderActionManager actionManager;
        IServiceProvider _serviceprovider;
        public readonly new IMapper mapper;
        public readonly new IAPIHelper _helper;
        private readonly IHostingEnvironment _hostingEnv;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IDispatcherService dispatcherService;
        IForemanService foremanService;
        ISupervisorService supervisorService;
        ITeamMembersService teamMembersService;
        INotificationService notificationService;
        ISMSService smsService;
        ITeamService teamService;
        IIdentityService identityService;
        IForemanService formanService;
        IEngineerService engineerService;
        IManagerService managerService;
        IArchivedOrderFileManager archivedOrderFileManager;
        ITechnicianService technicianService;
        ITechnicianStateService technicianStateService;
        INotificationCenterManager notificationCenterManageManager;

        public OrderService(IAPIHelper helper, IServiceProvider serviceprovider, IHostingEnvironment hostingEnv, IOrderManager _manager,
            ApplicationDbContext context,
            RoleManager<ApplicationRole> identityRoleManager,
            UserManager<ApplicationUser> identityUserManager,
          IHangFireJobService hangFireJobService,
            IOrderSettingManager _settingManager,
            IOrderActionManager _actionManager,
            IMapper _mapper,
            INotificationCenterManager _notificationCenterManageManager,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ITeamMembersService _teamMembersService,
            IDispatcherService _dispatcherService,
            IForemanService _foremanService,
            INotificationService _notificationService,
            ITeamService _teamService,
            IIdentityService _identityService,
            ISupervisorService _supervisorService,
            IForemanService _formanService,
            IEngineerService _engineerService,
            IManagerService _managerService,
            IArchivedOrderFileManager _archivedOrderFileManager,
            ITechnicianService _technicianService,
            ITechnicianStateService _technicianStateService,
            ISMSService _smsService,
            ITeamMemberManager teamMemberManager
            )
        {
            _helper = helper;


            _context = context;
            _identityUserManager = identityUserManager;
            _identityRoleManager = identityRoleManager;
            this.hangFireJobService = hangFireJobService;
            _serviceprovider = serviceprovider;
            _hostingEnv = hostingEnv;
            manager = _manager;

            settingManager = _settingManager;
            actionManager = _actionManager;
            mapper = _mapper;
            helper = _helper;
            notificationCenterManageManager = _notificationCenterManageManager;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            dispatcherService = _dispatcherService;
            foremanService = _foremanService;
            teamMembersService = _teamMembersService;
            notificationService = _notificationService;
            teamService = _teamService;
            supervisorService = _supervisorService;
            identityService = _identityService;
            formanService = _formanService;
            engineerService = _engineerService;
            managerService = _managerService;
            technicianService = _technicianService;
            technicianStateService = _technicianStateService;
            smsService = _smsService;
            archivedOrderFileManager = _archivedOrderFileManager;
            _teamMemberManager = teamMemberManager;
        }



        //New Setworking State        
        public async Task<ProcessResultViewModel<bool>> SetWorkingState([FromBody] WorkingViewModel model, HttpRequest Request)
        {
            int BaseWorkingType = model.WorkingTypeId.Value;

            var technicianState = mapper.Map<WorkingViewModel, TechniciansStateViewModel>(model);
            if (model.State == TechnicianState.Working && (model.OrderId == 0 || model.OrderId == null))
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "Order id is required to change team state to working");
            }
            else if (model.WorkingTypeId == (int)WorkingTypeEnum.RejectFirstOrder && string.IsNullOrEmpty(model.RejectionReason))
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "Rejection reason is required to reject the order");
            }
            var actionType = OrderActionType.Work;
            //if (model.WorkingTypeName == "End Work")
            //{
            //    actionType = "End Work";
            //}
            if (model.Type == TimeSheetType.Actual || model.Type == TimeSheetType.Traveling)
            {
                actionType = OrderActionType.Work;
                //model.WorkingTypeName = "Actual";
                //model.WorkingTypeId = 1;
            }
            else if (model.Type == TimeSheetType.Break)
            {
                actionType = OrderActionType.Break;
                //if(model.WorkingTypeName== "Breakfast"|| model.WorkingTypeName == "Prayer" || model.WorkingTypeName == "Lunch")
                //{
                model.WorkingTypeName = "Break";
                model.WorkingTypeId = 4;

                // }
            }
            else if (model.Type == TimeSheetType.Idle)
            {
                actionType = OrderActionType.Idle;
                //if (model.WorkingTypeName == "Sick" || model.WorkingTypeName == "No work assigned" || model.WorkingTypeName == "Petty Cash purchase" || model.WorkingTypeName == "Meetings\Training")
                //{
                model.WorkingTypeName = "Idle";
                model.WorkingTypeId = 3;

                // }
            }

            OrderObject orderObject = new OrderObject();
            if (model.OrderId != null && model.OrderId > 0)
            {
                var order = manager.Get((int)model.OrderId).Data;
                orderObject = Mapper.Map<OrderObject>(order);
            }

            if (model.State == TechnicianState.Working)
            {
                int acceptValue1 = StaticAppSettings.WorkingTypes[0].Id;
                int acceptValue2 = StaticAppSettings.WorkingTypes[3].Id;
                int rejectValue = StaticAppSettings.WorkingTypes[1].Id;
                int continueValue = StaticAppSettings.WorkingTypes[2].Id;

                if (acceptValue1 == model.WorkingTypeId || acceptValue2 == model.WorkingTypeId)
                {
                    // accept order and set on travel
                    var acceptAndSetTravelling = manager.AcceptOrderAndSetOnTravel((int)model.OrderId, StaticAppSettings.TravellingStatusName[0], StaticAppSettings.TravellingStatusId[0], StaticAppSettings.OnTravelSubStatus.Name, StaticAppSettings.OnTravelSubStatus.Id);
                    if (!(acceptAndSetTravelling.IsSucceeded && acceptAndSetTravelling.Data == true))
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, acceptAndSetTravelling.Status.Message);
                    }
                    else
                    {
                        try
                        {
                            // notify dispatcher with the order changes
                            ChangeStatusViewModel changeStatusView = new ChangeStatusViewModel()
                            {
                                OrderId = orderObject.Id,
                                StatusId = orderObject.StatusId,
                                //StatusName = orderObject.OrderStatus.Name,
                                SubStatusId = orderObject.SubStatusId,
                                SubStatusName = orderObject.SubStatus?.Name
                            };
                            //var notificationRes = await NotifyDispatcherWithNewStatus(changeStatusView);

                            // send SMS
                            string msgContent = string.Format(StaticAppSettings.SMSContent, orderObject.Code, orderObject.Status?.Name ?? "");
                            var sms = new SMSViewModel()
                            {
                                PhoneNumber = orderObject.PhoneOne,
                                Message = msgContent
                            };
                            //  var smsResult = await smsService.Send(sms);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            //throw;
                        }
                    }
                }
                else if (rejectValue == model.WorkingTypeId)
                {
                    try
                    {
                        // make order as rejected
                        AcceptenceViewModel acceptenceViewModel = new AcceptenceViewModel()
                        {
                            AcceptenceFlag = false,
                            OrderId = (int)model.OrderId,
                            RejectionReason = model.RejectionReason,
                            RejectionReasonId = model.RejectionReasonId
                        };
                        var acceptenceResult = await ChangeAcceptence(acceptenceViewModel, Request);
                        if (!(acceptenceResult.IsSucceeded && acceptenceResult.Data == true))
                        {
                            return ProcessResultViewModelHelper.Failed<bool>(false, acceptenceResult.Status.Message);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true, acceptenceResult.Status.Message);

                            // var notificationRes = await NotifyDispatcherWithOrderAcceptence(acceptenceViewModel);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        //throw;
                    }
                }
                else if (continueValue == model.WorkingTypeId)
                {

                }


            }
            ProcessResult<List<OrderAction>> addedOrderAction = new ProcessResult<List<OrderAction>>();
            if ((model?.OrderId ?? 0) > 0)
            {
                var orderObjectClone = (await _context.Orders.FirstOrDefaultAsync(x => x.Id == model.OrderId)).Clone();
                addedOrderAction = await AddAction(orderObjectClone, Request, actionType, model.WorkingTypeId, model.WorkingTypeName, model.TeamId, platformTypeSource: PlatformType.Mobile);
            }
            else
            {
                addedOrderAction = await AddAction(null, Request, actionType, model.WorkingTypeId, model.WorkingTypeName, model.TeamId, platformTypeSource: PlatformType.Mobile);
            }

            if (addedOrderAction.IsSucceeded)
            {
                //  var technicianState = mapper.Map<WorkingViewModel, TechniciansStateViewModel>(model);
                var result = technicianStateService.Add(technicianState);
                if (result.IsSucceeded == true && result.Data != null)
                {
                    return ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, result.Status.Message);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, addedOrderAction.Status.Message);
            }



        }

        public async Task<ProcessResultViewModel<bool>> SetWorkingAction([FromBody] WorkingViewModel model, HttpRequest Request)
        {
            var actionType = OrderActionType.Work;
            if (model.Type == TimeSheetType.Actual || model.Type == TimeSheetType.Traveling)
            {
                actionType = OrderActionType.Work;
            }
            else if (model.Type == TimeSheetType.Break)
            {
                actionType = OrderActionType.Break;
            }
            else if (model.Type == TimeSheetType.Idle)
            {
                actionType = OrderActionType.Idle;
            }


            var addedOrderAction = await AddAction(null, Request, actionType, model.WorkingTypeId, model.WorkingTypeName, model.TeamId);
            if (addedOrderAction.IsSucceeded)
            {
                var technicianState = mapper.Map<WorkingViewModel, TechniciansStateViewModel>(model);
                var result = technicianStateService.Add(technicianState);
                if (result.IsSucceeded == true && result.Data != null)
                {
                    return ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, result.Status.Message);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, addedOrderAction.Status.Message);
            }
        }

        //new
        public async Task<ProcessResultViewModel<bool>> RegisterEndWork([FromBody] WorkingViewModel workingViewModel)
        {
            var orderTeam = _context.Teams
                   .Include(x => x.Members)
                   .ThenInclude(x => x.User).ThenInclude(x => x.Technician)
                   .FirstOrDefault(x => x.Id == workingViewModel.TeamId);
            //orderTeam.Members = orderTeam.Members.Where(x => x.MemberType == MemberType.Technician).ToList();
            var members = orderTeam.Members;
            var actionDate = DateTime.Now;
            var actionDay = DateTime.Now.Date;
            //var members = await teamMembersService.GetMemberUsersByTeamId(teamId);
            //member.User.Technician.CostCenterId
            List<OrderAction> actionList = new List<OrderAction>();
            OrderAction model = new OrderAction();
            //Update Old Time 
            var lastActions = await _context.OrderActions
                   .Where(x => x.TeamId == workingViewModel.TeamId && x.ActionTime == null).ToListAsync();

            foreach (OrderAction lastOrderAction in lastActions)
            {
                lastOrderAction.ActionTime = actionDate - lastOrderAction.ActionDate;
                int days = lastOrderAction.ActionTime.Value.Days;
                //Stop Recording Time Sheet
                if (lastOrderAction.WorkingType?.Name == "End Work")
                {
                    lastOrderAction.ActionTime = null;
                    days = 0;
                }
                if (days > 0)
                {
                    //if (actionType != OrderActionType.Assign|| actionType != OrderActionType.UnAssgin)
                    //{
                    lastOrderAction.ActionTime = lastOrderAction.ActionTime.Value.Subtract(TimeSpan.FromDays(days));
                    lastOrderAction.ActionTimeDays = days;
                    // }                      


                }
                else
                {
                    lastOrderAction.ActionTimeDays = 0;
                    //orderAction.ActionTimeDays = lastActions[i].ActionTimeDays;
                }
            }
            //update last Order Actions

            _context.OrderActions.UpdateRange(lastActions);
            if (members != null)
            {
                foreach (var member in members)
                {
                    model = new OrderAction()
                    {
                        ActionDate = actionDate,
                        ActionDay = actionDay,
                        CostCenterId = member.User.Technician.CostCenterId,
                        TeamId = workingViewModel.TeamId,
                        CreatedUser = member.MemberParentName,
                        CurrentUserId = member.UserId,
                        ActionTypeId = (int)OrderActionType.Work,
                        ActionTypeName = EnumManager<OrderActionType>.GetName(OrderActionType.Work),
                        WorkingTypeId = workingViewModel.WorkingTypeId,//End Work
                        //WorkingTypeName = workingViewModel.WorkingTypeName,
                        PlatformTypeSource = PlatformType.Mobile,
                        TechnicianId = workingViewModel.TechnicianId
                    };               
                    var addedOrderAction =  actionManager.Add(model);

                    if (addedOrderAction.IsSucceeded)
                    {
                        TechniciansStateViewModel technicianStatemodel = new TechniciansStateViewModel();
                        technicianStatemodel.State = TechnicianState.End;
                        technicianStatemodel.TeamId = (int)workingViewModel.TeamId;
                        technicianStatemodel.ActionDate = DateTime.Now;
                        technicianStatemodel.Long = 00.00m;
                        technicianStatemodel.Lat = 00.00m;
                        technicianStatemodel.TechnicianId = (int)workingViewModel.TechnicianId;
                        var addedTechnicianStateRes = technicianStateService.Add(technicianStatemodel);
                    }
                }
                _context.SaveChanges();
            }
            return ProcessResultViewModelHelper.Succedded<bool>(true);
        }

        private List<OrderAction> GetActionsForMembers(OrderAction orderAction, List<TeamMember> members)
        {
            List<OrderAction> result = new List<OrderAction>();
            foreach (var member in members)
            {
                OrderAction cloneOrderAction = orderAction.Clone();
                cloneOrderAction.CostCenterId = member.User.Technician.CostCenterId;
                cloneOrderAction.TechnicianId = member.User.Technician.Id;
                cloneOrderAction.TeamId = member.TeamId;
                cloneOrderAction.CreatedUser = member.MemberParentName;
                result.Add(cloneOrderAction);
            }
            return result;
        }


        //public Task<ProcessResult<List<OrderAction>>> AddAction(OrderObject order, HttpRequest Request, int? TeamId, OrderActionType actionType, int? workingTypeId, string workingTypeName, string subFlag, string userId, string reason = null, string StateFlag = null)
        //{
        //    throw new NotImplementedException();
        //}




        //private OrderSubStatustatusEnum  GetOrderActionNewStatus (  OrderObject newOrder,int? NewteamId) 
        public async Task<ProcessResult<List<OrderAction>>> AddAction(OrderObject order, HttpRequest Request, OrderActionType actionType, int? workingTypeId, string workingTypeName, int? inputTeamId = null, string inputNewServeyReport = null, int? WorkingSubReasonId = null, string WorkingSubReason = null, string reason = null, PlatformType platformTypeSource = PlatformType.Web)
        {
            bool TeamHasOrders = false;
            int previosActionType = 0;
            if (actionType == OrderActionType.ChangeStatus)
            {
                platformTypeSource = PlatformType.Mobile;
            }
            if (inputTeamId == null)
                inputTeamId = order?.TeamId;
            if (order != null)
            {
                if (order.DispatcherId == 0)
                    order.DispatcherId = null;
            }
            //if (workingTypeName == "Accept & set first order On-Travel" || workingTypeName == "Set first order On-Travel")

            if (workingTypeId == (int)WorkingTypeEnum.AcceptAndsetFirstOrderOnTravel || workingTypeId == (int)WorkingTypeEnum.SetFirstOrderOnTravel)
            {
                actionType = OrderActionType.ChangeStatus;
            }
            if (workingTypeId == (int)WorkingTypeEnum.RejectFirstOrder)//"Reject first order"
            {
                actionType = OrderActionType.Rejection;
                //int? NewId = null;
                //order.Id = (int)NewId;
            }
            if (actionType == OrderActionType.UnAssgin || actionType == OrderActionType.Assign || actionType == OrderActionType.BulkAssign || actionType == OrderActionType.ChangeOrderRank || actionType == OrderActionType.ChangeTeamRank || actionType == OrderActionType.Rejection || actionType == OrderActionType.Acceptence)
            {
                var ordersCountForInputTeam = _context.Orders.Count(x => x.TeamId == inputTeamId &&
                        x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);

                var lastAction = await _context.OrderActions.LastOrDefaultAsync(x => x.TeamId == inputTeamId);
                if (lastAction != null)
                {
                    workingTypeId = lastAction.WorkingTypeId;
                    workingTypeName = lastAction.WorkingType.Name;
                    previosActionType = (int)lastAction.ActionTypeId;
                    if (workingTypeId == (int)TimeSheetType.Idle)
                    {
                        if (ordersCountForInputTeam > 0)
                        {
                            WorkingSubReason = StaticAppSettings.IdleHandling[2].Name;
                            WorkingSubReasonId = StaticAppSettings.IdleHandling[2].Id;
                            TeamHasOrders = true;
                        }
                        else
                        {
                            WorkingSubReason = StaticAppSettings.IdleHandling[0].Name;
                            WorkingSubReasonId = StaticAppSettings.IdleHandling[0].Id;
                            TeamHasOrders = false;
                        }

                    }
                    else
                    {
                        if (lastAction.StatusId == (int)OrderStatusEnum.Reached || lastAction.StatusId == (int)OrderStatusEnum.On_Travel)
                        {
                            WorkingSubReasonId = 1;
                            WorkingSubReason = "Travelling";
                        }
                        else if (lastAction.StatusId == (int)OrderStatusEnum.Started)
                        {
                            WorkingSubReasonId = 1;
                            WorkingSubReason = "Working";
                        }
                        else
                        {
                            WorkingSubReasonId = lastAction.WorkingSubReasonId;
                            WorkingSubReason = lastAction.WorkingSubReason;
                        }
                    }

                }

            }            
            string currentUserId = _helper.CurrentUserId();
            //   string currentUserId = "674e3898-1def-4bd0-886a-8b192ff18213";
            if (string.IsNullOrEmpty(currentUserId))
            {
                return ProcessResultHelper.Failed<List<OrderAction>>(null, null, "Unauthorized");
            }

            var user = await _context.Users
                //.Include(x => x.Technician).ThenInclude(x => x.Team).ThenInclude(x => x.Members)
                .Include(x => x.Engineer)
                .Include(x => x.Dispatcher)
                .Include(x => x.Supervisor)
                .Include(x => x.Foreman)
                .FirstOrDefaultAsync(x => x.Id == currentUserId);

            int? orderTeamFormanId = null;
            List<TeamMember> membersToAddActionToThem = null;
            if (inputTeamId != null)
            {
                orderTeamFormanId = _context.Teams
                    .FirstOrDefault(x => x.Id == inputTeamId)?.ForemanId;
                membersToAddActionToThem =
                    _context.Members.Include(x => x.User).ThenInclude(x => x.Technician)
                    .Where(x => x.TeamId == inputTeamId && (x.MemberType == MemberType.Technician || x.MemberType == MemberType.Driver)).ToList();
            }
            // get user data

            var actionDate = DateTime.Now;

            var orderActionDataContainer = new OrderAction()
            {
                ActionDate = actionDate,
                ActionDay = actionDate.Date,
                TeamId = inputTeamId,
                DispatcherId = order?.DispatcherId,
                SupervisorId = order?.SupervisorId,
                SubStatusId = order?.SubStatusId,
                StatusId = order?.StatusId,
                OrderId = order?.Id == 0 ? null : order?.Id,
                ForemanId = orderTeamFormanId,
                ServeyReport = inputNewServeyReport,
                Reason = actionType == OrderActionType.Rejection ? reason : null,
                WorkingSubReason = WorkingSubReason,
                WorkingSubReasonId = WorkingSubReasonId,
                //WorkingSubReason = actionType == OrderActionType.Idle || actionType == OrderActionType.Break ? WorkingSubReason : null,

                //WorkingSubReasonId = actionType == OrderActionType.Idle || actionType == OrderActionType.Break ? WorkingSubReasonId : null,
                //StatusName = order?.StatusName,
                //SubStatusName = order?.SubStatus?.Name,
                ActionTypeId = (int)actionType,
                ActionTypeName = actionType.ToString(),
                CurrentUserId = user.Id,
                FK_CreatedBy_Id = currentUserId,
                WorkingTypeId = workingTypeId == 0 ? null : workingTypeId,
                //WorkingTypeName = workingTypeName,
                PlatformTypeSource = platformTypeSource,
            };


            var orderActions = new List<OrderAction>() { };


            List<string> roles = (await _identityUserManager.GetRolesAsync(user)).ToList();
            //var roleNames = UserRes.Data.RoleNames.FirstOrDefault();
            var roleNames = roles.FirstOrDefault();
            var currentUserName = user.UserName; // current user Name
            // var roleNames = "Technician";
            switch (roleNames)
            {
                case "Technician":
                    orderActionDataContainer.CreatedUser = user.PF;

                    if (actionType == OrderActionType.Break || actionType == OrderActionType.Idle)
                        orderActionDataContainer.OrderId = null;

                    orderActions = GetActionsForMembers(orderActionDataContainer, membersToAddActionToThem);

                    break;

                case "Supervisor":

                    //create order actions to team members 
                    if (order?.DispatcherId != null && inputTeamId != null)
                    {
                        orderActions = GetActionsForMembers(orderActionDataContainer, membersToAddActionToThem);
                    }
                    else
                    {
                        //Create Order Action for UnSchedual Dispatcher 
                        //Create Order Action for Schedual Dispatcher 
                        orderActionDataContainer.CostCenterId = user.Supervisor.CostCenterId;
                        orderActions = new List<OrderAction>() { orderActionDataContainer.Clone() };
                    }

                    break;

                case "Dispatcher":


                    if (membersToAddActionToThem == null)
                    {
                        orderActionDataContainer.CostCenterId = user.Dispatcher.CostCenterId;
                        orderActions = new List<OrderAction>() { orderActionDataContainer.Clone() };
                    }
                    else
                    {
                        orderActions = GetActionsForMembers(orderActionDataContainer, membersToAddActionToThem);
                    }


                    break;
                case "Engineer":
                    throw new Exception("Not Valid");
                    //orderActionDataContainer.CostCenterId = user.Engineer.CostCenterId;
                    //orderActionDataContainer.TeamId = order.TeamId;
                    ////orderAction.CreatedUser = engineer.Data.Name;

                    ////CurrentUserId = currentUserId;
                    break;

                case "Foreman":
                    throw new Exception("Not Valid");
                    //var foreman = formanService.GetForemanByUserId(currentUserId);
                    //if (foreman != null && foreman.Data != null)
                    //{
                    //    CostCenterId = foreman.Data.CostCenterId;
                    //    CostCenterName = foreman.Data.CostCenterName;
                    //    orderActionDataContainer.CreatedUser = foreman.Data.Name;
                    //    orderActionDataContainer.TeamId = order.TeamId;
                    //    //CurrentUserId = currentUserId;
                    //}
                    break;
                default:
                    break;
                    //}
            }
            // binding order action details


            //Time sheet fixes
            if (order != null)
            {
                if (StaticAppSettings.TravellingStatusId.Contains(order.StatusId))
                {
                    orderActionDataContainer.WorkingTypeId = (int)TimeSheetType.Traveling;
                    //orderActionDataContainer.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling);
                }
                else
                {
                    orderActionDataContainer.WorkingTypeId = (int)TimeSheetType.Actual;
                    //orderActionDataContainer.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
                    // }
                }
            }


            if (actionType != OrderActionType.BulkAssign && actionType != OrderActionType.Assign
                && actionType != OrderActionType.UnAssgin && actionType != OrderActionType.AssignDispatcher
                && actionType != OrderActionType.UnAssignDispatcher
                && actionType != OrderActionType.ChangeOrderRank
                && actionType != OrderActionType.ChangeTeamRank)
            {
                if ((orderActionDataContainer?.TeamId ?? 0) > 0)
                {
                    var lastActions = await _context.OrderActions
                        .Where(x => x.TeamId == orderActionDataContainer.TeamId && x.ActionTime == null).ToListAsync();

                    foreach (OrderAction lastOrderAction in lastActions)
                    {
                        lastOrderAction.ActionTime = orderActionDataContainer.ActionDate - lastOrderAction.ActionDate;
                        int days = lastOrderAction.ActionTime.Value.Days;
                        //Stop Recording Time Sheet
                        if (lastOrderAction.WorkingTypeId == (int)WorkingTypeEnum.EndWork)
                        {
                            lastOrderAction.ActionTime = null;
                            days = 0;
                        }
                        if (days > 0)
                        {
                            //if (actionType != OrderActionType.Assign|| actionType != OrderActionType.UnAssgin)
                            //{
                            lastOrderAction.ActionTime = lastOrderAction.ActionTime.Value.Subtract(TimeSpan.FromDays(days));
                            lastOrderAction.ActionTimeDays = days;
                            // }                      


                        }
                        else
                        {
                            lastOrderAction.ActionTimeDays = 0;
                            //orderAction.ActionTimeDays = lastActions[i].ActionTimeDays;
                        }
                    }
                    //update last Order Actions

                    _context.OrderActions.UpdateRange(lastActions);
                    //  _context.SaveChanges();
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                }
            }
            if (orderActions.Count > 0)
            {
                _context.OrderActions.AddRange(orderActions);
                _context.SaveChanges();
                if (actionType == OrderActionType.Assign || actionType == OrderActionType.UnAssgin || actionType == OrderActionType.Acceptence
                    || actionType == OrderActionType.Rejection || actionType == OrderActionType.ChangeOrderRank
                    || actionType == OrderActionType.ChangeTeamRank || actionType == OrderActionType.BulkAssign)
                {
                    if (workingTypeId == (int)TimeSheetType.Idle)
                    {
                        bool StoppingIdleTime = false;
                        if ((actionType == OrderActionType.UnAssgin && previosActionType == (int)OrderActionType.Assign) || (actionType == OrderActionType.UnAssgin && previosActionType == (int)OrderActionType.BulkAssign))
                        {
                            StoppingIdleTime = true;
                        }
                        await GetTeamMode(orderActionDataContainer?.TeamId, TeamHasOrders, StoppingIdleTime);
                    }
                }
            }


            return ProcessResultHelper.Succedded(orderActions);
        }


        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode(int? teamId, bool isHasOrder,bool? StoppingIdleTime)
        {
            PlatformType platformType = PlatformType.Mobile;
            List<TeamMember> membersOfTeam = null;
            var orderTeam =
            _context.Members.Include(x => x.User).ThenInclude(x => x.Technician)
                   .Where(x => x.TeamId == teamId && (x.MemberType == MemberType.Technician || x.MemberType == MemberType.Driver)).ToList();
            membersOfTeam = orderTeam;
            //var members = orderTeam.Members;
            var actionDate = DateTime.Now;
            var actionDay = DateTime.Now.Date;
            //var members = await teamMembersService.GetMemberUsersByTeamId(teamId);
            //member.User.Technician.CostCenterId
            List<OrderAction> actionList = new List<OrderAction>();
            OrderAction model = new OrderAction();
            if (membersOfTeam != null)
            {
                foreach (var member in membersOfTeam)
                {
                    model = new OrderAction()
                    {
                        ActionDate = actionDate,
                        ActionDay = actionDay,
                        CostCenterId = member.User.Technician.CostCenterId,
                        TeamId = teamId,
                        CreatedUser = member.MemberParentName,
                        CurrentUserId = member.UserId,
                        ActionTypeId = (int)OrderActionType.Idle,
                        ActionTypeName = EnumManager<OrderActionType>.GetName(OrderActionType.Idle),
                        WorkingTypeId = (int)TimeSheetType.Idle,
                        //WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle),
                        PlatformTypeSource = StoppingIdleTime==true? PlatformType.Web :PlatformType.Mobile,           
                         TechnicianId = member.User.Technician.Id
                    };

                    if (isHasOrder)
                    {
                        model.WorkingSubReason = StaticAppSettings.IdleHandling[2].Name;
                        model.WorkingSubReasonId = StaticAppSettings.IdleHandling[2].Id;
                    }
                    else
                    {
                        model.WorkingSubReason = StaticAppSettings.IdleHandling[0].Name;
                        model.WorkingSubReasonId = StaticAppSettings.IdleHandling[0].Id;

                    }
                    var addedOrderAction = actionManager.Add(model);

                    if (addedOrderAction.IsSucceeded)
                    {
                        TechniciansStateViewModel technicianStatemodel = new TechniciansStateViewModel();
                        technicianStatemodel.State = TechnicianState.Idle;
                        technicianStatemodel.TeamId = (int)teamId;
                        technicianStatemodel.ActionDate = DateTime.Now;
                        technicianStatemodel.Long = 00.00m;
                        technicianStatemodel.Lat = 00.00m;
                        technicianStatemodel.TechnicianId = member.User.Technician.Id;
                        var addedTechnicianStateRes = technicianStateService.Add(technicianStatemodel);
                    }
                }

            }

            TeamModeViewModel result = new TeamModeViewModel();
            if (string.IsNullOrEmpty(model.WorkingType.Name))
            {
                result.Mode = "End Work";
            }
            else
            {
                if (string.IsNullOrEmpty(model.WorkingSubReason))
                {
                    result.Mode = $"{model.WorkingType.Name}";
                }
                else
                {
                    result.Mode = $"{model.WorkingType.Name} - {model.WorkingSubReason}";
                }
            }

            return ProcessResultHelper.Succedded<TeamModeViewModel>(result);

        }

        public async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewStatus(ChangeStatusViewModel model, HttpRequest Request)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = _helper.GetAuthHeader(Request);
            string currentUserId = _helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var UserRes = await identityService.GetById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var techIdentity = await identityService.GetById(currentUserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var orderRes = manager.Get(model.OrderId);
            var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            //string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            var roleNames = UserRes.Data.RoleNames.FirstOrDefault();
            if (roleNames == "Technician")
            {
                if (dispatcherRes != null && dispatcherRes.Data != null)
                {
                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        UserId = dispatcherRes.Data.UserId,
                        Title = "Change Status",
                        Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data?.Status?.Name}.",
                        //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change Status" }//,
                                                                                                                           // Data = new { OrderId = model.OrderId, NotificationType = "Change Status" }                                //   TeamId= model.TeamId

                    });
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = dispatcherRes.Data.UserId,
                        Title = "New Status",
                        Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data?.Status.Name}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Change Status",
                        IsRead = false,
                        CurrentUserId = techIdentity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                    notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                }
            }
            if (roleNames == "Dispatcher")
            {
                if (dispatcherRes != null && dispatcherRes.Data != null)
                {
                    if (orderRes.Data.TeamId != null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(model.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModels.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }//,
                                    // Data = new { OrderId = order.Id, NotificationType = "Change Status" }
                                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change Status" }//,


                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    Data = order.Id.ToString(),
                                    NotificationType = "",
                                    IsRead = false,
                                    CurrentUserId = techIdentity.Data?.UserName,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                    if (orderRes.Data.TeamId == null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(model.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModels.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }//,
                                    // Data = new { OrderId = order.Id, NotificationType = "Changed Status" }
                                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change Status" }//,


                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    Data = order.Id.ToString(),
                                    NotificationType = "",
                                    IsRead = false,
                                    CurrentUserId = techIdentity.Data?.UserName,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                    notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                }
            }

            return notificationRes;
        }


        public async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderAcceptence(AcceptenceViewModel model, HttpRequest Request)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = _helper.GetAuthHeader(Request);
            string currentUserId = _helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            //var UserRes = await identityService.GetUserById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var identity = await identityService.GetById(currentUserId);
            var orderRes = manager.Get(model.OrderId);
            var dispatcher = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            SendNotificationViewModel notificationModel;
            var userId = dispatcher.Data.UserId;
            var orderId = model.OrderId;
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            //string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (dispatcher != null && dispatcher.Data != null)
            {
                if (model.AcceptenceFlag)
                {
                    notificationModel = new SendNotificationViewModel()
                    {
                        UserId = dispatcher.Data.UserId,
                        Title = "Acceptence Order",
                        Body = $"Technician {identity.Data?.FullName} Accepted order No. {orderRes.Data.Code}.",
                        //Data = new { OrderId = model.OrderId, NotificationType = "Acceptence Order" },
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Acceptence Order" },
                        TeamId = orderRes.Data.TeamId //TODO Refactor issue
                    };
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = userId,
                        Title = "Acceptence Order",
                        Body = $"Technician {identity.Data?.FullName} Accepted order No. {orderRes.Data.Code}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Acceptence Order",
                        IsRead = false,
                        TeamId = orderRes.Data.TeamId,
                        CurrentUserId = identity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                }
                else
                {
                    notificationModel = new SendNotificationViewModel()
                    {
                        UserId = dispatcher.Data.UserId,
                        Title = "Acceptence Order",
                        Body = $"Technician {identity.Data?.FullName} rejected order No. {orderRes.Data.Code} as {model.RejectionReason}.",
                        // Data = new { OrderId = model.OrderId, NotificationType = "Acceptence Order" },
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Acceptence Order" }
                    };
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = userId,
                        Title = "Acceptence Order",
                        Body = $"Technician {identity.Data?.FullName} rejected order No. {orderRes.Data.Code} as {model.RejectionReason}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Acceptence Order",
                        IsRead = false,
                        CurrentUserId = identity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                }
                notificationRes = await notificationService.Send(notificationModel);
            }
            return notificationRes;
        }


        public async Task<ProcessResult<bool>> ChangeAcceptence(AcceptenceViewModel model, HttpRequest Request)
        {

            var order = manager.Get(model.OrderId).Data;
            int? newTeam = order.TeamId;

            ProcessResult<bool> entityResult = new ProcessResult<bool>();
            if (model.AcceptenceFlag == true)
            {
                entityResult = manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId, model.RejectionReason, null, null, null, null);
            }
            else
            {
                entityResult = manager.SetAcceptence(model.OrderId, model.AcceptenceFlag, model.RejectionReasonId, model.RejectionReason, StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name, StaticAppSettings.InitialSubStatus.Id, StaticAppSettings.InitialSubStatus.Name);
            }
            if (entityResult.IsSucceeded)
            {

                var orderObject = Mapper.Map<OrderObject>(order);
                if (model.AcceptenceFlag)
                {
                    await AddAction(orderObject, Request, OrderActionType.Acceptence, null, null, newTeam);
                }
                else
                {
                    await AddAction(orderObject, Request, OrderActionType.Rejection, null, null, newTeam, null, null, null, model.RejectionReason);
                }
            }
            return entityResult;
        }


        public ProcessResult<List<OrderViewModel>> GetTeamOrdersForWeb(int teamId)
        {
            if (teamId > 0)
            {
                var orders = manager.GetAll(x => x.TeamId == teamId && x.AcceptanceFlag != AcceptenceType.Rejected && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId)).Data.OrderBy(x => x.RankInTeam).ToList();

                var result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders);

                return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "team id is required", ProcessResultStatusCode.Failed, "GetTeamOrders Controller");
        }






        public ProcessResult<bool> BulkAssignForDispatcher(int orderId, bool isTeam, int? teamId, int? dispatcherId, string dispatcherName, int? supervisorId, string supervisorName, int statusId, string statusName, string excuter)
        {
            OrderObject order = _context.Orders.FirstOrDefault(x => x.Id == orderId);
            if (order == null)
                return ProcessResultHelper.Failed<bool>(false, null, "the order was not found ");

            if ((order.TeamId ?? 0) > 0)
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already assigned to a team, please refresh the page");

            if (order.DispatcherId == dispatcherId)
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already assigned to this dispatcher, please refresh the page");

            if ((dispatcherId ?? 0) == 0)
                return ProcessResultHelper.Failed<bool>(false, null, "the Dispatcher Is not valid");


            if (!isTeam)
            {
                order.DispatcherId = dispatcherId;
                //item.DispatcherName = dispatcherName;
                order.SupervisorId = supervisorId;
                //item.SupervisorName = supervisorName;
                order.StatusId = statusId;
                //order.StatusName = statusName;
                order.TeamId = null;
            }

            manager.Update(order);
            manager.AddActionForDispatcherForBuilkAssign(order, OrderActionType.BulkAssign);
            return new ProcessResult<bool>() { IsSucceeded = true };
        }



        //BuklAssign
        public async Task<ProcessResult<bool>> BulkAssignForTeam(int orderId, bool isTeam, int? teamId, int? dispatcherId, string dispatcherName, int? supervisorId, string supervisorName, int statusId, string statusName, string excuter, HttpRequest Request)
        {
            OrderObject order = _context.Orders.FirstOrDefault(x => x.Id == orderId);
            if (order == null)
                return ProcessResultHelper.Failed<bool>(false, null, "the order was not found ");

            if (order.TeamId == teamId)
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already assigned to this team, please refresh the page");

            if ((teamId ?? 0) == 0)
                return ProcessResultHelper.Failed<bool>(false, null, "the Team Is not valid");
            var ordersCountForInputTeam = _context.Orders.Count(x => x.TeamId == teamId &&
                        x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);

            OrderObject orderObject = manager.Assign(orderId, (int)teamId);
            await AddAction(orderObject, Request, OrderActionType.BulkAssign, null, null, orderObject.TeamId, null, null,null,null,PlatformType.Web);
            return new ProcessResult<bool>() { IsSucceeded = true };
        }






        public async Task<ProcessResult<bool>> ChangeOrderRankAsync(List<OrderObject> teamOrders, Dictionary<int, int> orders, int? orderId)
        {
            try
            {

                //var OrdersToChangeRankFor = _context.Orders.Where(x => orders.Keys.Contains(x.Id));

                foreach (var t in teamOrders)
                {
                    int newRank = 0;
                    if (!orders.TryGetValue(t.Id, out newRank))
                    {
                        newRank = t.RankInTeam ?? int.MaxValue;
                    }
                    t.RankInTeam = newRank;
                }

                var updatedOrders = manager.Update(teamOrders);
                if (updatedOrders.IsSucceeded && updatedOrders.Data == true)
                {
                    for (int i = 0; i < teamOrders.Count; i++)
                    {
                        if (orderId == teamOrders[i].Id)
                            await AddAction(teamOrders[i], null, OrderActionType.ChangeTeamRank, null, workingTypeName: null);
                    }
                }
                return updatedOrders;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex, "something wrong", ProcessResultStatusCode.Failed, "BulkUnAssign Manager");
            }
        }
        public async Task<ProcessResult<bool>> ChangeOrderRankAsyncForApproveOrder(List<OrderObject> teamOrders, Dictionary<int, int> orders, int? orderId)
        {
            try
            {

                //var OrdersToChangeRankFor = _context.Orders.Where(x => orders.Keys.Contains(x.Id));

                foreach (var t in teamOrders)
                {
                    int newRank = 0;
                    if (!orders.TryGetValue(t.Id, out newRank))
                    {
                        newRank = t.RankInTeam ?? int.MaxValue;
                    }
                    t.RankInTeam = newRank;
                }

                var updatedOrders = manager.Update(teamOrders);
                if (updatedOrders.IsSucceeded && updatedOrders.Data == true)
                {
                    for (int i = 0; i < teamOrders.Count; i++)
                    {
                        if (orderId == teamOrders[i].Id)
                            await AddAction(teamOrders[i], null, OrderActionType.ChangeOrderRank, null, workingTypeName: null);
                    }
                }
                return updatedOrders;
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, ex, "something wrong", ProcessResultStatusCode.Failed, "BulkUnAssign Manager");
            }
        }

        public async Task<ProcessResult<DispatcherAllOrdersViewModel>> GetDisptcherOrders(PaginatedUnAssignedOrdersForDispatcherFilterViewModel model)
        {
            var watch = new Stopwatch();
            watch.Start();
            DispatcherAllOrdersViewModel returendData = new DispatcherAllOrdersViewModel();
            if (model.DispatcherId != 0)
            {
                returendData.UnAssignedOrders = await GetUnAssignedOrdersForDisptcher(model);
                // this is hassel
                //OrdersboardFilterViewModel mm = new OrdersboardFilterViewModel()
                //{

                //    DispatcherId = model.DispatcherId

                //};
                var data = await GetAssignedOrdersForDisptcher(model);
                returendData.AssignedOrders = data;
            }
            watch.Stop();
            var elapsed = watch.ElapsedTicks;
            // this api take 12 second
            return new ProcessResult<DispatcherAllOrdersViewModel>() { Data = returendData };
        }

        public async Task<PaginatedItemsViewModel<OrderCardViewModel>> GetUnAssignedOrdersForDisptcher(PaginatedUnAssignedOrdersForDispatcherFilterViewModel model)
        {
            int pageindex = 1;
            int pageSize = 10;
            if (model.PageInfo.PageNo > 0)
                pageindex = model.PageInfo.PageNo - 1;
            if (model.PageInfo.PageSize > 0)
                pageSize = model.PageInfo.PageSize;

            var skipCount = pageindex * pageSize;




            //model = model ?? new PaginatedUnAssignedOrdersForDispatcherFilterViewModel();
            //var action = mapper.Map<PaginatedItemsViewModel<OrderCardViewModel>, PaginatedItems<OrderObject>>(model.PageInfo);
            //ProcessResult<PaginatedItems<OrderObject>> orders = new ProcessResult<PaginatedItems<OrderObject>>();
            var predicate = PredicateBuilder.True<OrderObject>();

            List<int> unassignedOrderStatusId = StaticAppSettings.UnassignedStatusId;
            if (model.DispatcherId > 0)
            {

                if (model.OrderCode != null && model.OrderCode != "")
                {
                    predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
                }

                if (model.CustomerCode != null && model.CustomerCode != "")
                {
                    predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
                }

                if (model.CustomerName != null && model.CustomerName != "")
                {
                    predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
                }

                if (model.CustomerPhone != null && model.CustomerPhone != "")
                {
                    predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
                }

                if (model.AreaIds != null && model.AreaIds.Any())
                {
                    predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
                }

                if (model.BlockName != null && model.BlockName != "")
                {
                    predicate = predicate.And(r => r.SAP_BlockName.Contains(model.BlockName));
                }

                if (model.ProblemIds != null && model.ProblemIds.Any())
                {
                    predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
                }

                if (model.StatusIds != null && model.StatusIds.Any())
                {
                    predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
                }

                if (model.Rejection == (int)RejectionEnum.NoAction)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
                }
                else if (model.Rejection == (int)RejectionEnum.Rejected)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
                }
                else if (model.Rejection == (int)RejectionEnum.Accepted)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
                }
                else if (model.Rejection == (int)RejectionEnum.All)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
                }

                if (model.DateFrom != null && model.DateTo != null)
                {
                    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
                }
                else if (model.DateFrom == null && model.DateTo == null)
                {
                    predicate = predicate.And(x => true);
                }
                else if (model.DateFrom == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
                }
                else if (model.DateTo == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
                }

                predicate = predicate.And(r => unassignedOrderStatusId.Contains(r.StatusId));
                predicate = predicate.And(r => r.DispatcherId == model.DispatcherId);


                var query = manager.GetAllQuerable().Data.Where(predicate);

                var OrderCardViewModel = await query.OrderByDescending(x => x.Id).Skip(skipCount).Take(pageSize).ProjectTo<OrderCardViewModel>().ToListAsync();

                var result = new PaginatedItemsViewModel<OrderCardViewModel>()
                {
                    PageNo = model.PageInfo.PageNo + 1,
                    PageSize = model.PageInfo.PageSize,
                    Data = OrderCardViewModel,
                    Count =await query.CountAsync()
                };



                //// return new ProcessResultViewModel<List<OrderCardViewModel>>() { Data = result.Data ,IsSucceeded=true,MethodName= "getAllUnAssigned" };
                //return new PaginatedItems<OrderCardViewModel>() { Data = result.Data, Count = result.Data.Count };
                return result;
            }
            return null;
        }

        public async Task<List<DispatcherAssignedOrdersViewModel>> GetAssignedOrdersForDisptcher(PaginatedUnAssignedOrdersForDispatcherFilterViewModel model)
        {
            List<TeamOrdersViewModel> currentTeams = new List<TeamOrdersViewModel>();
            // var action = mapper.Map<PaginatedItemsViewModel<TeamViewModel>, PaginatedItems<Team>>(model.PageInfo);
            var predicate = PredicateBuilder.True<OrderObject>();
            List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
            List<int> unassignedStatusesId = StaticAppSettings.UnassignedStatusId;

            if (model.OrderCode != null && model.OrderCode != "")
            {
                predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            }

            if (model.CustomerCode != null && model.CustomerCode != "")
            {
                predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
            }

            if (model.CustomerName != null && model.CustomerName != "")
            {
                predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
            }

            if (model.CustomerPhone != null && model.CustomerPhone != "")
            {
                predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
            }

            if (model.AreaIds != null && model.AreaIds.Any())
            {
                predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
            }

            if (model.BlockName != null && model.BlockName != "")
            {
                predicate = predicate.And(r => r.SAP_BlockName.Contains(model.BlockName));
            }

            if (model.ProblemIds != null && model.ProblemIds.Any())
            {
                predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
            }

            if (model.StatusIds != null && model.StatusIds.Any())
            {
                predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
            }

            if (model.Rejection == (int)RejectionEnum.NoAction)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
            }
            else if (model.Rejection == (int)RejectionEnum.Rejected)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
            }
            else if (model.Rejection == (int)RejectionEnum.Accepted)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
            }
            else if (model.Rejection == (int)RejectionEnum.All)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
            }

            if (model.DateFrom != null && model.DateTo != null)
            {
                predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
            }
            else if (model.DateFrom == null && model.DateTo == null)
            {
                predicate = predicate.And(x => true);
            }
            else if (model.DateFrom == null)
            {
                predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
            }
            else if (model.DateTo == null)
            {
                predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
            }

            predicate = predicate.And(r => !deactiveStatusesId.Contains(r.StatusId) && !unassignedStatusesId.Contains(r.StatusId));
            predicate = predicate.And(r => r.DispatcherId == model.DispatcherId);

            var ordersRes = await manager.GetAllAsync(predicate);
            // hayam

            var getTeamsWithOutTracking = await _context.Teams.AsNoTracking().Where(x => x.DispatcherId == model.DispatcherId && x.Members.Any()).ToListAsync();
            var mapTeamList = mapper.Map<List<Team>, List<TeamViewModel>>(getTeamsWithOutTracking);
            var dispatcherTeams = ProcessResultViewModelHelper.Succedded<List<TeamViewModel>>(mapTeamList, "");
            // var dispatcherTeams = await  teamService.GetTeamsByDispatcherId(model.DispatcherId);
            var foreman = foremanService.GetForemanByIds(dispatcherTeams.Data.Select(x => x.ForemanId).ToList());
            var identity = await identityService.GetByUserIds(foreman.Data.Select(x => x.UserId).ToList());

            currentTeams = (from disp in dispatcherTeams.Data
                            join foremn in foreman.Data on disp.ForemanId equals foremn.Id
                            join id in identity.Data on foremn.UserId equals id.Id
                            select new TeamOrdersViewModel
                            {
                                ForemanId = foremn.Id,
                                ForemanName = id.FirstName,
                                TeamId = disp.Id,
                                TeamName = disp.Name,
                                Phone = id.Phone,
                                StatusId = disp.StatusId,
                                StatusName = disp.StatusName,

                            }).ToList();



            if (ordersRes.IsSucceeded)
            {
                if (ordersRes.Data.Any())
                {
                    List<OrderCardViewModel> result = mapper.Map<List<OrderObject>, List<OrderCardViewModel>>(ordersRes.Data);
                    var groups = result.GroupBy(x => x.TeamId, (key, group) => new TeamOrdersViewModel { TeamId = key, Orders = group.OrderBy(x => x.RankInTeam) }).ToList();

                    //if (model.PageInfo != null && model.PageInfo.PageSize == -1)
                    //{
                    //    groups = groups.ToList();
                    //}
                    //else if (model.PageInfo != null && model.PageInfo.PageSize != -1)
                    //{
                    //    groups = groups.Skip(model.PageInfo.PageSize * (model.PageInfo.PageNo - 1)).Take(model.PageInfo.PageSize).ToList();
                    //}

                    if (groups.Any())
                    {

                        if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                        {

                            for (int i = 0; i < currentTeams.Count; i++)
                            {
                                currentTeams[i].Orders = new List<OrderCardViewModel>();
                                currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                                for (int j = 0; j < groups.Count; j++)
                                {
                                    if (currentTeams[i].TeamId == groups[j].TeamId)
                                    {
                                        currentTeams[i].Orders = groups[j].Orders;
                                    }
                                }

                            }

                        }

                    }
                    // here add foreman group binding
                    var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName, x.Phone }, (key, group) => new DispatcherAssignedOrdersViewModel
                    {
                        ForemanId = key.ForemanId,
                        ForemanName = key.ForemanName,
                        ForemanPhone = key.Phone,
                        Teams = group
                    }).ToList();
                    return newGroup;
                }
                else
                {
                    if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                    {

                        for (int i = 0; i < currentTeams.Count; i++)
                        {
                            currentTeams[i].Orders = new List<OrderCardViewModel>();
                            //currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                            //currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
                            //currentTeams[i].Phone = dispatcherTeams.Data[i].Phone;
                        }
                    }
                    // here add foreman group binding
                    var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName, x.Phone }, (key, group) => new DispatcherAssignedOrdersViewModel
                    {
                        ForemanId = key.ForemanId,
                        ForemanName = key.ForemanName,
                        Teams = group,
                        ForemanPhone = key.Phone
                    }).ToList();
                    return newGroup;
                }
            }

            return new List<DispatcherAssignedOrdersViewModel>();
        }

        public async Task<List<FormanTeamsViewModel>> GetDispatchersTeam(int dispatcherId)
        {
            //var getTeamsWithOutTracking = await _context.Teams.AsNoTracking().Where(x => x.DispatcherId == dispatcherId && x.Members.Any()).ToListAsync();
            //var mapTeamList = mapper.Map<List<Team>, List<TeamViewModel>>(getTeamsWithOutTracking);
            //var dispatcherTeams = ProcessResultViewModelHelper.Succedded<List<TeamViewModel>>(mapTeamList, "");
            //var foreman = foremanService.GetForemanByIds(dispatcherTeams.Data.Select(x => x.ForemanId).ToList());
            var formens = await _context.Foremans.AsNoTracking()
                .Where(x => x.DispatcherId == dispatcherId && !x.IsDeleted && (x.Teams.Any() && x.Teams.Any(t => t.DispatcherId == dispatcherId))

            ).ToListAsync();
            var teams = _context.Teams.AsNoTracking();
            var identity = await identityService.GetByUserIds(formens.Select(x => x.UserId).ToList());

            var FormanTeams = (from forman in formens
                               from id in identity.Data
                               where forman.UserId == id.Id

                               select new FormanTeamsViewModel
                               {
                                   ForemanId = forman.Id,
                                   ForemanName = id.FirstName,
                                   ForemanPhone = id.Phone,
                                   Teams = teams
                                   .Where(x => !x.IsDeleted && x.Members.Any() && x.DispatcherId == forman.DispatcherId && x.ForemanId == forman.Id)
                                   .Select(t => new CustomTeamViewModel { TeamId = t.Id, TeamName = t.Name, StatusId = t.StatusId })
                               });
            return FormanTeams.ToList();
        }

        public async Task<Tuple<List<OrderCardViewModel>, int>> GetTeamOrders(PaginatedTeamOrdersViewModel model)
        {


            var predicate = PredicateBuilder.True<OrderObject>();
            List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
            List<int> unassignedStatusesId = StaticAppSettings.UnassignedStatusId;

            if (model.OrderCode != null && model.OrderCode != "")
            {
                predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            }

            if (model.CustomerCode != null && model.CustomerCode != "")
            {
                predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
            }

            if (model.CustomerName != null && model.CustomerName != "")
            {
                predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
            }

            if (model.CustomerPhone != null && model.CustomerPhone != "")
            {
                predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
            }

            if (model.AreaIds != null && model.AreaIds.Any())
            {
                predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
            }

            if (model.BlockName != null && model.BlockName != "")
            {
                predicate = predicate.And(r => r.SAP_BlockName.Contains(model.BlockName));
            }

            if (model.ProblemIds != null && model.ProblemIds.Any())
            {
                predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
            }

            if (model.StatusIds != null && model.StatusIds.Any())
            {
                predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
            }

            if (model.Rejection == (int)RejectionEnum.NoAction)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
            }
            else if (model.Rejection == (int)RejectionEnum.Rejected)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
            }
            else if (model.Rejection == (int)RejectionEnum.Accepted)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
            }
            else if (model.Rejection == (int)RejectionEnum.All)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
            }

            if (model.DateFrom != null && model.DateTo != null)
            {
                predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
            }
            else if (model.DateFrom == null && model.DateTo == null)
            {
                predicate = predicate.And(x => true);
            }
            else if (model.DateFrom == null)
            {
                predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
            }
            else if (model.DateTo == null)
            {
                predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
            }

            predicate = predicate.And(r => !deactiveStatusesId.Contains(r.StatusId) && !unassignedStatusesId.Contains(r.StatusId));
            predicate = predicate.And(r => r.DispatcherId == model.DispatcherId && !r.IsDeleted && r.TeamId == model.TeamId);
            var ordersRes = _context.Orders.AsNoTracking().Where(predicate).OrderBy(x => x.RankInTeam).ThenBy(x => x.CreatedDate);



            if (model.PageInfo == null)
            {
                model.PageInfo = new PaginatedItemsViewModel<OrderCardViewModel>()
                {
                    PageNo = 1,
                    PageSize = 10
                };
            }

            List<OrderCardViewModel> listResult = await ordersRes.Skip(model.PageInfo.PageSize * (model.PageInfo.PageNo - 1))
                .Take(model.PageInfo.PageSize)
                .Select(card => new OrderCardViewModel
                {
                    Id = card.Id,
                    Code = card.Code,
                    AcceptanceFlag = card.AcceptanceFlag,
                    CurrentUserId = card.CurrentUserId,
                    IsRepeatedCall = card.IsRepeatedCall,
                    CustomerName = card.CustomerName,
                    Lat = card.Lat,
                    Long = card.Long,
                    SAP_BlockName = card.SAP_BlockName,
                    SAP_AreaName = card.SAP_AreaName,
                    IsExceedTime = card.IsExceedTime,
                    OrderTypeCode = card.OrderTypeCode,
                    PriorityName = card.Priority == null ? null : card.Priority.Name,
                    RankInDispatcher = card.RankInDispatcher,
                    RankInTeam = card.RankInTeam,
                    StatusName = card.Status != null ? card.Status.Name : "",
                    RejectionReason = card.RejectionReason,
                    SAP_CreatedDate = card.SAP_CreatedDate,
                    TeamId = card.TeamId,
                    SubStatusId = card.SubStatusId,
                    SubStatusName = card.SubStatusId == null ? null : card.SubStatus.Name,
                    DispatcherId = card.DispatcherId
                }).ToListAsync();

            return new Tuple<List<OrderCardViewModel>, int>(listResult, await ordersRes.CountAsync());
        }

        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode(int? teamId, bool isHasOrder)
        {           
            List<TeamMember> membersOfTeam = null;
            var orderTeam =
            _context.Members.Include(x => x.User).ThenInclude(x => x.Technician)
                   .Where(x => x.TeamId == teamId && (x.MemberType == MemberType.Technician || x.MemberType == MemberType.Driver)).ToList();
            membersOfTeam = orderTeam;
            //var members = orderTeam.Members;
            var actionDate = DateTime.Now;
            var actionDay = DateTime.Now.Date;
            //var members = await teamMembersService.GetMemberUsersByTeamId(teamId);
            //member.User.Technician.CostCenterId
            List<OrderAction> actionList = new List<OrderAction>();
            OrderAction model = new OrderAction();
            if (membersOfTeam != null)
            {
                foreach (var member in membersOfTeam)
                {
                    model = new OrderAction()
                    {
                        ActionDate = actionDate,
                        ActionDay = actionDay,
                        CostCenterId = null, //member.User.Technician.CostCenterId,
                        TeamId = teamId,
                        CreatedUser = member.MemberParentName,
                        CurrentUserId = member.UserId,
                        ActionTypeId = (int)OrderActionType.Idle,
                        ActionTypeName = EnumManager<OrderActionType>.GetName(OrderActionType.Idle),
                        WorkingTypeId = (int)TimeSheetType.Idle,
                        //WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle),
                        PlatformTypeSource = PlatformType.Mobile,
                        // PlatformTypeSource = StoppingIdleTime == true ? PlatformType.Web : PlatformType.Mobile,
                        TechnicianId = member.User.Technician.Id
                    };

                    if (isHasOrder)
                    {
                        model.WorkingSubReason = StaticAppSettings.IdleHandling[2].Name;
                        model.WorkingSubReasonId = StaticAppSettings.IdleHandling[2].Id;
                    }
                    else
                    {
                        model.WorkingSubReason = StaticAppSettings.IdleHandling[0].Name;
                        model.WorkingSubReasonId = StaticAppSettings.IdleHandling[0].Id;

                    }
                    var addedOrderAction = actionManager.Add(model);

                    if (addedOrderAction.IsSucceeded)
                    {
                        TechniciansStateViewModel technicianStatemodel = new TechniciansStateViewModel();
                        technicianStatemodel.State = TechnicianState.Idle;
                        technicianStatemodel.TeamId = (int)teamId;
                        technicianStatemodel.ActionDate = DateTime.Now;
                        technicianStatemodel.Long = 00.00m;
                        technicianStatemodel.Lat = 00.00m;
                        technicianStatemodel.TechnicianId = member.User.Technician.Id;
                        var addedTechnicianStateRes = technicianStateService.Add(technicianStatemodel);
                    }
                }

            }

            TeamModeViewModel result = new TeamModeViewModel();
            if (string.IsNullOrEmpty(model.WorkingType.Name))
            {
                result.Mode = "End Work";
            }
            else
            {
                if (string.IsNullOrEmpty(model.WorkingSubReason))
                {
                    result.Mode = $"{model.WorkingType.Name}";
                }
                else
                {
                    result.Mode = $"{model.WorkingType.Name} - {model.WorkingSubReason}";
                }
            }

            return ProcessResultHelper.Succedded<TeamModeViewModel>(result);



        }



        public Dictionary<int?, string> GetDefaultTechniciansForOrders()
       => GetDefaultTechnicianUsersForOrders().ToDictionary(x => x.Key, x => x.Value.FullName);


        public Dictionary<int?, ApplicationUser> GetDefaultTechnicianUsersForOrders()
        {
            Dictionary<int?, ApplicationUser> teamIdDefaultTechName = _context.Technicians.Include(x => x.User)
                .Where(x => x.TeamId != null && !x.IsDriver).DistinctBy(x => x.TeamId).ToDictionary(x => x.TeamId, x => x.User);
            return teamIdDefaultTechName;
        }
        public ApplicationUser GetDefaultTechniciansForTeam(int teamId)
        {
            ApplicationUser result;
            var teamIdDefaultTechName = GetDefaultTechnicianUsersForOrders();
            teamIdDefaultTechName.TryGetValue(teamId, out result);
            return result;
        }

    }
}