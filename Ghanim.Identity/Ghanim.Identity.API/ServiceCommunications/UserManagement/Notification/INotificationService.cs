﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.ServiceCommunications.Notification;

using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.Notification
{
    public interface INotificationService 
    {
        Task<ProcessResultViewModel<string>> Send(SendNotificationViewModel model);
        Task<ProcessResultViewModel<string>> OldSendToMultiUsers(List<SendNotificationViewModel> lstmodel);
        Task<ProcessResultViewModel<string>> SendToMultiUsers(List<SendNotificationViewModel> lstmodel);

        
    }
}
