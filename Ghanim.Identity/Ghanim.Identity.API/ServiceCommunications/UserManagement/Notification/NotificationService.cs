﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.API.Models;
using Ghanim.API.ServiceCommunications.Notification;

using Ghanim.BLL.IManagers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Utilites.ProcessingResult;
using Ghanim.Models.Context;
using Microsoft.AspNetCore.Http;

namespace Ghanim.API.ServiceCommunications.Notification
{
    public class NotificationService : INotificationService
    {
        public INotificationManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private IConfigurationRoot _configuration;
        public readonly ApplicationDbContext _context;
        private IHttpContextAccessor _iHttpContextAccessor;

        public NotificationService(IConfigurationRoot configuration, INotificationManager _manager, IMapper _mapper, IHttpContextAccessor iHttpContextAccessor,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,ApplicationDbContext context)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            _configuration = configuration;
            _context = context;
            _iHttpContextAccessor = iHttpContextAccessor;
        }

        /// <summary>
        /// This method is to call Notification service and send list of notifications to it
        /// </summary>
        /// <param name="lstModel">List of notifications needed to be sent</param>
        /// <returns>requestedAction::get the url from appsettings by creating this property in DropoutServiceSettings.cs file
        /// public string SendNotifications { get; set; }
        /// BaseUrl ::get from setting ,this url gotten from httpClient Setting
        /// Calling PostCustomize whick takes a model of data to be submitted as an input and returns string result if success or fail,
        /// and to make service communication between User Management And Notification services.
        /// </returns>

     
        public async Task<ProcessResultViewModel<string>> Send(SendNotificationViewModel model)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            if (!string.IsNullOrEmpty(model.UserId))
            {
                result = await SendPushNotificationAzureAsync(model.UserId, model.Title, model.Body, model.Data, apiKey);
            }
            return ProcessResultViewModelHelper.Succedded(result);
        }


        public async Task<ProcessResultViewModel<string>> OldSendToMultiUsers(List<SendNotificationViewModel> lstmodel)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            foreach (var model in lstmodel)
            {
                if (!string.IsNullOrEmpty(model.UserId))
                {
                    result = await SendPushNotificationAzureAsync(model.UserId, model.Title, model.Body, model.Data, apiKey);
                }
            }
            return ProcessResultViewModelHelper.Succedded(result);
        }
        //New
        public async Task<ProcessResultViewModel<string>> SendToMultiUsers(List<SendNotificationViewModel> lstmodel)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            foreach (var model in lstmodel)
            {
                if (!string.IsNullOrEmpty(model.UserId))
                {
                    var userDevices = _context.UserDevices.Where(x=>x.Fk_AppUser_Id==model.UserId).ToList();
                    if ( userDevices != null &&userDevices.Count>0)
                    {
                        foreach (var item in userDevices)
                        {
                            //if (_iHttpContextAccessor.HttpContext.Session.IsAvailable)
                            //{
                                result = SendPushNotification(item.DeveiceId, model.Title, model.Body, model.Data, apiKey);
                            //}
                            //else
                            //{
                            //    _iHttpContextAccessor.HttpContext.Session.Clear();
                            //}
                        }
                    }
                }

            }

            return ProcessResultViewModelHelper.Succedded(result);
        }





        public static string SendPushNotification(string deviceId, string title, string body,
            dynamic dataObj, string ApplicationId)
        {
            string str;
            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body,
                        title,
                        sound = "Enabled"
                    },
                    data = dataObj
                };
                var json = JsonConvert.SerializeObject(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", ApplicationId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return str;
        }
        public static async Task<string> SendPushNotificationAzureAsync(string userId, string title, string body,
            dynamic dataObj, string ApplicationId)
        {
          //  userId = "fAW2ceo5Yg0:APA91bGonZfMtN6-vU3sEPxS8RUUpXsixwky0OULqY7aFdy0MULhXnfTMfvXCpcVU1nhq4q0BQnKKz3YetYWAQMTxh5NUKIA9DcES0HTpIjsW8AXq4QmqcllutK695wswYhWrRswY6-K";
            string str = "error";
            string[] userTag = new string[1];
            userTag[0] = "username:" + userId;
            //userTag[1] = "from:" + user;

            Microsoft.Azure.NotificationHubs.NotificationOutcome outcome = null;

            try
            {
                if (dataObj.NotificationType == "Assign Member To Team")
                {
                    var data = new
                    {
                        data = new
                        {
                            title,
                            body,
                            dataObj
                        }
                    };
                    outcome = await Notifications.Instance.Hub.SendFcmNativeNotificationAsync(JsonConvert.SerializeObject(data), userTag);
                }
                else
                {
                    var data = new
                    {
                        notification = new
                        {
                            body,
                            title,
                            sound = "Enabled"
                        },
                        data = dataObj
                    };
                    outcome = await Notifications.Instance.Hub.SendFcmNativeNotificationAsync(JsonConvert.SerializeObject(data), userTag);
                }
                // Android
                //var notif = "{ \"data\" : {\"message\":\"" + body + "\"}}";

                if (outcome != null)
                {
                    if (!((outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Abandoned) ||
                        (outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Unknown)))
                    {
                        str = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return str;
        }


    }
}
