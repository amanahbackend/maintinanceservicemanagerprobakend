﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.Notification
{
    public interface ISMSService
    {
        ProcessResultViewModel<bool> Send(SendSmsViewModel model);
    }
}
