﻿using DnsClient;

using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.Notification
{
    public class SMSService : ISMSService
    {
        SMSKey _SMSKey;
        public SMSService(IOptions<SMSKey> SMSKey)
        {
            _SMSKey = SMSKey.Value;
        }

        public ProcessResultViewModel<bool> Send( SendSmsViewModel model)
        {
            var smsUrl = string.Format(_SMSKey.URL + "G=965{0}&M={1}&P={2}", model.PhoneNumber, model.Message, _SMSKey.Key);
            WebRequest request = WebRequest.Create(smsUrl);
            request.Method = "GET";

            //     string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(_SMSKey.Name + ":" + _SMSKey.Key));
            //   request.Headers.Add("Authorization", "Basic " + encoded);

            request.ContentLength = 0;
            WebResponse response = request.GetResponse();
            return ProcessResultViewModelHelper.Succedded(true);
        }

    }




}
