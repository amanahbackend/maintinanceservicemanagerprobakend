﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using Ghanim.API.Helper;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnums;
using DocumentFormat.OpenXml.Spreadsheet;
using Ghanim.API.Utilities.HangFireServices;
using Ghanim.Models.Context;
using Ghanim.Models.Order.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Utilities.ProcessingResult;
using DocumentFormat.OpenXml.EMMA;
using Utilites.PaginatedItemsViewModel;
using Ghanim.API.ViewModels.Order;
using Utilites.PaginatedItems;
using DispatchProduct.RepositoryModule;
using Ghanim.API.Models;
using System.Diagnostics;
using AutoMapper.QueryableExtensions;
using Ghanim.API.ViewModels.PaymentTransaction;
using Ghanim.Models.Payment.Entities;
using Utilities.Utilites.GenericListToExcel;

namespace Ghanim.API.ServiceCommunications.Payment
{

    public class PaymentTransactionService : IPaymentTransactionService
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<ApplicationRole> _identityRoleManager;
        private readonly UserManager<ApplicationUser> _identityUserManager;
        public IOrderManager manager;
        public readonly new IMapper mapper;
        public readonly new IAPIHelper _helper;
        private readonly IHostingEnvironment _hostingEnv;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;


        public PaymentTransactionService(IAPIHelper helper, IHostingEnvironment hostingEnv, IOrderManager _manager,
            ApplicationDbContext context,
            RoleManager<ApplicationRole> identityRoleManager,
            UserManager<ApplicationUser> identityUserManager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper
            )
        {
            _helper = helper;


            _context = context;
            _identityUserManager = identityUserManager;
            _identityRoleManager = identityRoleManager;
            _hostingEnv = hostingEnv;
            manager = _manager;

            mapper = _mapper;
            helper = _helper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }




        IQueryable<PaymentTransaction> CalcQuarable(PaymentTransactionFilters model = null)
        {
            int pageIndex = 0;
            int pageCount = 10;
            try
            {
                pageIndex = (model?.PageNo) == 0 ? 0 : (int)(model?.PageNo - 1);
                pageCount = model?.PageSize == 0 ? 10 : (int)(model?.PageSize);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            PaginationContainerQuarableItems<PaymentTransaction> result = new PaginationContainerQuarableItems<PaymentTransaction>();


            var baseQuery = _context.PaymentTransactions.AsQueryable();

            if (model.DateFrom != null)
            {
                baseQuery = baseQuery.Where(x => x.CreatedDate > model.DateFrom);
            }
            if (model.DateTo != null)
            {
                baseQuery = baseQuery.Where(x => x.CreatedDate < model.DateTo);
            }
            if (model.PaymentMode != null && model.PaymentMode.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.PaymentMode.Contains(x.PaymentMode));
            }
            if (model.PaymentStatus != null && model.PaymentStatus.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.PaymentStatus.Contains(x.PaymentStatus));
            }
            if (!string.IsNullOrEmpty(model.OrderCode))
            {
                baseQuery = baseQuery.Where(x => x.Order.Code == model.OrderCode);
            }
            if (model.OrderHistoryStatusId != null && model.OrderHistoryStatusId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.OrderHistoryStatusId.Contains(x.OrderHistoryStatusId));
            }
            if (model.OrderCurrentStatusId != null && model.OrderCurrentStatusId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.OrderCurrentStatusId.Contains(x.Order.StatusId));
            }
            if (model.FK_CreatedBy_Id != null && model.FK_CreatedBy_Id.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.FK_CreatedBy_Id.Contains(x.FK_CreatedBy_Id));
            }
            if (model.CurrentDispatcherId != null && model.CurrentDispatcherId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => x.CurrentDispatcherId != null && model.CurrentDispatcherId.Contains((int)x.CurrentDispatcherId));
            }
            if (model.CurrentSupervisorId != null && model.CurrentSupervisorId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => x.CurrentSupervisorId != null && model.CurrentSupervisorId.Contains((int)x.CurrentSupervisorId));

            }
            if (model.CurrentTechnicianId != null && model.CurrentTechnicianId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => x.CurrentTechnicianId != null && model.CurrentTechnicianId.Contains((int)x.CurrentTechnicianId));

            }
            if (model.CurrentForemanId != null && model.CurrentForemanId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => x.CurrentForemanId != null && model.CurrentForemanId.Contains((int)x.CurrentForemanId));

            }
            if (model.ServiceTypeId != null && model.ServiceTypeId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.ServiceTypeId.Contains(x.ServiceTypeId));

            }
            if (model.OrderTypeId != null && model.OrderTypeId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => x.Order.TypeId != null && model.OrderTypeId.Contains((int)x.Order.TypeId));
            }
            if (model.OrderContractTypeId != null && model.OrderContractTypeId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => x.Order.ContractTypeId != null && model.OrderContractTypeId.Contains((int)x.Order.ContractTypeId));
            }
            if (model.OrderAreaId != null && model.OrderAreaId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.OrderAreaId.Contains(x.Order.AreaId));
            }
            if (model.OrderCompanyId != null && model.OrderCompanyId.Count > 0)
            {
                baseQuery = baseQuery.Where(x => model.OrderCompanyId.Contains(x.Order.CompanyCodeId));
            }
            if (!string.IsNullOrEmpty(model.OrderCustomerCode))
            {
                baseQuery = baseQuery.Where(x => x.Order.CustomerCode == model.OrderCustomerCode);
            }

            baseQuery = baseQuery.OrderByDescending(x => x.CreatedDate);

            return baseQuery;

        }




        public virtual async Task<ProcessResultViewModel<PaginationContainer<PaymentTransactionViewModel>>> GetAllPaginatedAsync(PaymentTransactionFilters model = null)
        {
            model = model ?? new PaymentTransactionFilters();

            PaginationContainer<PaymentTransactionViewModel> result = new PaginationContainer<PaymentTransactionViewModel>();

            var query = CalcQuarable(model);

            int pageIndex = 0;
            int pageCount = 10;
            try
            {
                pageIndex = (model?.PageNo) == 0 ? 0 : (int)(model?.PageNo - 1);
                pageCount = model?.PageSize == 0 ? 10 : (int)(model?.PageSize);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            result.Count = await query.CountAsync();

            IQueryable<PaymentTransactionViewModel> timeSheetStructureViewLst = query
                .Skip(pageIndex * pageCount)
                .Take(pageCount)
                .ProjectTo<PaymentTransactionViewModel>();

            result.Items = await timeSheetStructureViewLst.ToListAsync();
            return ProcessResultViewModelHelper.Succedded(result);
        }



        public virtual async Task<ProcessResultViewModel<string>> ExportAllAsync(PaymentTransactionFilters model = null)
        {
            List<PaymentTransactionExcellViewModel> query = await CalcQuarable(model).ProjectTo<PaymentTransactionExcellViewModel>().ToListAsync();

            //var orderWithoutTrans = mapper.Map<List<View_Excel_MRObject>, List<OrderManagementReportExportOutputViewModel>>(list);
            //fileResult2 = ListToExcelHelper.WriteObjectsToExcel(orderWithoutTrans, newFile2);


            string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd  HH-mmtt ss-fff");
            string newFile = $"{ _hostingEnv.WebRootPath}\\ExcelSheet\\PaymentTransactions {dateTime}.csv";
            int ExcelSheetindex = newFile.IndexOf("\\ExcelSheet\\");
            string fileResult;

            //model.DateTo = model.DateTo.AddDays(1);

            fileResult = ListToExcelHelper.WriteObjectsToExcel(query, newFile);
            if (fileResult != null && fileResult != "")
            {


                var url = new Uri(fileResult);
                var absolute = url.AbsoluteUri.Substring(ExcelSheetindex);
                //FileInfo file = new FileInfo(newFile);
                //file.MoveTo(Path.ChangeExtension(newFile, ".xlsx"));
                //result = result.Replace(".csv", ".xlsx");
                return ProcessResultViewModelHelper.Succedded(absolute);
            }
            return ProcessResultViewModelHelper.Failed(string.Empty, "Something went wrong while exporting PaymentTransactions report");
        }







    }
}