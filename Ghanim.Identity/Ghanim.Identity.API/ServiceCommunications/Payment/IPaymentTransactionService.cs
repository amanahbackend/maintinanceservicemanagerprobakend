﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonEnum;
using Ghanim.API.Models;
using Ghanim.API.ViewModels.Order;
using Ghanim.API.ViewModels.PaymentTransaction;
using Ghanim.Models.Entities;
using Ghanim.Models.Order.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;

namespace Ghanim.API.ServiceCommunications.Payment
{

    public interface IPaymentTransactionService
    {
          Task<ProcessResultViewModel<PaginationContainer<PaymentTransactionViewModel>>> GetAllPaginatedAsync( PaymentTransactionFilters model = null);
          Task<ProcessResultViewModel<string>> ExportAllAsync(PaymentTransactionFilters model = null);
    }
}
