﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AvailabilityController : BaseAuthforAdminController<IAvailabilityManager, Availability, AvailabilityViewModel>
    {
        IServiceProvider _serviceprovider;
        IAvailabilityManager manager;
        IProcessResultMapper processResultMapper;
        public AvailabilityController(IServiceProvider serviceprovider, IAvailabilityManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_AvailabilityManager Lang_Availabilitymanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_AvailabilityManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAvailabilityByLanguage")]
        public ProcessResultViewModel<AvailabilityViewModel> GetAvailabilityByLanguage([FromQuery]int AvailabilityId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_AvailabilityRes = Lang_Availabilitymanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(AvailabilityId);
            entityResult.Data.Name = Lang_AvailabilityRes.Data.Name;
            var result = processResultMapper.Map<Availability, AvailabilityViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<AvailabilityViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "1";
            var actions = manager.GetAll();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var AvailabilityLocalizesRes = Lang_Availabilitymanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_Availability_ID == item.Id);
                    if (AvailabilityLocalizesRes != null && AvailabilityLocalizesRes.Data != null)
                    {
                        item.Name = AvailabilityLocalizesRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<Availability>, List<AvailabilityViewModel>>(actions.Data);
            }
            return processResultMapper.Map<List<Availability>, List<AvailabilityViewModel>>(actions);
        }


        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<AvailabilityViewModel> Post([FromBody] AvailabilityViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<AvailabilityViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<AvailabilityViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] AvailabilityViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
