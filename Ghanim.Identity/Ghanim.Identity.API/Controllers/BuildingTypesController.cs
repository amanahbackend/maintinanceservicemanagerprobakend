﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API.ServiceCommunications.DataManagementService.BuildingTypesService;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BuildingTypesController : BaseAuthforAdminController<IBuildingTypesManager, BuildingTypes, BuildingTypesViewModel>
    {
        IServiceProvider _serviceprovider;
        IBuildingTypesManager manager;
        private readonly IBuildingTypesService _service;
        IProcessResultMapper processResultMapper;
        public BuildingTypesController(IBuildingTypesService service, IServiceProvider serviceprovider, IBuildingTypesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            _service = service;
        }
        private ILang_BuildingTypesManager Lang_BuildingTypesmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_BuildingTypesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetBuildingTypeByLanguage")]
        public ProcessResultViewModel<BuildingTypesViewModel> GetBuildingTypeByLanguage([FromQuery]int BuildingTypeId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_BuildingTypesRes = Lang_BuildingTypesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(BuildingTypeId);
            entityResult.Data.Name = Lang_BuildingTypesRes.Data.Name;
            var result = processResultMapper.Map<BuildingTypes, BuildingTypesViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public async System.Threading.Tasks.Task<ProcessResultViewModel<BuildingTypesViewModel>> GetByNameAsync([FromRoute] string name)
            => await _service.GetByName(name);
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<BuildingTypesViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            var actions = manager.GetAll();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var BuildingTypesLocalizesRes = Lang_BuildingTypesmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_BuildingTypes_ID == item.Id);
                    if (BuildingTypesLocalizesRes != null && BuildingTypesLocalizesRes.Data != null)
                    {
                        item.Name = BuildingTypesLocalizesRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<BuildingTypes>, List<BuildingTypesViewModel>>(actions.Data);

            }
            return processResultMapper.Map<List<BuildingTypes>, List<BuildingTypesViewModel>>(actions);
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<BuildingTypesViewModel> Post([FromBody] BuildingTypesViewModel model)
        {

            if (model == null)
                return ProcessResultViewModelHelper.Failed<BuildingTypesViewModel>(null, SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<BuildingTypesViewModel>(null, string.Format(SharedResource.General_Duplicate, SharedResource.Entity_BuildingTypes), ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] BuildingTypesViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
