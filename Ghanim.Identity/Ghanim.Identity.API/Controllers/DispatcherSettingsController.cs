﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.API.Filters;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.RepositoryModule;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Ghanim.Models.Context;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilites.PaginatedItemsViewModel;
using Utilites.PaginatedItems;
using Utilites;
using Ghanim.ResourceLibrary.Resources;
using AreasVmForDispatcherSettings = Ghanim.BLL.NotMappedModels.AreasVmForDispatcherSettings;
using Microsoft.EntityFrameworkCore;
using Ghanim.API.ViewModels.UserManagement;
namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class DispatcherSettingsController : BaseAuthforAdminController<IDispatcherSettingsManager, DispatcherSettings, DispatcherSettingsViewModel>
    {
        IDispatcherSettingsManager manager;
        IProcessResultMapper processResultMapper;
        IServiceProvider serviceProvider;
        private readonly IIdentityService identityUserService;
        private readonly IDispatcherSettingsService dispatcherSettingsService;
        private readonly ApplicationDbContext _context;

        public DispatcherSettingsController(ApplicationDbContext context, IDispatcherSettingsService _dispatcherSettingsService, IServiceProvider _serviceProvider, IDispatcherSettingsManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityService _identityUserService) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            processResultMapper = _processResultMapper;
            serviceProvider = _serviceProvider;
            identityUserService = _identityUserService;
            dispatcherSettingsService = _dispatcherSettingsService;
            _context = context;
        }
        private IDispatcherManager dispatcherManager
        {
            get
            {
                return serviceProvider.GetService<IDispatcherManager>();
            }
        }
        /// <summary>
        /// This API to add one area and many order problems for one dispatcher in DispatcherSettings table
        /// </summary>
        /// <param name="Model">A model contains area, order problems and disparcherid and name</param>
        /// <returns>List of added dispatcher settings records</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddWithAreaAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithAreaAndOrderProblems([FromBody] AreaWithOrderProblems Model)
        {
            try
            {
                //Getting max groupId of dispatchers from the table then increment this Id so we can add a new groupId
                int lastGroupId = manager.GetAllQuerable().Data.Max(x => x.GroupId);
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                lastGroupId = lastGroupId++;
                //order problems for the dispatcher
                foreach (var item in Model.OrderProblems)
                {
                    DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                    {
                        DispatcherId = Model.DispatcherId,
                        DispatcherName = Model.DispatcherName,
                        AreaId = Model.AreaId,
                        AreaName = Model.AreaName,
                        OrderProblemId = item.ProblemId,
                        OrderProblemName = item.ProblemName,
                        GroupId = lastGroupId
                    };
                    //Getting Division for specific Dispatcher
                    var despatcherRes = dispatcherManager.Get(Model.DispatcherId);
                    if (despatcherRes != null && despatcherRes.Data != null)
                    {
                        res.DivisionId = despatcherRes.Data.DivisionId;
                        //todo mapping infects erorrs
                        //res.DivisionName = despatcherRes.Data.DivisionName;
                    }
                    dispatcherSettingsRes.Add(res);
                }
                //calling base method from the inherited class
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to add an area and many order problems for list of dispatchers in DispatcherSettings table
        /// </summary>
        /// <param name="Model">A model contains an areas, order problems for many disparcherIds and names</param>
        /// <returns>List of added dispatcher settings records</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddWithMultiAreaAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithMultiAreaAndOrderProblems([FromBody] List<AreaWithOrderProblems> lstModel)
        {
            try
            {
                //Getting max groupId of dispatchers from the table then increment this Id so we can add a new groupId
                int lastGroupId = manager.GetAllQuerable().Data.Max(x => x.GroupId);
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                lastGroupId = lastGroupId++;
                //Adding the area and distributing problems for all dispatchers in the list
                foreach (var item in lstModel)
                {
                    foreach (var item2 in item.OrderProblems)
                    {
                        DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                        {
                            DispatcherId = item.DispatcherId,
                            DispatcherName = item.DispatcherName,
                            AreaId = item.AreaId,
                            AreaName = item.AreaName,
                            OrderProblemId = item2.ProblemId,
                            OrderProblemName = item2.ProblemName,
                            GroupId = lastGroupId
                        };
                        //Getting Division for specific Dispatcher
                        var despatcherRes = dispatcherManager.Get(item.DispatcherId);
                        if (despatcherRes != null && despatcherRes.Data != null)
                        {
                            res.DivisionId = despatcherRes.Data.DivisionId;
                            //todo mapping infects erorrs
                            //res.DivisionName = despatcherRes.Data.DivisionName;
                        }
                        dispatcherSettingsRes.Add(res);
                    }
                }
                //calling base method from the inherited class
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to add many areas and many order problems for a dispatcher in DispatcherSettings table
        /// </summary>
        /// <param name="Model">A model contains areas, order problems and the disparcherId and name</param>
        /// <returns>List of added dispatcher settings records</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddWithAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithAreasAndOrderProblems([FromBody] AreasWithOrderProblems Model)
        {
            try
            {
                //Getting max groupId of dispatchers from the table then increment this Id so we can add a new groupId
                int lastGroupId = 0;
                var AllRes = manager.GetAllQuerable();
                if (AllRes != null && AllRes.Data != null && AllRes.Data.Count() > 0)
                {
                    lastGroupId = AllRes.Data.Max(x => x.GroupId);
                }
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                lastGroupId = ++lastGroupId;
                //Adding area info for the dispatcher
                foreach (var area in Model.Areas)
                {
                    //Adding order problem info for the dispatcher
                    foreach (var OrderProblem in Model.OrderProblems)
                    {
                        DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                        {
                            DispatcherId = Model.DispatcherId,
                            DispatcherName = Model.DispatcherName,
                            AreaId = area.AreaId,
                            AreaName = area.AreaName,
                            OrderProblemId = OrderProblem.ProblemId,
                            OrderProblemName = OrderProblem.ProblemName,
                            GroupId = lastGroupId
                        };
                        var despatcherRes = dispatcherManager.Get(Model.DispatcherId);
                        if (despatcherRes != null && despatcherRes.Data != null)
                        {
                            res.DivisionId = despatcherRes.Data.DivisionId;
                            //todo mapping infects erorrs
                            //res.DivisionName = despatcherRes.Data.DivisionName;
                        }
                        dispatcherSettingsRes.Add(res);
                    }
                }
                //calling base method from the inherited class
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to add multiple areas and order problems for List of dispatchers in DispatcherSettings table
        /// </summary>
        /// <param name="Model">A model contains multiple areas, order problems for many disparcherids and names</param>
        /// <returns>List of added dispatcher settings records</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddWithMultiAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddWithMultiAreasAndOrderProblems([FromBody] List<AreasWithOrderProblems> lstModel)
        {
            int alreadyAddedItems = 0;

            try
            {
                //Getting max groupId of dispatchers from the table then increment this Id so we can add a new groupId
                int lastGroupId = 0;
                var AllRes = manager.GetAllQuerable();
                if (AllRes != null && AllRes.Data != null && AllRes.Data.Count() > 0)
                {
                    lastGroupId = AllRes.Data.Max(x => x.GroupId);
                }
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                //distributing areas and problems for all dispatchers in the list
                foreach (var item in lstModel)
                {
                    lastGroupId = ++lastGroupId;
                    foreach (var area in item.Areas)
                    {
                        foreach (var OrderProblem in item.OrderProblems)
                        {
                            DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                            {
                                DispatcherId = item.DispatcherId,
                                DispatcherName = item.DispatcherName,
                                AreaId = area.AreaId,
                                AreaName = area.AreaName,
                                OrderProblemId = OrderProblem.ProblemId,
                                OrderProblemName = OrderProblem.ProblemName,
                                GroupId = lastGroupId
                            };
                            var despatcherRes = dispatcherManager.Get(item.DispatcherId);
                            if (despatcherRes != null && despatcherRes.Data != null)
                            {
                                res.DivisionId = despatcherRes.Data.DivisionId;
                            }
                            dispatcherSettingsRes.Add(res);


                            //var dispatcherDivision = _context.Dispatchers.FirstOrDefault(x => x.Id == item.DispatcherId)
                            //    ?.DivisionId;


                            //bool isAlreadyExisted =
                            //    _context.DispatcherSettings.Any(x =>
                            //        x.DivisionId == dispatcherDivision &&
                            //        x.OrderProblemId == OrderProblem.ProblemId &&
                            //        x.AreaId == area.AreaId
                            //    );

                            //if (!isAlreadyExisted)
                            //    dispatcherSettingsRes.Add(res);
                            //else
                            //    alreadyAddedItems++;

                        }
                    }
                }
                //calling base method from the inherited class
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to get dispatcher settings by dispatcher id
        /// </summary>
        /// <param name="dispatcherId">Dispatcher ID</param>
        /// <returns>Area and Order Problems for this dispather</returns>
        [HttpGet]
        [Route("GetbyDispatcherId/{dispatcherId}")]
        public List<AreaWithOrderProblems> GetbyDispatcherId([FromRoute] int dispatcherId)
        {
            try
            {
                //Getting dispatcher settings by disp id
                var entityResult = manager.GetByDispatcherId(dispatcherId);
                List<AreaWithOrderProblems> areaWithOrderProblems = new List<AreaWithOrderProblems>();
                AreaWithOrderProblems areaWithOrderProblem = new AreaWithOrderProblems();
                List<Problems> problems = new List<Problems>();
                if (entityResult != null)
                {
                    int i = 0;
                    foreach (var item in entityResult.Data)
                    {
                        if (i > 0 && areaWithOrderProblems[i - 1].AreaId == item.AreaId)
                        {
                            Problems problem = new Problems();
                            problem.ProblemId = item.OrderProblemId;
                            problem.ProblemName = item.OrderProblem.Name;
                            problems.Add(problem);
                            areaWithOrderProblem.OrderProblems = problems;
                        }
                        else
                        {
                            areaWithOrderProblem = new AreaWithOrderProblems();
                            areaWithOrderProblem.DispatcherId = item.DispatcherId;
                            areaWithOrderProblem.DispatcherName = $"{item.Dispatcher.User.FirstName} {item.Dispatcher.User.LastName}";
                            areaWithOrderProblem.AreaId = item?.AreaId;
                            areaWithOrderProblem.AreaName = item.Area.Name;
                            Problems problem = new Problems();
                            problem.ProblemId = item.OrderProblemId;
                            problem.ProblemName = item.OrderProblem.Name;
                            problems = new List<Problems>();
                            problems.Add(problem);
                            areaWithOrderProblem.OrderProblems = problems;
                            areaWithOrderProblems.Add(areaWithOrderProblem);
                            i++;
                        }

                    }

                }
                return areaWithOrderProblems;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to get dispatcher settings by dispatcher id
        /// </summary>
        /// <param name="dispatcherId">Dispatcher ID</param>
        /// <returns>The areas and Order Problems for this dispather</returns>
        [HttpGet]
        [Route("GetMultiAreasAndProblemsbyDispatcherId/{dispatcherId}")]
        public List<AreasWithOrderProblems> GetMultiAreasAndProblemsbyDispatcherId([FromRoute] int dispatcherId)
        {
            try
            {
                var entityResult = manager.GetByDispatcherId(dispatcherId);
                List<AreasWithOrderProblems> areasWithOrderProblemsLST = new List<AreasWithOrderProblems>();
                // get grouped data
                var grpRes = entityResult.Data.GroupBy(x => x.GroupId).Select(grp => grp.ToList()).ToList();
                // customize objects
                AreasWithOrderProblems areasWithOrderProblems = new AreasWithOrderProblems();
                List<Problems> problems;
                List<AreasVmForDispatcherSettings> areas;
                Problems problem;
                AreasVmForDispatcherSettings area;
                List<int> AreasId;
                List<int> problemsId;

                if (grpRes != null && grpRes != null && grpRes.Count > 0)
                {
                    foreach (var item2 in grpRes)
                    {
                        areasWithOrderProblems = new AreasWithOrderProblems();
                        problemsId = new List<int>();
                        AreasId = new List<int>();
                        AreasId = new List<int>();
                        areas = new List<AreasVmForDispatcherSettings>();
                        problems = new List<Problems>();
                        foreach (var item in item2)
                        {

                            if (!AreasId.Contains(item?.AreaId ?? 0))
                            {
                                area = new AreasVmForDispatcherSettings();
                                area.AreaId = item.AreaId;
                                area.AreaName = item.Area.Name;
                                AreasId.Add(item?.AreaId ?? 0);
                                areas.Add(area);
                            }
                            if (!problemsId.Contains(item.OrderProblemId))
                            {
                                problem = new Problems();
                                problem.ProblemId = item.OrderProblemId;
                                problem.ProblemName = item.OrderProblem.Name;
                                problemsId.Add(item.OrderProblemId);
                                problems.Add(problem);
                            }
                        }

                        var frstSetting = item2.FirstOrDefault();
                        areasWithOrderProblems.DispatcherId = frstSetting.DispatcherId;
                        areasWithOrderProblems.DispatcherName = $"{frstSetting.Dispatcher.User.FirstName} {frstSetting.Dispatcher.User.LastName}";
                        areasWithOrderProblems.OrderProblems = problems;
                        areasWithOrderProblems.Areas = areas;
                        areasWithOrderProblems.GroupId = frstSetting.GroupId;
                        areasWithOrderProblemsLST.Add(areasWithOrderProblems);
                    }

                }

                return areasWithOrderProblemsLST;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to get dispatcher settings by the settings itself like area and order problem and division together
        /// </summary>
        /// <param name="settings">a model contains area and problem and division</param>
        /// <returns>List of dispatcher settings which matches the input</returns>
        [HttpPost]
        [Route("GetDispatcherBySettings")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> GetDispatcherBySettings([FromBody] SettingsViewModel settings) =>
            dispatcherSettingsService.GetDispatcherSettingsByOrder(settings);
        /// <summary>
        /// This API to get dispatcher settings by group id of group of dispatchers
        /// </summary>
        /// <param name="GroupId">Group ID</param>
        /// <returns>List of areas and list of problems for this group</returns>
        [HttpGet]
        [Route("GetbyGroupId/{GroupId}")]
        public AreasWithOrderProblems GetbyGroupId([FromRoute] int GroupId)
        {
            try
            {
                var entityResult = manager.GetbyGroupId(GroupId);
                AreasWithOrderProblems areasWithOrderProblem = new AreasWithOrderProblems();
                List<Problems> problems = new List<Problems>();
                List<AreasVmForDispatcherSettings> areas = new List<AreasVmForDispatcherSettings>();
                Problems problem = new Problems();
                AreasVmForDispatcherSettings area = new AreasVmForDispatcherSettings();
                List<int> AreasId = new List<int>();
                List<int> problemsId = new List<int>();
                if (entityResult != null && entityResult.Data != null)
                {

                    foreach (var item in entityResult.Data)
                    {

                        if (!AreasId.Contains(item?.AreaId ?? 0))
                        {
                            area = new AreasVmForDispatcherSettings();
                            area.AreaId = item.AreaId;
                            area.AreaName = item.Area.Name;
                            AreasId.Add(item?.AreaId ?? 0);
                            areas.Add(area);
                        }
                        if (!problemsId.Contains(item.OrderProblemId))
                        {
                            problem = new Problems();
                            problem.ProblemId = item.OrderProblemId;
                            problem.ProblemName = item.OrderProblem.Name;
                            problemsId.Add(item.OrderProblemId);
                            problems.Add(problem);
                        }
                    }
                    var frstSetting = entityResult.Data.FirstOrDefault();
                    areasWithOrderProblem.DispatcherId = frstSetting.DispatcherId;
                    areasWithOrderProblem.DispatcherName = $"{frstSetting.Dispatcher.User.FirstName} {frstSetting.Dispatcher.User.LastName}";
                    areasWithOrderProblem.OrderProblems = problems;
                    areasWithOrderProblem.Areas = areas;
                    areasWithOrderProblem.GroupId = GroupId;
                }
                return areasWithOrderProblem;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to update areas and problems for dispatcher settings
        /// </summary>
        /// <param name="Model">Areas and order problems need to be updated</param>
        /// <returns>List of dispatcher settings after update</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("UpdateWithAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> UpdateWithAreasAndOrderProblems([FromBody] AreasWithOrderProblems Model)
        {
            try
            {
                // delete first
                var entityResult = manager.GetbyGroupId(Model.GroupId);
                if (entityResult != null && entityResult.Data != null)
                {
                    foreach (var item in entityResult.Data)
                    {
                        var deleteRes = base.Delete(item.Id);
                    }

                }
                // add new dispatcher Settings
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                foreach (var area in Model.Areas)
                {
                    foreach (var OrderProblem in Model.OrderProblems)
                    {
                        DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                        {
                            DispatcherId = Model.DispatcherId,
                            DispatcherName = Model.DispatcherName,
                            AreaId = area.AreaId,
                            AreaName = area.AreaName,
                            OrderProblemId = OrderProblem.ProblemId,
                            OrderProblemName = OrderProblem.ProblemName,
                            GroupId = Model.GroupId
                        };
                        var despatcherRes = dispatcherManager.Get(Model.DispatcherId);
                        if (despatcherRes != null && despatcherRes.Data != null)
                        {
                            res.DivisionId = despatcherRes.Data.DivisionId;
                            //todo mapping infects erorrs
                            //res.DivisionName = despatcherRes.Data.DivisionName;
                        }
                        dispatcherSettingsRes.Add(res);
                    }
                }
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to update areas and problems for many dispatchers settings
        /// </summary>
        /// <param name="lstModel">Areas and order problems need to be updated for every dispatcher</param>
        /// <returns>List of dispatcher settings after update</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("UpdateMulitWithAreasAndOrderProblems")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> UpdateMulitWithAreasAndOrderProblems([FromBody] List<AreasWithOrderProblems> lstModel)
        {
            try
            {
                ProcessResultViewModel<List<DispatcherSettingsViewModel>> Result = new ProcessResultViewModel<List<DispatcherSettingsViewModel>> { Data = null };
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                foreach (var model in lstModel)
                {
                    // delete first
                    var entityResult = manager.GetbyGroupId(model.GroupId);
                    if (entityResult != null && entityResult.Data != null)
                    {
                        foreach (var item in entityResult.Data)
                        {
                            var deleteRes = base.Delete(item.Id);
                        }
                    }
                    // add new dispatcher Settings

                    foreach (var area in model.Areas)
                    {
                        foreach (var OrderProblem in model.OrderProblems)
                        {
                            DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                            {
                                DispatcherId = model.DispatcherId,
                                DispatcherName = model.DispatcherName,
                                AreaId = area.AreaId,
                                AreaName = area.AreaName,
                                OrderProblemId = OrderProblem.ProblemId,
                                OrderProblemName = OrderProblem.ProblemName,
                                GroupId = model.GroupId
                            };
                            var despatcherRes = dispatcherManager.Get(model.DispatcherId);
                            if (despatcherRes != null && despatcherRes.Data != null)
                            {
                                res.DivisionId = despatcherRes.Data.DivisionId;
                                //todo mapping infects erorrs
                                //res.DivisionName = despatcherRes.Data.DivisionName;
                            }
                            dispatcherSettingsRes.Add(res);
                        }
                    }
                }
                Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to search in dispatchers settings with areas and problems and dispatchers 
        /// and get list of dispatcher settings which matches the search params
        /// </summary>
        /// <param name="model">Search params to search with</param>
        /// <returns>Paginated list of dispatcher settings after filtering</returns>
        [HttpPost]
        [Route("Search")]
        public async Task<IProcessResultViewModel<PaginatedItemsViewModel<DispatcherSettingsViewModel>>> Search([FromBody] DispatcherSettingsFiltersViewModel model)
        {
            try
            {

                var action = mapper.Map<PaginatedItemsViewModel<DispatcherSettingsViewModel>, PaginatedItems<DispatcherSettings>>(model.PageInfo);
                var predicate = PredicateBuilder.True<DispatcherSettings>();

                if (model.AreaNames.Count > 0)
                {
                    //TODO refactor front end must send Area ids not names 
                    predicate = predicate.And(r => model.AreaNames.Contains(r.Area.Name));
                }
                if (model.OrderProblemNames.Count > 0)
                {
                    //TODO refactor front end must send OrderProblemNames ids not names 
                     predicate = predicate.And(r => model.OrderProblemNames.Contains(r.OrderProblem.Name));
                   // var problemName = _context.OrderProblems.Where(op => op.Id == model.OrderProblemId);
                   // predicate = predicate.And(r => r.OrderProblemId==model.OrderProblemId);
                }
                if (model.DispatcherNames.Count > 0)
                {
                    //TODO refactor front end must send dispatcher ids not names 
                    predicate = predicate.And(r => model.DispatcherNames.Contains($"{r.Dispatcher.User.FirstName} {r.Dispatcher.User.LastName}"));
                }

                var paginatedRes = manager.GetAllPaginated(action, predicate);
                var result = processResultMapper.Map<PaginatedItems<DispatcherSettings>, PaginatedItemsViewModel<DispatcherSettingsViewModel>>(paginatedRes);
                var dispatcherIds = paginatedRes.Data.Data.Select(x => x.DispatcherId).ToList();
                var dispatchers = dispatcherManager.GetByIds(dispatcherIds).Data;
                if (dispatchers.Count() > 0)
                {
                    var userIds = dispatchers.Select(x => x.UserId).ToList();
                    //Calling identity service to get the full name of each dispatcher in the list
                    var identity = await identityUserService.GetByUserIds(userIds);

                    foreach (var item in result.Data.Data)
                    {
                        var userId = dispatchers.FirstOrDefault(x => x.Id == item.DispatcherId)?.UserId;
                        var identityObject = identity.Data.FirstOrDefault(x => x.Id == userId);
                        item.FullName = identityObject?.FullName;
                        item.PF = identityObject?.PF;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<PaginatedItemsViewModel<DispatcherSettingsViewModel>>(null, SharedResource.General_WrongWhileGet);
            }
        }
        /// <summary>
        /// This API to get all dispatchers who only have dispatcher settings saved
        /// </summary>
        /// <returns>List of dispatchers</returns>
        /// TODO MAX PERIORITY
        [HttpGet]
        [Route("GetAllDispatchers")]
        public async Task<List<DispatcherViewModel>> GetAllDispatchers()
        {
            try
            {
                List<DispatcherViewModel> Dispatchers = new List<DispatcherViewModel>();
                //Getting all dispatchers in the system

                var dispatcher = dispatcherManager.GetAll(c => c.DispatcherSettings);
                if (dispatcher.Data == null || !dispatcher.Data.Any())
                    return new List<DispatcherViewModel>();
                Dispatchers = dispatcher.Data.Where(c => c.DispatcherSettings != null && c.DispatcherSettings.Count != 0).Select(x => new DispatcherViewModel
                {
                    Id = x.Id,
                    Name = $"{x.User.FirstName} {x.User.LastName}",
                    UserId = x.UserId,
                    FullName = $"{x.User.FirstName} {x.User.LastName}"
                }).ToList();
                return Dispatchers;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This API to delete all the dispatchers settings in the system
        /// </summary>
        /// <returns>Bool result true if succeeded and false if failed</returns>
        [HttpDelete]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("DeleteAllDispatcherSettings")]
        public ProcessResult<bool> DeleteAllDispatcherSettings()
        {
            var deleteDispatcherSettings = manager.DeleteAll();
            if (deleteDispatcherSettings.Data)
            {
                return ProcessResultHelper.Succedded<bool>(true);
            }
           
            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_DispatcherSetting + SharedResource.General_S);
        }
        //////////////////////New /////////////////
        [HttpGet]
        [Route("GetAllProblemsPerDispatcher")]
        public ProcessResultViewModel<List<OrderProblemViewModel>> GetAllProblemsPerDispatcher(int dispatcherId)
        {
            List<OrderProblem> problems = new List<OrderProblem>();
            //var problems = _context.OrderProblems.Include(x => x.DispatcherSettings).ToList();
            //var ProbPerDispId=problems.Where(x=>x.DispatcherSettings.)
            var DispSettingList = _context.DispatcherSettings.Where(x => x.DispatcherId == dispatcherId).Include(x => x.OrderProblem).ToList();
            foreach (var item in DispSettingList)
            {
                problems.Add(item.OrderProblem);
            }
            var result = mapper.Map<List<OrderProblem>, List<OrderProblemViewModel>>(problems);
            return ProcessResultViewModelHelper.Succedded<List<OrderProblemViewModel>>(result);

        }
        [HttpGet]
        [Route("GetAllAreasPerProblem")]
        public ProcessResultViewModel<List<AreasViewModel>> GetAllAreasPerProblem(int problemId, int divisionId)
        {
            List<Areas> AreasList = new List<Areas>();
            var listAreasIdsThatAlreadyAssigned = _context.DispatcherSettings.Where(x => x.OrderProblemId == problemId && x.DivisionId == divisionId).Select(x => x.AreaId);
            var entityResult = _context.Areas.Where(x => !listAreasIdsThatAlreadyAssigned.Contains(x.Id) && x.IsDeleted == false).ToList();
            //foreach (var item in entityResult)
            //{
            //    AreasList.Add(item);
            //}
            var result = mapper.Map<List<Areas>, List<AreasViewModel>>(entityResult);
            return ProcessResultViewModelHelper.Succedded<List<AreasViewModel>>(result);

        }
        [HttpPost]
        [Route("AddDispatcherSetting")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> AddDispatcherSetting([FromBody] List<DispatcherSettingsInputsViewModel> lstModel)
        {
            try
            {
                //DispatcherSettingsViewModel result = new DispatcherSettingsViewModel();
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                if (lstModel.Count > 0)
                {
                    var groupedProplems = lstModel.GroupBy(x => x.OrderProblemId);

                    foreach (var proplemGroup in groupedProplems)
                    {
                        List<int> areasContainerThatalreadySaved = new List<int>();

                        foreach (var dispSetting in proplemGroup)
                        {
                           List<int> dispAreas = dispSetting.Areas.Except(areasContainerThatalreadySaved).ToList();
                            dispSetting.Areas = dispAreas;


                            areasContainerThatalreadySaved.AddRange(dispAreas);


                            foreach (var areaId in dispAreas)
                            {
                                DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                                {
                                    DispatcherId = dispSetting.DispatcherId,
                                    DispatcherName = dispSetting.DispatcherName,
                                    AreaId = areaId,
                                    // AreaName = area.AreaName,
                                    OrderProblemId = dispSetting.OrderProblemId,
                                };
                                var despatcherRes = dispatcherManager.Get(dispSetting.DispatcherId);
                                if (despatcherRes != null && despatcherRes.Data != null)
                                {
                                    res.DivisionId = despatcherRes.Data.DivisionId;
                                }
                                dispatcherSettingsRes.Add(res);
                            }

                        }

                    }



                    //foreach (var item in lstModel)
                    //{         
                    //    foreach (var areaId in item.Areas)
                    //    {                           
                    //            DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                    //            {
                    //                DispatcherId = item.DispatcherId,
                    //                DispatcherName = item.DispatcherName,
                    //                AreaId = areaId,
                    //               // AreaName = area.AreaName,
                    //                OrderProblemId = item.OrderProblemId,                                   
                    //            };
                    //            var despatcherRes = dispatcherManager.Get(item.DispatcherId);
                    //            if (despatcherRes != null && despatcherRes.Data != null)
                    //            {
                    //                res.DivisionId = despatcherRes.Data.DivisionId;
                    //            }
                    //            dispatcherSettingsRes.Add(res);                         
                    //    }

                    //}
                }
                var Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
                //   return ProcessResultViewModelHelper.Succedded<DispatcherSettingsViewModel>(null, "");
            }

            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherSettingsViewModel>>(null, ex.Message);
            }
        }
        [HttpGet]
        [Route("GetDispatcherSettingsByDispatcherId/{dispatcherId}")]
        public List<DispatcherSettingsViewModel> GetDispatcherSettingsByDispatcherId([FromRoute] int dispatcherId)
        {
            try
            {
                AreasVmForDispatcherSettings area;
                AreasVmForDispatcherSettings mappedAreas = new AreasVmForDispatcherSettings();
                List<int> AreasList = new List<int>();
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                var DispatcherObj = _context.Dispatchers.Include(x => x.User).FirstOrDefault(x => x.Id == dispatcherId);
                var listAreasAndProblemsThatAlreadyAssignedByDisp = _context.DispatcherSettings.Where(x => x.DispatcherId == dispatcherId).ToList();
               
                var DispSettingGrouped = listAreasAndProblemsThatAlreadyAssignedByDisp.GroupBy(x => x.OrderProblemId).Select(grp => new { orderProblemId = grp.Key, AreasList = grp.ToList() }).ToList();
               
                foreach (var item in DispSettingGrouped)
                {
                     AreasList =new List<int>();
                    foreach (var areaItem in item.AreasList)
                    {
                        area = new AreasVmForDispatcherSettings();
                        mappedAreas = mapper.Map<AreasVmForDispatcherSettings>(areaItem.Area);
                        AreasList.Add((int)areaItem.AreaId);
                    }
                    DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                    {
                        DispatcherId = dispatcherId,
                        FullName=DispatcherObj.User.FullName,
                        OrderProblemId = item.orderProblemId,
                        DivisionId=DispatcherObj.DivisionId,
                        Areas = AreasList
                    };
                    dispatcherSettingsRes.Add(res);

                }
                return dispatcherSettingsRes;
               // return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }



        [HttpPost]
        [Route("UpdateDispatcherSetting")]
        public ProcessResultViewModel<List<DispatcherSettingsViewModel>> UpdateDispatcherSetting([FromBody] List<DispatcherSettingsInputsViewModel> lstModel)      
        {
            try
            {
                ProcessResultViewModel<List<DispatcherSettingsViewModel>> Result = new ProcessResultViewModel<List<DispatcherSettingsViewModel>> { Data = null };
                List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
                List<DispatcherSettings> dispSettingListPerDispatcher=new List<DispatcherSettings>();
                foreach (var model in lstModel)
                {
                    if (model.Areas != null &&model.Areas.Count>0)
                    {
                        //Delete Old DispSettings
                         dispSettingListPerDispatcher = _context.DispatcherSettings.Where(x => x.DispatcherId == model.DispatcherId).ToList();
                    }
                    if(model.Areas == null)
                    {
                        dispSettingListPerDispatcher = _context.DispatcherSettings.Where(x => x.DispatcherId == model.DispatcherId&&x.OrderProblemId==model.OrderProblemId).ToList();

                    }
                    if (dispSettingListPerDispatcher != null && dispSettingListPerDispatcher.Count >0)
                    {
                        foreach (var item in dispSettingListPerDispatcher)
                        {
                            var deleteRes = base.Delete(item.Id);
                        }
                    }
                    // add new dispatcher Settings
                    if(model.Areas !=null)
                    {
                        foreach (var areaId in model.Areas)
                        {
                            DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
                            {
                                DispatcherId = model.DispatcherId,
                                AreaId = areaId,
                                OrderProblemId = model.OrderProblemId
                            };
                            var despatcherRes = dispatcherManager.Get(model.DispatcherId);
                            if (despatcherRes != null && despatcherRes.Data != null)
                            {
                                res.DivisionId = despatcherRes.Data.DivisionId;
                            }
                            dispatcherSettingsRes.Add(res);
                        }
                    }
                }
                Result = base.PostMulti(dispatcherSettingsRes);
                return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        [HttpGet]
        [Route("GetAllDispatcherSettingsByDispatcherId/{dispatcherId}")]
        public List<AllDispatcherSettingsViewModel> GetAllDispatcherSettingsByDispatcherId([FromRoute] int dispatcherId)
        {
            try
            {                
                List<int> Assignedareas =new List<int>();
                List<AreasViewModel> UnAssignedareas = new List<AreasViewModel>();
                AreasViewModel mappedAreas = new AreasViewModel();
                List<AreasWithAssignedFlag> AreasList = new List<AreasWithAssignedFlag>();
                List<AllDispatcherSettingsViewModel> dispatcherSettingsRes = new List<AllDispatcherSettingsViewModel>();
                var DispatcherObj = _context.Dispatchers.Include(x => x.User).FirstOrDefault(x => x.Id == dispatcherId);
                var listAreasAndProblemsThatAlreadyAssignedByDisp = _context.DispatcherSettings.Where(x => x.DispatcherId == dispatcherId).ToList();

                var DispSettingGrouped = listAreasAndProblemsThatAlreadyAssignedByDisp.GroupBy(x => x.OrderProblemId).Select(grp => new { orderProblemId = grp.Key, AreasList = grp.ToList() }).ToList();

                foreach (var item in DispSettingGrouped)
                {
                    Assignedareas = new List<int>();
                    UnAssignedareas = new List<AreasViewModel>();
                    var listAreasIdsThatAlreadyAssigned = _context.DispatcherSettings.Where(x => x.OrderProblemId == item.orderProblemId && x.DivisionId == DispatcherObj.DivisionId).Select(x => x.AreaId);
                    var UnAssignedentityResult = _context.Areas.Where(x =>x.IsDeleted == false).ToList();
                    var OrderproblemObj = _context.OrderProblems.FirstOrDefault(x => x.Id == item.orderProblemId);
                     var ProblemResult = mapper.Map<OrderProblem,OrderProblemViewModel>(OrderproblemObj);

                    foreach (var areaItem in item.AreasList)
                    {
                        //area = new AreasWithAssignedFlag();
                        //mappedAreas = mapper.Map<AreasWithAssignedFlag>(areaItem.Area);
                        //mappedAreas.IsAssigned = true;
                        Assignedareas.Add((int)areaItem.AreaId);
                        //AreasList.Add(mappedAreas);
                    }
                    foreach (var UnAssignItem in UnAssignedentityResult)
                    {
                       // UnAssignedareas.Add((int)UnAssignItem.Id);
                        //area = new AreasWithAssignedFlag();
                        mappedAreas = mapper.Map<AreasViewModel>(UnAssignItem);
                        //mappedAreas.IsAssigned = false;
                        UnAssignedareas.Add(mappedAreas);
                    }
                    AllDispatcherSettingsViewModel res = new AllDispatcherSettingsViewModel
                    {
                        DispatcherId = dispatcherId,
                        FullName = DispatcherObj.User.FullName,
                        DispatcherName= DispatcherObj.User.FullName,
                        OrderProblemId = item.orderProblemId,
                        DivisionId = DispatcherObj.DivisionId,
                        DivisionName=DispatcherObj.Division.Name,
                        UnAssignedAreasForProblem =UnAssignedareas ,
                        AssignedAreasForProblem=Assignedareas,
                        OrderProblemObj= ProblemResult
                    };
                    dispatcherSettingsRes.Add(res);

                }
                return dispatcherSettingsRes;
                // return Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }



        //[HttpGet]
        //[Route("GetDispatcherSettingsByDispatcherId")]
        //public List<AreasWithOrderProblems> GetDispatcherSettingsByDispatcherId([FromRoute] int dispatcherId)
        //{
        //    List<AreasVmForDispatcherSettings> AreasList = new List<AreasVmForDispatcherSettings>();
        //    List<DispatcherSettingsViewModel> dispatcherSettingsRes = new List<DispatcherSettingsViewModel>();
        //    var listAreasAndProblemsThatAlreadyAssignedByDisp = _context.DispatcherSettings.Where(x=>x.DispatcherId==dispatcherId).ToList();
        //    var listAreasThatAlreadyAssigned = _context.DispatcherSettings.Where(x => x.DispatcherId == dispatcherId).Select(x=>x.AreaId);

        //    var AreasObjList = _context.Areas.Where(x =>! listAreasThatAlreadyAssigned.Contains(x.Id) && x.IsDeleted == false).ToList();

        //    var DispSettingGrouped = listAreasAndProblemsThatAlreadyAssignedByDisp.GroupBy(x => x.OrderProblemId).Select(grp => new { orderProblemId = grp.Key, AreasList = grp.ToList() }).ToList();
        //    var AreasMapped = mapper.Map<List<AreasVmForDispatcherSettings>>(AreasObjList);
        //    foreach (var item in DispSettingGrouped)
        //    {
        //        foreach (var area in item.AreasList)
        //        {
        //            DispatcherSettingsViewModel res = new DispatcherSettingsViewModel
        //            {
        //                DispatcherId = dispatcherId,
        //                OrderProblemId = item.orderProblemId,
        //                Areas=AreasMapped
        //            };
        //        }

        //    }

        //}
    }
}
