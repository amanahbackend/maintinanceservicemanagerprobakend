﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.API.ViewModels;
using Ghanim.Models.Identity.Static;
using Ghanim.ResourceLibrary.Resources;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class EngineerController : BaseAuthforAdminController<IEngineerManager, Engineer, EngineerViewModel>
    {
        public IEngineerManager manger;
        public readonly new IMapper mapper;
        public readonly IEngineerService engineerService;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        public IForemanManager foreManManger;

        public EngineerController(IEngineerManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityService _identityUserService, IEngineerService _engineerService, IForemanManager _foreManManger)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            engineerService = _engineerService;
            foreManManger = _foreManManger;
        }

        /// <summary>
        /// This API to get engineer by his id data including identity data
        /// </summary>
        /// <param name="id">Engineer ID</param>
        /// <returns>A model with enginner data</returns>
        [HttpGet]
        [Route("GetEngineerUser/{id}")]
        public async Task<ProcessResultViewModel<EngineerUserViewModel>> GetEngineerUser([FromRoute] int id)
        {
            try
            {
                //Getting engineer data from Engineer table by id
                ProcessResult<Engineer> engineerTemp = manger.Get(id);
                if (engineerTemp.IsSucceeded && engineerTemp.Data != null)
                {
                    //Calling identity service to get engineer user data
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(engineerTemp.Data.UserId);
                    EngineerUserViewModel result = mapper.Map<Engineer, EngineerUserViewModel>(engineerTemp.Data);
                    mapper.Map<ApplicationUserViewModel, EngineerUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<EngineerUserViewModel>(null, SharedResource.NoEngineerId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<EngineerUserViewModel>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to get engineers data by division id
        /// </summary>
        /// <param name="divisionId">Division ID</param>
        /// <returns>List of engineers data under the given division</returns>
        [HttpGet]
        [Route("GetEngineerUsersByDivisionId/{divisionId}")]
        public async Task<ProcessResultViewModel<List<EngineerUserViewModel>>> GetEngineerUserByDivisionId([FromRoute] int divisionId)
        {
            try
            {
                //Getting all engineers under a specific division id
                ProcessResult<List<Engineer>> engineersTemp = manger.GetAll(x => x.DivisionId == divisionId);
                if (engineersTemp.IsSucceeded && engineersTemp.Data.Count > 0)
                {
                    //Selecting list of engineer user ids 
                    List<string> userIds = engineersTemp.Data.Select(x => x.UserId).ToList();
                    //Calling identity service to get engineers user data like full name, number, etc...
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<EngineerUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<EngineerUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Engineer, EngineerUserViewModel>(engineersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<EngineerUserViewModel>(), SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<EngineerUserViewModel>>(null, ex.Message);
            }
        }

        /// <summary>
        /// This NEW  API to get engineers specific data by division id
        /// </summary>
        /// <param name="divisionId">Division ID</param>
        /// <returns>List of engineers specific data (Id,UserId,FullName) under the given division</returns>
        [HttpGet]
        [Route("GetEngineersByDivisionId /{divisionId}")]
        public async Task<ProcessResultViewModel<List<UserDataViewModel>>> GetEngineersByDivisionId([FromRoute] int divisionId)
        {
            try
            {
                var entityResult = manger.GetAll(x => x.DivisionId == divisionId,c=>c.User);
                if (entityResult.Data != null && entityResult.Data.Count > 0)
                {
                    //var identity = await identityUserService.GetByUserIds(entityResult.Data.Select(x => x.UserId).ToList());

                    //var result = (from member in entityResult.Data
                    //              join id in identity.Data on member.UserId equals id.Id
                    //              select new UserDataViewModel
                    //              {
                    //                  Id = member.Id,
                    //                  FullName = id.FirstName + " " + id.LastName,
                    //                  UserId = member.UserId
                    //              });
                    var result = (from member in entityResult.Data
                                  select new UserDataViewModel
                                  {
                                      Id = member.Id,
                                      FullName = member.User.FirstName + " " + member.User.LastName,
                                      UserId = member.UserId
                                  });
                    return ProcessResultViewModelHelper.Succedded<List<UserDataViewModel>>(result.ToList());
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, SharedResource.General_NoDataFound);
                }
            }
            catch (Exception exp)
            {

                throw;
            }
        }


        /// <summary>
        /// This API to get all engineers data encluding their identity data
        /// </summary>
        /// <returns>List of engineers full data</returns>
        [HttpGet]
        [Route("GetEngineerUsers")]
        public async Task<ProcessResultViewModel<List<EngineerUserViewModel>>> GetEngineerUsers()
        {
            try
            {
                //Getting all engineers from Engineer table
                ProcessResult<List<Engineer>> engineersTemp = manger.GetAll();
                if (engineersTemp.IsSucceeded && engineersTemp.Data.Count > 0)
                {
                    //Selecting list of engineer user ids 
                    List<string> userIds = engineersTemp.Data.Select(x => x.UserId).ToList();
                    //Calling identity service to get engineers user data like full name, number, etc...
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<EngineerUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<EngineerUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Engineer, EngineerUserViewModel>(engineersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<EngineerUserViewModel>(), string.Format(SharedResource.General_No_Entity_Found, SharedResource.Entity_Engineer));
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<EngineerUserViewModel>>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to get a specific engineer data with his identity user id
        /// </summary>
        /// <param name="userId">Identity User ID</param>
        /// <returns>A model of engineer data</returns>
        [HttpGet]
        [Route("GetEngineerByUserId/{userId}")]
        public ProcessResultViewModel<EngineerViewModel> GetEngineerByUserId([FromRoute] string userId) =>
            engineerService.GetEngineerByUserId(userId);

        /// <summary>
        /// This API to block an engineer user
        /// </summary>
        /// <param name="id">Engineer ID</param>
        /// <returns>Bool result, true if block success or false if block failed</returns>
        [HttpPut]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null,SharedResource.UserNotFound  );
            }

            //Blocking user by making him InActive = true in AspNetUsers table
            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                //Logically deleting user from Engineer table by making IsDeleted = true
                var result = manger.LogicalDeleteById(id);
                return result;
            }

            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen + SharedResource.BlockUser);
        }

        /// <summary>
        /// This API just to disable the default GetAll API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Deprecated")]
        public override ProcessResultViewModel<List<EngineerViewModel>> Get()
        {
            return base.Get();
        }

        /// <summary>
        /// This is the new GetAll method which is async, This API gets all Engineer users data and their FullName from identity service
        /// </summary>
        /// <returns>List of engineer users data</returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<ProcessResultViewModel<List<EngineerViewModel>>> GetAsync()
        {
            // implement the base Get() method
            var users = base.Get();
            // Use userIds from Engineer table to get the identity data from identity service
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }

        /// <summary>
        /// This API just to disable the default GetWithBlocked API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("WithBlockedDeprecated")]
        public override ProcessResultViewModel<List<EngineerViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        /// <summary>
        /// This is the new GetAllWithBlocked API which is async, This API gets all Engineer users data 
        /// with their FullName from identity service INCLUDING THE DELETED USERS (IsDeleted = true)
        /// </summary>
        /// <returns>List of Engineer users</returns>
        [HttpGet]
        [Route("GetAllWithBlocked")]
        public async Task<ProcessResult<List<EngineerViewModel>>> GetAllWithBlockedAsync()
        {
            //New
            var users = manger.GetAllWithBlocked(x =>x.User);
            var result = mapper.Map<List<Engineer>, List<EngineerViewModel>>(users.Data);
            foreach (var user in users.Data)
            {
                foreach (var Res in result)
                {
                    if (user.Id == Res.Id)
                    {
                        Res.ForeManCount = user.Foremans.Count();                       
                    }
                }
            }
            return ProcessResultHelper.Succedded(result);
            // implement the base GetAllWithBlocked() method
            //var users = base.GetAllWithBlocked();
            //// Use userIds from Engineer table to get the identity data from identity service
            //var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            //foreach (var item in users.Data)
            //{
            //    item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            //}

            //return users;
        }

        //validate bfore edit
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] EngineerViewModel model)
        {
            try
            {
                var oldModel = manger.Get(model.Id).Data;
                if (model.PF != oldModel.User.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, "Can't change PF");
                }
                if (model.DivisionId != oldModel.DivisionId)
                {
                    var foreManCount = foreManManger.GetAll(x => x.EngineerId == model.Id).Data.Count();
                    if (foreManCount > 0)
                    {

                        return ProcessResultViewModelHelper.Failed<bool>(false,SharedResource.NoChangeEngineerDivForeman, ProcessResultStatusCode.HasChilds);
                    }
                }
                return base.Put(model);
            }

            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }
    }
}