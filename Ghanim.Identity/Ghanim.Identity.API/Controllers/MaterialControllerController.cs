﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{   
    
    [Authorize]

    [Route("api/Material")]
    public class MaterialControllerController : BaseAuthforAdminController<IMaterialControllerManager,MaterialController,MaterialControllerViewModel>
    {
        public IMaterialControllerManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;

        public MaterialControllerController(IMaterialControllerManager _manager, IMapper _mapper, 
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,IIdentityService _identityuserservice) 
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityuserservice;
        }

        /// <summary>
        /// This API to get MaterialController data by his id including identity data
        /// </summary>
        /// <param name="id">MaterialController ID</param>
        /// <returns>A model with MaterialController data</returns>
        [HttpGet]
        [Route("GetMaterialControllerUser/{id}")]
        public async Task<ProcessResultViewModel<MaterialControllerUserViewModel>> GetMaterialControllerUser([FromRoute] int id)
        {
            try
            {
                //Getting MaterialController data from Manager table by id
                ProcessResult<MaterialController> materialTemp = manager.Get(id);
                if (materialTemp.IsSucceeded && materialTemp.Data != null)
                {
                    //Calling identity service to get MaterialController full user data
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(materialTemp.Data.UserId);
                    MaterialControllerUserViewModel result = mapper.Map<MaterialController, MaterialControllerUserViewModel>(materialTemp.Data);
                    mapper.Map<ApplicationUserViewModel, MaterialControllerUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<MaterialControllerUserViewModel>(null, SharedResource.MatrialInvalidId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<MaterialControllerUserViewModel>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to block a MaterialController user
        /// </summary>
        /// <param name="id">MaterialController ID</param>
        /// <returns>Bool result, true if block success or false if block failed</returns>
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.UserNotFound);
            }

            //Blocking user by making him InActive = true in AspNetUsers table
            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                //Logically deleting user from MaterialController table by making IsDeleted = true
                var result = manger.LogicalDeleteById(id);
                return result;
            }

            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen + SharedResource.BlockUser);
        }

        /// <summary>
        /// This API just to disable the default GetAll API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Deprecated")]
        public override ProcessResultViewModel<List<MaterialControllerViewModel>> Get()
        {
            return base.Get();
        }

        /// <summary>
        /// This is the new GetAll method which is async, This API gets all MaterialController users data and their FullName from identity service
        /// </summary>
        /// <returns>List of MaterialController users data</returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<ProcessResultViewModel<List<MaterialControllerViewModel>>> GetAsync()
        {
            // implement the base Get() method
            var users = base.Get();
            // Use userIds from MaterialController table to get the identity data from identity service
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }

        /// <summary>
        /// This API just to disable the default GetWithBlocked API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("WithBlockedDeprecated")]
        public override ProcessResultViewModel<List<MaterialControllerViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        /// <summary>
        /// This is the new GetAllWithBlocked API which is async, This API gets all MaterialController users data 
        /// with their FullName from identity service INCLUDING THE DELETED USERS (IsDeleted = true)
        /// </summary>
        /// <returns>List of MaterialController users</returns>
        [HttpGet]
        [Route("GetAllWithBlocked")]
        public async Task<ProcessResultViewModel<List<MaterialControllerViewModel>>> GetAllWithBlockedAsync()
        {
            // implement the base GetAllWithBlocked() method
            var users = base.GetAllWithBlocked();
            // Use userIds from MaterialController table to get the identity data from identity service
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }


    }
}