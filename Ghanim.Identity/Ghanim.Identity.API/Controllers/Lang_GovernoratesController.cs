﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Ghanim.Models.Identity.Static;
using Ghanim.ResourceLibrary.Resources;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_GovernoratesController : BaseAuthforAdminController<ILang_GovernoratesManager, Lang_Governorates, Lang_GovernoratesViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_GovernoratesManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_GovernoratesController(IServiceProvider serviceprovider, ILang_GovernoratesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IGovernoratesManager Governoratesmanager
        {
            get
            {
                return _serviceprovider.GetService<IGovernoratesManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByGovernorateId/{GovernorateId}")]
        public ProcessResultViewModel<List<GovWithAllLanguagesViewModel>> GetAllLanguagesByGovernorateId([FromRoute]int GovernorateId)
        {
            List<GovWithAllLanguagesViewModel> govWithAllLanguages = new List<GovWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var GovernorateRes = Governoratesmanager.Get(GovernorateId);

            var entityResult = manager.GetAllLanguagesByGovernorateId(GovernorateId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }

            govWithAllLanguages.Add(new GovWithAllLanguagesViewModel { Gov_No = GovernorateRes.Data.Gov_No, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<GovWithAllLanguagesViewModel>>(govWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<GovWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<GovWithAllLanguagesViewModel> govWithAllLanguages = new List<GovWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var GovernorateRes = Governoratesmanager.GetAll();
            foreach (var Governorate in GovernorateRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByGovernorateId(Governorate.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = Governorate.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = Governorate.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                govWithAllLanguages.Add(new GovWithAllLanguagesViewModel { Gov_No = Governorate.Gov_No, languagesDictionaries = languagesDictionary, Id = Governorate.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<GovWithAllLanguagesViewModel>>(govWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public ProcessResultViewModel<List<GovWithAllLanguagesViewModel>> GetAllLanguagesWithBlocked()
        {
            //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
            List<GovWithAllLanguagesViewModel> govWithAllLanguages = new List<GovWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var GovernorateRes = Governoratesmanager.GetAllWithBlocked();
            var OrderedGovernorateRes = GovernorateRes.Data.OrderByDescending(g => g.Id).ToList();
            foreach (var Governorate in OrderedGovernorateRes)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByGovernorateId(Governorate.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = Governorate.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //{
                //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //    {
                //        if (SupportedLanguage.Name == "English")
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = Governorate.Name });

                //        }
                //        else
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //        }
                //    }
                //}
                govWithAllLanguages.Add(new GovWithAllLanguagesViewModel { Gov_No = Governorate.Gov_No, languagesDictionaries = languagesDictionary, Id = Governorate.Id, IsDeleted = Governorate.IsDeleted });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<GovWithAllLanguagesViewModel>>(govWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_Governorates> lstModel)
        {
            var entityResult = manager.UpdateByGovernorate(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]GovWithAllLanguagesViewModel Model)
        {
            string enName = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;
            List<Lang_Governorates> lstModel = new List<Lang_Governorates>();
            var stroredArea = Governoratesmanager.Get(x => x.Name == enName).Data;
            if (stroredArea != null && (stroredArea?.Id ?? 0) != Model.Id)
                return ProcessResultViewModelHelper.Failed<bool>(false, string.Format(SharedResource.General_Duplicate, SharedResource.Entity_Governorates), ProcessResultStatusCode.InvalidValue);

            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_Governorates { FK_Governorates_ID = Model.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = manager.UpdateByGovernorate(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
