﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchProduct.Controllers;

using Ghanim.API.Filters;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ViewModels.UserManagement;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class DispatcherController : BaseAuthforAdminController<IDispatcherManager, Dispatcher, DispatcherViewModel>
    {
        private readonly IDispatcherService _dispatcherservice;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        public IForemanManager foreManManger;
        public IOrderManager orderManager;
        public DispatcherController(IDispatcherService dispatcherservice ,IDispatcherManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityService _identityUserService, IForemanManager _foreManManger, IOrderManager _orderManager) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _dispatcherservice = dispatcherservice;
            manger = _manger;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            foreManManger = _foreManManger;
            orderManager = _orderManager;
        }


        [HttpGet]
        [HttpGet, Route("Get/{id}")]
        public override ProcessResultViewModel<DispatcherViewModel> Get([FromRoute] int id) =>
            _dispatcherservice.GetDispatcherById(id);



        /// <summary>
        /// API to get dispatcher user by his ID, Fetching from Dispatcher table joining with AspNetUsers table and get full data for dispatcher
        /// </summary>
        /// <param name="id">Dispatcher ID</param>
        /// <returns>All dispatcher data from database including data from AspNetUsers table</returns>
        [HttpGet]
        [Route("GetDispatcherUser/{id}")]
        public async Task<ProcessResultViewModel<DispatcherUserViewModel>> GetDispatcherUser([FromRoute] int id)
        {
            try
            {
                //Get dispatcher data from dispatcher table
                ProcessResult<Dispatcher> dispatcherTemp = manger.Get(id);
                if (dispatcherTemp.IsSucceeded && dispatcherTemp.Data != null)
                {
                    //Get dispatcher data from AspNetUsers (Identity service)
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(dispatcherTemp.Data.UserId);
                    DispatcherUserViewModel result = mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatcherTemp.Data);

                    //return foreman and unassigned orders for dispatcher
                    var foreManCount = foreManManger.GetAll(x => x.DispatcherId == id).Data.Count();
                    result.ForeManCount = foreManCount;
                    //var ordersCount = (await orderManager.GetUnAssignedOrdersForDispatcher(id)).Data.Count();
                    var ordersCount =  orderManager.GetAll(x => x.DispatcherId == id && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId)).Data.Count();
                    

                    result.OrderCount = ordersCount;


                    //Mapping both results to get the full data
                    mapper.Map<ApplicationUserViewModel, DispatcherUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<DispatcherUserViewModel>(null, SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<DispatcherUserViewModel>(null, ex.Message);
            }
        }
        /// <summary>
        /// API to get all dispatchers with all their data from Dispatcher table joining with AspNetUsers table
        /// </summary>
        /// <returns>List of Dispatchers data</returns>
        [HttpGet]
        [Route("GetDispatcherUsers")]
        public async Task<ProcessResultViewModel<List<DispatcherUserViewModel>>> GetDispatcherUsers()
        {
            try
            {
                //Get all users from Dispatcher table
                ProcessResult<List<Dispatcher>> dispatchersTemp = manger.GetAll();
                if (dispatchersTemp.IsSucceeded && dispatchersTemp.Data.Count > 0)
                {
                    List<string> userIds = dispatchersTemp.Data.Select(x => x.UserId).ToList();
                    //Get all dispatcher users from AspNetUsers table (Identity service)
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<DispatcherUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<DispatcherUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Dispatcher, DispatcherUserViewModel>(dispatchersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<DispatcherUserViewModel>(), SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DispatcherUserViewModel>>(null, ex.Message);
            }
        }

        /// <summary>
        /// API to get dispatchers by given divisionId
        /// </summary>
        /// <param name="divisionId">Division ID</param>
        /// <returns>List of dispatchers data</returns>
        [HttpGet]
        [Route("GetDispatcherUsersByDivisonId/{divisionId}")]
        public ProcessResultViewModel<List<DispatcherViewModel>> GetDispatcherUsersAsync(int divisionId) =>
            _dispatcherservice.GetDispatchersByDivision(divisionId);
        
        /// <summary>
        /// API to get dispatchers with all their data from Dispatcher table joining with AspNetUsers tabledispatchers by given SupervisorId
        /// </summary>
        /// <param name="supervisorId">Supervisor ID</param>
        /// <returns>List of dispatchers data</returns>
        [HttpGet]
        [Authorize(Roles = StaticRoles.Supervisor)]
        [Route("GetDispatcherUsersBySupervisorId/{supervisorId}")]
        public async Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetDispatcherUsersBySupervisorId(int supervisorId) =>
            await _dispatcherservice.GetDispatchersBySupervisorId(supervisorId);
        /// <summary>
        /// API to get dispatcher data by userId related to identity
        /// </summary>
        /// <param name="userId">User ID from AspNetUsers table</param>
        /// <returns>Dispatcher data</returns>
        [HttpGet]
        [Route("GetDispatcherByUserId/{userId}")]
        public ProcessResultViewModel<DispatcherViewModel> GetDispatcherByUserIdAsync([FromRoute] string userId) =>
            _dispatcherservice.GetDispatcherByUserId(userId);

        /// <summary>
        /// API to block dispatcher user
        /// </summary>
        /// <param name="id">Dispatcher ID</param>
        /// <returns>Bool result, true if block success or false if block failed</returns>
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.UserNotFound);
            }

            //Blocking user by making him InActive = true in AspNetUsers table
            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                //Logically deleting user from Dispatcher table by making IsDeleted = true
                var result = manger.LogicalDeleteById(id);
                return result;
            }

            return ProcessResultHelper.Failed<bool>(false, null,SharedResource.General_SomethingWrongHappen +SharedResource.BlockUser  );
        }
        /// <summary>
        /// This API just to disable the default GetAll method which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Deprecated")]
        public override ProcessResultViewModel<List<DispatcherViewModel>> Get()
        {
            return base.Get();
        }

        /// <summary>
        /// This is the new GetAll method which is async, This API gets all dispatcher users data with their FullName from identity service
        /// </summary>
        /// <returns>List of dispatcher users data</returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetAsync() =>
            await _dispatcherservice.GetAll();

        /// <summary>
        /// This API just to disable the default GetWithBlocked API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("WithBlockedDeprecated")]
        public override ProcessResultViewModel<List<DispatcherViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        /// <summary>
        /// This is the new GetAllWithBlocked() method which is async, This API gets all dispatcher users data 
        /// with their FullName from identity service INCLUDING THE DELETED USERS (IsDeleted = true)
        /// </summary>
        /// <returns>List of dispatcher users</returns>
        [HttpGet]
        [Route("GetAllWithBlocked")]
        public async Task<ProcessResult<List<DispatcherViewModelWithCounts>>> GetAllWithBlockedAsync()
        {
            var dispatchers = manger.GetAllQuerableWithBlocked().Data.ProjectTo<DispatcherViewModelWithCounts>().ToList();
            return ProcessResultHelper.Succedded(dispatchers);
        }

        /// <summary>
        /// This API to get dispatchers FullNames by their IDs
        /// </summary>
        /// <param name="ids">List of Dispatchers IDs</param>
        /// <returns>List of dispatcher identity data</returns>
        [HttpPost]
        [Route("GetFullNameByIds")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetFullNameByIds([FromBody] List<int> ids)
        {
            var disp = manger.GetByIds(ids).Data?.Select(x => x.UserId).ToList();
            if (disp == null || disp.Count() == 0)
            {
                return ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, SharedResource.General_InvalidInput);
            }

            var identity = await identityUserService.GetByUserIds(disp);
            return identity;
        }


        [HttpGet]
        [Route("GetByIds")]
        public override ProcessResultViewModel<List<DispatcherViewModel>> GetByIds([FromBody] List<int> ids) =>
             _dispatcherservice.GetDispatchersByIds(ids);



        //for validation
        [HttpGet]
        [Route("GetDispatchersDataBySupervisorId/{supervisorId}")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        //[Authorize(Roles = StaticRoles.Dispatcher)]
        public async Task<ProcessResultViewModel<List<UserDataViewModel>>> GetDispatchersDataBySupervisorId([FromRoute]int supervisorId)
        {
            try
            {
                var entityResult = manger.GetAll(x => x.SupervisorId == supervisorId, x => x.User);
                if (entityResult.Data != null && entityResult.Data.Count > 0)
                {
                    var result = entityResult.Data.Select(x => new UserDataViewModel
                    {
                        Id = x.Id,
                        FullName = x.User?.FullName ?? "",
                        UserId = x.UserId
                    });

                    return ProcessResultViewModelHelper.Succedded<List<UserDataViewModel>>(result.ToList());
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, ex.Message);
            }
        }
        //validate bfore edit
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("UpdateDisp")]
        public async Task<ProcessResultViewModel<bool>> PutAsync([FromBody] DispatcherViewModel model)
        {
            try
            {
                var oldModel = manger.Get(model.Id).Data;
                if (model.PF != oldModel.User.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false,SharedResource.NoChangePF);
                }
                if (model.SupervisorId != oldModel.SupervisorId || model.DivisionId != oldModel.DivisionId)
                {
                    var foreManCount = foreManManger.GetAll(x => x.DispatcherId == model.Id).Data.Count();
                    var ordersCount = orderManager.GetAll(x => x.DispatcherId == model.Id && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId)).Data.Count();

                    //if (foreManCount > 0 || ordersCount > 0)
                    //{

                    //    return ProcessResultViewModelHelper.Failed<bool>(false, "You can't change this dispatcher division, this dispatcher has current orders and foreman users");
                    //    //, Utilities.ProcessingResult.ProcessResultStatusCode.InvalidValue
                    //}
                    if (foreManCount > 0 )
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false,SharedResource.NoChangeDispatcherCurrentForman, ProcessResultStatusCode.HasChilds);
                    }
                    if( ordersCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeDispatcherCurrentOrder, ProcessResultStatusCode.AssignedForWork);
                    }
                }
                return base.Put(model);
            }

            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }

        //[HttpPost]
        //[Route("Add")]
        //public override ProcessResultViewModel<DispatcherViewModel> Post([FromBody] DispatcherViewModel model)
        //{            
        //    var res= _dispatcherservice.AddDispatcher(model);
        //    //return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message); 
            
        //    return null;
        //}

    }
}