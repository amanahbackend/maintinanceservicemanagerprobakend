﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.Models.Identity.Static;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Route("api/[controller]")]
    public class SupportedLanguagesController : BaseAuthforAdminController<ISupportedLanguagesManager, SupportedLanguages, SupportedLanguagesViewModel>
    {
        IProcessResultMapper processResultMapper;
        ISupportedLanguageService _supportedLanguageService;

        public SupportedLanguagesController(ISupportedLanguageService supportedLanguageService, ISupportedLanguagesManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            processResultMapper = _processResultMapper;
            _supportedLanguageService = supportedLanguageService;
        }

        [HttpGet]
        [Authorize]
        [Route("GetByName/{name}")]
        public  ProcessResultViewModel<SupportedLanguagesViewModel> GetByName(string name) =>
        _supportedLanguageService.GetByName(name).Result;

        /////new
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<SupportedLanguagesViewModel>> Get() =>
            _supportedLanguageService.GetAll().Result;

        [Route("Get/{id}")]
        [HttpGet]
        [Authorize]
        public override ProcessResultViewModel<SupportedLanguagesViewModel> Get([FromRoute]int id) =>
            _supportedLanguageService.Get(id).Result;


        [HttpPost]
        [Route("Add")]
        [Authorize(Roles = StaticRoles.Admin)]
        public override ProcessResultViewModel<SupportedLanguagesViewModel> Post([FromBody] SupportedLanguagesViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<SupportedLanguagesViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manger.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<SupportedLanguagesViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }


        [HttpPut]
        [Route("Update")]
        [Authorize(Roles = StaticRoles.Admin)]
        public override ProcessResultViewModel<bool> Put([FromBody] SupportedLanguagesViewModel model)
        {
            var entity = manger.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity != null && (entity?.Id ?? 0) != model.Id)
                return ProcessResultViewModelHelper.Failed<bool>(false, "Item Already Existed With That Name", ProcessResultStatusCode.InvalidValue);

            else
                return base.Put(model);

            //   return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }


        [HttpDelete]
        [Authorize(Roles = StaticRoles.Closed)]
        [Route("Delete/{id}")]
        public override ProcessResultViewModel<bool> Delete([FromRoute]int id)
        {
            return base.Delete(id);
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpDelete]
        [Route("LogicalDelete/{id}")]
        public override ProcessResultViewModel<bool> LogicalDelete([FromRoute]int id)
        {
            return base.LogicalDelete(id);

        }




    }
}
