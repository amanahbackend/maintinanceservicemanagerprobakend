﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AttendanceStatesController : BaseAuthforAdminController<IAttendanceStatesManager, AttendanceStates, AttendanceStatesViewModel>
    {
        IServiceProvider _serviceprovider;
        IAttendanceStatesManager manager;
        IProcessResultMapper processResultMapper;
        public AttendanceStatesController(IServiceProvider serviceprovider, IAttendanceStatesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_AttendanceStatesManager Lang_AttendanceStatesmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_AttendanceStatesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAttendanceStatesByLanguage")]
        public ProcessResultViewModel<AttendanceStatesViewModel> GetAttendanceStatesByLanguage([FromQuery]int AttendanceStatesId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_AttendanceStatesRes = Lang_AttendanceStatesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(AttendanceStatesId);
            entityResult.Data.Name = Lang_AttendanceStatesRes.Data.Name;
            var result = processResultMapper.Map<AttendanceStates, AttendanceStatesViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<AttendanceStatesViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "1";
            var actions = manager.GetAll();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var AttendanceStatesLocalizesRes = Lang_AttendanceStatesmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_AttendanceStates_ID == item.Id);
                    if (AttendanceStatesLocalizesRes != null && AttendanceStatesLocalizesRes.Data != null)
                    {
                        item.Name = AttendanceStatesLocalizesRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<AttendanceStates>, List<AttendanceStatesViewModel>>(actions.Data);
                //return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
                //return ProcessResultHelper.Succedded<List<AreasViewModel>>(result);
            }
            return processResultMapper.Map<List<AttendanceStates>, List<AttendanceStatesViewModel>>(actions);
        }


        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<AttendanceStatesViewModel> Post([FromBody] AttendanceStatesViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<AttendanceStatesViewModel>(null, SharedResource.General_NoVal_Body  , ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<AttendanceStatesViewModel>(null, string.Format(SharedResource.General_Duplicate, SharedResource.Entity_AttendanceStates), ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] AttendanceStatesViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }

    }
}
