﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Utilities.Utilites.GenericListToExcel;
using Ghanim.Models.Order.Enums;
using Microsoft.EntityFrameworkCore;
using Utilites.PaginatedItemsViewModel;
using Ghanim.Models.Views;
using Microsoft.AspNetCore.Authorization;
using Ghanim.API.Helper;

namespace Ghanim.API.Controllers
{
   
    public partial class OrderController
    {
       
        [HttpPost]
        [Route("Reschedual")]
        public ProcessResult<bool> Reschedual([FromBody] ReschedualOrderDto model)
        {
            try
            {
                if (model != null)
                {

                    var result = manager.GetAllQuerable().Data.FirstOrDefault(x => x.Code == model.OrderCode);
                    if (result != null )
                    {
                        
                        var visitSlotObj = _IVisitSlotManager.Get(x => x.Code == model.VisitSlotCode);
                        result.Visitdate = model.Visitdate;
                        result.VisitSlotId = visitSlotObj.Data.Id;
                        manager.Update(result);
                        OrderRemoteRequestsHistory tempHistory = new OrderRemoteRequestsHistory()
                        {
                            Code = result.Code,
                            RequestBody= Newtonsoft.Json.JsonConvert.SerializeObject(model),
                            CreatedDate =DateTime.Now,
                            
                        };
                       

                        _IOrderRemoteRequestsHistoryManager.Add(tempHistory);

                        OrderViewModel searchResult = mapper.Map<OrderObject, OrderViewModel>(result);
                        return ProcessResultHelper.Succedded<bool>(true,"appointment changed successfully");
                    }
                    else
                    {
                        OrderRemoteRequestsHistory tempHistory = new OrderRemoteRequestsHistory()
                        {
                            Code = model.OrderCode,
                            RequestBody = Newtonsoft.Json.JsonConvert.SerializeObject(model),
                            CreatedDate = DateTime.Now,
                            OrderRequestTypeId= OrderRequestTypeEnum.Reschedule

                        };
                        _IOrderRemoteRequestsHistoryManager.Add(tempHistory);

                        OrderRemoteRequestsOnProcess RemainRequest = new OrderRemoteRequestsOnProcess()
                        {
                            Code = model.OrderCode,
                            OrderRequestTypeId = OrderRequestTypeEnum.Reschedule,
                            VisitCode= model.VisitSlotCode,
                            VisitDate= model.Visitdate,
                            CreatedDate = DateTime.Now,

                        };
                        _IOrderRemoteRequestsOnProcessManager.Add(RemainRequest);
                        return ProcessResultHelper.Succedded<bool>(true, "Order not yeat reached ,will be changed once reach to Database ");
                    }
                }
                return ProcessResultHelper.Failed<bool>(false, null, "filter properties are required ");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<bool>(false, null, ex.Message); 
            }
         
        }
      

    }
     
}