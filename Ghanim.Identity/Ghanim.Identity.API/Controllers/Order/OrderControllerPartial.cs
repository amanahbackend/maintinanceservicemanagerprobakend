﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Utilities.Utilites.GenericListToExcel;
using Ghanim.Models.Order.Enums;
using Microsoft.EntityFrameworkCore;
using Utilites.PaginatedItemsViewModel;
using Ghanim.Models.Views;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using Ghanim.API.Helper;

namespace Ghanim.API.Controllers
{ 
        public partial class OrderController
        {




        //void CalcCount(IQueryable<View_Excel_MRObject> quarable, ref int count)
        //{
        //    count = quarable.Count();
        //}


        //void Getdata(IQueryable<View_Excel_MRObject> quarable,ref List<View_Excel_MRObject> data)
        //{
        //    data = quarable.ToList();
        //}

        //NOTE db Using View_Excel_MR
        private async Task<Tuple<int, List<View_Excel_MR_DefaultAction>>> GetView_Excel_MRAsync(OrderManagementReportViewModel model, bool IsForExporing = false)
        {
            if (model.PageInfo == null)
                model.PageInfo = new PaginatedItemsViewModel<OrderViewModel>();
            if (model.PageInfo.PageNo < 1)
                model.PageInfo.PageNo = 1;
            if (model.PageInfo.PageSize < 10)
                model.PageInfo.PageSize = 10;

            
            var baseQuery = _context.View_Excel_MR_DefaultAction.AsQueryable();
            //order action 
            //  baseQuery = baseQuery.Where(x => x.WorkingTypeId != 9);
            baseQuery = baseQuery.Where(x => x.WorkingTypeId == null || (x.WorkingTypeId != 9 && x.WorkingTypeId != 8));
            //baseQuery = baseQuery.Where(x => x.WorkingTypeId != 8 );

            //&& x.WorkingTypeId != 8)
            if (model.AreaId?.Count() > 0)
            {
                baseQuery = baseQuery.Where(x => model.AreaId.Contains(x.AreaId.Value));
            }









            //if (model?.DispatcherId?.Count() > 0)
            //{
            //    baseQuery = baseQuery.Where(r => model.DispatcherId.Contains(r.DispatcherId ?? 0));
            //}

            //if (model?.DivisionId?.Count() > 0)
            //{
            //    baseQuery = baseQuery.Where(r => model.DivisionId.Contains(r.DivisionId ?? 0));//TODO Refactor issue
            //}
            //if (model?.OrderTypeId?.Count() > 0)
            //{
            //    baseQuery = baseQuery.Where(r => model.OrderTypeId.Contains(r.TypeId));
            //}
            //if (model?.ProblemId?.Count() > 0)
            //{
            //    baseQuery = baseQuery.Where(r => model.ProblemId.Contains(r.ProblemId ?? 0));//TODO Refactor issue
            //}











            if (model?.DispatcherId?.Count() > 0)
            {
                baseQuery = baseQuery.Where(r => r.DispatcherId != null && model.DispatcherId.Contains((int)r.DispatcherId));
            }

            if (model?.DivisionId?.Count() > 0)
            {
                baseQuery = baseQuery.Where(r => r.DivisionId != null && model.DivisionId.Contains((int)r.DivisionId));//TODO Refactor issue
            }
            if (model?.OrderTypeId?.Count() > 0)
            {
                baseQuery = baseQuery.Where(r => model.OrderTypeId.Contains(r.TypeId));
            }
            if (model?.ProblemId?.Count() > 0)
            {
                baseQuery = baseQuery.Where(r => r.ProblemId != null && model.ProblemId.Contains((int)r.ProblemId));//TODO Refactor issue
            }
            if (model?.StatusId?.Count() > 0)
            {
                baseQuery = baseQuery.Where(r => model.StatusId.Contains(r.StatusId));
            }


                if (!string.IsNullOrEmpty(model?.CustomerCode))
                {
                    baseQuery = baseQuery.Where(r => model.CustomerCode == r.Customer_ID);
                }
                if (model?.CompanyCode?.Count() > 0)
                {
                    baseQuery = baseQuery.Where(r => model.CompanyCode.Contains(r.CompanyCodeId));
                }
                if (!string.IsNullOrEmpty(model.OrderId))
                {
                    baseQuery = baseQuery.Where(r => model.OrderId == r.Order_ID);
                }

                if (model.DateFrom != null)
                {
                    baseQuery = baseQuery.Where(x => (x.Created_Date >= model.DateFrom));
                }

                if (model.DateTo != null)
                {
                    baseQuery = baseQuery.Where(x => (x.Created_Date <= model.DateTo));
                }






                //if not include all progresss => get from order table
                //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO  get group by create date 
                //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO  get group by create date 
                //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO  get group by create date 
                //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO  get group by create date 
                //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO  get group by create date 
                //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO  get group by create date 
                //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO  get group by create date 













                //orderActionPredicate = orderActionPredicate.And(x => x.PlatformTypeSource != PlatformType.Ftp);

                //if (model.AllProgress && model.Techs.Count == 0)
                //{
                //    orderActionPredicate =
                //        orderActionPredicate.Or(x => (x.PlatformTypeSource == PlatformType.Ftp && x.Order.OrderActions.Count == 1));
                //}

            IQueryable<View_Excel_MR_DefaultAction> ViewExcel = null;
            if (!model.IncludeProgressData)
            {
                ViewExcel = baseQuery.Where(x => x.PlatformTypeSource == PlatformType.Ftp);
            }
            else
            {
                //Stopwatch sw1 = new Stopwatch();
                //sw1.Start();

                //var ordersThatHasNoActionsButFtb = baseQuery.GroupBy(x => x.Id).Where(x => x.Count() == 1).Select(k => k.FirstOrDefault());
                //var lst1 = ordersThatHasNoActionsButFtb.ToList();
                //sw1.Stop();
                //var elapsed = sw1.ElapsedMilliseconds;





                Stopwatch sw2 = new Stopwatch();

                sw2.Start();
 
                sw2.Stop();
                var elapsed2 = sw2.ElapsedMilliseconds;










                //special case
                if (model.AllProgress && model.Techs.Count == 0)
                {

                    //var list = _context.Orders.Where(x => x.OrderActions.Count == 1).Select(x => x.Id);//.ToList(); //TODO todo
                    //                                                                                   //var ordersThatHasNoActionsButFtb2 = baseQuery.Where(x => list.Contains(x.Id));
                    //var lst2 = list.ToList();
                    //ViewExcel = baseQuery.Where(x => x.PlatformTypeSource != PlatformType.Ftp || list.Contains(x.Id));

                    //// ViewExcel = ViewExcel.Concat(ordersThatHasNoActionsButFtb.ToList());
                    ////ViewExcel = ViewExcel.Union(ordersThatHasNoActionsButFtb2.ToList());
                    ////var  union = ViewExcel.(ordersThatHasNoActionsButFtb);
                    ////var asdasjdw = concattt.Count();
                    ////var asdasjd2 = union.Count();
                    ///

                    //
                    ViewExcel = baseQuery.Where(x =>
                    (x.PlatformTypeSource==PlatformType.Ftp &&x.LastActionID!=null&&x.DefaultActionID!=null&& x.Acid==x.LastActionID&&x.Acid==x.DefaultActionID) 
                    ||
                    (x.PlatformTypeSource!=PlatformType.Ftp &&x.DefaultActionID!=null&& x.DefaultActionID == x.Acid) 
                    );

                }
                else if(model.Techs.Count>0)
                {
                    ViewExcel = baseQuery.Where(x => x.PlatformTypeSource != PlatformType.Ftp);
                    ViewExcel = ViewExcel.Where(x => x.acTechnicianId != null && model.Techs.Contains((int)x.acTechnicianId));

                }
                else
                {
                    ViewExcel = baseQuery.Where(x => x.PlatformTypeSource != PlatformType.Ftp);
                }



    ////            List<int> defaultTechs = _context.Technicians
    ////.Where(x => x.TeamId != null && !x.IsDriver).DistinctBy(x => x.TeamId).Select(x => x.Id).ToList();
    //            if (model.Techs.Count > 0)
    //            {
    //                ViewExcel = ViewExcel.Where(x => x.acTechnicianId != null && model.Techs.Contains((int)x.acTechnicianId));
    //                // ViewExcel = ViewExcel.Where(x => x.Progress_Date_History != null).GroupBy(x => x.Progress_Date_History.Value).SelectMany(t => t);

    //            }
                if(!model.AllProgress) 
                {
                    //bool useGroupBy = false;
                    //if (useGroupBy)
                    //{
                    //    //dont show any actions for drivers && and only for one tech
                    //    ViewExcel = ViewExcel.Where(x => x.acTechnicianId == null || x.acTechnicianIsDriver == null || !x.acTechnicianIsDriver.Value);
                    //    ViewExcel = ViewExcel.Where(x => x.Progress_Date_History != null).GroupBy(x => x.Progress_Date_History.Value).Select(t => t.FirstOrDefault());

                    //}
                    //else
                    //{
                    //List<int> defaultTechs = _context.Technicians
                    //    .Where(x => x.TeamId != null && !x.IsDriver).DistinctBy(x => x.TeamId).Select(x => x.Id).ToList();
                    //ViewExcel = ViewExcel.Where(x => x.acTechnicianId == null || defaultTechs.Contains((int)x.acTechnicianId));
                    //}


                

                    ViewExcel = baseQuery.Where(x => x.PlatformTypeSource != PlatformType.Ftp &&x.DefaultActionID!=null&& x.Acid == x.DefaultActionID);


                      //  ViewExcel = ViewExcel.Where(x => x.acTechnicianId == null || defaultTechs.Contains((int)x.acTechnicianId));
                    

                    }




                if (model.AllProgress)
                {
                    //mo filter just include all actions
                }
                if (model.LastProgress)
                {


                    //ViewExcel = ViewExcel.OrderBy(x => x.Progress_Date_History).GroupBy(x => x.Id).Select(x => x.LastOrDefault());

                    ViewExcel = ViewExcel.Where(x =>x.LastActionID!=null && x.Acid == x.LastActionID);
                }
               
                if (model.OnHoldOnly)
                {
                    ViewExcel = ViewExcel.Where(x => x.acStatusId != null && x.acStatusId == (int)OrderStatusEnum.On_Hold

                    //to include orderActionId Index
                    || x.LastActionID > int.MaxValue

                    );
                    //orderPredicate = orderPredicate.And(x => x.OrderActions.Count > 0);
                }
                if (model.OnHoldLastProgress)
                {
                    ViewExcel = ViewExcel.Where(x => x.acStatusId == (int)OrderStatusEnum.On_Hold || (x.LastActionID !=null && x.Acid == x.LastActionID));
                    ////  ViewExcel = ViewExcel.Where(x => x.Acid == x.LastActionID || x.acStatusId == (int)OrderStatusEnum.On_Hold);

                    ////var last = ViewExcel.OrderBy(x => x.Progress_Date_History).GroupBy(x => x.Id)
                    ////    .Select(x => x.LastOrDefault(oa => oa.StatusId != (int)OrderStatusEnum.On_Hold)).Where(oa => oa != null);
                    

                    ////Get Last Order Actions In a seperate query
                    //var lastEnhanced = ViewExcel.Where(x => x.Acid == x.LastActionID && x.StatusId != (int)OrderStatusEnum.On_Hold);
                  
                    ////continue with onhold logic and get actions for default techs
                    //ViewExcel = ViewExcel.Where(x => x.acTechnicianId == null || defaultTechs.Contains((int)x.acTechnicianId));
                    //ViewExcel = ViewExcel.Where(x => x.acStatusId == (int)OrderStatusEnum.On_Hold);

                    ////union last actions with onhold actions
                    //ViewExcel = ViewExcel.Union(lastEnhanced.ToList());
                    ////orderPredicate = orderPredicate.And(x => x.OrderActions.Count > 0);
                }


                }


            //Stopwatch sw1 = new Stopwatch();
            //sw1.Start();



            //int countt = 0;
            //List<View_Excel_MRObject> data = null;
            //Task task1 = Task.Factory.StartNew(() => CalcCount(ViewExcel, ref countt));
            //Task task2 = Task.Factory.StartNew(() => Getdata(ViewExcel, ref data));

            //Task.WaitAll(task1, task2);

            //sw1.Stop();
            //var elapsed = sw1.ElapsedMilliseconds;



            //Console.WriteLine("All threads complete");







            if (!model.LastProgress)
               ViewExcel = ViewExcel.OrderBy(x  => x.Id).ThenBy(x => x.Progress_Date_History);


            if (IsForExporing)
                return new Tuple<int, List<View_Excel_MR_DefaultAction>>(0, await ViewExcel.ToListAsync());
            else
            {
                int count = await ViewExcel.CountAsync();
                ViewExcel = ViewExcel.Skip((model.PageInfo.PageNo - 1) * model.PageInfo.PageSize).Take(model.PageInfo.PageSize);


                return new Tuple<int, List<View_Excel_MR_DefaultAction>>(count, await ViewExcel.ToListAsync());
            } 
        }



            [HttpPost]
            [Route("GetOrderManagementReportExportRefactored")]
            [Authorize(Roles = StaticRoles.Supervisor)]
            public async Task<ProcessResult<string>> GetOrderManagementReportExportRefactored([FromBody] OrderManagementReportViewModel model)
            {




                string dateTime2 = DateTime.UtcNow.ToString("yyyy-MM-dd  HH-mmtt ss-fff");
                string newFile2 = $"{ _hostingEnv.WebRootPath}\\ExcelSheet\\Order Management {dateTime2}.csv";



                int ExcelSheetindex2 = newFile2.IndexOf("\\ExcelSheet\\");

                try
                {

                var dataQuarable = await GetView_Excel_MRAsync(model, true);
                var list = dataQuarable.Item2;
                //all transactions maaping
                string fileResult2 = "";
                if (!model.IncludeProgressData)
                {
                    var orderWithoutTrans = mapper.Map<List<View_Excel_MR_DefaultAction>, List<OrderManagementReportExportOutputViewModel>>(list);
                    fileResult2 = ListToExcelHelper.WriteObjectsToExcel(orderWithoutTrans, newFile2);
                }
                else
                {

                    var orderWithTrans = mapper.Map<List<View_Excel_MR_DefaultAction>, List<OrderManagementReportExportOutputViewModelWithProgreassColomns>>(list);
                    fileResult2 = ListToExcelHelper.WriteObjectsToExcel(orderWithTrans, newFile2);
                }

                    if (!string.IsNullOrEmpty(fileResult2))
                    {
                        var url = new Uri(fileResult2);
                        var absolute = url.AbsoluteUri.Substring(ExcelSheetindex2);
                        return ProcessResultHelper.Succedded(absolute);
                    }
                    else
                    {
                        return ProcessResultHelper.Failed(string.Empty, null, "Something went wrong while exporting order report");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }


                return null;


                //var teamIdDefaultTechName = GetDefaultTechniciansForOrders();





                //var result = GetReportQuery(model, IsForExporing: true);
                //var data = result.Item2.ToList();







                //string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd  HH-mmtt ss-fff");
                //string newFile = $"{ _hostingEnv.WebRootPath}\\ExcelSheet\\Order Management {dateTime}.csv";



                //int ExcelSheetindex = newFile.IndexOf("\\ExcelSheet\\");


                //string fileResult = "";


                ////Write to excell file
                //if (model.IncludeProgressData)
                //{
                //    var orderActionsListData = data.Select(x => new OrderManagementReportExportOutputViewModelWithProgreassColomns(x, teamIdDefaultTechName)).ToList();
                //    fileResult = ListToExcelHelper.WriteObjectsToExcel(orderActionsListData, newFile);
                //}
                //else
                //{
                //    var orderActionsListData = data.Select(x => new OrderManagementReportExportOutputViewModel(x, teamIdDefaultTechName)).ToList();
                //    fileResult = ListToExcelHelper.WriteObjectsToExcel(orderActionsListData, newFile);
                //}




                //if (!string.IsNullOrEmpty(fileResult))
                //{
                //    var url = new Uri(fileResult);
                //    var absolute = url.AbsoluteUri.Substring(ExcelSheetindex);
                //    return ProcessResultHelper.Succedded(absolute);
                //}
                //else
                //{
                //    return ProcessResultHelper.Failed(string.Empty, null, "Something went wrong while exporting order report");
                //}



            }




            [HttpPost]
            [Route("GetOrderManagementReportPaginatedRefactored")]
            [Authorize(Roles = StaticRoles.Supervisor)]
            public async Task<ProcessResult<OrderManagementReportPaginatedViewModel>> GetOrderManagementReportPaginatedRefactored([FromBody] OrderManagementReportViewModel model)
            {
                var dataQuarable = await GetView_Excel_MRAsync(model);

            var dataAsLst = new List<View_Excel_MR_DefaultAction>();
            try
            {
                dataAsLst = dataQuarable.Item2;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            var dataAsLstAndMApped = mapper.Map<List<View_Excel_MR_DefaultAction>, List<OrderManagementReportItem>>(dataAsLst);

                //Map To The View Models
                var output = new OrderManagementReportPaginatedViewModel()
                {
                    Count = dataQuarable.Item1,
                    Result = dataAsLstAndMApped,
                };
                return ProcessResultHelper.Succedded(output, "OrderManagementReport");
            }




            #region deprecated


            private Expression<Func<OrderObject, bool>> GetOrderPredicate(OrderManagementReportViewModel model)
            {
                var orderPredicate = PredicateBuilder.True<OrderObject>();


                if (model?.AreaId?.Count() > 0)
                {
                    orderPredicate = orderPredicate.And(r => model.AreaId.Contains(r.AreaId));
                }
                if (model?.DispatcherId?.Count() > 0)
                {
                    orderPredicate = orderPredicate.And(r => model.DispatcherId.Contains(r.DispatcherId ?? 0));//TODO Refactor issue
                }
                if (model?.DivisionId?.Count() > 0)
                {
                    orderPredicate = orderPredicate.And(r => model.DivisionId.Contains(r.DivisionId ?? 0));//TODO Refactor issue
                }
                if (model?.OrderTypeId?.Count() > 0)
                {
                    orderPredicate = orderPredicate.And(r => r.TypeId != null && model.OrderTypeId.Contains(r.TypeId.Value));
                }
                if (model?.ProblemId?.Count() > 0)
                {
                    orderPredicate = orderPredicate.And(r => model.ProblemId.Contains(r.ProblemId ?? 0));//TODO Refactor issue
                }
                if (model?.StatusId?.Count() > 0)
                {
                    orderPredicate = orderPredicate.And(r => model.StatusId.Contains(r.StatusId));
                }


                if (!string.IsNullOrEmpty(model?.CustomerCode))
                {
                    orderPredicate = orderPredicate.And(r => model.CustomerCode == r.CustomerCode);
                }
                if (model?.CompanyCode?.Count() > 0)
                {
                    orderPredicate = orderPredicate.And(r => model.CompanyCode.Contains(r.CompanyCodeId));
                }
                if (!string.IsNullOrEmpty(model.OrderId))
                {
                    orderPredicate = orderPredicate.And(r => model.OrderId == r.Code);
                }

                if (model.DateFrom != null)
                {
                    orderPredicate = orderPredicate.And(x => (x.SAP_CreatedDate >= model.DateFrom));
                }

                if (model.DateTo != null)
                {
                    orderPredicate = orderPredicate.And(x => (x.SAP_CreatedDate <= model.DateTo));
                }


                if (model.AllProgress)
                {
                    //mo filter just include all actions
                }
                if (model.LastProgress)
                {

                    //not a driver also
                    orderPredicate = orderPredicate.And(x => x.OrderActions.Count > 0);
                }
                if (model.OnHoldOnly)
                {
                    orderPredicate = orderPredicate.And(x => x.OrderActions.Count > 0);
                }
                if (model.OnHoldLastProgress)
                {
                    orderPredicate = orderPredicate.And(x => x.OrderActions.Count > 0);
                }


                return orderPredicate;
            }



            private Expression<Func<OrderAction, bool>> GetOrderActionPredicate(OrderManagementReportViewModel model, bool searchWithStatus)
            {
                var orderActionPredicate = PredicateBuilder.True<OrderAction>();


                orderActionPredicate = orderActionPredicate.And(x => x.OrderId != null);
                orderActionPredicate = orderActionPredicate.And(x => x.WorkingTypeId != 9);


                if (model.Techs.Count > 0)
                {
                    orderActionPredicate = orderActionPredicate.And(x => x.TechnicianId != null && model.Techs.Contains((int)x.TechnicianId));
                }
                else
                {
                    orderActionPredicate = orderActionPredicate.And(x => x.TechnicianId == null || !x.Technician.IsDriver);
                }

                orderActionPredicate = orderActionPredicate.And(x => x.PlatformTypeSource != PlatformType.Ftp);

                if (model.AllProgress && model.Techs.Count == 0)
                {
                    orderActionPredicate =
                        orderActionPredicate.Or(x => (x.PlatformTypeSource == PlatformType.Ftp && x.Order.OrderActions.Count == 1));
                }

                if (searchWithStatus)
                {
                    //mapping will be from order actions not the orders
                    if (model.AllProgress)
                    {
                        //if (model.Techs.Count > 0)
                        //{
                        //    orderActionPredicate =
                        //        orderActionPredicate.And(x => x.PlatformTypeSource != PlatformType.Ftp);
                        //}
                        //else
                        //{
                        //    //IF FILter will all progress and no tech was selected view also all orders that read from ftb and has no actions
                        //}
                    }
                    if (model.LastProgress)
                    {
                        //orderActionPredicate =
                        //    orderActionPredicate.And(x => x.PlatformTypeSource != PlatformType.Ftp);
                    }
                    if (model.OnHoldOnly)
                    {
                        //orderActionPredicate =
                        //    orderActionPredicate.And(x => x.PlatformTypeSource != PlatformType.Ftp);
                        orderActionPredicate = orderActionPredicate.And(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                    }
                    if (model.OnHoldLastProgress)
                    {
                        //orderActionPredicate =
                        //    orderActionPredicate.And(x => x.PlatformTypeSource != PlatformType.Ftp);
                        orderActionPredicate = orderActionPredicate.And(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                    }
                }


                return orderActionPredicate;
            }

            private Tuple<int, List<OrderManagementReportItem>> GetReportQuery(OrderManagementReportViewModel model, int pagezize = int.MaxValue, int pageindex = 0, bool IsForExporing = false)
            {


                List<Expression<Func<OrderObject, object>>> orderInclude = new List<Expression<Func<OrderObject, object>>>()
            {
                x=> x.Status,
                //x=> x.Dispatcher,
                x=> x.Dispatcher.User,
                //x=> x.Dispatcher.Supervisor,
                //x=> x.Dispatcher.Supervisor.User,
                x=> x.Division,
                x=> x.Problem,
                //x=> x.Supervisor,
                x=> x.Supervisor.User,
                //x=> x.Team,
                //x=> x.Team.Technicians,
                //x=> x.Team.Foreman,
                x=> x.Team.Foreman.User,
                x=> x.ContractType
            };
                List<Expression<Func<OrderAction, object>>> orderActionInclude = new List<Expression<Func<OrderAction, object>>>()
            {
                x=> x.CostCenter,
                //orderActionInclude.Add(x => x.Order);
               //x=> x. Technician,
               x=> x. Technician.User,
                x=>x.Status,
                x=>x.ExcuterUser,
                //x=> x.Dispatcher,
                x=> x.Dispatcher.User,
        };

                var orderPredicate = GetOrderPredicate(model);
                IQueryable<OrderObject> orderQuery = _context.Orders.Where(orderPredicate);
                IQueryable<OrderAction> orderActionQuery = null;

                foreach (var orderIncludeItem in orderInclude)
                    orderQuery = orderQuery.Include(orderIncludeItem);

                //TODO dddddddddddd get orders if its not include orders
                Tuple<int, IQueryable<OrderManagementReportItem>> result = null;
                if (!model.IncludeProgressData)
                {
                    var page = Paginate(orderQuery, model.PageInfo.PageSize, pageindex);
                    var list = page.Item2.Select(x => new OrderManagementReportItem(x)).ToList();


                    //result= new Tuple<int, IQueryable<OrderManagementReportItem>>(orderQuery.Count(), orderQuery.Select(x => mapper.Map<OrderObject, OrderManagementReportItem>(x)));
                    return new Tuple<int, List<OrderManagementReportItem>>(page.Item1, list);
                }
                else
                {

                    var orderActionPredicate = GetOrderActionPredicate(model, true);

                    List<int> ordersTogetActionsFor = _context.Orders.Where(orderPredicate).Select(x => x.Id).ToList();


                    //foreach (var orderActionIncludeItem in orderActionInclude)
                    //    orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                    //foreach (var orderIncludeItem in orderInclude)
                    //    orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);

                    if (model.AllProgress)
                    {
                        //dont pass pageSize or index in this case




                        //orderActionQuery = orderQuery.SelectMany(x => x.OrderActions.AsQueryable().Where(orderActionPredicate)).Where(x => x != null).AsQueryable().Select(x=> x.Id);
                        orderActionQuery = _context.OrderActions.Where(x => x.OrderId != null && ordersTogetActionsFor.Contains((int)x.OrderId)).Where(orderActionPredicate);

                        if (model.Techs.Count == 0)
                        {
                            List<int> listWithoutDublivation = orderActionQuery.GroupBy(x => x.ActionDate)
                                .Select(x => x.FirstOrDefault().Id).ToList();


                            orderActionQuery = _context.OrderActions;
                            foreach (var orderActionIncludeItem in orderActionInclude)
                                orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                            foreach (var orderIncludeItem in orderInclude)
                                orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);
                            orderActionQuery = orderActionQuery.Where(a => listWithoutDublivation.Contains(a.Id));
                        }
                        else
                        {
                            foreach (var orderActionIncludeItem in orderActionInclude)
                                orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                            foreach (var orderIncludeItem in orderInclude)
                                orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);
                        }




                        //if (model.Techs.Count == 0)
                        //{
                        //    orderActionQuery = orderActionQuery.DistinctBy(x => x.ActionDate).AsQueryable();
                        //    foreach (var orderActionIncludeItem in orderActionInclude)
                        //        orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                        //    foreach (var orderIncludeItem in orderInclude)
                        //        orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);


                        //}


                        if (IsForExporing)
                        {
                            //int totalCount = orderActionQuery.Count();
                            ////int numer =await orderActionQuery.CountAsync();
                            //int numerOfPages = (int)Math.Ceiling(a: totalCount / 10);
                            //List<OrderManagementReportItem> allpagesResults = new List<OrderManagementReportItem>();
                            //for (int i = 0; i < numerOfPages; i++)
                            //{
                            //    var qualrablePage = orderActionQuery.Skip(i * 10).Take(10);
                            //    List<OrderAction> page = qualrablePage.ToList();
                            //    List<OrderManagementReportItem> tesult = page.Select(x => new OrderManagementReportItem(x)).ToList();
                            //    allpagesResults.AddRange(tesult);
                            //}

                            //var qualrablePage = orderActionQuery.Skip(pageindex * pagezize).Take(pagezize);
                            List<OrderAction> page = orderActionQuery.ToList();
                            List<OrderManagementReportItem> tesult = page.Select(x => new OrderManagementReportItem(x)).ToList();
                            return new Tuple<int, List<OrderManagementReportItem>>(int.MaxValue, tesult);
                            //return new Tuple<int, List<OrderManagementReportItem>>(totalCount, allpagesResults);
                        }
                        else
                        {
                            var qualrablePage = orderActionQuery.Skip(pageindex * pagezize).Take(pagezize);
                            List<OrderAction> page = qualrablePage.ToList();
                            List<OrderManagementReportItem> tesult = page.Select(x => new OrderManagementReportItem(x)).ToList();
                            return new Tuple<int, List<OrderManagementReportItem>>(orderActionQuery.Count(), tesult);
                        }


                        //if (model.AllProgress && model.Techs.Count == 0)
                        //{

                        //    result = new Tuple<int, IQueryable<OrderManagementReportItem>>(orderActionQuery.Count(), orderActionQuery.Select(x => new OrderManagementReportItem(x)));
                        //    orderQuery = orderQuery.Where(x => x.OrderActions.Count() == 0);
                        //    foreach (var orderIncludeItem in orderInclude)
                        //        orderQuery = orderQuery.Include(orderIncludeItem);

                        //    //IQueryable<OrderManagementReportItem> orders = orderQuery.Select(x => mapper.Map<OrderObject, OrderManagementReportItem>(x));
                        //    IQueryable<OrderManagementReportItem> orders = orderQuery.Select(x => new OrderManagementReportItem(x));


                        //    List<OrderManagementReportItem> orderss = orders.ToList();
                        //    //List<string> ordersWeared = new List<string>()
                        //    //{
                        //    //    "5200712196",
                        //    //    "5101420601",
                        //    //    "5101337386",
                        //    //    "5101337387",
                        //    //    "5101337389",
                        //    //};
                        //    //foreach (var VARIABLE in orderss)
                        //    //{

                        //    //    if (ordersWeared.Contains(VARIABLE.OrderCode))
                        //    //    {

                        //    //    }
                        //    //}


                        //    result = new Tuple<int, IQueryable<OrderManagementReportItem>>(result.Item1 + orders.Count(), (result.Item2.Concat(orders)).Skip(pagezize * pageindex));
                        //}
                        //else
                        //{

                        //}

                        //return result;
                    }

                    if (model.OnHoldOnly)
                    {
                        //get on hold actions
                        orderActionQuery = orderQuery.SelectMany(x => x.OrderActions).Where(orderActionPredicate)
                            .Where(x => x != null);

                        //orderActionQuery = orderActionQuery.AsQueryable().OrderBy(x => x.OrderId);

                        foreach (var orderActionIncludeItem in orderActionInclude)
                            orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                        foreach (var orderIncludeItem in orderInclude)
                            orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);



                        orderActionQuery = orderActionQuery.DistinctBy(x => x.ActionDate);

                        foreach (var orderActionIncludeItem in orderActionInclude)
                            orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                        foreach (var orderIncludeItem in orderInclude)
                            orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);

                        var qualrablePage = orderActionQuery.Skip(pageindex * pagezize).Take(pagezize);

                        List<OrderAction> page = qualrablePage.ToList();
                        List<OrderManagementReportItem> tesult = page.Select(x => new OrderManagementReportItem(x)).ToList();
                        //var tesult = orderActionQuery.Skip(pageindex * pagezize).Take(pagezize).Select(x => new OrderManagementReportItem(x));

                        return new Tuple<int, List<OrderManagementReportItem>>(orderActionQuery.Count(), tesult);
                    }

                    if (model.OnHoldLastProgress)
                    {
                        //get on hold actions
                        orderActionQuery = orderQuery.SelectMany(x => x.OrderActions).Where(orderActionPredicate)
                            .Where(x => x != null)
                            .DistinctBy(x => x.ActionDate).AsQueryable();

                        //get last action
                        var predicatForLast = GetOrderActionPredicate(model, false);
                        var queryForLast = orderQuery.SelectMany(x => x.OrderActions).Where(predicatForLast).Where(x => x != null);
                        queryForLast = queryForLast.GroupBy(x => x.OrderId).Select(x => x.LastOrDefault()).AsQueryable();


                        //var l = queryForLast.ToList();
                        //var o = orderActionQuery.ToList();
                        //concat last with onhold


                        orderActionQuery = orderActionQuery.Concat(queryForLast).Distinct().OrderBy(x => x.OrderId);


                        //o = orderActionQuery.ToList();






                        foreach (var orderActionIncludeItem in orderActionInclude)
                            orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                        foreach (var orderIncludeItem in orderInclude)
                            orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);


                        List<OrderAction> page = orderActionQuery.Skip(pageindex * pagezize).Take(pagezize).ToList();

                        List<OrderManagementReportItem> tesult = page.Select(x => new OrderManagementReportItem(x)).ToList();

                        //try
                        //{
                        //    var asdasda = tesult.ToList();

                        //}
                        //catch (Exception e)
                        //{
                        //    Console.WriteLine(e);
                        //}


                        return new Tuple<int, List<OrderManagementReportItem>>(orderActionQuery.Count(), tesult);
                        //int testCount = orderActionQuery.Count();


                        //orderActionQuery = orderActionQuery.Include(x => x.Dispatcher);


                        //var testCountasd = orderActionQuery.ToList();
                    }

                    if (model.LastProgress)
                    {

                        //foreach (var orderIncludeItem in orderInclude)
                        //    orderQuery = orderQuery.Include(orderIncludeItem);

                        //foreach (var orderActionIncludeItem in orderActionInclude)
                        //    orderQuery = orderQuery.Include(x=> x.OrderActions).ThenInclude(orderActionIncludeItem);

                        //TODO find another soution in better time
                        orderActionQuery = orderQuery.Include(x => x.OrderActions).SelectMany(x => x.OrderActions).Where(orderActionPredicate).Where(x => x != null);
                        List<int> desiredOrderActionIds = orderActionQuery.GroupBy(x => x.OrderId).Select(x => x.LastOrDefault()).ToList().Select(x => x.Id).ToList();

                        orderActionQuery = _context.OrderActions.Where(x => desiredOrderActionIds.Contains(x.Id));




                        foreach (var orderActionIncludeItem in orderActionInclude)
                            orderActionQuery = orderActionQuery.Include(orderActionIncludeItem);

                        foreach (var orderIncludeItem in orderInclude)
                            orderActionQuery = orderActionQuery.Include(x => x.Order).ThenInclude(orderIncludeItem);


                        var tesult = orderActionQuery.Skip(pageindex * pagezize).Take(pagezize).Select(x => new OrderManagementReportItem(x)).ToList();
                        return new Tuple<int, List<OrderManagementReportItem>>(orderActionQuery.Count(), tesult);
                    }


                    return null;

                    //int testCount2 = orderActionQuery.Count();


                    //orderActionQuery.Include(x => x.Dispatcher);

                    //var asdasd2 = orderActionQuery.ToList();






                    //var dasdasd = orderActionQuery.FirstOrDefault().Dispatcher;
                    ////if (!model.AllProgress)
                    ////{
                    ////    orderActionQuery = orderActionQuery.DistinctBy(x => x.ActionDate);
                    ////}

                    ////result = new Tuple<int, IQueryable<OrderManagementReportItem>>(orderActionQuery.Count(), orderActionQuery.Select(x => mapper.Map<OrderAction, OrderManagementReportItem>(x)));



                    ////Map To The View Models
                }
                //return result;
            }


            private Tuple<int, IQueryable<T>> Paginate<T>(IQueryable<T> query, int pagezize = int.MaxValue, int pageindex = 0)
            {
                int count = query.Count();
                IQueryable<T> resut = query.Skip(pageindex * pagezize).Take(pagezize);
                return new Tuple<int, IQueryable<T>>(count, resut);
            }


            //@@
            [HttpPost]
            [Authorize(Roles = StaticRoles.Supervisor)]
            [Route("GetOrderManagementReportPaginatedRefactoredOld")]
            public async Task<ProcessResult<OrderManagementReportPaginatedViewModel>> GetOrderManagementReportPaginatedRefactoredOld([FromBody] OrderManagementReportViewModel model)
            {
                Tuple<int, List<OrderManagementReportItem>> result = null;
                List<OrderManagementReportItem> page = new List<OrderManagementReportItem>();
                //if (model.AllProgress)
                //{
                //    result = GetReportQuery(model);
                //    page = await result.Item2.Skip(model.PageInfo.PageSize * (model.PageInfo.PageNo - 1))
                //        .Take(model.PageInfo.PageSize).ToListAsync();
                //}
                //else
                //{
                //    result = GetReportQuery(model, model.PageInfo.PageSize, model.PageInfo.PageNo - 1);
                //    page = result.Item2.ToList();
                //}
                result = GetReportQuery(model, model.PageInfo.PageSize, model.PageInfo.PageNo - 1);
                page = result.Item2.ToList();


                //Dictionary<int?, string> teamIdDefaultTechName = qawdwdq.ToDictionary(x => x.TeamId, x => x.User.FullName);
                var teamIdDefaultTechName = manager.GetDefaultTechniciansForOrders();

                string dummy = "";
                foreach (var item in page)
                {
                    if (item.TeamId != null)
                    {
                        teamIdDefaultTechName.TryGetValue(item.TeamId, out dummy);
                        item.Technician = dummy;
                    }
                }



                //Map To The View Models
                var output = new OrderManagementReportPaginatedViewModel()
                {
                    Count = result.Item1,
                    Result = page,
                };
                return ProcessResultHelper.Succedded(output, "OrderManagementReport");

            }


            [HttpPost]
            [Route("GetOrderManagementReportExportRefactoredOld")]
            [Authorize(Roles = StaticRoles.Supervisor)]
            public async Task<ProcessResult<string>> GetOrderManagementReportExportRefactoredOld([FromBody] OrderManagementReportViewModel model)
            {


                model.PageInfo = new PaginatedItemsViewModel<OrderViewModel>(pageNo: 1, pageSize: int.MaxValue);


                var teamIdDefaultTechName = manager.GetDefaultTechniciansForOrders();


                //Dictionary<int?, string> teamIdDefaultTechName = qawdwdq.ToDictionary(x => x.TeamId, x => x.User.FullName);




                //List<string> ordersWeared = new List<string>()
                //{
                //    "5101267624",
                //    "5101267629",
                //    "5101267630",
                //};
                var result = GetReportQuery(model, IsForExporing: true);
                var data = result.Item2.ToList();
                //if (ordersWeared.Count> 0)
                //{
                //    foreach (var ordr in data)
                //    {
                //        if (ordersWeared.Contains(ordr.OrderCode))
                //        {

                //        }
                //    }
                //}






                string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd  HH-mmtt ss-fff");
                string newFile = $"{ _hostingEnv.WebRootPath}\\ExcelSheet\\Order Management {dateTime}.csv";



                int ExcelSheetindex = newFile.IndexOf("\\ExcelSheet\\");


                string fileResult = "";


                //Write to excell file
                if (model.IncludeProgressData)
                {
                    var orderActionsListData = data.Select(x => new OrderManagementReportExportOutputViewModelWithProgreassColomns(x, teamIdDefaultTechName)).ToList();
                    fileResult = ListToExcelHelper.WriteObjectsToExcel(orderActionsListData, newFile);
                }
                else
                {
                    var orderActionsListData = data.Select(x => new OrderManagementReportExportOutputViewModel(x, teamIdDefaultTechName)).ToList();
                    fileResult = ListToExcelHelper.WriteObjectsToExcel(orderActionsListData, newFile);
                }




                if (!string.IsNullOrEmpty(fileResult))
                {
                    var url = new Uri(fileResult);
                    var absolute = url.AbsoluteUri.Substring(ExcelSheetindex);
                    return ProcessResultHelper.Succedded(absolute);
                }
                else
                {
                    return ProcessResultHelper.Failed(string.Empty, null, "Something went wrong while exporting order report");
                }

                ////Write to excell file
                //Tuple<IQueryable<OrderObject>, IQueryable<OrderAction>> tubleQuery = GetReportQuery(model);

                //if (model.IncludeProgressData)
                //{
                //    var orderActionsListData = mapper.Map<List<OrderAction>, List<OrderManagementReportItem>>(tubleQuery.Item2.ToList())
                //        .Select(x => new OrderManagementReportExportOutputViewModelWithProgreassColomns(x)).ToList();
                //    fileResult = ListToExcelHelper.WriteObjectsToExcel(orderActionsListData, newFile);
                //}
                //else
                //{
                //    var orderActionsListData = mapper.Map<List<OrderObject>, List<OrderManagementReportItem>>(tubleQuery.Item1.ToList())
                //        .Select(x => new OrderManagementReportExportOutputViewModel(x)).ToList();
                //    fileResult = ListToExcelHelper.WriteObjectsToExcel(orderActionsListData, newFile);
                //}

                //if (!string.IsNullOrEmpty(fileResult))
                //{
                //    var url = new Uri(fileResult);
                //    var absolute = url.AbsoluteUri.Substring(37);
                //    return ProcessResultHelper.Succedded(absolute);
                //}
                //else
                //{
                //    return ProcessResultHelper.Failed(string.Empty, null, "Something went wrong while exporting order report");
                //}
                return null;
            }





            #endregion




        }
     

        public class OrderReportResult
        {
            public IQueryable<OrderObject> OrdersQuery { get; set; }
            public IQueryable<OrderAction> OrderActionsQuery { get; set; }
            public int TotalCount { get; set; }
        }
     
}