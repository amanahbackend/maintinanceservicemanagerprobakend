﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class NotificationCenterController : BaseController<INotificationCenterManager, NotificationCenter, NotificationCenterViewModel>
    {
        public INotificationCenterManager manager;
        public IOrderManager Ordermanager;
        public IOrderActionManager actionManager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public NotificationCenterController(INotificationCenterManager _manager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IOrderManager _Ordermanager,
            IOrderActionManager _actionManager,
        IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            Ordermanager = _Ordermanager;
            actionManager = _actionManager;
        }

        [HttpPost]
        [Route("GetByRecieverIdAndIsRead")]
        public ProcessResultViewModel<PaginatedItemsViewModel<NotificationCenterViewModel>> GetByRecieverIdAndIsRead([FromBody]PaginatedFilterNotificationViewModel model)
        {
            var result = new List<NotificationCenterViewModel>();
            List<NotificationCenterViewModel> OrderedGroup = null;
            var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);
            var predicate = PredicateBuilder.True<NotificationCenter>();
            if (model.RecieverId != null)
            {
                predicate = predicate.And(r => r.RecieverId == model.RecieverId);
            }
            if (model.IsRead != null)
            {
                //predicate = predicate.And(r => r.IsRead == model.IsRead);
            }
            var NotificationDta = manager.GetAll(x => x.RecieverId == model.RecieverId);
            var paginatedRes = manager.GetAllPaginated(action, predicate, x => x.CreatedDate);
            result = NotificationDta.Data.Select(x => new NotificationCenterViewModel()
            {
                Id = x.Id,
                RecieverId = x.RecieverId,
                Title = x.Title,
                Body = x.Body,
                Data = x.Data,
                NotificationType = x.NotificationType,
                IsRead = x.IsRead,
                TeamId = x.TeamId ?? 0,//== null?null:x.TeamId,                
                IsActionTaken = x.IsActionTaken ?? false,
                CreatedDate = x.CreatedDate,
                CurrentUserId = x.CurrentUserId,
                UpdatedDate = x.UpdatedDate
            }).ToList();

            OrderedGroup = result.OrderByDescending(x => x.Id).ToList().Take(model.PageInfo.PageSize).ToList();
            paginatedRes.Data.Data = mapper.Map<List<NotificationCenterViewModel>, List<NotificationCenter>>(OrderedGroup);
            return processResultMapper.Map<PaginatedItems<NotificationCenter>, PaginatedItemsViewModel<NotificationCenterViewModel>>(paginatedRes);
        }

        [HttpGet]
        [Route("CountByRecieverId/{recieverId}")]
        public ProcessResultViewModel<int> CountByRecieverId([FromRoute]string recieverId)
        {
            if (recieverId != null)
            {
                var result = manager.GetAllQuerable().Data.Where(x => x.RecieverId == recieverId && x.IsRead == false).ToList().Count;

                return ProcessResultViewModelHelper.Succedded<int>(result);
            }
            return ProcessResultViewModelHelper.Failed<int>(0, "reciever Id is required");

        }
        [HttpPost]
        [Route("FilterByDate")]
        public ProcessResultViewModel<PaginatedItemsViewModel<NotificationCenterViewModel>> FilterByDate([FromBody]PaginatedFilterNotificationByDateViewModel model)
        {
            var predicate = PredicateBuilder.True<NotificationCenter>();
            var result = new List<NotificationCenterViewModel>();
            List<NotificationCenterViewModel> OrderedGroup = null;
            // var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);
            var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);
            if (string.IsNullOrEmpty(model.RecieverId))
                return new ProcessResultViewModel<PaginatedItemsViewModel<NotificationCenterViewModel>>()
                {
                    Status = new ProcessResultStatus()
                    {
                        Code = ProcessResultStatusCode.MissingArguments,
                        Message = "RecieverId cant be null"
                    }
                };
            // you must view notifications of the current user
            predicate = predicate.And(r => r.RecieverId == model.RecieverId);
            if (model.tecs != null)
            {
                if (model.tecs.Count() > 0)
                    predicate = predicate.And(r => model.tecs.Contains(r.CurrentUserId));
            }

            if (!string.IsNullOrEmpty(model.orderCode))
            {
                predicate = predicate.And(r => r.OrderCode == model.orderCode);
                //predicate = predicate.And(r => model.OrderId == r.Code);
            }

            //if (model.DateFrom != null && model.DateTo != null)
            //{
            //    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
            //}

            if (model.endDate != null)
            {
                predicate = predicate.And(x => x.CreatedDate <= model.endDate);
            }
            if (model.startDate != null)
            {
                predicate = predicate.And(x => x.CreatedDate >= model.startDate);
            }
            // var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);

            var paginatedRes = manager.GetAllPaginated(action, predicate, x => x.CreatedDate);
            result = paginatedRes.Data.Data.Select(x => new NotificationCenterViewModel()
            {
                Id = x.Id,
                RecieverId = x.RecieverId,
                Title = x.Title,
                Body = x.Body,
                Data = x.Data,
                NotificationType = x.NotificationType,
                IsRead = x.IsRead,
                TeamId = x.TeamId ?? 0,//== null?null:x.TeamId,                
                IsActionTaken = x.IsActionTaken ?? false,
                CreatedDate = x.CreatedDate,
                CurrentUserId = x.CurrentUserId,
                UpdatedDate = x.UpdatedDate
            }).ToList();
            OrderedGroup = result.OrderByDescending(x => x.Id).ToList();
            //paginatedRes.Data = new PaginatedItems<NotificationCenter>
            paginatedRes.Data.Data = mapper.Map<List<NotificationCenterViewModel>, List<NotificationCenter>>(OrderedGroup);

            return processResultMapper.Map<PaginatedItems<NotificationCenter>, PaginatedItemsViewModel<NotificationCenterViewModel>>(paginatedRes);
        }

        //getAllNotificationCenterByTechAndOrderCode
        //[HttpPost]
        //[Route("FilterByTechAndOrderCode")]
        //public ProcessResultViewModel<PaginatedItemsViewModel<NotificationCenterViewModel>> FilterByTechAndOrderCode([FromBody]PaginatedFilterNotificationByDateViewModel model)
        //{
        //    List<NotificationCenter> res = new List<NotificationCenter>();
        //    var action = mapper.Map<PaginatedItemsViewModel<NotificationCenterViewModel>, PaginatedItems<NotificationCenter>>(model.PageInfo);
        //    var predicate = PredicateBuilder.True<NotificationCenter>();
        //    var OrderActionPredict = PredicateBuilder.True<OrderAction>();

        //                if ( model.tecs.Count > 0 && model.startDate == null && model.endDate == null&&model.orderCode==null)
        //                {                
        //                    predicate = predicate.And(r => model.tecs.Contains(r.TeamId));               
        //                }
        //                if (!string.IsNullOrEmpty(model.orderCode)&& model.tecs.Count <= 0)
        //                {
        //                     predicate=predicate.And(r => r.Body.Contains(model.orderCode.ToLower()));
        //                }
        //               if (!string.IsNullOrEmpty(model.orderCode) && model.tecs.Count > 0)
        //               {
        //                  predicate = predicate.And(r => (r.Body.ToLower().Contains(model.orderCode.ToLower())) && (model.tecs.Contains(r.TeamId)));
        //               }

        //    var paginatedRes = manager.GetAllPaginated(action, predicate, x => x.CreatedDate);          
        //    return processResultMapper.Map<PaginatedItems<NotificationCenter>, PaginatedItemsViewModel<NotificationCenterViewModel>>(paginatedRes);
        //}

        [HttpGet]
        [Route("MarkAllAsRead/{recieverId}")]
        public ProcessResult<bool> MarkAllAsRead([FromRoute]string recieverId)
        {
            if (string.IsNullOrEmpty(recieverId))
            {
                return ProcessResultHelper.Failed<bool>(false, null, "reciever id is required");
            }
            var result = manager.MarkAllAsRead(recieverId);

            if (result.Data)
            {
                return ProcessResultHelper.Succedded<bool>(true);
            }
            else
            {
                return ProcessResultHelper.Failed<bool>(false, null, "something wrong happened when trying to mark all notifications as read");
            }
        }


        [HttpGet]
        [Route("ChangeReadFlag/{notificationId}")]
        public ProcessResultViewModel<bool> ChangeReadFlag([FromRoute]int notificationId)
        {
            NotificationCenter obj = manager.Get(notificationId).Data;
            obj.IsRead = !obj.IsRead;
            ProcessResult<bool> result = manager.Update(obj);
            return processResultMapper.Map<bool, bool>(result);
        }

        [HttpGet]
        [Route("ChangeActionFlag/{notificationId}")]
        public ProcessResultViewModel<bool> ChangeActionFlag([FromRoute]int notificationId)
        {
            NotificationCenter obj = manager.Get(notificationId).Data;
            obj.IsActionTaken = obj.IsRead = true;
            ProcessResult<bool> result = manager.Update(obj);
            return processResultMapper.Map<bool, bool>(result);
        }

    }
}
