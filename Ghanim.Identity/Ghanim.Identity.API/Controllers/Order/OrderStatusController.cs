﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OrderStatusController : BaseAuthforAdminController<IOrderStatusManager, OrderStatus, OrderStatusViewModel>
    {
        IServiceProvider _serviceprovider;
        IOrderStatusManager orderStatusManager;
        //IMapper mapper;
        IProcessResultMapper processResultMapper;
       

        public OrderStatusController(IServiceProvider serviceprovider, IOrderStatusManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<OrderStatusViewModel> cash) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper, cash)
        {
            orderStatusManager = _manger;
            //mapper = _mapper;
            _serviceprovider = serviceprovider;
            processResultMapper = _processResultMapper;

        }

        private ILang_OrderStatusManager Lang_OrderStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_OrderStatusManager>();
            }
        }
        //[HttpGet]
        //[Route("GetAttendanceStatesByLanguage")]
        //public ProcessResultViewModel<AttendanceStatesViewModel> GetAttendanceStatesByLanguage([FromQuery]int AttendanceStatesId, string Language_code)
        //{
        //    var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
        //    var Lang_AttendanceStatesRes = Lang_AttendanceStatesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
        //    var entityResult = manager.Get(AttendanceStatesId);
        //    entityResult.Data.Name = Lang_AttendanceStatesRes.Data.Name;
        //    var result = processResultMapper.Map<AttendanceStates, AttendanceStatesViewModel>(entityResult);
        //    return result;
        //}
        //{stdid:int?}
        ////////////////////////////////////////
        /// <summary>        
        /// Simple Get Method to return the object when passing the LanguageId In The Header.
        /// This Method For getting Localized Order Stratus For Mobile
        /// </summary>
        /// <param name="LanguageId">The foriegn key field In Lang-OrderStatus Table .</param>       
        /// <returns>object if Manager returned one, else returns Default Data With English Language.</returns>
        /// if LanguageId is null ,it will returns the default data
        //For Mobile
        [HttpGet]
        [Route("GetAllExcluded")]///{langCode?}
        public ProcessResult<List<OrderStatusViewModel>> GetAllExcluded()
        {
            //int? langCode=null
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //string languageIdValue = langCode.ToString();
            var actions = orderStatusManager.GetAllExclude(StaticAppSettings.ExcludedStatusIds);
            if (languageIdValue!=null && languageIdValue!="")
            {
                foreach (var item in actions.Data)
                {
                    var OrderStatusRes = Lang_OrderStatusmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderStatus_ID == item.Id);
                    if (OrderStatusRes != null && OrderStatusRes.Data!=null)
                    {
                        item.Name = OrderStatusRes.Data.Name;
                    }
                }
            }

            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<OrderStatus>, List<OrderStatusViewModel>>(actions.Data);
                return ProcessResultHelper.Succedded<List<OrderStatusViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderStatusViewModel>>(null, null, SharedResource.General_WrongWhileGet);
        }
        //For Web
        [HttpGet]
        [Route("GetAll")]///{langCode?}
        public override ProcessResultViewModel<List<OrderStatusViewModel>> Get()
        {
            //int? langCode=null
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //string languageIdValue = langCode.ToString();
            var actions = base.Get();
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderStatusRes = Lang_OrderStatusmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderStatus_ID == item.Id);
                    if (OrderStatusRes != null && OrderStatusRes.Data != null)
                    {
                        item.Name = OrderStatusRes.Data.Name;
                    }
                }
            }

            return actions;
        }

        ////logical delete
        [HttpDelete]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("LogicalDelete/{id}")]
        public override ProcessResultViewModel<bool> LogicalDelete([FromRoute]int id)
        {
            var result = manger.LogicalDeleteById(id);
            return processResultMapper.Map<bool, bool>(result);
        }


        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<OrderStatusViewModel> Post([FromBody] OrderStatusViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<OrderStatusViewModel>(null, "Can not add empty value", ProcessResultStatusCode.InvalidValue);

            var entity = manger.Get(x => x.Name.ToLower() == model.Name.ToLower() || x.Code == model.Code)?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<OrderStatusViewModel>(null, "Order Status Existed With That Name or Code", ProcessResultStatusCode.Dublicated);
            return base.Post(model);
        }


    }
}