﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

//using Ghanim.BLL.IManagers;
using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;

using CommonEnum;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
        [Authorize]
    [Route("api/[controller]")]
    public class Lang_WorkingTypeController : BaseAuthforAdminController<ILang_WorkingTypeManager, Lang_WorkingType, Lang_WorkingTypeViewModel>
    {
        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_WorkingTypeManager manager;
        IWorkingTypeManager workingTypemanager;
        IProcessResultMapper processResultMapper;
       

        //ILang_BuildingTypesManager Buildingmanager;
        public Lang_WorkingTypeController(IServiceProvider serviceprovider,ISupportedLanguageService _supportedLanguageService, ILang_WorkingTypeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IWorkingTypeManager _workingTypeManager) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
            _serviceprovider = serviceprovider;
            manager = _manager;
            workingTypemanager = _workingTypeManager;
            processResultMapper = _processResultMapper;
           
        }
        private IWorkingTypeManager WorkingTypemanager
        {
            get
            {
                return _serviceprovider.GetService<IWorkingTypeManager>();
            }
        }
        //private ISupportedLanguagesManager supportedLanguagesmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ISupportedLanguagesManager>();
        //    }
        //}

        [HttpGet]
        [Route("GetAllLanguagesByWorkingTypeId/{WorkingTypeId}")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguagesByWorkingTypeId([FromRoute]int WorkingTypeId)
        {
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var WorkingTypeRes = WorkingTypemanager.Get(WorkingTypeId);
            var entityResult = manager.GetAllLanguagesByWorkingTypeId(WorkingTypeId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }

            codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = WorkingTypeRes.Data.Category, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguages()
        {
            var SupportedLanguagesRes = await supportedLanguageService.GetAll();
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary;
            var WorkingTypeRes = WorkingTypemanager.GetAll();
            foreach (var WorkingType in WorkingTypeRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = manager.GetAllLanguagesByWorkingTypeId(WorkingType.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = WorkingType.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = WorkingType.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = WorkingType.Category, languagesDictionaries = languagesDictionary, Id = WorkingType.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }
        //new
        [HttpGet]
        [Route("GetAllGroupedWithBlocked2")]
        private async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllGroupedWithBlocked2()
        {
            List<CodeWithLanguagesViewModel> WorkTypeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            //try
            //{
                List<LanguagesDictionariesViewModel> languagesDictionary;
                var SupportedLanguagesRes = await supportedLanguageService.GetAllWithBlocked();
                //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
                var WorkingTypeRes = workingTypemanager.GetAllWithBlocked();
            var OrderedWorkingTypeRes = WorkingTypeRes.Data.OrderByDescending(w => w.Id).ToList();
              var Newresult = OrderedWorkingTypeRes.GroupBy(x => x.Category, (key, group) => new CodeWithLanguagesViewModel { StateName = key, WorkingTypes = group }).ToList();
            for (int i = 0; i < Newresult.Count; i++)
            {
                if (Newresult[i].StateName.ToLower().Contains("end"))
                {
                    foreach (var workingType in WorkingTypeRes.Data)
                    {
                        languagesDictionary = new List<LanguagesDictionariesViewModel>();
                        var entityResult = await GetAllLanguagesByWorkingTypeIdnew(workingType.Id);
                        foreach (var item in entityResult.Data)
                        {
                            //var SupportedLanguageRes = supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                            var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);

                            if (SupportedLanguageRes.Data.Name == "English")
                            {

                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = workingType.Name, Id = item.Id });

                                // languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                            }
                            else
                            {
                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });

                                //  languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                            }

                        }
                        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                        {
                            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                            {
                                if (SupportedLanguage.Name == "English")
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = workingType.Name });

                                }
                                else
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                                }
                            }
                        }
                        WorkTypeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = workingType.Category, languagesDictionaries = languagesDictionary, Id = workingType.Id, IsDeleted = workingType.IsDeleted });
                       // Newresult 
                    }
                    Newresult[i].TypeId = (int)TimeSheetType.Actual;
                    Newresult[i].StateId = (int)TechnicianState.End;
                    var result1 = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
                    return result1;
                }
                else if (Newresult[i].StateName.ToLower().Contains("work"))
                {
                    foreach (var workingType in WorkingTypeRes.Data)
                    {
                        languagesDictionary = new List<LanguagesDictionariesViewModel>();
                        // var entityResult = Buildingmanager.GetAllLanguagesByBuildingTypesId(workingType.Id);
                        // var res = await supportedLanguageService.GetAll();
                        //  var SupportedLanguagesRes1 = supportedLanguagesmanager.GetAll();//.Data.OrderBy(x => x.Id).ToList();                                
                        // List<SupportedLanguageService> Orderedresult = new List<SupportedLanguageService>();
                        //  var Orderedresult = res.Data.OrderBy(x => x.Id).ToList();
                        // List<Ghanim.DataManagement.API.ViewModels.SupportedLanguagesViewModel> mapped = new List<DataManagement.API.ViewModels.SupportedLanguagesViewModel>(); Mapper.Map<Ghanim.DataManagement.API.ViewModels.SupportedLanguagesViewModel>(Orderedresult);
                        //var mapped1 = mapper.Map<Ghanim.DataManagement.API.ViewModels.SupportedLanguagesViewModel>, <Ghanim.DataManagement.API.ViewModels.SupportedLanguagesViewModel > (Orderedresult);
                        //Ghanim.DataManagement.API.ViewModels.SupportedLanguagesViewModel MappedviewModel = Mapper.Map<List<Ghanim.DataManagement.API.ViewModels.SupportedLanguagesViewModel>, Ghanim.DataManagement.API.ViewModels.SupportedLanguagesViewModel>(Orderedresult);
                        var entityResult = await GetAllLanguagesByWorkingTypeIdnew(workingType.Id);
                        foreach (var item in entityResult.Data)
                        {
                            //var SupportedLanguageRes = supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                            var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);

                            if (SupportedLanguageRes.Data.Name == "English")
                            {

                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = workingType.Name, Id = item.Id });

                                // languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                            }
                            else
                            {
                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });

                                //  languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                            }

                        }
                        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                        {
                            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                            {
                                if (SupportedLanguage.Name == "English")
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = workingType.Name });

                                }
                                else
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                                }
                            }
                        }
                        WorkTypeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = workingType.Category, languagesDictionaries = languagesDictionary, Id = workingType.Id, IsDeleted = workingType.IsDeleted });

                    }
                    Newresult[i].TypeId = (int)TimeSheetType.Actual;
                    Newresult[i].StateId = (int)TechnicianState.Working;
                    var result2 = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
                    return result2;
                }
                else if (Newresult[i].StateName.ToLower().Contains("idle"))
                {
                    foreach (var workingType in WorkingTypeRes.Data)
                    {
                        languagesDictionary = new List<LanguagesDictionariesViewModel>();
                        var entityResult = manager.GetAllLanguagesByWorkingTypeId(workingType.Id);
                        foreach (var item in entityResult.Data)
                        {
                            //var SupportedLanguageRes = supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                            var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);

                            if (SupportedLanguageRes.Data.Name == "English")
                            {

                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = workingType.Name, Id = item.Id });

                                // languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                            }
                            else
                            {
                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });

                                //  languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                            }

                        }
                        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                        {
                            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                            {
                                if (SupportedLanguage.Name == "English")
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = workingType.Name });

                                }
                                else
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                                }
                            }
                        }
                        WorkTypeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = workingType.Category, languagesDictionaries = languagesDictionary, Id = workingType.Id, IsDeleted = workingType.IsDeleted });

                    }
                    Newresult[i].TypeId = (int)TimeSheetType.Idle;
                    Newresult[i].StateId = (int)TechnicianState.Idle;
                    var result3 = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
                    return result3;
                }
                else if (Newresult[i].StateName.ToLower().Contains("break"))
                {
                    foreach (var workingType in WorkingTypeRes.Data)
                    {
                        languagesDictionary = new List<LanguagesDictionariesViewModel>();
                        var entityResult = manager.GetAllLanguagesByWorkingTypeId(workingType.Id);
                        foreach (var item in entityResult.Data)
                        {
                            //var SupportedLanguageRes = supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                            var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);

                            if (SupportedLanguageRes.Data.Name == "English")
                            {

                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = workingType.Name, Id = item.Id });

                                // languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                            }
                            else
                            {
                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });

                                //  languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                            }

                        }
                        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                        {
                            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                            {
                                if (SupportedLanguage.Name == "English")
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = workingType.Name });

                                }
                                else
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                                }
                            }
                        }
                        WorkTypeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = workingType.Category, languagesDictionaries = languagesDictionary, Id = workingType.Id, IsDeleted= workingType.IsDeleted });

                    }
                    Newresult[i].TypeId = (int)TimeSheetType.Break;
                    Newresult[i].StateId = (int)TechnicianState.Break;
                    var result4 = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
                    return result4;
                }              
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            return result;
            // return ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);

            // return ProcessResultHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkingTypeRes.Data);
            //  }
            //else
            //{
            //    return ProcessResultViewModelHelper.Failed<List<CodeWithLanguagesViewModel>>(null, "There is somehing wrong while get all data");
            //}
            // }
            //catch (Exception ex)
            //{
            //    return ProcessResultViewModelHelper.Failed<List<CodeWithLanguagesViewModel>>(null, "There is somehing wrong while getting data in GetAllGrouped");
            //}
            //var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            // var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            // return result;

        }
        //
        [HttpGet]
        [Route("GetAllGroupedWithBlocked")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllGroupedWithBlocked()
        {
            var SupportedLanguagesRes = await supportedLanguageService.GetAllWithBlocked();
            List<CodeWithLanguagesViewModel> WorkTypeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
           
           // List<CodeWithLanguagesViewModel> nullWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderStatusRes = workingTypemanager.GetAllWithBlocked();
            var OrderedOrderStatusRes = OrderStatusRes.Data.OrderByDescending(s => s.Id).ToList();
            foreach (var OrderStatus in OrderedOrderStatusRes)
            {               
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = await GetAllLanguagesByWorkingTypeIdnew(OrderStatus.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes != null && (SupportedLanguageRes.Data?.Name ?? "English") == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = (SupportedLanguageRes.Data?.Name ?? "English"), Value = OrderStatus.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = OrderStatus.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                WorkTypeWithAllLanguages.Add(new CodeWithLanguagesViewModel { languagesDictionaries = languagesDictionary, Code = OrderStatus.Category, Id = OrderStatus.Id, IsDeleted = OrderStatus.IsDeleted });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);

           // var result = ProcessResultViewModelHelper.Succedded<List<NullWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            return result;
        }

        //
        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguagesWithBlocked()
        {
            List<CodeWithLanguagesViewModel> WorkTypeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            try
            {
                List<LanguagesDictionariesViewModel> languagesDictionary;
                //var SupportedLanguagesRes = await supportedLanguageService.GetAllWithBlocked();
                //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
                var WorkingTypeRes = workingTypemanager.GetAllWithBlocked();
                if (WorkingTypeRes.IsSucceeded)
                {
                    ///old
                    var Newresult = WorkingTypeRes.Data.GroupBy(x => x.Category, (key, group) => new CodeWithLanguagesViewModel { StateName = key, WorkingTypes = group }).ToList();
                    for (int i = 0; i < Newresult.Count; i++)
                    {

                        if (Newresult[i].StateName.ToLower().Contains("end"))
                        {
                            Newresult[i].TypeId = (int)TimeSheetType.Actual;
                            Newresult[i].StateId = (int)TechnicianState.End;
                        }
                        else if (Newresult[i].StateName.ToLower().Contains("work"))
                        {
                            Newresult[i].TypeId = (int)TimeSheetType.Actual;
                            Newresult[i].StateId = (int)TechnicianState.Working;
                        }
                        else if (Newresult[i].StateName.ToLower().Contains("idle"))
                        {
                            Newresult[i].TypeId = (int)TimeSheetType.Idle;
                            Newresult[i].StateId = (int)TechnicianState.Idle;
                        }
                        else if (Newresult[i].StateName.ToLower().Contains("break"))
                        {
                            Newresult[i].TypeId = (int)TimeSheetType.Break;
                            Newresult[i].StateId = (int)TechnicianState.Break;
                        }
                    }
                    ///
                    foreach (var workingType in WorkingTypeRes.Data)
                    {
                        languagesDictionary = new List<LanguagesDictionariesViewModel>();
                        var entityResult = manager.GetAllLanguagesByWorkingTypeId(workingType.Id);
                        foreach (var item in entityResult.Data)
                        {
                            //var SupportedLanguageRes = supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                            var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);

                            if (SupportedLanguageRes.Data.Name == "English")
                            {
                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = workingType.Name, Id = item.Id });

                                // languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                            }
                            else
                            {
                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });

                                //  languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                            }

                        }
                        //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                        //{
                        //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                        //    {
                        //        if (SupportedLanguage.Name == "English")
                        //        {
                        //            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = workingType.Name });

                        //        }
                        //        else
                        //        {
                        //            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        //        }
                        //    }
                        //}
                        WorkTypeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = workingType.Category, languagesDictionaries = languagesDictionary, Id = workingType.Id });

                    }

                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<List<CodeWithLanguagesViewModel>>(null, SharedResource.General_WrongWhileGet);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<CodeWithLanguagesViewModel>>(null,SharedResource.General_SomethingWrongHappen  );
            }
            //var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            return result;
        }

        //
        private async Task< ProcessResult<List<Lang_WorkingType>>> GetAllLanguagesByWorkingTypeIdnew(int WorkingTypeId)
        {
            List<Lang_WorkingType> input = null;
            try
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetAll();
                var SupportedLanguagesRes2 = SupportedLanguagesRes.Data.OrderBy(x => x.Id).ToList();
                input = manager.GetAllQuerable().Data.Where(x => x.FK_WorkingType_ID == WorkingTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes2.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_WorkingType { FK_WorkingType_ID = WorkingTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_WorkingType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByWorkingTypeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_WorkingType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByWorkingTypeId");
            }
        }
        //new 
        //[HttpGet]
        //[Route("GetAllLanguagesWithBlocked")]
        //public ProcessResultViewModel<List<WorkingTypesWithAllLanguagesViewModel>> GetAllLanguagesWithBlocked()
        //{
        //    List<WorkingTypesWithAllLanguagesViewModel> workingTypesWithAllLanguages = new List<WorkingTypesWithAllLanguagesViewModel>();
        //    List<LanguagesDictionaryViewModel> languagesDictionary;
        //    var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
        //    var WorkingTypeRes = manager.GetAllWithBlocked();
        //    foreach (var type in WorkingTypeRes.Data)
        //    {//GetAllLanguagesByAreaIdWithBlocked
        //        languagesDictionary = new List<LanguagesDictionaryViewModel>();
        //        var entityResult = manager.GetAllLanguagesByWorkingTypeId(type.Id);
        //        foreach (var item in entityResult.Data)
        //        {
        //            var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
        //            if (SupportedLanguageRes.Data.Name == "English")
        //            {
        //                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = type.Name, Id = item.Id });
        //            }
        //            else
        //            {
        //                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
        //            }

        //        }

        //        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
        //        {
        //            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
        //            {
        //                if (SupportedLanguage.Name == "English")
        //                {
        //                    languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = type.Name });

        //                }
        //                else
        //                {
        //                    languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
        //                }

        //            }
        //        }
        //        workingTypesWithAllLanguages.Add(new WorkingTypesWithAllLanguagesViewModel { languagesDictionaries = languagesDictionary, Name = type.Name, IsDeleted = type.IsDeleted });
        //    }

        //    var result = ProcessResultViewModelHelper.Succedded<List<WorkingTypesWithAllLanguagesViewModel>>(workingTypesWithAllLanguages);

        //    return result;
        //}

        [HttpGet]
        [Route("GetAllGroupedWithBlocked1")]
        private async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllGroupedWithBlocked1()
        {
            List<CodeWithLanguagesViewModel> WorkTypeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            //try
            //{
            List<LanguagesDictionariesViewModel> languagesDictionary;
            var SupportedLanguagesRes = await supportedLanguageService.GetAllWithBlocked();
            //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
            var WorkingTypeRes = workingTypemanager.GetAllWithBlocked();
            //if (WorkingTypeRes.IsSucceeded)
            //{
            ///old
            var Newresult = WorkingTypeRes.Data.GroupBy(x => x.Category, (key, group) => new CodeWithLanguagesViewModel { StateName = key, WorkingTypes = group }).ToList();
            for (int i = 0; i < Newresult.Count; i++)
            {
                //if (Newresult[i].StateName.ToLower().Contains("end"))
                //{
                //    foreach (var workingType in WorkingTypeRes.Data)
                //    {
                //        languagesDictionary = new List<LanguagesDictionariesViewModel>();
                //        var entityResult = manager.GetAllLanguagesByWorkingTypeId(workingType.Id);
                //        foreach (var item in entityResult.Data)
                //        {
                //            //var SupportedLanguageRes = supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                //            var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);

                //            if (SupportedLanguageRes.Data.Name == "English")
                //            {

                //                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = workingType.Name, Id = item.Id });

                //                // languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                //            }
                //            else
                //            {
                //                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });

                //                //  languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                //            }

                //        }
                //        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //        {
                //            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //            {
                //                if (SupportedLanguage.Name == "English")
                //                {
                //                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = workingType.Name });

                //                }
                //                else
                //                {
                //                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //                }
                //            }
                //        }
                //        Newresult.Add(new CodeWithLanguagesViewModel { Code = workingType.Category, languagesDictionaries = languagesDictionary, Id = workingType.Id });
                //        // Newresult 
                //    }
                //    Newresult[i].TypeId = (int)TimeSheetType.Actual;
                //    Newresult[i].StateId = (int)TechnicianState.End;
                //    var result1 = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(Newresult);
                //    return result1;
                //}
               
                 if (Newresult[i].StateName.ToLower().Contains("idle"))
                {
                    foreach (var workingType in WorkingTypeRes.Data)
                    {
                        languagesDictionary = new List<LanguagesDictionariesViewModel>();
                        var entityResult =await GetAllLanguagesByWorkingTypeIdnew(workingType.Id);
                        foreach (var item in entityResult.Data)
                        {
                            //var SupportedLanguageRes = supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                            var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);

                            if (SupportedLanguageRes.Data.Name == "English")
                            {

                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = workingType.Name, Id = item.Id });

                                // languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                            }
                            else
                            {
                                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });

                                //  languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                            }

                        }
                        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                        {
                            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                            {
                                if (SupportedLanguage.Name == "English")
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = workingType.Name });

                                }
                                else
                                {
                                    languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                                }
                            }
                        }
                        WorkTypeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = workingType.Category, languagesDictionaries = languagesDictionary, Id = workingType.Id });

                    }
                    Newresult[i].TypeId = (int)TimeSheetType.Idle;
                    Newresult[i].StateId = (int)TechnicianState.Idle;
                    var result3 = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
                    return result3;
                }
               
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(Newresult);
            return result;
            // return ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);

            // return ProcessResultHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkingTypeRes.Data);
            //  }
            //else
            //{
            //    return ProcessResultViewModelHelper.Failed<List<CodeWithLanguagesViewModel>>(null, "There is somehing wrong while get all data");
            //}
            // }
            //catch (Exception ex)
            //{
            //    return ProcessResultViewModelHelper.Failed<List<CodeWithLanguagesViewModel>>(null, "There is somehing wrong while getting data in GetAllGrouped");
            //}
            //var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            // var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(WorkTypeWithAllLanguages);
            // return result;

        }

        //
        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_WorkingType> lstModel)
        {
            var entityResult = manager.UpdateByWorkingType(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody]CodeWithLanguagesViewModel Model)
        {
           
            string enName = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;
            var checkNoDub = WorkingTypemanager.CheckWorkingTypeNoDoublication(enName,  Model.Id);
            if (!checkNoDub.IsSucceeded)
                return ProcessResultViewModelHelper.Failed<bool>(false, checkNoDub.Status.Message, ProcessResultStatusCode.InvalidValue);
            List<Lang_WorkingType> lstModel = new List<Lang_WorkingType>();
            var WorkingTypeRes = WorkingTypemanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_WorkingType { FK_WorkingType_ID = WorkingTypeRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = await UpdateByWorkingTypeNew(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
        /////Update///
        private async Task<ProcessResult<bool>> UpdateByWorkingTypeNew(List<Lang_WorkingType> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var workingTypeRes = workingTypemanager.Get(item.FK_WorkingType_ID);
                        if (workingTypeRes != null && workingTypeRes.Data != null)
                        {
                            workingTypeRes.Data.Name = item.Name;
                            var updateRes = workingTypemanager.Update(workingTypeRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangOrderStatus = await GetByWorkingTypeIdAndsupprotedLangIdNew(item.FK_WorkingType_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderStatus != null && prevLangOrderStatus.Data != null && prevLangOrderStatus.Data.Count == 0)
                        {
                            result = manager.Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = manager.Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderStatus");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderStatus");
            }
        }
        private async Task<ProcessResult<List<Lang_WorkingType>>> GetByWorkingTypeIdAndsupprotedLangIdNew(int WorkingTypeId, int supprotedLangId)
        {
            List<Lang_WorkingType> input = null;
            try
            {
                input = manager.GetAllQuerable().Data.Where(x => x.FK_WorkingType_ID == WorkingTypeId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_WorkingType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByWorkingTypeIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_WorkingType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByWorkingTypeIdAndsupprotedLangId");
            }
        }
    }
}