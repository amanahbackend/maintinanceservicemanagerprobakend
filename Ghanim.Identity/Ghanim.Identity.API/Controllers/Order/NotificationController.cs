﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using Ghanim.Models.Entities;

using Ghanim.BLL.IManagers;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Utilites.ProcessingResult;
using Ghanim.API.Models;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;

using FcmSharp;
using FcmSharp.Requests;
using FcmSharp.Responses;
using FcmSharp.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ghanim.API.Models;
using Ghanim.API.ServiceCommunications.Notification;
using Microsoft.AspNetCore.Authorization;
using Utilites.ProcessingResult;
using Notification = Ghanim.Models.Entities.Notification;

namespace Ghanim.API.Controllers
{
        [Authorize]
    [Route("api/[controller]")]
    public class NotificationController : BaseAuthforAdminController<INotificationManager, Notification, NotificationViewModel>
    {
        public INotificationManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private IConfigurationRoot _configuration;
        private INotificationService _notificationService;

        public NotificationController(INotificationService notificationService, IConfigurationRoot configuration, INotificationManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) :
            base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            _configuration = configuration;
            _notificationService = notificationService;
        }

        [HttpPost, Route("Send")]
        public async Task<IProcessResultViewModel<string>> Send([FromBody] SendNotificationViewModel model) =>
          await  _notificationService.Send(model);

        [HttpPost, Route("SendToMultiUsers")]
        public async Task<IProcessResultViewModel<string>> SendToMultiUsers([FromBody]List<SendNotificationViewModel> lstmodel)=>
            await _notificationService.SendToMultiUsers(lstmodel);

    }
}