﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OrderTypeController : BaseAuthforAdminController<IOrderTypeManager, OrderType, OrderTypeViewModel>
    {
        IServiceProvider _serviceprovider;
        IOrderTypeManager manager;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public OrderTypeController(IServiceProvider serviceprovider,IOrderTypeManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<OrderTypeViewModel> cash) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper, cash)
        {

            manager = _manger;
            //mapper = _mapper;
            _serviceprovider = serviceprovider;
            processResultMapper = _processResultMapper;
        }
        private ILang_OrderTypeManager Lang_OrderTypeManager
        {
            get
            {
                return _serviceprovider.GetService<ILang_OrderTypeManager>();
            }
        }

        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<OrderTypeViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //var actions = manager.GetAll();
            var actions = base.Get();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderTypeLocalizesRes = Lang_OrderTypeManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderType_ID == item.Id);
                    if (OrderTypeLocalizesRes != null && OrderTypeLocalizesRes.Data != null)
                    {
                        item.Name = OrderTypeLocalizesRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                //var result = mapper.Map<List<OrderProblem>, List<OrderProblemViewModel>>(actions.Data);
                //return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
                //return ProcessResultHelper.Succedded<List<AreasViewModel>>(result);
            }
            return actions;
        }
    }
}