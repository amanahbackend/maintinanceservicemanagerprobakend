﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using Ghanim.BLL.Managers;
using Ghanim.Models.Entities;

using AutoMapper;
using Dispatching.Location.API;
using Dispatching.Location.API.ServiceCommunications.Order;
using Dispatching.Location.API.ViewModels;
using Utilites.ProcessingResult;
using Ghanim.BLL.IManagers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Utilites.PACI;
using Utilities.Utilites.PACI;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PACIController : BaseAuthforAdminController<IPACIManager, PACI, PACIViewModel>
    {
        public IPACIManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;

        private IConfigurationRoot _configurationRoot;
        IPACIService _paciService;


        public PACIController(
            IPACIManager _manger,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,


            IConfigurationRoot configurationRoot,
            IServiceProvider serviceProvider,
        IPACIService paciService
            ) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            _paciService = paciService;
            //this is form location PASI COntroller fporm the miicro service 
            _configurationRoot = configurationRoot;
            PACIHelper.Intialization(
                PACIConfig.proxyUrl,
                PACIConfig.paciServiceUrl,
                PACIConfig.paciNumberFieldName,
                PACIConfig.blockServiceUrl,
                PACIConfig.blockNameFieldNameBlockService,
                PACIConfig.nhoodNameFieldName,
                PACIConfig.streetServiceUrl,
                PACIConfig.blockNameFieldNameStreetService,
                PACIConfig.streetNameFieldName);

        }

        [HttpGet]
        [Route("GetByPaciNumber")]
        public ProcessResult<PACIViewModel> GetByPaciNumber([FromQuery]string paciNumber)
        {
           return _paciService.GetPaciByNumber(paciNumber);
        }


        [HttpGet]
        [Route("GetLocationByPaci")]
        public async Task<IActionResult> GetLocationByPaci(string paciNumber)
        {
            LocationViewModel locationViewModel = new LocationViewModel();

            ProcessResult<PACIViewModel> storedDbPaci = GetByPaciNumber(paciNumber);
            var savedPaci = storedDbPaci.Data;

            if (savedPaci == null)
            {
                var paciData = PACIHelper.GetLocationByPACI(Convert.ToInt32(paciNumber));
                var round = 1;
                while (round < 3 && paciData?.Latitude == null && paciData?.Longtiude == null)
                {
                    paciData = PACIHelper.GetLocationByPACI(Convert.ToInt32(paciNumber));
                    round++;
                }
                if (paciData == null)
                    return NotFound(locationViewModel);


                await _paciService.Add(new PACIViewModel
                {
                    PACINumber = paciNumber,
                    AreaName = paciData.Area?.Name,
                    AreaId = int.TryParse(paciData.Area?.Id, out int c) ? int.Parse(paciData.Area?.Id) : 0,
                    GovernorateName = paciData.Governorate?.Name,
                    GovernorateId = int.TryParse(paciData.Governorate?.Id, out int d) ? int.Parse(paciData.Governorate?.Id) : 0,
                    BlockName = paciData.Block,
                    BlockId = 0,
                    StreetName = paciData.Street,
                    StreetId = 0,
                    Longtiude = (decimal)paciData.Longtiude,
                    Latitude = (decimal)paciData.Latitude,
                    FloorNumber = paciData.FloorNumber,
                    AppartementNumber = paciData.AppartementNumber,
                    BuildingNumber = paciData.BuildingNumber
                });
                locationViewModel = Mapper.Map<PACIHelper.PACIInfo, LocationViewModel>(paciData);
            }
            else
            {
                locationViewModel = new LocationViewModel
                {
                    Area = new LocationItemViewModel { Name = savedPaci.AreaName, Id = savedPaci.AreaId.ToString() },
                    Block = new LocationItemViewModel { Name = savedPaci.BlockName, Id = savedPaci.BlockId.ToString() },
                    Street = new LocationItemViewModel { Name = savedPaci.StreetName, Id = savedPaci.StreetId.ToString() },
                    Governorate = new LocationItemViewModel { Name = savedPaci.GovernorateName, Id = savedPaci.GovernorateId.ToString() },
                    Lat = savedPaci.Latitude,
                    Lng = savedPaci.Longtiude,
                    AppartementNumber = savedPaci.AppartementNumber,
                    FloorNumber = savedPaci.FloorNumber,
                    BuildingNumber = savedPaci.BuildingNumber
                };
            }

            //var result = PACIHelper.GetLocationByPACI(Convert.ToInt32(paciNumber));
            return Ok(locationViewModel);
        }


        [HttpGet]
        [Route("GetAllGovernorates")]
        public IActionResult GetAllGovernorates()
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:GovernorateService:ServiceUrl"];

            var result = PACIHelper.GetAllGovernorates(proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAreas")]
        public IActionResult GetAreas(int? govId)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:AreaService:ServiceUrl"];
            string govIdfieldName = _configurationRoot["Location:AreaService:GovernorateIdFieldName"];
            var result = PACIHelper.GetAreas(govId, govIdfieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetBlocks")]
        public IActionResult GetBlocks(int? areaId)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:BlockService:ServiceUrl"];
            string areaIdfieldName = _configurationRoot["Location:BlockService:AreaIdFieldName"];
            var result = PACIHelper.GetBlocks(areaId, areaIdfieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStreets")]
        public IActionResult GetStreets(int? govId, int? areaId, string blockName)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            string govIdFieldName = _configurationRoot["Location:StreetService:GovIdFieldName"];
            string areaIdFieldName = _configurationRoot["Location:StreetService:AreaIdFieldName"];
            string blockNameFieldName = _configurationRoot["Location:StreetService:BlockNameFieldName"];
            var result = PACIHelper.GetStreets(govId, govIdFieldName, areaId, areaIdFieldName,
                                                blockName, blockNameFieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet("GetStreetCoors")]
        public ProcessResultViewModel<Point> GetStreetCoors([FromQuery]string streetName, string blockName)
        {
            try
            {
                string proxyUrl = _configurationRoot["Location:ProxyUrl"];
                string serviceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
                string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
                string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];

                var result = PACIHelper.GetStreetPACItModel(streetName, blockName, proxyUrl,
                                            serviceUrl, streetFieldName, blockNameFieldNameStreetService);
                var point = new Point();
                if (result != null)
                {
                    point.Latitude = (double)result.features[0].attributes.CENTROID_Y;
                    point.Longitude = (double)result.features[0].attributes.CENTROID_X;
                }
                return ProcessResultViewModelHelper.Succedded(point);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<Point>(null, ex.Message);
            }
        }





    }
}