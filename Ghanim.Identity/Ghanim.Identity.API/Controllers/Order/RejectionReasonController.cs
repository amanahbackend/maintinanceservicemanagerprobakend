﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class RejectionReasonController : BaseAuthforAdminController<IRejectionReasonManager, RejectionReason, RejectionReasonViewModel>
    {
        IServiceProvider _serviceprovider;
        IRejectionReasonManager manager;
        IProcessResultMapper processResultMapper;
        public RejectionReasonController(IServiceProvider serviceprovider, IRejectionReasonManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_RejectionReasonManager Lang_RejectionReasonManager
        {
            get
            {
                return _serviceprovider.GetService<ILang_RejectionReasonManager>();
            }
        }

        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<RejectionReasonViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            var actions = manager.GetAll();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var RejectionReasonLocalizesRes = Lang_RejectionReasonManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_RejectionReason_ID == item.Id);
                    if (RejectionReasonLocalizesRes != null && RejectionReasonLocalizesRes.Data != null)
                    {
                        item.Name = RejectionReasonLocalizesRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                //var result = mapper.Map<List<OrderProblem>, List<OrderProblemViewModel>>(actions.Data);
                //return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
                //return ProcessResultHelper.Succedded<List<AreasViewModel>>(result);
            }
            return processResultMapper.Map<List<RejectionReason>, List<RejectionReasonViewModel>>(actions);
        }
    }
}