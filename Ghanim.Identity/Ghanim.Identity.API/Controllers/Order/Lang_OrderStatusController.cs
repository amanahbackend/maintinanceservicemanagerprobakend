﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

using Ghanim.BLL.IManagers;
using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
        [Authorize]
    [Route("api/[controller]")]
    public class Lang_OrderStatusController : BaseAuthforAdminController<ILang_OrderStatusManager, Lang_OrderStatus, Lang_OrderStatusViewModel>
    {
        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_OrderStatusManager manager;
        IProcessResultMapper processResultMapper;
        IOrderStatusManager OrderStatusManager;
        public Lang_OrderStatusController(IServiceProvider serviceprovider, ISupportedLanguageService _supportedLanguageService, ILang_OrderStatusManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IOrderStatusManager _orderStatusManager) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            OrderStatusManager = _orderStatusManager;
        }  
        private IOrderStatusManager OrderStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderStatusManager>();
            }
        }
        //private IOrderStatusManager orderStatusmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<IOrderStatusManager>();
        //    }
        //}
        //private ISupportedLanguagesManager supportedLanguagesmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ISupportedLanguagesManager>();
        //    }
        //}

        [HttpGet]
        [Route("GetAllLanguagesByOrderStatusId/{OrderStatusId}")]
        public async Task<ProcessResultViewModel<List<LanguagesDictionariesViewModel>>> GetAllLanguagesByOrderStatusId([FromRoute]int OrderStatusId)
        {
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderStatusRes = OrderStatusmanager.Get(OrderStatusId);
            var entityResult = manager.GetAllLanguagesByOrderStatusId(OrderStatusId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<LanguagesDictionariesViewModel>>(languagesDictionary);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public async Task<ProcessResultViewModel<List<NullWithLanguagesViewModel>>> GetAllLanguages()
        {
            var SupportedLanguagesRes = await supportedLanguageService.GetAll();

            List<NullWithLanguagesViewModel> nullWithAllLanguages = new List<NullWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderStatusRes = OrderStatusmanager.GetAll();
            foreach (var OrderStatus in OrderStatusRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = manager.GetAllLanguagesByOrderStatusId(OrderStatus.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes != null && (SupportedLanguageRes.Data?.Name ?? "English") == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = (SupportedLanguageRes.Data?.Name ?? "English"), Value = OrderStatus.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = OrderStatus.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                nullWithAllLanguages.Add(new NullWithLanguagesViewModel { languagesDictionaries = languagesDictionary,Id= OrderStatus.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<NullWithLanguagesViewModel>>(nullWithAllLanguages);
            return result;
        }
        //GetAllOrderStatusWithBlocked
        [HttpGet]
        [Route("GetAllOrderStatusWithBlocked")]
        public async Task< ProcessResultViewModel<List<NullWithLanguagesViewModel>>> GetAllOrderStatusWithBlocked()
        {
            //var SupportedLanguagesRes = await supportedLanguageService.GetAllWithBlocked();
            List<NullWithLanguagesViewModel> nullWithAllLanguages = new List<NullWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderStatusRes = OrderStatusmanager.GetAllWithBlocked();
            var OrderedOrderStatusRes = OrderStatusRes.Data.OrderByDescending(s => s.Id).ToList();
            foreach (var OrderStatus in OrderedOrderStatusRes)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = await GetAllLanguagesByOrderStatusIdNew(OrderStatus.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes != null && (SupportedLanguageRes.Data?.Name ?? "English") == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = (SupportedLanguageRes.Data?.Name ?? "English"), Value = OrderStatus.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //{
                //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //    {
                //        if (SupportedLanguage.Name == "English")
                //        {
                //            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = OrderStatus.Name });

                //        }
                //        else
                //        {
                //            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //        }
                //    }
                //}
                nullWithAllLanguages.Add(new NullWithLanguagesViewModel { languagesDictionaries = languagesDictionary, Id = OrderStatus.Id,IsDeleted= OrderStatus.IsDeleted,Code = OrderStatus.Code });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<NullWithLanguagesViewModel>>(nullWithAllLanguages);
            return result;
        }

        private async Task<ProcessResult<List<Lang_OrderStatus>>> GetAllLanguagesByOrderStatusIdNew(int OrderStatusId)
        {
            List<Lang_OrderStatus> input = null;
            try
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetAll();
                var OrderedSupportedLanguagesRes= SupportedLanguagesRes.Data.OrderBy(x => x.Id).ToList();
                input =manager.GetAllQuerable().Data.Where(x => x.FK_OrderStatus_ID == OrderStatusId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = OrderedSupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderStatus { FK_OrderStatus_ID = OrderStatusId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderStatus>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByOrderStatusId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderStatusId");
            }
        }
        
        //[HttpPost]
        //[Route("AddMulti")]
        //public override ProcessResultViewModel<List<Lang_OrderStatus>> PostMulti([FromBody] List<Lang_OrderStatus> lstModel)
        //{
        //    ProcessResultViewModel<List<Lang_OrderStatus>> result = null;
        //    List<Lang_OrderStatus> addedCostCenter = new List<Lang_OrderStatus>();
        //    foreach (var item in lstModel)
        //    {
        //        var costCenterIsExit = manager.costCenterIsExit(item.Name);
        //        if (costCenterIsExit != null && costCenterIsExit.Data)
        //        {
        //            result = ProcessResultViewModelHelper.Failed<List<CostCenterViewModel>>(null, "there is Cost Center with this name:" + item.Name);
        //        }
        //        else
        //        {
        //            addedCostCenter.Add(item);
        //        }

        //    }
        //    result = base.PostMulti(addedCostCenter);

        //    return result;
        //}
        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_OrderStatus> lstModel)
        {
            var entityResult = manager.UpdateByOrderStatus(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody]NullWithLanguagesViewModel Model)
        {
            string enName =Model.languagesDictionaries.FirstOrDefault(d => d.Key == "English").Value;
         
            var checkNoDub = OrderStatusmanager.CheckOrderStatusNoDoublication(enName, Model.Code, Model.Id);
            if (!checkNoDub.IsSucceeded)
                return ProcessResultViewModelHelper.Failed<bool>(false, checkNoDub.Status.Message, ProcessResultStatusCode.InvalidValue);
            List<Lang_OrderStatus> lstModel = new List<Lang_OrderStatus>();
            var OrderStatusRes = OrderStatusmanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_OrderStatus { FK_OrderStatus_ID = OrderStatusRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = await UpdateByOrderStatusNew(lstModel);
            OrderStatusRes.Data.Code = Model.Code;
            //OrderStatusmanager.Update(OrderStatusRes.Data);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
        
        //Add
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<Lang_OrderStatusViewModel>> PostMulti([FromBody]List<Lang_OrderStatusViewModel> lstmodel)
        {
            //FillCurrentUser(lstmodel);
            var entityModel = mapper.Map<List<Lang_OrderStatusViewModel>, List<Lang_OrderStatus>>(lstmodel);
            var entityResult = manger.Add(entityModel);
            return processResultMapper.Map<List<Lang_OrderStatus>, List<Lang_OrderStatusViewModel>>(entityResult);
        }
        //Add OrderStatus
        //[HttpPost]
        //[Route("Add")]
        //public override ProcessResultViewModel<OrderStatusViewModel> ad([FromBody]OrderStatusViewModel model)
        //{
        //    //FillCurrentUser(model);
        //    var entityModel = mapper.Map<OrderStatusViewModel, OrderStatus>(model);
        //    var entityResult = OrderStatusManager.Add(entityModel);
        //    return processResultMapper.Map<OrderStatus, OrderStatusViewModel>(entityResult);
        //}
        /////Update///
        private async Task< ProcessResult<bool>> UpdateByOrderStatusNew(List<Lang_OrderStatus> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes =await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var orderstatusRes = OrderStatusmanager.Get(item.FK_OrderStatus_ID);
                        if (orderstatusRes != null && orderstatusRes.Data != null)
                        {
                            orderstatusRes.Data.Name = item.Name; 
                            var updateRes = OrderStatusmanager.Update(orderstatusRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangOrderStatus =await GetByOrderStatusIdAndsupprotedLangIdNew(item.FK_OrderStatus_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderStatus != null && prevLangOrderStatus.Data != null && prevLangOrderStatus.Data.Count == 0)
                        {
                            result =manager.Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                       if(supportedLanguageRes.Data.Name == "English" && !OrderStatusmanager.CheckCanUpdateOrBlock(item.FK_OrderStatus_ID)) 
                        { }
                       else
                        result = manager.Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderStatus");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderStatus");
            }
        }

        private async Task< ProcessResult<List<Lang_OrderStatus>>> GetByOrderStatusIdAndsupprotedLangIdNew(int OrderStatusId, int supprotedLangId)
        {
            List<Lang_OrderStatus> input = null;
            try
            {
                input =manager.GetAllQuerable().Data.Where(x => x.FK_OrderStatus_ID == OrderStatusId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderStatus>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByOrderStatusIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByOrderStatusIdAndsupprotedLangId");
            }
        }
        
        //[HttpPut]
        //[Route("LogicalDelete/{id}")]
        //public override ProcessResult<bool> LogicalDelete([FromRoute]int id)
        //{
        //    var user = manger.Get(id);
        //    if (user.Data == null)
        //    {
        //        return ProcessResultHelper.Failed<bool>(false, null, "OrderStatus not found");
        //    }

        //    //var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
        //    //if (IdentityBlock.Result.Data)
        //    //{
        //        var result = manger.LogicalDeleteById(id);
        //        return result;
        //   // }

        //    return ProcessResultHelper.Failed<bool>(false, null, "Something went wrong when trying to block user");
        //}
        //public virtual ProcessResult<bool> LogicalDelete(List<Lang_OrderStatus> entitylst)
        //{
        //    bool data = false;
        //    try
        //    {
        //        if (entitylst != null && entitylst.Count > 0)
        //        {
        //            foreach (var entity in entitylst)
        //            {
        //                var deleteResult = LogicalDelete(entity);
        //                if (!deleteResult.IsSucceeded)
        //                {
        //                    return deleteResult;
        //                }
        //            }
        //            data = true;
        //        }
        //        else
        //        {
        //            data = true;
        //        }
        //        return ProcessResultHelper.Succedded(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessResultHelper.Failed(data, ex);
        //    }
        //}
    }
}