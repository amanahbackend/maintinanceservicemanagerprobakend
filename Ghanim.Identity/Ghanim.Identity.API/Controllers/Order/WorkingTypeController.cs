﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnum;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

using Ghanim.BLL.IManagers;

using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.Managers;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class WorkingTypeController : BaseAuthforAdminController<IWorkingTypeManager, WorkingType, WorkingTypeMiniViewModel>
    {
        IServiceProvider _serviceprovider;
        IProcessResultMapper processResultMapper;
        IWorkingTypeManager manager;
        ILang_WorkingTypeManager lang_WorkingTypeManager;
        //IProcessResultMapper processResultMapper;       
        ISupportedLanguageService supportedLanguageService;
        public WorkingTypeController(IServiceProvider serviceprovider, IWorkingTypeManager _manager, ILang_WorkingTypeManager _lang_WorkingTypeManager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ISupportedLanguageService _supportedLanguageService, ICached<WorkingTypeMiniViewModel> cash) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper, cash)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
           // processResultMapper = _processResultMapper;
            supportedLanguageService = _supportedLanguageService;
            lang_WorkingTypeManager = _lang_WorkingTypeManager;
        }
        //private ILang_WorkingTypeManager Lang_workingTypeManager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ILang_OrderSubStatusManager>();
        //    }
        //}
        //private ISupportedLanguagesManager supportedLanguagesmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ISupportedLanguagesManager>();
        //    }
        //}

        [HttpGet]
        [Route("GetAllGrouped1")]
        private async Task<ProcessResult<List<WorkingTypeGroupViewModel>>> GetAllGrouped1()
        {
            try
            {
                var workingTypes = manager.GetAll();
                if (workingTypes.IsSucceeded)
                {
                    var result = workingTypes.Data.GroupBy(x => x.Category, (key, group) => new WorkingTypeGroupViewModel { StateName = key, WorkingTypes = group }).ToList();
                    for (int i = 0; i < result.Count; i++)
                    {

                        if (result[i].StateName.ToLower().Contains("end"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Actual;
                            result[i].StateId = (int)TechnicianState.End;
                        }
                        else if (result[i].StateName.ToLower().Contains("work"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Actual;
                            result[i].StateId = (int)TechnicianState.Working;
                        }
                        else if (result[i].StateName.ToLower().Contains("idle"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Idle;
                            result[i].StateId = (int)TechnicianState.Idle;
                        }
                        else if (result[i].StateName.ToLower().Contains("break"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Break;
                            result[i].StateId = (int)TechnicianState.Break;
                        }
                    }
                    return ProcessResultHelper.Succedded<List<WorkingTypeGroupViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, null, SharedResource.General_WrongWhileGet);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, ex, SharedResource.General_WrongWhileGet);
            }
        }
        //Language
        [HttpGet]
        [Route("GetAllGrouped")]//[FromQuery] int id
        public ProcessResult<List<WorkingTypeGroupViewModel>> GetAllGrouped()
        {
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "6";
            //string languageIdValue = langCode.ToString();
            var actions = manager.GetAll();
            if (actions.IsSucceeded)
            {
                var result = actions.Data.GroupBy(x => x.Category, (key, group) => new WorkingTypeGroupViewModel { StateName = key, WorkingTypes = group }).ToList();
                for (int i = 0; i < result.Count; i++)
                {
                    if (languageIdValue != null && languageIdValue != "")
                    {
                        foreach (var item in actions.Data)
                        {
                            var OrderSubStatusRes = lang_WorkingTypeManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_WorkingType_ID == item.Id);
                            if (OrderSubStatusRes != null && OrderSubStatusRes.Data != null)
                            {
                                item.Name = OrderSubStatusRes.Data.Name;
                                if (result[i].StateName.ToLower().Contains("end"))
                                {
                                    result[i].TypeId = (int)TimeSheetType.Actual;
                                    result[i].StateId = (int)TechnicianState.End;
                                }
                                else if (result[i].StateName.ToLower().Contains("work"))
                                {
                                    result[i].TypeId = (int)TimeSheetType.Actual;
                                    result[i].StateId = (int)TechnicianState.Working;
                                }
                                else if (result[i].StateName.ToLower().Contains("idle"))
                                {
                                    result[i].TypeId = (int)TimeSheetType.Idle;
                                    result[i].StateId = (int)TechnicianState.Idle;
                                }
                                else if (result[i].StateName.ToLower().Contains("break"))
                                {
                                    result[i].TypeId = (int)TimeSheetType.Break;
                                    result[i].StateId = (int)TechnicianState.Break;
                                }
                            }
                        }
                    }
                    // var OrderSubStatusRes = lang_WorkingTypeManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_WorkingType_ID == result[i].);

                }
                return ProcessResultHelper.Succedded<List<WorkingTypeGroupViewModel>>(result);
            }
            else
            {
                return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, null, SharedResource.General_WrongWhileGet);
            }
            ////if (actions.IsSucceeded && actions.Data.Count > 0)
            ////{
            ////   // var result = mapper.Map<List<WorkingType>, List<WorkingTypeViewModel>>(actions.Data);
            ////    return ProcessResultHelper.Succedded<List<WorkingTypeGroupViewModel>>(result);
            ////}
            ////return actions;
            //return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, null, "something wrong while getting data from get all excluded");

        }

        [HttpGet]
        [Route("GetAllGroupedWithBlocked")]
        public async Task<ProcessResult<List<WorkingTypeGroupViewModel>>> GetAllGroupedWithBlocked()
        {
            try
            {
                var workingTypes = manager.GetAllWithBlocked();
                if (workingTypes.IsSucceeded)
                {
                    var result = workingTypes.Data.GroupBy(x => x.Category, (key, group) => new WorkingTypeGroupViewModel { StateName = key, WorkingTypes = group }).ToList();
                    for (int i = 0; i < result.Count; i++)
                    {

                        if (result[i].StateName.ToLower().Contains("end"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Actual;
                            result[i].StateId = (int)TechnicianState.End;
                        }
                        else if (result[i].StateName.ToLower().Contains("work"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Actual;
                            result[i].StateId = (int)TechnicianState.Working;
                        }
                        else if (result[i].StateName.ToLower().Contains("idle"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Idle;
                            result[i].StateId = (int)TechnicianState.Idle;
                        }
                        else if (result[i].StateName.ToLower().Contains("break"))
                        {
                            result[i].TypeId = (int)TimeSheetType.Break;
                            result[i].StateId = (int)TechnicianState.Break;
                        }
                    }
                    return ProcessResultHelper.Succedded<List<WorkingTypeGroupViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, null, SharedResource.General_WrongWhileGet);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<WorkingTypeGroupViewModel>>(null, ex, SharedResource.General_WrongWhileGet);
            }
        }
        ////new
        //[HttpGet]
        //[Route("GetAllLanguagesWithBlocked")]
        //public ProcessResultViewModel<List<WorkingTypesWithAllLanguagesViewModel>> GetAllLanguagesWithBlocked()
        //{
        //    List<WorkingTypesWithAllLanguagesViewModel> workingTypesWithAllLanguages = new List<WorkingTypesWithAllLanguagesViewModel>();
        //    List<LanguagesDictionaryViewModel> languagesDictionary;
        //    var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
        //    var WorkingTypeRes = manager.GetAllWithBlocked();
        //    foreach (var type in WorkingTypeRes.Data)
        //    {//GetAllLanguagesByAreaIdWithBlocked
        //        languagesDictionary = new List<LanguagesDictionaryViewModel>();
        //        var entityResult = manager.GetAllLanguagesByWorkingTypeId(type.Id);
        //        foreach (var item in entityResult.Data)
        //        {
        //            var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
        //            if (SupportedLanguageRes.Data.Name == "English")
        //            {
        //                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = type.Name, Id = item.Id });
        //            }
        //            else
        //            {
        //                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
        //            }

        //        }

        //        if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
        //        {
        //            foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
        //            {
        //                if (SupportedLanguage.Name == "English")
        //                {
        //                    languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = type.Name });

        //                }
        //                else
        //                {
        //                    languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
        //                }

        //            }
        //        }
        //        workingTypesWithAllLanguages.Add(new WorkingTypesWithAllLanguagesViewModel { languagesDictionaries = languagesDictionary, Name = type.Name, IsDeleted = type.IsDeleted });
        //    }

        //    var result = ProcessResultViewModelHelper.Succedded<List<WorkingTypesWithAllLanguagesViewModel>>(workingTypesWithAllLanguages);

        //    return result;
        //}


        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<WorkingTypeMiniViewModel> Post([FromBody] WorkingTypeMiniViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<WorkingTypeMiniViewModel>(null, "Can not add empty value", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower() && x.CategoryCode == model.CategoryCode)?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<WorkingTypeMiniViewModel>(null, "Name Already Exist", ProcessResultStatusCode.Dublicated);
            return base.Post(model);
        }




    }
}