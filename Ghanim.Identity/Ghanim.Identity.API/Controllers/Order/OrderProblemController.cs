﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.API.ServiceCommunications.DataManagementService.Problem;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OrderProblemController : BaseAuthforAdminController<IOrderProblemManager, OrderProblem, OrderProblemViewModel>
    {
        IServiceProvider _serviceprovider;
        IOrderProblemManager manager;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IProblemService _problemService;

        public OrderProblemController(IProblemService problemService, IServiceProvider serviceprovider, IOrderProblemManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<OrderProblemViewModel> cash) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper, cash)
        {
            manager = _manger;
            //mapper = _mapper;
            _serviceprovider = serviceprovider;
            processResultMapper = _processResultMapper;
            _problemService = problemService;

        }
        private ILang_OrderProblemManager Lang_OrderProblemManager
        {
            get
            {
                return _serviceprovider.GetService<ILang_OrderProblemManager>();
            }
        }

        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<OrderProblemViewModel>> Get()
            => _problemService.GetProblems().Result;

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<OrderProblemViewModel> Post([FromBody] OrderProblemViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<OrderProblemViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<OrderProblemViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] OrderProblemViewModel model)
        {
            var entity = manager.Get(x => x.Code.ToLower() == model.Code.ToLower() || x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
