﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;
using CommonEnum;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using CommonEnums;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Utilities.Utilites.GenericListToExcel;
using Ghanim.BLL.ExcelSettings;
using System.IO;
using Utilites;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using System.Text.RegularExpressions;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.Models.Context;
using Ghanim.Models.Identity.Static;
using Ghanim.Models.Order.Enums;
using Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OrderActionController : BaseController<IOrderActionManager, OrderAction, OrderActionViewModel>
    {
        public IOrderActionManager manager;
        public readonly new IMapper mapper;
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnv;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IServiceProvider _serviceprovider;
        ITeamMembersService teamMembersService;
        ITechnicianStateService technicianStateService;
        ITechnicianService technicianService;
        IIdentityService identityService;
        IDispatcherService dispatcherService;

        public OrderActionController(
            ApplicationDbContext context,
            IServiceProvider serviceprovider, IHostingEnvironment hostingEnv, IOrderActionManager _manager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            ITechnicianStateService _technicianStateService,
            ITechnicianService _technicianService,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityService _identityService,
            IDispatcherService _dispatcherService,
            ITeamMembersService _teamMembersService) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _context = context;
            _serviceprovider = serviceprovider;
            _hostingEnv = hostingEnv;
            manager = _manager;
            mapper = _mapper;
            technicianStateService = _technicianStateService;
            technicianService = _technicianService;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            teamMembersService = _teamMembersService;
            identityService = _identityService;
            dispatcherService = _dispatcherService;
        }
        private IOrderManager orderManager
        {
            get
            {
                return _serviceprovider.GetService<IOrderManager>();
            }
        }
        [HttpPost]
        [Route("GetOrderProgress")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        public async Task<ProcessResultViewModel<PaginatedItemsViewModel<OrderActionViewModel>>> GetOrderProgress([FromBody]PaginatedFilterOrderActionViewModel model)
        {
            var action = mapper.Map<PaginatedItemsViewModel<OrderActionViewModel>, PaginatedItems<OrderAction>>(model.PageInfo);
            Expression<Func<OrderAction, bool>> predicateFilter = PredicateBuilder.True<OrderAction>();
            if (model.Filters != null)
            {
                predicateFilter = CreateFilterPredicate(model.Filters);
            }

            predicateFilter = predicateFilter.And(x => x.PlatformTypeSource != PlatformType.Ftp);



            var paginatedActions = manager.GetAllPaginated(action, predicateFilter);
            //var dispatchers = await dispatcherService.GetDispatchersByIds(paginatedActions.Data.Data.Where(y => y.CreatedUserId != 0 && y.CreatedUserId != null).Select(x => (int)x.CreatedUserId).ToList());
            //var techs = await technicianService.GetTechniciansByIds(paginatedActions.Data.Data.Where(y => y.CreatedUserId != 0 && y.CreatedUserId != null).Select(x => (int)x.CreatedUserId).ToList());
            //var techUserIds = new List<string>();
            //var dispUserIds = new List<string>();

            //if (techs.Data != null)
            //{
            //    techUserIds = techs.Data?.Select(y => y.UserId)?.ToList();
            //}

            //if (dispatchers.Data != null)
            //{
            //    dispUserIds = dispatchers.Data?.Select(x => x.UserId)?.ToList();
            //}

            //var userIds = dispUserIds.Concat(techUserIds).ToList();

            var identity = await identityService.GetByUserNames(paginatedActions.Data.Data.Where(y => !string.IsNullOrEmpty(y.CreatedUser)).Select(x => x.CreatedUser).ToList());

            foreach (var item in paginatedActions.Data.Data)
            {
                item.CreatedUser = identity.Data.FirstOrDefault(x => x.UserName == item.CreatedUser)?.FullName;
            }

            var paginatedActionsOrdered = paginatedActions;
            paginatedActionsOrdered.Data.Data = paginatedActions.Data.Data.GroupBy(x => x.ActionTime).Select(x => x.First()).OrderByDescending(x => x.ActionDate).ToList();
            return processResultMapper.Map<PaginatedItems<OrderAction>, PaginatedItemsViewModel<OrderActionViewModel>>(paginatedActionsOrdered);
        }
        //ceated by :  shaymaa
        //Date of ceation: 1/11/2020
        //last modified date: 11/2/2020
        //Last modified By : shaymaa
        [HttpGet]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        [Route("GetOrderProgress/{orderId}")]
        public async Task<ProcessResult<List<OrderActionViewModel>>> GetOrderProgressByOrderId([FromRoute]int orderId)
        {
            var result = new List<OrderActionViewModel>();
            Expression<Func<OrderAction, bool>> predicateFilter = PredicateBuilder.True<OrderAction>();
            predicateFilter = predicateFilter.And(x => x.PlatformTypeSource != PlatformType.Ftp);
            predicateFilter = predicateFilter.And(x => x.OrderId == orderId);

            var actions = manager.GetAll(predicateFilter, x => x.ExcuterUser, x => x.Status);

            if (actions.IsSucceeded)
            {
                var Executer = "";
                //var identity = await identityService.GetByUserNames(actions.Data.Where(y => !string.IsNullOrEmpty(y.CreatedUserId.ToString())).Select(x => x.CreatedUserId.ToString()).ToList());
                //foreach (var item in actions.Data)
                //{
                //    //item.CreatedUser = identity.Data.FirstOrDefault(x => x.UserName == item.CreatedUser)?.FullName;
                //    item.CreatedUser = identity.Data.FirstOrDefault(x => x.UserName == item.CreatedUserId.ToString())?.FullName;
                //    Executer = item.CreatedUser;
                //}
                // var ordered = actions.Data.GroupBy(x => x.ActionTime).Select(x => x.First()).OrderByDescending(x => x.ActionDate).ToList();               
                //GroupBy(p => p.PersonId, p => p.car,(key, g) => new {PersonId = key, Cars = g.ToList()
                //.GroupBy(p => new {p.Id, p.Name}, p => p, (key, g) => new { PersonId = key.Id, PersonName = key.Name, PersonCount = g.Count()})
                var ordered = actions.Data.GroupBy(x => x.ActionTime).Select(x => x.First()).OrderByDescending(x => x.Id).ToList();
                //var ordered = actions.Data.GroupBy(p => new { p.OrderId, p.StatusId }, p => p, (key, g) => new { OrderId = key.OrderId, StatusId = key.StatusId });
                //var selected = ordered.Count();
                var result1 = actions.Data.GroupBy(x => new { x.ActionDate, x.OrderId, x.StatusId }).Select(cl => new OrderActionViewModel()
                {
                    ActionDate = (DateTime)cl.FirstOrDefault()?.ActionDate,
                    CreatedUser = cl.FirstOrDefault()?.ExcuterUser.FullName,
                    ActionTime = cl.FirstOrDefault()?.ActionTime,
                    CreatedDate = (DateTime)cl.FirstOrDefault()?.CreatedDate,
                    CurrentUserId = cl.FirstOrDefault()?.CurrentUserId,
                    OrderId = cl.FirstOrDefault()?.OrderId,
                    ActionTypeId = cl.FirstOrDefault()?.ActionTypeId,
                    ActionTypeName = cl.FirstOrDefault()?.ActionTypeName,
                    SubStatusId = cl.FirstOrDefault()?.SubStatusId,
                    SubStatusName = cl.FirstOrDefault()?.SubStatus?.Name,
                    StatusId = cl.FirstOrDefault()?.StatusId,
                    StatusName = cl.FirstOrDefault()?.Status?.Name,
                    ServeyReport = cl.FirstOrDefault()?.ServeyReport,
                    CreatedUserId = cl.FirstOrDefault()?.CreatedUserId,
                    Reason = cl.FirstOrDefault()?.Reason,
                    Id = (int)cl.FirstOrDefault()?.Id,
                    WorkingTypeId = cl.FirstOrDefault().WorkingTypeId

                });

                List<OrderActionViewModel> OrderedGroup = null;
                try
                {

                    var list = result1.Where(x => x.ActionTypeName != "Work" && x.WorkingTypeId != 8).ToList();
                    OrderedGroup = list.OrderByDescending(x => x.Id).ToList();
                }
                catch (Exception e)
                {
                }

                //var newGroup = actions.Data.GroupBy(x => new { x.OrderId, x.StatusId }, (key, group) => new OrderActionViewModel
                //{
                //    OrderId = key.OrderId.Value,
                //    StatusId = key.StatusId.Value

                //}).ToList();
                return ProcessResultHelper.Succedded<List<OrderActionViewModel>>(OrderedGroup);
                //result = mapper.Map<List<OrderAction>, List<OrderActionViewModel>>(OrderedGroup);
                //return ProcessResultHelper.Succedded<List<OrderActionViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderActionViewModel>>(result, actions.Exception, SharedResource.General_WrongWhileGet);
        }

        [HttpGet]
        [Route("GetTeamMode/{teamId}")]
        public async Task<ProcessResult<TeamModeViewModel>> GetTeamModeAsync([FromRoute] int? teamId)
        {
            var result = new TeamModeViewModel();
            var ordersCountForInputTeam = _context.Orders.Count(x => x.TeamId == teamId &&
                          x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);

            // = _context.Orders.Where(x => x.TeamId == teamId&& !(StaticAppSettings.TravellingStatusId.Contains(x.StatusId))).Count();kkkkkk
            var actions = manager.GetAll(x => x.TeamId == teamId && (x.ActionTypeId != (int)OrderActionType.Assign && x.ActionTypeId != (int)OrderActionType.UnAssgin));
            if (actions.IsSucceeded)
            {
                if (actions.Data != null)
                {
                    if (actions.Data.Count() > 0)
                    {
                        var lastAction = mapper.Map<OrderAction, OrderActionViewModel>(actions.Data.OrderBy(c => c.Id).LastOrDefault());
                        if (lastAction.WorkingTypeId == (int)TimeSheetType.Actual && lastAction.OrderId == null)
                        {
                            result.Mode = "Stopped";
                            result.ModeId = lastAction.WorkingSubReasonId;
                        }
                        else if (lastAction.StatusId == (int)OrderStatusEnum.Started)
                        {
                            result.Mode = "Working";
                            result.ModeId = 1;
                            //result.ModeId = lastAction.WorkingSubReasonId;
                        }
                        else if (lastAction.StatusId == (int)OrderStatusEnum.Reached || lastAction.StatusId == (int)OrderStatusEnum.On_Travel)
                        {
                            result.Mode = "Travelling";
                            result.ModeId = 1;
                            //result.ModeId = lastAction.WorkingSubReasonId;
                        }
                        
                        else
                        {
                            if (lastAction.WorkingTypeId == (int)TimeSheetType.Idle)
                            {
                                if (ordersCountForInputTeam > 0)
                                {
                                    result.Mode = $"{ lastAction.WorkingTypeName} - { StaticAppSettings.IdleHandling[2].Name}";
                                    result.ModeId = StaticAppSettings.IdleHandling[2].Id;
                                }
                                else
                                {
                                    result.Mode = $"{ lastAction.WorkingTypeName} - { StaticAppSettings.IdleHandling[0].Name}";
                                    result.ModeId = StaticAppSettings.IdleHandling[0].Id;
                                }
                            }
                            else
                            {
                                result.Mode = $"{lastAction.WorkingTypeName} - {lastAction.WorkingSubReason}";
                                result.ModeId = lastAction.WorkingSubReasonId;
                            }
                        }
                        return ProcessResultHelper.Succedded<TeamModeViewModel>(result);
                    }
                }
                return ProcessResultHelper.Succedded<TeamModeViewModel>(result);
            }
            return ProcessResultHelper.Failed<TeamModeViewModel>(result, actions.Exception, SharedResource.General_WrongWhileGet);
        }

        [HttpGet]
        [Route("ChangeTeamMode/{teamId}/{isHasOrder}")]
        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode([FromRoute]int? teamId, [FromRoute] bool isHasOrder)
        {
            List<TeamMember> membersOfTeam = null;
            var orderTeam =
            _context.Members.Include(x => x.User).ThenInclude(x => x.Technician)
                   .Where(x => x.TeamId == teamId && (x.MemberType == MemberType.Technician || x.MemberType == MemberType.Driver)).ToList();
            membersOfTeam = orderTeam;
            //var members = orderTeam.Members;
            var actionDate = DateTime.Now;
            var actionDay = DateTime.Now.Date;
            //var members = await teamMembersService.GetMemberUsersByTeamId(teamId);
            //member.User.Technician.CostCenterId
            List<OrderAction> actionList = new List<OrderAction>();
            OrderAction model = new OrderAction();
            if (membersOfTeam != null)
            {
                foreach (var member in membersOfTeam)
                {
                    model = new OrderAction()
                    {
                        ActionDate = actionDate,
                        ActionDay = actionDay,
                        CostCenterId = member.User.Technician.CostCenterId,
                        TeamId = teamId,
                        CreatedUser = member.MemberParentName,
                        CurrentUserId = member.UserId,
                        ActionTypeId = (int)OrderActionType.Idle,
                        ActionTypeName = EnumManager<OrderActionType>.GetName(OrderActionType.Idle),
                        WorkingTypeId = (int)TimeSheetType.Idle,
                        //WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle),
                        PlatformTypeSource = PlatformType.Mobile,
                        TechnicianId = member.User.Technician.Id
                    };

                    if (isHasOrder)
                    {
                        model.WorkingSubReason = StaticAppSettings.IdleHandling[2].Name;
                        model.WorkingSubReasonId = StaticAppSettings.IdleHandling[2].Id;
                    }
                    else
                    {
                        model.WorkingSubReason = StaticAppSettings.IdleHandling[0].Name;
                        model.WorkingSubReasonId = StaticAppSettings.IdleHandling[0].Id;

                    }
                    var addedOrderAction = manager.Add(model);

                    if (addedOrderAction.IsSucceeded)
                    {
                        TechniciansStateViewModel technicianStatemodel = new TechniciansStateViewModel();
                        technicianStatemodel.State = TechnicianState.Idle;
                        technicianStatemodel.TeamId = (int)teamId;
                        technicianStatemodel.ActionDate = DateTime.Now;
                        technicianStatemodel.Long = 00.00m;
                        technicianStatemodel.Lat = 00.00m;
                        technicianStatemodel.TechnicianId = member.User.Technician.Id;
                        var addedTechnicianStateRes = technicianStateService.Add(technicianStatemodel);
                    }
                }

            }

            TeamModeViewModel result = new TeamModeViewModel();
            if (string.IsNullOrEmpty(model.WorkingType?.Name))
            {
                result.Mode = WorkingTypeEnum.EndWork.ToString();
            }
            else
            {
                if (string.IsNullOrEmpty(model.WorkingSubReason))
                {
                    result.Mode = $"{model.WorkingType?.Name}";
                }
                else
                {
                    result.Mode = $"{model.WorkingType?.Name} - {model.WorkingSubReason}";
                }
            }

            return ProcessResultHelper.Succedded<TeamModeViewModel>(result);

        }

        //[HttpPost]
        //[Route("GetTimeSheet")]
        //public async Task<ProcessResult<List<TimeSheetStructureViewModel>>> GetTimeSheet([FromBody]TimeSheetFilters model)
        //{
        //    if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
        //    {
        //        return ProcessResultHelper.Failed<List<TimeSheetStructureViewModel>>(null, null, "DateFrom and DateTo is requrired, DateTo should be greater than DateFrom");
        //    }
        //    else
        //    {
        //        var predicate = PredicateBuilder.True<OrderAction>();

        //        //if (!string.IsNullOrEmpty(model.PFNo))
        //        //{
        //        //    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("PF", model.PFNo));
        //        //}

        //        if (!string.IsNullOrEmpty(model.CostCenter))
        //        {
        //            predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("CostCenterName", model.CostCenter));
        //        }

        //        predicate = predicate.And(x => x.ActionDay.Date >= model.DateFrom.Date && x.ActionDay.Date <= model.DateTo);
        //        predicate = predicate.And(x => x.ActionTime != null);

        //        var timeSheetRow = manager.GetAll(predicate);

        //        if (timeSheetRow.IsSucceeded && timeSheetRow.Data != null)
        //        {

        //            //var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName }).Select(async cl => new TimeSheetStructureViewModel()
        //            //{
        //            //    DayDate = cl.First().ActionDay.Date,
        //            //    Name = cl.First().CreatedUser.ToString(),
        //            //    Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
        //            //    WorkType = cl.First().WorkingTypeName,
        //            //    //CostCenter = "",
        //            //    //DataEntryProfile = "SERV",
        //            //    //ActivityType = cl.First().ActionTypeName,
        //            //    //OrderNumber = orderManager.Get(cl.First().OrderId).Data.Code,
        //            //    //Operation = "0010",
        //            //    //SubOperation = "",
        //            //    //SalesOrderItemNumber = "",
        //            //    //SalesOrderNumber = "",
        //            //    //Network = "",
        //            //    //PFNo = (await teamMembersService.GetMembersByTeamId(orderManager.Get(cl.First().OrderId).Data.TeamId)).Data.FirstOrDefault().PF
        //            //});
        //            //// here the magic :D 
        //           // var xx = (await Task.WhenAll(result)).ToList();

        //            var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName }).Select(cl => new TimeSheetStructureViewModel()
        //            {
        //                DayDate = cl.First().ActionDay.Date,
        //                Name = cl.First().CreatedUser.ToString(),
        //                Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
        //                WorkType = cl.First().WorkingTypeName
        //            }).ToList();
        //            return ProcessResultHelper.Succedded<List<TimeSheetStructureViewModel>>(result);
        //            //return ProcessResultHelper.Succedded<List<TimeSheetStructureViewModel>>(xx);
        //        }
        //        else
        //        {
        //            return ProcessResultHelper.Failed<List<TimeSheetStructureViewModel>>(null, null, "Something wrong while getting data from order action manager");
        //        }
        //    }
        //}

        //HangFire
        private Expression<Func<OrderAction, bool>> CreateFilterPredicate(List<Filter> filters)
        {
            var predicate = PredicateBuilder.True<OrderAction>();
            foreach (var filter in filters)
            {
                filter.PropertyName = filter.PropertyName.ToPascalCase();
                if (filter.PropertyName.ToLower().Contains("date"))
                {
                    predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<OrderAction>(filter.PropertyName, filter.Value));
                }
                else
                {
                    predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<OrderAction>(filter.PropertyName, filter.Value));
                }
            }
            return predicate;
        }

        //[HttpPost]
        //[Route("GetTimeSheet")]
        //public async Task<ProcessResult<List<TimeSheetStructureExcelViewModel>>> GetTimeSheet([FromBody]TimeSheetFilters model)
        //{
        //    if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
        //    {
        //        return ProcessResultHelper.Failed<List<TimeSheetStructureExcelViewModel>>(null, null, "DateFrom and DateTo is requrired, DateTo should be greater than DateFrom");
        //    }
        //    else
        //    {
        //        List<TimeSheetStructureExcelViewModel> timeSheetStructureExcelViewLst = new List<TimeSheetStructureExcelViewModel>();
        //        var predicate = PredicateBuilder.True<OrderAction>();

        //        //if (!string.IsNullOrEmpty(model.PFNo))
        //        //{
        //        //    predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("PF", model.PFNo));
        //        //}

        //        if (!string.IsNullOrEmpty(model.CostCenter))
        //        {
        //            predicate = predicate.And(PredicateBuilder.CreateContainsExpression<OrderAction>("CostCenterName", model.CostCenter));
        //        }

        //        predicate = predicate.And(x => x.ActionDay.Date >= model.DateFrom.Date && x.ActionDay.Date <= model.DateTo);
        //        predicate = predicate.And(x => x.ActionTime != null);

        //        var timeSheetRow = manager.GetAll(predicate);

        //        if (timeSheetRow.IsSucceeded && timeSheetRow.Data != null)
        //        {
        //            var result = timeSheetRow.Data.GroupBy(x => new { x.ActionDay.Date, x.CostCenterName, x.CurrentUserId, x.WorkingTypeName}).Select(cl => new TimeSheetStructureViewModel()
        //            {
        //                DayDate = cl.First().ActionDay.Date,
        //                Name = cl.First().CreatedUser.ToString(),
        //                Time = new TimeSpan(cl.Sum(c => c.ActionTime.Value.Ticks)),
        //                WorkType = cl.First().WorkingTypeName,
        //                CostCenter = "",
        //                DataEntryProfile = "SERV",
        //                OrderNumber = cl.First().OrderId.ToString(),
        //                ActivityType = cl.First().ActionTypeName,
        //                Operation = "0010",
        //                SubOperation = "",
        //                SalesOrderItemNumber = "",
        //                SalesOrderNumber = "",
        //                Network = "",
        //                CreatedUserId = cl.First().CurrentUserId
        //            });

        //            List<TimeSheetStructureViewModel> timeSheetStructureViewLst = result.ToList();
        //            List<string> DispatcherUserIdLst = new List<string>();

        //            foreach (var mdl in timeSheetStructureViewLst.ToList())
        //            {
        //                if (mdl.OrderNumber != null && mdl.OrderNumber != "")
        //                {
        //                    var orderRes = orderManager.Get(int.Parse(mdl.OrderNumber));
        //                    if (orderRes.IsSucceeded && orderRes.Data != null)
        //                    {
        //                        mdl.OrderNumber = orderRes.Data.Code;
        //                    }
        //                }
        //                if (mdl.CreatedUserId != null && mdl.CreatedUserId != "")
        //                {
        //                    var techRes = await technicianService.GetTechnicianByUserId(mdl.CreatedUserId);
        //                    if (techRes.IsSucceeded && techRes.Data != null)
        //                    {
        //                        mdl.PFNo = techRes.Data.PF;
        //                    }
        //                    else
        //                    {
        //                        DispatcherUserIdLst.Add(mdl.CreatedUserId);
        //                    }
        //                }
        //                else
        //                {
        //                    timeSheetStructureViewLst.Remove(mdl);
        //                }

        //                if (mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual)|| mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Break)|| mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle)|| mdl.WorkType == EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling))
        //                {
        //                    mdl.WorkType = "ATRH";
        //                }
        //            }                    
        //            timeSheetStructureViewLst.RemoveAll(r => DispatcherUserIdLst.Contains(r.CreatedUserId));
        //            timeSheetStructureExcelViewLst = timeSheetStructureViewLst.GroupBy(x => x.DayDate).Select(cl => new TimeSheetStructureExcelViewModel() {
        //                Data_Entry_Profile = cl.First().DataEntryProfile,
        //                Key_Date = cl.First().DayDate,
        //                Personal_Number = cl.First().PFNo,
        //                Activity_Type = cl.First().ActivityType,
        //                Cost_Center = cl.First().CostCenter,
        //                Sales_Order_Number = cl.First().SalesOrderNumber,
        //                Sales_Order_Item_Number = cl.First().SalesOrderItemNumber,
        //                Order_Number = cl.First().OrderNumber,
        //                Network = cl.First().Network,
        //                Operation = cl.First().Operation,
        //                Sub_Operation = cl.First().SubOperation,
        //                Attendance_or_activity_type = cl.First().WorkType,
        //                Time = cl.First().Time.TotalHours
        //            }).ToList();

        //            return ProcessResultHelper.Succedded<List<TimeSheetStructureExcelViewModel>>(timeSheetStructureExcelViewLst);
        //        }
        //        else
        //        {
        //            return ProcessResultHelper.Failed<List<TimeSheetStructureExcelViewModel>>(null, null, "Something wrong while getting data from order action manager");
        //        }
        //    }
        //}







        [HttpPost]
        [Route("GetTimeSheetExcelExport")]
        public async Task<ProcessResult<string>> GetTimeSheetExcelExport([FromBody]TimeSheetFilters model)
        {



            string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd  HH-mmtt ss-fff");
            string newFile = $"{ _hostingEnv.WebRootPath}\\ExcelSheet\\TimeSheet {dateTime}.csv";
            int ExcelSheetindex = newFile.IndexOf("\\ExcelSheet\\");
            string fileResult;
            if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
            {
                return ProcessResultHelper.Failed("DateFrom and DateTo is requrired, DateTo should be greater than DateFrom", null);
            }
            else
            {
                model.PageInfo = new PaginatedItemsViewModel<OrderActionViewModel>()
                {
                    PageNo = 1,
                    PageSize = int.MaxValue
                };
                //model.DateTo = model.DateTo.AddDays(1);
                List<TimeSheetStructureExcelViewModel> timeSheetStructureExcelViewLst = (await GetTimeSheet(model)).Data.Items;

                fileResult = ListToExcelHelper.WriteObjectsToExcel(timeSheetStructureExcelViewLst, newFile);
                if (fileResult != null && fileResult != "")
                {


                    var url = new Uri(fileResult);
                    var absolute = url.AbsoluteUri.Substring(ExcelSheetindex);
                    //FileInfo file = new FileInfo(newFile);
                    //file.MoveTo(Path.ChangeExtension(newFile, ".xlsx"));
                    //result = result.Replace(".csv", ".xlsx");
                    return ProcessResultHelper.Succedded(absolute);
                }
                return ProcessResultHelper.Failed(string.Empty, null, "Something went wrong while exporting order report");

                //else
                //{
                //    return ProcessResultHelper.Failed<List<TimeSheetStructureExcelViewModel>>(null, null, "Something wrong while getting data from order action manager");
                //}
            }
        }



        [HttpPost]
        [Route("GetTimeSheet")]
        public async Task<ProcessResult<PaginationContainer<TimeSheetStructureExcelViewModel>>> GetTimeSheet([FromBody]TimeSheetFilters model)
        {
            int pageIndex = 0;
            int pageCount = 10;
            try
            {
                pageIndex = (model?.PageInfo?.PageNo) == 0 ? 0 : (int)(model?.PageInfo?.PageNo - 1);
                pageCount = model?.PageInfo?.PageSize == 0 ? 10 : (int)(model?.PageInfo?.PageSize);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }




            if (model.DateFrom == null || model.DateTo == null || model.DateTo < model.DateFrom)
            {
                return ProcessResultHelper.Failed<PaginationContainer<TimeSheetStructureExcelViewModel>>(null, null, "DateFrom and DateTo is requrired, DateTo should be greater than DateFrom");
            }
            else
            {
                //model.DateTo = model.DateTo.AddDays(1);
                PaginationContainer<TimeSheetStructureExcelViewModel> result = new PaginationContainer<TimeSheetStructureExcelViewModel>();
                var predicate = PredicateBuilder.True<OrderAction>();

                //predicate = predicate.And(x => x.PlatformTypeSource != PlatformType.Ftp);

                if (!string.IsNullOrEmpty(model.PFNo))
                {
                    predicate = predicate.And(x => x.TechnicianId != null && x.Technician.User.PF == model.PFNo);
                }
                if (!string.IsNullOrEmpty(model.CostCenter))
                {
                    predicate = predicate.And(x => x.CostCenter.Name == model.CostCenter);
                }
                //a.Start.Date >= startDate.Date && a.Start.Date <= endDate)
                // predicate = predicate.And(x => x.ActionDate >= model.DateFrom && (x.ActionDate.Add(new TimeSpan(x.ActionTime.Value.Add(TimeSpan.FromDays(x.ActionTimeDays != null ? x.ActionTimeDays.Value : 0)).Ticks))) <= model.DateTo);
                predicate = predicate.And(x => x.ActionDate >= model.DateFrom && x.ActionDate <= model.DateTo);
                //predicate = predicate.And(x => x.ActionDate == model.DateFrom && x.ActionDate == model.DateTo);

                predicate = predicate.And(y => y.ActionDay != null && y.CostCenterId != null && y.CurrentUserId != null && y.WorkingTypeId != null);
                //predicate = predicate.And(y => y.StatusId!= (int)OrderStatusEnum.Cancelled|| y.StatusId != (int)OrderStatusEnum.Compelete|| y.StatusId != (int)OrderStatusEnum.On_Hold);


                predicate = predicate.And(y => y.PlatformTypeSource == PlatformType.Mobile);
                predicate = predicate.And(y => y.TechnicianId != null);



                predicate = predicate.And(x => x.WorkingTypeId == null || (x.WorkingTypeId != 10 && x.WorkingTypeId != 8));
                predicate = predicate.And(x => x.StatusId == null ||
                                               (x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.On_Hold && x.StatusId != (int)OrderStatusEnum.Compelete)
                );



                var query = _context.OrderActions
                    .Include(x => x.Order)
                    .Include(x => x.CostCenter)
                    .Include(x => x.Technician)
                    .ThenInclude(x => x.User)
                    .Where(predicate);

                result.Count = query.Count();


                IQueryable<TimeSheetStructureViewModel> timeSheetStructureViewLst = query
            .OrderByDescending(x => x.ActionDate)
            .Skip(pageIndex * pageCount)
            .Take(pageCount)
                .Select(cl => new TimeSheetStructureViewModel()
                {
                    //cl.First()
                    DayDate = cl.ActionDate,
                    OrderNumber = cl.Order != null ? cl.Order.Code : null,
                    Name = cl.Technician.User.FullName,
                    PFNo = cl.Technician.User.PF,
                    Time =
                        cl.ActionTime == null ?
                            new TimeSpan((DateTime.Now - cl.ActionDate).Ticks)
                       : new TimeSpan(cl.ActionTime.Value.Add(TimeSpan.FromDays(cl.ActionTimeDays != null ? cl.ActionTimeDays.Value : 0)).Ticks),
                    WorkType = cl.WorkingType.Name,
                    WorkTypeId = cl.WorkingTypeId,
                    //CostCenter = "",
                    CostCenter = cl.CostCenter != null ? cl.CostCenter.Name : null,
                    DataEntryProfile = "SERV",
                    ActivityType = cl.ActionTypeName,
                    Operation = "0010",
                    SubOperation = "",
                    SalesOrderItemNumber = "",
                    SalesOrderNumber = "",
                    Network = "",
                    CreatedUserId = cl.CurrentUserId,
                    StatusId = cl.StatusId
                });


                List<TimeSheetStructureViewModel> timeSheetStructureViewLstasd = timeSheetStructureViewLst.ToList();

                //var accep = timeSheetStructureViewLstasd.FirstOrDefault(x => x.WorkTypeId == 13);

                List<int?> workTypesIdsThatWillBe_ATRH = new List<int?>() { 1, 2, 13, 9 };//"Actual" or "Traveling" or "Accept & Set First Order On-Travel"


                foreach (var item in timeSheetStructureViewLstasd)
                {
                    if (workTypesIdsThatWillBe_ATRH.Contains(item.WorkTypeId))
                        item.WorkType = "ATRH";

                    //  decimal hoursAsInt = (decimal) item.Time.Minutes /60;
                    decimal totlaMinutesRounded = Math.Round((decimal)item.Time.TotalMinutes, 0, MidpointRounding.AwayFromZero);
                    decimal totlalHoursRoundedFromMinutes = Math.Round(totlaMinutesRounded / 60, 2, MidpointRounding.AwayFromZero);

                    string hoursStringValues = totlalHoursRoundedFromMinutes.ToString();
                    result.Items.Add(new TimeSheetStructureExcelViewModel
                    {
                        Data_Entry_Profile = item.DataEntryProfile,
                        Key_Date = item.DayDate.AddHours(3).ToString("dd.MM.yyyy"),
                        //Key_Date = item.DayDate.ToString("dd.MM.yyyy"),
                        Personal_Number = item.PFNo,
                        Activity_Type = "FLDLBR",
                        //Activity_Type = item.ActivityType,
                        Cost_Center = item.CostCenter,
                        //Cost_Center = item.CostCenter,
                        Sales_Order_Number = item.SalesOrderNumber,
                        Sales_Order_Item_Number = item.SalesOrderItemNumber,
                        Order_Number = item.OrderNumber,
                        Network = item.Network,
                        Operation = item.Operation,
                        Sub_Operation = item.SubOperation,
                        //Attendance_or_activity_type = "ATRH",
                        Attendance_or_activity_type = item.WorkType,
                        Hours = hoursStringValues
                    });
                }
                return ProcessResultHelper.Succedded(result);
            }
        }






        [HttpGet]
        [Route("Test")]
        public ProcessResultViewModel<bool> Test()
        {
            //string[] files = Directory.GetFiles(_hostingEnv.WebRootPath + "\\ExcelSheet", $"*.xls");
            //foreach (var item in files.ToList())
            //{
            //    if (item== "ExcelSheet")
            //    {
            try
            {

                string[] files = Directory.GetFiles(FileUploadSettings.SourceFilePath, $"*.{FileUploadSettings.FileExtention}");

                if (files.Length > 0)
                {

                    return ProcessResultViewModelHelper.Succedded<bool>(true, files[0]);
                }

                return ProcessResultViewModelHelper.Failed<bool>(false, _hostingEnv.WebRootPath + "\\GhanimSAPFiles\\");
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }
        [HttpGet]
        [Route("Hang")]
        public async Task<ProcessResult<bool>> RegisterIdleBreakNotifications()
        {
            var descOrderActions = _context.OrderActions.Where(x => x.TeamId != null && x.TeamId != 0).ToList();
            var OrderActionsPerTeamgroup = descOrderActions.GroupBy(x => x.TeamId).Select(grp => new { TeamId = grp.Key, TeamList = grp.ToList() }).ToList();
            foreach (var teamItem in OrderActionsPerTeamgroup)
            {
                var lastAction = teamItem.TeamList.LastOrDefault();
                var AlertTime = TimeSpan.FromMinutes(30);
                //  var lastAction = mapper.Map<OrderAction, OrderActionViewModel>(item.TeamList.LastOrDefault());
                //int AlertTime = 30;
                if (lastAction != null)
                {
                    if (lastAction.WorkingTypeId == (int)TimeSheetType.Idle || lastAction.WorkingTypeId == (int)TimeSheetType.Break)
                    {
                        var LastTime = lastAction.ActionTime == null ?
                                           new TimeSpan((DateTime.Now - lastAction.ActionDate).Ticks)
                                      : new TimeSpan(lastAction.ActionTime.Value.Add(TimeSpan.FromDays(lastAction.ActionTimeDays != null ? lastAction.ActionTimeDays.Value : 0)).Ticks);
                        // var LastTime = T.Minutes;
                        if (LastTime > AlertTime)
                        {
                            //   var NotificationRes = await NotifyDispatcherWithTechnicianLate(teamItem.TeamId.Value);
                        }
                    }
                }
            }

            return ProcessResultHelper.Succedded<bool>(true);
        }

    }
}