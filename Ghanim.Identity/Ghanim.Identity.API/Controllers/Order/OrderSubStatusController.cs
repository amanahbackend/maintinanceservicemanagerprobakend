﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OrderSubStatusController : BaseAuthforAdminController<IOrderSubStatusManager, OrderSubStatus, OrderSubStatusViewModel>
    {
        IOrderSubStatusManager manager;
        IProcessResultMapper processResultMapper;
        IServiceProvider _serviceprovider;

        public OrderSubStatusController(IServiceProvider serviceprovider, IOrderSubStatusManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            _serviceprovider = serviceprovider;
        }
        private ILang_OrderSubStatusManager Lang_OrderSubStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_OrderSubStatusManager>();
            }
        }
        //For Mobile
        [HttpGet]
        [Route("GetByStatusIdLocalized/{id}")]
        public ProcessResult<List<OrderSubStatusViewModel>> GetByStatusIdLocalized(int id)
        {
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            // string languageIdValue = langCode.ToString();
            var actions = manager.GetByStatusId(id);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderSubStatusRes = Lang_OrderSubStatusmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderSubStatus_ID == item.Id);
                    if (OrderSubStatusRes != null && OrderSubStatusRes.Data != null)
                    {
                        item.Name = OrderSubStatusRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<OrderSubStatus>, List<OrderSubStatusViewModel>>(actions.Data);
                return ProcessResultHelper.Succedded<List<OrderSubStatusViewModel>>(result);
            }
            //return actions;
            return ProcessResultHelper.Failed<List<OrderSubStatusViewModel>>(null, null, SharedResource.General_WrongWhileGet);

        }
        //For Web
        [HttpGet]
        [Route("GetByStatusId")]//[FromQuery] int id
        [Authorize(Roles = StaticRoles.Dispatcher)]
        public ProcessResult<List<OrderSubStatusViewModel>> GetByStatusId([FromQuery] int id)
        {
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //string languageIdValue = langCode.ToString();
            var actions = manager.GetByStatusId(id);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderSubStatusRes = Lang_OrderSubStatusmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderSubStatus_ID == item.Id);
                    if (OrderSubStatusRes != null && OrderSubStatusRes.Data != null)
                    {
                        item.Name = OrderSubStatusRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<OrderSubStatus>, List<OrderSubStatusViewModel>>(actions.Data);
                return ProcessResultHelper.Succedded<List<OrderSubStatusViewModel>>(result);
            }
            //return actions;
            return ProcessResultHelper.Failed<List<OrderSubStatusViewModel>>(null, null, SharedResource.General_WrongWhileGet);

        }

        [HttpGet]
        [Route("GetByStatusIdWithBlocked")]
        public ProcessResult<List<OrderSubStatus>> GetByStatusIdWithBlocked([FromQuery] int id)
        {
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            var actions = manager.GetByStatusIdWithBlocked(id);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderSubStatusRes = Lang_OrderSubStatusmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderSubStatus_ID == item.Id);
                    if (OrderSubStatusRes != null && OrderSubStatusRes.Data != null)
                    {
                        item.Name = OrderSubStatusRes.Data.Name;
                    }
                }
            }
            return actions;
        }
        //[HttpPost]
        //[Route("Add")]
        //public override ProcessResultViewModel<OrderSubStatusViewModel> Post([FromBody]OrderSubStatusViewModel model)
        //{
        //    //FillCurrentUser(model);
        //    var entityModel = mapper.Map<OrderSubStatusViewModel, OrderSubStatus>(model);
        //    var entityResult = manger.AddOrderSubStatus(entityModel);
        //    return processResultMapper.Map<OrderSubStatus, OrderSubStatusViewModel>(entityResult);
        //}
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<OrderSubStatusViewModel> Post([FromBody] OrderSubStatusViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<OrderSubStatusViewModel>(null, "OrderSubStatus Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower() ||
                     x.Code == model.Code
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<OrderSubStatusViewModel>(null, "OrderSubStatus Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }


    }
}