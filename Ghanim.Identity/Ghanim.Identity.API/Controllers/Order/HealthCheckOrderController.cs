﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ghanim.API.Controllers
{
    //[ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class HealthCheckOrderController : Controller
    {
        public HealthCheckOrderController()
        {
        }

        [HttpGet("")]
        [HttpHead("")]
        public IActionResult Ping()
        {
            return Ok("I'm fine");
        }
    }
}