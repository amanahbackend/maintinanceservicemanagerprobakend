﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using CommonEnums;
using DispatchProduct.RepositoryModule;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.API.Helper;
using Microsoft.AspNetCore.Mvc;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Hosting;
using Ghanim.API.ServiceCommunications.Order;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities;
using Utilities.Utilites.GenericListToExcel;
using Utilities.ProcessingResult;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;
using Ghanim.Models.Order.Enums;
using Ghanim.API.Models;
using StackExchange.Redis;
using Exception = System.Exception;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.API.Utilities.HangFireServices;
using Ghanim.API.ViewModels.Order;
using System.Diagnostics;
using AutoMapper.QueryableExtensions;
using Ghanim.Models.Views;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Ghanim.API.ServiceCommunications.FlixyApi;
using Ghanim.BLL.Services.FlixyApi;
using Ghanim.BLL.Order.Managers;

namespace Ghanim.API.Controllers
{



    [Authorize]
    [Route("api/[controller]")]
    public partial class OrderController : BaseController
        <IOrderManager, OrderObject, OrderViewModel>
    {
        private ApplicationDbContext _context;
        public IHangFireJobService hangFireJobService;
        public new IOrderManager manager;
        public IOrderSettingManager settingManager;
        public IOrderActionManager actionManager;
        IServiceProvider _serviceprovider;
        public readonly new IMapper mapper;
        private readonly IHostingEnvironment _hostingEnv;




        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IDispatcherService dispatcherService;
        IForemanService foremanService;
        ISupervisorService supervisorService;
        ITeamMembersService teamMembersService;
        INotificationService notificationService;
        ISMSService smsService;
        ITeamService teamService;
        ITeamManager _teamManager;
        IIdentityService identityService;
        IForemanService formanService;
        IEngineerService engineerService;
        IManagerService managerService;
        IArchivedOrderFileManager archivedOrderFileManager;
        ITechnicianService technicianService;
        IAPIHelper helper;
        ITechnicianStateService technicianStateService;
        INotificationCenterManager notificationCenterManageManager;
        IDispatcherManager _IDispatcherManager;
        ITechnicianManager _ITechnicianManager;
        private readonly IOrderService _orderService;
        private readonly IOrderHistoryTeamManager _IOrderHistoryTeamManager;
        private readonly IVisitSlotManager _IVisitSlotManager;
        private readonly IOrderRemoteRequestsHistoryManager _IOrderRemoteRequestsHistoryManager;
        private readonly IOrderRemoteRequestsOnProcessManager _IOrderRemoteRequestsOnProcessManager;
        private IFlixyService _flixService;

        void someDummyMethod()
        {


            var ordersToDelete = _context.Orders.Select(x => new { x.Id, x.Code, OrderActions = x.OrderActions.Select(a => a.Id) }).GroupBy(x => x.Code)
                   .Where(x => x.Count() > 1).Select(x => x.FirstOrDefault()).ToList();


            List<int> orderActions = ordersToDelete.SelectMany(x => x.OrderActions).ToList();
            _context.OrderActions.RemoveRange(_context.OrderActions.Where(oa => orderActions.Contains(oa.Id)));


            List<int> oredrs = ordersToDelete.Select(x => x.Id).ToList();
            _context.Orders.RemoveRange(_context.Orders.Where(oa => oredrs.Contains(oa.Id)));
            _context.SaveChanges();
        }




        public OrderController(IServiceProvider serviceprovider, IHostingEnvironment hostingEnv, IOrderManager _manager,
            ApplicationDbContext context,
          IHangFireJobService hangFireJobService,
            IOrderSettingManager _settingManager,
            IOrderActionManager _actionManager,
            IAPIHelper _helper,
            IMapper _mapper,
            INotificationCenterManager _notificationCenterManageManager,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ITeamMembersService _teamMembersService,
            IDispatcherService _dispatcherService,
            IForemanService _foremanService,
            INotificationService _notificationService,
            ITeamService _teamService,
            ITeamManager teamManager,
            IIdentityService _identityService,
            ISupervisorService _supervisorService,
            IForemanService _formanService,
            IEngineerService _engineerService,
            IManagerService _managerService,
            IArchivedOrderFileManager _archivedOrderFileManager,
            ITechnicianService _technicianService,
            ITechnicianStateService _technicianStateService,
            ISMSService _smsService,
           IOrderService orderService,
           IDispatcherManager IDispatcherManager,
           IOrderHistoryTeamManager IOrderHistoryTeamManager,
           ITechnicianManager ITechnicianManager,
            IFlixyService flixService,
            IVisitSlotManager IVisitSlotManager,
            IOrderRemoteRequestsHistoryManager IOrderRemoteRequestsHistoryManager,
            IOrderRemoteRequestsOnProcessManager IOrderRemoteRequestsOnProcessManager
            ) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            this._context = context;


            ////List<Technician> allTechUser = context.Users.Include(x => x.Technician).Where(x => x.Technician != null)
            ////    .Select(c => c.Technician).ToList();

            //var x = new OrderManagementReportOutputViewModel() { OrderCode = "asdasdas"};
            //var withProgress = new OrderManagementReportExportOutputViewModelWithProgreassColomns(x);
            //var with = new OrderManagementReportExportOutputViewModel(x);




            //List<x> listParetn = new List<x>()
            //{
            //    new x(){Code = "a",list=new List<int>(){1,2,3,4,5,6}},
            //    new x(){Code = "b",list=new List<int>(){}},
            //    new x(){Code = "c",list=new List<int>(){7}},
            //    new x(){Code = "d",list=new List<int>(){8,9,10}},
            //    new x(){Code = "e",list=new List<int>(){}},
            //    new x(){Code = "f",list=new List<int>(){1,2,3,4,5,6}},
            //    new x(){Code = "g",list=new List<int>(){}},
            //};

            //var query = from parent in listParetn
            //            join child in parent.list on parent.list equals pet.Owner
            //    select new { OwnerName = person.FirstName, PetName = pet.Name };










            _teamManager = teamManager;
            this.hangFireJobService = hangFireJobService;
            _serviceprovider = serviceprovider;
            _hostingEnv = hostingEnv;
            manager = _manager;
            settingManager = _settingManager;
            actionManager = _actionManager;
            mapper = _mapper;
            helper = _helper;
            notificationCenterManageManager = _notificationCenterManageManager;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            dispatcherService = _dispatcherService;
            foremanService = _foremanService;
            teamMembersService = _teamMembersService;
            notificationService = _notificationService;
            teamService = _teamService;
            supervisorService = _supervisorService;
            identityService = _identityService;
            formanService = _formanService;
            engineerService = _engineerService;
            managerService = _managerService;
            technicianService = _technicianService;
            technicianStateService = _technicianStateService;
            smsService = _smsService;
            archivedOrderFileManager = _archivedOrderFileManager;
            _orderService = orderService;
            _IDispatcherManager = IDispatcherManager;
            _ITechnicianManager = ITechnicianManager;
            _IOrderHistoryTeamManager = IOrderHistoryTeamManager;
            _flixService = flixService;
            _IVisitSlotManager = IVisitSlotManager;
            _IOrderRemoteRequestsHistoryManager = IOrderRemoteRequestsHistoryManager;
            _IOrderRemoteRequestsOnProcessManager = IOrderRemoteRequestsOnProcessManager;




            //someDummyMethod();
        }
        private IOrderStatusManager OrderStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderStatusManager>();
            }
        }
        private IOrderSubStatusManager OrderSubStatusmanager
        {
            get
            {
                return _serviceprovider.GetService<IOrderSubStatusManager>();
            }
        }





        [HttpGet]
        [Route("testFlixyDto/{orderId}")]//205 
        public async Task<ProcessResultViewModel<string>> testFlixyDtoAsync(int orderId)
        {
            var inpt = manager.GetOrderStatusChangedFlixyDto(orderId, 3);
            var result = await _flixService.OrderStatusChanged(inpt);
            return result;
        }




        [HttpPost]
        [Route("SetAcceptence")]
        public async Task<ProcessResultViewModel<bool>> SetAcceptence([FromBody]AcceptenceViewModel model)
        {
            if (model.OrderId > 0)
            {

                //    //notify dispatcher with acceptance or rejection

                //    var NotificationRes = await NotifyDispatcherWithOrderAcceptence(model);
                //    return ProcessResultViewModelHelper.Succedded<bool>(true);
                //}
                //else
                //{
                //    return ProcessResultViewModelHelper.Failed<bool>(false, entityResult.Status.Message);
                //}
                var orderResult = manager.Get(model.OrderId);
                if (orderResult.Data.TeamId == null)
                    return ProcessResultViewModelHelper.Failed(false, "This Order Must Has A Team");



                var acceptenceResult = await _orderService.ChangeAcceptence(model, Request);
                if (acceptenceResult.IsSucceeded && acceptenceResult.Data == true)
                {
                    var NotificationRes = await _orderService.NotifyDispatcherWithOrderAcceptence(model, Request);
                    return ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, acceptenceResult.Status.Message);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "order id is required");
            }
        }




        [HttpPut]
        [Route("ChangeStatus")] //for Web And Mobile 
        public async Task<ProcessResult<bool>> ChangeStatus([FromBody] ChangeStatusViewModel model)
        {
            if ((model.OrderId == 0 || model.OrderId == null) || (model.StatusId == 0 || model.StatusId == null) || (model.SubStatusId == 0 || model.SubStatusId == null))
            {
                throw new Exception("OrderId , StatusId , SubStatusId are required");
                //return ProcessResultHelper.Failed<bool>(false, null, "OrderId , StatusId , SubStatusId and TeamId are required");
            }
            //PlatformType SourcePlatformType = model.PlatformType != PlatformType.Mobile ? PlatformType.Web : PlatformType.Mobile;

            //if (model.PlatformType != PlatformType.Mobile)
            //    model.PlatformType = PlatformType.Web;
            var orderResult = manager.Get(model.OrderId);
            if (StaticAppSettings.ArchivedStatusId.Contains(orderResult.Data.StatusId))
            {
                var ProcessResultViewModelVM = ProcessResultViewModelHelper.Failed(false, null, ProcessResultStatusCode.Failed, "Can't edit closed Order", (int)OrderUpdateResponseStatus.Completed);
                var res = Mapper.Map<ProcessResult<bool>>(ProcessResultViewModelVM);
                return res;
            }


            var currentTech = _ITechnicianManager.GetByUserId(helper.CurrentUserId());
            List<int> validStatusIfNoTeamInOrder = new List<int>()
            {
                (int)OrderStatusEnum.On_Hold,
                (int) OrderStatusEnum.Cancelled,
                (int)OrderStatusEnum.Open
            };

            //TODO if (orderResult.Data.TeamId == null || currentTech.Data == null || orderResult.Data.TeamId != currentTech.Data.TeamId)
            if (orderResult.Data.TeamId == null && !validStatusIfNoTeamInOrder.Contains(orderResult.Data.StatusId))
                return ProcessResultHelper.Failed(false, null, "This Order Must Has A Team");


            List<int> workingStatusId = StaticAppSettings.WorkingStatusId;
            if (workingStatusId.Contains(model?.StatusId ?? 0))
                model.ServeyReport = null;


            model.TeamId = orderResult.Data.TeamId;//TODO Refactor issue
            var statusRes = OrderStatusmanager.Get(model.StatusId);
            var subStatusRes = OrderSubStatusmanager.Get(model.SubStatusId);
            //if (orderResult.Data.DispatcherId != 0)
            //{
            //    var dispRes = dispatcherService.GetDispatcherById(orderResult.Data.DispatcherId ?? 0);//TODO Refactor issue
            //    //orderResult.Data.DispatcherName = dispRes.Data.Name;
            //}


            orderResult.Data.StatusId = (int)(model.StatusId == 0 ? null : model.StatusId);
            //orderResult.Data.StatusName = statusRes.Data.Name;
            orderResult.Data.SubStatusId = (int)(model.SubStatusId == 0 ? null : model.SubStatusId);
            orderResult.Data.ServeyReport = model.ServeyReport;


            if (StaticAppSettings.TravellingStatusId.Contains((int)model.StatusId))
            {
                // send sms here
                string msgContent = string.Format(StaticAppSettings.SMSContent, orderResult.Data.Code, statusRes.Data.Name);
                var sms = new SMSViewModel()
                {
                    PhoneNumber = orderResult.Data.PhoneOne,
                    Message = msgContent
                };
                // var smsResult = await smsService.Send(sms);
            }
            // make order unassigned incase it changed to on hold
            if (StaticAppSettings.UnassignedStatusId.Contains((int)model.StatusId))
            {
                orderResult.Data.PrevTeamId = orderResult.Data.TeamId;
                orderResult.Data.TeamId = null;
                orderResult.Data.RankInTeam = 0;

                //order team history
                OrderHistoryTeam ordHistory = new OrderHistoryTeam();
                ordHistory.OrderId = orderResult.Data.Id;
                ordHistory.Code = orderResult.Data.Code;
                ordHistory.PrevTeamId = orderResult.Data.PrevTeamId;
                ordHistory.TeamId = orderResult.Data.TeamId;
                _IOrderHistoryTeamManager.Add(ordHistory);

            }
            orderResult.Data.AcceptanceFlag = AcceptenceType.Accepted;
            var updateResult = manager.Update(orderResult.Data);
            //new
            // var teamMembers = await teamMembersService.GetMemberUsersByTeamId(model.TeamId);
            int? NewTeam = model.TeamId;
            //orderResult.Data.TeamId = NewTeam;


            var orderActionType = model.PlatformType == PlatformType.Web ? OrderActionType.UpdateOrder : OrderActionType.ChangeStatus;

            await _orderService.AddAction(orderResult.Data, Request, orderActionType, (int)TimeSheetType.Actual, EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual), NewTeam, model.ServeyReport, null, model.PlatformType.ToString());

            //notify teamMembers and dispatcher with new status
            //model.StatusName = statusRes.Data.Name;
            model.SubStatusName = subStatusRes.Data.Name;
            var NotificationRes = await _orderService.NotifyDispatcherWithNewStatus(model, Request);

            return ProcessResultHelper.Succedded<bool>(true);


        }

        [HttpPost]
        [Route("AddMulti")]
        [Authorize(Roles = StaticRoles.Closed)]
        public override ProcessResultViewModel<List<OrderViewModel>> PostMulti([FromBody]List<OrderViewModel> orders)
        {
            try
            {
                for (int i = 0; i < orders.Count; i++)
                {
                    // Check for repeated call
                    DateTime currentTime = DateTime.Now;

                    var customerOrders = manager.GetAll(x => x.CustomerCode == orders[i].CustomerCode && (currentTime - x.CreatedDate).TotalHours <= 24);
                    if (customerOrders.IsSucceeded && customerOrders.Data.Count > 0)
                    {
                        orders[i].IsRepeatedCall = true;
                    }

                }
                var addedOrders = base.PostMulti(orders);
                if (addedOrders.IsSucceeded && addedOrders.Data.Count > 0)
                {
                    var mappedOrders = mapper.Map<List<OrderViewModel>, List<OrderObject>>(addedOrders.Data);
                    for (int i = 0; i < mappedOrders.Count; i++)
                    {
                        _orderService.AddAction(mappedOrders[i], Request, OrderActionType.AddOrder, null, null, null, null, null);
                    }
                }

                return addedOrders;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        //
        //[Route("Get/{id}")]
        //[HttpGet]
        //public override ProcessResultViewModel<ApplicationUserViewModel> Get([FromRoute]int id)
        //{
        //    var entityResult = manger.Get(id);
        //    //FillCurrentUser(entityResult);
        //    return processResultMapper.Map<AspNetUsers, ApplicationUserViewModel>(entityResult);
        //}

        [HttpPut]
        [Route("BulkAssign")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        public async Task<ProcessResult<bool>> BulkAssign([FromBody] List<BulkAssignViewModel> model)
        {
            var header = helper.GetAuthHeader(Request);
            // string currentUserId = "7e1717f7-d3ac-40b1-8787-e2065b3d008e";
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultHelper.Failed<bool>(false, null, "Unauthorized");
            }

            var UserRes = await identityService.GetById(currentUserId);
            var excuter = UserRes?.Data?.UserName;

            var result = false;
            var failedOrders = new List<int>();

            foreach (var item in model)
            {
                var dispRes = dispatcherService.GetDispatcherById(item.DispatcherId.Value);
                ProcessResultViewModel<string> NotificationRes = null;

                if (!item.IsTeam && item.DispatcherId == null && (item.StatusId != StaticAppSettings.OnHoldStatus.Id && item.StatusId != StaticAppSettings.InitialStatus.Id))
                {
                    return ProcessResultHelper.Failed<bool>(false, null, "dispatcher id is required while assign type is to dispatcher for order " + item.OrderId);
                }
                else if (item.IsTeam && item.TeamId == null && (item.StatusId != StaticAppSettings.OnHoldStatus.Id && item.StatusId != StaticAppSettings.InitialStatus.Id))
                {
                    return ProcessResultHelper.Failed<bool>(false, null, "team id is required while assign type is to team for order " + item.OrderId);
                }

                if (!item.IsTeam)
                {
                    //Assign For Dispatcher
                    result = _orderService.BulkAssignForDispatcher(item.OrderId, false, item.TeamId, item.DispatcherId, item.DispatcherName, item.SupervisorId, item.SupervisorName, item.StatusId, item.StatusName, excuter).IsSucceeded;
                }
                else
                {
                    //NewBulkAssign
                    // result = manager.BulkAssign(item.OrderId, item.IsTeam, item.TeamId, item.DispatcherId, item.DispatcherName, item.SupervisorId, item.SupervisorName, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name, excuter).Data;                   
                    ProcessResult<bool> processResult = await _orderService.BulkAssignForTeam(item.OrderId, item.IsTeam, item.TeamId, item.DispatcherId, item.DispatcherName, item.SupervisorId, item.SupervisorName, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name, excuter, Request);
                    result = processResult.IsSucceeded;
                }

                try
                {
                    if (result)
                        NotificationRes = await NotifyTeamMembersandDispatcherWithBulkAssign(item);
                }
                catch (Exception e) { }


                if (!result)
                {
                    failedOrders.Add(item.OrderId);
                }
            }
            if (failedOrders.Count() > 0)
            {

                string ordersCOde = string.Join(',', manager.GetAllQuerable().Data.Where(x => failedOrders.Contains(x.Id)).Select(x => x.Code));
                return ProcessResultHelper.Failed<bool>(false, null, "something went wrong while re-assigning orders " + ordersCOde);
            }

            return ProcessResultHelper.Succedded(true);
        }

        //
        [HttpPut]
        [Route("Assign")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        public async Task<ProcessResult<bool>> Assign([FromBody] AssignViewModel model)
        {
            if (model.OrderId == 0 && model.TeamId == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order id & team id is required");
            }
            var team = _teamManager.Get(model.TeamId);
            if (team.IsSucceeded && team.Data != null)
            {
                if (team.Data.StatusId != 1)
                    return ProcessResultHelper.Failed<bool>(false, null, "team is not available", ProcessResultStatusCode.InvalidValue);
            }

            var order = manager.GetAllQuerable().Data.Select(x => new
            {
                x.Id,
                OldTeam = x.TeamId,
            }).FirstOrDefault(x => x.Id == model.OrderId);




            if (order == null)
                return ProcessResultHelper.Failed<bool>(false, null, "the order was not found ");

            if (order.OldTeam == model.TeamId)
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already assigned to this team, please refresh the page");

            if (model.TeamId == 0)
                return ProcessResultHelper.Failed<bool>(false, null, "the Team Is not valid");
            var ordersCountForInputTeam = _context.Orders.Count(x => x.TeamId == model.TeamId &&
                       x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);


            OrderObject updatedorder = manager.Assign(model.OrderId, model.TeamId);


            //if (order.StatusId == StaticAppSettings.OnHoldStatus.Id)
            //{
            //    //Re-Scheduled
            //    assignResult = manager.Assign(model.OrderId, model.TeamId, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name, StaticAppSettings.DispatchedSubStatusIds[1], StaticAppSettings.DispatchedSubStatusNames[1]);
            //}
            //else
            //{
            //    //Scheduled
            //    assignResult = manager.Assign(model.OrderId, model.TeamId, StaticAppSettings.DispatchedStatus.Id, StaticAppSettings.DispatchedStatus.Name, StaticAppSettings.DispatchedSubStatusIds[0], StaticAppSettings.DispatchedSubStatusNames[0]);
            //}

            if (updatedorder != null)
            {
                await _orderService.AddAction(updatedorder, Request, OrderActionType.Assign, null, null, model.TeamId, null, null, null, null, PlatformType.Web);

                await NotifyTeamMembersWithNewOrder(model.TeamId, updatedorder);
                if ((order.OldTeam ?? 0) != 0)
                {
                    await NotifyTeamMembersWithUnassignOrder((int)order.OldTeam, model.OrderId);
                }

                return ProcessResultHelper.Succedded<bool>(true);
            }
            else
            {
                return ProcessResultHelper.Failed<bool>(false, null, "something wrong while assign order to team");
            }
        }

        [HttpPut]
        [Route("UnAssign")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]

        public async Task<ProcessResult<bool>> UnAssign([FromBody] UnAssignViewModel model)
        {
            if (model.OrderId == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order id is required");
            }
            var orderRes = manager.Get(model.OrderId);
            int? oldTeamId = orderRes.Data.TeamId;//TODO Refactor issue


            if (oldTeamId == null)
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already Unassigned from this team, please refresh the page");


            if (StaticAppSettings.CanUnassignStatusId.Contains(orderRes.Data.Status.Id))
            {

                manager.UnAssign(model.OrderId);

                //if (orderRes.Data.StatusId == StaticAppSettings.OnHoldStatus.Id)
                //{

                //    unAssignResult = manager.UnAssign(model.OrderId, StaticAppSettings.OnHoldStatus.Id, StaticAppSettings.OnHoldStatus.Name);
                //}
                //else
                //{
                //    unAssignResult = manager.UnAssign(model.OrderId, StaticAppSettings.InitialStatus.Id, StaticAppSettings.InitialStatus.Name);
                //}

                var order = Get(model.OrderId).Data;
                var orderObject = Mapper.Map<OrderObject>(order);
                var ordersCountForInputTeam = _context.Orders.Count(x => x.TeamId == oldTeamId &&
                       x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);

                await _orderService.AddAction(orderObject, Request, OrderActionType.UnAssgin, null, null, oldTeamId, null, null, null, null, PlatformType.Web);
                await NotifyTeamMembersWithUnassignOrder(oldTeamId ?? 0, model.OrderId);
                return ProcessResultHelper.Succedded<bool>(true);
            }
            else
            {
                return ProcessResultHelper.Failed<bool>(false, null, "couldn't unassign this order with this status");
            }

        }

        [HttpPut]
        [Route("BulkUnAssign")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        public ProcessResult<bool> BulkUnAssign([FromBody] List<int> model)
        {
            if (model == null || model.Count == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order ids is required while unassign orders");
            }

            return manager.BulkUnAssign(model, StaticAppSettings.InitialStatus.Id);
        }

        [HttpPut]
        [Route("ChangeTeamRank")]
        [Authorize(Roles = StaticRoles.Dispatcher)]
        public async Task<ProcessResult<bool>> ChangeTeamRank([FromBody] ChangeRankInTeamViewModel model)
        {
            List<int> workingStatusId = StaticAppSettings.WorkingStatusId;
            if (model == null || model.TeamId == 0 || model.Orders == null || model.Orders.Count < 1)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "team id and order id and rank are required");
            }
            //var order = Get(model.orderId).Data;
            int OrderNovalue = 0;
            int OrderInvalidvalue = 0;
            OrderStatusEnum? OrderNovalueStatus = null;
            OrderStatusEnum? OrderInvalidvalueStatus = null;
            var OrderInModelIndex = 0;
            var OrderInBackIndex = 0;
            var OrderInModelRank = 0;
            var OngoingOrderRankInMode = 0;

            for (int i = 0; i < model.Orders.Count; i++)
            {
                if (model.Orders.ElementAt(i).Value == 1)
                {
                    OrderNovalue = model.Orders.ElementAt(i).Key;
                }
            }
            for (int i = 0; i < model.Orders.Count; i++)
            {
                if (model.Orders.ElementAt(i).Value == 0)
                {
                    OrderInvalidvalue = model.Orders.ElementAt(i).Key;
                }
            }
            ///
            var InvalidStatus = manager.GetAll(x => x.TeamId == model.TeamId && x.Id == OrderNovalue);
            var InvalidStatus2 = manager.GetAll(x => x.TeamId == model.TeamId && x.Id == OrderInvalidvalue);
            foreach (var item in InvalidStatus.Data)
            {
                OrderNovalueStatus = (OrderStatusEnum)item.StatusId;
            }
            foreach (var item in InvalidStatus2.Data)
            {
                OrderInvalidvalueStatus = (OrderStatusEnum)item.StatusId;
            }
            var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            var orderObject = teamOrders.Data.FirstOrDefault(x => x.Id == (int)model.orderId);
            var OrderInBackOldRank = teamOrders.Data.FirstOrDefault(x => x.Id == model.orderId).RankInTeam;
            var OldOrderIdAtSeconIndex = 0; //teamOrders.Data[1].Id;
                                            // var rr = teamOrders.Data.FirstOrDefault(x => x.RankInTeam == 1).Id;
            var OldRanksSorted = teamOrders.Data.OrderBy(x => x.RankInTeam).ToList();
            var BeforeLastOrder = 0;
            //int ArrLength = OldRanksSorted.Count;
            for (int i = 0; i < OldRanksSorted.Count; i++)
            {
                if (i == 1)
                {
                    BeforeLastOrder = (int)OldRanksSorted[i].RankInTeam;
                    OldOrderIdAtSeconIndex = OldRanksSorted[i].Id;

                }
            }
            // order = Get(model.orderId).Data;
            //var orderObject = Mapper.Map<OrderObject>(order);
            if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
            {
                foreach (var teamorder in teamOrders.Data)
                {
                    if (workingStatusId.Contains(teamorder.StatusId))
                    {
                        if (OrderNovalueStatus == OrderStatusEnum.On_Travel || OrderNovalueStatus == OrderStatusEnum.Reached || OrderNovalueStatus == OrderStatusEnum.Started)
                        {
                            var changeRes = manager.ChangeOrderRank2(teamOrders.Data, model.Orders, teamorder.Id, OrderInvalidvalue);
                            var UpdatedteamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                            var NewOrderIdAtSeconIndex = UpdatedteamOrders.Data.FirstOrDefault(x => x.RankInTeam == 1).Id;
                            if (changeRes.IsSucceeded && changeRes.Data == true)
                            {
                                //var OrderInBackNewRank = UpdatedteamOrders.Data.FirstOrDefault(x => x.Id == model.orderId).RankInTeam;
                                // OrderInBackIndex= UpdatedteamOrders.Data.FindIndex(a => a.Id == model.orderId);
                                //var c = OrderInModelRank;

                                if (OldOrderIdAtSeconIndex != NewOrderIdAtSeconIndex)
                                {
                                    await _orderService.AddAction(orderObject, Request, OrderActionType.ChangeTeamRank, null, null, model.TeamId);
                                    AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = true, OrderId = (int)model.orderId };
                                    var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                                    //await AddAction(teamOrders.Data.FirstOrDefault(), OrderActionType.ChangeTeamRank, null, null);

                                }

                                return ProcessResultHelper.Succedded<bool>(true);
                            }
                            else
                            {
                                return ProcessResultHelper.Failed<bool>(false, changeRes.Exception);
                            }
                        }
                        else
                        {
                            //teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                            //if (changeRes.IsSucceeded && changeRes.Data == true)
                            //{
                            //    await _orderService.AddAction(orderObject, Request, OrderActionType.ChangeTeamRank, null, null, model.TeamId);
                            //    AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = true, OrderId = (int)model.orderId };
                            //    var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                            //    //await AddAction(teamOrders.Data.FirstOrDefault(), OrderActionType.ChangeTeamRank, null, null);
                            //    return ProcessResultHelper.Succedded<bool>(true);
                            //}
                            //else
                            //{
                            //    return ProcessResultHelper.Failed<bool>(false, changeRes.Exception);
                            //}
                        }
                    }
                    //if (travellingStatusId.Contains(teamorder.StatusId))
                    //{
                    //    return ProcessResultHelper.Failed<bool>(false, null, "You can't change order rank when technician has order with on travel or reached status.", Utilities.ProcessingResult.ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
                    //}
                }

                var res = await _orderService.ChangeOrderRankAsync(teamOrders.Data, model.Orders, model.orderId);
                if (res.IsSucceeded)
                {
                    AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = true, OrderId = model.orderId };
                    var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                }

                return ProcessResultHelper.Succedded<bool>(true);
            }

            return ProcessResultHelper.Failed<bool>(false, null, "something worng while getting team orders", ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        }

        //
        //[HttpPut]
        //[Route("ChangeTeamRank")]
        //public async Task<ProcessResult<bool>> ChangeTeamRank([FromBody] ChangeRankInTeamViewModel model)
        //{
        //    List<int> travellingStatusId = StaticAppSettings.TravellingStatusId;
        //    if (model == null || model.TeamId == 0 || model.Orders == null || model.Orders.Count < 1)
        //    {
        //        return ProcessResultHelper.Failed<bool>(false, null, "team id and order id and rank are required");
        //    }

        //    var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
        //    if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
        //    {
        //        foreach (var teamorder in teamOrders.Data)
        //        {
        //            if (travellingStatusId.Contains(teamorder.StatusId))
        //            {
        //                return ProcessResultHelper.Failed<bool>(false, null, "You can't change order rank when technician has order with on travel or reached status.", ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        //            }
        //        }

        //        var result = manager.ChangeOrderRank(teamOrders.Data, model.Orders);
        //        if (result.IsSucceeded)
        //        {
        //            AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = true, OrderId = model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key };
        //            var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
        //        }

        //        return result;
        //    }

        //    return ProcessResultHelper.Failed<bool>(false, null, "something worng while getting team orders", ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        //}

        //
        [HttpGet]
        [Route("GetTeamOrders")]
        public ProcessResult<List<OrderViewModel>> GetTeamOrders([FromQuery] int teamId)
        {
            if (teamId > 0)
            {
                var orders = manager.GetAll(x => x.TeamId == teamId && x.AcceptanceFlag != AcceptenceType.Rejected && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId)).Data.OrderBy(x => x.RankInTeam).ToList();

                var result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders);

                return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "team id is required", ProcessResultStatusCode.Failed, "GetTeamOrders Controller");
        }

        [HttpGet]
        [Route("GetTeamOrdersForMobile")]
        public ProcessResult<List<TeamOrdersForMobileViewModel>> GetTeamOrdersForMobile([FromQuery] int teamId, [FromQuery]int PageNo, [FromQuery]int PageSize)
        {
            int pageIndex = 0;
            int pageCount = 10;

            try
            {
                pageIndex = (PageNo) == 0 ? 0 : (int)(PageNo - 1);
                pageCount = PageSize == 0 ? 10 : (int)(PageSize);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (teamId > 0)
            {
                var orders = manager.GetAllQuerable().Data.Where(x => x.TeamId == teamId && x.AcceptanceFlag != AcceptenceType.Rejected && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                orders = orders.OrderBy(x => x.RankInTeam).ThenBy(x => x.CreatedDate);
                var pagedOrders = orders.Skip(pageIndex * pageCount).Take(pageCount).ToList();
                var result = mapper.Map<List<OrderObject>, List<TeamOrdersForMobileViewModel>>(pagedOrders);
                return ProcessResultHelper.Succedded<List<TeamOrdersForMobileViewModel>>(result);

                // return ProcessResultHelper.Succedded<List<TeamOrdersForMobileViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<TeamOrdersForMobileViewModel>>(null, null, "team id is required", ProcessResultStatusCode.Failed, "GetTeamOrders Controller");
        }


        /* [Route("GetTeamOrders")]/*///{teamId}
        [HttpGet]
        [Route("GetTeamOrdersForWeb/{teamId}")]
        public ProcessResult<List<OrderViewModel>> GetTeamOrdersForWeb(int teamId)//[FromQuery]
        {
            if (teamId > 0)
            {
                var orders = manager.GetAll(x => x.TeamId == teamId && x.AcceptanceFlag != AcceptenceType.Rejected && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId)).Data.OrderBy(x => x.RankInTeam).ToList();

                var result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders);

                return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "team id is required", ProcessResultStatusCode.Failed, "GetTeamOrders Controller");
        }


        [HttpGet]
        [Route("GetAssignTypes")]
        public ProcessResult<List<EnumEntity>> GetAssignTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<AssignType>.GetEnumList());
        }

        [HttpGet]
        [Route("GetAcceptenceTypes")]
        public ProcessResult<List<EnumEntity>> GetAcceptenceTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<AcceptenceType>.GetEnumList());
        }

        [HttpGet]
        [Route("GetAccomplishTypes")]
        public ProcessResult<List<EnumEntity>> GetAccomplishTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<AccomplishType>.GetEnumList());
        }

        [HttpPost]
        [Route("Search")]
        public ProcessResult<List<OrderViewModel>> Search([FromBody] FilterOrderViewModel model)
        {

            if (model != null && model.Filters.Count > 0)
            {
                Expression<Func<OrderObject, bool>> predicateFilter = null;
                if (model != null)
                {
                    predicateFilter = CreateOrdersFilterPredicate(model.Filters, model.IncludeUnAssign);
                }

                var result = manager.GetAll(predicateFilter);
                if (result.IsSucceeded && result.Data.Count > 0)
                {
                    List<OrderViewModel> searchResult = mapper.Map<List<OrderObject>, List<OrderViewModel>>(result.Data);
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(searchResult);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(new List<OrderViewModel>());
                }
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "filter properties are required at least one property");
        }

        [HttpGet]
        [Route("GetUnAssignedOrdersForDispatcher")]
        [Authorize(Roles = StaticRoles.Supervisor)]
        //public ProcessResult<List<OrderViewModel>> GetUnAssignedOrdersForDispatcher([FromQuery] int dispatcherId)
        public async Task<ProcessResult<List<OrderCardViewModel>>> GetUnAssignedOrdersForDispatcher([FromQuery] int dispatcherId)
        {

            if (dispatcherId > 0)
            {
                var orders = await manager.GetAllAsync(x => x.DispatcherId == dispatcherId && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));

                if (orders.IsSucceeded && orders.Data.Any())
                {
                    List<OrderCardViewModel> result = mapper.Map<List<OrderObject>, List<OrderCardViewModel>>(orders.Data);
                    return ProcessResultHelper.Succedded<List<OrderCardViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<OrderCardViewModel>>(new List<OrderCardViewModel>());
                }
            }
            return ProcessResultHelper.Failed<List<OrderCardViewModel>>(null, null, "dispatcher id is required");
        }

        [HttpPost]
        [Route("GetUnAssignedOrdersForDispatcherPaginated")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        //public ProcessResult<PaginatedItemsViewModel<OrderViewModel>> GetUnAssignedOrdersForDispatcherPaginated([FromBody] PaginatedUnAssignedOrdersForDispatcherViewModel model)
        public async Task<ProcessResult<PaginatedItemsViewModel<OrderCardViewModel>>> GetUnAssignedOrdersForDispatcherPaginatedAsync([FromBody] PaginatedUnAssignedOrdersForDispatcherFilterViewModel model)
        {
            //var action = mapper.Map<PaginatedItemsViewModel<OrderCardViewModel>, PaginatedItems<OrderObject>>(model.PageInfo);
            //ProcessResult<PaginatedItems<OrderObject>> orders = new ProcessResult<PaginatedItems<OrderObject>>();
            //var predicate = PredicateBuilder.True<OrderObject>();

            //if (model.DispatcherId > 0)
            //{
            //    predicate = predicate.And(x => x.DispatcherId == model.DispatcherId && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));
            //    if (!string.IsNullOrEmpty(model.OrderCode))
            //    {
            //        predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            //    }

            //    orders = manager.GetAllPaginated(action, predicate,
            //        x => x.Problem,
            //        x => x.Division,
            //        x => x.BuildingType,
            //        x => x.ContractType,
            //        x => x.Supervisor,
            //        x => x.Supervisor.User,
            //        x => x.Dispatcher.Supervisor.User,
            //        x => x.Dispatcher.User,
            //        x => x.Team
            //    );
            //    var result = mapper.Map<PaginatedItems<OrderObject>, PaginatedItemsViewModel<OrderCardViewModel>>(orders.Data);
            //    return ProcessResultHelper.Succedded(result);
            //}
            // return ProcessResultHelper.Failed<PaginatedItemsViewModel<OrderCardViewModel>>(null, null, "dispatcher id is required");

            var result = await _orderService.GetUnAssignedOrdersForDisptcher(model);
            if (result != null)
            {
                return ProcessResultHelper.Succedded(result);
            }
            return ProcessResultHelper.Failed<PaginatedItemsViewModel<OrderCardViewModel>>(null, null, "dispatcher id is required");

        }
        [HttpPost]
        [Route("GetUnAssignedOrdersForDispatcherFilter")]
        public async Task<ProcessResult<PaginatedItemsViewModel<OrderCardViewModel>>> GetUnAssignedOrdersForDispatcherFilter([FromBody] PaginatedUnAssignedOrdersForDispatcherFilterViewModel model)
        {
            //var action = mapper.Map<PaginatedItemsViewModel<OrderCardViewModel>, PaginatedItems<OrderObject>>(model.PageData);
            //ProcessResult<PaginatedItems<OrderObject>> orders = new ProcessResult<PaginatedItems<OrderObject>>();

            //if (model.DispatcherId > 0)
            //{
            //    var predicate = PredicateBuilder.True<OrderObject>();

            //    List<int> unassignedOrderStatusId = StaticAppSettings.UnassignedStatusId;

            //    if (model.OrderCode != null && model.OrderCode != "")
            //    {
            //        predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            //    }

            //    if (model.CustomerCode != null && model.CustomerCode != "")
            //    {
            //        predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
            //    }

            //    if (model.CustomerName != null && model.CustomerName != "")
            //    {
            //        predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
            //    }

            //    if (model.CustomerPhone != null && model.CustomerPhone != "")
            //    {
            //        predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
            //    }

            //    if (model.AreaIds != null && model.AreaIds.Any())
            //    {
            //        predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
            //    }

            //    if (model.BlockName != null && model.BlockName != "")
            //    {
            //        predicate = predicate.And(r => r.BlockName.Contains(model.BlockName));
            //    }

            //    if (model.ProblemIds != null && model.ProblemIds.Any())
            //    {
            //        predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
            //    }

            //    if (model.StatusIds != null && model.StatusIds.Any())
            //    {
            //        predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
            //    }

            //    if (model.Rejection == (int)RejectionEnum.NoAction)
            //    {
            //        predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
            //    }
            //    else if (model.Rejection == (int)RejectionEnum.Rejected)
            //    {
            //        predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
            //    }
            //    else if (model.Rejection == (int)RejectionEnum.Accepted)
            //    {
            //        predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
            //    }
            //    else if (model.Rejection == (int)RejectionEnum.All)
            //    {
            //        predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
            //    }

            //    if (model.DateFrom != null && model.DateTo != null)
            //    {
            //        predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
            //    }
            //    else if (model.DateFrom == null && model.DateTo == null)
            //    {
            //        predicate = predicate.And(x => true);
            //    }
            //    else if (model.DateFrom == null)
            //    {
            //        predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
            //    }
            //    else if (model.DateTo == null)
            //    {
            //        predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
            //    }

            //    predicate = predicate.And(r => unassignedOrderStatusId.Contains(r.StatusId));
            //    predicate = predicate.And(r => r.DispatcherId == model.DispatcherId);

            //    orders = manager.GetAllPaginated(action, predicate, x => x.Status);

            //    var result = mapper.Map<PaginatedItems<OrderObject>, PaginatedItemsViewModel<OrderCardViewModel>>(orders.Data);
            //    return ProcessResultHelper.Succedded(result);

            //}
            //return ProcessResultHelper.Failed<PaginatedItemsViewModel<OrderCardViewModel>>(null, null, "dispatcher id is required");
            var result = await _orderService.GetUnAssignedOrdersForDisptcher(model);
            if (result != null)
            {
                return ProcessResultHelper.Succedded(result);
            }
            return ProcessResultHelper.Failed<PaginatedItemsViewModel<OrderCardViewModel>>(null, null, "dispatcher id is required");
        }
        [HttpPost]
        [Route("GetAllOrdersForDispatcherFilter")]
        public async Task<ProcessResult<Ghanim.API.Models.DispatcherAllOrdersViewModel>> GetAllOrdersForDispatcherFilter([FromBody] PaginatedUnAssignedOrdersForDispatcherFilterViewModel model)
        {

            DispatcherAllOrdersViewModel returendData = new DispatcherAllOrdersViewModel();
            if (model.DispatcherId != 0)
            {
                returendData.UnAssignedOrders = await _orderService.GetUnAssignedOrdersForDisptcher(model);
                returendData.AssignedOrders = await _orderService.GetAssignedOrdersForDisptcher(model);
            }

            if (returendData != null)
            {
                return ProcessResultHelper.Succedded(returendData);
            }
            return null;
        }

        [HttpGet]
        [Route("GetUnAssignedOrdersForSupervisor")]
        public async Task<ProcessResult<List<OrderCardViewModel>>> GetUnAssignedOrdersForSupervisor([FromQuery] int supervisrId)
        {

            if (supervisrId > 0)
            {
                var orders = await manager.GetAllAsync(x => x.SupervisorId == supervisrId && x.DispatcherId == null && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderCardViewModel> result = mapper.Map<List<OrderObject>, List<OrderCardViewModel>>(orders.Data);
                    return ProcessResultHelper.Succedded<List<OrderCardViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<OrderCardViewModel>>(new List<OrderCardViewModel>());
                }
            }
            return ProcessResultHelper.Failed<List<OrderCardViewModel>>(null, null, "supervisor id is required");
        }

        [HttpPost]
        [Route("GetUnAssignedOrdersForSupervisorPaginated")]
        public async Task<ProcessResult<PaginatedItemsViewModel<OrderCardViewModel>>> GetUnAssignedOrdersForSupervisorPaginated([FromBody] PaginatedUnAssignedOrdersForSupervisorViewModel model)
        {
            var action = mapper.Map<PaginatedItemsViewModel<OrderCardViewModel>, PaginatedItems<OrderObject>>(model.PageData);
            ProcessResult<PaginatedItems<OrderObject>> orders = new ProcessResult<PaginatedItems<OrderObject>>();

            if (model.SupervisrId > 0)
            {
                if (!string.IsNullOrEmpty(model.OrderCode))
                {
                    orders = manager.GetAllPaginated(action, x => x.SupervisorId == model.SupervisrId && x.DispatcherId == null && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId) && x.Code == model.OrderCode);
                }
                else
                {
                    orders = manager.GetAllPaginated(action, x => x.SupervisorId == model.SupervisrId && x.DispatcherId == null && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));
                }
                var result = mapper.Map<PaginatedItems<OrderObject>, PaginatedItemsViewModel<OrderCardViewModel>>(orders.Data);
                return ProcessResultHelper.Succedded(result);
            }
            return ProcessResultHelper.Failed<PaginatedItemsViewModel<OrderCardViewModel>>(null, null, "supervisor id is required");
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Supervisor)]
        [Route("GetUnAssignedOrdersForSupervisorFilter")]
        public async Task<ProcessResult<PaginatedItemsViewModel<OrderCardViewModel>>> GetUnAssignedOrdersForSupervisorFilter([FromBody] PaginatedUnAssignedOrdersForSupervisorFilterViewModel model)
        {
            int pageindex = 1;
            int pageSize = 10;
            if (model.PageInfo.PageNo > 0)
                pageindex = model.PageInfo.PageNo - 1;
            if (model.PageInfo.PageSize > 0)
                pageSize = model.PageInfo.PageSize;

            var skipCount = pageindex * pageSize;
    


            var action = mapper.Map<PaginatedItemsViewModel<OrderCardViewModel>, PaginatedItems<OrderObject>>(model.PageInfo);
            ProcessResult<PaginatedItems<OrderObject>> orders = new ProcessResult<PaginatedItems<OrderObject>>();

            if (model.SupervisorId > 0)
            {
                var predicate = PredicateBuilder.True<OrderObject>();

                List<int> unassignedOrderStatusId = StaticAppSettings.UnassignedStatusId;

                if (model.OrderCode != null && model.OrderCode != "")
                {
                    predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
                }

                if (model.CustomerCode != null && model.CustomerCode != "")
                {
                    predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
                }

                if (model.CustomerName != null && model.CustomerName != "")
                {
                    predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
                }

                if (model.CustomerPhone != null && model.CustomerPhone != "")
                {
                    predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
                }

                if (model.AreaIds != null && model.AreaIds.Any())
                {
                    predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
                }

                if (model.BlockName != null && model.BlockName != "")
                {
                    predicate = predicate.And(r => r.SAP_BlockName.Contains(model.BlockName));
                }

                if (model.ProblemIds != null && model.ProblemIds.Any())
                {
                    predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
                }

                if (model.StatusIds != null && model.StatusIds.Any())
                {
                    predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
                }

                if (model.Rejection == (int)RejectionEnum.NoAction)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
                }
                else if (model.Rejection == (int)RejectionEnum.Rejected)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
                }
                else if (model.Rejection == (int)RejectionEnum.Accepted)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
                }
                else if (model.Rejection == (int)RejectionEnum.All)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
                }

                if (model.DateFrom != null && model.DateTo != null)
                {
                    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
                }
                else if (model.DateFrom == null && model.DateTo == null)
                {
                    predicate = predicate.And(x => true);
                }
                else if (model.DateFrom == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
                }
                else if (model.DateTo == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
                }

                predicate = predicate.And(r => r.DispatcherId == null);
                predicate = predicate.And(r => r.SupervisorId == model.SupervisorId);


              var query =  manager.GetAllQuerable().Data.Where(predicate).OrderByDescending(x => x.Id).Skip(skipCount) .Take(pageSize);

              var OrderCardViewModel =await query.ProjectTo<OrderCardViewModel>().ToListAsync();

              var res = new PaginatedItemsViewModel<OrderCardViewModel>()
              {
                  PageNo = model.PageInfo.PageNo+1,
                  PageSize = model.PageInfo.PageSize,
                  Data = OrderCardViewModel
              };

                return ProcessResultHelper.Succedded(res);
            }
            return ProcessResultHelper.Failed<PaginatedItemsViewModel<OrderCardViewModel>>(null, null, "supervisor id is required");
        }

        [HttpGet]
        [Route("GetAssignedOrdersForDispatcher")]
        [Authorize(Roles = StaticRoles.Supervisor)]
        public async Task<ProcessResult<List<DispatcherAssignedOrdersViewModel>>> GetAssignedOrdersForDispatcher([FromQuery] int dispatcherId)
        {
            var TeamsOrders = new List<TeamOrdersViewModel>();
            if (dispatcherId > 0)
            {
                List<int> deactiveStatusesId = new List<int>() { StaticAppSettings.InitialStatus.Id };
                deactiveStatusesId.AddRange(StaticAppSettings.ArchivedStatusId);

                var orders = await manager.GetAllAsync(x => x.DispatcherId == dispatcherId && x.TeamId > 0 && !deactiveStatusesId.Contains(x.StatusId));

                // var dispatcherTeams = await  teamService.GetTeamsByDispatcherId(dispatcherId);
                var getTeamsWithOutTracking = await _context.Teams.AsNoTracking().Where(x => x.DispatcherId == dispatcherId && x.Members.Any()).ToListAsync();
                var mapTeamList = mapper.Map<List<Team>, List<TeamViewModel>>(getTeamsWithOutTracking);
                var dispatcherTeams = ProcessResultViewModelHelper.Succedded<List<TeamViewModel>>(mapTeamList, "");

                var foreman = foremanService.GetForemanByIds(dispatcherTeams.Data.Select(x => x.ForemanId).ToList());
                var identity = await identityService.GetByUserIds(foreman.Data.Select(x => x.UserId).ToList());

                var currentTeams = (from disp in dispatcherTeams.Data
                                    join foremn in foreman.Data on disp.ForemanId equals foremn.Id
                                    join id in identity.Data on foremn.UserId equals id.Id
                                    select new TeamOrdersViewModel
                                    {
                                        ForemanId = foremn.Id,
                                        ForemanName = id.FirstName,
                                        TeamId = disp.Id,
                                        TeamName = disp.Name,
                                        Phone = id.Phone,
                                        StatusId = disp.StatusId,
                                        StatusName = disp.StatusName
                                    }).ToList();

                //var currentTeams = mapper.Map<List<TeamViewModel>, List<TeamOrdersViewModel>>(dispatcherTeams.Data);

                if (orders.IsSucceeded)
                {
                    if (orders.Data.Any())
                    {
                        List<OrderCardViewModel> result = mapper.Map<List<OrderObject>, List<OrderCardViewModel>>(orders.Data);
                        var groups = result.GroupBy(x => x.TeamId, (key, group) => new TeamOrdersViewModel { TeamId = key, Orders = group.OrderBy(x => x.RankInTeam.Value) }).ToList();

                        if (groups.Any() && dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                        {
                            if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                            {

                                for (int i = 0; i < currentTeams.Count; i++)
                                {
                                    currentTeams[i].Orders = new List<OrderCardViewModel>();
                                    //currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                                    //currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
                                    for (int j = 0; j < groups.Count; j++)
                                    {
                                        if (currentTeams[i].TeamId == groups[j].TeamId)
                                        {
                                            currentTeams[i].Orders = groups[j].Orders;
                                        }
                                    }

                                }
                            }
                            //for (int i = 0; i < currentTeams.Count; i++)
                            //{
                            //    currentTeams[i].Orders = new List<OrderCardViewModel>();
                            //    //currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                            //    //currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
                            //    for (int j = 0; j < groups.Count; j++)
                            //    {
                            //        if (currentTeams[i].TeamId == groups[j].TeamId)
                            //        {
                            //            currentTeams[i].Orders = groups[j].Orders;
                            //        }
                            //    }
                            //}


                        }
                        // here add foreman group binding
                        var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName, x.Phone }, (key, group) => new DispatcherAssignedOrdersViewModel
                        {
                            ForemanId = key.ForemanId,
                            ForemanName = key.ForemanName,
                            ForemanPhone = key.Phone,
                            Teams = group
                        }).ToList();
                        return ProcessResultHelper.Succedded<List<DispatcherAssignedOrdersViewModel>>(newGroup);
                    }
                    else
                    {
                        if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
                        {

                            for (int i = 0; i < currentTeams.Count; i++)
                            {
                                currentTeams[i].Orders = new List<OrderCardViewModel>();
                                //currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
                                //currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
                            }
                        }
                        // here add foreman group binding
                        var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName, x.Phone }, (key, group) => new DispatcherAssignedOrdersViewModel
                        {
                            ForemanId = key.ForemanId,
                            ForemanName = key.ForemanName,
                            ForemanPhone = key.Phone,
                            Teams = group
                        }).ToList();
                        return ProcessResultHelper.Succedded<List<DispatcherAssignedOrdersViewModel>>(newGroup);
                    }
                }
                return ProcessResultHelper.Succedded<List<DispatcherAssignedOrdersViewModel>>(new List<DispatcherAssignedOrdersViewModel>());
            }
            return ProcessResultHelper.Failed<List<DispatcherAssignedOrdersViewModel>>(null, null, "dispatcher id is required");
        }


        [HttpPost]
        [Authorize(Roles = StaticRoles.Supervisor)]
        [Route("GetAssignedOrdersForDispatchersBySupervisorFilter")]
        public async Task<ProcessResult<List<DispatchersUnAssignedOrdersViewModel>>> GetAssignedOrdersForDispatchersBySupervisorFilter([FromBody] PaginatedUnAssignedOrdersForSupervisorFilterViewModel model)
        {
            if (model.SupervisorId > 0)
            {
                var dispatchers = _context.Dispatchers.Where(x => !x.IsDeleted && x.SupervisorId == model.SupervisorId);   //await dispatcherService.GetDispatchersBySupervisorId(model.SupervisorId);
                // var dd = _context.Dispatchers.Select(x => x.SupervisorId = model.SupervisorId);
                var dispatcherIds = dispatchers.Select(x => x.Id);
                var predicate = PredicateBuilder.True<OrderObject>();

                if (model.OrderCode != null && model.OrderCode != "")
                {
                    predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
                }

                if (model.CustomerCode != null && model.CustomerCode != "")
                {
                    predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
                }

                if (model.CustomerName != null && model.CustomerName != "")
                {
                    predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
                }

                if (model.CustomerPhone != null && model.CustomerPhone != "")
                {
                    predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
                }

                if (model.AreaIds != null && model.AreaIds.Any())
                {
                    predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
                }

                if (model.BlockName != null && model.BlockName != "")
                {
                    predicate = predicate.And(r => r.SAP_BlockName.Contains(model.BlockName));
                }

                if (model.ProblemIds != null && model.ProblemIds.Any())
                {
                    predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
                }

                if (model.StatusIds != null && model.StatusIds.Any())
                {
                    predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
                }

                if (model.Rejection == (int)RejectionEnum.NoAction)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
                }
                else if (model.Rejection == (int)RejectionEnum.Rejected)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
                }
                else if (model.Rejection == (int)RejectionEnum.Accepted)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
                }
                else if (model.Rejection == (int)RejectionEnum.All)
                {
                    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
                }

                if (model.DateFrom != null && model.DateTo != null)
                {
                    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
                }
                else if (model.DateFrom == null && model.DateTo == null)
                {
                    predicate = predicate.And(x => true);
                }
                else if (model.DateFrom == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
                }
                else if (model.DateTo == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
                }

                predicate = predicate.And(r => r.StatusId == StaticAppSettings.InitialStatus.Id || r.StatusId == StaticAppSettings.OnHoldStatus.Id);
                predicate = predicate.And(r => dispatcherIds.Contains(r.DispatcherId ?? 0));//TODO Refactor issue


                IQueryable<OrderObject> ordersQuarable = _context.Orders.Where(predicate);

                var result = new List<DispatchersUnAssignedOrdersViewModel>();


                foreach (var disp in dispatchers)
                {
                    List<OrderObject> ordersList = new List<OrderObject>();
                    if (model.PageInfo != null)
                    {
                        //ordersList = ordersQuarable.Where(x => x.DispatcherId == disp.Id)
                        //.Take(model.PageInfo.PageSize)
                        //.ToList();

                        ordersList = ordersQuarable.Where(x => x.DispatcherId == disp.Id)
                       .OrderByDescending(o => o.Id)
                       .Take(model.PageInfo.PageSize)
                       .ToList();
                    }

                    var item = new DispatchersUnAssignedOrdersViewModel()
                    {
                        DispatcherId = disp.Id,
                        DispatcherName = disp.User.FullName,
                        DispatcherPhone = disp.User.Phone1,
                        Orders = mapper.Map<List<OrderObject>, List<OrderCardViewModel>>(ordersList)
                    };
                    result.Add(item);
                }

                //List<OrderViewModel> model = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                //var groups = model.GroupBy(x => x.DispatcherId, (key, group) => new DispatchersUnAssignedOrdersViewModel { DispatcherId = key, Orders = group }).ToList();

                //for (int i = 0; i < result.Count; i++)
                //{
                //    result[i].Orders = new List<OrderViewModel>();
                //    for (int j = 0; j < groups.Count; j++)
                //    {
                //        if (model[i].DispatcherId == groups[j].DispatcherId)
                //        {
                //            result[i].Orders = groups[j].Orders;
                //        }
                //    }
                //}

                return ProcessResultHelper.Succedded<List<DispatchersUnAssignedOrdersViewModel>>(result);

            }
            return ProcessResultHelper.Failed<List<DispatchersUnAssignedOrdersViewModel>>(null, null, "supervisor id is required");
        }

        [HttpGet]
        [Route("GetMapOrdersForDispatcher")]
        public async Task<ProcessResult<List<OrderViewModel>>> GetMapOrdersForDispatcher([FromQuery] int dispatcherId)
        {
            if (dispatcherId > 0)
            {
                List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;

                var orders = await manager.GetAllAsync(x => x.DispatcherId == dispatcherId && x.Long > 0 && x.Lat > 0 && !deactiveStatusesId.Contains(x.StatusId));
                if (orders.IsSucceeded)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Failed<List<OrderViewModel>>(null, orders.Exception, "Someting worng while getting orders");
                }
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "dispatcher id is required");
        }

        [HttpPost]
        [Route("SetWorkingAction")]
        public async Task<ProcessResultViewModel<bool>> SetWorkingAction([FromBody]WorkingViewModel model)
        {
            return await _orderService.SetWorkingAction(model, Request);
        }

        [HttpPost]
        [Route("SetWorkingState")]// For Technician
        public async Task<ProcessResultViewModel<bool>> SetWorkingState([FromBody]WorkingViewModel model)
        {
            int BaseWorkingTypeId = (int)model.WorkingTypeId;
            string BaseWorkingTypeName = model.WorkingTypeName;
            var technicianState = mapper.Map<WorkingViewModel, TechniciansStateViewModel>(model);
            if (model.State == TechnicianState.Working && (model.OrderId == 0 || model.OrderId == null))
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "Order id is required to change team state to working");
            }
            //TimeSheetType
            else if (model.WorkingTypeId == (int)WorkingTypeEnum.RejectFirstOrder && string.IsNullOrEmpty(model.RejectionReason))
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, "Rejection reason is required to reject the order");
            }
            var actionType = OrderActionType.Work;
            if (model.Type == TimeSheetType.Actual || model.Type == TimeSheetType.Traveling)
            {
                actionType = OrderActionType.Work;
                //model.WorkingTypeName = "Actual";
                //model.WorkingTypeId = 1;
            }
            else if (model.Type == TimeSheetType.Break)
            {
                actionType = OrderActionType.Break;
                model.WorkingTypeName = WorkingTypeEnum.Break.ToString();
                model.WorkingTypeId = (int)WorkingTypeEnum.Break;
                if (actionType == OrderActionType.Break && string.IsNullOrEmpty(model.RejectionReason))
                {
                    model.WorkingSubReasonId = BaseWorkingTypeId;
                    model.WorkingSubReason = BaseWorkingTypeName;
                }

            }
            else if (model.Type == TimeSheetType.Idle)
            {
                actionType = OrderActionType.Idle;
                model.WorkingTypeName = WorkingTypeEnum.Idle.ToString(); //"Idle";
                model.WorkingTypeId = (int)WorkingTypeEnum.Idle;
                if (actionType == OrderActionType.Idle && string.IsNullOrEmpty(model.RejectionReason))
                {
                    model.WorkingSubReasonId = BaseWorkingTypeId;
                    model.WorkingSubReason = BaseWorkingTypeName;
                }

            }

            OrderObject orderObject = new OrderObject();
            if (model.OrderId != null && model.OrderId > 0)
            {
                var order = Get((int)model.OrderId).Data;
                orderObject = Mapper.Map<OrderObject>(order);

                if (order.TeamId == null)
                    return ProcessResultViewModelHelper.Failed(false, "This Order Must Has A Team");
            }



            if (model.State == TechnicianState.Working)
            {
                //Here Test Again
                int acceptValue1 = StaticAppSettings.WorkingTypes[0].Id;
                int acceptValue2 = StaticAppSettings.WorkingTypes[3].Id;
                int rejectValue = StaticAppSettings.WorkingTypes[1].Id;
                int continueValue = StaticAppSettings.WorkingTypes[2].Id;

                if (acceptValue1 == model.WorkingTypeId || acceptValue2 == model.WorkingTypeId)
                {
                    // accept order and set on travel
                    var acceptAndSetTravelling = manager.AcceptOrderAndSetOnTravel((int)model.OrderId, StaticAppSettings.TravellingStatusName[0], StaticAppSettings.TravellingStatusId[0], StaticAppSettings.OnTravelSubStatus.Name, StaticAppSettings.OnTravelSubStatus.Id);
                    if (!(acceptAndSetTravelling.IsSucceeded && acceptAndSetTravelling.Data == true))
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, acceptAndSetTravelling.Status.Message);
                    }
                    else
                    {
                        try
                        {
                            // notify dispatcher with the order changes
                            ChangeStatusViewModel changeStatusView = new ChangeStatusViewModel()
                            {
                                OrderId = orderObject.Id,
                                StatusId = orderObject.StatusId,
                                //StatusName = orderObject.OrderStatus.Name,
                                SubStatusId = orderObject.SubStatusId,
                                SubStatusName = orderObject.SubStatus?.Name
                            };
                            var notificationRes = await NotifyDispatcherWithNewStatus(changeStatusView);

                            // send SMS
                            string msgContent = string.Format(StaticAppSettings.SMSContent, orderObject.Code, orderObject.Status?.Name ?? "");
                            var sms = new SMSViewModel()
                            {
                                PhoneNumber = orderObject.PhoneOne,
                                Message = msgContent
                            };
                            //  var smsResult = await smsService.Send(sms);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            //throw;
                        }
                    }
                }
                else if (rejectValue == model.WorkingTypeId)
                {
                    try
                    {
                        // make order as rejected
                        AcceptenceViewModel acceptenceViewModel = new AcceptenceViewModel()
                        {
                            AcceptenceFlag = false,
                            OrderId = (int)model.OrderId,
                            RejectionReason = model.RejectionReason,
                            RejectionReasonId = model.RejectionReasonId
                        };
                        var acceptenceResult = await _orderService.ChangeAcceptence(acceptenceViewModel, Request);
                        if (!(acceptenceResult.IsSucceeded && acceptenceResult.Data == true))
                        {
                            return ProcessResultViewModelHelper.Failed<bool>(false, acceptenceResult.Status.Message);
                        }
                        else
                        {
                            var notificationRes = await NotifyDispatcherWithOrderAcceptence(acceptenceViewModel);
                            // return ProcessResultViewModelHelper.Succedded<bool>(true, acceptenceResult.Status.Message);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        //throw;
                    }
                }
                else if (continueValue == model.WorkingTypeId)
                {

                }


            }
            ProcessResult<List<OrderAction>> addedOrderAction = new ProcessResult<List<OrderAction>>();
            if ((model?.OrderId ?? 0) > 0)
            {
                var orderObjectClone = (await _context.Orders.FirstOrDefaultAsync(x => x.Id == model.OrderId)).Clone();
                addedOrderAction = await _orderService.AddAction(orderObjectClone, Request, actionType, model.WorkingTypeId, model.WorkingTypeName, model.TeamId, null, model.WorkingSubReasonId, model.WorkingSubReason, model.RejectionReason, platformTypeSource: PlatformType.Mobile);
            }
            else
            {
                addedOrderAction = await _orderService.AddAction(null, Request, actionType, model.WorkingTypeId, model.WorkingTypeName, model.TeamId, null, model.WorkingSubReasonId, model.WorkingSubReason, model.RejectionReason, platformTypeSource: PlatformType.Mobile);
            }

            if (addedOrderAction.IsSucceeded)
            {
                //  var technicianState = mapper.Map<WorkingViewModel, TechniciansStateViewModel>(model);
                var result = technicianStateService.Add(technicianState);
                if (result.IsSucceeded == true && result.Data != null)
                {
                    return ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, result.Status.Message);
                }
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, addedOrderAction.Status.Message);
            }
        }

        [HttpGet]
        [Route("GetOrderActionTypes")]
        public ProcessResult<List<EnumEntity>> GetOrderActionTypes()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<OrderActionType>.GetEnumList());
        }
        [HttpPost]
        [Route("OrderFilterMap")]
        [Authorize(Roles = StaticRoles.Dispatcher)]
        public async Task<ActionResult<List<string>>> GetOrdersForMap([FromBody]OrdersMapBoundaryViewModel model)
        {
            var predicate = PredicateBuilder.True<OrderObject>();
            List<int> unassignedOrderStatusId = StaticAppSettings.UnassignedStatusId;
            List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
            List<int> unassignedStatusesId = StaticAppSettings.UnassignedStatusId;

            if (model.AreaId != null && model.AreaId.Any())
            {
                predicate = predicate.And(r => model.AreaId.Contains(r.AreaId));
            }
            if (model.DispatcherId != null && model.DispatcherId.Any())
            {
                predicate = predicate.And(r => model.DispatcherId.Contains(r.DispatcherId ?? 0));//TODO Refactor issue
            }
            if (model.DivisionId != null && model.DivisionId.Any())
            {
                predicate = predicate.And(r => model.DivisionId.Contains(r.DivisionId ?? 0));//TODO Refactor issue
            }
            if (model.OrderTypeId != null && model.OrderTypeId.Any())
            {
                predicate = predicate.And(r => r.TypeId != null && model.OrderTypeId.Contains(r.TypeId.Value));
            }
            if (model.ProblemId != null && model.ProblemId.Any())
            {
                predicate = predicate.And(r => model.ProblemId.Contains(r.ProblemId ?? 0));//TODO Refactor issue
            }
            if (model.Assigned == (int)OrderStatusEnumForFilterInMap.Assigned)
            {
                predicate = predicate.And(r => !deactiveStatusesId.Contains(r.StatusId) && !unassignedStatusesId.Contains(r.StatusId));
            }
            else if (model.Assigned == (int)OrderStatusEnumForFilterInMap.Unassigned)
            {
                predicate = predicate.And(r => unassignedOrderStatusId.Contains(r.StatusId));
            }


            if (model.StatusId != null && model.StatusId.Any())
            {
                predicate = predicate.And(r => model.StatusId.Contains(r.StatusId));
            }

            if (model.TeamId != null && model.TeamId.Any())
                predicate = predicate.And(r => model.TeamId.Contains(r.TeamId ?? 0) && r.TeamId != 0);//TODO Refactor issue

            if (model.DateFrom != null && model.DateTo != null)
                predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));

            else if (model.DateFrom == null && model.DateTo == null)
                predicate = predicate.And(x => true);
            else if (model.DateFrom == null)
                predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);


            if (model.SouthWest != null && model.NorthEast != null)
            {
                var coordiantes = new List<Coordinate>() { model.SouthWest, model.NorthEast };
                predicate = predicate.And(x => isWithin((decimal)x.Lat, (decimal)x.Long, model.SouthWest, model.NorthEast) == true);
            }
            else
                predicate = predicate.And(x => x.Lat != 0 && x.Long != 0);

            Stopwatch sw1 = new Stopwatch();
            sw1.Start();
            var dtoOrders = _context.Orders.Where(predicate).Select(o => new
            {
                o.OnHoldCount,
                o.Code,
                o.Lat,
                o.Long,
                TypeName = o.Type.Name,
                o.SAP_CreatedDate,
                o.ProblemId
            }).ToList();




            //var ordersModel = processResultMapper.Map<List<OrderObject>, List<OrdersMapViewModel>>(paginatedRes);

            //Parallel.ForEach(ordersModel.Data, order =>
            //{
            //    order.Pin = manager.ChoosePin(order.OnHoldCount, order.TypeName, order.SAP_CreatedDate, order.ProblemName);
            //});


            var result = dtoOrders.Select(o => new OrdersMapViewCustomModel
            {
                OnHoldCount = o.OnHoldCount,
                Code = o.Code,
                Lat = o.Lat,
                Long = o.Long,
                Pin = manager.ChoosePin(o.OnHoldCount, o.TypeName, o.SAP_CreatedDate, o.ProblemId)
            }).ToList();
            sw1.Stop();
            var elapsed = sw1.ElapsedMilliseconds;
            return Ok(result);
        }
        private bool isWithin(decimal Latitude, decimal Longitude, Coordinate sw, Coordinate ne)
        {
            return Latitude >= sw.Latitude &&
                   Latitude <= ne.Latitude &&
                   Longitude >= sw.Longitude &&
                   Longitude <= ne.Longitude;
        }


        [HttpPost]
        [Route("UnassignedOrdersBoardFilter")]
        public async Task<ProcessResultViewModel<List<OrderCardViewModel>>> UnassignedOrdersBoardFilter([FromBody]OrdersboardFilterViewModel model)
        {

            model = model ?? new OrdersboardFilterViewModel();

            var predicate = PredicateBuilder.True<OrderObject>();

            List<int> unassignedOrderStatusId = StaticAppSettings.UnassignedStatusId;

            if (model.OrderCode != null && model.OrderCode != "")
            {
                predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            }

            if (model.CustomerCode != null && model.CustomerCode != "")
            {
                predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
            }

            if (model.CustomerName != null && model.CustomerName != "")
            {
                predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
            }

            if (model.CustomerPhone != null && model.CustomerPhone != "")
            {
                predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
            }

            if (model.AreaIds != null && model.AreaIds.Any())
            {
                predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
            }

            if (model.BlockName != null && model.BlockName != "")
            {
                predicate = predicate.And(r => r.SAP_BlockName.Contains(model.BlockName));
            }

            if (model.ProblemIds != null && model.ProblemIds.Any())
            {
                predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
            }

            if (model.StatusIds != null && model.StatusIds.Any())
            {
                predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
            }

            if (model.Rejection == (int)RejectionEnum.NoAction)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
            }
            else if (model.Rejection == (int)RejectionEnum.Rejected)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
            }
            else if (model.Rejection == (int)RejectionEnum.Accepted)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
            }
            else if (model.Rejection == (int)RejectionEnum.All)
            {
                predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
            }

            if (model.DateFrom != null && model.DateTo != null)
            {
                predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
            }
            else if (model.DateFrom == null && model.DateTo == null)
            {
                predicate = predicate.And(x => true);
            }
            else if (model.DateFrom == null)
            {
                predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
            }
            else if (model.DateTo == null)
            {
                predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
            }

            predicate = predicate.And(r => unassignedOrderStatusId.Contains(r.StatusId));
            predicate = predicate.And(r => r.DispatcherId == model.DispatcherId);

            var paginatedRes = await manager.GetAllAsync(predicate);

            return processResultMapper.Map<List<OrderObject>, List<OrderCardViewModel>>(paginatedRes);

        }

        [HttpPost]
        [Route("assignedOrdersBoardFilter")]
        public async Task<ProcessResult<List<DispatcherAssignedOrdersViewModel>>> assignedOrdersBoardFilter([FromBody]PaginatedUnAssignedOrdersForDispatcherFilterViewModel model)
        {
            //var predicate = PredicateBuilder.True<OrderObject>();
            //List<int> deactiveStatusesId = StaticAppSettings.ArchivedStatusId;
            //List<int> unassignedStatusesId = StaticAppSettings.UnassignedStatusId;

            //if (model.OrderCode != null && model.OrderCode != "")
            //{
            //    predicate = predicate.And(r => r.Code.Contains(model.OrderCode));
            //}

            //if (model.CustomerCode != null && model.CustomerCode != "")
            //{
            //    predicate = predicate.And(r => r.CustomerCode.Contains(model.CustomerCode));
            //}

            //if (model.CustomerName != null && model.CustomerName != "")
            //{
            //    predicate = predicate.And(r => r.CustomerName.Contains(model.CustomerName));
            //}

            //if (model.CustomerPhone != null && model.CustomerPhone != "")
            //{
            //    predicate = predicate.And(r => r.PhoneOne.Contains(model.CustomerPhone) || r.PhoneTwo.Contains(model.CustomerPhone) || r.Caller_ID.Contains(model.CustomerPhone));
            //}

            //if (model.AreaIds != null && model.AreaIds.Any())
            //{
            //    predicate = predicate.And(r => model.AreaIds.Contains(r.AreaId));
            //}

            //if (model.BlockName != null && model.BlockName != "")
            //{
            //    predicate = predicate.And(r => r.SAP_BlockName.Contains(model.BlockName));
            //}

            //if (model.ProblemIds != null && model.ProblemIds.Any())
            //{
            //    predicate = predicate.And(r => model.ProblemIds.Contains(r.ProblemId ?? 0));//TODO Refactor issue
            //}

            //if (model.StatusIds != null && model.StatusIds.Any())
            //{
            //    predicate = predicate.And(r => model.StatusIds.Contains(r.StatusId));
            //}

            //if (model.Rejection == (int)RejectionEnum.NoAction)
            //{
            //    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.NoAction);
            //}
            //else if (model.Rejection == (int)RejectionEnum.Rejected)
            //{
            //    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected);
            //}
            //else if (model.Rejection == (int)RejectionEnum.Accepted)
            //{
            //    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Accepted);
            //}
            //else if (model.Rejection == (int)RejectionEnum.All)
            //{
            //    predicate = predicate.And(r => r.AcceptanceFlag == AcceptenceType.Rejected || r.AcceptanceFlag == AcceptenceType.Accepted || r.AcceptanceFlag == AcceptenceType.NoAction);
            //}

            //if (model.DateFrom != null && model.DateTo != null)
            //{
            //    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
            //}
            //else if (model.DateFrom == null && model.DateTo == null)
            //{
            //    predicate = predicate.And(x => true);
            //}
            //else if (model.DateFrom == null)
            //{
            //    predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
            //}
            //else if (model.DateTo == null)
            //{
            //    predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
            //}

            //predicate = predicate.And(r => !deactiveStatusesId.Contains(r.StatusId) && !unassignedStatusesId.Contains(r.StatusId));
            //predicate = predicate.And(r => r.DispatcherId == model.DispatcherId);

            //var ordersRes = await manager.GetAllAsync(predicate);
            //// hayam

            //var getTeamsWithOutTracking = await _context.Teams.AsNoTracking().Where(x => x.DispatcherId == model.DispatcherId && x.Members.Any()).ToListAsync();
            //var mapTeamList = mapper.Map<List<Team>, List<TeamViewModel>>(getTeamsWithOutTracking);
            //var dispatcherTeams = ProcessResultViewModelHelper.Succedded<List<TeamViewModel>>(mapTeamList, "");
            //// var dispatcherTeams = await  teamService.GetTeamsByDispatcherId(model.DispatcherId);
            //var foreman = foremanService.GetForemanByIds(dispatcherTeams.Data.Select(x => x.ForemanId).ToList());
            //var identity = await identityService.GetByUserIds(foreman.Data.Select(x => x.UserId).ToList());

            //List<TeamOrdersViewModel> currentTeams = (from disp in dispatcherTeams.Data
            //                                          join foremn in foreman.Data on disp.ForemanId equals foremn.Id
            //                                          join id in identity.Data on foremn.UserId equals id.Id
            //                                          select new TeamOrdersViewModel
            //                                          {
            //                                              ForemanId = foremn.Id,
            //                                              ForemanName = id.FirstName,
            //                                              TeamId = disp.Id,
            //                                              TeamName = disp.Name,
            //                                              Phone = id.Phone,
            //                                              StatusId = disp.StatusId,
            //                                              StatusName = disp.StatusName
            //                                          }).ToList();



            //if (ordersRes.IsSucceeded)
            //{
            //    if (ordersRes.Data.Any())
            //    {
            //        List<OrderCardViewModel> result = mapper.Map<List<OrderObject>, List<OrderCardViewModel>>(ordersRes.Data);
            //        var groups = result.GroupBy(x => x.TeamId, (key, group) => new TeamOrdersViewModel { TeamId = key, Orders = group.OrderBy(x => x.RankInTeam) }).ToList();

            //        if (groups.Any())
            //        {
            //            if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
            //            {

            //                Parallel.For(0, currentTeams.Count,
            //      i =>
            //      {
            //          currentTeams[i].Orders = new List<OrderCardViewModel>();
            //          currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
            //          Parallel.For(0, groups.Count, j =>
            //          {
            //              if (currentTeams[i].TeamId == groups[j].TeamId)
            //              {
            //                  currentTeams[i].Orders = groups[j].Orders;
            //              }
            //          });
            //      });


            //                //for (int i = 0; i < currentTeams.Count; i++)
            //                //{
            //                //    currentTeams[i].Orders = new List<OrderCardViewModel>();
            //                //    currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
            //                //    for (int j = 0; j < groups.Count; j++)
            //                //    {
            //                //        if (currentTeams[i].TeamId == groups[j].TeamId)
            //                //        {
            //                //            currentTeams[i].Orders = groups[j].Orders;
            //                //        }
            //                //    }
            //                //}
            //            }
            //        }
            //        // here add foreman group binding
            //        var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName, x.Phone }, (key, group) => new DispatcherAssignedOrdersViewModel
            //        {
            //            ForemanId = key.ForemanId,
            //            ForemanName = key.ForemanName,
            //            ForemanPhone = key.Phone,
            //            Teams = group
            //        }).ToList();
            //        return ProcessResultHelper.Succedded<List<DispatcherAssignedOrdersViewModel>>(newGroup);
            //    }
            //    else
            //    {
            //        if (dispatcherTeams.IsSucceeded && dispatcherTeams.Data != null)
            //        {
            //            //for (int i = 0; i < currentTeams.Count; i++)
            //            //{
            //            //    currentTeams[i].Orders = new List<OrderCardViewModel>();
            //            //    //currentTeams[i].ForemanId = dispatcherTeams.Data[i].ForemanId;
            //            //    //currentTeams[i].ForemanName = dispatcherTeams.Data[i].ForemanName;
            //            //    //currentTeams[i].Phone = dispatcherTeams.Data[i].Phone;
            //            //}

            //            Parallel.For(0, currentTeams.Count,
            //      i =>
            //      {
            //          currentTeams[i].Orders = new List<OrderCardViewModel>();
            //      });
            //        }
            //        // here add foreman group binding
            //        var newGroup = currentTeams.GroupBy(x => new { x.ForemanId, x.ForemanName, x.Phone }, (key, group) => new DispatcherAssignedOrdersViewModel
            //        {
            //            ForemanId = key.ForemanId,
            //            ForemanName = key.ForemanName,
            //            Teams = group,
            //            ForemanPhone = key.Phone
            //        }).ToList();
            //        return ProcessResultHelper.Succedded<List<DispatcherAssignedOrdersViewModel>>(newGroup);
            //    }
            //}

            //return ProcessResultHelper.Succedded<List<DispatcherAssignedOrdersViewModel>>(new List<DispatcherAssignedOrdersViewModel>());

            return ProcessResultHelper.Succedded<List<DispatcherAssignedOrdersViewModel>>(await _orderService.GetAssignedOrdersForDisptcher(model));
        }

        private Expression<Func<OrderObject, bool>> CreateOrdersFilterPredicate(List<Filter> filters, bool includeUnAssign)
        {
            var predicate = PredicateBuilder.True<OrderObject>();
            foreach (var filter in filters)
            {
                filter.PropertyName = filter.PropertyName.ToPascalCase();
                if (filter.PropertyName.ToLower().Contains("date"))
                {
                    predicate = predicate.And(PredicateBuilder.CreateGreaterThanOrLessThanSingleExpression<OrderObject>(filter.PropertyName, filter.Value));
                }
                else
                {
                    predicate = predicate.And(PredicateBuilder.CreateEqualSingleExpression<OrderObject>(filter.PropertyName, filter.Value));
                }
            }
            if (includeUnAssign)
            {
                predicate = predicate.And(x => !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            }
            else
            {
                List<int> excludeStatusIds = StaticAppSettings.ArchivedStatusId;
                excludeStatusIds.Add(StaticAppSettings.InitialStatus.Id);
                predicate = predicate.And(x => !excludeStatusIds.Contains(x.StatusId));
            }
            return predicate;
        }

        //private async Task<ProcessResult<List<OrderAction>>> AddAction(OrderObject order, int? TeamId, OrderActionType actionType, int? workingTypeId, string workingTypeName, string subFlag, string userId, string reason = null, string StateFlag = null)
        //{
        //    string currentUserId = "";
        //    //currentUserId = "16023cd4-c0b7-42ef-9633-e269a5cfafa4";
        //    //currentUserId = "7e1717f7-d3ac-40b1-8787-e2065b3d008e";
        //    if (!string.IsNullOrEmpty(userId))
        //    {
        //        currentUserId = userId;
        //    }
        //    if (string.IsNullOrEmpty(userId))
        //    {
        //        var header = helper.GetAuthHeader(Request);
        //        currentUserId = helper.GetUserIdFromToken(header);
        //    }
        //    //string currentUserId = "3b8481c8-8f34-42d3-a296-cd01289fc67e";
        //    if (currentUserId == null || currentUserId == "")
        //    {
        //        return ProcessResultHelper.Failed<List<OrderAction>>(null, null, "Unauthorized");
        //    }
        //    var UserRes = await identityService.GetUserById(currentUserId);
        //    string CostCenterName = null;
        //    int? CostCenterId = 0;
        //    int teamId = 0;
        //    // get user data
        //    var orderAction = new OrderAction();
        //    var orderActions = new List<OrderAction>();

        //    orderAction.ActionDate = DateTime.Now;
        //    orderAction.ActionDay = DateTime.Now.Date;
        //    //orderAction.ActionTime = DateTime.Now.TimeOfDay;

        //    //if (UserRes != null && UserRes.Data Dispatcher!= null)
        //    //{
        //    var roleNames = UserRes.Data.RoleNames.FirstOrDefault();
        //    var Executer = UserRes.Data.UserName;
        //    // var roleNames = "Technician";
        //    switch (roleNames)
        //    {
        //        case "Engineer":
        //            var engineer = await engineerService.GetEngineerByUserId(currentUserId);
        //            if (engineer != null && engineer.Data != null)
        //            {
        //                CostCenterId = engineer.Data.CostCenterId;
        //                CostCenterName = engineer.Data.CostCenterName;
        //                orderAction.CreatedUser = engineer.Data.Name;
        //                orderAction.TeamId = order.TeamId;
        //                CurrentUserId = currentUserId;
        //            }
        //            break;

        //        case "Technician":
        //            var technician = await technicianService.GetTechnicianByUserId(currentUserId);
        //            if (technician != null && technician.Data != null)
        //            {
        //                CostCenterId = technician.Data.CostCenterId;
        //                CostCenterName = technician.Data.CostCenterName;
        //                orderAction.TeamId = technician.Data.TeamId;
        //                teamId = technician.Data.TeamId;
        //                orderAction.CreatedUser = technician.Data.Name;
        //                orderAction.CurrentUserId = technician.Data.UserId;
        //                //CurrentUserId = currentUserId;                       
        //                //var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
        //                var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
        //                if (teamMembers.IsSucceeded && teamMembers.Data != null)
        //                {
        //                    for (int i = 0; i < teamMembers.Data.Count; i++)
        //                    {
        //                        var _oa = new OrderAction()
        //                        {
        //                            CostCenterId = teamMembers.Data[i].CostCenterId,
        //                            CostCenterName = teamMembers.Data[i].CostCenter,
        //                            TeamId = teamMembers.Data[i].TeamId,
        //                            // ActionDate = DateTime.Now,
        //                            CreatedUser = teamMembers.Data[i].MemberParentName,
        //                            CurrentUserId = teamMembers.Data[i].UserId,
        //                            ActionDay = orderAction.ActionDay,
        //                            ActionTime = orderAction.ActionDate.TimeOfDay,
        //                            CreatedUserId = Int32.Parse(Executer),
        //                            Reason = reason,
        //                            ActionDate = orderAction.ActionDate,
        //                            ActionTypeName = OrderActionType.ChangeStatus.ToString(),
        //                            ActionTypeId = (Int32)OrderActionType.ChangeStatus,
        //                            //,
        //                            //PF=teamMembers.Data[i].PF
        //                        };





        //                        //if (technician.Data.Name != teamMembers.Data[i].MemberParentName)
        //                        //{
        //                        orderActions.Add(_oa);
        //                        // }
        //                    }
        //                }

        //            }
        //            break;

        //        case "Foreman":
        //            var foreman = await formanService.GetForemanByUserId(currentUserId);
        //            if (foreman != null && foreman.Data != null)
        //            {
        //                CostCenterId = foreman.Data.CostCenterId;
        //                CostCenterName = foreman.Data.CostCenterName;
        //                orderAction.CreatedUser = foreman.Data.Name;
        //                orderAction.TeamId = order.TeamId;
        //                CurrentUserId = currentUserId;
        //            }
        //            break;

        //        case "Supervisor":
        //            var supervisor = await supervisorService.GetSupervisorByUserId(currentUserId);
        //            if (order.DispatcherId == 0)
        //            {
        //                if (supervisor != null && supervisor.Data != null)
        //                {
        //                    CostCenterId = supervisor.Data.CostCenterId;
        //                    CostCenterName = supervisor.Data.CostCenterName;
        //                    orderAction.CreatedUser = supervisor.Data.Name;
        //                    orderAction.TeamId = order.TeamId;
        //                    CurrentUserId = currentUserId;
        //                }
        //            }
        //            else
        //            {
        //                var teamMembersInSub = await teamMembersService.GetMemberUsersByTeamId(TeamId.Value);
        //                if (teamMembersInSub.IsSucceeded && teamMembersInSub.Data != null)
        //                {
        //                    for (int i = 0; i < teamMembersInSub.Data.Count; i++)
        //                    {

        //                        orderActions.Add(new OrderAction()
        //                        {
        //                            CostCenterId = teamMembersInSub.Data[i].CostCenterId,
        //                            CostCenterName = teamMembersInSub.Data[i].CostCenter,
        //                            TeamId = teamMembersInSub.Data[i].TeamId,
        //                            ActionDate = orderAction.ActionDate,
        //                            CreatedUser = teamMembersInSub.Data[i].MemberParentName,
        //                            CurrentUserId = currentUserId, //teamMembersInSub.Data[i].UserId,
        //                                                           //CurrentUserId = currentUserId,//  teamMembers1.Data[i].UserId,
        //                            ActionDay = orderAction.ActionDay,
        //                            ActionTime = orderAction.ActionDate.TimeOfDay,
        //                            CreatedUserId = Int32.Parse(Executer)
        //                            //,
        //                            //PF=teamMembers.Data[i].PF
        //                        });

        //                    }
        //                }
        //            }


        //            break;

        //        case "Dispatcher":
        //            var dispatcher = await dispatcherService.GetDispatcherByUserId(currentUserId);
        //            var teamMembers1 = await teamMembersService.GetMemberUsersByTeamId(TeamId.Value);
        //            if (teamMembers1.IsSucceeded && teamMembers1.Data != null)
        //            {



        //                foreach (TeamMemberViewModel teamMemberVm in teamMembers1.Data)
        //                {
        //                    OrderAction orderActionOfi = new OrderAction()
        //                    {
        //                        CostCenterId = teamMemberVm.CostCenterId,
        //                        CostCenterName = teamMemberVm.CostCenter,
        //                        TeamId = teamMemberVm.TeamId,
        //                        ActionDate = orderAction.ActionDate,
        //                        CreatedUser = teamMemberVm.MemberParentName,
        //                        CurrentUserId = currentUserId, // teamMemberVm.UserId, //currentUserId,//  teamMemberVm.UserId,
        //                        ActionDay = orderAction.ActionDay,
        //                        ActionTime = orderAction.ActionDate.TimeOfDay,
        //                        CreatedUserId = Int32.Parse(Executer),
        //                        ActionTypeName = actionType == OrderActionType.ChangeTeamRank
        //                            ? OrderActionType.ChangeTeamRank.ToString()
        //                            : OrderActionType.UpdateOrder.ToString(),
        //                        ActionTypeId = actionType == OrderActionType.ChangeTeamRank
        //                            ? (Int32)OrderActionType.ChangeTeamRank
        //                            : (Int32)OrderActionType.UpdateOrder,
        //                    };
        //                    orderActions.Add(orderActionOfi);
        //                }
        //                //for (int i = 0; i < teamMembers1.Data.Count; i++)
        //                //{


        //                //}
        //            }
        //            break;

        //        default:
        //            break;
        //            //}
        //    }
        //    // binding order action details
        //    if (order != null)
        //    {
        //        orderAction.ServeyReport = order.ServeyReport;
        //        orderAction.StatusId = order.StatusId;
        //        orderAction.StatusName = order.StatusName;
        //        orderAction.OrderId = order.Id == 0 ? (int?)null : order.Id;
        //        orderAction.SubStatusId = order.SubStatusId;
        //        orderAction.SubStatusName = order.SubStatusName;
        //        orderAction.ActionTypeId = (int)actionType;
        //        orderAction.ActionTypeName = EnumManager<OrderActionType>.GetName(actionType);
        //        orderAction.SupervisorId = order.SupervisorId;
        //        orderAction.DispatcherId = order.DispatcherId;
        //        orderAction.DispatcherName = order.DispatcherName;
        //        orderAction.SupervisorName = order.SupervisorName;
        //        orderAction.TeamId = orderAction.TeamId;
        //        //orderAction.CurrentUserId = orderAction.CurrentUserId; //currentUserId;
        //        orderAction.CostCenterId = CostCenterId;
        //        orderAction.CostCenterName = CostCenterName;
        //        orderAction.Reason = actionType == OrderActionType.Rejection ? reason : null;
        //    }
        //    else
        //    {
        //        orderAction.ActionTypeId = (int)actionType;
        //        orderAction.ActionTypeName = EnumManager<OrderActionType>.GetName(actionType);
        //        orderAction.CurrentUserId = orderAction.CurrentUserId; //currentUserId;
        //        orderAction.CostCenterId = CostCenterId;
        //        orderAction.CostCenterName = CostCenterName;
        //    }
        //    if (order != null)
        //    {
        //        if (actionType == OrderActionType.Acceptence || actionType == OrderActionType.Rejection)
        //        {
        //            orderAction.Reason = reason;
        //            //if (orderAction.TeamId ;> 0)
        //            //{
        //            //    var lastOrderActions = actionManager.GetAll(x => x.TeamId == orderAction.TeamId && x.ActionTypeId != (int)OrderActionType.Assign && x.ActionTypeId != (int)OrderActionType.UnAssgin).Data.OrderBy(r => r.CreatedDate).ToList();
        //            //    if (lastOrderActions.Count > 0)
        //            //    {
        //            //        orderAction.WorkingTypeId = lastOrderActions.LastOrDefault().WorkingTypeId;
        //            //        orderAction.WorkingTypeName = lastOrderActions.LastOrDefault().WorkingTypeName;
        //            //    }
        //            //}
        //            //orderAction.Reason = actionType == OrderActionType.Rejection ? reason : null;
        //        }
        //        else if (StaticAppSettings.TravellingStatusId.Contains(order.StatusId))
        //        {
        //            orderAction.WorkingTypeId = (int)TimeSheetType.Traveling;
        //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling);

        //        }
        //        //else if (StaticAppSettings.WorkingStatusId.Contains(order.StatusId))
        //        //{                    
        //        //    orderAction.WorkingTypeId = (int)TimeSheetType.Idle;
        //        //    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle);

        //        //}                
        //        else if (actionType == OrderActionType.ChangeTeamRank || actionType == OrderActionType.Assign || actionType == OrderActionType.UnAssgin || actionType == OrderActionType.BulkUnAssign)
        //        {
        //            //orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
        //            //orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
        //            orderAction.WorkingTypeId = null;
        //            orderAction.WorkingTypeName = null;
        //            // }
        //        }
        //        else
        //        {
        //            orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
        //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
        //            //orderAction.WorkingTypeId = workingTypeId;
        //            //orderAction.WorkingTypeName = workingTypeName;
        //            // }
        //        }
        //    }
        //    else
        //    {
        //        //var TeamModeresult = await GetTeamMode(orderAction.TeamId);
        //        //if (TeamModeresult.Data.Mode.Contains("Idle"))
        //        //{
        //        //    //actionType = OrderActionType.Idle;
        //        //    orderAction.WorkingTypeId = (int)TimeSheetType.Idle;
        //        //    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle);
        //        //}
        //        //else if (TeamModeresult.Data.Mode.Contains("Break"))
        //        //{
        //        //    //actionType = OrderActionType.Break;
        //        //    orderAction.WorkingTypeId = (int)TimeSheetType.Break;
        //        //    orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Break);
        //        //}
        //        //orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
        //        //orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
        //        ///// for time sheet groups
        //        if (actionType == OrderActionType.Idle)
        //        {
        //            orderAction.WorkingTypeId = (int)TimeSheetType.Idle;
        //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Idle);
        //            orderAction.Reason = workingTypeName;
        //        }
        //        else if (actionType == OrderActionType.Break)
        //        {
        //            orderAction.WorkingTypeId = (int)TimeSheetType.Break;
        //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Break);
        //            orderAction.Reason = workingTypeName;
        //        }
        //        else if (actionType == OrderActionType.ChangeTeamRank || actionType == OrderActionType.Assign || actionType == OrderActionType.UnAssgin || actionType == OrderActionType.BulkUnAssign)
        //        {
        //            //orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
        //            //orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
        //            orderAction.WorkingTypeId = null;
        //            orderAction.WorkingTypeName = null;
        //            // }
        //        }
        //        else
        //        {
        //            orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
        //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
        //        }

        //    }




        //    //if (actionType == OrderActionType.Work)
        //    //{
        //    //    if (order != null)
        //    //    {
        //    //        if (StaticAppSettings.TravellingStatusId.Contains(order.StatusId))
        //    //        {
        //    //            orderAction.WorkingTypeId = (int)TimeSheetType.Traveling;
        //    //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Traveling);
        //    //        }
        //    //        else
        //    //        {
        //    //            orderAction.WorkingTypeId = (int)TimeSheetType.Actual;
        //    //            orderAction.WorkingTypeName = EnumManager<TimeSheetType>.GetName(TimeSheetType.Actual);
        //    //        }
        //    //    }
        //    //    //else
        //    //    //{
        //    //    //    if (true)
        //    //    //    {

        //    //    //    }
        //    //    //    return ProcessResultHelper.Failed<OrderAction>(null, null, "order is required");
        //    //    //}
        //    //}
        //    //else
        //    //{
        //    //    orderAction.WorkingTypeId = workingTypeId;
        //    //    orderAction.WorkingTypeName = workingTypeName;
        //    //}

        //    if (workingTypeName == "End Work")
        //    {
        //        orderAction.Reason = "End Work";
        //    }
        //    if (actionType == OrderActionType.Rejection)
        //        orderAction.Reason = reason;
        //    //
        //    if (!string.IsNullOrEmpty(TeamId.ToString()))
        //    {
        //        orderAction.TeamId = TeamId.Value;
        //    }
        //    if (orderAction.TeamId > 0)
        //    {
        //        var lastActions = actionManager.GetAll(x => x.TeamId == orderAction.TeamId && x.ActionTime == null).Data.ToList();
        //        if (lastActions.Count > 0)
        //        {
        //            for (int i = 0; i < lastActions.Count; i++)
        //            {
        //                lastActions[i].ActionTime = orderAction.ActionDate - lastActions[i].ActionDate;

        //                //if (i > 0)
        //                //{
        //                //    lastActions[i].ActionTime = orderAction.ActionDate - lastActions[i].ActionDate;
        //                //}
        //                //else
        //                //{
        //                //    lastActions[i].ActionTime = orderAction.ActionDate.TimeOfDay;
        //                //}
        //                int days = lastActions[i].ActionTime.Value.Days;
        //                //Stop Recording Time Sheet
        //                if (lastActions[i].WorkingTypeName == "End Work")
        //                {
        //                    lastActions[i].ActionTime = null;
        //                    days = 0;
        //                }
        //                if (days > 0)
        //                {
        //                    lastActions[i].ActionTime = lastActions[i].ActionTime.Value.Subtract(TimeSpan.FromDays(days));
        //                    lastActions[i].ActionTimeDays = days;
        //                    //orderAction.ActionTime = lastActions[i].ActionTime;
        //                    //orderAction.ActionTimeDays = days;

        //                }
        //                else
        //                {
        //                    lastActions[i].ActionTimeDays = 0;
        //                    //orderAction.ActionTimeDays = lastActions[i].ActionTimeDays;
        //                }
        //            }
        //            actionManager.Update(lastActions);
        //            //for (int i = 0; i < lastActions.Count; i++)
        //            //{
        //            //    var Time = new TimeSpan(lastActions[lastActions.Count].ActionTime.Value.Ticks);
        //            //    var jobId = BackgroundJob.Schedule(
        //            //    () => Hangfire(lastActions[lastActions.Count]), TimeSpan.FromMinutes(1));
        //            //}
        //            //foreach (var action in lastActions)
        //            //{
        //            //    var Time = new TimeSpan(action.ActionTime.Value.Ticks);
        //            //    var jobId = BackgroundJob.Schedule(
        //            //    () => Hangfire(action), TimeSpan.FromMinutes(1));
        //            //}
        //        }
        //    }
        //    // Supervisor Operations
        //    if (orderActions.Count == 0 && (!string.IsNullOrEmpty(subFlag)))
        //    {
        //        //for (int i = 0; i < orderActions.Count; i++)
        //        //{
        //        if (order != null)
        //        {
        //            orderAction.StatusId = order.StatusId;
        //            orderAction.StatusName = order.StatusName;
        //            orderAction.OrderId = order.Id == 0 ? (int?)null : order.Id;
        //            orderAction.SubStatusId = order.SubStatusId;
        //            orderAction.SubStatusName = order.SubStatusName;
        //            orderAction.SupervisorId = order.SupervisorId;
        //            orderAction.DispatcherId = order.DispatcherId;
        //            orderAction.DispatcherName = order.DispatcherName;
        //            orderAction.SupervisorName = order.SupervisorName;
        //            orderAction.ServeyReport = order.ServeyReport;
        //            orderAction.TeamId = order.TeamId;
        //        }
        //        orderAction.ActionTypeId = orderAction.ActionTypeId;
        //        orderAction.ActionTypeName = orderAction.ActionTypeName;
        //        //orderActions[i].TeamId = orderAction.TeamId;                   
        //        if (order == null)
        //        {
        //            orderAction.TeamId = orderAction.TeamId;
        //        }
        //        orderAction.WorkingTypeId = orderAction.WorkingTypeId;
        //        orderAction.WorkingTypeName = orderAction.WorkingTypeName;
        //        orderAction.CurrentUserId = orderAction.CurrentUserId; //currentUserId;


        //        if (orderAction.ActionTypeName == OrderActionType.Assign.ToString() || orderAction.ActionTypeName == OrderActionType.UnAssgin.ToString() || orderAction.ActionTypeName == OrderActionType.BulkAssign.ToString() || orderAction.ActionTypeName == OrderActionType.BulkUnAssign.ToString())
        //        {
        //            orderAction.ServeyReport = "";
        //        }
        //        if (order.DispatcherId == 0)
        //        {
        //            order.DispatcherName = "";
        //            orderAction.ActionTypeName = "UnAssign Dispatcher";
        //        }
        //        if (order.DispatcherId != 0)
        //        {
        //            orderAction.ActionTypeName = "Assign Dispatcher";
        //        }
        //        orderActions.Add(new OrderAction()
        //        {
        //            Reason = orderAction.Reason,
        //            StatusId = order.StatusId,
        //            StatusName = order.StatusName,
        //            OrderId = order.Id == 0 ? (int?)null : order.Id,
        //            SubStatusId = orderAction.SubStatusId,
        //            SubStatusName = orderAction.SubStatusName,
        //            SupervisorId = order.SupervisorId,
        //            DispatcherId = order.DispatcherId,
        //            DispatcherName = order.DispatcherName,
        //            SupervisorName = order.SupervisorName,
        //            //Fixing  Survey Report Probelm of Mangement Report and Order Progress In assign Assign Dispatcher &&UnAssign Dispatcher
        //            ServeyReport = orderAction.ServeyReport,
        //            ActionTypeName = orderAction.ActionTypeName,
        //            TeamId = order.TeamId,
        //            ActionDate = orderAction.ActionDate,
        //            //CreatedUser = teamMembers1.Data[i].MemberParentName,
        //            //CurrentUserId = orderAction.CurrentUserId, //currentUserId,//  teamMembers1.Data[i].UserId,
        //            ActionDay = orderAction.ActionDay,
        //            ActionTime = orderAction.ActionTime,
        //            ActionTimeDays = orderAction.ActionTimeDays,
        //            CreatedUserId = Int32.Parse(Executer ?? "0"),
        //            ActionTypeId = orderAction.ActionTypeId,
        //        });

        //    }

        //    if (orderActions.Count > 0 && (string.IsNullOrEmpty(subFlag)))
        //    {
        //        for (int i = 0; i < orderActions.Count; i++)
        //        {
        //            if (order != null)
        //            {
        //                orderActions[i].StatusId = order.StatusId;
        //                orderActions[i].StatusName = order.StatusName;
        //                orderActions[i].OrderId = order.Id == 0 ? (int?)null : order.Id;
        //                orderActions[i].SubStatusId = order.SubStatusId;
        //                orderActions[i].SubStatusName = order.SubStatusName;
        //                orderActions[i].SupervisorId = order.SupervisorId;
        //                orderActions[i].DispatcherId = order.DispatcherId;
        //                orderActions[i].DispatcherName = order.DispatcherName;
        //                orderActions[i].SupervisorName = order.SupervisorName;
        //                orderActions[i].ServeyReport = order.ServeyReport;
        //                orderActions[i].TeamId = order.TeamId;
        //                orderActions[i].ActionTime = orderAction.ActionTime;
        //                orderActions[i].ActionTimeDays = orderAction.ActionTimeDays;
        //                orderActions[i].Reason = orderAction.Reason;
        //            }
        //            //orderActions[i].ActionTypeId = orderAction.ActionTypeId;
        //            //orderActions[i].ActionTypeName = orderAction.ActionTypeName;
        //            //orderActions[i].TeamId = orderAction.TeamId;                   
        //            if (order == null)
        //            {
        //                orderActions[i].TeamId = orderAction.TeamId;
        //                // Set ActionTiME & ActionTime Days From OrderAction
        //                orderActions[i].ActionTime = orderAction.ActionTime;
        //                orderActions[i].ActionTimeDays = orderAction.ActionTimeDays;
        //            }
        //            if (workingTypeName == "End Work" && workingTypeId == 10)
        //            {
        //                orderAction.Reason = "End Work";
        //                orderActions[i].WorkingTypeId = workingTypeId;
        //                orderActions[i].WorkingTypeName = workingTypeName;
        //                orderActions[i].ActionTypeName = "";
        //                orderActions[i].ActionTypeId = 0;
        //                // Set ActionTiME & ActionTime Days From OrderAction
        //                orderActions[i].ActionTime = orderAction.ActionTime;
        //                orderActions[i].ActionTimeDays = orderAction.ActionTimeDays;
        //            }
        //            else
        //            {
        //                orderActions[i].WorkingTypeId = orderAction.WorkingTypeId;
        //                orderActions[i].WorkingTypeName = orderAction.WorkingTypeName;
        //                // Set ActionTiME & ActionTime Days From OrderAction
        //                orderActions[i].ActionTime = orderAction.ActionTime;
        //                orderActions[i].ActionTimeDays = orderAction.ActionTimeDays;
        //            }
        //            //Solving  Survey Report Probelm of Mnangement Report and Order Progress
        //            // if ''


        //            string actionTyp = orderActions[i].ActionTypeName;

        //            //if (actionTyp == OrderActionType.Assign.ToString() || actionTyp == OrderActionType.UnAssgin.ToString() || actionTyp == OrderActionType.Acceptence.ToString() || actionTyp == OrderActionType.BulkAssign.ToString() || actionTyp == OrderActionType.UpdateOrder.ToString() || actionTyp == OrderActionType.Work.ToString() || actionTyp == "Assign Dispatcher" || actionTyp == "UnAssign Dispatcher" || actionTyp == "ChangeTeamRank" || actionTyp == "Rejection" || actionTyp == "Traveling")
        //            if (actionTyp != OrderActionType.ChangeStatus.ToString() || actionTyp != OrderActionType.UpdateOrder.ToString())
        //            {
        //                //onhold-cancel-complete
        //                if (orderActions[i].StatusName != "Compelete" && orderActions[i].StatusName != "Cancelled" && orderActions[i].StatusName != "On-Hold")
        //                {
        //                    orderActions[i].ServeyReport = "";
        //                }
        //            }
        //            if (orderActions[i].WorkingTypeName == "Traveling" && orderActions[i].ActionTypeName == OrderActionType.ChangeStatus.ToString())
        //                orderActions[i].ServeyReport = "";
        //            if ((orderActions[i].WorkingTypeName == "" && orderActions[i].ActionTypeName == OrderActionType.ChangeStatus.ToString()) || (orderActions[i].WorkingTypeName == null && orderActions[i].ActionTypeName == OrderActionType.ChangeStatus.ToString()))
        //            {
        //                //Acceptence
        //                orderActions[i].ServeyReport = "";
        //                orderActions[i].ActionTypeName = OrderActionType.Acceptence.ToString();
        //                orderActions[i].ActionTypeId = (int)OrderActionType.Acceptence;
        //                //Rejection
        //                if (orderActions[i].TeamId == 0 && (!string.IsNullOrEmpty(orderActions[i].Reason)))
        //                {
        //                    orderActions[i].ActionTypeName = OrderActionType.Rejection.ToString();
        //                    orderActions[i].ActionTypeId = (int)OrderActionType.Rejection;
        //                }
        //            }
        //            if (orderActions[i].StatusId == 6 && orderActions[i].ActionTypeName == OrderActionType.UpdateOrder.ToString())
        //                orderActions[i].ServeyReport = order.ServeyReport;

        //        }
        //    }
        //    if (orderActions.Count == 0 && (string.IsNullOrEmpty(subFlag)) && TeamId.Value == 0)
        //    {
        //        orderActions.Add(new OrderAction()
        //        {
        //            StatusId = order.StatusId,
        //            StatusName = order.StatusName,
        //            OrderId = order.Id == 0 ? (int?)null : order.Id,
        //            SubStatusId = order.SubStatusId,
        //            SubStatusName = order.SubStatusName,
        //            SupervisorId = order.SupervisorId,
        //            DispatcherId = order.DispatcherId,
        //            DispatcherName = order.DispatcherName,
        //            SupervisorName = order.SupervisorName,
        //            ServeyReport = order.ServeyReport,
        //            TeamId = order.TeamId,
        //            ActionTime = orderAction.ActionTime,
        //            ActionTimeDays = orderAction.ActionTimeDays,
        //            ActionDate = orderAction.ActionDate,
        //            //CreatedUser = currentUserId,
        //            CurrentUserId = currentUserId,// teamMembers1.Data[i].UserId, //currentUserId,//  teamMembers1.Data[i].UserId,
        //            ActionDay = orderAction.ActionDay,
        //            CreatedUserId = Int32.Parse(Executer),

        //            //,
        //            //PF=teamMembers.Data[i].PF
        //        });
        //        for (int i = 0; i < orderActions.Count; i++)
        //        {
        //            if (orderActions[i].StatusName == "Open")
        //            {
        //                orderActions[i].SubStatusName = "Scheduled";
        //                orderActions[i].SubStatusId = 2;
        //                orderActions[i].ServeyReport = "";
        //                orderActions[i].ActionTypeName = OrderActionType.UpdateOrder.ToString();
        //                orderActions[i].ActionTypeId = (Int32)OrderActionType.UpdateOrder;
        //            }
        //            else
        //            {
        //                orderActions[i].SubStatusName = order.SubStatusName;
        //                orderActions[i].SubStatusId = order.SubStatusId;
        //                orderActions[i].ActionTypeName = OrderActionType.UpdateOrder.ToString();
        //                orderActions[i].ActionTypeId = (Int32)OrderActionType.UpdateOrder;
        //                orderActions[i].ServeyReport = order.ServeyReport;
        //            }

        //        }

        //    }
        //    for (int i = 0; i < orderActions.Count; i++)
        //    {
        //        if (StateFlag == OrderActionType.Assign.ToString())
        //        {
        //            orderActions[i].ActionTypeName = OrderActionType.Assign.ToString();
        //            orderActions[i].ActionTypeId = (Int32)OrderActionType.Assign;
        //        }
        //        if (StateFlag == OrderActionType.UnAssgin.ToString())
        //        {
        //            orderActions[i].ActionTypeName = OrderActionType.UnAssgin.ToString();
        //            orderActions[i].ActionTypeId = (Int32)OrderActionType.UnAssgin;
        //        }
        //        if (StateFlag == OrderActionType.BulkAssign.ToString())
        //        {
        //            orderActions[i].ActionTypeName = OrderActionType.BulkAssign.ToString();
        //            orderActions[i].ActionTypeId = (Int32)OrderActionType.BulkAssign;
        //            orderActions[i].ServeyReport = "";
        //        }
        //    }

        //    return actionManager.Add(orderActions);
        //}
        //public IActionResult SendIdeleBreakAlert()
        //{
        //    var jobId = BackgroundJob.Schedule(
        //    () => Hangfire(), TimeSpan.FromMinutes(1));
        //    //var result = await Hangfire();
        //    return View();
        //}

        //private async Task<ProcessResult<bool>> Hangfire(OrderAction action)
        //{
        //    var orderActions = manager.GetAll();
        //    var tt = new TimeSpan(30);
        //    var Time = new TimeSpan(action.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays != null ? action.ActionTimeDays.Value : 0)).Ticks);  
        //    //var Time = action.ActionTime.Value.Subtract(TimeSpan.FromMinutes());
        //    if(Time>tt)
        //    if (action.WorkingTypeId == (int)TimeSheetType.Idle || action.WorkingTypeId == (int)TimeSheetType.Break)
        //    {
        //         var order = Get(action.OrderId.Value).Data;
        //        var NotificationRes = await NotifyDispatcherWithTechnicianLate(order);
        //        return ProcessResultHelper.Succedded<bool>(true);
        //    }
        //    return ProcessResultHelper.Succedded<bool>(true);
        //}

        //TeamMode
        [HttpGet]
        [Route("GetTeamMode/{teamId}")]
        public async Task<ProcessResult<TeamModeViewModel>> GetTeamMode([FromRoute]int teamId)
        {
            var result = new TeamModeViewModel();
            var actions = actionManager.GetAll(x => x.TeamId == teamId && (x.ActionTypeId != (int)OrderActionType.Assign && x.ActionTypeId != (int)OrderActionType.UnAssgin));
            if (actions.IsSucceeded)
            {
                if (actions.Data != null)
                {
                    if (actions.Data.Count() > 0)
                    {
                        var lastAction = mapper.Map<OrderAction, OrderActionViewModel>(actions.Data.LastOrDefault());
                        if (lastAction.WorkingTypeId == (int)TimeSheetType.Actual && lastAction.OrderId == null)
                        {
                            result.Mode = "Stopped";
                        }
                        else if (lastAction.WorkingTypeId == (int)TimeSheetType.Actual || lastAction.WorkingTypeName == "Continue working")
                        {
                            result.Mode = "Working";
                        }
                        else if (lastAction.WorkingTypeName == "Accept & set first order On-Travel" || lastAction.WorkingTypeName == "Set first order On-Travel" || lastAction.StatusId == (int)OrderStatusEnum.Reached)
                        {
                            result.Mode = "Travelling";
                        }
                        else
                        {
                            result.Mode = $"{lastAction.WorkingTypeName} - {lastAction.Reason}";
                        }
                        return ProcessResultHelper.Succedded<TeamModeViewModel>(result);
                    }
                }
                return ProcessResultHelper.Succedded<TeamModeViewModel>(result);
            }
            return ProcessResultHelper.Failed<TeamModeViewModel>(result, actions.Exception, SharedResource.General_WrongWhileGet);
        }

        //
        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithNewOrders(int teamId, List<int> OrderIds)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    foreach (var OrderId in OrderIds)
                    {
                        var orderRes = manager.Get(OrderId);
                        var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
                        var identity = await identityService.GetById(dispatcherRes.Data.UserId);
                        notificationModels.Add(new SendNotificationViewModel()
                        {
                            UserId = item.UserId,
                            Title = "New Order",
                            Body = $"Dispatcher {identity.Data?.FullName} assigned New order No. {orderRes.Data.Code} to your team",
                            Data = new { OrderId = OrderId, NotificationType = "New Order" }
                        });
                        ///

                        //
                        notificationCenterManageManager.Add(new NotificationCenter()
                        {
                            RecieverId = item.UserId,
                            Title = "New Order",
                            Body = $"Dispatcher {identity.Data?.FullName} assigned New order No. {orderRes.Data.Code} to your team",
                            Data = OrderId.ToString(),
                            NotificationType = "New Order",
                            IsRead = false,
                            CurrentUserId = identity.Data?.UserName
                            // TeamId= teamId
                        });
                    }

                }

                notificationRes = await notificationService.SendToMultiUsers(notificationModels);
            }
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithNewOrder(int teamId, OrderObject order)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
            var dispatcherRes = dispatcherService.GetDispatcherById(order.DispatcherId ?? 0);
            var identity = await identityService.GetById(dispatcherRes.Data.UserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            //var orderRes = manager.Get(order.Id);
            var orderObj = Mapper.Map<OrderViewModel>(order);
            var jsonOrder = JsonConvert.SerializeObject(orderObj, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        // UserId = "fAW2ceo5Yg0:APA91bGonZfMtN6-vU3sEPxS8RUUpXsixwky0OULqY7aFdy0MULhXnfTMfvXCpcVU1nhq4q0BQnKKz3YetYWAQMTxh5NUKIA9DcES0HTpIjsW8AXq4QmqcllutK695wswYhWrRswY6-K"
                        //  ,

                        UserId = item.UserId,
                        Title = "New Order",
                        Body = $"Dispatcher {identity.Data?.FullName} assigned New order No. {order.Code} to your team",
                        Data = new { OrderId = order.Id, NotificationType = "New Order" }

                        //Data = new { OrderId = order.Id, NotificationType = "New Order" }
                    });
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = item.UserId,
                        //RecieverId = "fAW2ceo5Yg0:APA91bGonZfMtN6-vU3sEPxS8RUUpXsixwky0OULqY7aFdy0MULhXnfTMfvXCpcVU1nhq4q0BQnKKz3YetYWAQMTxh5NUKIA9DcES0HTpIjsW8AXq4QmqcllutK695wswYhWrRswY6-K"

                        Title = "New Order",
                        Body = $"Dispatcher {identity.Data?.FullName} assigned New order No. {order.Code} to your team",
                        Data = order.Id.ToString(),
                        NotificationType = "New Order",
                        IsRead = false,
                        CurrentUserId = identity.Data?.UserName,
                        OrderCode = order.Code
                    });
                }
                notificationRes = await notificationService.SendToMultiUsers(notificationModels);
            }
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersWithUnassignOrder(int teamId, int OrderId)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var orderRes = manager.Get(OrderId);
            var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            var identity = await identityService.GetById(dispatcherRes.Data.UserId);
            var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            //var orderRes = manager.Get(x=>x.Id==OrderId);
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            //var order = Mapper.Map<OrderViewModel>(orderObj);
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            if (teamMembers != null && teamMembers.Data != null)
            {
                foreach (var item in teamMembers.Data)
                {
                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        UserId = item.UserId,
                        Title = "Unassigned Order",
                        Body = $"Dispatcher {identity.Data?.FullName} unassigned order No. {orderRes.Data.Code} from your team",
                        Data = new { OrderId = OrderId, NotificationType = "Unassigned Order" }
                        // Data = new { Order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Unassigned Order" }
                    });
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = item.UserId,
                        Title = "Un assign Order",
                        Body = $"Dispatcher {identity.Data?.FullName} unassigned order No. {orderRes.Data.Code} from your team",
                        Data = OrderId.ToString(),
                        NotificationType = "Unassigned Order",
                        IsRead = false,
                        CurrentUserId = identity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                }

                notificationRes = await notificationService.SendToMultiUsers(notificationModels);
            }
            return notificationRes;
        }
        //Supervisor
        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderAssign(OrderViewModel order)
        {
            AssignDispatcherOrderViewModel assignDispatcherOrderViewModel = new AssignDispatcherOrderViewModel();
            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            // assignDispatcherOrderViewModel = Mapper.Map<OrderObject, AssignDispatcherOrderViewModel>(order);
            var supIdentity = await identityService.GetById(currentUserId);
            //var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
            var dispatcherRes = dispatcherService.GetDispatcherById(order.DispatcherId ?? 0);
            //var identity = await identityService.GetUserById(dispatcherRes.Data.UserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            //var OrderAsSmall =JsonConvert.SerializeObject(order, Formatting.Indented, new JsonSerializerSettings() { ContractResolver = new LowercaseContractResolver() });        

            //string orderAsJsonSmall = orderAsJson.ToLower();
            notificationModels.Add(new SendNotificationViewModel()
            {
                UserId = dispatcherRes.Data.UserId,
                Title = "Assign Dispatcher",
                Body = $"Supervisor {supIdentity.Data?.FullName} assigned New order No. {order.Code} to you",
                Data = new { Order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "assignDispatcher" }
            });

            notificationCenterManageManager.Add(new NotificationCenter()
            {
                RecieverId = dispatcherRes.Data.UserId,
                Title = "Assign Order",
                Body = $"Supervisor {supIdentity.Data?.FullName} assigned New order No. {order.Code} to you",
                Data = order.Id.ToString(),
                NotificationType = "New Order",
                IsRead = false,
                CurrentUserId = supIdentity.Data?.UserName,
                OrderCode = order.Code
            });
            // }

            notificationRes = await notificationService.SendToMultiUsers(notificationModels);
            //}
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderUnassign(OrderViewModel order, int DispID)
        {


            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var supIdentity = await identityService.GetById(currentUserId);
            //var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamId);
            var dispatcherRes = dispatcherService.GetDispatcherById(DispID);
            //var identity = await identityService.GetUserById(dispatcherRes.Data.UserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            notificationModels.Add(new SendNotificationViewModel()
            {
                UserId = dispatcherRes.Data.UserId,
                Title = "Unassign Order",
                Body = $"Supervisor {supIdentity.Data?.FullName} Unassigned order No. {order.Code} From you",
                Data = new { Order = order.Id, NotificationType = "unAssignDispatcher" }
            });
            notificationCenterManageManager.Add(new NotificationCenter()
            {
                RecieverId = dispatcherRes.Data.UserId,
                Title = "New Order",
                Body = $"Supervisor {supIdentity.Data?.FullName} Unassigned order No. {order.Code} From you",
                Data = order.Id.ToString(),
                NotificationType = "New Order",
                IsRead = false,
                CurrentUserId = supIdentity.Data?.UserName,
                OrderCode = order.Code
            });
            // }

            notificationRes = await notificationService.SendToMultiUsers(notificationModels);
            //}
            return notificationRes;
        }

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewOrders(int dispatcherId, List<int> OrderIds)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var dispatcher = await dispatcherService.GetDispatcherById(dispatcherId);
        //    List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
        //    if (dispatcher != null && dispatcher.Data != null)
        //    {
        //        foreach (var OrderId in OrderIds)
        //        {
        //            notificationModels.Add(new SendNotificationViewModel()
        //            {
        //                UserId = dispatcher.Data.UserId,
        //                Title = "New Order",
        //                Body = "There is new Order With Id: " + OrderId,
        //                Data = new { OrderId = OrderId }
        //            });
        //        }
        //        notificationRes = await notificationService.SendToMultiUsers(notificationModels);
        //    }
        //    return notificationRes;
        //}

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewOrder(int dispatcherId, int orderId)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var dispatcher = await dispatcherService.GetDispatcherById(dispatcherId);
        //    SendNotificationViewModel notificationModel = new SendNotificationViewModel()
        //    {
        //        UserId = dispatcher.Data.UserId,
        //        Title = "New Order",
        //        Body = "There is new Order With Id: " + orderId,
        //        Data = new { OrderId = orderId }
        //    };
        //    notificationRes = await notificationService.SendNotification(notificationModel);
        //    return notificationRes;
        //}

        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderAcceptence(AcceptenceViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            //var UserRes = await identityService.GetUserById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var identity = await identityService.GetById(currentUserId);
            var orderRes = manager.Get(model.OrderId);
            var dispatcher = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            SendNotificationViewModel notificationModel;
            var userId = dispatcher.Data.UserId;
            var orderId = model.OrderId;
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            //string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (dispatcher != null && dispatcher.Data != null)
            {
                if (model.AcceptenceFlag)
                {
                    notificationModel = new SendNotificationViewModel()
                    {
                        UserId = dispatcher.Data.UserId,
                        Title = "Acceptence Order",
                        Body = $"Technician {identity.Data?.FullName} Accepted order No. {orderRes.Data.Code}.",
                        // Data = new { OrderId = model.OrderId, NotificationType = "Acceptence Order" },
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Acceptence Order" },
                        TeamId = orderRes.Data.TeamId ?? 0//TODO Refactor issue
                    };
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = userId,
                        Title = "Acceptence Order",
                        Body = $"Technician {identity.Data?.FullName} Accepted order No. {orderRes.Data.Code}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Acceptence Order",
                        IsRead = false,
                        TeamId = orderRes.Data.TeamId,
                        CurrentUserId = identity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                }
                else
                {
                    notificationModel = new SendNotificationViewModel()
                    {
                        UserId = dispatcher.Data.UserId,
                        Title = "Rejection Order",
                        Body = $"Technician {identity.Data?.FullName} rejected order No. {orderRes.Data.Code} as {model.RejectionReason}.",
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Acceptence Order" }
                        //  Data = new { OrderId = model.OrderId, NotificationType = "Acceptence Order" }

                    };
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = userId,
                        Title = "Rejection Order",
                        Body = $"Technician {identity.Data?.FullName} rejected order No. {orderRes.Data.Code} as {model.RejectionReason}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Rejection Order",
                        IsRead = false,
                        CurrentUserId = identity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                }
                notificationRes = await notificationService.Send(notificationModel);
            }
            return notificationRes;
        }

        //private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderChange(AcceptenceViewModel model)
        //{
        //    ProcessResultViewModel<string> notificationRes = null;
        //    var orderRes = manager.Get(model.OrderId);
        //    var dispatcher = await dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId);
        //    SendNotificationViewModel notificationModel;
        //    var userId = dispatcher.Data.UserId;
        //    var orderId = model.OrderId;
        //    if (model.AcceptenceFlag)
        //    {
        //        notificationModel = new SendNotificationViewModel()
        //        {
        //            UserId = userId,
        //            Title = "Acceptence Order",
        //            Body = $"The order With Id: { orderId } is accepted",
        //            Data = new { Order = orderRes }
        //        };
        //    }
        //    else
        //    {
        //        notificationModel = new SendNotificationViewModel()
        //        {
        //            UserId = dispatcher.Data.UserId,
        //            Title = "Acceptence Order",
        //            Body = "The order With Id: " + model.OrderId + "is rejected as " + model.RejectionReason,
        //            Data = new { Order = orderRes }
        //        };
        //    }
        //    notificationRes = await notificationService.SendNotification(notificationModel);
        //    return notificationRes;
        //}

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersandDispatcherWithNewStatus(ChangeStatusViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            //var UserRes = await identityService.GetUserById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var techIdentity = await identityService.GetById(currentUserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var orderRes = manager.Get(model.OrderId);
            var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            var dispIdentity = await identityService.GetById(dispatcherRes.Data?.UserId);
            if (orderRes.Data.TeamId != 0)
            {
                var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                if (teamMembers != null && teamMembers.Data != null)
                {
                    foreach (var item in teamMembers.Data)
                    {
                        notificationModels.Add(new SendNotificationViewModel()
                        {
                            UserId = item.UserId,
                            Title = "New Status",
                            Body = $"Dispatcher {dispIdentity.Data?.FullName } changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status.Name}.",
                            Data = new { OrderId = model.OrderId, NotificationType = "New Status" }
                        });
                        notificationCenterManageManager.Add(new NotificationCenter()
                        {
                            RecieverId = item.UserId,
                            Title = "New Status",
                            Body = $"Dispatcher {dispIdentity.Data?.FullName } changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                            Data = model.OrderId.ToString(),
                            NotificationType = "New Status",
                            IsRead = false,
                            CurrentUserId = dispIdentity.Data?.UserName,
                            OrderCode = orderRes.Data.Code
                        });
                    }
                }

            }
            var jsonOrder = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            if (dispatcherRes != null && dispatcherRes.Data != null)
            {
                notificationModels.Add(new SendNotificationViewModel()
                {
                    UserId = dispatcherRes.Data.UserId,
                    Title = "New Status",
                    Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status.Name}.",
                    //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }
                    Data = new { OrderId = model.OrderId, NotificationType = "Change Status" }

                });
                notificationCenterManageManager.Add(new NotificationCenter()
                {
                    RecieverId = dispatcherRes.Data.UserId,
                    Title = "New Status",
                    Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status.Name}.",
                    Data = model.OrderId.ToString(),
                    NotificationType = "",
                    IsRead = false,
                    OrderCode = orderRes.Data.Code
                });
            }
            notificationRes = await notificationService.SendToMultiUsers(notificationModels);
            return notificationRes;
        }

        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithNewStatus(ChangeStatusViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var UserRes = await identityService.GetById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var techIdentity = await identityService.GetById(currentUserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var orderRes = manager.Get(model.OrderId);
            var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            // string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            var roleNames = UserRes.Data.RoleNames.FirstOrDefault();
            if (roleNames == "Technician")
            {
                if (dispatcherRes != null && dispatcherRes.Data != null)
                {
                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        UserId = dispatcherRes.Data.UserId,
                        Title = "New Status",
                        Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status.Name}.",
                        //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change Status" }
                        //Data = new { OrderId = model.OrderId, NotificationType = "Change Status" }                        

                    });
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = dispatcherRes.Data.UserId,
                        Title = "New Status",
                        Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Change Status",
                        IsRead = false,
                        CurrentUserId = techIdentity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                    notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                }
            }
            else if (roleNames == "Dispatcher")
            {
                if (dispatcherRes != null && dispatcherRes.Data != null)
                {
                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        UserId = dispatcherRes.Data.UserId,
                        Title = "New Status",
                        Body = $"Dispatcher {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status.Name}.",
                        //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                        //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }//,
                        // Data = new { OrderId = model.OrderId, NotificationType = "Change Status" }    
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change Status" }
                    });
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = dispatcherRes.Data.UserId,
                        Title = "New Status",
                        Body = $"Dispatcher {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "Change Status",
                        IsRead = false,
                        CurrentUserId = techIdentity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                    notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                }
            }

            return notificationRes;
        }
        //Update Order
        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithUpdateStatus(OrderViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var UserRes = await identityService.GetById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var techIdentity = await identityService.GetById(currentUserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            var orderRes = manager.Get(model.Id);
            var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            //string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            //var jsonOrder = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            var roleNames = UserRes.Data.RoleNames.FirstOrDefault();
            if (roleNames == "Technician")
            {
                if (orderRes.Data.TeamId != 0)
                {
                    var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                    if (teamMembers != null && teamMembers.Data != null)
                    {
                        foreach (var item in teamMembers.Data)
                        {
                            notificationModels.Add(new SendNotificationViewModel()
                            {
                                UserId = item.UserId,
                                Title = "Update Status",
                                Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                                //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }//,
                                // Data = new { OrderId = model.Id, NotificationType = "Update Order Status" }
                                Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Update Order Status" }



                            });
                            notificationCenterManageManager.Add(new NotificationCenter()
                            {
                                RecieverId = item.UserId,
                                Title = "Update Status",
                                Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                Data = model.Id.ToString(),
                                NotificationType = "Update Order Status",
                                IsRead = false,
                                CurrentUserId = techIdentity.Data?.UserName,
                                OrderCode = orderRes.Data.Code
                            });
                        }
                    }
                }
                if (orderRes.Data.TeamId == null)
                {
                    var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                    if (teamMembers != null && teamMembers.Data != null)
                    {
                        foreach (var item in teamMembers.Data)
                        {
                            notificationModels.Add(new SendNotificationViewModel()
                            {
                                UserId = dispatcherRes.Data.UserId,
                                Title = "Update Status",
                                Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                Data = new { OrderId = model.Id, NotificationType = "Update Order Status" }
                                //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                                //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }//,
                                //   TeamId= model.TeamId

                            });
                            notificationCenterManageManager.Add(new NotificationCenter()
                            {
                                RecieverId = dispatcherRes.Data.UserId,
                                Title = "Update Status",
                                Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                Data = model.Id.ToString(),
                                NotificationType = "",
                                IsRead = false,
                                CurrentUserId = techIdentity.Data?.UserName,
                                OrderCode = orderRes.Data.Code
                            });
                        }
                    }
                }
                notificationRes = await notificationService.SendToMultiUsers(notificationModels);

            }
            if (roleNames == "Dispatcher")
            {
                if (dispatcherRes != null && dispatcherRes.Data != null)
                {
                    if (orderRes.Data.TeamId != null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModels.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Update Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }//,
                                    //  Data = new { OrderId = model.Id, NotificationType = "Update Order Status" }
                                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Update Order Status" }



                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Update Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    Data = model.Id.ToString(),
                                    NotificationType = "",
                                    IsRead = false,
                                    CurrentUserId = techIdentity.Data?.UserName,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                    if (orderRes.Data.TeamId == null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModels.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Update Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder) }//,
                                    //   Data = new { OrderId = model.Id, NotificationType = "Update Order Status" }
                                    Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Update Order Status" }


                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Update Status",
                                    Body = $"Dispatcher {techIdentity.Data?.FullName} Updated The status of order No. {orderRes.Data.Code} to {orderRes.Data.Status?.Name ?? ""}.",
                                    Data = model.Id.ToString(),
                                    NotificationType = "",
                                    IsRead = false,
                                    CurrentUserId = techIdentity.Data?.UserName,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                    notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                }
            }

            return notificationRes;
        }
        //Bulk Assign Notifications
        private async Task<ProcessResultViewModel<string>> NotifyTeamMembersandDispatcherWithBulkAssign(BulkAssignViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            //var UserRes = await identityService.GetUserById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var techIdentity = await identityService.GetById(currentUserId);
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            List<SendNotificationViewModel> notificationsForDispatcherOfTeam = new List<SendNotificationViewModel>();
            var orderRes = manager.Get(model.OrderId);
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            //string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
            var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            var dispIdentity = await identityService.GetById(dispatcherRes.Data?.UserId);
            var roleNames = techIdentity.Data.RoleNames.FirstOrDefault();
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            if (roleNames == "Dispatcher")
            {
                if (orderRes.Data.TeamId != null)
                {
                    var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                    if (teamMembers.Data.Count > 0)
                    {
                        foreach (var item in teamMembers.Data)
                        {
                            notificationModels.Add(new SendNotificationViewModel()
                            {
                                UserId = item.UserId,
                                Title = "New Bulk Status",
                                Body = $"Dispatcher {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",
                                Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "New Bulk Status" }

                                //Data = new { OrderId = model.OrderId, NotificationType = "New Bulk Status" }
                            });
                            notificationCenterManageManager.Add(new NotificationCenter()
                            {
                                RecieverId = item.UserId,
                                Title = "New Bulk Status",
                                Body = $"Dispatcher {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",
                                //Body = $"Dispatcher {dispIdentity.Data?.FullName } changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                                Data = model.OrderId.ToString(),
                                NotificationType = "New Bulk Status",
                                IsRead = false,
                                CurrentUserId = techIdentity.Data?.UserName,
                                OrderCode = orderRes.Data.Code
                            });
                        }
                    }

                }
                if (orderRes.Data.TeamId == null)
                {
                    //var jsonOrder = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                    if (dispatcherRes != null && dispatcherRes.Data != null)
                    {
                        notificationModels.Add(new SendNotificationViewModel()
                        {
                            UserId = dispatcherRes.Data.UserId,
                            Title = "New Bulk Status",
                            Body = $"Dispatcher {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",
                            //Data = new { OrderId = model.OrderId, NotificationType = "New Bulk Status" }
                            //Body = $"Dispatcher {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                            //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                            Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "New Bulk Status" }
                        });
                        notificationCenterManageManager.Add(new NotificationCenter()
                        {
                            RecieverId = dispatcherRes.Data.UserId,
                            Title = "New Bulk Status",
                            Body = $"Dispatcher {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",
                            //Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                            Data = model.OrderId.ToString(),
                            NotificationType = "",
                            IsRead = false,
                            OrderCode = orderRes.Data.Code
                        });
                    }
                }
                notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                if (model.TeamId != null)
                {
                    notificationsForDispatcherOfTeam.Add(new SendNotificationViewModel()
                    {
                        UserId = dispatcherRes.Data.UserId,
                        Title = "New Bulk Status",
                        Body = $"Dispatcher {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to your Team.",
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "New Bulk Status" }

                        //Data = new { OrderId = model.OrderId, NotificationType = "New Bulk Status" }
                    });
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = dispatcherRes.Data.UserId,
                        Title = "New Bulk Status",
                        Body = $"Dispatcher {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to your Team.",
                        //Body = $"Dispatcher {dispIdentity.Data?.FullName } changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "New Bulk Status",
                        IsRead = false,
                        CurrentUserId = techIdentity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                }
                notificationRes = await notificationService.SendToMultiUsers(notificationsForDispatcherOfTeam);
            }
            else if (roleNames == "Supervisor")
            {
                if (orderRes.Data.TeamId != null)
                {
                    var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                    if (teamMembers.Data.Count != 0)
                    {
                        foreach (var item in teamMembers.Data)
                        {
                            notificationModels.Add(new SendNotificationViewModel()
                            {
                                UserId = item.UserId,
                                Title = "New Bulk Status",
                                Body = $"Supervisor {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",
                                Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "New Bulk Status" }

                                //Data = new { OrderId = model.OrderId, NotificationType = "New Bulk Status" }
                            });
                            notificationCenterManageManager.Add(new NotificationCenter()
                            {
                                RecieverId = item.UserId,
                                Title = "New Bulk Status",
                                Body = $"Supervisor {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",
                                //Body = $"Dispatcher {dispIdentity.Data?.FullName } changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                                Data = model.OrderId.ToString(),
                                NotificationType = "New Bulk Status",
                                IsRead = false,
                                CurrentUserId = techIdentity.Data?.UserName,
                                OrderCode = orderRes.Data.Code
                            });
                        }
                    }

                }
                if (orderRes.Data.TeamId == null)
                {
                    //string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
                    //var jsonOrder = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                    if (dispatcherRes != null && dispatcherRes.Data != null)
                    {
                        notificationModels.Add(new SendNotificationViewModel()
                        {
                            UserId = dispatcherRes.Data.UserId,
                            Title = "New Bulk Status",
                            Body = $"Supervisor {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",
                            //Data = new { OrderId = model.OrderId, NotificationType = "New Bulk Status" }
                            Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "New Bulk Status" }

                            //Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                            //Data = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })
                            // Data = new { OrderId = order.Id, NotificationType = "New Order" }
                            //Data = //new { order = JsonConvert.DeserializeObject(jsonOrder) }
                        });
                        notificationCenterManageManager.Add(new NotificationCenter()
                        {
                            RecieverId = dispatcherRes.Data.UserId,
                            Title = "New Bulk Status",
                            Body = $"Supervisor {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to you.",

                            //Body = $"Technician {techIdentity.Data?.FullName} changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                            Data = model.OrderId.ToString(),
                            NotificationType = "New Bulk Status",
                            //NotificationType = "",
                            IsRead = false,
                            OrderCode = orderRes.Data.Code
                        });
                    }

                }
                notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                if (model.TeamId != null)
                {
                    notificationsForDispatcherOfTeam.Add(new SendNotificationViewModel()
                    {
                        UserId = dispatcherRes.Data.UserId,
                        Title = "New Bulk Status",
                        Body = $"Supervisor {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to your Team.",
                        Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "New Bulk Status" }

                        //Data = new { OrderId = model.OrderId, NotificationType = "New Bulk Status" }
                    });
                    notificationCenterManageManager.Add(new NotificationCenter()
                    {
                        RecieverId = dispatcherRes.Data.UserId,
                        Title = "New Bulk Status",
                        Body = $"Supervisor {techIdentity.Data?.FullName } Assigned order No. {orderRes.Data.Code} to your Team.",
                        //Body = $"Dispatcher {dispIdentity.Data?.FullName } changed The status of order No. {orderRes.Data.Code} to {orderRes.Data.StatusName}.",
                        Data = model.OrderId.ToString(),
                        NotificationType = "New Bulk Status",
                        IsRead = false,
                        CurrentUserId = techIdentity.Data?.UserName,
                        OrderCode = orderRes.Data.Code
                    });
                }
                notificationRes = await notificationService.SendToMultiUsers(notificationsForDispatcherOfTeam);
            }
            return notificationRes;
        }


        [HttpPost]
        [Route("NotifyDispatcherWithOrderChangeRank")]
        public async Task<ProcessResultViewModel<string>> NotifyDispatcherWithOrderChangeRank([FromBody] ChangeRankInTeamViewModel model)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var techIdentity = await identityService.GetById(currentUserId);
            //var technicianRes = await technicianService.GetTechnicianByUserId(currentUserId);
            var orderRes = manager.Get(model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key);
            var dispatcher = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);//TODO Refactor issue
            SendNotificationViewModel notificationModel;
            var userId = dispatcher.Data.UserId;
            var order = Mapper.Map<OrderViewModel>(orderRes.Data);
            var jsonOrder = JsonConvert.SerializeObject(order, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            //string jsonOrder = Newtonsoft.Json.JsonConvert.SerializeObject(order);
            notificationModel = new SendNotificationViewModel()
            {
                UserId = userId,
                Title = "Change order priority",
                Body = $"Technician {techIdentity.Data?.FullName} requests changing the priority of order No. {orderRes.Data.Code} to be able to work on it.",
                Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change order priority", rankInTeamModel = model },
                TeamId = orderRes.Data.TeamId ?? 0//TODO Refactor issue
            };
            notificationCenterManageManager.Add(new NotificationCenter()
            {
                RecieverId = userId,
                Title = "Change order priority",
                Body = $"Technician {techIdentity.Data?.FullName} requests changing the priority of order No. {orderRes.Data.Code} to be able to work on it.",
                Data = JsonConvert.SerializeObject(model),
                NotificationType = "Change order priority",
                IsRead = false,
                TeamId = orderRes.Data.TeamId,
                CurrentUserId = techIdentity.Data?.UserName,
                OrderCode = orderRes.Data.Code
            });

            notificationRes = await notificationService.Send(notificationModel);
            return notificationRes;
        }


        [HttpPut]
        [Route("ChangeOrderRank/{acceptChange}")]
        public async Task<ProcessResult<bool>> ChangeOrderRankWeb([FromRoute]bool acceptChange, [FromBody] ChangeRankInTeamViewModel model)
        {
            if (model == null || model.TeamId == null || model.TeamId == 0 || model.Orders == null || model.Orders.Count < 1)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "team id and order id and rank are required");
            }
            var orderId = model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key;
            if (acceptChange)
            {
                var teamOrders = manager.GetAllQuerable().Data.Where(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                var changeRes = await _orderService.ChangeOrderRankAsyncForApproveOrder(await teamOrders.ToListAsync(), model.Orders, orderId);
                AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = true, OrderId = orderId };
                var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                return ProcessResultHelper.Succedded<bool>(true);
                //teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                //if (changeRes.IsSucceeded && changeRes.Data == true)
                //{
                //    await _orderService.AddAction(teamOrders.Data.FirstOrDefault(), Request, OrderActionType.ChangeTeamRank, null, null, model.TeamId);
                //    AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = true, OrderId = orderId };
                //    //var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                //    return ProcessResultHelper.Succedded<bool>(true);
                //}
                //else
                //{
                //    return ProcessResultHelper.Failed<bool>(false, changeRes.Exception, null, ProcessResultStatusCode.InvalidValue);
                //}

            }
            else
            {
                AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = false, OrderId = orderId };
                var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                return ProcessResultHelper.Succedded<bool>(true);
            }

            return ProcessResultHelper.Failed<bool>(false, null, "something worng while getting team orders", ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        }




        [HttpPut]
        [Route("ChangeTeamRankByDrag/{acceptChange}")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        public async Task<ProcessResult<bool>> ChangeTeamRankByDrag([FromRoute]bool acceptChange, [FromBody] ChangeRankInTeamByDragViewModel model)
        {
            if (model == null || model.TeamId == 0 || model.TeamId == null || model.Orders == null || model.Orders.Count < 1)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "team id and order id and rank are required");
            }
            if (acceptChange)
            {

                var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
                {
                    var oldRankInTeam = teamOrders.Data.FirstOrDefault(x => x.Id == model.orderId)?.RankInTeam;
                    if (oldRankInTeam != model.Orders[model.orderId])
                    {
                        var changeRes = await _orderService.ChangeOrderRankAsync(teamOrders.Data, model.Orders, model.orderId);
                        //teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                        //if (changeRes.IsSucceeded && changeRes.Data == true)
                        //{
                        //    await _orderService.AddAction(teamOrders.Data.FirstOrDefault(x => x.Id == model.orderId), Request, OrderActionType.ChangeTeamRank, null, null, model.TeamId);
                        //    AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = true, OrderId = model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key };
                        //    var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                        //    return ProcessResultHelper.Succedded<bool>(true);
                        //}
                        //else
                        //{
                        //    return ProcessResultHelper.Failed<bool>(false, changeRes.Exception, null, ProcessResultStatusCode.InvalidValue);
                        //}
                    }
                    else // to make validations 
                    {
                        return ProcessResultHelper.Failed<bool>(false, teamOrders.Exception, null, ProcessResultStatusCode.InvalidValue);
                    }
                }
            }
            else
            {
                AcceptenceViewModel acceptenceModel = new AcceptenceViewModel { AcceptenceFlag = false, OrderId = model.Orders.OrderBy(x => x.Value).FirstOrDefault().Key };
                var NotificationRes = await NotifyTechnicianWithOrderAcceptencechange(acceptenceModel);
                return ProcessResultHelper.Succedded<bool>(true);
            }

            return ProcessResultHelper.Failed<bool>(false, null, "something worng while getting team orders", ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        }


        //[HttpPut]
        //[Route("ChangeOrderRankWithoutNotification")]
        //public ProcessResult<bool> ChangeOrderRankWithoutNotification([FromBody] ChangeRankInTeamViewModel model)
        //{
        //    if (model == null || model.TeamId == 0 || model.Orders == null || model.Orders.Count < 1)
        //    {
        //        return ProcessResultHelper.Failed<bool>(false, null, "team id and order id and rank are required");
        //    }

        //    var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
        //    if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
        //    {
        //        var changeRes = manager.ChangeOrderRank(teamOrders.Data, model.Orders);
        //        teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
        //        if (changeRes.IsSucceeded && changeRes.Data == true)
        //        {
        //            //await AddAction(teamOrders.Data.FirstOrDefault(), OrderActionType.ChangeTeamRank, null, null);
        //            return ProcessResultHelper.Succedded<bool>(true);
        //        }
        //        else
        //        {
        //            return ProcessResultHelper.Failed<bool>(false, changeRes.Exception);
        //        }
        //    }

        //    return ProcessResultHelper.Failed<bool>(false, null, "something worng while getting team orders", ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        //}
        //////

        ////

        [HttpPut]
        [Route("ChangeOrderRankWithoutNotification")]
        [Authorize(Roles = StaticRoles.Dispatcher)]
        public async Task<ProcessResult<bool>> ChangeOrderRankWithoutNotification([FromBody] ChangeRankInTeamViewModel model)
        {
            List<int> workingStatusId = StaticAppSettings.WorkingStatusId;
            if (model == null || model.TeamId == 0 || model.TeamId == null || model.Orders == null || model.Orders.Count < 1)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "team id and order id and rank are required");
            }
            int OrderNovalue = 0;
            int OrderInvalidvalue = 0;

            OrderStatusEnum? OrderNovalueStatus = null;
            OrderStatusEnum? OrderInvalidvalueStatus = null;
            //////
            for (int i = 0; i < model.Orders.Count; i++)
            {
                if (model.Orders.ElementAt(i).Value == 1)
                {
                    OrderNovalue = model.Orders.ElementAt(i).Key;
                }
            }
            for (int i = 0; i < model.Orders.Count; i++)
            {
                if (model.Orders.ElementAt(i).Value == 0)
                {
                    OrderInvalidvalue = model.Orders.ElementAt(i).Key;
                }
            }
            ///
            var InvalidStatus = manager.GetAll(x => x.TeamId == model.TeamId && x.Id == OrderNovalue);
            var InvalidStatus2 = manager.GetAll(x => x.TeamId == model.TeamId && x.Id == OrderInvalidvalue);
            // var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            // var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            var teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
            foreach (var item in InvalidStatus.Data)
            {
                OrderNovalueStatus = (OrderStatusEnum)item.StatusId;
            }
            foreach (var item in InvalidStatus2.Data)
            {
                OrderInvalidvalueStatus = (OrderStatusEnum)item.StatusId;
            }
            if (teamOrders.IsSucceeded && teamOrders.Data != null && teamOrders.Data.Count > 0)
            {
                if (OrderNovalueStatus == OrderStatusEnum.On_Travel || OrderNovalueStatus == OrderStatusEnum.Reached || OrderNovalueStatus == OrderStatusEnum.Started || OrderInvalidvalueStatus == OrderStatusEnum.On_Travel || OrderInvalidvalueStatus == OrderStatusEnum.Reached || OrderInvalidvalueStatus == OrderStatusEnum.Started)
                {
                    foreach (var teamorder in teamOrders.Data)
                    {
                        if (workingStatusId.Contains(teamorder.StatusId))
                        {
                            if (OrderNovalueStatus == OrderStatusEnum.On_Travel || OrderNovalueStatus == OrderStatusEnum.Reached || OrderNovalueStatus == OrderStatusEnum.Started)
                            {
                                var changeRes = manager.ChangeOrderRank2(teamOrders.Data, model.Orders, teamorder.Id, OrderInvalidvalue);
                                teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                                if (changeRes.IsSucceeded && changeRes.Data == true)
                                {
                                    //await AddAction(teamOrders.Data.FirstOrDefault(), OrderActionType.ChangeTeamRank, null, null);
                                    return ProcessResultHelper.Succedded<bool>(true);
                                }
                                else
                                {
                                    return ProcessResultHelper.Failed<bool>(false, changeRes.Exception);
                                }
                            }
                            else
                            {
                                var changeRes = await _orderService.ChangeOrderRankAsync(teamOrders.Data, model.Orders, model.orderId);
                                //teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                                //if (changeRes.IsSucceeded && changeRes.Data == true)
                                //{
                                //    //await AddAction(teamOrders.Data.FirstOrDefault(), OrderActionType.ChangeTeamRank, null, null);
                                //    return ProcessResultHelper.Succedded<bool>(true);
                                //}
                                //else
                                //{
                                //    return ProcessResultHelper.Failed<bool>(false, changeRes.Exception);
                                //}
                            }
                        }
                    }

                }
                else
                {
                    var changeRes = await _orderService.ChangeOrderRankAsync(teamOrders.Data, model.Orders, model.orderId);
                    //teamOrders = manager.GetAll(x => x.TeamId == model.TeamId && !StaticAppSettings.ArchivedStatusId.Contains(x.StatusId));
                    if (changeRes.IsSucceeded && changeRes.Data == true)
                    {
                        //await AddAction(teamOrders.Data.FirstOrDefault(), OrderActionType.ChangeTeamRank, null, null);
                        return ProcessResultHelper.Succedded<bool>(true);
                    }
                    else
                    {
                        return ProcessResultHelper.Failed<bool>(false, changeRes.Exception);
                    }
                }


            }

            return ProcessResultHelper.Failed<bool>(false, null, "something worng while getting team orders", ProcessResultStatusCode.Failed, "ChangeTeamRank Controller");
        }
        //////

        [HttpGet]
        [Route("GetDispatcherOrders")]
        public ProcessResult<List<DispatcherOrdersViewModel>> GetDispatcherOrders(int SupervisorId)
        {
            var dispatchersOrdersList = new List<DispatcherOrdersViewModel>();
            var orders = manager.GetDispatcherOrders(SupervisorId, StaticAppSettings.InitialStatus.Id);
            if (orders.Data != null)
            {
                var dispatchersOrders = orders.Data.GroupBy(x => new { x.DispatcherId }).ToList();
                foreach (var item in dispatchersOrders)
                {
                    foreach (var order in item)
                    {
                        var orderList = new List<OrderViewModel>();
                        var orderItem = Mapper.Map<OrderViewModel>(order);
                        orderList.Add(orderItem);

                        var dispatchersOrder = new DispatcherOrdersViewModel
                        {
                            DispatcherId = item.Key.DispatcherId ?? 0,//TODO Refactor issue
                            DispatcherName = order.Dispatcher.User.FirstName + " " + order.Dispatcher.User.FirstName,
                            Orders = orderList
                        };
                        dispatchersOrdersList.Add(dispatchersOrder);
                    }
                }
                return ProcessResultHelper.Succedded<List<DispatcherOrdersViewModel>>(dispatchersOrdersList);
            }
            return ProcessResultHelper.Failed<List<DispatcherOrdersViewModel>>(null, null, "there are no dispatchers for this supervisor", ProcessResultStatusCode.Failed, "GetDispatcherOrders Controller");
        }
        //[HttpPut]
        //[Route("UpdateOrdersWithOnholdCount")]
        //public async Task<ProcessResult<bool>> UpdateOrdersWithOnholdCOunt()
        //{
        //     var OnholdOrdersActionsRes = actionManager.GetAll(x=>x.StatusId==6);
        //     if (OnholdOrdersActionsRes.IsSucceeded && OnholdOrdersActionsRes.Data.Count()>0)
        //     {
        //        foreach (var order in OnholdOrdersActionsRes.Data)
        //        {
        //            var orderRes = manager.Get(order.OrderId);
        //            orderRes.Data.OnHoldCount = orderRes.Data.OnHoldCount + 1;
        //            var changeOnholdCount = manager.Update(orderRes.Data);
        //        }
        //         return ProcessResultHelper.Succedded<bool>(true);
        //     }
        //     else
        //     {
        //         return ProcessResultHelper.Failed<bool>(false, OnholdOrdersActionsRes.Exception);
        //     }
        //}


        private async Task<ProcessResultViewModel<string>> NotifyTechnicianWithOrderAcceptencechange(AcceptenceViewModel model)
        {
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            // string currentUserId = "7138922f-dc6f-4c59-a6a3-538453e866d1";
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<string>(null, "Unauthorized");
            }
            var UserRes = await identityService.GetById(currentUserId);
            var techIdentity = await identityService.GetById(currentUserId);
            ProcessResultViewModel<string> notificationRes = null;
            var orderRes = manager.Get(model.OrderId);
            var dispatcherRes = dispatcherService.GetDispatcherById(orderRes.Data.DispatcherId ?? 0);
            var dispIdentity = await identityService.GetById(dispatcherRes.Data.UserId);
            List<SendNotificationViewModel> notificationModelLst = new List<SendNotificationViewModel>();
            //var jsonOrder = JsonConvert.SerializeObject(orderRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            var roleNames = UserRes.Data.RoleNames.FirstOrDefault();
            if (roleNames == "Dispatcher")
            {
                if (model.AcceptenceFlag)
                {
                    if (orderRes.Data.TeamId != null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModelLst.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Dispatcher {dispIdentity.Data?.FullName} changed the priority of order No. {orderRes.Data.Code}.",
                                    Data = new { OrderId = model.OrderId, NotificationType = "Change order priority" }
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change order priority" }//,
                                    // TeamId = orderRes.Data.TeamId
                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Dispatcher {dispIdentity.Data?.FullName} changed the priority of order No. {orderRes.Data.Code}.",
                                    Data = orderRes.Data.Id.ToString(),
                                    NotificationType = "Change order priority",
                                    IsRead = false,
                                    CurrentUserId = techIdentity.Data?.UserName,
                                    IsActionTaken = true,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                }
                else
                {
                    if (orderRes.Data.TeamId != null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModelLst.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Dispatcher {dispIdentity.Data?.FullName} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                    Data = new { OrderId = model.OrderId, NotificationType = "Change order priority" }
                                    // Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change order priority" }
                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Dispatcher {dispIdentity.Data?.FullName} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                    Data = orderRes.Data.Id.ToString(),
                                    NotificationType = "Change order priority",
                                    IsRead = false,
                                    IsActionTaken = true,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                }
                if (notificationModelLst.Count > 0)
                {
                    notificationRes = await notificationService.SendToMultiUsers(notificationModelLst);
                }
            }
            if (roleNames == "Technician")
            {
                if (model.AcceptenceFlag)
                {
                    if (orderRes.Data.TeamId != null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModelLst.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Technician {techIdentity.Data?.FullName} changed the priority of order No. {orderRes.Data.Code}.",
                                    Data = new { OrderId = model.OrderId, NotificationType = "Change order priority" }
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change order priority" }//,
                                    // TeamId = orderRes.Data.TeamId
                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Technician {techIdentity.Data?.FullName} changed the priority of order No. {orderRes.Data.Code}.",
                                    Data = orderRes.Data.Id.ToString(),
                                    NotificationType = "Change order priority",
                                    IsRead = false,
                                    CurrentUserId = techIdentity.Data?.UserName,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                }
                else
                {
                    if (orderRes.Data.TeamId != null)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModelLst.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Technician {techIdentity.Data?.FullName} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                    Data = new { OrderId = model.OrderId, NotificationType = "Change order priority" }
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change order priority" }
                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Technician {techIdentity.Data?.FullName} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                    Data = orderRes.Data.Id.ToString(),
                                    NotificationType = "Change order priority",
                                    IsRead = false,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                }
                if (notificationModelLst.Count > 0)
                {
                    notificationRes = await notificationService.SendToMultiUsers(notificationModelLst);
                }
            }
            if (roleNames == "Supervisor")
            {
                if (model.AcceptenceFlag)
                {
                    if (orderRes.Data.TeamId != 0)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModelLst.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Supervisor {techIdentity.Data?.FullName} changed the priority of order No. {orderRes.Data.Code}.",
                                    Data = new { OrderId = model.OrderId, NotificationType = "Change order priority" }
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change order priority" }//,
                                    // TeamId = orderRes.Data.TeamId
                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Supervisor {techIdentity.Data?.FullName} changed the priority of order No. {orderRes.Data.Code}.",
                                    Data = orderRes.Data.Id.ToString(),
                                    NotificationType = "Change order priority",
                                    IsRead = false,
                                    CurrentUserId = techIdentity.Data?.UserName,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                }
                else
                {
                    if (orderRes.Data.TeamId != 0)
                    {
                        var teamMembers = await teamMembersService.GetMemberUsersByTeamId(orderRes.Data.TeamId ?? 0);//TODO Refactor issue
                        if (teamMembers != null && teamMembers.Data != null)
                        {
                            foreach (var item in teamMembers.Data)
                            {
                                notificationModelLst.Add(new SendNotificationViewModel()
                                {
                                    UserId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Technician {techIdentity.Data?.FullName} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                    Data = new { OrderId = model.OrderId, NotificationType = "Change order priority" }
                                    //Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Change order priority" }
                                });
                                notificationCenterManageManager.Add(new NotificationCenter()
                                {
                                    RecieverId = item.UserId,
                                    Title = "Change order priority",
                                    Body = $"Technician {techIdentity.Data?.FullName} rejected changing the priority of order No. {orderRes.Data.Code}.",
                                    Data = orderRes.Data.Id.ToString(),
                                    NotificationType = "Change order priority",
                                    IsRead = false,
                                    OrderCode = orderRes.Data.Code
                                });
                            }
                        }
                    }
                }
                if (notificationModelLst.Count > 0)
                {
                    notificationRes = await notificationService.SendToMultiUsers(notificationModelLst);
                }
            }
            return notificationRes;
        }

        [HttpPost]
        [Route("OldPrintOut")]
        [Authorize(Roles = StaticRoles.Closed)]
        public async Task<ProcessResultViewModel<List<PrintOutResultViewModel>>> OldPrintOut([FromBody] PrintOutFilterViewModel model)
        {
            //
            ProcessResultViewModel<List<PrintOutResultViewModel>> result = new ProcessResultViewModel<List<PrintOutResultViewModel>>();
            List<PrintOutResultViewModel> modelLst = new List<PrintOutResultViewModel>();
            List<ForemenWithOrders> formanWithOrderLst = new List<ForemenWithOrders>();
            List<ForemenWithOrders> OrdersRes = new List<ForemenWithOrders>();
            Dictionary<int?, string> dicTeamIdTechName = manager.GetDefaultTechniciansForOrders();
            try
            {
                if (model.IncludeOpenOrders)
                {
                    if (model.teamsId == null && model.DateFrom != null && model.DateTo != null)
                    {
                        // OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (x.StatusId == StaticAppSettings.OnHoldStatus.Id)).Data.GroupBy(x => new { x.AreaId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = group.FirstOrDefault().AreaName, PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue

                        OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId && (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo)) && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).Data.GroupBy(x => new { x.AreaId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = group.FirstOrDefault().Area?.Name, PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         //  OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId && (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo)) && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).Data.GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = key.AreaName, PrevTeamId = key.PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                    }
                    else if (model.teamsId != null && model.DateFrom == null && model.DateTo == null)
                    {
                        //   OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (model.teamsId.Contains(x.PrevTeamId ?? 0) || x.PrevTeamId == null) && x.TeamId == null && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).Data.GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = key.AreaName, PrevTeamId = key.PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue

                        OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) &&
                        (model.teamsId.Contains(x.PrevTeamId ?? 0) || x.PrevTeamId == null) && x.TeamId == null
                        && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).
                            Data.GroupBy(x => new { x.AreaId }, (key, group) =>
                            new ForemenWithOrders
                            {
                                AreaId = key.AreaId,
                                AreaName = group.FirstOrDefault().Area?.Name,
                                PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0,
                                Orders = group.Select(q => Mapper.Map<OrderViewModel>(q))
                            }).ToList();//TODO Refactor issue
                    }
                    else if (model.teamsId == null && model.DateFrom == null && model.DateTo == null)
                    {
                        //  OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).Data.GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = key.AreaName, PrevTeamId = key.PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                        OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).Data.GroupBy(x => new { x.AreaId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = group.FirstOrDefault().Area?.Name, PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                    }
                    else if (model.teamsId != null && model.DateFrom != null && model.DateTo != null)
                    {
                        //  OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId)&&(x.SAP_CreatedDate>=model.DateFrom) &&(x.SAP_CreatedDate<=model.DateTo )&& (model.teamsId.Contains(x.PrevTeamId ?? 0) || x.PrevTeamId == null) && x.TeamId == null && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).Data.GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = key.AreaName, PrevTeamId = key.PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue

                        OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId && (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo)) && (model.teamsId.Contains(x.PrevTeamId ?? 0) || x.PrevTeamId == null) && x.TeamId == null && (StaticAppSettings.UnassignedStatusId.Contains(x.StatusId))).Data.GroupBy(x => new { x.AreaId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = group.FirstOrDefault().Area?.Name, PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                    }

                    var orderIds = OrdersRes.SelectMany(x => x.Orders.Select(y => y.Id)).ToList();
                    var prevIds = OrdersRes.SelectMany(x => x.Orders.Where(e => x != null).Select(y => y.PrevTeamId ?? 0)).ToList();
                    //     var prevIds = OrdersRes.SelectMany(x => x.Orders.Where(e => x != null).Select(y => (int)y.PrevTeamId)).ToList();
                    var teamResList = teamService.GetTeamsByIds(prevIds);
                    var foreman = teamResList.Data.Select(x => x.ForemanName).ToList();
                    var supervisors = OrdersRes.SelectMany(x => x.Orders.Select(y => y.SupervisorName)).ToList();
                    var dispatchers = OrdersRes.SelectMany(x => x.Orders.Select(y => y.DispatcherName)).ToList();


                    var orderActions = actionManager.GetAll(x => orderIds.Contains((int)x.OrderId)).Data.GroupBy(x => x.OrderId).Select(x => x.Last()).ToList();
                    var createdUsers = orderActions.Where(x => x.OrderId != null && x.CreatedUser != null).Select(x => x.CreatedUser).ToList();
                    // var currentUserIds = await identityService.GetUserByUsernames(supervisors.Union(dispatchers).Union(createdUsers).ToList());
                    var currentUserIds = await identityService.GetByUserNames(supervisors.Union(dispatchers).Union(foreman).ToList());


                    foreach (var item in OrdersRes.ToList())
                    {
                        TeamViewModel teamRes = new TeamViewModel();
                        List<OrderViewModel> itemorders = new List<OrderViewModel>();
                        string name = "";
                        if (item.PrevTeamId != 0)
                        {
                            teamRes = teamResList.Data?.FirstOrDefault(x => x.Id == item.PrevTeamId);
                            name = dicTeamIdTechName[teamRes.Id];
                        }
                        //        var name = dicTeamIdTechName[teamRes.Id];


                        foreach (var orderItem in item.Orders)
                        {
                            orderItem.TeamName = name;
                            orderItem.SupervisorName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.SupervisorName && x.RoleNames.Contains("Supervisor"))?.FullName;
                            //orderItem.DispatcherName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.DispatcherName && x.RoleNames.Contains("Dispatcher"))?.FullName;
                            var dispRes = _context.Dispatchers.FirstOrDefault(x => x.Id == orderItem.DispatcherId);
                            orderItem.DispatcherName = _context.Users.FirstOrDefault(x => x.Id == dispRes.UserId).FullName;

                            foreach (var actionItem in orderActions)
                            {
                                //    //orderItem.TechId = actionItem.CreatedUserId;
                                //    //orderItem.TechName = currentUserIds.Data.FirstOrDefault(x => x.UserName == actionItem?.CreatedUser && x.RoleNames.Contains("Technician"))?.FullName;
                                //    //orderItem.SupervisorName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.SupervisorName && x.RoleNames.Contains("Supervisor"))?.FullName;
                                //    //orderItem.DispatcherName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.DispatcherName && x.RoleNames.Contains("Dispatcher"))?.FullName;
                                //    orderItem.SubStatusName = actionItem.SubStatusName;
                                orderItem.TechNotes = actionItem.ServeyReport;
                            }
                            itemorders.Add(orderItem);
                            if (item.AreaName == null)
                                item.AreaName = orderItem.SAP_AreaName;
                        }
                        // var teamRes = teamResList.Data?.FirstOrDefault(x => x.Id == item.PrevTeamId);
                        if (teamRes != null && item.PrevTeamId != 0)
                        {

                            if (teamRes.ForemanId != 0)
                            {
                                formanWithOrderLst.Add(new ForemenWithOrders()
                                {
                                    ForemanId = teamRes.ForemanId,
                                    ForemanName = currentUserIds.Data.FirstOrDefault(x => x.UserName == teamRes.ForemanName && x.RoleNames.Contains("Foreman"))?.FullName,
                                    PrevTeamId = item.PrevTeamId,
                                    AreaId = item.AreaId,
                                    AreaName = item.AreaName,
                                    Orders = itemorders.Where(c => c.AreaId == item.AreaId)
                                });
                            }
                        }
                        //else
                        //{
                        //    if (teamRes.ForemanId != 0)
                        //    {
                        //        formanWithOrderLst.Add(new ForemenWithOrders()
                        //        {
                        //            ForemanId = 0,
                        //            ForemanName = "",
                        //            AreaId = item.AreaId,
                        //            AreaName = item.AreaName,
                        //            Orders = itemorders.Where(c => c.AreaId == item.AreaId)
                        //            // Orders = item.Orders
                        //        });
                        //    }
                        //}
                    }

                    modelLst.Add(new PrintOutResultViewModel()
                    {
                        DispatcherId = model.DispatcherId,
                        foremenWithOrders = formanWithOrderLst
                    });
                }
                else
                {
                    if (model.teamsId == null)
                    {
                        // OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (x.StatusId == StaticAppSettings.OnHoldStatus.Id)).Data.GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = key.AreaName, PrevTeamId = key.PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                        OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (x.StatusId == StaticAppSettings.OnHoldStatus.Id)).Data.GroupBy(x => new { x.AreaId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = group.FirstOrDefault().Area?.Name, PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                    }
                    else
                    {
                        // OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (model.teamsId.Contains(x.PrevTeamId ?? 0)) && x.TeamId == null && (x.StatusId == StaticAppSettings.OnHoldStatus.Id)).Data.GroupBy(x => new { x.AreaId, x.AreaName, x.PrevTeamId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = key.AreaName, PrevTeamId = key.PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                        OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (model.teamsId.Contains(x.PrevTeamId ?? 0)) && x.TeamId == null && (x.StatusId == StaticAppSettings.OnHoldStatus.Id)).Data.GroupBy(x => new { x.AreaId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = group.FirstOrDefault().Area?.Name, PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                    }

                    var orderIds = OrdersRes.SelectMany(x => x.Orders.Select(y => y.Id)).ToList();
                    var prevIds = OrdersRes.SelectMany(x => x.Orders.Where(e => x != null).Select(y => y.PrevTeamId ?? 0)).ToList();
                    var teamResList = teamService.GetTeamsByIds(prevIds);
                    var foreman = teamResList.Data.Select(x => x.ForemanName).ToList();

                    var supervisors = OrdersRes.SelectMany(x => x.Orders.Select(y => y.SupervisorName)).ToList();
                    var dispatchers = OrdersRes.SelectMany(x => x.Orders.Select(y => y.DispatcherName)).ToList();
                    var currentUserIds = await identityService.GetByUserNames(supervisors.Union(dispatchers).Union(foreman).ToList());
                    var orderActions = actionManager.GetAll(x => orderIds.Contains((int)x.OrderId)).Data.GroupBy(x => x.OrderId).Select(x => x.Last()).ToList();
                    List<OrderViewModel> itemorders = new List<OrderViewModel>();
                    foreach (var item in OrdersRes.ToList())
                    {
                        TeamViewModel teamRes = new TeamViewModel();
                        if (item.PrevTeamId != 0)
                        {
                            teamRes = teamResList.Data?.FirstOrDefault(x => x.Id == item.PrevTeamId);
                        }
                        var name = teamRes.Name;
                        foreach (var orderItem in item.Orders.ToList())
                        {
                            orderItem.TeamName = dicTeamIdTechName[teamRes.Id];
                            orderItem.SupervisorName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.SupervisorName && x.RoleNames.Contains("Supervisor"))?.FullName;
                            orderItem.DispatcherName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.DispatcherName && x.RoleNames.Contains("Dispatcher"))?.FullName;
                            var dispRes = _context.Dispatchers.FirstOrDefault(x => x.Id == orderItem.DispatcherId);
                            orderItem.DispatcherName = _context.Users.FirstOrDefault(x => x.Id == dispRes.UserId).FullName;

                            //foreach (var actionItem in orderActions)
                            //{
                            //    orderItem.SubStatusName = actionItem.SubStatusName;
                            //    orderItem.TechNotes = actionItem.ServeyReport;
                            //}
                            if (item.AreaName == null)
                                item.AreaName = orderItem.SAP_AreaName;
                            itemorders.Add(orderItem);
                        }

                        if (teamRes != null && item.PrevTeamId != 0)
                        {
                            formanWithOrderLst.Add(new ForemenWithOrders()
                            {
                                ForemanId = teamRes.ForemanId,
                                ForemanName = currentUserIds.Data.FirstOrDefault(x => x.UserName == teamRes.ForemanName && x.RoleNames.Contains("Foreman"))?.FullName,
                                PrevTeamId = item.PrevTeamId,
                                AreaId = item.AreaId,
                                AreaName = item.AreaName,
                                Orders = itemorders.Where(c => c.AreaId == item.AreaId)
                            });
                        }
                        //else
                        //{
                        //    formanWithOrderLst.Add(new ForemenWithOrders()
                        //    {
                        //        ForemanId = 0,
                        //        ForemanName = "",
                        //        AreaId = item.AreaId,
                        //        AreaName = item.AreaName,
                        //        Orders = itemorders
                        //    });
                        //}
                    }
                    modelLst.Add(new PrintOutResultViewModel()
                    {
                        DispatcherId = model.DispatcherId,
                        foremenWithOrders = formanWithOrderLst
                    });
                }

                return ProcessResultViewModelHelper.Succedded<List<PrintOutResultViewModel>>(modelLst, "PrintOut");
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Succedded<List<PrintOutResultViewModel>>(null, ex.Message);

            }
        }

        [HttpPost]
        [Route("PrintOut")]
        [Authorize(Roles = StaticRoles.Dispatcher)]
        public async Task<ProcessResultViewModel<List<PrintOutResultViewModel>>> PrintOut([FromBody] PrintOutFilterViewModel model)
        {
            //
            ProcessResultViewModel<List<PrintOutResultViewModel>> result = new ProcessResultViewModel<List<PrintOutResultViewModel>>();
            List<PrintOutResultViewModel> modelLst = new List<PrintOutResultViewModel>();
            List<ForemenWithOrders> formanWithOrderLst = new List<ForemenWithOrders>();
            List<ForemenWithOrders> OrdersRes = new List<ForemenWithOrders>();
            Dictionary<string, string> dicTeamIdTechName2 = GetAllTechniciansUserId();
            try
            {
                var QurableOrder = _context.View_OrderPrintOut.Where(x => x.DispatcherId == model.DispatcherId);
                if (model.IncludeOpenOrders)
                {
                    QurableOrder = QurableOrder.Where(x => StaticAppSettings.UnassignedStatusId.Contains(x.StatusId));
                }
                else
                {
                    QurableOrder = QurableOrder.Where(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                }
                if (model.teamsId != null)
                {//we search in prevteam because it's open or onhold status so no current team available 
                    QurableOrder = QurableOrder.Where(x => model.teamsId.Contains(x.PrevTeamId ?? 0) || x.PrevTeamId == null);
                }
                if (model.DateFrom != null && model.DateTo != null)
                {
                    QurableOrder = QurableOrder.Where(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
                }

                var OrdersMappedViewG = QurableOrder.GroupBy(x => x.Id).ToList();

                var dispatcherObj = _IDispatcherManager.Get(model.DispatcherId);
                //all actors
                var prevIds = OrdersMappedViewG
                              .Where(x => x.FirstOrDefault().PrevTeamId != null).Select(x => x.FirstOrDefault().PrevTeamId.Value)
                              .ToList();
                var teamResList = teamService.GetTeamsWithBlockedByIds(prevIds);



                List<OrderViewModel> FinalOrder = new List<OrderViewModel>();
                //var temp= Mapper.Map<List<OrderViewModel>>(OrdersTemp);
                foreach (var item in OrdersMappedViewG)
                { //check we find tech to loop again 
                    bool TechAdded = false;

                    var first = item.FirstOrDefault();
                    OrderViewModel obj = mapper.Map<View_OrderPrintOutObject, OrderViewModel>(first);
                    //obj = Mapper.Map<OrderViewModel>(OrdItem);
                    //check area 
                    if (obj.AreaName == null || obj.AreaName == "")
                    {
                        obj.AreaName = obj.SAP_AreaName;
                    }
                    //check is there order action
                    if (item.Count() == 0)
                    {
                        FinalOrder.Add(obj);
                        continue;
                    }
                    //check if the last excuter is an technachin
                    var lastAction = item.Where(x => x.acPlatformTypeSource != null && x.acPlatformTypeSource != PlatformType.Ftp).OrderByDescending(x => x.acId).FirstOrDefault();

                    if (lastAction == null)
                    {
                        FinalOrder.Add(obj);
                        continue;
                    }

                    if (dicTeamIdTechName2.ContainsKey(lastAction.acFK_CreatedBy_Id))
                    {
                        obj.TechName = dicTeamIdTechName2[lastAction.acFK_CreatedBy_Id];
                        obj.TechNotes = lastAction.ServeyReport;
                        FinalOrder.Add(obj);
                        continue;
                    }
                    //last excuter is the dispatcher
                    else if (lastAction.acFK_CreatedBy_Id == dispatcherObj.Data.UserId)
                    {
                        var desCreatedByIds = item.Where(x => x.acFK_CreatedBy_Id != dispatcherObj.Data.UserId && x.acPlatformTypeSource != PlatformType.Ftp).OrderByDescending(x => x.acId);
                        foreach (var CretId in desCreatedByIds)
                        {
                            if (dicTeamIdTechName2.ContainsKey(CretId.acFK_CreatedBy_Id))
                            {
                                obj.TechName = dicTeamIdTechName2[CretId.acFK_CreatedBy_Id];
                                obj.TechNotes = CretId.ServeyReport;
                                FinalOrder.Add(obj);
                                TechAdded = true;
                                break;
                            }

                        }
                        if (!TechAdded)
                            FinalOrder.Add(obj);

                    }
                    //last excuter is the driver 
                    else if (_ITechnicianManager.GetAllQuerable().Data.Any(x => x.IsDriver == true && x.UserId == lastAction.acFK_CreatedBy_Id))
                    {
                        int? driverExcuterTeamId = _ITechnicianManager.GetAllQuerable()
                                              .Data
                                              .Where(x => x.IsDriver == true && x.UserId == lastAction.acFK_CreatedBy_Id)
                                              .Select(x => x.TeamId)
                                              .FirstOrDefault();
                        if (driverExcuterTeamId != null && driverExcuterTeamId != null)
                        {
                            obj.TechName = GetFirstTechnicianNameByTeamId(driverExcuterTeamId.Value);
                            obj.TechNotes = "";
                            FinalOrder.Add(obj);
                        }
                    }
                    else
                    {
                        FinalOrder.Add(obj);

                    }
                }

                OrdersRes = FinalOrder.
                    GroupBy(x => new { x.PrevTeamId, x.AreaId },
                    (key, group) => new ForemenWithOrders
                    {
                        ForemanId = GetForemanId(group.FirstOrDefault().PrevTeamId, teamResList.Data),
                        ForemanName = GetForemanName(group.FirstOrDefault().PrevTeamId, teamResList.Data),
                        AreaId = group.FirstOrDefault().AreaId,
                        AreaName = group.FirstOrDefault().AreaName,
                        PrevTeamId = group.FirstOrDefault().PrevTeamId ?? 0,
                        Orders = group.Select(q => q)
                    })
                    .ToList();
                modelLst.Add(new PrintOutResultViewModel()
                {
                    DispatcherId = model.DispatcherId,
                    foremenWithOrders = OrdersRes
                });
                return ProcessResultViewModelHelper.Succedded<List<PrintOutResultViewModel>>(modelLst, "PrintOut");
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Succedded<List<PrintOutResultViewModel>>(null, ex.Message);

            }
        }


        private int? GetForemanId(int? PrevTeamId, List<TeamIdForemanIdNameViewModel> lst)
        {
            if (PrevTeamId == null || PrevTeamId == 0)
                return 0;
            else
            {
                var obj = lst.FirstOrDefault(x => x.Id == PrevTeamId.Value);
                return obj.ForemanId == null ? 0 : obj.ForemanId;
            }
        }
        private string GetForemanName(int? PrevTeamId, List<TeamIdForemanIdNameViewModel> lst)
        {
            if (PrevTeamId == null || PrevTeamId == 0)
                return null;
            else
                return lst.FirstOrDefault(x => x.Id == PrevTeamId.Value).ForemanName;
        }
        [HttpPost]
        [Authorize(Roles = StaticRoles.Closed)]
        [Route("PrintOut2")]
        public async Task<ProcessResultViewModel<List<PrintOutResultViewModel>>> PrintOut2([FromBody]PrintOutFilterViewModel model)
        {
            ProcessResultViewModel<List<PrintOutResultViewModel>> result = new ProcessResultViewModel<List<PrintOutResultViewModel>>();
            List<PrintOutResultViewModel> modelLst = new List<PrintOutResultViewModel>();
            List<ForemenWithOrders> formanWithOrderLst = new List<ForemenWithOrders>();
            List<ForemenWithOrders> OrdersRes = new List<ForemenWithOrders>();
            try
            {
                if (model.teamsId == null)
                {
                    OrdersRes = manager.GetAll(x => (x.DispatcherId == model.DispatcherId) && (x.StatusId == StaticAppSettings.OnHoldStatus.Id)).Data.GroupBy(x => new { x.AreaId, AreaName = x.Area?.Name, x.PrevTeamId }, (key, group) => new ForemenWithOrders { AreaId = key.AreaId, AreaName = key.AreaName, PrevTeamId = key.PrevTeamId ?? 0, Orders = group.Select(q => Mapper.Map<OrderViewModel>(q)) }).ToList();//TODO Refactor issue
                }
                var orderIds = OrdersRes.SelectMany(x => x.Orders.Select(y => y.Id)).ToList();
                var prevIds = OrdersRes.SelectMany(x => x.Orders.Where(e => x != null).Select(y => (int)y.PrevTeamId)).ToList();
                var teamResList = teamService.GetTeamsByIds(prevIds);
                var foreman = teamResList.Data.Select(x => x.ForemanName).ToList();

                var supervisors = OrdersRes.SelectMany(x => x.Orders.Select(y => y.SupervisorName)).ToList();
                var dispatchers = OrdersRes.SelectMany(x => x.Orders.Select(y => y.DispatcherName)).ToList();
                var currentUserIds = await identityService.GetByUserNames(supervisors.Union(dispatchers).Union(foreman).ToList());

                foreach (var item in OrdersRes.ToList())
                {
                    var teamRes = teamResList.Data?.FirstOrDefault(x => x.Id == item.PrevTeamId);
                    foreach (var orderItem in item.Orders)
                    {
                        orderItem.TeamName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.DispatcherName && x.RoleNames.Contains("Dispatcher"))?.FullName;
                        orderItem.SupervisorName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.SupervisorName && x.RoleNames.Contains("Supervisor"))?.FullName;
                        orderItem.DispatcherName = currentUserIds.Data.FirstOrDefault(x => x.UserName == orderItem.DispatcherName && x.RoleNames.Contains("Dispatcher"))?.FullName;

                        if (teamRes != null)
                        {
                            formanWithOrderLst.Add(new ForemenWithOrders()
                            {
                                ForemanId = teamRes.ForemanId,
                                ForemanName = currentUserIds.Data.FirstOrDefault(x => x.UserName == teamRes.ForemanName && x.RoleNames.Contains("Foreman"))?.FullName,
                                PrevTeamId = item.PrevTeamId,
                                AreaId = item.AreaId,
                                AreaName = item.AreaName,
                                Orders = item.Orders
                            });
                        }
                        else
                        {
                            formanWithOrderLst.Add(new ForemenWithOrders()
                            {
                                ForemanId = 0,
                                ForemanName = "",
                                AreaId = item.AreaId,
                                AreaName = item.AreaName,
                                Orders = item.Orders
                            });
                        }
                    }


                }
                modelLst.Add(new PrintOutResultViewModel()
                {
                    DispatcherId = model.DispatcherId,
                    foremenWithOrders = formanWithOrderLst
                });

                return ProcessResultViewModelHelper.Succedded<List<PrintOutResultViewModel>>(modelLst, "PrintOut");
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Succedded<List<PrintOutResultViewModel>>(null, ex.Message);

            }
        }


        [HttpPut]
        [Route("AssignDispatcher")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        public async Task<ProcessResult<bool>> AssignDispatcher([FromBody] AssignDispatcherViewModel model)
        {


            if ((model?.OrderId ?? 0) == 0 && (model?.DispatcherId ?? 0) == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "the order id & dispatcher id are required");
            }

            var dborder = Get((int)model.OrderId).Data;
            if (dborder.TeamId != null)
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already assigned to a team, please refresh the page");

            if (dborder.DispatcherId == model.DispatcherId)
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already assigned to this dispatcher, please refresh the page");



            var order = manager.AssignDispatcher((int)model.OrderId, (int)model.DispatcherId).Data;


            await _orderService.AddAction(order, Request, OrderActionType.AssignDispatcher, null, null, order.TeamId);
            //await AddAction(order.Data,order.Data.TeamId, OrderActionType.Assign, null, null,SubFlag);
            //AssignDispatcherOrderViewModel result = mapper.Map<OrderObject, AssignDispatcherOrderViewModel>(orderObject);            
            //var MappedorderObject = Mapper.Map<AssignDispatcherOrderViewModel>(order);
            var MappedorderObject = Mapper.Map<OrderViewModel>(order);
            var notifyTeamRes = await NotifyDispatcherWithOrderAssign(MappedorderObject);

            return ProcessResultHelper.Succedded<bool>(true);
        }
        [HttpPut]
        [Route("UnAssignDispatcher")]
        [Authorize(Roles = StaticRoles.Supervisor_or_Dispatcher)]
        public async Task<ProcessResult<bool>> UnAssignDispatcher([FromBody] AssignDispatcherViewModel model)
        {
            var order = Get((int)model.OrderId).Data;
            int? dispatcherId = order.DispatcherId;

            if ((model?.OrderId ?? 0) == 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "order id is required");
            }
            if (order.TeamId != null && order.TeamId > 0)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already assigned to a team, please refresh the page");
            }
            if (order.DispatcherId == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "This order has been already Unassigned from this dispatcher, please refresh the page");
            }

            var unAssigned = manager.UnAssignDispatcher((int)model.OrderId);
            //var order = manager.Get(model.OrderId);

            order = Get((int)model.OrderId).Data;
            var orderObject = Mapper.Map<OrderObject>(order);



            await _orderService.AddAction(orderObject, Request, OrderActionType.UnAssignDispatcher, null, null, null);

            var notifyTeamRes = await NotifyDispatcherWithOrderUnassign(order, dispatcherId ?? 0);
            return ProcessResultHelper.Succedded<bool>(true);
        }
        [HttpDelete]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("DeleteAllOrders")]
        public ProcessResult<bool> DeleteAllOrders()
        {
            var deleteOrdersFiles = archivedOrderFileManager.DeleteAll();
            var deleteOrderActions = actionManager.DeleteAll();
            var deleteOrders = manager.DeleteAll();
            if (deleteOrders.Data)
            {
                //var deleteOrderActions = actionManager.DeleteAll();
                if (deleteOrders.Data)
                {
                    //var deleteOrdersFiles = archivedOrderFileManager.DeleteAll();
                    return ProcessResultHelper.Succedded<bool>(true);
                }
            }

            return ProcessResultHelper.Failed<bool>(false, null, "something went wrong while deleting orders");
        }
        [HttpGet]
        [Route("TestNotif")]
        public async Task<ProcessResultViewModel<string>> TestNotif(string userId)
        {
            ProcessResultViewModel<string> notificationRes = null;
            SendNotificationViewModel notificationModel = new SendNotificationViewModel()
            {
                UserId = userId,
                Title = "Test",
                Body = "test",
                Data = new { order = "Test", NotificationType = "Acceptence Order" }
            };
            notificationCenterManageManager.Add(new NotificationCenter()
            {
                RecieverId = userId,
                Title = "Test",
                Body = "test",
                Data = "Test",
                NotificationType = "Acceptence Order",
                IsRead = false
            });
            notificationRes = await notificationService.Send(notificationModel);
            return notificationRes;
        }
        private void AddPinToMapOrders(List<OrderViewModel> orders)
        {
            foreach (var order in orders)
            {
                order.Pin = manager.ChoosePin(order.OnHoldCount, order.TypeName, order.CreatedDate, order.ProblemId);
            }
        }



        private Dictionary<string, string> GetAllTechniciansUserId()
        {
            Dictionary<string, string> teamIdDefaultTechName = _context.Technicians.Include(x => x.User)
                .Where(x => !x.IsDriver && !x.IsDeleted).ToDictionary(x => x.UserId, x => x.User.FullName);
            return teamIdDefaultTechName;
        }

        private string GetFirstTechnicianNameByTeamId(int teamId)
        {
            return _context.Technicians.Include(x => x.User)
                .Where(x => x.IsDriver == false && x.TeamId == teamId && !x.IsDeleted)
               .Select(x => x.User.FullName).FirstOrDefault();

        }

        //[Route("Get/{id}")]
        //[HttpGet]
        //public override ProcessResultViewModel<OrderViewModel> Get([FromRoute]int id)
        //{
        //    var entityResult = Get(id);
        //    //FillCurrentUser(entityResult);
        //    return processResultMapper.Map<OrderViewModel,Order>(entityResult);
        //}


        [HttpPost]
        [Route("GetOrderManagementReportExport")]
        public async Task<ProcessResult<string>> GetOrderManagementReportExport([FromBody]OrderManagementReportViewModel model)
        {

            try
            {
                string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd  HH-mmtt ss-fff");
                string newFile = $"{ _hostingEnv.WebRootPath}\\ExcelSheet\\Order Management {dateTime}.csv";
                string fileResult;

                //var actions = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<OrderObject>>(model.PageInfo);

                //var orderActions = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<OrderAction>>(model.PageInfo);


                var predicate = PredicateBuilder.True<OrderObject>();
                var predicateOrderAction = PredicateBuilder.True<OrderAction>();

                if (model.AreaId.Count() > 0)
                {
                    predicate = predicate.And(r => model.AreaId.Contains(r.AreaId));
                }
                if (model.DispatcherId.Count() > 0)
                {
                    predicate = predicate.And(r => model.DispatcherId.Contains(r.DispatcherId ?? 0));//TODO Refactor issue
                }
                if (model.DivisionId.Count() > 0)
                {
                    predicate = predicate.And(r => model.DivisionId.Contains(r.DivisionId ?? 0));
                }
                if (model.OrderTypeId.Count() > 0)
                {
                    predicate = predicate.And(r => r.TypeId != null && model.OrderTypeId.Contains(r.TypeId.Value));
                }
                if (model.ProblemId.Count() > 0)
                {
                    predicate = predicate.And(r => model.ProblemId.Contains(r.ProblemId ?? 0));//TODO Refactor issue
                }
                if (model.StatusId.Count() > 0)
                {
                    predicate = predicate.And(r => model.StatusId.Contains(r.StatusId));
                }

                //model.TeamId = model.TeamId.Where(x => x != 0).Distinct().ToList();
                //if (model.TeamId.Count() > 0)
                //{
                //    //var teamIds = new List<int>();
                //    //foreach (var item in model.TechnicianId)
                //    //{
                //    //    var res = await teamMembersService.GetTeamIdByTechnicianMember(item);
                //    //    var teamId = res.Data?.FirstOrDefault()?.TeamId;
                //    //    if (teamId != null && teamId != 0)
                //    //    {
                //    //        teamIds.Add(teamId.Value);
                //    //    }
                //    //}
                //    predicate = predicate.And(r => model.TeamId.Contains(r.TeamId));
                //}
                if (!string.IsNullOrEmpty(model.CustomerCode))
                {
                    predicate = predicate.And(r => model.CustomerCode == r.CustomerCode);
                }
                if (model.CompanyCode.Count() > 0)
                {
                    predicate = predicate.And(r => model.CompanyCode.Contains(r.CompanyCodeId));
                }
                if (!string.IsNullOrEmpty(model.OrderId))
                {
                    predicate = predicate.And(r => model.OrderId == r.Code);
                }

                if (model.DateFrom != null && model.DateTo != null)
                {
                    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
                }
                else if (model.DateFrom == null && model.DateTo == null)
                {
                    predicate = predicate.And(x => true);
                }
                else if (model.DateFrom == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
                }
                else if (model.DateTo == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
                }
                var paginatedRes = manager.GetAll(predicate);



                var result = new List<OrderManagementReportItem>();

                var forMansUserIds = new List<string>();

                var forManIdentity = new ProcessResult<List<ForemanViewModel>>();

                var techUsers = paginatedRes.Data.Where(x => x.TeamId != 0).Select(y => y.TeamId).ToList();
                var techService = technicianService.GetTechniciansByTeamIds(techUsers.Select(x => (int)x).ToList());//TODO Refactor issue
                var techs = techService.Data.Where(x => x.IsDriver == false);
                if (techs == null || techs.Count() == 0 || model.Techs.Count() > 0)
                {
                    techs = techService.Data;
                }
                //var techsIdentity = await identityService.GetByUserIds(techs.Select(x => x.UserId).ToList());
                var team = teamService.GetTeamsByIds(techUsers.Select(x => (int)x).ToList());//TODO Refactor issue
                //var dispatchers = await dispatcherService.GetDispatchersByIds(paginatedRes.Data.Data.Select(x => x.DispatcherId).ToList());

                //var supervisors = await supervisorService.GetSupervisorsByIds(paginatedRes.Data.Data.Select(x => x.SupervisorId).ToList());
                var teams = teamService.GetTeamsByIds(paginatedRes.Data.Select(x => (int)x.TeamId).ToList());//TODO Refactor issue

                var foreMans = foremanService.GetForeManByTeamsIds(teams.Data.Select(x => x.ForemanId).ToList());
                var allUsers = await identityService.GetAll();
                //var dispIdentity = await identityService.GetByUserIds(paginatedRes.Data.Select(x => x.DispatcherId??0).ToList());


                //var foreManIdentity = new ProcessResultViewModel<List<ApplicationUserViewModel>>
                //{
                //    Data = new List<ApplicationUserViewModel>()
                //};

                //if (dispatchers.Data != null && dispatchers.Data.Count > 0)
                //{
                //    dispUserIds = dispatchers.Data?.Select(x => x.UserId)?.ToList();
                //    if (dispUserIds.Count > 0)
                //    {
                //        dispIdentity = await identityService.GetByUserIds(dispUserIds);
                //    }
                //}

                //if (foreMans.Data != null && foreMans.Data.Count > 0)
                //{
                //    forMansUserIds = foreMans.Data?.Select(x => x.UserId)?.ToList();
                //    if (forMansUserIds.Count > 0)
                //    {
                //foreManIdentity = await identityService.GetByUserIds(forMansUserIds);
                //    }
                //}


                //IncludeProgressData filteration
                List<TechnicianViewModel> techTeam = new List<TechnicianViewModel>();
                if (model.Techs.Count > 0)
                {
                    if (model.IncludeProgressData)
                    {
                        //var filterTech = technicianService.GetTechniciansByIds(model.Techs);
                        //techTeam = filterTech.Data;
                    }
                    else
                    {
                        //var commomTechs = techService.Data.Select(x => x.Id).Intersect(model.Techs).ToList();
                        //var filterTech = technicianService.GetTechniciansByIds(commomTechs);
                        //techTeam = filterTech.Data;
                        //
                        //List<> TechUserIds = technicians.Data?.Where(x => x.IsDriver == false).Select(x => x.UserId).ToList();
                        // if (TechUserIds.Count > 0)
                        // {
                        //     TechIdentity = await identityService.GetByUserIds(TechUserIds);
                        // }
                        //
                    }
                    //techTeam = filterTechs.Data.Intersect(techs).ToList();
                    //techsIdentity = await identityService.GetByUserIds(techTeam.Select(x => x.UserId).ToList());
                }

                //var userIds = dispUserIds.Concat(techUserIds).ToList();

                //var supIdentity = await identityService.GetByUserIds(dispUserIds);

                result = paginatedRes.Data.Select(x => new OrderManagementReportItem
                {
                    //OrderId = x.Id,
                    TeamId = x.TeamId ?? 0,//TODO Refactor issue
                    OrderCode = x.Code,
                    Area = x.SAP_AreaName,
                    Block = x.SAP_BlockName,
                    Street = x.SAP_StreetName,
                    PACINum = x.SAP_PACI,
                    CompanyCode = x.CompanyCode.Code,
                    //DispatcherNotes = x.DispatcherNote,
                    Dispatcher = x.Dispatcher.User.FirstName + " " + x.Dispatcher.User.LastName, // allUsers?.Data.FirstOrDefault(y => y.Id == x.Dispatcher.UserId)?.FullName,
                    ICAgentNotes = x.ICAgentNote,
                    //Division = x.DivisionName,
                    Status = x.Status?.Name ?? "",
                    OrderDesc = x.OrderDescription,
                    OrderType = x.Type?.Name ?? "",
                    //ProblemType = x.ProblemName,
                    ClosedDate = x.StatusId == 7 || x.StatusId == 8 ? x.UpdatedDate : (DateTime?)null,
                    CreatedDate = x.SAP_CreatedDate,
                    UpdatedDate = x.UpdatedDate == DateTime.MinValue ? x.SAP_CreatedDate : x.UpdatedDate,
                    CustomerID = x?.CustomerCode,
                    CustomerFirstPhone = x?.PhoneOne,
                    CustomerSecondPhone = x?.PhoneTwo,
                    CustomerCallerPhone = x?.Caller_ID,
                    GovName = x?.GovName,
                    FunctionalLocation = x?.FunctionalLocation,
                    SAPBuilding = x?.SAP_HouseKasima,
                    SAPFloor = x?.SAP_Floor,
                    SAPApartmentNo = x?.SAP_AppartmentNo,
                    ContractNo = x?.ContractCode,
                    ContractType = x?.ContractType.Name,
                    ContractCreationDate = x?.ContractStartDate,
                    ContractExpirationDate = x?.ContractExpiryDate,
                    SupervisorName = x.Supervisor?.User?.FullName, //allUsers?.Data.FirstOrDefault(y => y.UserName == x.SupervisorName)?.FullName,
                    Executer = null,
                    Supervisor = x.Supervisor?.User?.FullName,
                    ForeMan = x.Team.Foreman.User.FullName
                }).ToList();

                //var techs = actionManager.GetAll(x => x.OrderId != null && paginatedRes.Data.Data.Select(y => y.Id).Contains(x.OrderId.Value)).Data;
                //var technitions = await technicianService.GetTechniciansByIds(techs.Where(y => y.CreatedUserId != 0 && y.CreatedUserId != null).Select(x => (int)x.CreatedUserId).ToList());
                //var techUserIds = new List<string>();



                //if (technitions.Data != null)
                //{
                //techUserIds = technitions.Data.Select(y => y.UserId).ToList();
                //}
                var teamIds = paginatedRes.Data.Where(x => x.TeamId != 0).Select(x => x.TeamId).ToList();
                var teamsObj = team.Data.Where(x => teamIds.Contains(x.Id));
                var formanIds = foremanService.GetForemanByIds(teamsObj.Select(x => x.ForemanId).ToList());
                var formanUserIds = await identityService.GetByUserIds(formanIds.Data.Select(x => x.UserId).ToList());

                foreach (var item in result)
                {
                    var teamId = paginatedRes.Data.FirstOrDefault(x => x.Code == item.OrderCode && x.TeamId != 0)?.TeamId;
                    //var dispId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode)?.DispatcherId;
                    //var supVisId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode)?.SupervisorId;

                    //var userId = dispatchers.Data.FirstOrDefault(x => x.Id == dispId)?.UserId;
                    //var supervisorsUserId = supervisors.Data.FirstOrDefault(x => x.Id == supVisId)?.UserId;

                    if (techTeam.Count == 0)
                    {
                        item.Technician = allUsers.Data.FirstOrDefault(x => x.Id == techs.FirstOrDefault(y => y.TeamId == teamId)?.UserId)?.FullName;
                        //
                        item.TechnicianName = allUsers.Data.FirstOrDefault(x => x.Id == techs.FirstOrDefault(y => y.TeamId == teamId)?.UserId)?.FullName;
                    }

                    if (teamId == 0 || teamId == null)
                    {
                        item.ForeMan = "";
                    }
                    else
                    {
                        var data = team.Data.FirstOrDefault(x => x.Id == teamId);
                        var formanId = formanIds?.Data?.FirstOrDefault(x => x.Id == data?.ForemanId);
                        forManIdentity.Data = new List<ForemanViewModel>
                        {
                            formanId
                        };
                        item.ForeMan = formanUserIds?.Data.FirstOrDefault(x => x.Id == formanId?.UserId)?.FullName;
                    }
                    //item.Dispatcher = dispIdentity.Data.FirstOrDefault(x => x.Id == userId)?.FullName;
                    //item.Supervisor = subVisIdentity.Data.FirstOrDefault(x => x.Id == supervisorsUserId)?.FullName;
                }

                var temp = new List<OrderManagementReportItem>(result.ToList());

                if (techTeam.Count > 0)
                {
                    result.Clear();
                    var group = techTeam.GroupBy(x => x.TeamId).Select(grp => new { GroupID = grp.Key, TechList = grp.ToList() }).ToList();
                    foreach (var item in group)
                    {
                        var recs = temp.Where(x => x.TeamId == item.GroupID).ToList();
                        foreach (var techItem in item.TechList)
                        {
                            var techName = allUsers.Data.FirstOrDefault(y => y.Id == techItem.UserId)?.FullName;
                            var res = new List<OrderManagementReportItem>();
                            foreach (var itm in recs)
                            {
                                var obj = new OrderManagementReportItem
                                {
                                    Area = itm.Area,
                                    Block = itm.Block,
                                    ClosedDate = itm.ClosedDate,
                                    //OrderId = itm.OrderId,
                                    TeamId = itm.TeamId,
                                    OrderCode = itm.OrderCode,
                                    Street = itm.Street,
                                    PACINum = itm.PACINum,
                                    CompanyCode = itm.CompanyCode,
                                    Dispatcher = itm.Dispatcher,
                                    //DispatcherNotes = itm.DispatcherNotes,
                                    DispatcherName = itm.DispatcherName,
                                    Supervisor = itm.Supervisor,
                                    SupervisorName = itm.SupervisorName,
                                    ForeMan = itm.ForeMan,
                                    ICAgentNotes = itm.ICAgentNotes,
                                    Division = itm.Division,
                                    Status = itm.Status,
                                    OrderDesc = itm.OrderDesc,
                                    OrderType = itm.OrderType,
                                    ProblemType = itm.ProblemType,
                                    CreatedDate = itm.CreatedDate,
                                    UpdatedDate = itm.UpdatedDate,
                                    CustomerID = itm.CustomerID,
                                    CustomerFirstPhone = itm.CustomerFirstPhone,
                                    CustomerSecondPhone = itm.CustomerSecondPhone,
                                    CustomerCallerPhone = itm.CustomerCallerPhone,
                                    GovName = itm.GovName,
                                    FunctionalLocation = itm.FunctionalLocation,
                                    SAPBuilding = itm.SAPBuilding,
                                    SAPFloor = itm.SAPFloor,
                                    SAPApartmentNo = itm.SAPApartmentNo,
                                    ContractNo = itm.ContractNo,
                                    ContractType = itm.ContractType,
                                    ContractCreationDate = itm.ContractCreationDate,
                                    ContractExpirationDate = itm.ContractExpirationDate,
                                    Executer = null,
                                    Technician = techName,
                                    TechnicianName = techName
                                };
                                res.Add(obj);
                            }
                            result.AddRange(res);
                        }
                    }
                }

                //var orderAction = new ProcessResult<PaginatedItems<OrderAction>>();
                var orderAction = new ProcessResult<List<OrderAction>>();
                List<TechnicianViewModel> technsWithoutDrivers = new List<TechnicianViewModel>();
                List<TechnicianViewModel> techOblyDrivers = new List<TechnicianViewModel>();
                if (model.IncludeProgressData && paginatedRes.Data.Count > 0)
                {
                    //var createdUsers = orderAction.Data.Where(x => x.OrderId != null && x.CreatedUser != null).Select(x => x.CreatedUser);
                    //var createdUsersIds = orderAction.Data.Where(x => x.OrderId != null && x.CreatedUserId != null).Select(x => x.CreatedUserId.ToString());
                    var techns = await technicianService.GetAllTechnicians();
                    if (techns != null)
                    {
                        technsWithoutDrivers = techns.Data.Where(x => !x.IsDriver).ToList();
                        techOblyDrivers = techns.Data.Where(x => x.IsDriver).ToList();
                    }
                    var orderIds = paginatedRes.Data.Select(y => y.Id);
                    predicateOrderAction = predicateOrderAction.And(x => orderIds.Contains(x.OrderId.Value));

                    if (techTeam.Count > 0)
                    {
                        predicateOrderAction = predicateOrderAction.And(x => techTeam.Select(y => y.Name).Contains(x.CreatedUser));
                    }
                    //else
                    //{
                    //    predicateOrderAction = predicateOrderAction.And(x => technsWithoutDrivers.Select(y => y.Name).Contains(x.CreatedUser));
                    //}


                    //var ignoreDriversPridicate = PredicateBuilder.True<OrderAction>().And(o =>
                    //    o.CreatedUser == null ||technsWithoutDrivers.Any(d => d.Name == o.CreatedUser)
                    //    );
                    //predicateOrderAction = predicateOrderAction.And(ignoreDriversPridicate);
                    //var ignoreDriversPridicate = PredicateBuilder.True<OrderAction>().And(o => o.CreatedUser == null || !techOblyDrivers.Any(d => d.Name == o.CreatedUser));
                    //predicateOrderAction = predicateOrderAction.And(ignoreDriversPridicate);
                    if (model.AllProgress)
                    {

                        if (techTeam.Count == 0)
                        {
                            predicateOrderAction = predicateOrderAction.And(PredicateBuilder.True<OrderAction>().And(o =>
                                o.CreatedUser == null || technsWithoutDrivers.Any(d => d.Name == o.CreatedUser)));
                        }




                        //  orderAction = actionManager.GetAll(predicateOrderAction);
                        // var paginatedRes = manager.GetAllPaginated(actions, predicate);

                        //ignoreDriversPridicate = PredicateBuilder.True<OrderAction>().And(o =>
                        //    !techs.Any(d => d.Name == o.CreatedUser)
                        //);



                        //TODO add add to check only get contaied list users

                        //ignoreDriversPridicate
                        orderAction = actionManager.GetAll(predicateOrderAction); //orderAction.Data.Select()
                        //orderAction.Data = orderAction.Data.Where(x => technsWithoutDrivers.Any(c => c.PF == x.CreatedUser)).ToList() ;
                    }
                    else if (model.LastProgress)
                    {

                        orderAction = actionManager.GetAll(predicateOrderAction);
                        if (orderAction.Data != null && orderAction.Data.Count > 0)
                        {
                            orderAction.Data = orderAction.Data.GroupBy(x => x.OrderId).Select(g => g.OrderByDescending(c => c.ActionDate).FirstOrDefault()).ToList();
                            //.Where(x => technsWithoutDrivers.Any(c => c.PF == x.CreatedUser))
                        }

                        //orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate).FirstOrDefault());
                    }
                    else if (model.OnHoldOnly)
                    {
                        predicateOrderAction = predicateOrderAction.And(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                        orderAction = actionManager.GetAll(predicateOrderAction);
                    }
                    else if (model.OnHoldLastProgress)
                    {
                        var orderActionTemp = actionManager.GetAll(predicateOrderAction);
                        if (orderActionTemp.Data != null && orderActionTemp.Data.Count > 0)
                        {
                            var lastProg = orderActionTemp.Data.GroupBy(x => x.OrderId).Select(g => g.OrderByDescending(c => c.ActionDate).FirstOrDefault()).ToList();
                            var lastProgGrouped = lastProg.GroupBy(x => x.ActionDate).Select(x => x.First()).ToList();
                            var onHold = orderActionTemp.Data.Where(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                            var onHoldGrouped = onHold.GroupBy(x => x.ActionDate).Select(x => x.First()).ToList();
                            orderAction.Data = lastProgGrouped.Union(onHoldGrouped).GroupBy(x => new { x.ActionDate, x.OrderId, x.StatusId }).Select(g => g.First()).OrderBy(x => x.Id).ToList();
                            //foreach (var item in onHold)
                            //{
                            //    if (!lastProg.Select(x => x.OrderId).Contains(item.OrderId))
                            //    {
                            //        orderAction.Data.Add(item);
                            //    }
                            //}

                            // var lastAction = orderAction.Data.Last();
                            // orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate));
                            // orderAction = actionManager.GetAll(predicateOrderAction);
                            //if (lastAction.StatusId != StaticAppSettings.OnHoldStatus.Id)
                            //{
                            //    orderAction.Data.Add(lastAction);
                            //}
                        }
                    }

                    if (orderAction.Data != null && orderAction.Data.Count > 0)
                    {
                        //Add dispatchers to the list that i am getting from

                        if (techTeam.Count > 0 || model.OnHoldLastProgress)
                        {
                            orderAction.Data = orderAction.Data.ToList();
                        }
                        else
                        {
                            orderAction.Data = orderAction.Data.ToList().GroupBy(x => x.ActionDate).Select(x => x.First()).ToList();
                        }

                        var supervisorsForAction = orderAction.Data.Select(x => x.Supervisor.User.FullName).ToList();//TODO Refactor remove this
                        var createdUserIdForAction = orderAction.Data.Select(x => (x?.CreatedUserId ?? 0).ToString()).ToList();
                        var dispatchersForAction = orderAction.Data.Select(x => x.Dispatcher.User.FullName).ToList();//TODO Refactor remove this
                        var techForAction = techns.Data.Select(x => x.Name).ToList();
                        var currentUserIds = await identityService.GetByUserNames(supervisorsForAction.Union(dispatchersForAction).Union(techForAction).Union(createdUserIdForAction).ToList());


                        OrderManagementReportItem _oa = null;
                        result = new List<OrderManagementReportItem>();

                        string excuterName;
                        string sctionUserName;
                        string techName;
                        string techNameHistory;

                        TechnicianViewModel techVmTemp = null;
                        ApplicationUserViewModel techIdentity = null;
                        foreach (OrderObject order in paginatedRes.Data)
                        {
                            //load current Technician
                            techVmTemp = techns.Data.FirstOrDefault(x => order.TeamId != 0 && x.TeamId == order.TeamId && !x.IsDriver);
                            techIdentity = currentUserIds.Data.FirstOrDefault(tech => tech.UserName == (techVmTemp?.PF ?? ""));
                            techName = techIdentity?.FullName ?? "";
                            excuterName = "";
                            techNameHistory = "";


                            _oa = new OrderManagementReportItem
                            {
                                OrderId = order?.Id ?? 0,
                                OrderCode = order?.Code,
                                Area = order?.SAP_AreaName,
                                Block = order?.SAP_BlockName,
                                Street = order?.SAP_StreetName,
                                PACINum = order?.SAP_PACI,
                                CompanyCode = order?.CompanyCode.Code,
                                DispatcherNotes = order?.DispatcherNote,
                                CustomerID = order?.CustomerCode,
                                CustomerFirstPhone = order?.PhoneOne,
                                CustomerSecondPhone = order?.PhoneTwo,
                                CustomerCallerPhone = order?.Caller_ID,
                                GovName = order?.GovName,
                                FunctionalLocation = order?.FunctionalLocation,
                                SAPBuilding = order?.SAP_HouseKasima,
                                SAPFloor = order?.SAP_Floor,
                                SAPApartmentNo = order?.SAP_AppartmentNo,
                                ContractNo = order?.ContractCode,
                                ContractType = order?.ContractType.Name,
                                ContractCreationDate = order?.ContractStartDate,
                                ContractExpirationDate = order?.ContractExpiryDate,
                                Dispatcher = order.Dispatcher.User.FullName,
                                Supervisor = order.Supervisor?.User?.FullName,
                                //DispatcherName = dispIdentity?.Data.FirstOrDefault(y => y.UserName == action.DispatcherName)?.FullName,
                                //SupervisorName = subVisIdentity?.Data.FirstOrDefault(y => y.UserName == action.SupervisorName)?.FullName,
                                ForeMan = order.Team.Foreman.Name,
                                ICAgentNotes = order?.ICAgentNote,
                                Division = order?.Division.Name,
                                //Status = order?.StatusName,
                                //ActionTypeName = action?.ActionTypeName,
                                OrderDesc = order?.OrderDescription,
                                OrderType = order?.Type?.Name,
                                ProblemType = order?.Problem.Name,
                                //ProgressDate = action?.ActionDate,
                                //Executer = excuterName,
                                //Executer = currentUserIds.Data.FirstOrDefault(x => x.UserName == action?.CreatedUserId.ToString())?.FullName,
                                //ProgressStatus = action?.StatusName,
                                //ProgressSubStatus = action?.SubStatusName,
                                //ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime : action?.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays.Value)),
                                // ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime?.Value :new TimeSpan(1,1,1),
                                //TechnicianNotes = action?.ServeyReport,
                                ClosedDate = order.StatusId == 7 || order.StatusId == 8 ? order.UpdatedDate : (DateTime?)null,
                                Technician = techName,
                                //Technician = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
                                //TechnicianName = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
                                //Technician = currentUserIds.Data.FirstOrDefault(x =>
                                //        x.UserName == action?.CreatedUser && x.RoleNames.Contains("Technician"))
                                //    ?.FullName,
                                //TechnicianName = techNameHistory,
                                CreatedDate = order?.SAP_CreatedDate ?? default(DateTime),
                                UpdatedDate = order?.UpdatedDate == DateTime.MinValue ? order.SAP_CreatedDate : order.UpdatedDate
                            };



                            List<OrderAction> orderActions = orderAction.Data
                                .Where(x => x.OrderId != null && x.OrderId == order.Id).ToList();

                            if (model.AllProgress && orderActions.Count == 0 && model.Techs.Count == 0)
                                result.Add(_oa);


                            OrderAction action = null;
                            for (int i = 0; i < orderActions.Count; i++)
                            {
                                OrderManagementReportItem _oaCloned = _oa.Clone();
                                action = orderActions[i];

                                sctionUserName = (action?.CreatedUserId ?? 0).ToString();

                                if (!string.IsNullOrEmpty(sctionUserName))
                                    excuterName = currentUserIds.Data.FirstOrDefault(x => x.UserName == sctionUserName)
                                        ?.FullName;
                                else
                                    excuterName = "";

                                //load Historty Technician
                                techVmTemp = techns.Data.FirstOrDefault(x => x.PF == action.CreatedUser);
                                if (techVmTemp != null)
                                {
                                    techIdentity = currentUserIds.Data.FirstOrDefault(tech =>
                                        tech.UserName == (techVmTemp?.PF ?? ""));
                                    techNameHistory = techIdentity?.FullName ?? "";
                                }
                                else
                                    techNameHistory = "";

                                //set progress data 
                                _oaCloned.ActionTypeName = action?.ActionTypeName;
                                _oaCloned.ProgressDate = action?.ActionDate;
                                _oaCloned.Executer = excuterName;
                                _oaCloned.ProgressStatus = action?.Status.Name;
                                _oaCloned.ProgressSubStatus = action?.SubStatus?.Name;
                                _oaCloned.ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime : action?.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays.Value));
                                _oaCloned.TechnicianNotes = action?.ServeyReport;
                                _oaCloned.TechnicianName = techNameHistory;
                                _oaCloned.DispatcherName = action.Dispatcher.User.FullName; //    allUsers?.Data.FirstOrDefault(y => y.UserName == action.DispatcherName)?.FullName;
                                _oaCloned.SupervisorName = action.Supervisor.User.FullName; //  allUsers?.Data.FirstOrDefault(y => y.UserName == action.SupervisorName)?.FullName;
                                result.Add(_oaCloned);
                            }



                        }

                    }
                }

                //Write to excell file
                if (model.IncludeProgressData)
                {
                    var listData = result
                        .Select(x => new OrderManagementReportExportOutputViewModelWithProgreassColomns(x, null)).ToList();

                    fileResult = ListToExcelHelper.WriteObjectsToExcel(listData, newFile);
                }
                else
                {
                    var listData = result
                        .Select(x => new OrderManagementReportExportOutputViewModel(x)).ToList();
                    fileResult = ListToExcelHelper.WriteObjectsToExcel(listData, newFile);
                }

                //fileResult = ListToExcelHelper.WriteObjectsToExcel(mapper.Map<List<OrderManagementProgressReportExportOutputViewModel>>(orderAction.Data), newFile);
                if (!string.IsNullOrEmpty(fileResult))
                {
                    var url = new Uri(fileResult);
                    var absolute = url.AbsoluteUri.Substring(37);
                    //FileInfo file = new FileInfo(newFile);
                    //file.MoveTo(Path.ChangeExtension(newFile, ".xlsx"));
                    //result = result.Replace(".csv", ".xlsx");
                    return ProcessResultHelper.Succedded(absolute);
                }
                else
                {
                    return ProcessResultHelper.Failed(string.Empty, null, "Something went wrong while exporting order report");
                }

            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(string.Empty, ex);
            }
        }


        //@
        [HttpPost]
        [Route("GetOrderManagementReportPaginated")]
        public async Task<ProcessResult<OrderManagementReportPaginatedViewModel>> GetOrderManagementReportPaginated([FromBody]OrderManagementReportViewModel model)
        {
            try
            {
                var actions = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<OrderObject>>(model.PageInfo);

                var predicate = PredicateBuilder.True<OrderObject>();
                var predicateOrderAction = PredicateBuilder.True<OrderAction>();

                if (model.AreaId.Count() > 0)
                {
                    predicate = predicate.And(r => model.AreaId.Contains(r.AreaId));
                }
                if (model.DispatcherId.Count() > 0)
                {
                    predicate = predicate.And(r => model.DispatcherId.Contains(r.DispatcherId ?? 0));//TODO Refactor issue
                }
                if (model.DivisionId.Count() > 0)
                {
                    predicate = predicate.And(r => model.DivisionId.Contains(r.DivisionId ?? 0));//TODO Refactor issue
                }
                if (model.OrderTypeId.Count() > 0)
                {
                    predicate = predicate.And(r => r.TypeId != null && model.OrderTypeId.Contains(r.TypeId.Value));
                }
                if (model.ProblemId.Count() > 0)
                {
                    predicate = predicate.And(r => model.ProblemId.Contains(r.ProblemId ?? 0));//TODO Refactor issue
                }
                if (model.StatusId.Count() > 0)
                {
                    predicate = predicate.And(r => model.StatusId.Contains(r.StatusId));
                }

                //model.TeamId = model.TeamId.Where(x => x != 0).Distinct().ToList();
                //if (model.TeamId.Count() > 0)
                //{
                //    //var teamIds = new List<int>();
                //    //foreach (var item in model.TechnicianId)
                //    //{
                //    //    var res = await teamMembersService.GetTeamIdByTechnicianMember(item);
                //    //    var teamId = res.Data?.FirstOrDefault()?.TeamId;
                //    //    if (teamId != null && teamId != 0)
                //    //    {
                //    //        teamIds.Add(teamId.Value);
                //    //    }
                //    //}
                //    predicate = predicate.And(r => model.TeamId.Contains(r.TeamId));
                //}
                if (!string.IsNullOrEmpty(model.CustomerCode))
                {
                    predicate = predicate.And(r => model.CustomerCode == r.CustomerCode);
                }
                if (model.CompanyCode.Count() > 0)
                {
                    predicate = predicate.And(r => model.CompanyCode.Contains(r.CompanyCodeId));
                }
                if (!string.IsNullOrEmpty(model.OrderId))
                {
                    predicate = predicate.And(r => model.OrderId == r.Code);
                }

                if (model.DateFrom != null && model.DateTo != null)
                {
                    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
                }
                else if (model.DateFrom == null && model.DateTo == null)
                {
                    predicate = predicate.And(x => true);
                }
                else if (model.DateFrom == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
                }
                else if (model.DateTo == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
                }
                ProcessResult<PaginatedItems<OrderObject>> paginatedRes = new ProcessResult<PaginatedItems<OrderObject>>();

                //paginatedRes = manager.GetAllPaginated(actions, predicate);


                if (model.IncludeProgressData)
                {
                    var all = manager.GetAll(predicate);
                    paginatedRes.Data = new PaginatedItems<OrderObject>() { Data = all.Data, Count = all.Data.Count };
                }
                else
                {
                    paginatedRes = manager.GetAllPaginated(actions, predicate);
                }

                var result = new List<OrderManagementReportItem>();
                var output = new OrderManagementReportPaginatedViewModel
                {
                    Count = 0,
                    Result = result
                };

                if (paginatedRes.Data.Data == null || paginatedRes.Data.Count == 0)
                {
                    return ProcessResultHelper.Succedded(output, "OrderManagementReport");
                }

                var forMansUserIds = new List<string>();

                var forManIdentity = new ProcessResult<List<ForemanViewModel>>();

                var techUsers = paginatedRes.Data.Data.Where(x => x.TeamId != 0).Select(y => y.TeamId).ToList();
                var techService = technicianService.GetTechniciansByTeamIds(techUsers.Select(x => (int)x).ToList());
                var techs = techService.Data;
                if (techs == null || techs.Count() == 0 || model.Techs.Count() > 0)
                {
                    techs = techService.Data;
                }
                var allUsers = await identityService.GetByUserIds(techs.Select(x => x.UserId).ToList());
                var team = teamService.GetTeamsByIds(techUsers.Select(x => (int)x).ToList());
                //var dispatchers = await dispatcherService.GetDispatchersByIds(paginatedRes.Data.Data.Select(x => x.DispatcherId).ToList());

                //var supervisors = await supervisorService.GetSupervisorsByIds(paginatedRes.Data.Data.Select(x => x.SupervisorId).ToList());
                var teams = teamService.GetTeamsByIds(paginatedRes.Data.Data.Select(x => (int)x.TeamId).ToList());

                var foreMans = foremanService.GetForeManByTeamsIds(teams.Data.Select(x => x.ForemanId).ToList());

                //var dispIdentity = await identityService.GetByUserNames(paginatedRes.Data.Data.Select(x => x.DispatcherName).ToList());

                //var subVisIdentity = await identityService.GetByUserNames(paginatedRes.Data.Data.Select(x => x.SupervisorName).ToList());

                //var foreManIdentity = new ProcessResultViewModel<List<ApplicationUserViewModel>>
                //{
                //    Data = new List<ApplicationUserViewModel>()
                //};

                //if (dispatchers.Data != null && dispatchers.Data.Count > 0)
                //{
                //    dispUserIds = dispatchers.Data?.Select(x => x.UserId)?.ToList();
                //    if (dispUserIds.Count > 0)
                //    {
                //        dispIdentity = await identityService.GetByUserIds(dispUserIds);
                //    }
                //}

                //if (foreMans.Data != null && foreMans.Data.Count > 0)
                //{
                //    forMansUserIds = foreMans.Data?.Select(x => x.UserId)?.ToList();
                //    if (forMansUserIds.Count > 0)
                //    {
                //        foreManIdentity = await identityService.GetByUserIds(forMansUserIds);
                //    }
                //}

                //if (supervisors.Data != null)
                //{
                //    supVisUserIds = supervisors.Data?.Select(x => x.UserId)?.ToList();
                //    if (supVisUserIds.Count > 0)
                //    {
                //        subVisIdentity = await identityService.GetByUserIds(supVisUserIds);
                //    }
                //}

                List<TechnicianViewModel> techTeam = new List<TechnicianViewModel>();
                if ((model?.Techs?.Count ?? 0) > 0 && model.IncludeProgressData)
                {
                    //var filterTech = technicianService.GetTechniciansByIds(model.Techs);
                    //techTeam = filterTech.Data;
                    //allUsers = await identityService.GetByUserIds(techTeam.Select(x => x.UserId).ToList());
                }

                //var userIds = dispUserIds.Concat(techUserIds).ToList();

                //var supIdentity = await identityService.GetByUserIds(dispUserIds);

                result = paginatedRes.Data.Data.Select(x => new OrderManagementReportItem
                {
                    OrderId = x.Id,
                    TeamId = x.TeamId ?? 0,
                    OrderCode = x.Code,
                    Area = x.SAP_AreaName,
                    Block = x.SAP_BlockName,
                    Street = x.SAP_StreetName,
                    PACINum = x.SAP_PACI,
                    CompanyCode = x.CompanyCode.Code,
                    DispatcherNotes = x.DispatcherNote,
                    Dispatcher = x.Dispatcher.User.FirstName + " " + x.Dispatcher.User.FirstName,
                    ICAgentNotes = x.ICAgentNote,
                    Division = x.Division.Name,
                    //Status = x.StatusName,
                    OrderDesc = x.OrderDescription,
                    OrderType = x.Type.Name,
                    ProblemType = x.Problem.Name,
                    ClosedDate = x.StatusId == 7 || x.StatusId == 8 ? x.UpdatedDate : (DateTime?)null,
                    CreatedDate = x.SAP_CreatedDate,
                    UpdatedDate = x.UpdatedDate == DateTime.MinValue ? x.SAP_CreatedDate : x.UpdatedDate,
                    CustomerID = x?.CustomerCode,
                    CustomerFirstPhone = x?.PhoneOne,
                    CustomerSecondPhone = x?.PhoneTwo,
                    CustomerCallerPhone = x?.Caller_ID,
                    GovName = x?.GovName,
                    FunctionalLocation = x?.FunctionalLocation,
                    SAPBuilding = x?.SAP_HouseKasima,
                    SAPFloor = x?.SAP_Floor,
                    SAPApartmentNo = x?.SAP_AppartmentNo,
                    ContractNo = x?.ContractCode,
                    ContractType = x?.ContractType.Name,
                    ContractCreationDate = x?.ContractStartDate,
                    ContractExpirationDate = x?.ContractExpiryDate,
                    SupervisorName = x.Dispatcher.Supervisor.User.FirstName + " " + x.Dispatcher.Supervisor.User.LastName, //subVisIdentity?.Data.FirstOrDefault(y => y.UserName == x.SupervisorName)?.FullName,
                    Executer = null,

                    Supervisor = x.Dispatcher.Supervisor.User.FirstName + " " + x.Dispatcher.Supervisor.User.LastName,
                    ForeMan = x.Team.Foreman.User.FirstName + " " + x.Team.Foreman.User.LastName,
                }).ToList();

                //var techs = actionManager.GetAll(x => x.OrderId != null && paginatedRes.Data.Data.Select(y => y.Id).Contains(x.OrderId.Value)).Data;
                //var technitions = await technicianService.GetTechniciansByIds(techs.Where(y => y.CreatedUserId != 0 && y.CreatedUserId != null).Select(x => (int)x.CreatedUserId).ToList());
                //var techUserIds = new List<string>();



                //if (technitions.Data != null)
                //{
                //techUserIds = technitions.Data.Select(y => y.UserId).ToList();
                //}
                var teamIds = paginatedRes.Data.Data.Where(x => x.TeamId != 0).Select(x => x.TeamId).ToList();
                var teamsObj = team.Data.Where(x => teamIds.Contains(x.Id));
                //var formanIds = foremanService.GetForemanByIds(teamsObj.Select(x => x.ForemanId).ToList());
                //var formanUserIds = await identityService.GetByUserIds(formanIds.Data.Select(x => x.UserId).ToList());

                foreach (var item in result)
                {
                    var teamId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode && x.TeamId != 0)?.TeamId;
                    //var dispId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode)?.DispatcherId;
                    //var supVisId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode)?.SupervisorId;

                    //var userId = dispatchers.Data.FirstOrDefault(x => x.Id == dispId)?.UserId;
                    //var supervisorsUserId = supervisors.Data.FirstOrDefault(x => x.Id == supVisId)?.UserId;

                    int arderActionsHistoryCount = actionManager.GetAll(x => (x.OrderId ?? 0) == item.OrderId && !x.IsDeleted).Data.Count;
                    if (techTeam.Count == 0 && arderActionsHistoryCount != 0)
                    {
                        item.Technician = allUsers.Data.FirstOrDefault(x => x.Id == techs.FirstOrDefault(y => y.TeamId == teamId)?.UserId)?.FullName;
                        //
                        item.TechnicianName = allUsers.Data.FirstOrDefault(x => x.Id == techs.FirstOrDefault(y => y.TeamId == teamId)?.UserId)?.FullName;
                    }

                    //if (teamId == 0 || teamId == null)
                    //{
                    //    item.ForeMan = "";
                    //}
                    //else
                    //{
                    //    var data = team.Data.FirstOrDefault(x => x.Id == teamId);
                    //    var formanId = formanIds?.Data?.FirstOrDefault(x => x.Id == data?.ForemanId);
                    //    forManIdentity.Data = new List<ForemanViewModel>
                    //    {
                    //        formanId
                    //    };
                    //    item.ForeMan = formanUserIds?.Data.FirstOrDefault(x => x.Id == formanId?.UserId)?.FullName;
                    //}
                    //item.Dispatcher = dispIdentity.Data.FirstOrDefault(x => x.Id == userId)?.FullName;
                    //item.Supervisor = subVisIdentity.Data.FirstOrDefault(x => x.Id == supervisorsUserId)?.FullName;
                }

                var temp = new List<OrderManagementReportItem>(result.ToList());

                if (techTeam.Count > 0)
                {
                    result.Clear();
                    var group = techTeam.GroupBy(x => x.TeamId).Select(grp => new { GroupID = grp.Key, TechList = grp.ToList() }).ToList();
                    foreach (var item in group)
                    {
                        var recs = temp.Where(x => x.TeamId == item.GroupID).ToList();
                        foreach (var techItem in item.TechList)
                        {
                            var techName = allUsers.Data.FirstOrDefault(y => y.Id == techItem.UserId)?.FullName;
                            var res = new List<OrderManagementReportItem>();
                            foreach (var itm in recs)
                            {
                                var obj = new OrderManagementReportItem
                                {
                                    Area = itm.Area,
                                    Block = itm.Block,
                                    ClosedDate = itm.ClosedDate,
                                    OrderId = itm.OrderId,
                                    TeamId = itm.TeamId,
                                    OrderCode = itm.OrderCode,
                                    Street = itm.Street,
                                    PACINum = itm.PACINum,
                                    CompanyCode = itm.CompanyCode,
                                    Dispatcher = itm.Dispatcher,
                                    DispatcherNotes = itm.DispatcherNotes,
                                    DispatcherName = itm.DispatcherName,
                                    Supervisor = itm.Supervisor,
                                    SupervisorName = itm.SupervisorName,
                                    ForeMan = itm.ForeMan,
                                    ICAgentNotes = itm.ICAgentNotes,
                                    Division = itm.Division,
                                    Status = itm.Status,
                                    OrderDesc = itm.OrderDesc,
                                    OrderType = itm.OrderType,
                                    ProblemType = itm.ProblemType,
                                    CreatedDate = itm.CreatedDate,
                                    UpdatedDate = itm.UpdatedDate,
                                    CustomerID = itm.CustomerID,
                                    CustomerFirstPhone = itm.CustomerFirstPhone,
                                    CustomerSecondPhone = itm.CustomerSecondPhone,
                                    CustomerCallerPhone = itm.CustomerCallerPhone,
                                    GovName = itm.GovName,
                                    FunctionalLocation = itm.FunctionalLocation,
                                    SAPBuilding = itm.SAPBuilding,
                                    SAPFloor = itm.SAPFloor,
                                    SAPApartmentNo = itm.SAPApartmentNo,
                                    ContractNo = itm.ContractNo,
                                    ContractType = itm.ContractType,
                                    ContractCreationDate = itm.ContractCreationDate,
                                    ContractExpirationDate = itm.ContractExpirationDate,
                                    Executer = null,
                                    Technician = techName,
                                    TechnicianName = techName
                                };
                                res.Add(obj);
                            }
                            result.AddRange(res);
                        }
                    }
                }

                //var orderAction = new ProcessResult<PaginatedItems<OrderAction>>();
                var orderAction = new ProcessResult<List<OrderAction>>();
                List<TechnicianViewModel> technsWithoutDrivers = new List<TechnicianViewModel>();
                List<TechnicianViewModel> techOnlyDrivers = new List<TechnicianViewModel>();
                if (model.IncludeProgressData && paginatedRes.Data.Count > 0)
                {
                    //var createdUsers = orderAction.Data.Where(x => x.OrderId != null && x.CreatedUser != null).Select(x => x.CreatedUser);
                    //var createdUsersIds = orderAction.Data.Where(x => x.OrderId != null && x.CreatedUserId != null).Select(x => x.CreatedUserId.ToString());
                    var techns = await technicianService.GetAllTechnicians();
                    if (techns != null)
                    {
                        technsWithoutDrivers = techns.Data.Where(x => !x.IsDriver).ToList();
                        techOnlyDrivers = techns.Data.Where(x => x.IsDriver).ToList();
                    }
                    var orderIds = paginatedRes.Data.Data.Select(y => y.Id);
                    predicateOrderAction = predicateOrderAction.And(x => orderIds.Contains(x.OrderId.Value));


                    //else
                    //{
                    //    predicateOrderAction = predicateOrderAction.And(x => technsWithoutDrivers.Select(y => y.Name).Contains(x.CreatedUser));
                    //}



                    if (model.AllProgress)
                    {
                        if (techTeam.Count > 0)
                        {
                            predicateOrderAction = predicateOrderAction.And(x => techTeam.Any(y => y.PF.ToLower() == x.CreatedUser.ToLower()));
                        }
                        else
                        {
                            predicateOrderAction = predicateOrderAction.And(o => o.CreatedUser == null || !techOnlyDrivers.Any(d => d.PF == o.CreatedUser));
                        }




                        //  orderAction = actionManager.GetAll(predicateOrderAction);
                        // var paginatedRes = manager.GetAllPaginated(actions, predicate);

                        //ignoreDriversPridicate = PredicateBuilder.True<OrderAction>().And(o =>
                        //    !techs.Any(d => d.Name == o.CreatedUser)
                        //);



                        //TODO add add to check only get contaied list users

                        //ignoreDriversPridicate
                        orderAction = actionManager.GetAll(predicateOrderAction); //orderAction.Data.Select()
                        //orderAction.Data = orderAction.Data.Where(x => technsWithoutDrivers.Any(c => c.PF == x.CreatedUser)).ToList() ;
                    }
                    else if (model.LastProgress)
                    {

                        orderAction = actionManager.GetAll(predicateOrderAction);
                        if (orderAction.Data != null && orderAction.Data.Count > 0)
                        {
                            orderAction.Data = orderAction.Data.GroupBy(x => x.OrderId).Select(g => g.OrderByDescending(c => c.ActionDate).FirstOrDefault()).ToList();
                            //.Where(x => technsWithoutDrivers.Any(c => c.PF == x.CreatedUser))
                        }

                        //orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate).FirstOrDefault());
                    }


                    else if (model.OnHoldOnly)
                    {
                        predicateOrderAction = predicateOrderAction.And(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                        orderAction = actionManager.GetAll(predicateOrderAction);
                    }
                    else if (model.OnHoldLastProgress)
                    {
                        var orderActionTemp = actionManager.GetAll(predicateOrderAction);
                        if (orderActionTemp.Data != null && orderActionTemp.Data.Count > 0)
                        {
                            var lastProg = orderActionTemp.Data.GroupBy(x => x.OrderId).Select(g => g.OrderByDescending(c => c.ActionDate).FirstOrDefault()).ToList();
                            var lastProgGrouped = lastProg.GroupBy(x => x.ActionDate).Select(x => x.First()).ToList();
                            var onHold = orderActionTemp.Data.Where(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                            var onHoldGrouped = onHold.GroupBy(x => x.ActionDate).Select(x => x.First()).ToList();
                            orderAction.Data = lastProgGrouped.Union(onHoldGrouped).GroupBy(x => new { x.ActionDate, x.OrderId, x.StatusId }).Select(g => g.First()).OrderBy(x => x.Id).ToList();
                            //foreach (var item in onHold)
                            //{
                            //    if (!lastProg.Select(x => x.OrderId).Contains(item.OrderId))
                            //    {
                            //        orderAction.Data.Add(item);
                            //    }
                            //}

                            // var lastAction = orderAction.Data.Last();
                            // orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate));
                            // orderAction = actionManager.GetAll(predicateOrderAction);
                            //if (lastAction.StatusId != StaticAppSettings.OnHoldStatus.Id)
                            //{
                            //    orderAction.Data.Add(lastAction);
                            //}
                        }
                    }








                    if (orderAction.Data != null && orderAction.Data.Count > 0)
                    {
                        //Add dispatchers to the list that i am getting from
                        //dispIdentity.Data.AddRange((await identityService.GetByUserNames(orderAction.Data.Select(oa => oa.DispatcherName).ToList())).Data); // await identityService.GetUserByUsernames(paginatedRes.Data.Data.Select(x => x.DispatcherName).ToList());

                        if (techTeam.Count > 0 || model.OnHoldLastProgress)
                        {
                            orderAction.Data = orderAction.Data.ToList();
                        }
                        else
                        {
                            orderAction.Data = orderAction.Data.ToList().GroupBy(x => x.ActionDate).Select(x => x.First()).ToList();
                        }

                        //var supervisorsForAction = orderAction.Data.Select(x => x.SupervisorName).ToList();
                        //var createdUserIdForAction = orderAction.Data.Select(x => (x?.CreatedUserId ?? 0).ToString()).ToList();
                        //var dispatchersForAction = orderAction.Data.Select(x => x.DispatcherName).ToList();
                        //var techForAction = techns.Data.Select(x => x.Name).ToList();
                        //var currentUserIds = await identityService.GetUserByUsernames(supervisorsForAction.Union(dispatchersForAction).Union(techForAction).Union(createdUserIdForAction).ToList());
                        var currentUserIds = await identityService.GetAll();

                        OrderManagementReportItem _oa = null;
                        result = new List<OrderManagementReportItem>();

                        string excuterName;
                        string sctionUserName;
                        string techName;
                        string techNameHistory;

                        TechnicianViewModel techVmTemp = null;
                        ApplicationUserViewModel techIdentity = null;
                        foreach (OrderObject order in paginatedRes.Data.Data)
                        {
                            //load current Technician
                            techVmTemp = techns.Data.FirstOrDefault(x => order.TeamId != 0 && x.TeamId == order.TeamId && !x.IsDriver);
                            techIdentity = currentUserIds.Data.FirstOrDefault(tech => tech.UserName == (techVmTemp?.PF ?? ""));
                            techName = techIdentity?.FullName ?? "";
                            excuterName = "";
                            techNameHistory = "";


                            _oa = new OrderManagementReportItem
                            {
                                OrderId = order?.Id ?? 0,
                                OrderCode = order?.Code,
                                Area = order?.SAP_AreaName,
                                Block = order?.SAP_BlockName,
                                Street = order?.SAP_StreetName,
                                PACINum = order?.SAP_PACI,
                                CompanyCode = order?.CompanyCode.Code,
                                DispatcherNotes = order?.DispatcherNote,
                                CustomerID = order?.CustomerCode,
                                CustomerFirstPhone = order?.PhoneOne,
                                CustomerSecondPhone = order?.PhoneTwo,
                                CustomerCallerPhone = order?.Caller_ID,
                                GovName = order?.GovName,
                                FunctionalLocation = order?.FunctionalLocation,
                                SAPBuilding = order?.SAP_HouseKasima,
                                SAPFloor = order?.SAP_Floor,
                                SAPApartmentNo = order?.SAP_AppartmentNo,
                                ContractNo = order?.ContractCode,
                                ContractType = order?.ContractType.Name,
                                ContractCreationDate = order?.ContractStartDate,
                                ContractExpirationDate = order?.ContractExpiryDate,
                                Dispatcher = order.Dispatcher.User.FullName,
                                Supervisor = order.Supervisor?.User?.FullName,
                                //DispatcherName = dispIdentity?.Data.FirstOrDefault(y => y.UserName == action.DispatcherName)?.FullName,
                                //SupervisorName = subVisIdentity?.Data.FirstOrDefault(y => y.UserName == action.SupervisorName)?.FullName,
                                ForeMan = order.Team.Foreman.User.FullName,
                                ICAgentNotes = order?.ICAgentNote,
                                Division = order?.Division.Name,
                                //Status = order?.StatusName,
                                //ActionTypeName = action?.ActionTypeName,
                                OrderDesc = order?.OrderDescription,
                                OrderType = order?.Type?.Name,
                                ProblemType = order?.Problem.Name,
                                //ProgressDate = action?.ActionDate,
                                //Executer = excuterName,
                                //Executer = currentUserIds.Data.FirstOrDefault(x => x.UserName == action?.CreatedUserId.ToString())?.FullName,
                                //ProgressStatus = action?.StatusName,
                                //ProgressSubStatus = action?.SubStatusName,
                                //ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime : action?.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays.Value)),
                                // ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime?.Value :new TimeSpan(1,1,1),
                                //TechnicianNotes = action?.ServeyReport,
                                ClosedDate = order.StatusId == 7 || order.StatusId == 8 ? order.UpdatedDate : (DateTime?)null,
                                Technician = techName,
                                //Technician = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
                                //TechnicianName = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
                                //Technician = currentUserIds.Data.FirstOrDefault(x =>
                                //        x.UserName == action?.CreatedUser && x.RoleNames.Contains("Technician"))
                                //    ?.FullName,
                                //TechnicianName = techNameHistory,
                                CreatedDate = order?.SAP_CreatedDate ?? default(DateTime),
                                UpdatedDate = order?.UpdatedDate == DateTime.MinValue ? order.SAP_CreatedDate : order.UpdatedDate
                            };


                            List<OrderAction> orderActions = orderAction.Data
                                .Where(x => x.OrderId != null && x.OrderId == order.Id).ToList();



                            if (model.AllProgress && orderActions.Count == 0 && model.Techs.Count == 0)
                                result.Add(_oa);



                            OrderAction action = null;
                            for (int i = 0; i < orderActions.Count; i++)
                            {

                                OrderManagementReportItem _oaCloned = _oa.Clone();
                                action = orderActions[i];

                                sctionUserName = (action?.CreatedUserId ?? 0).ToString();

                                if (!string.IsNullOrEmpty(sctionUserName))
                                    excuterName = currentUserIds.Data.FirstOrDefault(x => x.UserName == sctionUserName)
                                        ?.FullName;
                                else
                                    excuterName = "";

                                //load Historty Technician
                                techVmTemp = techns.Data.FirstOrDefault(x => x.PF == action.CreatedUser);
                                if (techVmTemp != null)
                                {
                                    techIdentity = currentUserIds.Data.FirstOrDefault(tech =>
                                        tech.UserName == (techVmTemp?.PF ?? ""));
                                    techNameHistory = techIdentity?.FullName ?? "";
                                }
                                else
                                    techNameHistory = "";

                                //set progress data 
                                _oaCloned.ActionTypeName = action?.ActionTypeName;
                                _oaCloned.ProgressDate = action?.ActionDate;
                                _oaCloned.Executer = excuterName;
                                _oaCloned.ProgressStatus = action?.Status.Name;
                                _oaCloned.ProgressSubStatus = action?.SubStatus?.Name;
                                _oaCloned.ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime : action?.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays.Value));
                                _oaCloned.TechnicianNotes = action?.ServeyReport;
                                _oaCloned.TechnicianName = techNameHistory;
                                _oaCloned.DispatcherName = action.Dispatcher.User.FullName;// allUsers?.Data.FirstOrDefault(y => y.UserName == action.DispatcherName)?.FullName;
                                _oaCloned.SupervisorName = action.Supervisor.User.FullName;// allUsers?.Data.FirstOrDefault(y => y.UserName == action.SupervisorName)?.FullName;
                                result.Add(_oaCloned);
                            }


                        }

                    }
                }

                if (result == null || result.Count == 0)
                    result = new List<OrderManagementReportItem>();


                else if (model.OnHoldOnly && (orderAction?.Data == null || orderAction?.Data?.Count == 0))
                {
                    output = new OrderManagementReportPaginatedViewModel
                    {
                        Count = 0,
                        Result = new List<OrderManagementReportItem>()
                    };
                }
                // To Resolve the problem of The excel report is not matching the management report view (columns wise)
                else if ((orderAction?.Data != null || orderAction?.Data?.Count != 0) && model.IncludeProgressData)
                {

                    output = new OrderManagementReportPaginatedViewModel
                    {
                        Count = result.Count,
                        Result = result.Skip(model.PageInfo.PageSize * (model.PageInfo.PageNo - 1)).Take(model.PageInfo.PageSize).ToList()
                    };
                    //paginatedRes = new PaginatedItems<OrderManagementReportOutputViewModel>(model.PageInfo.PageNo, model.PageInfo.PageSize, result.Count, result)

                }
                // To Resolve the problem of viewing only the last Order Progress in case of filtering with Order Id And Choosing IncludeProgressData 
                else if (!string.IsNullOrEmpty(model.OrderId) && (orderAction?.Data != null || orderAction?.Data?.Count != 0) && model.IncludeProgressData)
                {
                    output = new OrderManagementReportPaginatedViewModel
                    {
                        Count = result.Count,
                        Result = result.Skip(model.PageInfo.PageSize * (model.PageInfo.PageNo - 1)).Take(model.PageInfo.PageSize).ToList()
                    };
                }

                else
                {
                    output = new OrderManagementReportPaginatedViewModel
                    {
                        Count = paginatedRes.Data.Count,
                        Result = result
                    };
                }
                return ProcessResultHelper.Succedded(output, "OrderManagementReport");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<OrderManagementReportPaginatedViewModel>(null, ex);
            }
        }


        //
        [HttpGet]
        [Route("GetOrderByCode")]
        public async Task<ProcessResultViewModel<OrderViewModel>> GetOrderByCode([FromQuery] string code)
        {
            var dd = "";
            var defaultDateTimeValue = default(DateTime);
            Nullable<DateTime> dt = null;
            if (string.IsNullOrEmpty(code))
            {
                return ProcessResultViewModelHelper.Failed<OrderViewModel>(null, "invalid order code");
            }

            var orderRes = manager.GetAll(x => x.Code == code, x => x.Dispatcher, x => x.Dispatcher.User, x => x.Team).Data.FirstOrDefault();

            if (orderRes == null)
            {
                return ProcessResultViewModelHelper.Failed<OrderViewModel>(null, "invalid order code");
            }

            var orderItem = mapper.Map<OrderObject, OrderViewModel>(orderRes);


            if (orderRes.TeamId != null)
            {
                TeamMemberViewModel memberVm = teamMembersService.GetDeafualtTeamMemberIdByTeamId((int)orderRes.TeamId).Data;
                //var teamIdDefaultTechName = GetDefaultTechniciansForOrders();
                //teamIdDefaultTechName.TryGetValue(orderItem.TeamId, out dummy);
                orderItem.TechName = memberVm?.FullName;
            }


            //team && tech 


            if (orderItem.StatusName == "Compelete" || orderItem.StatusName == "Cancelled")
            {
                orderItem.UpdatedDate = orderItem.UpdatedDate;
            }
            else
                orderItem.UpdatedDate = defaultDateTimeValue;
            return ProcessResultViewModelHelper.Succedded(orderItem);
        }

        private async Task<ProcessResultViewModel<bool>> updateOrderLogic(ProcessResult<OrderObject> OrderObj, OrderViewModel model)
        {
            ProcessResultViewModel<bool> resultViewModel = new ProcessResultViewModel<bool>();
            OrderObj.Data.TeamId = model.TeamId;

            int? NewTeam = model.TeamId;
            // make order unassigned incase it changed to open 
            if (StaticAppSettings.UnassignedStatusId.Contains(model.StatusId))
            {
                OrderObj.Data.PrevTeamId =
                    OrderObj.Data.TeamId != null ? OrderObj.Data.TeamId : OrderObj.Data.PrevTeamId;
                OrderObj.Data.TeamId = null;
                OrderObj.Data.RankInTeam = 0;
                model.TeamId = null;

                //order team history
                OrderHistoryTeam ordHistory = new OrderHistoryTeam();
                ordHistory.OrderId = OrderObj.Data.Id;
                ordHistory.Code = OrderObj.Data.Code;
                ordHistory.PrevTeamId = OrderObj.Data.PrevTeamId;
                ordHistory.TeamId = OrderObj.Data.TeamId;
                _IOrderHistoryTeamManager.Add(ordHistory);
            }
            if (OrderObj.IsSucceeded && OrderObj.Data != null)
            {
                // model.AcceptanceFlag = AcceptenceType.Accepted;
                if (OrderObj.Data.StatusId != model.StatusId || OrderObj.Data.SubStatusId != model.SubStatusId)
                {
                    model.AcceptanceFlag = AcceptenceType.Accepted;
                    resultViewModel = base.Put(model);

                    OrderObj.Data.StatusId = model.StatusId;
                    OrderObj.Data.SubStatusId = model.SubStatusId;
                    OrderObj.Data.ServeyReport = model.ServeyReport;
                    // var entityResult = manager.SetAcceptence(OrderObj.Data.Id, true, null, null, null, null, null, null);
                    //}
                    await _orderService.AddAction(OrderObj.Data, Request, OrderActionType.UpdateOrder, null, null, NewTeam);
                    var NotificationRes = await NotifyDispatcherWithUpdateStatus(model);
                    // await AddAction(OrderObj.Data, OrderActionType.ChangeStatus, null, null);
                }
                else
                {
                    resultViewModel = base.Put(model);
                }
                //  await AddAction(OrderObj.Data, OrderActionType.UpdateOrder, null, null);
            }
            return resultViewModel;
        }



        [HttpPut]
        [Authorize(Roles = StaticRoles.Dispatcher)]
        [Route("UpdateAsync")]
        public async Task<ProcessResultViewModel<bool>> PutAsync([FromBody] OrderViewModel model)
        {
            OrderStatusEnum currentStatusEnum = (OrderStatusEnum)model.StatusId;

            switch (currentStatusEnum)
            {
                case OrderStatusEnum.Open:
                    model.SubStatusId = (int)OrderSubstatusEnum.Released;
                    model.SubStatusName = OrderSubstatusEnum.Released.ToString();
                    break;
                case OrderStatusEnum.Dispatched:// will follow the team logic
                    if (model.PrevTeamId != null && model.PrevTeamId != 0)
                    {
                        model.SubStatusId = (int)OrderSubstatusEnum.Re_Scheduled;
                        model.SubStatusName = OrderSubstatusEnum.Re_Scheduled.ToString();
                    }
                    else
                    {
                        model.SubStatusId = (int)OrderSubstatusEnum.Scheduled;
                        model.SubStatusName = OrderSubstatusEnum.Scheduled.ToString();
                    }
                    break;
                case OrderStatusEnum.On_Travel:
                    model.SubStatusId = (int)OrderSubstatusEnum.On_Travel;
                    model.SubStatusName = OrderSubstatusEnum.On_Travel.ToString();
                    break;
                case OrderStatusEnum.Reached:
                    model.SubStatusId = (int)OrderSubstatusEnum.Reached;
                    model.SubStatusName = OrderSubstatusEnum.Reached.ToString();
                    break;
                case OrderStatusEnum.Started:
                    model.SubStatusId = (int)OrderSubstatusEnum.Started;
                    model.SubStatusName = OrderSubstatusEnum.Started.ToString();
                    break;
                case OrderStatusEnum.On_Hold://will take the value from front
                    break;
                case OrderStatusEnum.Compelete://will take the value from front
                    break;
                case OrderStatusEnum.Cancelled://will take the value from front
                    break;
            }



            ProcessResultViewModel<bool> resultViewModel = new ProcessResultViewModel<bool>();
            var OrderObj = manager.Get(x => x.Id == model.Id);
            OrderObj.Data.AcceptanceFlag = model.AcceptanceFlag;
            OrderObj.Data.TypeId = model.TypeId;

            var dispRes = dispatcherService.GetDispatcherById(OrderObj.Data.DispatcherId ?? 0);//TODO Refactor issue
            //OrderObj.Data.DispatcherName = dispRes.Data.Name;
            //model.DispatcherName = OrderObj.Data.DispatcherName;
            model.DispatcherId = OrderObj.Data.DispatcherId ?? 0;//TODO Refactor issue
            int? ObjTeam = OrderObj.Data.TeamId;//TODO Refactor issue

            if ((StaticAppSettings.AssignedStatusId.Contains(model.StatusId)) && ObjTeam == 0)
            {
                var errmsg = "Order has no team to update to" + OrderObj.Data.Status?.Name ?? "";
                resultViewModel = ProcessResultViewModelHelper.Failed(false, null, ProcessResultStatusCode.Failed, errmsg, (int)OrderUpdateResponseStatus.NoTeam);
            }
            else if (StaticAppSettings.ArchivedStatusId.Contains(OrderObj.Data.StatusId))
            {
                // order is closed for updates
                resultViewModel = ProcessResultViewModelHelper.Failed(false, null, ProcessResultStatusCode.Failed, "Can't edit closed Order", (int)OrderUpdateResponseStatus.Completed);
            }
            else
            {
                resultViewModel = await updateOrderLogic(OrderObj, model);
            }
            return resultViewModel;
        }

        //for validations for update supervisor
        [HttpGet]
        [Route("GetOrdersForSupervisor")]
        public ProcessResult<List<OrderViewModel>> GetOrdersForSupervisor([FromQuery] int supervisrId)
        {
            if (supervisrId > 0)
            {
                var orders = manager.GetAll(x => x.SupervisorId == supervisrId);

                if (orders.IsSucceeded && orders.Data.Count > 0)
                {
                    List<OrderViewModel> result = mapper.Map<List<OrderObject>, List<OrderViewModel>>(orders.Data);
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(result);
                }
                else
                {
                    return ProcessResultHelper.Succedded<List<OrderViewModel>>(new List<OrderViewModel>());
                }
            }
            return ProcessResultHelper.Failed<List<OrderViewModel>>(null, null, "supervisor id is required");
        }


        #region old apis
        ////New
        //[HttpPost]
        //[Route("GetOrderManagementReportPaginatedOld")]
        //private async Task<ProcessResult<OrderManagementReportPaginatedViewModel>> GetOrderManagementReportPaginatedOld([FromBody]OrderManagementReportViewModel model)
        //{
        //    try
        //    {
        //        //new
        //        TechnicianViewModel technicianViewModel = new TechnicianViewModel();
        //        //var dispatchers = await technicianService.GetTechnicianById(paginatedRes.Data.Data.Select(x => x.TeamId).ToList());
        //        //  var teamMembers = await teamMembersService.GetMemberUsersByTeamId(model.TeamId.FirstOrDefault());

        //        var actions = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<OrderObject>>(model.PageInfo);

        //        var orderActions = mapper.Map<PaginatedItemsViewModel<OrderViewModel>, PaginatedItems<OrderAction>>(model.PageInfo);


        //        var predicate = PredicateBuilder.True<OrderObject>();
        //        var predicateOrderAction = PredicateBuilder.True<OrderAction>();

        //        if (model.AreaId.Count() > 0)
        //        {
        //            predicate = predicate.And(r => model.AreaId.Contains(r.AreaId));
        //        }
        //        if (model.DispatcherId.Count() > 0)
        //        {
        //            predicate = predicate.And(r => model.DispatcherId.Contains(r.DispatcherId ?? 0));//TODO Refactor issue
        //        }
        //        if (model.DivisionId.Count() > 0)
        //        {
        //            predicate = predicate.And(r => model.DivisionId.Contains(r.DivisionId ?? 0));//TODO Refactor issue
        //        }
        //        if (model.OrderTypeId.Count() > 0)
        //        {
        //            predicate = predicate.And(r => model.OrderTypeId.Contains(r.TypeId));
        //        }
        //        if (model.ProblemId.Count() > 0)
        //        {
        //            predicate = predicate.And(r => model.ProblemId.Contains(r.ProblemId ?? 0));//TODO Refactor issue
        //        }
        //        if (model.StatusId.Count() > 0)
        //        {
        //            predicate = predicate.And(r => model.StatusId.Contains(r.StatusId));
        //        }

        //        //model.TeamId = model.TeamId.Where(x => x != 0).Distinct().ToList();
        //        //if (model.TeamId.Count() > 0)
        //        //{
        //        //    //var teamIds = new List<int>();
        //        //    //foreach (var item in model.TechnicianId)
        //        //    //{
        //        //    //    var res = await teamMembersService.GetTeamIdByTechnicianMember(item);
        //        //    //    var teamId = res.Data?.FirstOrDefault()?.TeamId;
        //        //    //    if (teamId != null && teamId != 0)
        //        //    //    {
        //        //    //        teamIds.Add(teamId.Value);
        //        //    //    }
        //        //    //}
        //        //    predicate = predicate.And(r => model.TeamId.Contains(r.TeamId));
        //        //}
        //        if (!string.IsNullOrEmpty(model.CustomerCode))
        //        {
        //            predicate = predicate.And(r => model.CustomerCode == r.CustomerCode);
        //        }
        //        if (model.CompanyCode.Count() > 0)
        //        {
        //            predicate = predicate.And(r => model.CompanyCode.Contains(r.CompanyCodeId));
        //        }
        //        if (!string.IsNullOrEmpty(model.OrderId))
        //        {
        //            predicate = predicate.And(r => model.OrderId == r.Code);
        //        }

        //        if (model.DateFrom != null && model.DateTo != null)
        //        {
        //            predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
        //        }
        //        else if (model.DateFrom == null && model.DateTo == null)
        //        {
        //            predicate = predicate.And(x => true);
        //        }
        //        else if (model.DateFrom == null)
        //        {
        //            predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
        //        }
        //        else if (model.DateTo == null)
        //        {
        //            predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
        //        }
        //        ProcessResult<PaginatedItems<OrderObject>> paginatedRes = new ProcessResult<PaginatedItems<OrderObject>>();

        //        if (model.IncludeProgressData)
        //        {
        //            var all = manager.GetAll(predicate);
        //            paginatedRes.Data = new PaginatedItems<OrderObject>() { Data = all.Data, Count = all.Data.Count };
        //        }
        //        else
        //        {
        //            paginatedRes = manager.GetAllPaginated(actions, predicate);
        //        }

        //        var result = new List<OrderManagementReportOutputViewModel>();
        //        var output = new OrderManagementReportPaginatedViewModel
        //        {
        //            Count = 0,
        //            Result = result
        //        };

        //        if (paginatedRes.Data.Data == null || paginatedRes.Data.Count == 0)
        //        {
        //            return ProcessResultHelper.Succedded(output, "OrderManagementReport");
        //        }

        //        var dispUserIds = new List<string>();
        //        var supVisUserIds = new List<string>();
        //        //new
        //        var TechUserIds = new List<string>();
        //        var NewTechUserIds = new List<string>();
        //        var superVisorUserIds = new List<string>();
        //        var forMansUserIds = new List<string>();

        //        var forManIdentity = new ProcessResult<List<ForemanViewModel>>();

        //        List<int> techUsers = paginatedRes.Data.Data.Where(x => x.TeamId != 0 && x.TeamId != null).Select(y => (int)y.TeamId).ToList();
        //        var team = teamService.GetTeamsByIds(techUsers);
        //        var dispatchers = dispatcherService.GetDispatchersByIds(paginatedRes.Data.Data.Select(x => (int)x.DispatcherId).ToList());

        //        var supervisors = supervisorService.GetSupervisorsByIds(paginatedRes.Data.Data.Select(x => x.SupervisorId).ToList());
        //        var teams = teamService.GetTeamsByIds(paginatedRes.Data.Data.Select(x => (int)x.TeamId).ToList());
        //        //new
        //        var technicians = technicianService.GetTechniciansByIds(paginatedRes.Data.Data.Select(x => (int)x.TeamId).ToList());

        //        var foreMans = foremanService.GetForeManByTeamsIds(teams.Data.Select(x => x.ForemanId).ToList());

        //        var dispIdentity = new ProcessResultViewModel<List<ApplicationUserViewModel>>
        //        {
        //            Data = new List<ApplicationUserViewModel>()
        //        };

        //        var subVisIdentity = new ProcessResultViewModel<List<ApplicationUserViewModel>>
        //        {
        //            Data = new List<ApplicationUserViewModel>()
        //        };

        //        var foreManIdentity = new ProcessResultViewModel<List<ApplicationUserViewModel>>
        //        {
        //            Data = new List<ApplicationUserViewModel>()
        //        };
        //        //new
        //        var TechIdentity = new ProcessResultViewModel<List<ApplicationUserViewModel>>
        //        {
        //            Data = new List<ApplicationUserViewModel>()
        //        };

        //        if (dispatchers.Data != null && dispatchers.Data.Count > 0)
        //        {
        //            dispUserIds = dispatchers.Data?.Select(x => x.UserId)?.ToList();
        //            if (dispUserIds.Count > 0)
        //            {
        //                dispIdentity = await identityService.GetByUserIds(dispUserIds);
        //            }
        //        }
        //        if (foreMans.Data != null && foreMans.Data.Count > 0)
        //        {
        //            forMansUserIds = foreMans.Data?.Select(x => x.UserId)?.ToList();
        //            if (forMansUserIds.Count > 0)
        //            {
        //                foreManIdentity = await identityService.GetByUserIds(forMansUserIds);
        //            }
        //        }

        //        if (supervisors.Data != null)
        //        {
        //            supVisUserIds = supervisors.Data?.Select(x => x.UserId)?.ToList();
        //            if (supVisUserIds.Count > 0)
        //            {
        //                subVisIdentity = await identityService.GetByUserIds(supVisUserIds);
        //            }
        //        }
        //        //New                
        //        TechUserIds = technicians.Data?.Where(x => x.IsDriver == false).Select(x => x.UserId).ToList();
        //        if (TechUserIds.Count > 0)
        //        {
        //            TechIdentity = await identityService.GetByUserIds(TechUserIds);
        //        }

        //        //var userIds = dispUserIds.Concat(techUserIds).ToList();

        //        //var supIdentity = await identityService.GetByUserIds(dispUserIds);

        //        result = paginatedRes.Data.Data.Select(x => new OrderManagementReportOutputViewModel
        //        {
        //            OrderId = x.Id,
        //            OrderCode = x.Code,
        //            Area = x.SAP_AreaName,
        //            Block = x.SAP_BlockName,
        //            Street = x.SAP_StreetName,
        //            PACINum = x.SAP_PACI,
        //            CompanyCode = x.CompanyCodeName,
        //            DispatcherNotes = x.DispatcherNote,
        //            Dispatcher = x.Dispatcher.User.FullName,
        //            ICAgentNotes = x.ICAgentNote,
        //            Division = x.Division.Name,
        //            Status = x.StatusName,
        //            OrderDesc = x.OrderDescription,
        //            OrderType = x.TypeName,
        //            ProblemType = x.Problem.Name,
        //            ClosedDate = x.StatusId == 7 || x.StatusId == 8 ? x.UpdatedDate : (DateTime?)null,
        //            CreatedDate = x.SAP_CreatedDate,
        //            UpdatedDate = x.UpdatedDate == DateTime.MinValue ? x.SAP_CreatedDate : x.UpdatedDate,
        //            CustomerID = x?.CustomerCode,
        //            CustomerFirstPhone = x?.PhoneOne,
        //            CustomerSecondPhone = x?.PhoneTwo,
        //            CustomerCallerPhone = x?.Caller_ID,
        //            GovName = x?.GovName,
        //            FunctionalLocation = x?.FunctionalLocation,
        //            SAPBuilding = x?.SAP_HouseKasima,
        //            SAPFloor = x?.SAP_Floor,
        //            SAPApartmentNo = x?.SAP_AppartmentNo,
        //            ContractNo = x?.ContractCode,
        //            ContractType = x?.ContractType.Name,
        //            ContractCreationDate = x?.ContractStartDate,
        //            ContractExpirationDate = x?.ContractExpiryDate,
        //            SupervisorName = x.SupervisorName, //subVisIdentity?.Data.FirstOrDefault(sup => sup.Id == supervisors.Data.FirstOrDefault(y => y.Id == x.SupervisorId)?.UserId)?.FullName,
        //            Executer = null,
        //            Supervisor = x.SupervisorName,
        //            ForeMan = x.Team.Foreman.User.FullName,
        //        }).ToList();

        //        //var techs = actionManager.GetAll(x => x.OrderId != null && paginatedRes.Data.Data.Select(y => y.Id).Contains(x.OrderId.Value)).Data;
        //        //var technitions = await technicianService.GetTechniciansByIds(techs.Where(y => y.CreatedUserId != 0 && y.CreatedUserId != null).Select(x => (int)x.CreatedUserId).ToList());
        //        //var techUserIds = new List<string>();



        //        //if (technitions.Data != null)
        //        //{
        //        //techUserIds = technitions.Data.Select(y => y.UserId).ToList();
        //        //}
        //        var teamIds = paginatedRes.Data.Data.Where(x => x.TeamId != 0).Select(x => x.TeamId).ToList();
        //        var teamsObj = team.Data.Where(x => teamIds.Contains(x.Id));
        //        var formanIds = foremanService.GetForemanByIds(teamsObj.Select(x => x.ForemanId).ToList());
        //        var formanUserIds = await identityService.GetByUserIds(formanIds.Data.Select(x => x.UserId).ToList());

        //        foreach (var item in result)
        //        {
        //            var teamId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode && x.TeamId != 0)?.TeamId;
        //            var dispId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode)?.DispatcherId;
        //            //var supVisId = paginatedRes.Data.Data.FirstOrDefault(x => x.Code == item.OrderCode)?.SupervisorId;
        //            //new                  

        //            var userId = dispatchers.Data.FirstOrDefault(x => x.Id == dispId)?.UserId;

        //            var techId = technicians.Data.FirstOrDefault(x => x.TeamId == teamId)?.UserId;
        //            //if(teamId !=null && techId!=null)
        //            //{
        //            //    item.Technician = TechIdentity.Data.FirstOrDefault(x => x.Id == techId).FullName;
        //            //    item.TechnicianName = TechIdentity.Data.FirstOrDefault(x => x.Id == techId).FullName;
        //            //}                   
        //            //item.Technician = team.Data.FirstOrDefault(x => x.Id == teamId)?.Name;
        //            //new 

        //            //for (int i = 0; i < technicians.Data.Count; i++)
        //            //{
        //            //var TechWithIsDrive = technicians.Data.FirstOrDefault(x => x.IsDriver == true).Id;
        //            //var TechWithIsNotDrive = technicians.Data.FirstOrDefault(x => x.IsDriver == false).Id;
        //            //item.Technician = technicians.Data.FirstOrDefault(x => x.IsDriver==false);

        //            // }
        //            //if (teamMembers.Data.Count>0)
        //            //{
        //            for (int i = 0; i < techUsers.Count; i++)
        //            {

        //                item.Technician = TechIdentity.Data.FirstOrDefault(x => x.Id == techUsers[i].ToString()).FullName;
        //            }
        //            //}                    
        //            if (teamId == 0 || teamId == null)
        //            {
        //                item.ForeMan = "";
        //            }
        //            else
        //            {
        //                var data = team.Data.FirstOrDefault(x => x.Id == teamId);
        //                var formanId = formanIds?.Data?.FirstOrDefault(x => x.Id == data?.ForemanId);
        //                forManIdentity.Data = new List<ForemanViewModel>
        //                {
        //                    formanId
        //                };
        //                item.ForeMan = formanUserIds?.Data.FirstOrDefault(x => x.Id == formanId?.UserId)?.FullName;
        //            }
        //            item.Dispatcher = dispIdentity.Data.FirstOrDefault(x => x.Id == userId)?.FullName;
        //            item.Supervisor = subVisIdentity.Data.FirstOrDefault(x => x.Id == userId)?.FullName;
        //            //new
        //            //item.Technician = "shaimaa";
        //            //item.TechnicianName = "shaimaa";

        //        }
        //        //var orderAction = new ProcessResult<PaginatedItems<OrderAction>>();
        //        var orderAction = new ProcessResult<List<OrderAction>>();

        //        if (model.IncludeProgressData && paginatedRes.Data.Count > 0)
        //        {
        //            var orderIds = paginatedRes.Data.Data.Select(y => y.Id);
        //            predicateOrderAction = predicateOrderAction.And(x => orderIds.Contains(x.OrderId.Value));
        //            if (model.AllProgress)
        //            {
        //                //  orderAction = actionManager.GetAll(predicateOrderAction);
        //                // var paginatedRes = manager.GetAllPaginated(actions, predicate);

        //                orderAction = actionManager.GetAll(predicateOrderAction);
        //            }
        //            else if (model.LastProgress)
        //            {
        //                orderAction = actionManager.GetAll(predicateOrderAction);
        //                if (orderAction.Data != null && orderAction.Data.Count > 0)
        //                {
        //                    orderAction.Data = orderAction.Data.GroupBy(x => x.OrderId).Select(g => g.OrderByDescending(c => c.Id).FirstOrDefault()).ToList();
        //                }

        //                //orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate).FirstOrDefault());
        //            }
        //            else if (model.OnHoldOnly)
        //            {
        //                predicateOrderAction = predicateOrderAction.And(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
        //                orderAction = actionManager.GetAll(predicateOrderAction);
        //            }
        //            else if (model.OnHoldLastProgress)
        //            {
        //                var orderActionTemp = actionManager.GetAll(predicateOrderAction);
        //                if (orderActionTemp.Data != null && orderActionTemp.Data.Count > 0)
        //                {
        //                    var lastProg = orderActionTemp.Data.GroupBy(x => x.OrderId).Select(g => g.OrderByDescending(c => c.Id).FirstOrDefault()).ToList();
        //                    orderAction.Data = lastProg;
        //                    orderAction.Data.AddRange(orderActionTemp.Data.Where(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id));
        //                    // var lastAction = orderAction.Data.Last();
        //                    // orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate));
        //                    // orderAction = actionManager.GetAll(predicateOrderAction);
        //                    //if (lastAction.StatusId != StaticAppSettings.OnHoldStatus.Id)
        //                    //{
        //                    //    orderAction.Data.Add(lastAction);
        //                    //}
        //                }
        //            }

        //            if (orderAction.Data != null && orderAction.Data.Count > 0)
        //            {
        //                orderAction.Data = orderAction.Data.ToList().GroupBy(x => x.ActionTime).Select(x => x.First()).ToList();
        //                var currentUserIds = await identityService.GetByUserNames(orderAction.Data.Where(x => x.OrderId != null && x.CreatedUser != null).Select(x => x.CreatedUser).ToList());

        //                //if (model.OnHoldOnly || model.OnHoldLastProgress)
        //                //{
        //                result = (from order in paginatedRes.Data.Data
        //                          join action in orderAction.Data.Where(x => x.OrderId != null) on order.Id equals action?.OrderId into act
        //                          from action in act.DefaultIfEmpty()
        //                          select new OrderManagementReportOutputViewModel
        //                          {
        //                              OrderId = order?.Id ?? 0,
        //                              OrderCode = order?.Code,
        //                              Area = order?.SAP_AreaName,
        //                              Block = order?.SAP_BlockName,
        //                              Street = order?.SAP_StreetName,
        //                              PACINum = order?.SAP_PACI,
        //                              CompanyCode = order?.CompanyCodeName,
        //                              DispatcherNotes = order?.DispatcherNote,
        //                              CustomerID = order?.CustomerCode,
        //                              CustomerFirstPhone = order?.PhoneOne,
        //                              CustomerSecondPhone = order?.PhoneTwo,
        //                              CustomerCallerPhone = order?.Caller_ID,
        //                              GovName = order?.GovName,
        //                              FunctionalLocation = order?.FunctionalLocation,
        //                              SAPBuilding = order?.SAP_HouseKasima,
        //                              SAPFloor = order?.SAP_Floor,
        //                              SAPApartmentNo = order?.SAP_AppartmentNo,
        //                              ContractNo = order?.ContractCode,
        //                              //ContractType = order?.ContractTypeName,
        //                              ContractCreationDate = order?.ContractStartDate,
        //                              ContractExpirationDate = order?.ContractExpiryDate,
        //                              Dispatcher = order.Dispatcher.User.FullName, //dispIdentity?.Data.FirstOrDefault(x => x.Id == dispatchers.Data.FirstOrDefault(y => y.Id == order.DispatcherId)?.UserId)?.FullName,
        //                              Supervisor = order.SupervisorName,// subVisIdentity?.Data.FirstOrDefault(x => x.Id == supervisors.Data.FirstOrDefault(y => y.Id == order.SupervisorId)?.UserId)?.FullName,

        //                              DispatcherName = order.Dispatcher.User.FullName,// dispIdentity?.Data.FirstOrDefault(x => x.Id == dispatchers.Data.FirstOrDefault(y => y.Id == order.DispatcherId)?.UserId)?.FullName,

        //                              SupervisorName = order.SupervisorName,  /// subVisIdentity?.Data.FirstOrDefault(sup => sup.Id == supervisors.Data.FirstOrDefault(y => y.Id == order.SupervisorId)?.UserId)?.FullName,
        //                              ForeMan = order.Team.Foreman.User.FullName, //foreManIdentity?.Data.FirstOrDefault(x => x.Id == foreMans.Data.FirstOrDefault(or => or.Id == teams.Data.FirstOrDefault(y => y.Id == order.TeamId)?.ForemanId)?.UserId)?.FullName,
        //                              ICAgentNotes = order?.ICAgentNote,
        //                              //Division = order?.DivisionName,
        //                              Status = order?.StatusName,
        //                              OrderDesc = order?.OrderDescription,
        //                              OrderType = order?.TypeName,
        //                              //ProblemType = order?.ProblemName,
        //                              ProgressDate = action?.ActionDate,
        //                              Executer = currentUserIds.Data.FirstOrDefault(x => x.UserName == action?.CreatedUser)?.FullName,
        //                              ProgressStatus = action?.StatusName,
        //                              ProgressSubStatus = action?.SubStatusName,
        //                              ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime : action?.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays.Value)),
        //                              // ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime?.Value :new TimeSpan(1,1,1),
        //                              TechnicianNotes = action?.ServeyReport,
        //                              ClosedDate = order.StatusId == 7 || order.StatusId == 8 ? order.UpdatedDate : (DateTime?)null,
        //                              //Technician = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
        //                              //TechnicianName = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
        //                              Technician = currentUserIds.Data.FirstOrDefault(x => x.UserName == action?.CreatedUser && x.RoleNames.Contains("Technician"))?.FullName,
        //                              TechnicianName = currentUserIds.Data.FirstOrDefault(x => x.UserName == action?.CreatedUser && x.RoleNames.Contains("Technician"))?.FullName,
        //                              CreatedDate = order?.SAP_CreatedDate ?? default(DateTime),
        //                              UpdatedDate = order?.UpdatedDate == DateTime.MinValue ? order.SAP_CreatedDate : order.UpdatedDate
        //                          })?.ToList();
        //                //}
        //                //else
        //                //{
        //                //    result = (from order in paginatedRes.Data.Data
        //                //              join action in orderAction.Data.Where(x => x.OrderId != null) on order.Id equals action?.OrderId into act
        //                //              from action in act.DefaultIfEmpty()
        //                //              select new OrderManagementReportOutputViewModel
        //                //              {
        //                //                  OrderId = order?.Id ?? 0,
        //                //                  OrderCode = order?.Code,
        //                //                  Area = order?.SAP_AreaName,
        //                //                  Block = order?.SAP_BlockName,
        //                //                  Street = order?.SAP_StreetName,
        //                //                  PACINum = order?.SAP_PACI,
        //                //                  CompanyCode = order?.CompanyCodeName,
        //                //                  DispatcherNotes = order?.DispatcherNote,
        //                //                  CustomerID = order?.CustomerCode,
        //                //                  CustomerFirstPhone = order?.PhoneOne,
        //                //                  CustomerSecondPhone = order?.PhoneTwo,
        //                //                  CustomerCallerPhone = order?.Caller_ID,
        //                //                  GovName = order?.GovName,
        //                //                  FunctionalLocation = order?.FunctionalLocation,
        //                //                  SAPBuilding = order?.SAP_HouseKasima,
        //                //                  SAPFloor = order?.SAP_Floor,
        //                //                  SAPApartmentNo = order?.SAP_AppartmentNo,
        //                //                  ContractNo = order?.ContractCode,
        //                //                  ContractType = order?.ContractTypeName,
        //                //                  ContractCreationDate = order?.ContractStartDate,
        //                //                  ContractExpirationDate = order?.ContractExpiryDate,
        //                //                  Dispatcher = dispIdentity?.Data.FirstOrDefault(x => x.Id == dispatchers.Data.FirstOrDefault(y => y.Id == order.DispatcherId)?.UserId)?.FullName,
        //                //                  Supervisor = subVisIdentity?.Data.FirstOrDefault(x => x.Id == supervisors.Data.FirstOrDefault(y => y.Id == order.SupervisorId)?.UserId)?.FullName,

        //                //                  DispatcherName = dispIdentity?.Data.FirstOrDefault(x => x.Id == dispatchers.Data.FirstOrDefault(y => y.Id == order.DispatcherId)?.UserId)?.FullName,

        //                //                  SupervisorName = dispIdentity?.Data.FirstOrDefault(x => x.Id == supervisors.Data.FirstOrDefault(y => y.Id == order.DispatcherId)?.UserId)?.FullName,
        //                //                  ForeMan = foreManIdentity?.Data.FirstOrDefault(x => x.Id == foreMans.Data.FirstOrDefault(or => or.Id == teams.Data.FirstOrDefault(y => y.Id == order.TeamId)?.ForemanId)?.UserId)?.FullName,
        //                //                  ICAgentNotes = order?.ICAgentNote,
        //                //                  Division = order?.DivisionName,
        //                //                  Status = order?.StatusName,
        //                //                  OrderDesc = order?.OrderDescription,
        //                //                  OrderType = order?.TypeName,
        //                //                  ProblemType = order?.ProblemName,
        //                //                  ProgressDate = action?.ActionDate,
        //                //                  Executer = action?.CurrentUserId != null ? identityService.GetUserById(action?.CurrentUserId)?.Result?.Data?.FullName : string.Empty,
        //                //                  ProgressStatus = action?.StatusName,
        //                //                  ProgressSubStatus = action?.SubStatusName,
        //                //                  ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime : action?.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays.Value)),
        //                //                  // ProgressTime = action?.ActionTimeDays == 0 || action?.ActionTimeDays == null ? action?.ActionTime?.Value :new TimeSpan(1,1,1),
        //                //                  TechnicianNotes = action?.ServeyReport,
        //                //                  ClosedDate = action?.ActionDate,
        //                //                  Technician = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
        //                //                  TechnicianName = team?.Data.FirstOrDefault(x => x.Id == order.TeamId)?.Name,
        //                //                  CreatedDate = order?.SAP_CreatedDate ?? default(DateTime),
        //                //                  UpdatedDate = order?.UpdatedDate == DateTime.MinValue ? order.SAP_CreatedDate : order.UpdatedDate
        //                //              })?.ToList();
        //                //}
        //            }
        //        }
        //        if (orderAction.Data != null && orderAction.Data.Count > 0)
        //        {
        //            var skipCount = (model.PageInfo.PageNo - 1) * model.PageInfo.PageSize;
        //            output = new OrderManagementReportPaginatedViewModel
        //            {
        //                Count = result.Count,
        //                Result = result.Skip(skipCount).Take(model.PageInfo.PageSize).ToList()
        //            };
        //        }
        //        else if (model.IncludeProgressData && (orderAction.Data.Count == 0 || orderAction.Data == null))
        //        {
        //            output = new OrderManagementReportPaginatedViewModel
        //            {
        //                Count = 0,
        //                Result = new List<OrderManagementReportOutputViewModel>()
        //            };
        //        }
        //        else
        //        {
        //            output = new OrderManagementReportPaginatedViewModel
        //            {
        //                Count = paginatedRes.Data.Count,
        //                Result = result
        //            };
        //        }
        //        return ProcessResultHelper.Succedded(output, "OrderManagementReport");
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessResultHelper.Failed<OrderManagementReportPaginatedViewModel>(null, ex);
        //    }
        //}



        [HttpPost]
        [Route("GetOrderManagementReport")]
        public ProcessResultViewModel<List<OrderManagementReportItem>> GetOrderManagementReport([FromBody]OrderManagementReportViewModel model)
        {



            try
            {
                var predicate = PredicateBuilder.True<OrderObject>();
                var predicateOrderAction = PredicateBuilder.True<OrderAction>();

                if (model.AreaId.Count() > 0)
                {
                    predicate = predicate.And(r => model.AreaId.Contains(r.AreaId));
                }
                if (model.DispatcherId.Count() > 0)
                {
                    predicate = predicate.And(r => model.DispatcherId.Contains(r.DispatcherId ?? 0));//TODO Refactor issue
                }
                if (model.DivisionId.Count() > 0)
                {
                    predicate = predicate.And(r => model.DivisionId.Contains(r.DivisionId ?? 0));//TODO Refactor issue
                }
                if (model.OrderTypeId.Count() > 0)
                {
                    predicate = predicate.And(r => r.TypeId != null && model.OrderTypeId.Contains(r.TypeId.Value));
                }
                if (model.ProblemId.Count() > 0)
                {
                    predicate = predicate.And(r => model.ProblemId.Contains(r.ProblemId ?? 0));//TODO Refactor issue
                }
                if (model.StatusId.Count() > 0)
                {
                    predicate = predicate.And(r => model.StatusId.Contains(r.StatusId));
                }

                //model.TeamId = model.TeamId.Where(x => x != 0).Distinct().ToList();
                //if (model.TeamId.Count() > 0)
                //{
                //    //var teamIds = new List<int>();
                //    //foreach (var item in model.TechnicianId)
                //    //{
                //    //    var res = await teamMembersService.GetTeamIdByTechnicianMember(item);
                //    //    var teamId = res.Data?.FirstOrDefault()?.TeamId;
                //    //    if (teamId != null && teamId != 0)
                //    //    {
                //    //        teamIds.Add(teamId.Value);
                //    //    }
                //    //}
                //    predicate = predicate.And(r => model.TeamId.Contains(r.TeamId));
                //}
                if (!string.IsNullOrEmpty(model.CustomerCode))
                {
                    predicate = predicate.And(r => model.CustomerCode == r.CustomerCode);
                }
                if (model.CompanyCode.Count() > 0)
                {
                    predicate = predicate.And(r => model.CompanyCode.Contains(r.CompanyCodeId));
                }
                if (!string.IsNullOrEmpty(model.OrderId))
                {
                    predicate = predicate.And(r => model.OrderId == r.Code);
                }

                if (model.DateFrom != null && model.DateTo != null)
                {
                    predicate = predicate.And(x => (x.SAP_CreatedDate >= model.DateFrom) && (x.SAP_CreatedDate <= model.DateTo));
                }
                else if (model.DateFrom == null && model.DateTo == null)
                {
                    predicate = predicate.And(x => true);
                }
                else if (model.DateFrom == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate <= model.DateTo);
                }
                else if (model.DateTo == null)
                {
                    predicate = predicate.And(x => x.SAP_CreatedDate >= model.DateFrom);
                }

                var paginatedRes = manager.GetAll(predicate);
                var result = new List<OrderManagementReportItem>();

                if (paginatedRes.Data == null || paginatedRes.Data.Count == 0)
                {
                    return ProcessResultViewModelHelper.Succedded(result);
                }

                result = paginatedRes.Data.Select(x => new OrderManagementReportItem
                {
                    OrderId = x.Id,
                    OrderCode = x.Code,
                    Area = x.SAP_AreaName,
                    Block = x.SAP_BlockName,
                    Street = x.SAP_StreetName,
                    PACINum = x.SAP_PACI,
                    CompanyCode = x.CompanyCode.Code,
                    DispatcherNotes = x.DispatcherNote,
                    //Dispatcher = x.DispatcherName,
                    ICAgentNotes = x.ICAgentNote,
                    //Division = x.DivisionName,
                    //Status = x.StatusName,
                    OrderDesc = x.OrderDescription,
                    OrderType = x.Type?.Name,
                    //ProblemType = x.ProblemName,
                    ClosedDate = StaticAppSettings.ArchivedStatusId.Contains(x.StatusId) ? x.UpdatedDate : (DateTime?)null,
                    CreatedDate = x.SAP_CreatedDate,
                    UpdatedDate = x.UpdatedDate == DateTime.MinValue ? x.SAP_CreatedDate : x.UpdatedDate
                }).ToList();

                var techs = actionManager.GetAll(x => x.OrderId != null && result.Select(y => y.OrderId).Contains(x.OrderId.Value)).Data;

                foreach (var item in result)
                {
                    item.Technician = techs.FirstOrDefault(x => x.OrderId == item.OrderId)?.CreatedUser;
                }

                if (model.IncludeProgressData && paginatedRes.Data.Count > 0)
                {
                    var orderIds = paginatedRes.Data.Select(y => y.Id);
                    predicateOrderAction = predicateOrderAction.And(x => orderIds.Contains(x.OrderId.Value));
                    var orderAction = new ProcessResult<List<OrderAction>>();
                    if (model.AllProgress)
                    {
                        orderAction = actionManager.GetAll(predicateOrderAction);
                    }
                    else if (model.LastProgress)
                    {
                        orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate).FirstOrDefault());
                    }
                    else if (model.OnHoldOnly)
                    {
                        predicateOrderAction = predicateOrderAction.And(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                        orderAction = actionManager.GetAll(predicateOrderAction);
                    }
                    else if (model.OnHoldLastProgress)
                    {
                        predicateOrderAction = predicateOrderAction.And(x => x.StatusId == StaticAppSettings.OnHoldStatus.Id);
                        orderAction.Data.Add(actionManager.GetAll(predicateOrderAction).Data.OrderByDescending(x => x.ActionDate).FirstOrDefault());
                    }

                    if (orderAction.Data != null && orderAction.Data.Count > 0)
                    {
                        result = (from order in paginatedRes.Data
                                  join action in orderAction.Data.Where(x => x.OrderId != null) on order.Id equals action?.OrderId into act
                                  from action in act.DefaultIfEmpty()
                                  select new OrderManagementReportItem
                                  {
                                      OrderId = order.Id,
                                      OrderCode = order.Code,
                                      Area = order.SAP_AreaName,
                                      Block = order.SAP_BlockName,
                                      Street = order.SAP_StreetName,
                                      PACINum = order.SAP_PACI,
                                      CompanyCode = order.CompanyCode.Code,
                                      DispatcherNotes = order.DispatcherNote,
                                      //Dispatcher = order.DispatcherName,
                                      DispatcherName = action.Dispatcher.User.FullName,
                                      ICAgentNotes = order.ICAgentNote,
                                      //Division = order.DivisionName,
                                      Status = order.Status.Name,
                                      OrderDesc = order.OrderDescription,
                                      OrderType = order.Type?.Name,
                                      //ProblemType = order.ProblemName,
                                      ProgressDate = action.ActionDate,
                                      ProgressStatus = action.Status.Name,
                                      ProgressSubStatus = action.SubStatus?.Name,
                                      ProgressTime = action.ActionTimeDays == 0 || action.ActionTimeDays == null ? action.ActionTime.Value : action.ActionTime.Value.Add(TimeSpan.FromDays(action.ActionTimeDays.Value)),
                                      TechnicianNotes = action.ServeyReport,
                                      ClosedDate = action.ActionDate,
                                      TechnicianName = action.CreatedUser,
                                      Technician = action.CreatedUser,
                                      CreatedDate = order.SAP_CreatedDate,
                                      UpdatedDate = order.UpdatedDate == DateTime.MinValue ? order.SAP_CreatedDate : order.UpdatedDate
                                  }).ToList();
                    }
                }
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<OrderManagementReportItem>>(null, ex.Message);
            }
        }



        [HttpGet]
        [AllowAnonymous]
        [Route("addFtbOrderActionsToOrdersThatHavNot")]
        public int addFtbOrderActionsToOrdersThatHavNot()
        {
            Console.Write($"Starting The RegisterIdleBreakNotifications{DateTime.Now:F}");


            List<int> orderActionsIds = new List<int>();
            List<OrderObject> orders =
                _context.Orders.Where(x => !x.OrderActions.Any(a => a.PlatformTypeSource == PlatformType.Ftp)).ToList();


            if (orders.Count > 0)
            {
                foreach (var order in orders)
                {
                    var orderACtion = new OrderAction()
                    {
                        OrderId = order.Id,
                        ActionDate = DateTime.Now,
                        PlatformTypeSource = PlatformType.Ftp
                    };
                    _context.OrderActions.Add(orderACtion);
                    orderActionsIds.Add(orderACtion.Id);
                }
                return _context.SaveChanges();
            }

            return 0;


        }










        #endregion

        #region  EnhanceForBoardsPerformance
        [HttpGet]
        [Route("get-dispatcher-teams")]
        public async Task<ProcessResult<List<FormanTeamsViewModel>>> GetDispatcherTeams([FromQuery] int dispatcherId)
        {
            return ProcessResultHelper.Succedded<List<FormanTeamsViewModel>>(await _orderService.GetDispatchersTeam(dispatcherId));

        }
        [HttpPost]
        [Route("get-teams-orders")]
        public async Task<ProcessResult<PaginatedItemsViewModel<OrderCardViewModel>>> GetTeamsOrders([FromBody]PaginatedTeamOrdersViewModel model)
        {
            var orders = await _orderService.GetTeamOrders(model);
            var result = new PaginatedItemsViewModel<OrderCardViewModel> { Data = orders.Item1, Count = orders.Item2, PageNo = model.PageInfo.PageNo, PageSize = model.PageInfo.PageSize };
            return ProcessResultHelper.Succedded(result);
        }

        #endregion


    }
}