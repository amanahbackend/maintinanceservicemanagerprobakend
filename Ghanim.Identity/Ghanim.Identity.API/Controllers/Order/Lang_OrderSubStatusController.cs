﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;

using Ghanim.BLL.IManagers;
using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Route("api/[controller]")]
        [Authorize]
    public class Lang_OrderSubStatusController : BaseAuthforAdminController<ILang_OrderSubStatusManager, Lang_OrderSubStatus, Lang_OrderSubStatusViewModel>
    {
        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_OrderSubStatusManager manager;
        IProcessResultMapper processResultMapper;
        IOrderSubStatusManager OrderSubStatusmanager;
        public Lang_OrderSubStatusController(IServiceProvider serviceprovider, ISupportedLanguageService _supportedLanguageService, ILang_OrderSubStatusManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IOrderSubStatusManager _OrderSubStatusmanager) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            OrderSubStatusmanager = _OrderSubStatusmanager;
        }
        //private IOrderSubStatusManager OrderSubStatusmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<IOrderSubStatusManager>();
        //    }
        //}

        [HttpGet]
        [Route("GetAllLanguagesByOrderSubStatusId/{OrderSubStatusId}")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguagesByOrderSubStatusId([FromRoute]int OrderSubStatusId)
        {
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var OrderSubStatusRes = OrderSubStatusmanager.Get(OrderSubStatusId);
            var entityResult = manager.GetAllLanguagesByOrderSubStatusId(OrderSubStatusId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }

            codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = OrderSubStatusRes.Data.Code, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetAllLanguages()
        {
            var SupportedLanguagesRes = await supportedLanguageService.GetAll();

            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary;
            var OrderSubStatusRes = OrderSubStatusmanager.GetAll();
            foreach (var OrderSubStatus in OrderSubStatusRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = manager.GetAllLanguagesByOrderSubStatusId(OrderSubStatus.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = OrderSubStatus.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = OrderSubStatus.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = OrderSubStatus.Code, languagesDictionaries = languagesDictionary, Id = OrderSubStatus.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }
        [HttpGet]
        [Route("GetByStatusIdWithBlocked")]
        public async Task<ProcessResultViewModel<List<CodeWithLanguagesViewModel>>> GetByStatusIdWithBlocked([FromQuery] int id)
        {
           // var SupportedLanguagesRes = await supportedLanguageService.GetAll();
            //var SupportedLanguagesRes = await supportedLanguageService.GetAllWithBlocked();
            List<CodeWithLanguagesViewModel> codeWithAllLanguages = new List<CodeWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary;
            //var OrderSubStatusRes = OrderSubStatusmanager.GetAllWithBlocked();            
            var OrderSubStatusRes = await GetByStatusIdWithBlockedNew(id);
            var OrderedOrderSubStatusRes = OrderSubStatusRes.Data.OrderByDescending(o => o.Id).ToList();
            foreach (var OrderSubStatus in OrderedOrderSubStatusRes)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = await GetAllLanguagesByOrderSubStatusIdNew(OrderSubStatus.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes != null && (SupportedLanguageRes.Data?.Name ?? "English") == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = (SupportedLanguageRes.Data?.Name ?? "English"), Value = OrderSubStatus.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //{
                //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //    {
                //        if (SupportedLanguage.Name == "English")
                //        {
                //            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = OrderSubStatus.Name });

                //        }
                //        else
                //        {
                //            languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //        }
                //    }
                //}
                codeWithAllLanguages.Add(new CodeWithLanguagesViewModel { Code = OrderSubStatus.Code, languagesDictionaries = languagesDictionary, Id = OrderSubStatus.Id, IsDeleted = OrderSubStatus.IsDeleted });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }
        private async Task<ProcessResult<List<Lang_OrderSubStatus>>> GetAllLanguagesByOrderSubStatusIdNew(int OrderSubStatusId)
        {
            List<Lang_OrderSubStatus> input = null;
            try
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetAll();
                var SupportedLanguagesRes2 = SupportedLanguagesRes.Data.OrderBy(x => x.Id).ToList();
                // input = manager.GetAllQuerable().Data.Where(x => x.FK_OrderSubStatus_ID == OrderSubStatusId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();

                input = manager.GetAllQuerable().Data.Where(x => x.FK_OrderSubStatus_ID == OrderSubStatusId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = SupportedLanguagesRes2.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_OrderSubStatus { FK_OrderSubStatus_ID = OrderSubStatusId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderSubStatus>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByOrderSubStatusId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderSubStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByOrderSubStatusId");
            }
        }
        private async Task<ProcessResult<List<OrderSubStatus>>> GetByStatusIdWithBlockedNew(int statusId)
        {
            return OrderSubStatusmanager.GetAllWithBlocked(x => x.StatusId == statusId);
        }
        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_OrderSubStatus> lstModel)
        {
            var entityResult = manager.UpdateByOrderSubStatus(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
           
        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody]CodeWithLanguagesViewModel Model)
        {
            string SubStatusNewEnNAme = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;

            List<Lang_OrderSubStatus> lstModel = new List<Lang_OrderSubStatus>();
            var isValidToUpdate =
                OrderSubStatusmanager.CheckOrderSubStatusNoDoublication(SubStatusNewEnNAme, Model.Id, Model.Code);
            if (!isValidToUpdate.IsSucceeded)
                return ProcessResultViewModelHelper.Failed<bool>(false, isValidToUpdate.Status.Message, ProcessResultStatusCode.InvalidValue);


            //  var OrderSubStatusRes = OrderSubStatusmanager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                // lstModel.Add(new Lang_OrderSubStatus { FK_OrderSubStatus_ID = OrderSubStatusRes.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_OrderSubStatus { FK_OrderSubStatus_ID = Model.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = await UpdateByOrderSubStatusNew(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
        private async Task<ProcessResult<bool>> UpdateByOrderSubStatusNew(List<Lang_OrderSubStatus> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var ordersubstatusRes = OrderSubStatusmanager.Get(item.FK_OrderSubStatus_ID);
                        if (ordersubstatusRes != null && ordersubstatusRes.Data != null)
                        {
                            ordersubstatusRes.Data.Name = item.Name;
                            var updateRes = OrderSubStatusmanager.Update(ordersubstatusRes.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangOrderSubStatus = await GetByOrderSubStatusIdAndsupprotedLangIdNew(item.FK_OrderSubStatus_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangOrderSubStatus != null && prevLangOrderSubStatus.Data != null && prevLangOrderSubStatus.Data.Count == 0)
                        {
                            result = manager.Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        result = manager.Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderSubStatus");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByOrderSubStatus");
            }
        }
        private async Task<ProcessResult<List<Lang_OrderSubStatus>>> GetByOrderSubStatusIdAndsupprotedLangIdNew(int OrderStatusId, int supprotedLangId)
        {
            List<Lang_OrderSubStatus> input = null;
            try
            {
                input = manager.GetAllQuerable().Data.Where(x => x.FK_OrderSubStatus_ID == OrderStatusId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_OrderSubStatus>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByOrderStatusIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_OrderSubStatus>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByOrderStatusIdAndsupprotedLangId");
            }
        }

    }
}