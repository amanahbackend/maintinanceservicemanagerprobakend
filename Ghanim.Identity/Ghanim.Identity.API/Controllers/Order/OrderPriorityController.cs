﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OrderPriorityController : BaseAuthforAdminController<IOrderPriorityManager, OrderPriority, OrderPriorityViewModel>
    {
        IServiceProvider _serviceprovider;
        IOrderPriorityManager manager;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public OrderPriorityController(IServiceProvider serviceprovider,IOrderPriorityManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manger;
            //mapper = _mapper;
            _serviceprovider = serviceprovider;
            processResultMapper = _processResultMapper;
        }
        private ILang_OrderPriorityManager Lang_OrderPriorityManager
        {
            get
            {
                return _serviceprovider.GetService<ILang_OrderPriorityManager>();
            }
        }

        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<OrderPriorityViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            var actions = manager.GetAll();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderPriorityLocalizesRes = Lang_OrderPriorityManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_OrderPriority_ID == item.Id);
                    if (OrderPriorityLocalizesRes != null && OrderPriorityLocalizesRes.Data != null)
                    {
                        item.Name = OrderPriorityLocalizesRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                //var result = mapper.Map<List<OrderProblem>, List<OrderProblemViewModel>>(actions.Data);
                //return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
                //return ProcessResultHelper.Succedded<List<AreasViewModel>>(result);
            }
            return processResultMapper.Map<List<OrderPriority>, List<OrderPriorityViewModel>>(actions);
        }
    }
}