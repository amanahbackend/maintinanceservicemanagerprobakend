﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.Models.Identity.Static;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_CustomerTypeController : BaseAuthforAdminController<ILang_CustomerTypeManager, Lang_CustomerType, Lang_CustomerTypeViewModel>
    {

        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_CustomerTypeManager manager;
        IProcessResultMapper processResultMapper;
        ICustomerTypeManager CustomerTypeManager;
        public Lang_CustomerTypeController(IServiceProvider serviceprovider, ISupportedLanguageService _supportedLanguageService, ILang_CustomerTypeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICustomerTypeManager _CustomerTypeManager) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            CustomerTypeManager = _CustomerTypeManager;
        }



        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public async Task<ProcessResultViewModel<List<NullWithLanguagesViewModel>>> GetAllLanguagesWithBlockedAsync()
        {
            List<NullWithLanguagesViewModel> TypesWithAllLanguages = new List<NullWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary;
            var TypeRes = CustomerTypeManager.GetAllWithBlocked();
            var CustomerTypeRes = TypeRes.Data.OrderByDescending(o => o.Id).ToList();
            foreach (var Type in CustomerTypeRes)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = manager.GetAllLanguagesByCustomerTypeIdWithBlocked(Type.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes != null && (SupportedLanguageRes.Data.Name ?? "English") == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = (SupportedLanguageRes.Data?.Name ?? "English"), Value = Type.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }

                }

                TypesWithAllLanguages.Add(new NullWithLanguagesViewModel { languagesDictionaries = languagesDictionary, Id = Type.Id, IsDeleted = Type.IsDeleted });
            }

            var result = ProcessResultViewModelHelper.Succedded<List<NullWithLanguagesViewModel>>(TypesWithAllLanguages);

            return result;
        }


        [HttpGet]
        [Route("GetAllLanguagesByCustomerTypeId/{CustomerTypeId}")]
        public async Task<ProcessResultViewModel<List<LanguagesDictionariesViewModel>>> GetAllLanguagesByCustomerTypeId([FromRoute] int CustomerTypeId)
        {
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var Res = CustomerTypeManager.Get(CustomerTypeId);
            var entityResult = manager.GetAllLanguagesByCustomerTypeId(CustomerTypeId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<LanguagesDictionariesViewModel>>(languagesDictionary);
            return result;
        }




        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody] NullWithLanguagesViewModel Model)
        {
            string enName = Model.languagesDictionaries.FirstOrDefault(d => d.Key == "English").Value;

            var checkNoDub = CustomerTypeManager.CheckCustomerTypeNoDoublication(enName, Model.Id);
            if (!checkNoDub.IsSucceeded)
                return ProcessResultViewModelHelper.Failed<bool>(false, checkNoDub.Status.Message, ProcessResultStatusCode.InvalidValue);
            List<Lang_CustomerType> lstModel = new List<Lang_CustomerType>();
            var Res = CustomerTypeManager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_CustomerType { FK_CustomerType_ID = Res.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = await UpdateByCsutomerTypeNew(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        private async Task<ProcessResult<List<Lang_CustomerType>>> GetAllLanguagesByCustomerTypeIdNew(int CustomerTypeId)
        {
            List<Lang_CustomerType> input = null;
            try
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetAll();
                var OrderedSupportedLanguagesRes = SupportedLanguagesRes.Data.OrderBy(x => x.Id).ToList();
                input = manager.GetAllQuerable().Data.Where(x => x.FK_CustomerType_ID == CustomerTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = OrderedSupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_CustomerType { FK_CustomerType_ID = CustomerTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_CustomerType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByCustomerTypeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_CustomerType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByCustomerTypeId");
            }
        }


        private async Task<ProcessResult<bool>> UpdateByCsutomerTypeNew(List<Lang_CustomerType> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var Res = CustomerTypeManager.Get(item.FK_CustomerType_ID);
                        if (Res != null && Res.Data != null)
                        {
                            Res.Data.Name = item.Name;
                            var updateRes = CustomerTypeManager.Update(Res.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangCustomerType= await GetByCustomerTypeIdAndsupprotedLangIdNew(item.FK_CustomerType_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangCustomerType != null && prevLangCustomerType.Data != null && prevLangCustomerType.Data.Count == 0)
                        {
                            result = manager.Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        if (supportedLanguageRes.Data.Name == "English" && !CustomerTypeManager.CheckCanUpdateOrBlock(item.FK_CustomerType_ID))
                        { }
                        else
                            result = manager.Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByCustomerType");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByCustomerType");
            }
        }



        private async Task<ProcessResult<List<Lang_CustomerType>>> GetByCustomerTypeIdAndsupprotedLangIdNew(int CustomerTypeId, int supprotedLangId)
        {
            List<Lang_CustomerType> input = null;
            try
            {
                input = manager.GetAllQuerable().Data.Where(x => x.FK_CustomerType_ID == CustomerTypeId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_CustomerType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByCustomerTypeIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_CustomerType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByCustomerTypeIdAndsupprotedLangId");
            }
        }

    }
}
