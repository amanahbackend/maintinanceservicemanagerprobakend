﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.API.ServiceCommunications.DataManagementService.Problem;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class CustomerTypeController : BaseAuthforAdminController<ICustomerTypeManager, CustomerType, CustomerTypeViewModel>
    {
        IServiceProvider _serviceprovider;
        ICustomerTypeManager manager;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IProblemService _problemService;
        ILang_CustomerTypeManager Lang_CustomerTypeManager;
        public CustomerTypeController(IProblemService problemService, IServiceProvider serviceprovider, ICustomerTypeManager _manger, 
            IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ICached<CustomerTypeViewModel> cash, ILang_CustomerTypeManager _Lang_CustomerTypeManager) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper, cash)
        {
            manager = _manger;
            //mapper = _mapper;
            _serviceprovider = serviceprovider;
            processResultMapper = _processResultMapper;
            _problemService = problemService;
            Lang_CustomerTypeManager = _Lang_CustomerTypeManager;

        }
        //private ILang_CustomerTypeManager Lang_CustomerTypeManager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ILang_CustomerTypeManager>();
        //    }
        //}

        //For Web
        [HttpGet]
        [Route("GetAll")]///{langCode?}
        public override ProcessResultViewModel<List<CustomerTypeViewModel>> Get()
        {
            //int? langCode=null
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //string languageIdValue = langCode.ToString();
            var actions = base.Get();
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var Res = Lang_CustomerTypeManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_CustomerType_ID == item.Id);
                    if (Res != null && Res.Data != null)
                    {
                        item.Name = Res.Data.Name;
                    }
                }
            }

            return actions;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<CustomerTypeViewModel> Post([FromBody] CustomerTypeViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<CustomerTypeViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<CustomerTypeViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] CustomerTypeViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                //return new ProcessResultViewModel<bool>("ProcessResultViewModel");
            return ProcessResultViewModelHelper.Failed<bool>(false, "Customer Type already existed with that name or code", ProcessResultStatusCode.InvalidValue);

        }
    }
}
