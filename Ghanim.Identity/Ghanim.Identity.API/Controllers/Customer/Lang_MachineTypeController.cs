﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.API.ServiceCommunications.DataManagementService.SupportedLanguageService;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_MachineTypeController : BaseAuthforAdminController<ILang_MachineTypeManager, Lang_MachineType, Lang_MachineTypeViewModel>
    {

        ISupportedLanguageService supportedLanguageService;
        IServiceProvider _serviceprovider;
        ILang_MachineTypeManager manager;
        IProcessResultMapper processResultMapper;
        ISupportedLanguagesManager supportedLanguagesmanager;
        IMachineTypeManager MachineTypeManager;

        public Lang_MachineTypeController(IServiceProvider serviceprovider, ISupportedLanguageService _supportedLanguageService,
            ILang_MachineTypeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper, ISupportedLanguagesManager _supportedLanguagesmanager, IMachineTypeManager _MachineTypeManager) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            supportedLanguageService = _supportedLanguageService;
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            supportedLanguagesmanager = _supportedLanguagesmanager;
            MachineTypeManager = _MachineTypeManager;

        }





        [HttpGet]
        [Route("GetAllLanguagesByMachineTypeId/{MachinetypeId}")]
        public async Task<ProcessResultViewModel<List<LanguagesDictionariesViewModel>>> GetAllLanguagesByMachineTypeId([FromRoute] int MachinetypeId)
        {
            List<LanguagesDictionariesViewModel> languagesDictionary = new List<LanguagesDictionariesViewModel>();
            var Res = MachineTypeManager.Get(MachinetypeId);
            var entityResult = manager.GetAllLanguagesByMachineTypeId(MachinetypeId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<LanguagesDictionariesViewModel>>(languagesDictionary);
            return result;
        }


        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public ProcessResultViewModel<List<NullWithLanguagesViewModel>> GetAllLanguagesWithBlocked()
        {
            List<NullWithLanguagesViewModel> TypesWithAllLanguages = new List<NullWithLanguagesViewModel>();
            List<LanguagesDictionariesViewModel> languagesDictionary;
            var TypeRes = MachineTypeManager.GetAllWithBlocked();
            var MachineTypeRes = TypeRes.Data.OrderByDescending(o => o.Id).ToList();
            foreach (var Type in MachineTypeRes)
            {
                languagesDictionary = new List<LanguagesDictionariesViewModel>();
                var entityResult = manager.GetAllLanguagesByMachineTypeIdWithBlocked(Type.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes != null && (SupportedLanguageRes.Data?.Name ?? "English") == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = (SupportedLanguageRes.Data?.Name ?? "English"), Value = Type.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionariesViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }

                }

                TypesWithAllLanguages.Add(new NullWithLanguagesViewModel { languagesDictionaries = languagesDictionary, Id = Type.Id, IsDeleted = Type.IsDeleted });
            }

            var result = ProcessResultViewModelHelper.Succedded<List<NullWithLanguagesViewModel>>(TypesWithAllLanguages);

            return result;
        }



        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public async Task<ProcessResultViewModel<bool>> UpdateAllLanguages([FromBody] NullWithLanguagesViewModel Model)
        {
            string enName = Model.languagesDictionaries.FirstOrDefault(d => d.Key == "English").Value;

            var checkNoDub = MachineTypeManager.CheckMachineTypeNoDoublication(enName, Model.Id);
            if (!checkNoDub.IsSucceeded)
                return ProcessResultViewModelHelper.Failed<bool>(false, checkNoDub.Status.Message, ProcessResultStatusCode.InvalidValue);
            List<Lang_MachineType> lstModel = new List<Lang_MachineType>();
            var Res = MachineTypeManager.Get(Model.Id);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_MachineType { FK_MachineType_ID = Res.Data.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = await UpdateByMAchineTypeNew(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        private async Task<ProcessResult<List<Lang_MachineType>>> GetAllLanguagesByMachineTypeIdNew(int MachineTypeId)
        {
            List<Lang_MachineType> input = null;
            try
            {
                var SupportedLanguagesRes = await supportedLanguageService.GetAll();
                var OrderedSupportedLanguagesRes = SupportedLanguagesRes.Data.OrderBy(x => x.Id).ToList();
                input = manager.GetAllQuerable().Data.Where(x => x.FK_MachineType_ID == MachineTypeId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                var res = OrderedSupportedLanguagesRes.Where(x => !input.Select(y => y.FK_SupportedLanguages_ID).ToList().Contains(x.Id));
                foreach (var SupportedLanguage in res)
                {
                    input.Add(new Lang_MachineType { FK_MachineType_ID = MachineTypeId, FK_SupportedLanguages_ID = SupportedLanguage.Id, Name = string.Empty });
                }
                input = input.OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_MachineType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetAllLanguagesByMachineTypeId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_MachineType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetAllLanguagesByMachineTypeId");
            }
        }


        private async Task<ProcessResult<bool>> UpdateByMAchineTypeNew(List<Lang_MachineType> entities)
        {
            bool result = false;
            try
            {
                foreach (var item in entities)
                {
                    var supportedLanguageRes = await supportedLanguageService.Get(item.FK_SupportedLanguages_ID);
                    if (supportedLanguageRes != null && supportedLanguageRes.Data != null && supportedLanguageRes.Data.Name == "English")
                    {
                        var Res = MachineTypeManager.Get(item.FK_MachineType_ID);
                        if (Res != null && Res.Data != null)
                        {
                            Res.Data.Name = item.Name;
                            var updateRes = MachineTypeManager.Update(Res.Data);
                        }
                    }

                    if (item.Id == 0 && item.Name != string.Empty && item.Name != null)
                    {
                        var prevLangMachineType= await GetByMachineTypeIdAndsupprotedLangIdNew(item.FK_MachineType_ID, item.FK_SupportedLanguages_ID);
                        if (prevLangMachineType != null && prevLangMachineType.Data != null && prevLangMachineType.Data.Count == 0)
                        {
                            result = manager.Add(item).IsSucceeded;
                        }
                    }
                    else if (item.Id != 0)
                    {
                        if (supportedLanguageRes.Data.Name == "English" && !MachineTypeManager.CheckCanUpdateOrBlock(item.FK_MachineType_ID))
                        { }
                        else
                            result = manager.Update(item).Data;
                    }
                    else
                    {
                        result = true;
                    }
                }
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByMachineType");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Succedded<bool>(result, (string)null, ProcessResultStatusCode.Succeded, "UpdateByMachineType");
            }
        }



        private async Task<ProcessResult<List<Lang_MachineType>>> GetByMachineTypeIdAndsupprotedLangIdNew(int MachineTypeId, int supprotedLangId)
        {
            List<Lang_MachineType> input = null;
            try
            {
                input = manager.GetAllQuerable().Data.Where(x => x.FK_MachineType_ID == MachineTypeId && x.FK_SupportedLanguages_ID == supprotedLangId).OrderBy(x => x.FK_SupportedLanguages_ID).ToList();
                return ProcessResultHelper.Succedded<List<Lang_MachineType>>(input, (string)null, ProcessResultStatusCode.Succeded, "GetByMachineTypeIdAndsupprotedLangId");
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed<List<Lang_MachineType>>(input, ex, (string)null, ProcessResultStatusCode.Failed, "GetByMachineTypeIdAndsupprotedLangId");
            }
        }

    }
}
