﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers.Machine
{
    [Authorize]
    [Route("api/[controller]")]
    public class MachineTypeController : BaseAuthforAdminController<IMachineTypeManager, MachineType, MachineTypeViewModel>
    {
        IServiceProvider _serviceprovider;
        IMachineTypeManager manager;
        IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        ILang_MachineTypeManager Lang_MachineTypeManager;
        public MachineTypeController(IServiceProvider serviceprovider, IMachineTypeManager _manger,
            IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ICached<MachineTypeViewModel> cash, ILang_MachineTypeManager _Lang_MachineTypeManager) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper, cash)
        {
            manager = _manger;
            //mapper = _mapper;
            _serviceprovider = serviceprovider;
            processResultMapper = _processResultMapper;
            Lang_MachineTypeManager = _Lang_MachineTypeManager;

        }


        //For Web
        [HttpGet]
        [Route("GetAll")]///
        public override ProcessResultViewModel<List<MachineTypeViewModel>> Get()
        {
            //int? langCode=null
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //string languageIdValue = langCode.ToString();
            var actions = base.Get();
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var Res = Lang_MachineTypeManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_MachineType_ID == item.Id);
                    if (Res != null && Res.Data != null)
                    {
                        item.Name = Res.Data.Name;
                    }
                }
            }

            return actions;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<MachineTypeViewModel> Post([FromBody] MachineTypeViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<MachineTypeViewModel>(null, "Item Already Existed With That Name", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<MachineTypeViewModel>(null, "Item Already Existed With That Name", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] MachineTypeViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                //return new ProcessResultViewModel<bool>("ProcessResultViewModel");
            return ProcessResultViewModelHelper.Failed<bool>(false, "Phone Type already existed with that name", ProcessResultStatusCode.InvalidValue);

        }
    }
}
