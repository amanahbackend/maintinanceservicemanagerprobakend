﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Ghanim.Models.Identity.Static;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ShiftController : BaseAuthforAdminController<IShiftManager, Shift, ShiftViewModel>
    {
         IServiceProvider _serviceprovider;
        IShiftManager manager;
        IProcessResultMapper processResultMapper;
        public ShiftController(IServiceProvider serviceprovider, IShiftManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper ) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
 
        }
        private ILang_ShiftManager Lang_Shiftmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_ShiftManager>();
            }
        }
        private Expression<Func<Shift, bool>> IdenticalPredicate(ShiftViewModel model)
        {
            return PredicateBuilder.True<Shift>().And(
                 x =>
                     (x.ToTime.Hours == model.ToTime.Hour && x.ToTime.Minutes == model.ToTime.Minute)
                     &&
                     (x.FromTime.Hours == model.FromTime.Hour && x.FromTime.Minutes == model.FromTime.Minute)
             );
        }




        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetShiftByLanguage")]
        public ProcessResultViewModel<ShiftViewModel> GetShiftByLanguage([FromQuery]int ShiftId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_ShiftRes = Lang_Shiftmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(ShiftId);
            entityResult.Data.ToTime = Lang_ShiftRes.Data.ToTime;
            entityResult.Data.FromTime = Lang_ShiftRes.Data.FromTime;
            var result = processResultMapper.Map<Shift, ShiftViewModel>(entityResult);
            return result;
        }


        [HttpGet]
        [Route("GetShiftWithAllLanguages/{ShiftId}")]
        public ProcessResultViewModel<List<ShiftViewModel>> GetShiftWithAllLanguages([FromRoute]int ShiftId)
        {
            ProcessResult<List<Shift>> entityResult = new ProcessResult<List<Shift>>();
            var Lang_ShiftRes = Lang_Shiftmanager.GetAll().Data.FindAll(x => x.FK_Shift_ID == ShiftId);
            foreach (var item in Lang_ShiftRes)
            {
                var ShiftRes = manager.Get(x => x.Id == item.FK_Shift_ID);
                ShiftRes.Data.ToTime = item.ToTime;
                ShiftRes.Data.FromTime = item.FromTime;
                entityResult.Data.Add(ShiftRes.Data);
            }
            var result = processResultMapper.Map<List<Shift>, List<ShiftViewModel>>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<ShiftViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "1";
            var actions = manager.GetAll();
            //FillCurrentUser(entityResult);
            //if (languageIdValue != null && languageIdValue != "")
            //{
            //    foreach (var item in actions.Data)
            //    {
            //        var ShifLocalizesRes = Lang_Shiftmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_Shift_ID == item.Id);
            //        if (ShifLocalizesRes != null && ShifLocalizesRes.Data != null)
            //        {
            //            item.Name = ShifLocalizesRes.Data.Name;
            //        }
            //    }
            //}
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<Shift>, List<ShiftViewModel>>(actions.Data);
                ;
            }
            return processResultMapper.Map<List<Shift>, List<ShiftViewModel>>(actions);
        }

        

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<ShiftViewModel> Post([FromBody] ShiftViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<ShiftViewModel>(null, SharedResource.General_InvalidInput, ProcessResultStatusCode.InvalidValue);

            model.FromTime = new DateTime(1, 1, 1, model.FromTime.Hour, model.FromTime.Minute, 0);
            model.ToTime = new DateTime(1, 1, 1, model.ToTime.Hour, model.ToTime.Minute, 0);

            //if (model.ToTime <= model.FromTime)
            //    return ProcessResultViewModelHelper.Failed<ShiftViewModel>(null, "The ToTime Value Must Be Greater than the FromTime Value", ProcessResultStatusCode.InvalidValue);

            Shift result = mapper.Map<ShiftViewModel, Shift>(model);

            var entity = manager.Get(IdenticalPredicate(model))?.Data;


            if (entity != null)
                return ProcessResultViewModelHelper.Failed<ShiftViewModel>(null, SharedResource.Shift_Dublication, ProcessResultStatusCode.InvalidValue);
          
            var res = base.Post(model);
            return res;
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] ShiftViewModel model)
        {
            model.FromTime = new DateTime(1, 1, 1, model.FromTime.Hour, model.FromTime.Minute, 0);
            model.ToTime = new DateTime(1, 1, 1, model.ToTime.Hour, model.ToTime.Minute, 0);

            //if (model.ToTime <= model.FromTime)
            //    return ProcessResultViewModelHelper.Failed<bool>(false, "The ToTime Value Must Be Greater than the FromTime Value", ProcessResultStatusCode.InvalidValue);



            var all = manager.GetAllQuerableWithBlocked().Data.Where(IdenticalPredicate(model)).ToList();
            //emad: to slolve the worng data added before
            if (all != null && all.Count() >1)
                return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.Shift_Dublication, ProcessResultStatusCode.InvalidValue);
            

            if (all == null||all.Count() ==0 || all.FirstOrDefault().Id == model.Id)
                return base.Put(model);
            else
                return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.Shift_Dublication, ProcessResultStatusCode.InvalidValue);

            //return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
