﻿using AutoMapper;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.Models.Identity.Static;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [ApiVersion("1.0")]
    [Authorize(Roles = StaticRoles.Closed)]
    [Route("api/[controller]")]
    public class JunkUserController : Controller
    {
        private IJunkUserManager _junkUserManager;

        public JunkUserController(IJunkUserManager junkUserManager)
        {
            _junkUserManager = junkUserManager;
        }

        [HttpGet, Route("GetAll")]
        public ProcessResultViewModel<List<JunkUserViewModel>> GetAll()
        {
            ProcessResultViewModel<List<JunkUserViewModel>> result = null;
            try
            {
                var junkUsers = _junkUserManager.GetAll().Data.ToList();
                var junkUsersModel = Mapper.Map<List<JunkUser>, List<JunkUserViewModel>>(junkUsers);
                result = ProcessResultViewModelHelper.Succedded(junkUsersModel);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<JunkUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("Delete")]
        public ProcessResultViewModel<bool> Delete([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                bool isDeleted = _junkUserManager.DeleteByUsername(username).Data;
                result = ProcessResultViewModelHelper.Succedded(isDeleted);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }
    }
}
