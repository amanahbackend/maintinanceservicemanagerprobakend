﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize(Roles = StaticRoles.Admin)]
    [Route("api/[controller]")]
    public class AttendenceController : BaseAuthforAdminController<IAttenenceManager, Attendence, AttendenceViewModel>
    {
        public IAttenenceManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        public AttendenceController(IAttenenceManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
        }

    }
}