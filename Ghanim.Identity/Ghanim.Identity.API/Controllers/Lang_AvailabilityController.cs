﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using System.Linq;
using Ghanim.Models.Identity.Static;
using Ghanim.ResourceLibrary.Resources;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_AvailabilityController : BaseAuthforAdminController<ILang_AvailabilityManager, Lang_Availability, Lang_AvailabilityViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_AvailabilityManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_AvailabilityController(IServiceProvider serviceprovider, ILang_AvailabilityManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IAvailabilityManager Availabilitymanager
        {
            get
            {
                return _serviceprovider.GetService<IAvailabilityManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [Route("GetAllWithBlocked")]
        [HttpGet]
        public override ProcessResultViewModel<List<Lang_AvailabilityViewModel>> GetAllWithBlocked()
        {

            var entityResult = manger.GetAllWithBlocked();
            //FillCurrentUser(entityResult);
            return processResultMapper.Map<List<Lang_Availability>, List<Lang_AvailabilityViewModel>>(entityResult);
        }
        [HttpGet]
        [Route("GetAllLanguagesByAvailabilityId/{AvailabilityId}")]
        public ProcessResultViewModel<List<LanguagesDictionaryViewModel>> GetAllLanguagesByAvailabilityId([FromRoute]int AvailabilityId)
        {
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var AvailabilityStatesRes = Availabilitymanager.Get(AvailabilityId);
            var entityResult = manager.GetAllLanguagesByAvailabilityId(AvailabilityId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<LanguagesDictionaryViewModel>>(languagesDictionary);

            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<NullWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<NullWithAllLanguagesViewModel> nullWithAllLanguages = new List<NullWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var AvailabilityStatesRes = Availabilitymanager.GetAll();
            foreach (var AvailabilityStates in AvailabilityStatesRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByAvailabilityId(AvailabilityStates.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = AvailabilityStates.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = AvailabilityStates.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                nullWithAllLanguages.Add(new NullWithAllLanguagesViewModel { languagesDictionaries = languagesDictionary, Id = AvailabilityStates.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<NullWithAllLanguagesViewModel>>(nullWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public ProcessResultViewModel<List<NullWithAllLanguagesViewModel>> GetAllLanguagesWithBlocked()
        {
            //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithoutBlocked();
            List<NullWithAllLanguagesViewModel> nullWithAllLanguages = new List<NullWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var AvailabilityStatesRes = Availabilitymanager.GetAllWithBlocked();
            var OrderedAvailabilityStatesRes = AvailabilityStatesRes.Data.OrderByDescending(o => o.Id).ToList();
            foreach (var AvailabilityStates in OrderedAvailabilityStatesRes)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByAvailabilityId(AvailabilityStates.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = AvailabilityStates.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //{
                //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //    {
                //        if (languagesDictionary.Any(x => x.Key == SupportedLanguage.Name))
                //            continue;


                //       if (SupportedLanguage.Name == "English")
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = AvailabilityStates.Name });

                //        }
                //        else
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //        }
                //    }
                //}
                nullWithAllLanguages.Add(new NullWithAllLanguagesViewModel { languagesDictionaries = languagesDictionary, Id = AvailabilityStates.Id, IsDeleted = AvailabilityStates.IsDeleted });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<NullWithAllLanguagesViewModel>>(nullWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_Availability> lstModel)
        {
            var entityResult = manager.UpdateByAvailability(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]NullWithAllLanguagesViewModel Model)
        {
            string areaNewEnNAme = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;
            List<Lang_Availability> lstModel = new List<Lang_Availability>();
            var stroredArea = Availabilitymanager.Get(x => x.Name == areaNewEnNAme).Data;
            if (stroredArea != null && (stroredArea?.Id ?? 0) != Model.Id)
                return ProcessResultViewModelHelper.Failed<bool>(false, string.Format(SharedResource.General_DuplicateWithoutCode, SharedResource.Entity_Availability), ProcessResultStatusCode.InvalidValue);


            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_Availability { FK_Availability_ID = Model.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = manager.UpdateByAvailability(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
