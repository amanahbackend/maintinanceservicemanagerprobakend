﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;
using System.Linq;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_ContractTypesController : BaseAuthforAdminController<ILang_ContractTypesManager, Lang_ContractTypes, Lang_ContractTypesViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_ContractTypesManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_ContractTypesController(IServiceProvider serviceprovider, ILang_ContractTypesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private IContractTypesManager ContractTypesmanager
        {
            get
            {
                return _serviceprovider.GetService<IContractTypesManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByContractTypesId/{ContractTypesId}")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguagesByContractTypesId([FromRoute]int ContractTypesId)
        {
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var ContractTypesRes = ContractTypesmanager.Get(ContractTypesId);
            var entityResult = manager.GetAllLanguagesByContractTypesId(ContractTypesId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name });
            }
            codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = ContractTypesRes.Data.Code, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);

            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var ContractTypesRes = ContractTypesmanager.GetAll();
            foreach (var ContractTypes in ContractTypesRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByContractTypesId(ContractTypes.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = ContractTypes.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = ContractTypes.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = ContractTypes.Code, languagesDictionaries = languagesDictionary, Id = ContractTypes.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguagesWithBlocked()
        {
            //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var ContractTypesRes = ContractTypesmanager.GetAllWithBlocked();
            var OrderedContractTypesRes = ContractTypesRes.Data.OrderByDescending(c => c.Id).ToList();
            foreach (var ContractTypes in OrderedContractTypesRes)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByContractTypesId(ContractTypes.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = ContractTypes.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //{
                //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //    {
                //        if (SupportedLanguage.Name == "English")
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = ContractTypes.Name });

                //        }
                //        else
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //        }
                //    }
                //}
                codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = ContractTypes.Code, languagesDictionaries = languagesDictionary, Id = ContractTypes.Id, IsDeleted = ContractTypes.IsDeleted });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_ContractTypes> lstModel)
        {
            var entityResult = manager.UpdateByContractTypes(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]CodeWithAllLanguagesViewModel Model)
        {
            string enName = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;
            List<Lang_ContractTypes> lstModel = new List<Lang_ContractTypes>(); 
            var checkNoDub  = ContractTypesmanager.CheckContractTypesNoDoublication(enName,Model.Code ,Model.Id);
            if (!checkNoDub.IsSucceeded)
                return ProcessResultViewModelHelper.Failed<bool>(false, checkNoDub.Status.Message, ProcessResultStatusCode.InvalidValue);
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_ContractTypes { FK_ContractTypes_ID = Model.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });

                }
            }
            var entityResult = manager.UpdateByContractTypes(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
