﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Ghanim.BLL.Caching.Abstracts;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class CompanyCodeController : BaseAuthforAdminController<ICompanyCodeManager, CompanyCode, CompanyCodeViewModel>
    {
        IServiceProvider _serviceprovider;
        ICompanyCodeManager manager;
        IProcessResultMapper processResultMapper;
        private readonly ICompanyCodeService _service;


        public CompanyCodeController(ICompanyCodeService service, IServiceProvider serviceprovider, ICompanyCodeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<CompanyCodeViewModel> cash) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper, cash)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            _service = service;
        }
        private ILang_CompanyCodeManager Lang_CompanyCodemanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_CompanyCodeManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetCompanyCodeByLanguage")]
        public ProcessResultViewModel<CompanyCodeViewModel> GetCompanyCodeByLanguage([FromQuery]int CompanyCodeId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_CompanyCodeRes = Lang_CompanyCodemanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = base.Get(CompanyCodeId);
            entityResult.Data.Name = Lang_CompanyCodeRes.Data.Name;
            return entityResult;
        }

        [HttpGet]
        [Route("GetByCode/{code}")]
        public async System.Threading.Tasks.Task<ProcessResultViewModel<CompanyCodeViewModel>> GetByCodeAsync([FromRoute] string code)
            => await _service.GetByCode(code);

        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<CompanyCodeViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "1";
            var actions = base.Get();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var CompanyCodeLocalizesRes = Lang_CompanyCodemanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_CompanyCode_ID == item.Id);
                    if (CompanyCodeLocalizesRes != null && CompanyCodeLocalizesRes.Data != null)
                    {
                        item.Name = CompanyCodeLocalizesRes.Data.Name;
                    }
                }
            }

            return actions;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<CompanyCodeViewModel> Post([FromBody] CompanyCodeViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<CompanyCodeViewModel>(null, SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<CompanyCodeViewModel>(null, string.Format(SharedResource.General_Duplicate, SharedResource.Entity_CompanyCode), ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] CompanyCodeViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
