﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnums;
using Ghanim.Models.Order.Enums;
using Ghanim.Models.Identity.Static;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class DriverController : BaseAuthforAdminController<IDriverManager, Driver, DriverViewModel>
    {
        public IDriverManager manger;
        public ITeamMemberManager teamMemberManager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        public DriverController(IDriverManager _manger, ITeamMemberManager _teamMemberManager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityService _identityUserService) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            teamMemberManager = _teamMemberManager;
        }
        /// <summary>
        /// This is API to add a record in TeamMember table after adding a record in Driver table
        /// </summary>
        /// <param name="model">Driver details</param>
        /// <returns>Driver details model after adding</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<DriverViewModel> Post([FromBody] DriverViewModel model)
        {
            //First call the base Post method from the inherited class
            ProcessResultViewModel<DriverViewModel> resultViewModel = base.Post(model);
            //Then add a new record to TeamMember table
            teamMemberManager.Add(new TeamMember()
            {
                MemberParentId = resultViewModel.Data.Id,
                MemberParentName = resultViewModel.Data.Name,
                MemberType = MemberType.Driver,
                UserId = resultViewModel.Data.UserId,
                UserTypeId=UserTypesEnum.Driver
            });
            return resultViewModel;
        }

        /// <summary>
        /// This API to add multi recorgd in TeamMember table after adding multi records in Driver table
        /// </summary>
        /// <param name="lstmodel">List of driver details needed to be added</param>
        /// <returns>List of drivers details after added</returns>
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<DriverViewModel>> PostMulti([FromBody] List<DriverViewModel> lstmodel)
        {
            //First call the base PostMulti method from the inherited class
            ProcessResultViewModel<List<DriverViewModel>> resultViewModel = base.PostMulti(lstmodel);
            //Then add new records to TeamMember table
            foreach (var item in resultViewModel.Data)
            {
                teamMemberManager.Add(new TeamMember()
                {
                    MemberParentId = item.Id,
                    MemberParentName = item.Name,
                    MemberType = MemberType.Driver,
                    UserId = item.UserId,
                    UserTypeId = UserTypesEnum.Driver

                });
            }
            return resultViewModel;
        }

        /// <summary>
        /// This API to Get driver with his identity data by driver id
        /// </summary>
        /// <param name="id">Driver ID</param>
        /// <returns>Drive data including identity data</returns>
        [HttpGet]
        [Route("GetDriverUser/{id}")]
        public async Task<ProcessResultViewModel<DriverUserViewModel>> GetDriverUser([FromRoute] int id)
        {
            try
            {
                //Getting driver by his id
                ProcessResult<Driver> driverTemp = manger.Get(id);
                if (driverTemp.IsSucceeded && driverTemp.Data != null)
                {
                    //Calling identity service to get driver's data then mapping them with driver data
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(driverTemp.Data.UserId);
                    DriverUserViewModel result = mapper.Map<Driver, DriverUserViewModel>(driverTemp.Data);
                    mapper.Map<ApplicationUserViewModel, DriverUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<DriverUserViewModel>(null, SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<DriverUserViewModel>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to get all drivers with their identity data
        /// </summary>
        /// <returns>List of drivers data including identity data</returns>
        [HttpGet]
        [Route("GetDriverUsers")]
        public async Task<ProcessResultViewModel<List<DriverUserViewModel>>> GetDriverUsers()
        {
            try
            {
                //Getting all drivers from Driver data
                ProcessResult<List<Driver>> driversTemp = manger.GetAll();
                if (driversTemp.IsSucceeded && driversTemp.Data.Count > 0)
                {
                    //Selecting drivers IDs then getting identity data by them
                    List<string> userIds = driversTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<DriverUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<DriverUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Driver, DriverUserViewModel>(driversTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<DriverUserViewModel>(), SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DriverUserViewModel>>(null, ex.Message);
            }
        }
        /// <summary>
        /// This API to get drivers data by division id
        /// </summary>
        /// <param name="divisionId">Division ID</param>
        /// <returns>List of drivers data including identity data related to the given division id</returns>
        [HttpGet]
        [Route("GetDriverUsersByDivisionId/{divisionId}")]
        public async Task<ProcessResultViewModel<List<DriverUserViewModel>>> GetDriverUsersByDivisionId([FromRoute] int divisionId)
        {
            try
            {
                //Getting drivers by division id from Drivers table
                ProcessResult<List<Driver>> engineersTemp = manger.GetAll(x => x.DivisionId == divisionId);
                if (engineersTemp.IsSucceeded && engineersTemp.Data.Count > 0)
                {
                    //Selecting drivers IDs then getting identity data by them
                    List<string> userIds = engineersTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<DriverUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<DriverUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Driver, DriverUserViewModel>(engineersTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<DriverUserViewModel>(), SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<DriverUserViewModel>>(null, ex.Message);
            }
        }
    }
}