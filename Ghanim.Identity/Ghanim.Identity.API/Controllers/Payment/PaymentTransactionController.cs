﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchProduct.RepositoryModule;
using Ghanim.API.Helper;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.Order;
using Ghanim.API.ServiceCommunications.Payment;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Ghanim.API.Utilities.HangFireServices;
using Ghanim.API.ViewModels.PaymentTransaction;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Ghanim.Models.Payment.Entities;
using Ghanim.Models.Payment.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;

namespace Ghanim.API.Controllers.Payment
{
    [Route("api/[controller]")]
    public class PaymentTransactionController
        : BaseController<IPaymentTransactionManager, PaymentTransaction, PaymentTransactionViewModel>

    {
        private ApplicationDbContext _context;
        private readonly IAPIHelper _helper;
        private readonly IOrderManager _orderManager;
        private readonly IPaymentTransactionService _orderService;

        public PaymentTransactionController(
            IOrderManager orderManager,
            IPaymentTransactionManager _manager,
            ApplicationDbContext context,
            IAPIHelper helper,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
                IPaymentTransactionService orderService
            ) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _context = context;
            _helper = helper;
            _orderManager = orderManager;
            _orderService = orderService;


            //List<PaymentTransaction> paymentTransactions = new  List<PaymentTransaction>();
            //paymentTransactions.Add(new PaymentTransaction());
            //paymentTransactions.Add(new PaymentTransaction());
            //paymentTransactions.Add(new PaymentTransaction());
            //paymentTransactions.Add(new PaymentTransaction());





            //List<PaymentTransactionViewModel> asdasasdasdd =
            //    _context.PaymentTransactions.ProjectTo<PaymentTransactionViewModel>().ToList();

            //foreach (var item in _context.PaymentTransactions)
            //{
            //    try
            //    {
            //        var asQ = new List<PaymentTransaction>() {item}.AsQueryable();
            //        List<PaymentTransactionViewModel> asdasasdasdd =
            //            _context.PaymentTransactions.ProjectTo<PaymentTransactionViewModel>().ToList();
            //    }
            //    catch (Exception e)
            //    {
            //        Console.WriteLine(e);
            //    }
            //}


            //try
            //{
            //    List<PaymentTransactionViewModel> asdasasdasdd =
            //       _context.PaymentTransactions.ProjectTo<PaymentTransactionViewModel>().ToList();
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //}
        }

        [Route("GetAllFilterdPaginated")]
        [HttpPost]
        public virtual async Task<ProcessResultViewModel<PaginationContainer<PaymentTransactionViewModel>>> GetAllFilterdPaginatedAsync(
            [FromBody] PaymentTransactionFilters model = null)
            => await _orderService.GetAllPaginatedAsync(model);

        [Route("ExportAllFilterd")]
        [HttpPost]
        public virtual async Task<ProcessResultViewModel<string>> ExportAllFilterdAsync(
            [FromBody] PaymentTransactionFilters model = null)
            => await _orderService.ExportAllAsync(model);

        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<PaymentTransactionViewModel> Post([FromBody]PaymentTransactionViewModel model)
        {
            var order = _orderManager.Get(model.OrderId);
            model.OrderHistoryStatusId = order.Data.StatusId;

            model.FK_CreatedBy_Id = _helper.CurrentUserId();
            model.CreatedDate = DateTime.UtcNow;
            model.CurrentDispatcherId = order?.Data?.DispatcherId;
            model.CurrentForemanId = order.Data.Team.Foreman.Id;
            model.CurrentSupervisorId = order.Data.SupervisorId;
            if (!string.IsNullOrEmpty(order?.Data?.LastActorTechUserId))
            {
                model.CurrentTechnicianId = _context.Technicians.FirstOrDefault(x => x.UserId == order.Data.LastActorTechUserId)?.Id;
            }

       
            return base.Post(model);
        }

        [HttpPost]
        [Route("AddMulti")]
        [Authorize(Roles = StaticRoles.Closed)]
        public override ProcessResultViewModel<List<PaymentTransactionViewModel>> PostMulti([FromBody]List<PaymentTransactionViewModel> lstmodel)
       => base.PostMulti(lstmodel);

        [HttpPut]
        [Route("Update")]
        [Authorize(Roles = StaticRoles.Closed)]
        public override ProcessResultViewModel<bool> Put([FromBody]PaymentTransactionViewModel model)
            => base.Put(model);


        [HttpDelete]
        [Route("LogicalDelete/{id}")]
        [Authorize(Roles = StaticRoles.Closed)]
        public override ProcessResultViewModel<bool> LogicalDelete([FromRoute] int id)
            => base.LogicalDelete(id);



        [HttpDelete]
        [Route("Delete/{id}")]
        [Authorize(Roles = StaticRoles.Closed)]
        public override ProcessResultViewModel<bool> Delete([FromRoute]int id)
            => base.Delete(id);


        [HttpGet]
        [Route("AdddummyTestData")]
        public void AdddummyTestData()
        {
            var s = new ServiceType()
            {
                Name = "dummy for test",
            };
            _context.ServiceTypes.Add(s);
            _context.SaveChanges();

            var order = _context.Orders.FirstOrDefault(x => x.DispatcherId != null);

            for (int i = 0; i < 20; i++)
            {
                Post(new PaymentTransactionViewModel()
                {
                    PaymentMode = PaymentModeEnum.Cash,
                    PaymentStatus = PaymentStatusEnum.Completed,
                    PaymentMethod = "PaymentMethod asdasdasdasdasd",
                    Payment = i * (decimal)2.5,
                    Receipt = "asdasdasdasd recipt",
                    ManualReceiptNumber = "num recipt",
                    OfferDiscount = 20,
                    OrderId = order.Id,
                    CurrentDispatcherId = order.DispatcherId,
                    ServiceTypeId = s.Id
                });
            }
        }
    }
}
