﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.API.ServiceCommunications.Order;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.Models.Context;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.Models.Identity.Static;
using Ghanim.Models.Order.Enums;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;
using Ghanim.BLL.UserManagement.NotMappedModels;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TeamController : BaseAuthforAdminController<ITeamManager, Team, TeamViewModel>
    {
        public ITeamManager manger;
        private readonly ApplicationDbContext _context;
        public readonly ITeamMemberManager teamMemberManger;
        public IDispatcherManager dispatcherManger;
        public readonly new IMapper mapper;
        IServiceProvider serviceProvider;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        private readonly ITeamService _teamService;
        private readonly ITeamMembersService _teamMembersService;


        IOrderService _orderService;
        public TeamController(ApplicationDbContext context, ITeamService teamService, IServiceProvider _serviceProvider, IOrderService orderService, ITeamManager _manger, ITeamMemberManager _teamMemberManger, ITeamMembersService teamMembersService, IDispatcherManager _dispatcherManager,
            IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityService _identityUserService) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _context = context;
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            teamMemberManger = _teamMemberManger;
            dispatcherManger = _dispatcherManager;
            serviceProvider = _serviceProvider;
            _orderService = orderService;
            _teamService = teamService;
            _teamMembersService = teamMembersService;
        }
        private IForemanManager formanManager
        {
            get
            {
                return serviceProvider.GetService<IForemanManager>();
            }
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPost, Route("ResetPassword")]
        public async Task<ProcessResultViewModel<bool>> ResetPassword([FromBody]TeamResetPasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                if (model != null && !string.IsNullOrEmpty(model.NewPassword) && model.TeamId > 0)
                {
                    var members = teamMemberManger.GetAll(x => x.TeamId == model.TeamId).Data.Select(x => x.MemberParentName).ToList();
                    ResetUsersPasswordViewModel resetPasswordModel = new ResetUsersPasswordViewModel()
                    {
                        NewPassword = model.NewPassword,
                        Usernames = members
                    };
                    result = await identityUserService.ResetPassword(resetPasswordModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false,SharedResource.TeamRestPassInvalid );
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<TeamViewModel> Post([FromBody] TeamViewModel model)
        {
            //ProcessResultViewModel res = new ProcessResultViewModel();
            bool isThereAnyOtherTeamsWithMyname = _context.Teams.Any(x => !x.IsDeleted && x.Name.ToLower() == model.Name.ToLower());
            if (isThereAnyOtherTeamsWithMyname)
                return ProcessResultViewModelHelper.Failed<TeamViewModel>(null, SharedResource.TeamNameExisted, ProcessResultStatusCode.Dublicated);

            try
            {
                var formanRes = formanManager.Get(model.ForemanId);
                if (formanRes != null && formanRes.Data != null)
                {
                    model.DispatcherId = formanRes.Data.DispatcherId;
                    //model.DispatcherName = formanRes.Data.DispatcherName;
                    model.EngineerId = formanRes.Data.EngineerId;
                    //model.EngineerName = formanRes.Data.EngineerName;
                    model.SupervisorId = formanRes.Data.SupervisorId;
                    //model.SupervisorName = formanRes.Data.SupervisorName;
                    return base.Post(model);
                    // return ProcessResultViewModelHelper.Failed<TeamViewModel>(null, "there isn't forman with Id:" + model.ForemanId);

                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<TeamViewModel>(null, SharedResource.NoFormanWithId + model.ForemanId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TeamViewModel>(null, ex.Message);
            }
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] TeamViewModel model)
        {
            var OldTeamObj = _context.Teams.FirstOrDefault(x => x.Id == model.Id);
            var TeamOngoingOrdersCount = _context.Orders.Count(x => x.TeamId == model.Id
                       && x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);
            //_context.Orders.Count(x => x.TeamId == model.Id
            //&& (x.StatusId == (int)OrderStatusEnum.On_Travel
            //|| x.StatusId == (int)OrderStatusEnum.Reached
            //|| x.StatusId == (int)OrderStatusEnum.Started));
            bool isThereAnyOtherTeamsWithMyname = _context.Teams.Any(x => x.Id != model.Id && x.Name.ToLower() == model.Name.ToLower());
            if (isThereAnyOtherTeamsWithMyname)
                return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.TeamNameExisted, ProcessResultStatusCode.Dublicated);

            try
            {
                var formanRes = formanManager.Get(model.ForemanId);
                if (formanRes != null && formanRes.Data != null)
                {
                    model.DispatcherId = formanRes.Data.DispatcherId;
                    model.EngineerId = formanRes.Data.EngineerId;
                    model.SupervisorId = formanRes.Data.SupervisorId;
                    if (OldTeamObj.ForemanId != model.ForemanId)
                    {
                        if (TeamOngoingOrdersCount > 0)
                        {
                            return ProcessResultViewModelHelper.Failed<bool>(false, "you cannot change the foreman of this team because there is Ongoing orders in this team:");

                        }
                    }
                    return base.Put(model);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoFormanWithId + model.ForemanId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetByDispatcherId/{dispatcherId}")]
        public async Task<ProcessResultViewModel<List<TeamViewModel>>> GetByDispatcherId([FromRoute]int? dispatcherId) =>
          await _teamService.GetTeamsByDispatcherId((int)dispatcherId);

        [HttpGet]
        [Route("GetByFormanId")]
        public ProcessResultViewModel<List<TeamViewModel>> GetByFormanId([FromQuery]List<int?> formansId)
        {
            var entityResult = manger.GetByFormansId(formansId);
            var result = processResultMapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
            return result;
        }
        [HttpGet]
        [Route("GetByFormansIdWithBlocked")]
        public ProcessResultViewModel<List<TeamViewModelMini>> GetByFormansIdWithBlocked([FromQuery]List<int?> formansId)
        {
            var entityResult = manger.GetByFormansIdWithBlocked(formansId);
            var result = processResultMapper.Map<List<Team>, List<TeamViewModelMini>>(entityResult);
            return result;
        }
        [HttpPut]
        [Route("ReassignTeamToDispatcher")]
        public ProcessResultViewModel<bool> ReassignTeamToDispatcher([FromQuery] int? teamId, int? dispatcherId)
        {
            ProcessResultViewModel<bool> result = null; ;

            var dispatcherRes = dispatcherManger.Get(dispatcherId);
            var teamRes = manger.Get(teamId);
            if (teamRes != null && teamRes.Data != null && dispatcherRes != null && dispatcherRes.Data != null)
            {
                teamRes.Data.DispatcherId = dispatcherRes.Data.Id;
                //teamRes.Data.DispatcherName = $"{dispatcherRes.Data.User.FirstName} {dispatcherRes.Data.User.LastName}";
                teamRes.Data.SupervisorId = dispatcherRes.Data.SupervisorId;
                //todo mapping infects erorrs
                //teamRes.Data.SupervisorName = dispatcherRes.Data.SupervisorName;
                var entityResult = manger.Update(teamRes.Data);
                result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            }

            return result;
        }

        [HttpGet]
        [Route("GetAllWithDispatcher")]
        public ProcessResultViewModel<List<DispatcherWithTeamsViewModel>> GetAllWithDispatcher()
        {
            List<DispatcherWithTeamsViewModel> result = null;
            var dispatchers = dispatcherManger.GetAllQuerable();
            if (dispatchers.IsSucceeded && dispatchers.Data.Count() > 0)
            {
                List<int> dispatcherIds = dispatchers.Data.Select(x => x.Id).ToList();
                result = manger.GetByDispatchersId(dispatcherIds).Data;
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        foreach (var team in item.Teams)
                        {
                            var teamMemberRes = teamMemberManger.GetByTeamId(team.Id);
                            if (teamMemberRes != null && teamMemberRes.Data.Count() > 0)
                            {
                                team.Members = teamMemberRes.Data;
                            }
                        }
                    }
                }
                foreach (var item in dispatchers.Data)
                {
                    var isFound = result.Find(x => x.DispatcherId == item.Id);
                    if (isFound == null)
                    {
                        result.Add(new DispatcherWithTeamsViewModel()
                        {
                            DispatcherId = item.Id,
                            DispatcherName = $"{item.User.FirstName} {item.User.LastName}",
                            Teams = new List<Team>()
                        });
                    }
                }
                return ProcessResultViewModelHelper.Succedded(result);
            }
            return ProcessResultViewModelHelper.Failed<List<DispatcherWithTeamsViewModel>>(null,SharedResource.General_WrongWhileGet);
        }

        [HttpGet]
        [Route("SearchByTeamName")]
        public ProcessResultViewModel<List<TeamViewModel>> SearchByTeamName([FromQuery]string word)
        {
            var entityResult = manger.SearchByTeamName(word);
            var result = processResultMapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
            return result;
        }
        [HttpDelete]
        [Route("Delete")]
        [Authorize(Roles = StaticRoles.Admin)]
        public override ProcessResultViewModel<bool> Delete([FromQuery] int teamId)
        {
            bool data = false;
            try
            {
                var entityResult = manger.GetAll(x => x.Id == teamId);
                var res = processResultMapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
                var TeamOngoingOrdersCount = _context.Orders.Count(x => x.TeamId == teamId
                    && (x.StatusId == (int)OrderStatusEnum.On_Travel
                    || x.StatusId == (int)OrderStatusEnum.Reached
                    || x.StatusId == (int)OrderStatusEnum.Started || x.StatusId == (int)OrderStatusEnum.Dispatched));
                if (TeamOngoingOrdersCount > 0)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, "This team has ongoing orders, you can’t delete the team");
                }
                else
                {
                    var ActionDate = DateTime.Now;
                    var MembersPerThisTeam = _context.Members.Where(x => x.TeamId == teamId).Select(x => x.Id).ToList();
                    var UnassignedMembersRes = _teamMembersService.UnassignedMembers(MembersPerThisTeam);

                    //Stop Calculating Action Time In Order Action when Blocking the Team
                    var lastActions = _context.OrderActions.Where(x => x.TeamId == teamId && x.ActionTime == null).ToList();
                    if (lastActions.Count > 0)
                    {
                        foreach (OrderAction lastOrderAction in lastActions)
                        {
                            lastOrderAction.ActionTime = ActionDate - lastOrderAction.ActionDate;
                        }
                        //update last Order Actions
                        _context.OrderActions.UpdateRange(lastActions);
                    }                   
                    _context.SaveChanges();
                    //
                    base.LogicalDelete(teamId);
                }

            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed(data, ex.Message);
            }
            return ProcessResultViewModelHelper.Succedded(true);
        }
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<TeamViewModel>> Get()
        {
            var entityResult = manger.GetAll().Data.ToList();
            var teamViewModel = mapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
            // List<> tt = mapper.Map<List<Team>, List<TeamViewModel>>(entityResult);
            if (entityResult != null && entityResult.Count > 0)
            {
                foreach (var item in teamViewModel)
                {
                    item.ordersCount = _context.Orders.Count(x => x.TeamId == item.Id
                         && x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);
                    //  item.ordersCount = _context.Orders.Count(x => x.TeamId == item.Id
                    //&& (x.StatusId == (int)OrderStatusEnum.On_Travel
                    //|| x.StatusId == (int)OrderStatusEnum.Reached
                    //|| x.StatusId == (int)OrderStatusEnum.Started));
                    item.hasOrders = item?.ordersCount > 0;
                }
            }
            return ProcessResultViewModelHelper.Succedded<List<TeamViewModel>>(teamViewModel);
            // return processResultMapper.Map<List<TeamViewModel>, List<Team>>(tt);
        }
        [Route("GetAllTeams")]
        [HttpGet]
        public async Task<ProcessResultViewModel<List<TeamWithoutMembersViewModel>>> GetAllTeams()
        {
            var result = await manger.GetTeamsWithOutMembers();
            return ProcessResultViewModelHelper.Succedded(result);
        }

        //
        //[HttpDelete, Route("Delete")]
        //public async Task<ProcessResultViewModel<bool>> Delete([FromQuery] int id)
        //{
        //    ProcessResultViewModel<bool> result = null;
        //    try
        //    {
        //        var isDeleted = await manger.DeleteAsync(id);
        //        if (isDeleted)
        //        {

        //            result = ProcessResultViewModelHelper.Succedded(isDeleted);
        //        }
        //        else
        //        {
        //            result = ProcessResultViewModelHelper.Failed(false, "Failed to delete user");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result = ProcessResultViewModelHelper.Failed(false, ex.Message);
        //    }
        //    return result;
        //}


    }
}