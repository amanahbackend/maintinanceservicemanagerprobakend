﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Identity.Static;
using Ghanim.API.Caching.Abstracts;
using Ghanim.API.Caching.BL;
using Ghanim.BLL.Caching.Abstracts;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AreasController : BaseAuthforAdminController<IAreasManager, Areas, AreasViewModel>
    {
        IServiceProvider _serviceprovider;
        IAreasManager manager;
        IProcessResultMapper processResultMapper;
        

        public ISupportedLanguagesManager supportedLanguagesManager;

        public AreasController(IServiceProvider serviceprovider, IAreasManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<AreasViewModel> areaBLCashed) 
            : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper, areaBLCashed)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
           
        }
        private ILang_AreasManager Lang_Areasmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_AreasManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        //[HttpPost]
        //[Route("Add")]
        //public override ProcessResultViewModel<AreasViewModel> Post([FromBody] AreasViewModel entityLst)
        //{
        //    var entityModel = mapper.Map<AreasViewModel, Areas>(entityLst);
        //    var entityResult = manger.Add(entityModel);
        //    return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
        //}
        //////// ///////////////Language

      

        [HttpGet]
        [Route("GetAreaByLanguage")]
        public ProcessResultViewModel<AreasViewModel> GetAreaByLanguage([FromQuery]int AreaId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_AreasRes = Lang_Areasmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(AreaId);
            entityResult.Data.Name = Lang_AreasRes.Data.Name;
            var result = processResultMapper.Map<Areas, AreasViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public ProcessResultViewModel<AreasViewModel> GetByName([FromRoute]string name)
        {
            var entityResult = manager.Get(x => x.Name == name);
            var result = processResultMapper.Map<Areas, AreasViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetAreasByGovId")]
        public ProcessResultViewModel<List<AreasViewModel>> GetAreasByGovId([FromQuery]int GovId)
        {
            var entityResult = manager.GetAreasByGovId(GovId);
            var result = processResultMapper.Map<List<Areas>, List<AreasViewModel>>(entityResult);
            return result;
        }
        //[HttpGet]
        //[Route("GetAreasByGovId")]
        //public ProcessResultViewModel<List<Areas>> GetAreasByGovId([FromQuery]int GovId)
        //{
        //    var entityResult = manager.GetAreasByArea_No(GovId);
        //    var result = processResultMapper.Map<List<Areas>, List<AreasViewModel>>(entityResult);
        //    return result;
        //}

        //[HttpGet]
        //[Route("GetAreaWithAllLanguages/{AreaId}")]
        //public ProcessResultViewModel<List<AreasViewModel>> GetAreaWithAllLanguages([FromRoute]int AreaId)
        //{
        //    ProcessResult<List<Areas>> entityResult = null;
        //    var Lang_AreasRes = Lang_Areasmanager.GetAll().Data.FindAll(x=>x.FK_Area_ID==AreaId);
        //    foreach (var item in Lang_AreasRes)
        //    {
        //        var AreasRes = manager.Get(x => x.Id == item.FK_Area_ID);
        //        AreasRes.Data.Name = item.Name;
        //        entityResult.Data.Add(AreasRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<Areas>, List<AreasViewModel>>(entityResult);
        //    return result;
        //}



        //[HttpPost]
        //[Route("Add")]
        //public override ProcessResultViewModel<AreasViewModel> Post([FromBody] AreasViewModel model)
        //{
        //    if (model == null)
        //        return ProcessResultViewModelHelper.Failed<AreasViewModel>(null, "Area Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

        //    var entity = manager.Get(
        //        x => x.Name.ToLower() == model.Name.ToLower() ||
        //         x.Area_No == model.Area_No
        //        )?.Data;
        //    if (entity != null)
        //        return ProcessResultViewModelHelper.Failed<AreasViewModel>(null, "Area Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
        //    return base.Post(model);
        //}
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] AreasViewModel model)
        {
            var entity = manager.Get(
              x =>
              x.Id != model.Id && (
              x.Name.ToLower() == model.Name.ToLower() ||
               x.Area_No == model.Area_No)
              )?.Data;
            if (entity == null)
                return base.Put(model);
            else
                return ProcessResultViewModelHelper.Failed<bool>(false, "Area already existed with that name or code", ProcessResultStatusCode.InvalidValue);
        }

        [Authorize(Roles = "Foreman")]
        [Route("TestForman")]
        [HttpGet]
        public ProcessResultViewModel<string> TestForman()
        {

            ProcessResultViewModel<string> result = new ProcessResultViewModel<string>() { Data = "Forman Role" };
            return result;
        }

        [Authorize(Roles = "Admin")]
        [Route("TestAdmin")]
        [HttpGet]
        public ProcessResultViewModel<string> TestAdmin()
        {

            ProcessResultViewModel<string> result = new ProcessResultViewModel<string>() { Data = "Admin Role" };
            return result;
        }


        [Authorize(Roles = "Engineer")]
        [Route("TestEngineer")]
        [HttpGet]
        public ProcessResultViewModel<string> TestEngineer()
        {

            ProcessResultViewModel<string> result = new ProcessResultViewModel<string>() { Data = "Engineer Role" };
            return result;
        }

        [Authorize(Roles = StaticRoles.Supervisor)]
        [Route("TestSupervisor")]
        [HttpGet]
        public ProcessResultViewModel<string> TestSupervisor()
        {

            ProcessResultViewModel<string> result = new ProcessResultViewModel<string>() { Data = "Supervisor Role" };
            return result;
        }

        [Authorize(Roles = "Technician")]
        [Route("TestTechnician")]
        [HttpGet]
        public ProcessResultViewModel<string> TestTechnician()
        {

            ProcessResultViewModel<string> result = new ProcessResultViewModel<string>() { Data = "Technician Role" };
            return result;
        }


        //[Authorize(Roles = "Dispatcher")]
        [Authorize]
        [Route("testauth")]
        [HttpGet]
        public ProcessResultViewModel<string> testauth()
        {

            ProcessResultViewModel<string> result = new ProcessResultViewModel<string>() { Data = "test auth" };
            return result;
        }

        [Route("test")]
        [HttpGet]
        public ProcessResultViewModel<string> test()
        {

            ProcessResultViewModel<string> result = new ProcessResultViewModel<string>() { Data = "test" };
            return result;
        }
        [Route("testChashed")]
        [HttpGet]
        public List<AreasViewModel> testChashed()
        {
            return _cashed.CachedGetAll();
        }

        

    }
}
