﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        [Route("GetDateNowFromServer")]
        [HttpGet]
        public virtual IActionResult GetDateNowFromServer()
        {
            return Ok(new
            {
                DateValue = DateTime.UtcNow
            });
        }
    }
}