﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Utilites.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.Models.Context;
using Ghanim.Models.Identity.Static;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ManagerController : BaseAuthforAdminController<IManagerManager,Manager,ManagerViewModel>
    {
        public IManagerManager manager;
        public readonly new IMapper mapper;
        private readonly IManagerService _managerService;
        IProcessResultMapper ProcessResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        public readonly ApplicationDbContext _context;

        public ManagerController(ApplicationDbContext context,IManagerService managerService, IManagerManager _manager,IMapper _mapper,IProcessResultMapper _processResultMapper,IProcessResultPaginatedMapper _processResultPaginatedMapper,IIdentityService _identityUserService):base(_manager,_mapper,_processResultMapper,_processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            ProcessResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            _managerService= managerService;
            _context = context;
        }

        /// <summary>
        /// This API to get a specific manager data using his identity userId
        /// </summary>
        /// <param name="userId">Identiy User ID for manager</param>
        /// <returns>A model of manager data</returns>
        [HttpGet]
        [Route("GetManagerByUserId/{userId}")]
        public ProcessResultViewModel<ManagerViewModel> GetManagerByUserId([FromRoute] string userId) =>
            _managerService.GetManagerByUserId(userId);

        /// <summary>
        /// This API to get manager data by his id including identity data
        /// </summary>
        /// <param name="id">Manager ID</param>
        /// <returns>A model with manager data</returns>
        [HttpGet]
        [Route("GetManagerUser/{id}")]
        public async Task<ProcessResultViewModel<ManagerUserViewModel>> GetManagerUser([FromRoute] int id)
        {
            try
            {
                //Getting manager data from Manager table by id
                ProcessResult<Manager> managerTemp = manger.Get(id);
                if (managerTemp.IsSucceeded && managerTemp.Data != null)
                {
                    //Calling identity service to get manager full user data
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(managerTemp.Data.UserId);
                    ManagerUserViewModel result = mapper.Map<Manager, ManagerUserViewModel>(managerTemp.Data);
                    mapper.Map<ApplicationUserViewModel, ManagerUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<ManagerUserViewModel>(null, SharedResource.ManagerInvalidId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ManagerUserViewModel>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to block a manager user
        /// </summary>
        /// <param name="id">Manager ID</param>
        /// <returns>Bool result, true if block success or false if block failed</returns>
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null,SharedResource.UserNotFound );
            }

            //Blocking user by making him InActive = true in AspNetUsers table
            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                //Logically deleting user from Manager table by making IsDeleted = true
                var result = manger.LogicalDeleteById(id);
                return result;
            }

            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen + SharedResource.BlockUser);
        }

        /// <summary>
        /// This API just to disable the default GetAll API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Deprecated")]
        public override ProcessResultViewModel<List<ManagerViewModel>> Get()
        {
            return base.Get();
        }

        /// <summary>
        /// This is the new GetAll method which is async, This API gets all Manager users data and their FullName from identity service
        /// </summary>
        /// <returns>List of manager users data</returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<ProcessResultViewModel<List<ManagerViewModel>>> GetAsync()
        {
            // implement the base Get() method
            var users = base.Get();
            // Use userIds from Manager table to get the identity data from identity service
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }

        /// <summary>
        /// This API just to disable the default GetWithBlocked API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("WithBlockedDeprecated")]
        public override ProcessResultViewModel<List<ManagerViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        /// <summary>
        /// This is the new GetAllWithBlocked API which is async, This API gets all Manager users data
        /// with their FullName from identity service INCLUDING THE DELETED USERS (IsDeleted = true)
        /// </summary>
        /// <returns>List of Manager users</returns>
        [HttpGet]
        [Route("GetAllWithBlocked")]
        public async Task<ProcessResultViewModel<List<ManagerViewModel>>> GetAllWithBlockedAsync()
        {
            // implement the base GetAllWithBlocked() method
            var users = base.GetAllWithBlocked();
            // Use userIds from Manager table to get the identity data from identity service
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] ManagerViewModel model)
        {
            try
            {
                var oldModel = _context.Managers.FirstOrDefault(x => x.Id ==model.Id);
                if (model.PF != oldModel.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false,SharedResource.NoChangePF );
                }
                var SupervisorCount = oldModel.Supervisors.Count();
                if (SupervisorCount > 0)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeManagerSupUser, ProcessResultStatusCode.HasChilds);
                }

                return base.Put(model);
            }

            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }
    }
}