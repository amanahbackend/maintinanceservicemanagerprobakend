﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Ghanim.Models.Identity.Static;
using Ghanim.BLL.Caching.Abstracts;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_AreasController : BaseAuthforAdminController<ILang_AreasManager, Lang_Areas, Lang_AreasViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_AreasManager manager;
        IProcessResultMapper processResultMapper;
        private ICached<AreasViewModel> _areaBLCashed;

        public Lang_AreasController(ICached<AreasViewModel> areaBLCashed,IServiceProvider serviceprovider, ILang_AreasManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            _areaBLCashed = areaBLCashed;
        }
        private IAreasManager Areamanager
        {
            get
            {
                return _serviceprovider.GetService<IAreasManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        /*FIXME(dws) needs to allow for multiple widgets */

        [HttpGet]
        [Route("GetAllLanguagesByAreaId/{AreaId}")]
        public ProcessResultViewModel<List<AreaWithAllLanguagesViewModel>> GetAllLanguagesByAreaId([FromRoute]int AreaId)
        {
            List<AreaWithAllLanguagesViewModel> areaWithAllLanguages = new List<AreaWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var AreaRes = Areamanager.Get(AreaId);
            var entityResult = manager.GetAllLanguagesByAreaId(AreaId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguagesRes.Data.Code, Value = item.Name, Id = item.Id });
            }

            areaWithAllLanguages.Add(new AreaWithAllLanguagesViewModel { Area_No = AreaRes.Data.Area_No, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<AreaWithAllLanguagesViewModel>>(areaWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<AreaWithAllLanguagesViewModel>> GetAllLanguages()
        {
            List<AreaWithAllLanguagesViewModel> areaWithAllLanguages = new List<AreaWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;

            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            var AreaRes = Areamanager.GetAll();
            foreach (var Area in AreaRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByAreaId(Area.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = Area.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }

                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = Area.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }

                    }
                }
                areaWithAllLanguages.Add(new AreaWithAllLanguagesViewModel { Area_No = Area.Area_No, languagesDictionaries = languagesDictionary, Id = Area.Id });
            }

            var result = ProcessResultViewModelHelper.Succedded<List<AreaWithAllLanguagesViewModel>>(areaWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public ProcessResultViewModel<List<AreaWithAllLanguagesViewModel>> GetAllLanguagesWithBlocked()
        {
            //string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            List<AreaWithAllLanguagesViewModel> areaWithAllLanguages = new List<AreaWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
            var AreaRes = Areamanager.GetAllWithBlocked();
            var OrderedAreaRes = AreaRes.Data.OrderByDescending(o => o.Id).ToList();
            foreach (var Area in OrderedAreaRes)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByAreaIdWithBlocked(Area.Id);
                foreach (var item in entityResult.Data)
                {
                    //var NewAreasRes = manager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_Area_ID == item.Id);
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes != null && (SupportedLanguageRes.Data?.Name ?? "English") == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = (SupportedLanguageRes.Data?.Name ?? "English"), Value = Area.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }

                }

                //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //{
                //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //    {
                //        if (SupportedLanguage.Name == "English")
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = Area.Name });

                //        }
                //        else
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //        }

                //    }
                //}
                areaWithAllLanguages.Add(new AreaWithAllLanguagesViewModel { Area_No = Area.Area_No, languagesDictionaries = languagesDictionary, Id = Area.Id, IsDeleted = Area.IsDeleted });
            }

            var result = ProcessResultViewModelHelper.Succedded<List<AreaWithAllLanguagesViewModel>>(areaWithAllLanguages);

            return result;
        }

        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_Areas> lstModel)
        {
            var entityResult = manager.UpdateByArea(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]AreaWithAllLanguagesViewModel Model)
        {
            string areaNewEnNAme = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;
            List<Lang_Areas> lstModel = new List<Lang_Areas>(); 
            var checkNoDubArea = Areamanager.CheckAreaNoDoublication(areaNewEnNAme,Model.Id);
            if (!checkNoDubArea.IsSucceeded )
               return ProcessResultViewModelHelper.Failed<bool>(false, checkNoDubArea.Status.Message, ProcessResultStatusCode.InvalidValue);  
            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data!=null)
                {
                    lstModel.Add(new Lang_Areas { FK_Area_ID = Model.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = manager.UpdateByArea(lstModel);
            _areaBLCashed.ClearCashed();
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
