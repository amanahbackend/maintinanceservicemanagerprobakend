﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ghanim.API.ServiceCommunications.DataManagementService.GovernoratesService;
using Ghanim.Models.Identity.Static;
using Ghanim.ResourceLibrary.Resources;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class GovernoratesController : BaseAuthforAdminController<IGovernoratesManager, Governorates, GovernoratesViewModel>
    {
        IServiceProvider serviceprovider;
        IGovernoratesManager manager;
        IProcessResultMapper processResultMapper;
        IGovernoratesService _governoratesService;
        public GovernoratesController(IGovernoratesService governoratesService,IServiceProvider _serviceprovider, IGovernoratesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            serviceprovider = _serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            _governoratesService = governoratesService; 
        }
        private ILang_GovernoratesManager Lang_Governoratesmanager
        {
            get
            {
                return serviceprovider.GetService<ILang_GovernoratesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetGovernorateByLanguage")]
        public ProcessResultViewModel<GovernoratesViewModel> GetGovernorateByLanguage([FromQuery]int GovernorateId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_GovernoratesRes = Lang_Governoratesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            var entityResult = manager.Get(GovernorateId);
            entityResult.Data.Name = Lang_GovernoratesRes.Data.Name;
            var result = processResultMapper.Map<Governorates, GovernoratesViewModel>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public async Task<ProcessResultViewModel<GovernoratesViewModel>> GetByName([FromRoute] string name)
            => await _governoratesService.GetByName(name);

        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<GovernoratesViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "1";
            var actions = manager.GetAll();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var GovernoratesLocalizesRes = Lang_Governoratesmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_Governorates_ID == item.Id);
                    if (GovernoratesLocalizesRes != null && GovernoratesLocalizesRes.Data != null)
                    {
                        item.Name = GovernoratesLocalizesRes.Data.Name;
                    }
                }
            }
            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<Governorates>, List<GovernoratesViewModel>>(actions.Data);
                //return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
                //return ProcessResultHelper.Succedded<List<AreasViewModel>>(result);
            }
            return processResultMapper.Map<List<Governorates>, List<GovernoratesViewModel>>(actions);
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<GovernoratesViewModel> Post([FromBody] GovernoratesViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<GovernoratesViewModel>(null, SharedResource.General_NoVal_Body, ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<GovernoratesViewModel>(null, string.Format(SharedResource.General_Duplicate, SharedResource.Entity_Governorates), ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] GovernoratesViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
