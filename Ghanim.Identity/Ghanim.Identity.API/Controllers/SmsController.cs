﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.Models.Identity.Static;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize(Roles = StaticRoles.Closed)]
    [Route("api/[controller]")]
    public class SmsController : Controller
    {
        ISMSService _smsSevrvice;
        public SmsController(ISMSService smsService)
        {
            _smsSevrvice = smsService;
        }

        [HttpPost, Route("SendTestAccount")]
        public ProcessResultViewModel<bool> SendTestAccount([FromBody] SendSmsViewModel model) =>
            _smsSevrvice.Send(model);

    }
}
