﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchProduct.Controllers;

using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.Models.Context;
using Google;
using Microsoft.EntityFrameworkCore;
using Ghanim.API.ViewModels;
using Ghanim.Models.Identity.Static;
using Ghanim.Models.Order.Enums;
using Ghanim.ResourceLibrary.Resources;

using Microsoft.AspNetCore.Authorization;
namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ForemanController : BaseAuthforAdminController<IForemanManager, Foreman, ForemanViewModel>
    {
        public IForemanManager manger;
        public ITeamManager teamManager;
        public readonly new IMapper mapper;
        public readonly ApplicationDbContext _context;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        private readonly IApplicationUserManager _usermanager;
        private readonly IForemanService foremanService;
        public ForemanController(IApplicationUserManager usermanager, ApplicationDbContext context, IForemanService _foremanService, IForemanManager _manger, ITeamManager _teamManager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityService _identityUserService) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _usermanager = usermanager;
            _context = context;
            manger = _manger;
            mapper = _mapper;
            teamManager = _teamManager;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            foremanService = _foremanService;
        }

        /// <summary>
        /// This API to get foreman data by his id including identity data
        /// </summary>
        /// <param name="id">Foreman ID</param>
        /// <returns>A model with foreman data</returns>
        [HttpGet]
        [Route("GetForemanUser/{id}")]
        public async Task<ProcessResultViewModel<ForemanUserViewModel>> GetForemanUser([FromRoute] int id)
        {
            try
            {
                //Getting foreman data from Foreman table by id
                ProcessResult<Foreman> foremanTemp = manger.Get(id);
                if (foremanTemp.IsSucceeded && foremanTemp.Data != null)
                {
                    // get teams under formen
                    var formenTeams = teamManager.GetByFormansId(new List<int?>() { foremanTemp.Data.Id });

                    //Calling identity service to get foreman full user data
                    ApplicationUserViewModel userVm = mapper.Map<ApplicationUser, ApplicationUserViewModel>(foremanTemp.Data.User);
                    ForemanUserViewModel result = mapper.Map<Foreman, ForemanUserViewModel>(foremanTemp.Data);
                    mapper.Map<ApplicationUserViewModel, ForemanUserViewModel>(userVm, result);
                    int OrdersCount = 0;
                    foreach (var item in formenTeams.Data)
                    {
                        var ordersCount = _context.Orders.Count(x => x.TeamId == item.Id
                        && x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);
                        result.ordersCount = ordersCount;
                            //_context.Orders.Count(x => x.TeamId == item.Id
                                  //           && (x.StatusId == (int)OrderStatusEnum.On_Travel
                                   //          || x.StatusId == (int)OrderStatusEnum.Reached
                                   //          || x.StatusId == (int)OrderStatusEnum.Started));
                        OrdersCount = OrdersCount +(int)result.ordersCount;                      

                    }
                    result.ordersCount = OrdersCount;
                    result.hasOrders = result?.ordersCount > 0;
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<ForemanUserViewModel>(null, SharedResource.NoFormanWithId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<ForemanUserViewModel>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to get foremans by dispatcher id
        /// </summary>
        /// <param name="dispatcherId">Dispatcher ID</param>
        /// <returns>List of foremans data for a specific dispatcher id</returns>
        [HttpGet]
        [Route("GetForemansByDispatcherId/{dispatcherId}")]
        public async Task<ProcessResultViewModel<List<UserDataViewModel>>> GetForemansByDispatcherId([FromRoute] int dispatcherId)
        {
            try
            {

                //Getting all foremans filtering by the given dispatcher id
                ProcessResult<List<Foreman>> entityResult = manger.GetAll(x => x.DispatcherId == dispatcherId);
                if (entityResult.Data != null && entityResult.Data.Count > 0)
                {
                    var result = entityResult.Data.Select(x => new UserDataViewModel
                    {
                        Id = x.Id,
                        FullName = x.User?.FullName ?? "",
                        UserId = x.UserId
                    });

                    return ProcessResultViewModelHelper.Succedded<List<UserDataViewModel>>(result.ToList());
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, string.Format(SharedResource.General_No_Entity_Found, SharedResource.Entity_Dispatcher)  );
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to get foremans specific data by dispatcher id
        /// </summary>
        /// <param name="dispatcherId">Dispatcher ID</param>
        /// <returns>List of foremans specific data for a specific dispatcher id</returns>
        [HttpGet]
        [Route("GetForemansDataByDispatcherId/{dispatcherId}")]
        public async Task<ProcessResultViewModel<List<UserDataViewModel>>> GetForemansDataByDispatcherId([FromRoute]int dispatcherId)
        {
            try
            {
                var entityResult = manger.GetAll(x => x.DispatcherId == dispatcherId,c=>c.User);
                if (entityResult.Data != null && entityResult.Data.Count > 0)
                {
                    // var identity = await identityUserService.GetByUserIds(entityResult.Data.Select(x => x.UserId).ToList());

                    //var result = (from member in entityResult.Data
                    //              join id in identity.Data on member.UserId equals id.Id
                    //              select new UserDataViewModel
                    //              {
                    //                  Id = member.Id,
                    //                  FullName = id.FirstName + " " + id.LastName,
                    //                  UserId = member.UserId
                    //              });
                            var result = from member in entityResult.Data select new UserDataViewModel
                                 {
                                     Id = member.Id,
                                     FullName = member.User.FullName,
                                     UserId = member.UserId
                                 };
                    return ProcessResultViewModelHelper.Succedded<List<UserDataViewModel>>(result.ToList());
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, ex.Message);
            }
        }


        /// <summary>
        /// This API to get all foremans data including identity data
        /// </summary>
        /// <returns>List of foremans full data</returns>
        [HttpGet]
        [Route("GetForemanUsers")]
        public async Task<ProcessResultViewModel<List<ForemanUserViewModel>>> GetForemanUsers()
        {
            try
            {
                //Getting all foremans from Foreman table
                ProcessResult<List<Foreman>> foremansTemp = manger.GetAll(x => x.User);
                if (foremansTemp.IsSucceeded && foremansTemp.Data.Count > 0)
                {
                    //Selecting all foremans user ids from Foreman table
                    List<ApplicationUser> users = foremansTemp.Data.Select(x => x.User).ToList();
                    //Calling identity service to get user data using above userIds
                    List<ForemanUserViewModel> result = mapper.Map<List<ApplicationUser>, List<ForemanUserViewModel>>(users);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<ForemanUserViewModel>(), SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<ForemanUserViewModel>>(null, ex.Message);
            }
        }

        /// <summary>
        /// This API to get a specific foreman data using his identity userId
        /// </summary>
        /// <param name="userId">Identiy User ID for foreman</param>
        /// <returns>A model of foreman data</returns>
        [HttpGet]
        [Route("GetForemanByUserId/{userId}")]
        public ProcessResultViewModel<ForemanViewModel> GetForemanByUserId([FromRoute] string userId) =>
            foremanService.GetForemanByUserId(userId);
        /// <summary>
        /// This API to get a specific foreman data using his identity userId
        /// Note: This API is same as the previous one but with different name :D
        /// </summary>
        /// <param name="userId">Identity User ID</param>
        /// <returns>A model of foreman data<</returns>
        [HttpGet]
        [Route("GetForemanById/{userId}")]
        public ProcessResultViewModel<ForemanViewModel> GetForemanById([FromRoute] int userId) =>
            foremanService.GetForemanById(userId);
        //{
        //    try
        //    {
        //        var entityResult = manger.GetAll(x => x.Id == userId).Data.FirstOrDefault();
        //        ForemanViewModel result = mapper.Map<Foreman, ForemanViewModel>(entityResult);
        //        return ProcessResultViewModelHelper.Succedded(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessResultViewModelHelper.Failed<ForemanViewModel>(null, ex.Message);
        //    }
        //}

        /// <summary>
        /// This API to block a foreman user
        /// </summary>
        /// <param name="id">Foreman ID</param>
        /// <returns>Bool result, true if block success or false if block failed</returns>
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null,SharedResource.UserNotFound );
            }

            //Blocking user by making him InActive = true in AspNetUsers table
            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                //Logically deleting user from Foreman table by making IsDeleted = true
                var result = manger.LogicalDeleteById(id);
                return result;
            }
            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen + SharedResource.BlockUser);
        }

        /// <summary>
        /// This API to override the base Update API which inherited from base just to update Team table with the new data
        /// after updating foreman data in Foreman table
        /// </summary>
        /// <param name="model">New data to update the old data</param>
        /// <returns>Foreman new data after update</returns>
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("UpdateAsync")]
        public ProcessResult<bool> Put([FromBody] ForemanViewModel model)
       => foremanService.Put(model);

        /// <summary>
        /// This API just to disable the default GetAll API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Deprecated")]
        public override ProcessResultViewModel<List<ForemanViewModel>> Get()
        {
            return base.Get();
        }

        /// <summary>
        /// This is the new GetAll method which is async, This API gets all Foreman users data and their FullName from identity service
        /// </summary>
        /// <returns>List of foreman users data</returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<ProcessResultViewModel<List<ForemanViewModel>>> GetAsync() =>
            await foremanService.GetAll();

        /// <summary>
        /// This API just to disable the default GetWithBlocked API which inherited from the base
        /// Note: We don't use this API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("WithBlockedDeprecated")]
        public override ProcessResultViewModel<List<ForemanViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        /// <summary>
        /// This is the new GetAllWithBlocked API which is async, This API gets all Foreman users data 
        /// with their FullName from identity service INCLUDING THE DELETED USERS (IsDeleted = true)
        /// </summary>
        /// <returns>List of Foreman users</returns>
        [HttpGet]
        [Route("GetAllWithBlocked")]
        public async Task<ProcessResultViewModel<List<ForemanViewModelWithTeamsCount>>> GetAllWithBlockedAsync()
        {
            var foremans = manger.GetAllQuerableWithBlocked().Data.ProjectTo<ForemanViewModelWithTeamsCount>().ToList();
            return ProcessResultViewModelHelper.Succedded(foremans);

        }
    }
}