﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.API.ServiceCommunications.DataManagementService.ContractTypesService;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ContractTypesController : BaseAuthforAdminController<IContractTypesManager, ContractTypes, ContractTypesViewModel>
    {
        IServiceProvider _serviceprovider;
        IContractTypesManager manager;
        IProcessResultMapper processResultMapper;
        private readonly IContractTypesService _service;
        public ContractTypesController(IContractTypesService service, IServiceProvider serviceprovider, IContractTypesManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<ContractTypesViewModel> ContractTypeCached) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper, ContractTypeCached)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            _service = service;
        }
        private ILang_ContractTypesManager Lang_ContractTypesmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_ContractTypesManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }
        [HttpGet]
        [Route("GetContractTypesByLanguage")]
        public ProcessResultViewModel<ContractTypesViewModel> GetContractTypesByLanguage([FromQuery]int ContractTypeId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_ContractTypesRes = Lang_ContractTypesmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            //var entityResult = manager.Get(ContractTypeId);
            var entityResult = base.Get(ContractTypeId);
            entityResult.Data.Name = Lang_ContractTypesRes.Data.Name;
            //var result = processResultMapper.Map<ContractTypes, ContractTypesViewModel>(entityResult);
            //return result;
            return entityResult;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public async System.Threading.Tasks.Task<ProcessResultViewModel<ContractTypesViewModel>> GetByNameAsync([FromRoute] string name)
            => await _service.GetByName(name);

        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<ContractTypesViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "1";
            //var actions = manager.GetAll();
            var actions = base.Get();
            //FillCurrentUser(entityResult);
            if (!string.IsNullOrEmpty(languageIdValue))
            {
                foreach (var item in actions.Data)
                {
                    var ContractTypesLocalizesRes = Lang_ContractTypesmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_ContractTypes_ID == item.Id);
                    if (ContractTypesLocalizesRes != null && ContractTypesLocalizesRes.Data != null)
                    {
                        item.Name = ContractTypesLocalizesRes.Data.Name;
                    }
                }
            }
            //if (actions.IsSucceeded && actions.Data.Count > 0)
            //{
            //    var result = mapper.Map<List<ContractTypes>, List<ContractTypesViewModel>>(actions.Data);

            //}
            //return processResultMapper.Map<List<ContractTypes>, List<ContractTypesViewModel>>(actions);
            return actions;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<ContractTypesViewModel> Post([FromBody] ContractTypesViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<ContractTypesViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<ContractTypesViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] ContractTypesViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
