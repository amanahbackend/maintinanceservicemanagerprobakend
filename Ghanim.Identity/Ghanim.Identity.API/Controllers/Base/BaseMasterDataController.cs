﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Utilites.ProcessingResult;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ghanim.API.Controllers
{
    public class BaseAuthforAdminController<ITManager, T, TViewModel> : BaseController<ITManager, T, TViewModel>
        where ITManager : IRepository<T>
        where TViewModel : class, IRepoistryBaseEntity
        where T : class, IBaseEntity
    {

        public BaseAuthforAdminController
            (ITManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<TViewModel> cashed = null)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper, cashed)
        {

        }

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<TViewModel> Post([FromBody]TViewModel model) => base.Post(model);

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<TViewModel>> PostMulti([FromBody]List<TViewModel> lstmodel) => base.PostMulti(lstmodel);
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody]TViewModel model) => base.Put(model);
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Delete/{id}")]

        public override ProcessResultViewModel<bool> Delete([FromRoute] int id) => base.Delete(id);
        #endregion

        #region LogicalDeleteApi
        [HttpDelete]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("LogicalDelete/{id}")]
        public override ProcessResultViewModel<bool> LogicalDelete([FromRoute]int id) => base.LogicalDelete(id);
        #endregion

    }
}
