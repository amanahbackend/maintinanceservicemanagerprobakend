﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Utilites.ProcessingResult;
using System.Linq;
using Ghanim.Models.Identity.Static;
using Ghanim.ResourceLibrary.Resources;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_CompanyCodeController : BaseAuthforAdminController<ILang_CompanyCodeManager, Lang_CompanyCode, Lang_CompanyCodeViewModel>
    {
        IServiceProvider _serviceprovider;
        ILang_CompanyCodeManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_CompanyCodeController(IServiceProvider serviceprovider, ILang_CompanyCodeManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ICompanyCodeManager CompanyCodemanager
        {
            get
            {
                return _serviceprovider.GetService<ICompanyCodeManager>();
            }
        }
        private ISupportedLanguagesManager supportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetAllLanguagesByCompanyCodeId/{CompanyCodeId}")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguagesByCompanyCodeId([FromRoute]int CompanyCodeId)
        {
             var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary = new List<LanguagesDictionaryViewModel>();
            var CompanyCodeRes = CompanyCodemanager.Get(CompanyCodeId);
            var entityResult = manager.GetAllLanguagesByCompanyCodeId(CompanyCodeId);
            foreach (var item in entityResult.Data)
            {
                var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
            }

            if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
            {
                foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                {
                    languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                }
            }
            codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = CompanyCodeRes.Data.Code, languagesDictionaries = languagesDictionary });
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguages")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguages()
        {
            var SupportedLanguagesRes = supportedLanguagesmanager.GetAll();
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var CompanyCodeRes = CompanyCodemanager.GetAll();
            foreach (var CompanyCode in CompanyCodeRes.Data)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByCompanyCodeId(CompanyCode.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = CompanyCode.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                {
                    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                    {
                        if (SupportedLanguage.Name == "English")
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = CompanyCode.Name });

                        }
                        else
                        {
                            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                        }
                    }
                }
                codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = CompanyCode.Code, languagesDictionaries = languagesDictionary, Id = CompanyCode.Id });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [HttpGet]
        [Route("GetAllLanguagesWithBlocked")]
        public ProcessResultViewModel<List<CodeWithAllLanguagesViewModel>> GetAllLanguagesWithBlocked()
        {
            //var SupportedLanguagesRes = supportedLanguagesmanager.GetAllWithBlocked();
            List<CodeWithAllLanguagesViewModel> codeWithAllLanguages = new List<CodeWithAllLanguagesViewModel>();
            List<LanguagesDictionaryViewModel> languagesDictionary;
            var CompanyCodeRes = CompanyCodemanager.GetAllWithBlocked();
            var OrderedCompanyCodeRes = CompanyCodeRes.Data.OrderByDescending(o => o.Id).ToList();
            foreach (var CompanyCode in OrderedCompanyCodeRes)
            {
                languagesDictionary = new List<LanguagesDictionaryViewModel>();
                var entityResult = manager.GetAllLanguagesByCompanyCodeId(CompanyCode.Id);
                foreach (var item in entityResult.Data)
                {
                    var SupportedLanguageRes = supportedLanguagesmanager.Get(item.FK_SupportedLanguages_ID);
                    if (SupportedLanguageRes.Data.Name == "English")
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = CompanyCode.Name, Id = item.Id });
                    }
                    else
                    {
                        languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguageRes.Data.Name, Value = item.Name, Id = item.Id });
                    }
                }

                //if (SupportedLanguagesRes.Data.Count > entityResult.Data.Count)
                //{
                //    foreach (var SupportedLanguage in SupportedLanguagesRes.Data)
                //    {
                //        if (SupportedLanguage.Name == "English")
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = CompanyCode.Name });

                //        }
                //        else
                //        {
                //            languagesDictionary.Add(new LanguagesDictionaryViewModel { Key = SupportedLanguage.Name, Value = string.Empty });
                //        }
                //    }
                //}
                codeWithAllLanguages.Add(new CodeWithAllLanguagesViewModel { Code = CompanyCode.Code, languagesDictionaries = languagesDictionary, Id = CompanyCode.Id, IsDeleted = CompanyCode.IsDeleted });
            }
            var result = ProcessResultViewModelHelper.Succedded<List<CodeWithAllLanguagesViewModel>>(codeWithAllLanguages);
            return result;
        }

        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_CompanyCode> lstModel)
        {
            var entityResult = manager.UpdateByCompanyCode(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [Route("UpdateAllLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateAllLanguages([FromBody]CodeWithAllLanguagesViewModel Model)
        {
            string enName = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;
            List<Lang_CompanyCode> lstModel = new List<Lang_CompanyCode>();
            var stroredArea = CompanyCodemanager.Get(x => x.Name == enName).Data;
            if (stroredArea != null && (stroredArea?.Id ?? 0) != Model.Id)
                return ProcessResultViewModelHelper.Failed<bool>(false, string.Format(SharedResource.General_Duplicate, SharedResource.Entity_CompanyCode), ProcessResultStatusCode.InvalidValue);


            foreach (var languagesDictionar in Model.languagesDictionaries)
            {
                var SupportedLanguagesRes = supportedLanguagesmanager.GetLanguageByName(languagesDictionar.Key);
                if (SupportedLanguagesRes.Data != null)
                {
                    lstModel.Add(new Lang_CompanyCode { FK_CompanyCode_ID = Model.Id, FK_SupportedLanguages_ID = SupportedLanguagesRes.Data.Id, Name = languagesDictionar.Value, Id = languagesDictionar.Id });
                }
            }
            var entityResult = manager.UpdateByCompanyCode(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
