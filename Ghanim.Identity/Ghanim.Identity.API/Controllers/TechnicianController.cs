﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchProduct.Controllers;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using CommonEnums;
using Ghanim.API.Helper;

using Ghanim.API.ServiceCommunications.Order;
using Ghanim.BLL.IManagers;
using CommonEnum;

using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.Models.Identity.Static;
using Utilities.ProcessingResult;
using Ghanim.Models.Order.Enums;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.AspNetCore.Authorization;
using Ghanim.Models.Order.Enums;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TechnicianController : BaseAuthforAdminController<ITechnicianManager, Technician, TechnicianViewModel>
    {
        private readonly IOrderService _orderService;
        //public ITechnicianManager manger;
        public ITeamMemberManager teamMemberManager;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        ITechniciansStateManager techniciansStateManager;
        private readonly ITechnicianService _technicianService;
        //IAreaService areaService;
        IAPIHelper helper;
        public TechnicianController(
            ITechnicianService technicianService,
            ITechnicianManager _manger,
            IAPIHelper _helper,
            ITeamMemberManager _teamMemberManager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityService _identityUserService,
          //IAreaService _areaService,
          IOrderService orderService,
        ITechniciansStateManager _techniciansStateManager) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            helper = _helper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            teamMemberManager = _teamMemberManager;
            techniciansStateManager = _techniciansStateManager;
            _orderService = orderService;
            _technicianService = technicianService;
            //areaService = _areaService;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<TechnicianViewModel> Post([FromBody] TechnicianViewModel model)
        {
            model.TeamId = model.TeamId == 0 ? null : model.TeamId;

            try
            {
                ProcessResultViewModel<TechnicianViewModel> resultViewModel = base.Post(model);
                teamMemberManager.Add(new TeamMember()
                {
                    MemberParentId = resultViewModel.Data.Id,
                    MemberParentName = resultViewModel.Data.Name,
                    MemberType = model.IsDriver?MemberType.Driver :  MemberType.Technician,
                    UserId = resultViewModel.Data.UserId,
                    TeamId = model.TeamId,
                    DivisionId = resultViewModel.Data.DivisionId,
                    UserTypeId =model.IsDriver ? UserTypesEnum.Driver :  UserTypesEnum.Technician
                });

                return resultViewModel;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianViewModel>(null, ex.Message);
            }
        }
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddAsync")]
        public ProcessResult<Technician> Add([FromBody] TechnicianViewModel model)
       => _technicianService.Add(model);
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("UpdateAsync")]
        public ProcessResult<bool> Put([FromBody] TechnicianViewModel model)
       => _technicianService.Put(model);

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<TechnicianViewModel>> PostMulti([FromBody] List<TechnicianViewModel> lstmodel)
        {

            foreach (var input in lstmodel)
            {
                input.TeamId = input.TeamId == 0 ? null : input.TeamId;
            }

            try
            {
                ProcessResultViewModel<List<TechnicianViewModel>> resultViewModel = base.PostMulti(lstmodel);
                foreach (var item in resultViewModel.Data)
                {
                    var res = teamMemberManager.Add(new TeamMember()
                    {
                        MemberParentId = item.Id,
                        MemberParentName = item.Name,
                        MemberType = MemberType.Technician,
                        UserId = item.UserId,
                        TeamId = item.TeamId,
                        DivisionId = item.DivisionId,

                    });
                }
                return resultViewModel;
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TechnicianViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetTechnicianUser/{id}")]
        public async Task<ProcessResultViewModel<TechnicianUserViewModel>> GetTechnicianUser([FromRoute] int id)
        {
            try
            {
                ProcessResult<Technician> technicianTemp = manger.Get(id);
                if (technicianTemp.IsSucceeded && technicianTemp.Data != null)
                {
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(technicianTemp.Data.UserId);
                    TechnicianUserViewModel result = mapper.Map<Technician, TechnicianUserViewModel>(technicianTemp.Data);
                    mapper.Map<ApplicationUserViewModel, TechnicianUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<TechnicianUserViewModel>(null, SharedResource.General_InvalidId);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetTechnicianUsers")]
        public async Task<ProcessResultViewModel<List<TechnicianUserViewModel>>> GetTechnicianUsers()
        {
            try
            {
                ProcessResult<List<Technician>> techniciansTemp = manger.GetAll();
                if (techniciansTemp.IsSucceeded && techniciansTemp.Data.Count > 0)
                {
                    List<string> userIds = techniciansTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<TechnicianUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<TechnicianUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map<Technician, TechnicianUserViewModel>(techniciansTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<TechnicianUserViewModel>(), string.Format(SharedResource.General_No_Entity_Found, SharedResource.Entity_Technician));
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TechnicianUserViewModel>>(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login([FromBody] LoginViewModel model)
        {

            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.Username))
            {
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, SharedResource.UserNamePassRequired);
            }
            else
            {
                ProcessResultViewModel<LoginResultViewModel> loginResult = await identityUserService.Login(model);

                if (loginResult.IsSucceeded && loginResult.Data != null)
                {
                    var technicianResult = manger.Get(x => x.UserId == loginResult.Data.UserId);
                    if (technicianResult.IsSucceeded && technicianResult.Data != null)
                    {
                        var teamMemberResult = teamMemberManager.Get(x => x.MemberParentId == technicianResult.Data.Id);
                        if (teamMemberResult.IsSucceeded && teamMemberResult.Data != null)
                        {
                            if (teamMemberResult.Data.TeamId != null)
                            {
                                loginResult.Data.TeamId = (int)teamMemberResult.Data.TeamId;
                                loginResult.Data.Id = (int)technicianResult.Data.Id;
                                loginResult.Data.FK_SupportedLanguages_ID = (int)technicianResult.Data.FK_SupportedLanguages_ID;
                                var lastState = techniciansStateManager.GetLastStateByTechnicianId(technicianResult.Data.Id);
                                if (lastState.IsSucceeded && lastState.Data != null)
                                {
                                    loginResult.Data.LastState = lastState.Data.State;
                                }
                                return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                            }
                            else
                            {
                                return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data, "this technican is not assigned to team");
                            }
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "something wrong while fetching technician's member user");
                        }
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, "something wrong while fetching technician data");
                    }
                }
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, SharedResource.WrongLoginIdentity);
            }
        }

        [HttpPost]
        [Route("Logout")]
        public async Task<ProcessResultViewModel<bool>> Logout([FromBody]LogoutViewModel model)
        {
            WorkingViewModel workingTypeViewModel = new WorkingViewModel();
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.DeviceId))
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.UserNamePassRequired);
            }
            else
            {
                ProcessResultViewModel<bool> logoutResult = identityUserService.Logout(model.UserId, model.DeviceId);

                if (logoutResult.IsSucceeded)
                {
                    var technicianResult = manger.Get(x => x.UserId == model.UserId);
                    if (technicianResult.IsSucceeded && technicianResult.Data != null)
                    {
                        var teamMemberResult = teamMemberManager.Get(x => x.MemberParentId == technicianResult.Data.Id);
                        if (teamMemberResult.IsSucceeded && teamMemberResult.Data != null)
                        {
                            if (teamMemberResult.Data.TeamId != null)
                            {
                                var technicianStateRes = techniciansStateManager.Add(new TechniciansState
                                {
                                    Lat = model.Lat,
                                    Long = model.Long,
                                    //todo mapping infects erorrs
                                    //TeamId = technicianResult.Data.TeamId,
                                    TechnicianId = technicianResult.Data.Id,
                                    State = TechnicianState.End
                                });
                                workingTypeViewModel.TeamId = teamMemberResult.Data.TeamId;
                                workingTypeViewModel.TechnicianId = technicianStateRes.Data.TechnicianId;
                                workingTypeViewModel.Long = technicianStateRes.Data.Long;
                                workingTypeViewModel.Lat = technicianStateRes.Data.Lat;
                                //workingTypeViewModel.StateID = (int)TechnicianState.End;
                                workingTypeViewModel.WorkingTypeId = 10;
                                workingTypeViewModel.WorkingTypeName = "End Work";
                                workingTypeViewModel.ActionDate = DateTime.Now;
                                workingTypeViewModel.RejectionReasonId = null;
                                workingTypeViewModel.RejectionReason = "";
                                workingTypeViewModel.OrderId = null;
                                workingTypeViewModel.State = TechnicianState.End;
                                workingTypeViewModel.userId = model.UserId;
                                var result = await _orderService.RegisterEndWork(workingTypeViewModel);
                                return ProcessResultViewModelHelper.Succedded<bool>(true);
                            }
                            else
                            {
                                return ProcessResultViewModelHelper.Succedded<bool>(false, SharedResource.TechNotAssignedToTeam);
                            }
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<bool>(false, string.Format(SharedResource.General_WrongWhileFetching, SharedResource.General_TechMemberUser));
                        }
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, string.Format(SharedResource.General_WrongWhileFetching, SharedResource.General_TechData)   );
                    }
                }
                return ProcessResultViewModelHelper.Failed<bool>(false,  SharedResource.WrongLoginIdentity);
            }
        }

        [HttpGet]
        [Route("GetTechnicianByUserId/{userId}")]
        public ProcessResultViewModel<TechnicianViewModel> GetTechnicianByUserId([FromRoute] string userId)
        {
            try
            {
                var entityResult = manger.Get(x => x.UserId == userId).Data;
                TechnicianViewModel result = mapper.Map<Technician, TechnicianViewModel>(entityResult);
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<TechnicianViewModel>(null, ex.Message);
            }
        }

        [HttpPost]
        [Route("GetTechniciansByTeamsId")]
        public ProcessResultViewModel<List<TechnicianViewModel>> GetTechniciansByUserId([FromBody] List<int> teamsId) =>
            _technicianService.GetTechniciansByTeamIds(teamsId);

        [HttpGet]
        [Route("Search")]
        public ProcessResultViewModel<List<TechnicianViewModel>> Search(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return ProcessResultViewModelHelper.Succedded(new List<TechnicianViewModel>(), SharedResource.WordCannotEmpty);
            }
            else
            {
                var entityResult = manger.Search(word);
                var result = processResultMapper.Map<List<Technician>, List<TechnicianViewModel>>(entityResult);
                return result;
            }
        }

        [HttpPut]
        [Route("UpdateTechnicianLanguage/{languageId}")]
        public IProcessResultViewModel<bool> UpdateTechnicianLanguage(int languageId)
        {
            var header = helper.GetAuthHeader(Request);
            string currentUserId = helper.GetUserIdFromToken(header);
            if (currentUserId == null || currentUserId == "")
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.UnAuthorized);
            }
            var technicianRes = manger.GetAll().Data.Where(x => x.UserId == currentUserId).FirstOrDefault();
            technicianRes.FK_SupportedLanguages_ID = languageId;
            var result = manger.Update(technicianRes);
            if (result.IsSucceeded)
            {
                return ProcessResultViewModelHelper.Succedded<bool>(true);
            }
            else
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.LangTechDoesnotChange);
            }
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.UserNotFound);
            }

            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                var result = manger.LogicalDeleteById(id);
                return result;
            }

            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen + SharedResource.BlockUser);
        }

        [HttpGet]
        [Route("Deprecated")]
        public override ProcessResultViewModel<List<TechnicianViewModel>> Get()
        {
            return base.Get();
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ProcessResultViewModel<List<TechnicianViewModel>>> GetAsync() =>
            await _technicianService.GetAllTechnicians();

        //Added by Shaimaa Sayed
        [HttpGet]
        [Route("GetAllAssignedTechnicians")]
        public async Task<ProcessResultViewModel<List<TechnicianViewModel>>> GetAllAssignedTechnicians()
        {
            //var users = base.Get();
            //var identity = await identityUserService.GetByUserIds(users.Data.Where(x => x.TeamId != null).Select(x => x.UserId).ToList());

            //foreach (var item in users.Data)
            //{
            //    item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            //}

            //return users;

            var techQuarable = manger.GetAllQuerable().Data.Where(x => x.TeamId != null);


            var users = techQuarable.ProjectTo<TechnicianViewModel>();
            //var identity = await identityUserService.GetByUserIds(users.Select(x => x.UserId).ToList());

            //foreach (var item in users)
            //{
            //    item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            //}
            return ProcessResultViewModelHelper.Succedded(users.ToList());

        }

        [HttpGet]
        [Route("WithBlockedDeprecated")]
        public override ProcessResultViewModel<List<TechnicianViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        [HttpGet]
        [Route("GetAllWithBlocked")]
        public async Task<ProcessResultViewModel<List<TechnicianViewModel>>> GetAllWithBlockedAsync()
        {
            var users = manger.GetAllQuerableWithBlocked().Data.ProjectTo<TechnicianViewModel>().ToList();
            return ProcessResultViewModelHelper.Succedded(users);
        }

        [HttpPost]
        [Route("GetFullNameByIds")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetFullNameByIds([FromBody] List<int> ids)
        {
            var techs = manger.GetByIds(ids).Data?.Select(x => x.UserId).ToList();
            if (techs == null || techs.Count() == 0)
            {
                return ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, SharedResource.General_InvalidInput);
            }

            var identity = await identityUserService.GetByUserIds(techs);
            return identity;
        }
    }
}