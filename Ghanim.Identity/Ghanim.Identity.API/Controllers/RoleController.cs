﻿using AutoMapper;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.Models.Identity.Static;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    //[ApiVersion("1.0")]
    [Authorize(Roles = StaticRoles.Admin)]
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private IApplicationRoleManager _applicationRoleManager;

        public RoleController(IApplicationRoleManager applicationRoleManager)
        {
            _applicationRoleManager = applicationRoleManager;
        }

        [HttpPost, Route("Add")]
        public async Task<ProcessResultViewModel<ApplicationRoleViewModel>> Add([FromBody] ApplicationRoleViewModel roleViewModel)
        {
            ProcessResultViewModel<ApplicationRoleViewModel> result = null;
            try
            {
                var role = Mapper.Map<ApplicationRoleViewModel, ApplicationRole>(roleViewModel);
                role = await _applicationRoleManager.AddRoleAsync(role);
                if (role != null)
                {
                    roleViewModel = Mapper.Map<ApplicationRole, ApplicationRoleViewModel>(role);
                    result = ProcessResultViewModelHelper.Succedded(roleViewModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, SharedResource.CouldnotCreateRole);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpDelete, Route("Delete")]
        public async Task<ProcessResultViewModel<bool>> Delete([FromQuery]string roleName)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeleted = await _applicationRoleManager.Delete(roleName);
                result = ProcessResultViewModelHelper.Succedded(isDeleted);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPut, Route("Update")]
        public async Task<ProcessResultViewModel<ApplicationRoleViewModel>> Update([FromBody] ApplicationRoleViewModel roleViewModel)
        {
            ProcessResultViewModel<ApplicationRoleViewModel> result = null;
            try
            {
                var role = Mapper.Map<ApplicationRoleViewModel, ApplicationRole>(roleViewModel);
                role = await _applicationRoleManager.Update(role);
                if (role != null)
                {
                    roleViewModel = Mapper.Map<ApplicationRole, ApplicationRoleViewModel>(role);
                    result = ProcessResultViewModelHelper.Succedded(roleViewModel);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, SharedResource.NoUpdateRole);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationRoleViewModel>(null, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("GetAll")]
        public ProcessResultViewModel<List<ApplicationRoleViewModel>> GetAll()
        {
            ProcessResultViewModel<List<ApplicationRoleViewModel>> result = null;
            try
            {
                var roles = _applicationRoleManager.GetAll();
                var roleViewModels = Mapper.Map<List<ApplicationRole>, List<ApplicationRoleViewModel>>(roles);
                result = ProcessResultViewModelHelper.Succedded(roleViewModels);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationRoleViewModel>>(null, ex.Message);
            }
            return result;
        }

    }
}
