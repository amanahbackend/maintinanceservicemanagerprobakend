﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.BLL.IManagers;
using Ghanim.API.Helper;
using Ghanim.Models.Identity.Static;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.Models.Context;
using Microsoft.AspNetCore.Authorization;
using Ghanim.Models.Context;

namespace Ghanim.API.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public partial class UserManagementUserController : Controller
    {
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        IDispatcherManager dispatcherManager;
        ISupervisorManager supervisorManager;
        ITechnicianManager technicianManager;
        ITeamMemberManager teamMemberManager;
        ITeamManager teamManager;
        IForemanManager foremanManager;
        IManagerManager managerManager;
        IEngineerManager engineerManager;
        IDriverManager driverManager;
        IVehicleManager vehicleManager;
        IMaterialControllerManager materialControllerManager;
        IAPIHelper helper;
        private readonly ApplicationDbContext _context;
        public UserManagementUserController(IProcessResultMapper _processResultMapper,
            ApplicationDbContext Context,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            IIdentityService _identityUserService,
            IDispatcherManager _dispatcherManager,
            ISupervisorManager _supervisorManager,
            ITechnicianManager _technicianManager,
            ITeamMemberManager _teamMemberManager,
            ITeamManager _teamManager,
            IForemanManager _foremanManager,
            IManagerManager _managerManager,
            IEngineerManager _engineerManager,
            IDriverManager _driverManager,
            IVehicleManager _vehicleManager,
             IAPIHelper _helper,
            IMaterialControllerManager _materialControllerManager)
        {
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            dispatcherManager = _dispatcherManager;
            supervisorManager = _supervisorManager;
            technicianManager = _technicianManager;
            teamMemberManager = _teamMemberManager;
            teamManager = _teamManager;
            foremanManager = _foremanManager;
            managerManager = _managerManager;
            engineerManager = _engineerManager;
            driverManager = _driverManager;
            vehicleManager = _vehicleManager;
            materialControllerManager = _materialControllerManager;
            helper = _helper;
            _context = Context;
        }


        [HttpPost]
        [Route("Login")]
        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login([FromBody] LoginViewModel model)
        {
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetByUserName(model.Username);


            if (userTemp.Data.IsDeleted)
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null,SharedResource.UserBloked);

            if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.Username))
            {
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, SharedResource.UserNamePassRequired);
            }
            else
            {
                ProcessResultViewModel<LoginResultViewModel> loginResult = await identityUserService.Login(model);

                if (loginResult.IsSucceeded && loginResult.Data != null)
                {
                    loginResult.Data.IsFirstLogin = userTemp.Data.IsFirstLogin;
                    loginResult.Data.LanguageId = userTemp.Data.LanguageId;
                    if (loginResult.Data.LanguageId == 1)
                        loginResult.Data.LanguageCode = "Ar";
                    if (loginResult.Data.LanguageId == 2)
                        loginResult.Data.LanguageCode = "En";
                    if (loginResult.Data.LanguageId == 6)
                        loginResult.Data.LanguageCode = "Hi";
                    if (loginResult.Data.LanguageId == 7)
                        loginResult.Data.LanguageCode = "Ur";
                    //loginResult.Data.LanguageName= userTemp.Data.LanguageName.Value;
                    if (loginResult.Data.Roles.Contains("Dispatcher"))
                    {
                        var dispatcherResult = dispatcherManager.Get(x => x.UserId == loginResult.Data.UserId);
                        if (dispatcherResult.IsSucceeded && dispatcherResult.Data != null)
                        {
                            loginResult.Data.Id = dispatcherResult.Data.Id;
                            return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, SharedResource.General_WrongWhileGet);
                        }
                    }
                    else if (loginResult.Data.Roles.Contains("Supervisor"))
                    {
                        var supervisorResult = supervisorManager.Get(x => x.UserId == loginResult.Data.UserId);
                        if (supervisorResult.IsSucceeded && supervisorResult.Data != null)
                        {
                            loginResult.Data.Id = supervisorResult.Data.Id;
                            return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, SharedResource.General_WrongWhileGet);
                        }
                    }
                    else if (loginResult.Data.Roles.Contains("Technician"))
                    {
                        var technicianResult = technicianManager.Get(x => x.UserId == loginResult.Data.UserId);
                        if (technicianResult.IsSucceeded && technicianResult.Data != null)
                        {
                            var teamMemberResult = teamMemberManager.Get(x => x.MemberParentId == technicianResult.Data.Id);
                            if (teamMemberResult.IsSucceeded && teamMemberResult.Data != null)
                            {
                                if (teamMemberResult.Data.TeamId != null)
                                {
                                    loginResult.Data.TeamId = (int)teamMemberResult.Data.TeamId;
                                    return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                                }
                                else
                                {
                                    return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data, SharedResource.TechNotAssignedToTeam);
                                }
                            }
                            else
                            {
                                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, string.Format(SharedResource.General_WrongWhileFetching, SharedResource.General_TechMemberUser)  );
                            }
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null,  string.Format(SharedResource.General_WrongWhileFetching, SharedResource.General_TechData)  );
                        }
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Succedded<LoginResultViewModel>(loginResult.Data);
                    }
                }

                // }
                return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(input: null, statusCode: ProcessResultStatusCode.NotFound, message: loginResult.Status?.Message ?? SharedResource.General_SomethingWrongHappen);
            }

            //  }
            return ProcessResultViewModelHelper.Failed<LoginResultViewModel>(null, SharedResource.General_SomethingWrongHappen);
        }
        [Authorize]
        [HttpPost]
        [Route("SubmitLanguage")]
        public async Task<ProcessResult<bool>> SubmitLanguage([FromBody] SubmitLanguageViewModel model)
        {
            ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(model.UserId);
            //var roleNames = userTemp.Data.RoleNames.FirstOrDefault();
            if (userTemp.IsSucceeded && userTemp.Data != null)
            {
                userTemp.Data.IsFirstLogin = false;
                userTemp.Data.LanguageId = model.LanguageId;
                ProcessResultViewModel<ApplicationUserViewModel> result = await identityUserService.UpdateUser(userTemp.Data);
                return ProcessResultHelper.Succedded<bool>(true);
            }
            return ProcessResultHelper.Succedded<bool>(true);
        }
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpDelete]
        [Route("DeleteAllUsers")]
        public ProcessResult<bool> DeleteAllUsers()
        {
            var Orders = _context.Orders.Where(x => x.SupervisorId != null).ToList();
            if (Orders.Count > 0)
            {
                return ProcessResultHelper.Failed<bool>(false,null , "Sorry, you can’t delete users, some users are assigned to orders");
            }
            else
            {
                var deleteMaterialControllers = materialControllerManager.DeleteAll();
                if (!deleteMaterialControllers.Data)
                    return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_MaterialsControllers);

            var deleteDrivers = driverManager.DeleteAll();
            if (!deleteDrivers.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Driver + SharedResource.General_S );

            var deleteVehicles = vehicleManager.DeleteAll();
            if (!deleteVehicles.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Vehicle + SharedResource.General_S  );

            var deleteTechnicians = technicianManager.DeleteAll();
            if (!deleteTechnicians.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Technician + SharedResource.General_S  );

            var deleteTeamMembers = teamMemberManager.DeleteAll();
            if (!deleteTeamMembers.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_TeamMember + SharedResource.General_S  );

            var deleteTeams = teamManager.DeleteAll();
            if (!deleteTeams.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Team + SharedResource.General_S   );

            var deleteDispatchers = dispatcherManager.DeleteAll();
            if (!deleteDispatchers.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Dispatcher + SharedResource.General_S   );

            var deleteSupervisors = supervisorManager.DeleteAll();
            if (!deleteSupervisors.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Supervisor + SharedResource.General_S   );

            var deleteManagers = managerManager.DeleteAll();
            if (!deleteManagers.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Manager + SharedResource.General_S  );

            var deleteEngineers = engineerManager.DeleteAll();
            if (!deleteEngineers.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Engineer + SharedResource.General_S   );

            var deleteForemans = foremanManager.DeleteAll();
            if (!deleteForemans.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Foreman + SharedResource.General_S  );

            }
            return ProcessResultHelper.Succedded<bool>(true);
        }

        [Authorize]
        [HttpGet]
        [Route("GetUserNameById/{userId}")]
        public async Task<ProcessResult<string>> GetUserNameById(string userId)
        {
            ProcessResultViewModel<ApplicationUserViewModel> user = await identityUserService.GetById(userId);
            return ProcessResultHelper.Succedded<string>(user?.Data?.UserName ?? null);
        }


    }
}