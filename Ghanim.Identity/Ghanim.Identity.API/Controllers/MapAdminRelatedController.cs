﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Authorization;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API.Controllers
{
    [Route("api/[controller]")]
    public class MapAdminRelatedController : BaseAuthforAdminController<IMapAdminRelatedManager, MapAdminRelated, MapAdminRelatedViewModel>
    {
        private IMapAdminRelatedManager _manger;
        private IMapper _mapper;
        private IProcessResultMapper _processResultMapper;
        private IProcessResultPaginatedMapper _processResultPaginatedMapper;

        public MapAdminRelatedController(IMapAdminRelatedManager manger, IMapper mapper, IProcessResultMapper processResultMapper, IProcessResultPaginatedMapper processResultPaginatedMapper, ICached<MapAdminRelatedViewModel> cash)
            : base(manger, mapper, processResultMapper, processResultPaginatedMapper, cash)
        {
            _manger = manger;
            _mapper = mapper;
            _processResultMapper = processResultMapper;
            _processResultPaginatedMapper = processResultPaginatedMapper;
        }





        #region GetApi



        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<MapAdminRelatedViewModel> Post([FromBody]MapAdminRelatedViewModel model)
            => base.Post(model);


        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<MapAdminRelatedViewModel>> PostMulti([FromBody]List<MapAdminRelatedViewModel> lstmodel)
            => base.PostMulti(lstmodel);

        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody]MapAdminRelatedViewModel model)
            => base.Put(model);

        #endregion
        #region DeleteApi
        [HttpDelete]
        [Authorize(Roles = StaticRoles.Closed)]
        [Route("Delete/{id}")]
        public override ProcessResultViewModel<bool> Delete([FromRoute]int id)
            => base.Delete(id);

        #endregion

        #region LogicalDeleteApi

        [HttpDelete]
        [Authorize(Roles = StaticRoles.Closed)]
        [Route("LogicalDelete/{id}")]
        public override ProcessResultViewModel<bool> LogicalDelete([FromRoute] int id)
            => base.LogicalDelete(id);

        #endregion



    }
}