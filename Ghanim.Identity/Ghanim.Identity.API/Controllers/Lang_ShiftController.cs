﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Ghanim.Models.Identity.Static;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class Lang_ShiftController : BaseAuthforAdminController<ILang_ShiftManager, Lang_Shift, Lang_ShiftViewModel>
    {
        ILang_ShiftManager manager;
        IProcessResultMapper processResultMapper;
        public Lang_ShiftController(ILang_ShiftManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manager;
            processResultMapper = _processResultMapper;
        }

        [HttpGet]
        [Route("GetAllLanguagesByShiftId/{ShiftId}")]
        public ProcessResultViewModel<List<Lang_ShiftViewModel>> GetAllLanguagesByAreaId([FromRoute]int ShiftId)
        {
            var entityResult = manager.GetAllLanguagesByShiftId(ShiftId);
            var result = processResultMapper.Map<List<Lang_Shift>, List<Lang_ShiftViewModel>>(entityResult);
            return result;
        }

        [Route("UpdateLanguages")]
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPut]
        public ProcessResultViewModel<bool> UpdateLanguages([FromBody]List<Lang_Shift> lstModel)
        {
            //string enName = Model.languagesDictionaries.FirstOrDefault(x => x.Key == "English")?.Value;
            //List<Lang_Shift> lstModel = new List<Lang_Shift>();
            //var stroredArea = BuildingTypesmanager.Get(x => x.Name == enName).Data;
            //if (stroredArea != null && (stroredArea?.Id ?? 0) != Model.Id)
            //    return ProcessResultViewModelHelper.Failed<bool>(false, "Item Already Existed With That Name", ProcessResultStatusCode.InvalidValue);



            var entityResult = manager.UpdateByShift(lstModel);
            var result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }
    }
}
