﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Ghanim.API.ViewModels;
using Ghanim.Models.Identity.Static;
using Utilities.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class FinanceController : BaseAuthforAdminController<IFinanceManager, Finance, FinanceViewModel>
    {
        public IFinanceManager manger;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        public IForemanManager foreManManger;

        public FinanceController(IFinanceManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, IIdentityService _identityUserService, IForemanManager _foreManManger)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;

            foreManManger = _foreManManger;
        }

        /// <summary>
        /// This API to get Finance by his id data including identity data
        /// </summary>
        /// <param name="id">Finance ID</param>
        /// <returns>A model with enginner data</returns>
        [HttpGet]
        [Route("GetFinanceUser/{id}")]
        public async Task<ProcessResultViewModel<FinanceUserViewModel>> GetFinanceUser([FromRoute] int id)
        {
            try
            {
                //Getting Finance data from Finance table by id
                ProcessResult<Finance> FinanceTemp = manger.Get(id);
                if (FinanceTemp.IsSucceeded && FinanceTemp.Data != null)
                {
                    //Calling identity service to get Finance user data
                    ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(FinanceTemp.Data.UserId);
                    FinanceUserViewModel result = mapper.Map<Finance, FinanceUserViewModel>(FinanceTemp.Data);
                    mapper.Map<ApplicationUserViewModel, FinanceUserViewModel>(userTemp.Data, result);
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<FinanceUserViewModel>(null, "invalid Finance id");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<FinanceUserViewModel>(null, ex.Message);
            }
        }



  


        /// <summary>
        /// This API to get all Finances data encluding their identity data
        /// </summary>
        /// <returns>List of Finances full data</returns>
        [HttpGet]
        [Route("GetFinanceUsers")]
        public async Task<ProcessResultViewModel<List<FinanceUserViewModel>>> GetFinanceUsers()
        {
            try
            {
                //Getting all Finances from Finance table
                ProcessResult<List<Finance>> financesTemp = manger.GetAll();
                if (financesTemp.IsSucceeded && financesTemp.Data.Count > 0)
                {
                    //Selecting list of Finance user ids 
                    List<string> userIds = financesTemp.Data.Select(x => x.UserId).ToList();
                    //Calling identity service to get Finances user data like full name, number, etc...
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<FinanceUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<FinanceUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map(financesTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<FinanceUserViewModel>(), "no Finances found");
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<FinanceUserViewModel>>(null, ex.Message);
            }
        }



        /// <summary>
        /// This API to block an Finance user
        /// </summary>
        /// <param name="id">Finance ID</param>
        /// <returns>Bool result, true if block success or false if block failed</returns>
        [HttpPut]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null, "User not found");
            }

            //Blocking user by making him InActive = true in AspNetUsers table
            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                //Logically deleting user from Finance table by making IsDeleted = true
                var result = manger.LogicalDeleteById(id);
                return result;
            }

            return ProcessResultHelper.Failed<bool>(false, null, "Something went wrong when trying to block user");
        }


 


        //validate bfore edit
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] FinanceViewModel model)
        {
            try
            {
                var oldModel = manger.Get(model.Id).Data;
                if (model.PF != oldModel.User.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, "Can't change PF");
                }
              
                return base.Put(model);
            }

            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }
    }
}