﻿using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class CostCenterController : BaseAuthforAdminController<ICostCenterManager, CostCenter, CostCenterViewModel>
    {
        IServiceProvider _serviceprovider;
        ICostCenterManager manager;
        IProcessResultMapper processResultMapper;
        public CostCenterController(IServiceProvider serviceprovider, ICostCenterManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<CostCenterViewModel> CostCenterCached) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper, CostCenterCached)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        private ILang_CostCenterManager Lang_CostCenterManager
        {
            get
            {
                return _serviceprovider.GetService<ILang_CostCenterManager>();
            }
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<CostCenterViewModel>> PostMulti([FromBody] List<CostCenterViewModel> lstModel)
        {
            ProcessResultViewModel<List<CostCenterViewModel>> result = null;
            List<CostCenterViewModel> addedCostCenter = new List<CostCenterViewModel>();
            foreach (var item in lstModel)
            {
                var costCenterIsExit = manager.costCenterIsExit(item.Name);
                if (costCenterIsExit != null && costCenterIsExit.Data)
                {
                    result = ProcessResultViewModelHelper.Failed<List<CostCenterViewModel>>(null, SharedResource.CostCenterWithName + item.Name);
                }
                else
                {
                    addedCostCenter.Add(item);
                }

            }
            result = base.PostMulti(addedCostCenter);

            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<CostCenterViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //var actions = manager.GetAll();
            var actions = base.Get();
            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var OrderTypeLocalizesRes = Lang_CostCenterManager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_CostCenter_ID == item.Id);
                    if (OrderTypeLocalizesRes != null && OrderTypeLocalizesRes.Data != null)
                    {
                        item.Name = OrderTypeLocalizesRes.Data.Name;
                    }
                }
            }
            //if (actions.IsSucceeded && actions.Data.Count > 0)
            //{
            //    //var result = mapper.Map<List<OrderProblem>, List<OrderProblemViewModel>>(actions.Data);
            //    //return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
            //    //return ProcessResultHelper.Succedded<List<AreasViewModel>>(result);
            //}
            //return processResultMapper.Map<List<CostCenter>, List<CostCenterViewModel>>(actions);
            return actions;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<CostCenterViewModel> Post([FromBody] CostCenterViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<CostCenterViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<CostCenterViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] CostCenterViewModel model)
        {            
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity != null && (entity?.Id ?? 0) != model.Id)
                return ProcessResultViewModelHelper.Failed<bool>(false, "Item Already Existed With That Name", ProcessResultStatusCode.InvalidValue);
                       
            else
                 return base.Put(model);
                //   return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
