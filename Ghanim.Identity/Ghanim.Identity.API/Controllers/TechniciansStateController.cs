﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using CommonEnums;
using DispatchProduct.Controllers;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[Controller]")]
    public class TechniciansStateController : BaseAuthforAdminController<ITechniciansStateManager,TechniciansState,TechniciansStateViewModel>
    {
        public ITechniciansStateManager manager;
        public readonly new IMapper mapper;
        IProcessResultMapper ProcessResultMapper;
        IProcessResultPaginatedMapper ProcessResultPaginatedMapper;
        private readonly ITechnicianStateService _technicianStateService;
        //private readonly IIdentityService identityUserService;

        public TechniciansStateController(
            ITechnicianStateService technicianStateService, 
            ITechniciansStateManager _manager,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper):base(_manager,_mapper,_processResultMapper,_processResultPaginatedMapper)
        {
            manager = _manager;
            mapper = _mapper;
            ProcessResultMapper = _processResultMapper;
            ProcessResultPaginatedMapper = _processResultPaginatedMapper;
            _technicianStateService = technicianStateService;
            //identityUserService = _identityUserService;
        }
        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<TechniciansStateViewModel> Post([FromBody] TechniciansStateViewModel model) =>
            _technicianStateService.Add(model);

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<TechniciansStateViewModel>> PostMulti([FromBody] List<TechniciansStateViewModel> lstmodel)
        {
            foreach (var item in lstmodel)
            {
                item.ActionDate = DateTime.UtcNow;
            }
            ProcessResultViewModel<List<TechniciansStateViewModel>> resultViewModel = base.PostMulti(lstmodel);
            
            return resultViewModel;
        }

        [HttpGet]
        [Route("GetTechnicianStates")]
        public ProcessResult<List<EnumEntity>> GetTechnicianStates()
        {
            return ProcessResultHelper.Succedded<List<EnumEntity>>(EnumManager<TechnicianState>.GetEnumList());
        }
    }
}