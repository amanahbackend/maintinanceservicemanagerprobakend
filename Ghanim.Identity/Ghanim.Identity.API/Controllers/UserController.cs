﻿//last modified date: 11-2-2020
//Last modified By : Shaimaa Marey
//history of modifcation :-change on Setting The language id and Is First login Add User Method (shaimaa marey) 
using AutoMapper;
using Ghanim.API.Utilities;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.GenericListToExcel;
using Utilites;
using System.IO;
using Ghanim.API.Enums;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Microsoft.Extensions.DependencyInjection;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;
using Ghanim.API.ViewModels.UserManagement;
using Ghanim.Models.Order.Enums;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.ResourceLibrary.Resources;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    //[ApiVersion("1.0")]
    [Route("api/[controller]")]
    public partial class UserController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IApplicationUserManager _applicationUserManager;
        private readonly IApplicationUserHistoryManager _applicationUserHistoryManager;
        private readonly IConfigurationRoot _configuration;
        private readonly IProcessResultMapper _processResultMapper;
        private readonly IPasswordTokenPinManager _passwordTokenPinManager;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IUploadImageFileManager _imageManager;
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly IIdentityService _identityService;
        private readonly new ApplicationDbContext _context;
        public ISupervisorManager _supervisorManager;
        public IDispatcherManager _dispatcherManager;
        public IForemanManager _foremanManager;
        public IEngineerManager _engineerManager;
        public IManagerManager _managerManager;
        public ITechnicianManager _technicianManager;
        public ITechnicianService _technicianService;
        public IForemanService _foremanService;
        public IFinanceManager _financeManager;
        

        IServiceProvider _serviceprovider;

        public UserController(ApplicationDbContext context, IForemanService foremanService, ITechnicianService technicianService, ISupervisorManager supervisorManager, IDispatcherManager dispatcherManager, IForemanManager foremanManager, IEngineerManager engineerManager, IManagerManager managerManager, ITechnicianManager technicianManager, IServiceProvider serviceprovider, IApplicationUserManager applicationUserManager,
            IHostingEnvironment hostingEnv, IConfigurationRoot configuration,
            IPasswordTokenPinManager passwordTokenPinManager, IPasswordHasher<ApplicationUser> passwordHasher,
             IUploadImageFileManager imageManager,
             IProcessResultMapper processResultMapper,
             IUserDeviceManager userDeviceManager,
             IApplicationUserHistoryManager applicationUserHistoryManager,
            IIdentityService identityService,
            IFinanceManager financeManager
            )
        {
            _context = context;
            _technicianService = technicianService;
            _foremanService = foremanService;
            _supervisorManager = supervisorManager;
            _dispatcherManager = dispatcherManager;
            _engineerManager = engineerManager;
            _managerManager = managerManager;
            _technicianManager = technicianManager;
            _foremanManager = foremanManager;
            _identityService = identityService;
            _serviceprovider = serviceprovider;
            _passwordHasher = passwordHasher;
            _passwordTokenPinManager = passwordTokenPinManager;
            _configuration = configuration;
            _hostingEnv = hostingEnv;
            _applicationUserManager = applicationUserManager;
            _imageManager = imageManager;
            _userDeviceManager = userDeviceManager;
            _processResultMapper = processResultMapper;
            _applicationUserHistoryManager = applicationUserHistoryManager;
            _financeManager = financeManager;
        }

        private IApplicationUserHistoryManager ApplicationUserHistorymanager
        {
            get
            {
                return _serviceprovider.GetService<IApplicationUserHistoryManager>();
            }
        }

        [HttpPost, Route("Login")]
        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login([FromBody]LoginViewModel loginViewModel) =>
          await _identityService.Login(loginViewModel);

        //[HttpPost, Route("AddUserDevice")]
        //public async Task<ProcessResultViewModel<bool>> AddUserDevice([FromBody]UserDevice userDevice)
        //{
        //    _userDeviceManager.AddIfNotExist(userDevice);
        //    return ProcessResultViewModelHelper.Succedded(true);
        //}

        [Authorize]
        [HttpPost, Route("AddUserDevice")]
        public async Task<ProcessResultViewModel<bool>> AddUserDevice([FromBody]UserDevice userDevice)
        {
            await _userDeviceManager.AddIfNotExistAzureAsync(userDevice);
            return ProcessResultViewModelHelper.Succedded(true);
        }


        //[HttpPut, Route("Logout")]
        //public  ProcessResultViewModel<bool> Logout([FromQuery]string userId, [FromQuery] string deviceId)
        //{

        //    var loginUserRes = ApplicationUserHistorymanager.GetLoginUser(userId,deviceId);
        //    if (loginUserRes !=null && loginUserRes.Data.Count >0)
        //    {
        //        foreach (var item in loginUserRes.Data)
        //        {
        //            item.LogoutDate = DateTime.UtcNow;
        //            var logoutUserRes = ApplicationUserHistorymanager.Update(item);
        //        }
        //         _userDeviceManager.Delete(deviceId);
        //        return ProcessResultViewModelHelper.Succedded(true);
        //    }
        //    else
        //    {
        //        return ProcessResultViewModelHelper.Failed<bool>(false, "This User can't logout. ");
        //    }

        //}

        [Authorize]
        [HttpPut, Route("Logout")]
        public ProcessResultViewModel<bool> Logout([FromQuery] string userId, [FromQuery] string deviceId) =>
            _identityService.Logout(userId, deviceId);

        [HttpGet, Route("Test")]
        public IActionResult Test()
        {
            //var currentUserName = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var currentUserName2 = HttpContext.User.FindFirst(JwtRegisteredClaimNames.Sub).Value;
            var currentUserId = HttpContext.User.FindFirst(JwtRegisteredClaimNames.Jti).Value;
            return Ok();
        }
        //Last Modified by ::shaimaa marey
        //Setting IsFirstLogin=true ,LanguageId=2 to the default values when creating New Users , To resolve a bug in login 
        [HttpPost, Route("Add")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Add([FromBody] ApplicationUserViewModel userViewModel)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            try
            {
                //if (userViewModel.Picture != null)
                //{
                //    userViewModel.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.Picture,
                //                                                 userViewModel.UserName, _imageManager, _hostingEnv);
                //}

                var isExisting = await _applicationUserManager.IsUserNameExistAsync(userViewModel.UserName);
                if (isExisting)
                {
                    return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, SharedResource.UserNameExist, ProcessResultStatusCode.Dublicated);
                }
                ApplicationUser user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel);
                user.IsFirstLogin = true;
                user.LanguageId = 2;
                var userAddResult = await _applicationUserManager.AddUserAsync(user, userViewModel.Password);
                if (userAddResult.IsSucceeded)
                {
                    user = userAddResult.Data;
                    userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    result = ProcessResultViewModelHelper.Succedded(userViewModel);
                }
                else
                {
                    //if (userViewModel.Picture != null)
                    //{
                    //    ProfilePictureManager.DeleteProfilePicture(user.UserName, _hostingEnv);
                    //    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, userAddResult.Exception.Message);
                    //}
                    result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, userAddResult.Exception.Message);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, ex.Message);
            }
            return result;
        }
        //New
        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPost, Route("CreateUser")]
        public async Task<ProcessResultViewModel<bool>> CreateUser([FromBody] UserViewModel userViewModel)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {

                var isExisting = await _applicationUserManager.IsUserNameExistAsync(userViewModel.BaseUser.UserName);
                if (isExisting)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.UserNameExist, ProcessResultStatusCode.Dublicated);
                }
                ApplicationUser user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel.BaseUser);
                user.IsFirstLogin = true;
                user.LanguageId = 2;
                #region UserType

                //Dispatcher
                if (userViewModel.DispatcherViewModel != null)
                    user.UserTypeId = UserTypesEnum.Dispatcher;
                //Supervisor
                if (userViewModel.SupervisorViewModel != null)
                    user.UserTypeId = UserTypesEnum.Supervisor;
                //Technician
                if (userViewModel.TechnicianViewModel != null)
                    user.UserTypeId = userViewModel.TechnicianViewModel.IsDriver ? UserTypesEnum.Driver :
                        UserTypesEnum.Technician;
                // UserTypesEnum.Technician;
                //Manager
                if (userViewModel.ManagerViewModel != null)
                    user.UserTypeId = UserTypesEnum.Manager;
                //Foreman Calling 
                if (userViewModel.ForemanViewModel != null)
                    user.UserTypeId = UserTypesEnum.Foreman;
                //Engineer Calling 
                if (userViewModel.EngineerViewModel != null)
                    user.UserTypeId = UserTypesEnum.Engineer;
                if (userViewModel.FinanceViewModel != null)
                    user.UserTypeId = UserTypesEnum.Finance;
                #endregion


                var userAddResult = await _applicationUserManager.AddUserAsync(user, userViewModel.BaseUser.Password);
                if (userAddResult.IsSucceeded)
                {
                    user = userAddResult.Data;
                    //Here Call Service Communication
                    //Dispatcher Calling                    
                    if (userViewModel.DispatcherViewModel != null)
                    {
                        userViewModel.DispatcherViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<DispatcherViewModel, Dispatcher>(userViewModel.DispatcherViewModel);
                        var DispatcherAddResult = _dispatcherManager.Add(Entityresult);
                        if (DispatcherAddResult.IsSucceeded)
                        {
                            //userViewModel = Mapper.Map<UserViewModel, userViewModel.DispatcherViewModel>(user);
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                    }
                    //Supervisor Calling 
                    if (userViewModel.SupervisorViewModel != null)
                    {
                        userViewModel.SupervisorViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<SupervisorViewModel, Supervisor>(userViewModel.SupervisorViewModel);
                        var SuperVisorAddResult = _supervisorManager.Add(Entityresult);
                        if (SuperVisorAddResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                    }
                    //Technician Calling 
                    if (userViewModel.TechnicianViewModel != null)
                    {
                        userViewModel.TechnicianViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<TechnicianViewModel, Technician>(userViewModel.TechnicianViewModel);
                        var TechAddResult = _technicianService.Add(userViewModel.TechnicianViewModel);
                        if (TechAddResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                    }
                    //Manager Calling 
                    if (userViewModel.ManagerViewModel != null)
                    {
                        userViewModel.ManagerViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<ManagerViewModel, Manager>(userViewModel.ManagerViewModel);
                        var MangAddResult = _managerManager.Add(Entityresult);
                        if (MangAddResult.IsSucceeded)
                        {
                            //userViewModel = Mapper.Map<UserViewModel, userViewModel.DispatcherViewModel>(user);
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                    }
                    //Foreman Calling 
                    if (userViewModel.ForemanViewModel != null)
                    {
                        userViewModel.ForemanViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<ForemanViewModel, Foreman>(userViewModel.ForemanViewModel);
                        var ForeAddResult = _foremanManager.Add(Entityresult);
                        if (ForeAddResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                    }
                    //Engineer Calling 
                    if (userViewModel.EngineerViewModel != null)
                    {
                        userViewModel.EngineerViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<EngineerViewModel, Engineer>(userViewModel.EngineerViewModel);
                        var EnginAddResult = _engineerManager.Add(Entityresult);
                        if (EnginAddResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                    }

                    if (userViewModel.FinanceViewModel != null)
                    {
                        userViewModel.FinanceViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<FinanceViewModel, Finance>(userViewModel.FinanceViewModel);
                        var EnginAddResult = _financeManager.Add(Entityresult);
                        if (EnginAddResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                    }

                    result = ProcessResultViewModelHelper.Succedded<bool>(true);
                }
                else
                {

                    result = ProcessResultViewModelHelper.Failed<bool>(false, userAddResult.Exception.Message);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
            return result;
        }


        [Authorize]
        [HttpGet, Route("GetBy")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetBy([FromQuery] string username) =>
          await _identityService.GetByUserName(username);
        [HttpPut, Route("Update")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Update([FromBody]ApplicationUserViewModel userViewModel) =>
        await _identityService.UpdateUser(userViewModel);
        //New Update User
        [HttpPut, Route("UpdateUser")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<bool>> UpdateUser([FromBody]UserViewModel userViewModel)
        {
            if (userViewModel.BaseUser.Picture != null)
            {
                userViewModel.BaseUser.PicturePath = ProfilePictureManager.SaveProfilePicture(userViewModel.BaseUser.Picture,
                                                                 userViewModel.BaseUser.UserName, _imageManager, _hostingEnv);
            }
            var user = Mapper.Map<ApplicationUserViewModel, ApplicationUser>(userViewModel.BaseUser);
            //Here Call Service Communication
            //Dispatcher Calling                    
            if (userViewModel.DispatcherViewModel != null)
            {

                var oldModel = _context.Dispatchers.FirstOrDefault(x => x.Id == userViewModel.DispatcherViewModel.Id);
                if (userViewModel.DispatcherViewModel.PF != oldModel.User.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NotAllowPF, ProcessResultStatusCode.InvalidValue);
                }
                if (userViewModel.DispatcherViewModel.SupervisorId != oldModel.SupervisorId || userViewModel.DispatcherViewModel.DivisionId != oldModel.DivisionId)
                {
                    var foreManCount = oldModel.Foremans.Count();
                    var ordersCount = oldModel.Orders.Count(x =>
                        x.StatusId == (int)OrderStatusEnum.On_Hold && x.StatusId == (int)OrderStatusEnum.Open);

                    //var foreManCount = foreManManger.GetAll(x => x.DispatcherId == model.Id).Data.Count();
                    //var ordersCount = orderManager.GetAll(x => x.DispatcherId == model.Id && StaticAppSettings.UnassignedStatusId.Contains(x.StatusId)).Data.Count();
                    if (ordersCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeDispatcherCurrentOrder, ProcessResultStatusCode.AssignedForWork);
                    }
                    if (foreManCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeDispatcherCurrentForman, ProcessResultStatusCode.HasChilds);
                    }
                    if (foreManCount > 0 && ordersCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, "", ProcessResultStatusCode.AssignedForWorkAndHasUsers);
                    }
                }

                var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                if (updateUserResult.IsSucceeded)
                {
                    user = updateUserResult.Data;
                    userViewModel.DispatcherViewModel.UserId = user.Id;
                    var Entityresult = Mapper.Map<DispatcherViewModel, Dispatcher>(userViewModel.DispatcherViewModel);
                    var DispatcherUpdateResult = _dispatcherManager.Update(Entityresult);
                    if (DispatcherUpdateResult.IsSucceeded)
                    {
                        //userViewModel = Mapper.Map<UserViewModel, userViewModel.DispatcherViewModel>(user);
                        return ProcessResultViewModelHelper.Succedded<bool>(true, null);
                    }
                }
                else
                    return ProcessResultViewModelHelper.Failed<bool>(false, null);


            }
            //Supervisor Calling 
            if (userViewModel.SupervisorViewModel != null)
            {
                var oldObject = await _context.Supervisors.FirstOrDefaultAsync(x => x.Id == userViewModel.SupervisorViewModel.Id);
                if (userViewModel.SupervisorViewModel.ManagerId != oldObject.ManagerId || userViewModel.SupervisorViewModel.DivisionId != oldObject.DivisionId)
                {
                    var DispUserCount = oldObject.Dispatchers.Count();
                    var SupervisorOrdersCount = oldObject.Orders.Count(x =>
                        x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);
                    if (DispUserCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeSupDivHasDispUser, ProcessResultStatusCode.HasChilds);
                    }
                    if (SupervisorOrdersCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeSupDivHasCurOrder, ProcessResultStatusCode.AssignedForWork);

                    }
                    if (DispUserCount > 0 && SupervisorOrdersCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeSupDivCurOrderDispUsr, ProcessResultStatusCode.AssignedForWorkAndHasUsers);

                    }
                    else
                    {
                        var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                        if (updateUserResult.IsSucceeded)
                        {
                            user = updateUserResult.Data;
                            //userViewModel.SupervisorViewModel.UserId = user.Id;
                            //userViewModel.SupervisorViewModel.UserId = user.Id;
                            var Entityresult = Mapper.Map<SupervisorViewModel, Supervisor>(userViewModel.SupervisorViewModel);
                            var SuperVisorUpdateResult = _supervisorManager.Update(Entityresult);
                            if (SuperVisorUpdateResult.IsSucceeded)
                            {
                                //userViewModel = Mapper.Map<UserViewModel, userViewModel.DispatcherViewModel>(user);
                                return ProcessResultViewModelHelper.Succedded<bool>(true, null);
                            }
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, null);
                    }
                }
                else
                {
                    var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                    if (updateUserResult.IsSucceeded)
                    {
                        user = updateUserResult.Data;
                        //userViewModel.SupervisorViewModel.UserId = user.Id;
                        //userViewModel.SupervisorViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<SupervisorViewModel, Supervisor>(userViewModel.SupervisorViewModel);
                        var SuperVisorUpdateResult = _supervisorManager.Update(Entityresult);
                        if (SuperVisorUpdateResult.IsSucceeded)
                        {
                            //userViewModel = Mapper.Map<UserViewModel, userViewModel.DispatcherViewModel>(user);
                            return ProcessResultViewModelHelper.Succedded<bool>(true, null);
                        }
                    }
                    else
                        return ProcessResultViewModelHelper.Failed<bool>(false, null);
                }


            }
            //Technician Calling 
            if (userViewModel.TechnicianViewModel != null)
            {
                var oldObject = _context.Technicians.FirstOrDefault(x => x.Id == userViewModel.TechnicianViewModel.Id);

                if (oldObject.User.PF != userViewModel.TechnicianViewModel.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangePF, ProcessResultStatusCode.InvalidValue);
                }
                if (oldObject.DivisionId != userViewModel.TechnicianViewModel.DivisionId && userViewModel.TechnicianViewModel.TeamId != null)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeDivTechAssigned, ProcessResultStatusCode.UserHasTeam);
                }
                else
                {
                    user.UserTypeId = userViewModel.TechnicianViewModel.IsDriver ?
                        UserTypesEnum.Driver : UserTypesEnum.Technician;
                        
                       var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                    if (updateUserResult.IsSucceeded)
                    {
                        user = updateUserResult.Data;
                        userViewModel.TechnicianViewModel.UserId = user.Id;
                        //var Entityresult = Mapper.Map<TechnicianViewModel, Technician>(userViewModel.TechnicianViewModel);
                        //var TechUpdateResult = _technicianManager.Update(Entityresult);
                        var TechUpdateResult = _technicianService.Put(userViewModel.TechnicianViewModel);
                        if (TechUpdateResult.IsSucceeded)
                        {
                            //userViewModel = Mapper.Map<UserViewModel, userViewModel.DispatcherViewModel>(user);
                            return ProcessResultViewModelHelper.Succedded<bool>(true);
                        }
                    }
                    else
                        return ProcessResultViewModelHelper.Failed<bool>(false);
                }
            }
            //Manager Calling 
            if (userViewModel.ManagerViewModel != null)
            {
                var oldModel = _context.Managers.FirstOrDefault(x => x.Id == userViewModel.ManagerViewModel.Id);
                if (userViewModel.ManagerViewModel.PF != oldModel.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangePF, ProcessResultStatusCode.InvalidValue);
                }
                var SupervisorCount = oldModel.Supervisors.Count();
                if (SupervisorCount > 0)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeManagerSupUser, ProcessResultStatusCode.HasChilds);
                }
                else
                {
                    var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                    if (updateUserResult.IsSucceeded)
                    {
                        user = updateUserResult.Data;
                        userViewModel.ManagerViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<ManagerViewModel, Manager>(userViewModel.ManagerViewModel);
                        var MangUpdateResult = _managerManager.Update(Entityresult);
                        if (MangUpdateResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Failed<bool>(true, null);
                        }
                    }
                    else
                        return ProcessResultViewModelHelper.Failed<bool>(false, null);
                }

            }
            //Foreman Calling 
            if (userViewModel.ForemanViewModel != null)
            {
                var teams = _context.Teams.Where(x => x.ForemanId == userViewModel.ForemanViewModel.Id).ToList();
                var oldForemenDat = _context.Foremans.FirstOrDefault(x => x.Id == userViewModel.ForemanViewModel.Id);
                int OrdersCount = 0;
                int newCount = 0;
                if (teams != null && teams.Count > 0)
                {
                    foreach (var item in teams)
                    {
                        var ordersCount = _context.Orders.Count(x => x.TeamId == item.Id
                        && x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);
                        // && (x.StatusId == (int)OrderStatusEnum.On_Travel
                        //   || x.StatusId == (int)OrderStatusEnum.Reached
                        //   || x.StatusId == (int)OrderStatusEnum.Started));
                        OrdersCount = OrdersCount + ordersCount;

                    }
                }
                newCount = OrdersCount;
                if (oldForemenDat.DivisionId != userViewModel.ForemanViewModel.DivisionId && newCount > 0)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.Entity_Division + SharedResource.NoChangeForemanAssigned, ProcessResultStatusCode.HasChilds);
                }

                else if (oldForemenDat.SupervisorId != userViewModel.ForemanViewModel.SupervisorId && teams.Count > 0)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.Entity_Supervisor + SharedResource.NoChangeForemanAssigned, ProcessResultStatusCode.HasChilds);
                }
                else if (oldForemenDat.DispatcherId != userViewModel.ForemanViewModel.DispatcherId && newCount > 0)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.Entity_Dispatcher + SharedResource.NoChangeForemanAssigned, ProcessResultStatusCode.HasChilds);
                }
                else if (oldForemenDat.EngineerId != userViewModel.ForemanViewModel.EngineerId && newCount > 0)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.Entity_Engineer + SharedResource.NoChangeForemanAssigned, ProcessResultStatusCode.HasChilds);
                }
                else
                {
                    var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                    if (teams != null && teams.Count > 0)
                    {
                        foreach (var item in teams)
                        {
                            item.ForemanId = userViewModel.ForemanViewModel.Id;
                            item.DispatcherId = userViewModel.ForemanViewModel.DispatcherId;
                            item.EngineerId = userViewModel.ForemanViewModel.EngineerId;
                        }
                        _context.Teams.UpdateRange(teams);
                    }
                    if (updateUserResult.IsSucceeded)
                    {
                        user = updateUserResult.Data;
                        userViewModel.ForemanViewModel.UserId = user.Id;
                        var ForeUpdateResult = _foremanService.Put(userViewModel.ForemanViewModel);
                        if (ForeUpdateResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true, "Succedded");
                        }
                    }
                    else
                        return ProcessResultViewModelHelper.Failed<bool>(false, updateUserResult.Exception.Message);
                }

            }
            //Engineer Calling 
            if (userViewModel.EngineerViewModel != null)
            {
                var oldModel = _context.Engineers.FirstOrDefault(x => x.Id == userViewModel.EngineerViewModel.Id);
                if (userViewModel.EngineerViewModel.PF != oldModel.User.PF)
                {
                    return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangePF, ProcessResultStatusCode.InvalidValue);
                }
                if (userViewModel.EngineerViewModel.DivisionId != oldModel.DivisionId)
                {
                    var foreManCount = oldModel.Foremans.Count();
                    if (foreManCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeEngineerDivForeman, ProcessResultStatusCode.HasChilds);
                    }
                    else
                    {
                        var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                        if (updateUserResult.IsSucceeded)
                        {
                            user = updateUserResult.Data;
                            userViewModel.EngineerViewModel.UserId = user.Id;
                            var Entityresult = Mapper.Map<EngineerViewModel, Engineer>(userViewModel.EngineerViewModel);
                            var EnginUpdateResult = _engineerManager.Update(Entityresult);
                            if (EnginUpdateResult.IsSucceeded)
                            {
                                return ProcessResultViewModelHelper.Succedded<bool>(true, SharedResource.General_Succedded);

                            }
                        }
                        else
                            return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.General_FailedUpdate);
                    }
                }
                else
                {
                    var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
                    if (updateUserResult.IsSucceeded)
                    {
                        user = updateUserResult.Data;
                        userViewModel.EngineerViewModel.UserId = user.Id;
                        var Entityresult = Mapper.Map<EngineerViewModel, Engineer>(userViewModel.EngineerViewModel);
                        var EnginUpdateResult = _engineerManager.Update(Entityresult);
                        if (EnginUpdateResult.IsSucceeded)
                        {
                            return ProcessResultViewModelHelper.Succedded<bool>(true, SharedResource.General_Succedded);

                        }
                    }
                    else
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.General_FailedUpdate);
                }

            }
            if (userViewModel.DispatcherViewModel == null && userViewModel.SupervisorViewModel == null && userViewModel.TechnicianViewModel == null && userViewModel.ManagerViewModel == null && userViewModel.ForemanViewModel == null && userViewModel.EngineerViewModel == null && userViewModel.BaseUser != null)
            {
                var updateUserResult = await _applicationUserManager.UpdateUserAsync(user);
            }
            return ProcessResultViewModelHelper.Succedded<bool>(true);
        }

        [Authorize]
        [HttpGet, Route("GetAll")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAll() =>
            await _identityService.GetAll();
        //[HttpPost, Route("GetCustomerUsersPaginatedByCountry/{country}")]
        //public async Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCustomerUsersPaginatedByCountry
        //    ([FromRoute]Country country, [FromBody] IdentityPaginatedItems<ApplicationUserViewModel> model)
        //{
        //    var paginatedItems = new IdentityPaginatedItems<ApplicationUser>()
        //    {
        //        Count = model.Count,
        //        PageNo = model.PageNo,
        //        PageSize = model.PageSize
        //    };
        //    var resultData = await _applicationUserManager.GetAllCustomersPaginated(paginatedItems, country);
        //    model.Count = resultData.Count;
        //    model.PageNo = resultData.PageNo;
        //    model.PageSize = resultData.PageSize;
        //    model.Data = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(resultData.Data);
        //    return model;
        //}

        //[HttpPost, Route("GetCompanyUsersPaginatedByCountry/{country}")]
        //public async Task<IdentityPaginatedItems<ApplicationUserViewModel>> GetCompanyUsersPaginatedByCountry
        //    ([FromRoute]Country country, [FromBody] IdentityPaginatedItems<ApplicationUserViewModel> model)
        //{
        //    var paginatedItems = new IdentityPaginatedItems<ApplicationUser>()
        //    {
        //        Count = model.Count,
        //        PageNo = model.PageNo,
        //        PageSize = model.PageSize
        //    };
        //    var resultData = await _applicationUserManager.GetAllCompaniesPaginated(paginatedItems, country);
        //    model.Count = resultData.Count;
        //    model.PageNo = resultData.PageNo;
        //    model.PageSize = resultData.PageSize;
        //    model.Data = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(resultData.Data);
        //    return model;
        //}

        [HttpDelete, Route("Delete")]
        [Authorize(Roles = StaticRoles.Closed)]
        public async Task<ProcessResultViewModel<bool>> Delete([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeleted = await _applicationUserManager.DeleteAsync(username);
                if (isDeleted)
                {
                    ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, SharedResource.General_FailedDelete);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpDelete, Route("DeleteById")]
        public async Task<ProcessResultViewModel<bool>> DeleteById([FromQuery] string id)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var user = await _applicationUserManager.Get(id);
                var username = user?.UserName;
                var isDeleted = await _applicationUserManager.DeleteByIdAsunc(id);
                if (isDeleted)
                {
                    ProfilePictureManager.DeleteProfilePicture(username, _hostingEnv);
                    result = ProcessResultViewModelHelper.Succedded(isDeleted);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed(false, SharedResource.General_FailedDelete);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("GetByRole")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByRole([FromQuery] string role)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = (List<ApplicationUser>)await _applicationUserManager.GetUsersInRole(role);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, SharedResource.NoUserThsRole);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("GetAdminUsers")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetAdminUsers()
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                var users = (List<ApplicationUser>)await _applicationUserManager.GetUsersInRole(Enum.GetName(typeof(Roles), Roles.Admin));
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, SharedResource.NoUserThsRole);
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("IsEmailExist")]
        public async Task<ProcessResultViewModel<bool>> IsEmailExist([FromQuery]string email)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isEmailExist = await _applicationUserManager.IsEmailExistAsync(email);
                result = ProcessResultViewModelHelper.Succedded(isEmailExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("IsUsernameExist")]
        public async Task<ProcessResultViewModel<bool>> IsUsernameExist([FromQuery]string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isExisting = await _applicationUserManager.IsUserNameExistAsync(username);
                result = ProcessResultViewModelHelper.Succedded(isExisting);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("IsPhoneExist")]
        public ProcessResultViewModel<bool> IsPhoneExist([FromQuery] string phone)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isPhoneExist = _applicationUserManager.IsPhoneExist(phone);
                result = ProcessResultViewModelHelper.Succedded(isPhoneExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("Search")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> Search([FromQuery] string searchToken)
        {
            ProcessResultViewModel<List<ApplicationUserViewModel>> result = null;
            try
            {
                string[] searchFields = _configuration.GetSection("UserSearchFields").Get<string[]>();
                var users = await _applicationUserManager.Search(searchToken, searchFields);
                if (users.Count > 0)
                {
                    var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
                    result = ProcessResultViewModelHelper.Succedded(userModels);
                }
                else
                {
                    result = ProcessResultViewModelHelper.Succedded<List<ApplicationUserViewModel>>(null, string.Format(SharedResource.General_No_Entity_Found, SharedResource.Entity_Users));
                }
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed<List<ApplicationUserViewModel>>(null, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("Get")]
        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> Get([FromQuery] string id) =>
            await _identityService.GetById(id);

        [HttpGet, Route("Deactivate")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<bool>> Deactivate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isDeactivated = await _applicationUserManager.Deactivate(username);
                result = ProcessResultViewModelHelper.Succedded(isDeactivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("Activate")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<bool>> Activate([FromQuery] string username)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isActivated = await _applicationUserManager.Activate(username);
                result = ProcessResultViewModelHelper.Succedded(isActivated);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        //[HttpPost, Route("GetPhoneVerficationToken")]
        //public async Task<ProcessResultViewModel<string>> GetPhoneVerficationToken([FromBody] PhoneValidationViewModel model)
        //{
        //    ProcessResultViewModel<string> result = null;
        //    try
        //    {
        //        var code = await _applicationUserManager.GeneratePhoneNumberToken(model.Username, model.Phone);
        //        SendSmsViewModel sendSmsViewModel = new SendSmsViewModel
        //        {
        //            Message = $"Mawashi verification code: {code}",
        //            PhoneNumber = model.Phone
        //        };
        //        if (model.Country == Country.Kuwait)
        //        {
        //            _dropoutService.SendKw("v1", sendSmsViewModel);
        //        }
        //        else if (model.Country == Country.UAE)
        //        {
        //            _dropoutService.SendUae("v1", sendSmsViewModel);
        //        }
        //        var user = await _applicationUserManager.GetBy(model.Username);
        //        _dropoutService.SendMail("v1", new SendMailViewModel
        //        {
        //            Subject = "Mawashi: Activate Your Account",
        //            To = new List<string> { user.Email },
        //            Body = $"<h2>Welcome {user.FirstName} {user.LastName}</h2><hr/><h4>Mawashi verification code: {code}</h4>"
        //        });
        //        result = ProcessResultViewModelHelper.Succedded(code);
        //    }
        //    catch (Exception ex)
        //    {
        //        result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
        //    }
        //    return result;
        //}

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPost, Route("IsPhoneVerificationTokenValid")]
        public async Task<ProcessResultViewModel<bool>> IsPhoneVerificationTokenValid([FromBody] PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isValid = await _applicationUserManager.CheckPhoneValidationToken(model.Username, model.Phone, model.Code);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPost, Route("ConfirmPhone")]
        public async Task<ProcessResultViewModel<bool>> ConfirmPhone([FromBody]PhoneValidationViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var confirmed = await _applicationUserManager.ConfirmPhone(model.Username, model.Phone, model.Code);
                result = ProcessResultViewModelHelper.Succedded(confirmed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        //[HttpGet, Route("GenerateForgetPasswordToken")]
        //public async Task<ProcessResultViewModel<string>> GenerateForgetPasswordToken([FromQuery]string username)
        //{
        //    ProcessResultViewModel<string> result = null;
        //    try
        //    {
        //        var token = await _applicationUserManager.GenerateForgetPasswordToken(username);
        //        // we will implement send sms with this token to the user phone
        //        var user = await _applicationUserManager.GetBy(username);

        //        SendSmsViewModel sendSmsViewModel = new SendSmsViewModel
        //        {
        //            Message = $"Mawashi forget password verification code: {token}",
        //            PhoneNumber = user.Phone1
        //        };
        //        if (user.Fk_Country_Id == Country.Kuwait)
        //        {
        //            _dropoutService.SendKw("v1", sendSmsViewModel);
        //        }
        //        else if (user.Fk_Country_Id == Country.UAE)
        //        {
        //            _dropoutService.SendUae("v1", sendSmsViewModel);
        //        }
        //        _dropoutService.SendMail("v1", new SendMailViewModel
        //        {
        //            Subject = "Mawashi: Forget Password",
        //            To = new List<string> { user.Email },
        //            Body = $"<h2>Hello {user.FirstName} {user.LastName}</h2><hr/><h4>Mawashi forget password verification code: {token}</h4>"
        //        });

        //        result = ProcessResultViewModelHelper.Succedded(token);
        //    }
        //    catch (Exception ex)
        //    {
        //        result = ProcessResultViewModelHelper.Failed<string>(null, ex.Message);
        //    }
        //    return result;
        //}

        [HttpPost, Route("ForgetPassword")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<bool>> ForgetPassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var changed = await _applicationUserManager.ForgetPassword(model.Username, model.NewPassword, model.Code);
                result = ProcessResultViewModelHelper.Succedded(changed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ChangePassword")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<bool>> ChangePassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var changed = await _applicationUserManager.ChangePassword(model.Username, model.OldPassword, model.NewPassword);
                result = ProcessResultViewModelHelper.Succedded(changed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpPost, Route("ResetPassword")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<bool>> ResetPassword([FromBody]ChangePasswordViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isReset = await _applicationUserManager.ResetPassword(model.Username, model.NewPassword);
                result = ProcessResultViewModelHelper.Succedded(isReset);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }


        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPost, Route("ResetUsersPassword")]
        public async Task<ProcessResultViewModel<bool>> ResetUsersPassword([FromBody]ResetUsersPasswordViewModel model)
            => await _identityService.ResetPassword(model);



        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("IsPasswordPinExist")]
        public ProcessResultViewModel<bool> IsPasswordPinExist([FromQuery]string pin)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var isExist = _passwordTokenPinManager.IsPinExist(pin);
                result = ProcessResultViewModelHelper.Succedded(isExist);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("IsTokenValid")]
        public ProcessResultViewModel<bool> IsTokenValid()
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
                string token = authorizationValue.Substring("Bearer ".Length).Trim();
                bool isValid = TokenManager.ValidateToken(token);
                result = ProcessResultViewModelHelper.Succedded(isValid);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPost, Route("GetByIds")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByIds([FromBody]List<string> ids) =>
            await _identityService.GetByUserIds(ids);

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpPost, Route("GetByUsernames")]
        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUsernames([FromBody]List<string> usernames) =>
            await _identityService.GetByUserIds(usernames);

        [HttpPost, Route("AssignToRole")]
        [Authorize(Roles = StaticRoles.Closed)]
        public async Task<ProcessResultViewModel<bool>> AssignToRole([FromBody]AssignToRoleViewModel model)
        {
            ProcessResultViewModel<bool> result = null;
            try
            {
                var user = await _applicationUserManager.Get(model.UserId);
                user.RoleNames = model.Roles.ToList();
                var isSucceed = await _applicationUserManager.AddUserToRolesAsync(user);
                result = ProcessResultViewModelHelper.Succedded(isSucceed);
            }
            catch (Exception ex)
            {
                result = ProcessResultViewModelHelper.Failed(false, ex.Message);
            }
            return result;
        }

        [HttpGet, Route("DownloadReportFile")]
        [Authorize(Roles = StaticRoles.Closed)]
        public async Task<IActionResult> DownloadReportFile([FromQuery] string fileName)
        {
            var webRootInfo = _hostingEnv.ContentRootPath;
            var dirPath = webRootInfo + @"/wwwroot/Reports";
            string filePath = $"{dirPath}/{fileName}";
            var memoryStream = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memoryStream);
            }
            memoryStream.Position = 0;
            return File(memoryStream, FileUtilities.GetContentType(filePath), Path.GetFileName(filePath));
        }

        //[HttpGet, Route("UpdateUserLanguage")]
        //public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateUserLanguage([FromQuery]SupportedLanguage languageId)
        //{
        //    string authorizationValue = HttpContext.Request.Headers[HeaderNames.Authorization];
        //    string token = authorizationValue.Substring("Bearer ".Length).Trim();
        //    string userId = TokenManager.GetUserIdFromToken(token);
        //    var user = await _applicationUserManager.UpdateUserLanguage(userId, languageId);
        //    if (user != null)
        //    {
        //        var userViewModel = Mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
        //        return ProcessResultViewModelHelper.Succedded(userViewModel);
        //    }
        //    else
        //    {
        //        return ProcessResultViewModelHelper.Failed<ApplicationUserViewModel>(null, "An error occured while updating user");
        //    }
        //}

        [Authorize(Roles = StaticRoles.Admin)]
        [HttpGet, Route("GetDevices/{userId}")]
        public ProcessResultViewModel<List<UserDevice>> GetDevices([FromRoute] string userId)
        {
            var result = new ProcessResultViewModel<List<UserDevice>>();
            try
            {
                var userDevices = _userDeviceManager.GetByUserId(userId);
                result = ProcessResultViewModelHelper.Succedded(userDevices);
            }
            catch (Exception e)
            {
                result = ProcessResultViewModelHelper.Failed<List<UserDevice>>(null, e.Message);
            }
            return result;
        }
       
        [HttpDelete]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("DeleteAllUsers")]
        public ProcessResult<bool> DeleteAllUsers()
        {
            var userDevices = _userDeviceManager.DeleteAll();
            if (!userDevices)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_UserDevice);

            var usersHistory = _applicationUserHistoryManager.DeleteAll();
            if (!usersHistory.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_UsersHistory);

            var users = _applicationUserManager.DeleteAll();
            if (!users.Data)
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_WrongWhileDeleting + SharedResource.Entity_Users);

            return ProcessResultHelper.Succedded<bool>(true);
        }
      
        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("ToggleActivation/{id}")]
        public async Task<ProcessResultViewModel<bool>> ToggleActivation([FromRoute] string id) =>
        await _identityService.ToggleActivation(id);
    }
}