﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.API.ViewModels.Order;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    //[ApiController]
    public class VisitSlotController : BaseAuthforAdminController<IVisitSlotManager, VisitSlot, VisitSlotViewModel>
    {
        IMapper mapper;
        IVisitSlotManager manager;
        IProcessResultMapper processResultMapper;
        public VisitSlotController(IVisitSlotManager _manger, IMapper _mapper,
            IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
            : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manager = _manger;
            processResultMapper = _processResultMapper;
        }



        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<VisitSlotViewModel>> Get()
        {
            var entityResult = manger.GetAll();
            return processResultMapper.Map<List<VisitSlot>, List<VisitSlotViewModel>>(entityResult);
        }





    }
}
