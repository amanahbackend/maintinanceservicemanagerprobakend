﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;

using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.API.ViewModels.UserManagement;
using Ghanim.Models.Context;
using Ghanim.Models.Identity.Static;
using Ghanim.Models.Order.Enums;
using Microsoft.EntityFrameworkCore;
using Utilities.ProcessingResult;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.AspNetCore.Authorization;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class SupervisorController : BaseAuthforAdminController<ISupervisorManager, Supervisor, SupervisorViewModel>
    {
        public ISupervisorManager manger;
        private readonly new ApplicationDbContext _context;
        public readonly new IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        private readonly ISupervisorService _supervisorService;
        public SupervisorController(ApplicationDbContext context, ISupervisorService supervisorService, ISupervisorManager _manger, IIdentityService _identityUserService, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _context = context;
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            _supervisorService = supervisorService;
        }

        [HttpGet]
        [Route("GetSupervisorUser/{id}")]
        public async Task<ProcessResultViewModel<SupervisorUserViewModel>> GetSupervisorUser([FromRoute] int id)
        {
            try
            {
                Supervisor entity = manger.Get(id).Data;

                SupervisorUserViewModel result = mapper.Map<Supervisor, SupervisorUserViewModel>(entity);

                ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(entity.UserId);


                //ProcessResultViewModel<ApplicationUserViewModel> userTemp = await identityUserService.GetById(supervisorTemp.UserId);
                var DispUserCount = _context.Dispatchers.Count(x => !x.IsDeleted && x.SupervisorId == id);
                var SupervisorOrdersCount = _context.Orders.Count(x => !x.IsDeleted && x.SupervisorId == id && x.StatusId != (int)OrderStatusEnum.Cancelled &&
                                                                       x.StatusId != (int)OrderStatusEnum.Compelete);

                //SupervisorUserViewModel result = mapper.Map<Supervisor, SupervisorUserViewModel>(supervisorTemp);
                result.OrdersCount = SupervisorOrdersCount;
                result.DispUserCount = DispUserCount;
                //mapper.Map<ApplicationUserViewModel, SupervisorUserViewModel>(userTemp.Data, result);
                mapper.Map<ApplicationUserViewModel, SupervisorUserViewModel>(userTemp.Data, result);
                return ProcessResultViewModelHelper.Succedded(result);

            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<SupervisorUserViewModel>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetSupervisorUsers")]
        public async Task<ProcessResultViewModel<List<SupervisorUserViewModel>>> GetSupervisorUsers()
        {
            try
            {
                ProcessResult<List<Supervisor>> supervisorsTemp = manger.GetAll();
                if (supervisorsTemp.IsSucceeded && supervisorsTemp.Data.Count > 0)
                {
                    List<string> userIds = supervisorsTemp.Data.Select(x => x.UserId).ToList();
                    ProcessResultViewModel<List<ApplicationUserViewModel>> usersTemp = await identityUserService.GetByUserIds(userIds);
                    List<SupervisorUserViewModel> result = mapper.Map<List<ApplicationUserViewModel>, List<SupervisorUserViewModel>>(usersTemp.Data);
                    // YARAB SAM7NY .. Fawzy
                    for (int i = 0; i < result.Count; i++)
                    {
                        mapper.Map(supervisorsTemp.Data[i], result[i]);
                    }
                    return ProcessResultViewModelHelper.Succedded(result);
                }
                else
                {
                    return ProcessResultViewModelHelper.Succedded(new List<SupervisorUserViewModel>(), SharedResource.General_NoDataFound);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<SupervisorUserViewModel>>(null, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetSupervisorsByDivisionId/{divisionId}")]
        public ProcessResultViewModel<List<SupervisorViewModel>> GetSupervisorsByDivisionId([FromRoute]int divisionId) =>
            _supervisorService.GetSupervisorsByDivisionId(divisionId);

        [HttpGet]
        [Route("GetSupervisorByDivisionId/{divisionId}")]
        public ProcessResultViewModel<SupervisorViewModel> GetSupervisorByDivisionId([FromRoute]int divisionId) =>
            _supervisorService.GetSupervisorByDivisionId(divisionId);

        [HttpGet]
        [Route("GetSupervisorByUserId/{userId}")]
        public ProcessResultViewModel<SupervisorViewModel> GetSupervisorByUserId([FromRoute] string userId) =>
            _supervisorService.GetSupervisorByUserId(userId);

        [HttpPut]
        [Route("Block/{id}")]
        public virtual ProcessResult<bool> Block([FromRoute]int id)
        {
            var user = manger.Get(id);
            if (user.Data == null)
            {
                return ProcessResultHelper.Failed<bool>(false, null, SharedResource.UserNotFound);
            }

            var IdentityBlock = identityUserService.ToggleActivation(user.Data.UserId);
            if (IdentityBlock.Result.Data)
            {
                var result = manger.LogicalDeleteById(id);
                return result;
            }

            return ProcessResultHelper.Failed<bool>(false, null, SharedResource.General_SomethingWrongHappen + SharedResource.BlockUser);
        }

        [HttpGet]
        [Route("Deprecated")]
        public override ProcessResultViewModel<List<SupervisorViewModel>> Get()
        {
            return base.Get();
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ProcessResultViewModel<List<SupervisorViewModel>>> GetAsync()
        {
            var users = base.Get();
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }

        [HttpGet]
        [Route("WithBlockedDeprecated")]
        public override ProcessResultViewModel<List<SupervisorViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        [HttpGet]
        [Route("GetAllWithBlocked")]
        public async Task<ProcessResultViewModel<List<SupervisorViewModel>>> GetAllWithBlockedAsync()
        {
            var users = base.GetAllWithBlocked();
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }



        //validation 
        //New  Api To Get Small Data About  Supervisor By Division Id 
        [HttpGet]
        [Route("GetSupervisorsDataByDivisionId/{divisionId}")]
        public async Task<ProcessResultViewModel<List<UserDataViewModel>>> GetSupervisorsDataByDivisionId([FromRoute]int divisionId)
        {
            try
            {
                var entityResult = manger.GetAll(x => x.DivisionId == divisionId, x => x.User);
                if (entityResult.Data != null && entityResult.Data.Count > 0)
                {
                    var result = entityResult.Data.Select(x => new UserDataViewModel
                    {
                        Id = x.Id,
                        FullName = x.User?.FullName ?? "",
                        UserId = x.UserId
                    });

                    return ProcessResultViewModelHelper.Succedded<List<UserDataViewModel>>(result.ToList());
                }
                else
                {
                    return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, string.Format(SharedResource.General_No_Entity_Found , SharedResource.Entity_Supervisor)  );
                }
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<UserDataViewModel>>(null, ex.Message);
            }
        }



        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("UpdateAsync")]
        public async Task<ProcessResultViewModel<bool>> PutAsync([FromBody] SupervisorViewModel model)
        {
            ProcessResultViewModel<bool> resultViewModel = new ProcessResultViewModel<bool>();
            try
            {
                var oldObject = await _context.Supervisors.FirstOrDefaultAsync(x => x.Id == model.Id);
                if (model.ManagerId != oldObject.ManagerId || model.DivisionId != oldObject.DivisionId)
                {
                    var DispUserCount = oldObject.Dispatchers.Count();
                    var SupervisorOrdersCount = oldObject.Orders.Count(x =>
                        x.StatusId != (int)OrderStatusEnum.Cancelled && x.StatusId != (int)OrderStatusEnum.Compelete);
                    if (DispUserCount > 0 && SupervisorOrdersCount == 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false,SharedResource.NoChangeSupDivHasDispUser , ProcessResultStatusCode.AssignedForWork);
                    }
                    if (SupervisorOrdersCount > 0 && DispUserCount == 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeSupDivHasCurOrder  , ProcessResultStatusCode.HasChilds);

                    }
                    if (DispUserCount > 0 && SupervisorOrdersCount > 0)
                    {
                        return ProcessResultViewModelHelper.Failed<bool>(false, SharedResource.NoChangeSupDivCurOrderDispUsr, ProcessResultStatusCode.HasChilds);

                    }
                }
                else
                {
                    return base.Put(model);
                }
                return resultViewModel;
            }

            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<bool>(false, ex.Message);
            }
        }
    }
}