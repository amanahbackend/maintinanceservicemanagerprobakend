﻿using AutoMapper;
using DispatchProduct.Controllers;
using DispatchProduct.RepositoryModule;

using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using Utilities.ProcessingResult;
using Ghanim.API.ServiceCommunications.DataManagementService.DivisionService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.Models.Identity.Static;
using Microsoft.AspNetCore.Authorization;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class DivisionController : BaseAuthforAdminController<IDivisionManager, Division, DivisionViewModel>
    {
        IServiceProvider _serviceprovider;
        IDivisionManager manager;
        ISupervisorService _supervisorService;
        IProcessResultMapper processResultMapper;
        private readonly IDivisionService _divisionService;
        public DivisionController(ISupervisorService supervisorService, IServiceProvider serviceprovider, IDivisionManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper, ICached<DivisionViewModel> DivisionCached) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper, DivisionCached)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
            _supervisorService = supervisorService;
        }
        private ILang_DivisionManager Lang_Divisionmanager
        {
            get
            {
                return _serviceprovider.GetService<ILang_DivisionManager>();
            }
        }
        private ISupportedLanguagesManager SupportedLanguagesmanager
        {
            get
            {
                return _serviceprovider.GetService<ISupportedLanguagesManager>();
            }
        }

        [HttpGet]
        [Route("GetDivisionByLanguage")]
        public ProcessResultViewModel<DivisionViewModel> GetDivisionByLanguage([FromQuery]int DivisionId, string Language_code)
        {
            var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
            var Lang_DivisionRes = Lang_Divisionmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
            //var entityResult = manager.Get(DivisionId);
            var entityResult = base.Get(DivisionId);

            entityResult.Data.Name = Lang_DivisionRes.Data.Name;
            //var result = processResultMapper.Map<Division, DivisionViewModel>(entityResult);
            //return result;
            return entityResult;
        }

        [HttpGet]
        [Route("GetByName/{name}")]
        public async System.Threading.Tasks.Task<ProcessResultViewModel<DivisionViewModel>> GetByNameAsync([FromRoute] string name)
            => await _divisionService.GetByName(name);
        [Route("GetAll")]
        [HttpGet]
        public override ProcessResultViewModel<List<DivisionViewModel>> Get()
        {
            //var entityResult = manger.GetAll();
            //HttpContext.Request.GetTypedHeaders().AcceptLanguage;
            //var header = helper.GetAuthHeader(Request);
            string languageIdValue = HttpContext.Request.Headers["LanguageId"];
            //languageIdValue = "1";
            //var actions = manager.GetAll();
            var actions = base.Get();

            //FillCurrentUser(entityResult);
            if (languageIdValue != null && languageIdValue != "")
            {
                foreach (var item in actions.Data)
                {
                    var AreaLocalizesRes = Lang_Divisionmanager.Get(x => x.FK_SupportedLanguages_ID == int.Parse(languageIdValue) && x.FK_Division_ID == item.Id);
                    if (AreaLocalizesRes != null && AreaLocalizesRes.Data != null)
                    {
                        item.Name = AreaLocalizesRes.Data.Name;
                    }
                    SupervisorViewModel result = _supervisorService.GetSupervisorByDivisionId(item.Id).Data;
                    item.CurrentUserId = result?.Name;
                }
            }
            //if (actions.IsSucceeded && actions.Data.Count > 0)
            //{
            //    var result = mapper.Map<List<Division>, List<DivisionViewModel>>(actions.Data);
            //    //return processResultMapper.Map<Areas, AreasViewModel>(entityResult);
            //    //return ProcessResultHelper.Succedded<List<AreasViewModel>>(result);
            //}
            return actions;
        }

        [HttpPost]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Add")]
        public override ProcessResultViewModel<DivisionViewModel> Post([FromBody] DivisionViewModel model)
        {
            if (model == null)
                return ProcessResultViewModelHelper.Failed<DivisionViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);

            var entity = manager.Get(
                x => x.Name.ToLower() == model.Name.ToLower()
            )?.Data;
            if (entity != null)
                return ProcessResultViewModelHelper.Failed<DivisionViewModel>(null, "Item Already Existed With That Name or Code", ProcessResultStatusCode.InvalidValue);
            return base.Post(model);
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("Update")]
        public override ProcessResultViewModel<bool> Put([FromBody] DivisionViewModel model)
        {
            var entity = manager.Get(x => x.Name.ToLower() == model.Name.ToLower())?.Data;
            if (entity == null || entity != null && entity.Id == model.Id)
                return base.Put(model);
            else
                return new ProcessResultViewModel<bool>("ProcessResultViewModel");
        }
    }
}
