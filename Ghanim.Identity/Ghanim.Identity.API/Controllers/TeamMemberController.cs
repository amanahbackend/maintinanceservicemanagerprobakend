﻿using AutoMapper;
using CommonEnums;
using DispatchProduct.Controllers;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;

using Ghanim.BLL.IManagers;
using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.ResourceLibrary.Resources;
using Microsoft.AspNetCore.Authorization;
using Ghanim.Models.Identity.Static;
using Microsoft.EntityFrameworkCore;

namespace Ghanim.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TeamMemberController : BaseAuthforAdminController<ITeamMemberManager, TeamMember, TeamMemberViewModel>
    {
        public ITeamMemberManager manger;
        public readonly new IMapper mapper;
        IServiceProvider serviceProvider;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        private readonly IIdentityService identityUserService;
        INotificationService notificationService;
        private readonly ITeamMembersService _teamMembersService;
        //INotificationCenterManager notificationCenterManageManager;
        private readonly ITeamMembersHistoryManager _ITeamMembersHistoryManager;
        public TeamMemberController(IServiceProvider _serviceProvider,
            ITeamMemberManager _manger,
            IMapper _mapper,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            INotificationService _notificationService,
            ITeamMembersService teamMembersService,
            //INotificationCenterManager _notificationCenterManageManager,
            IIdentityService _identityUserService,
            ITeamMembersHistoryManager ITeamMembersHistoryManager
            ) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            manger = _manger;
            mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            identityUserService = _identityUserService;
            serviceProvider = _serviceProvider;
            notificationService = _notificationService;
            _teamMembersService = teamMembersService;
            _ITeamMembersHistoryManager = ITeamMembersHistoryManager;
            //notificationCenterManageManager = _notificationCenterManageManager;
        }

        private ITeamManager teamManager
        {
            get
            {
                return serviceProvider.GetService<ITeamManager>();
            }
        }
        private IVehicleManager vehicleManager
        {
            get
            {
                return serviceProvider.GetService<IVehicleManager>();
            }
        }

        private ITechnicianManager technicianManager
        {
            get
            {
                return serviceProvider.GetService<ITechnicianManager>();
            }
        }

        [HttpGet]
        [Route("GetUnassignedMember")]
        [Authorize(Roles = StaticRoles.Admin)]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetUnassignedMember()
        {
            var entityResult = manger.GetUnassignedMember();
            var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetUnassignedMemberByDivisionId/{divisionId}")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetUnassignedMemberByDivisionId([FromRoute]int? divisionId)
        {
            try
            {
                var entityResult = manger.GetUnassignedMemberByDivisionId((int)divisionId);
                var result = await entityResult.Data.ProjectTo<TeamMemberViewModel>().ToListAsync();
                return ProcessResultViewModelHelper.Succedded(result);
            }
            catch (Exception ex)
            {
                return ProcessResultViewModelHelper.Failed<List<TeamMemberViewModel>>(null, ex.Message);
            }
        }

        private string FlattenException(Exception exception, string Method)
        {
            string logFormat;
            logFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString();
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(logFormat);
            stringBuilder.AppendLine("********************************");

            var st = new StackTrace(exception, true);
            var frame = st.GetFrame(0);
            stringBuilder.AppendLine("Line Number: " + frame.GetFileLineNumber());
            stringBuilder.AppendLine("File Name: " + frame.GetFileName());
            stringBuilder.AppendLine("Method Name: " + frame.GetMethod());
            stringBuilder.AppendLine(Method);

            while (exception != null)
            {
                stringBuilder.AppendLine("-------------------------------------");
                stringBuilder.AppendLine("\n Message=" + exception.Message);
                stringBuilder.AppendLine("\n StackTrace=" + exception.StackTrace);
                stringBuilder.AppendLine("\n Exception=" + exception.ToString());
                stringBuilder.AppendLine("----------------------------------------------------------");
                exception = exception.InnerException;
            }
            stringBuilder.AppendLine("**************END******************");
            return stringBuilder.ToString();
        }

        [HttpGet]
        [Route("GetUnassignedVehicleMember")]
        [Authorize(Roles = StaticRoles.Admin)]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetUnassignedVehicleMember()
        {
            var entityResult = manger.GetUnassignedVehicleMember();
            var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("GetMembersByTeamId/{teamId}")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetMembersByTeamId([FromRoute]int teamId) =>
            _teamMembersService.GetMembersByTeamId(teamId);

        [HttpGet]
        [Route("GetTeamIdByTechnicianMember/{Id}")]
        [Authorize(Roles = StaticRoles.Admin)]
        public ProcessResultViewModel<List<TeamMemberViewModel>> GetTeamIdByTechnicianMember([FromRoute]int Id) =>
            _teamMembersService.GetTeamIdByTechnicianMember(Id);

        private async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> BindingTechnicianDataToMember(ProcessResultViewModel<List<TeamMemberViewModel>> models, ProcessResult<List<TeamMember>> enitityModel)
        {
            var userIds = models.Data.Select(x => x.UserId).ToList();
            var identity = await identityUserService.GetByUserIds(userIds);

            for (int i = 0; i < models.Data.Count; i++)
            {
                if (models.Data[i].MemberTypeName == "Technician")
                {
                    var technician = technicianManager.Get(enitityModel.Data[i].MemberParentId);
                    var identityUser = identity.Data.FirstOrDefault(x => x.Id == technician.Data.UserId);

                    if (technician.IsSucceeded && technician.Data != null)
                    {
                        //todo mapping infects erorrs
                        //models.Data[i].CostCenter = technician.Data.CostCenterName;
                        models.Data[i].CostCenterId = technician.Data.CostCenterId;
                        models.Data[i].DivisionId = technician.Data.DivisionId;
                        //models.Data[i].DivisionName = technician.Data.DivisionName;
                        models.Data[i].PF = technician.Data.User.PF;
                        models.Data[i].UserId = technician.Data.UserId;
                        models.Data[i].MemberName = identityUser?.FirstName;
                        models.Data[i].MemberPhone = identityUser?.Phone;
                        models.Data[i].FullName = identityUser?.FullName;
                    }
                }
            }
            return models;
        }
        [HttpGet]
        [Route("GetMemberUsersByTeamId/{teamId}")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMemberUsersByTeamId(int teamId) =>
            await _teamMembersService.GetMemberUsersByTeamId(teamId);
        //{

        //    var members = manger.GetAll(x => x.TeamId == teamId && x.MemberType != (int)MemberType.Vehicle).Data;

        //    if (members.Count > 0)
        //    {
        //        var result = mapper.Map<List<TeamMember>, List<TeamMemberUserViewModel>>(members);
        //        for (int i = 0; i < result.Count; i++)
        //        {
        //            if (result[i].MemberTypeName == "Technician")
        //            {
        //                var technician = technicianManager.Get(result[i].MemberParentId);
        //                result[i].UserId = technician.Data.UserId;
        //                result[i].PF = technician.Data.PF;
        //                result[i].CostCenter = technician.Data.CostCenterName;
        //                result[i].CostCenterId = technician.Data.CostCenterId;
        //            }
        //            else
        //            {

        //            }
        //        }
        //        return ProcessResultHelper.Succedded<List<TeamMemberUserViewModel>>(result);
        //    }
        //    else
        //    {
        //        return ProcessResultHelper.Succedded<List<TeamMemberUserViewModel>>(null, "this Order 's Team has No members To Assign tasks to them ");
        //    }
        //}

        //[HttpPut]
        //[Route("assignedMemberToTeam/{teamId}")]
        //public ProcessResultViewModel<bool> AssignedMemberToTeam([FromRoute] int teamId, int memberId)
        //{
        //    ProcessResultViewModel<bool> result = new ProcessResultViewModel<bool> { Data = false }; var memberRes = manger.Get(memberId);
        //    var teamRes = teamManager.Get(teamId);
        //    var technicianRes = technicianManager.Get(memberRes.Data.MemberParentId);

        //    if (memberRes.Data.MemberType == MemberType.Technician)
        //    {
        //        if (teamRes != null && teamRes.Data != null && technicianRes != null && technicianRes.Data != null)
        //        {
        //            if (teamRes.Data.DivisionId == technicianRes.Data.DivisionId)
        //            {
        //                var entityResult = manger.AssignedMemberToTeam(teamId, memberId);
        //                result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
        //            }
        //            else
        //            {
        //                return ProcessResultViewModelHelper.Failed(false, "Technician and Team Should be in the same Division.");

        //            }
        //        }
        //        else
        //        {
        //            return ProcessResultViewModelHelper.Failed(false, "There isn't team with this teamId or there  isn't technician with this technicianId.");

        //        }
        //    }
        //    else if (memberRes.Data.MemberType == MemberType.Vehicle)
        //    {
        //        var vehicleMemberRes = manger.GetByMemberType_TeamId(teamId, (int)memberRes.Data.MemberType);
        //        var vehicleRes = vehicleManager.Get(memberRes.Data.MemberParentId);
        //        // there is Vehicle in this team
        //        if (vehicleMemberRes != null && vehicleMemberRes.Data != null)
        //        {
        //            return ProcessResultViewModelHelper.Failed(false, "There is another Vehicle for this team.");
        //        }
        //        else
        //        {
        //            // no Vehicle in team 
        //            if (teamRes != null && teamRes.Data != null && vehicleRes != null && vehicleRes.Data != null)
        //            {
        //                var entityResult = manger.AssignedMemberToTeam(teamId, memberId);
        //                if (entityResult.Data)
        //                {
        //                    vehicleRes.Data.Isassigned = true;
        //                    var vehicleUpdateRes = vehicleManager.Update(vehicleRes.Data);
        //                }
        //                result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
        //            }
        //            else
        //            {
        //                return ProcessResultViewModelHelper.Failed(false, "There isn't team with this teamId or there  isn't Vehicle with this Plate No.");
        //            }
        //        }
        //    }
        //    return result;
        //}

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("assignedMembersToTeam/{teamId}/{defaultMemberId}")]
        public ProcessResultViewModel<bool> AssignedMembersToTeam([FromRoute] int teamId, [FromRoute] int defaultMemberId, [FromBody] List<int> membersIds)
        {
            ProcessResultViewModel<bool> result = new ProcessResultViewModel<bool> { Data = false };
            // check if there are old members
            var oldMembersRes = GetMembersByTeamId(teamId);
            var oldMembersIds = new List<int>();
            var teamRes = teamManager.Get(teamId);
            membersIds = membersIds ?? new List<int>();
            // unassign old members
            if (oldMembersRes != null && oldMembersRes.Data.Count > 0)
            {
                oldMembersIds = oldMembersRes.Data.Select(x => x.Id).ToList();
                var deletedMembers = oldMembersIds.Except(membersIds).ToList();
                var membersRes = manger.GetAll(x => deletedMembers.Contains(x.Id));
                //await NotifyTeamMembers(false, teamId, teamRes.Data.Name, membersRes.Data.Select(x => x.UserId).ToList());
                var UnassignedMembersRes = _teamMembersService.UnassignedMembers(oldMembersIds);
            }
            // assign new members to team
            foreach (var memberId in membersIds)
            {
                var memberRes = manger.Get(memberId);
                var technicianRes = technicianManager.Get(memberRes.Data.MemberParentId);
                if (memberRes.Data.MemberType == MemberType.Technician || memberRes.Data.MemberType == MemberType.Driver)
                {
                    //if (!oldMembersIds.Contains(memberId))
                    //{
                    //    await NotifyTeamMembers(true, teamId, teamRes.Data.Name, new List<string> { memberRes.Data.UserId });
                    //}

                    if (teamRes != null && teamRes.Data != null && technicianRes != null && technicianRes.Data != null)
                    {
                        if (teamRes.Data.DivisionId == technicianRes.Data.DivisionId)
                        {
                            var entityResult = manger.AssignedMemberToTeam(teamId, memberId, defaultMemberId == memberId);
                            if (entityResult.Data)
                            {
                                technicianRes.Data.TeamId = teamId;
                                var technicianUpdateRes = technicianManager.Update(technicianRes.Data);
                            }
                            result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed(false, SharedResource.TechTeamMustSameDiv);

                        }
                    }
                    else
                    {
                        return ProcessResultViewModelHelper.Failed(false, SharedResource.NoTeamThisId + SharedResource.General_OR + SharedResource.NoTechnicianThisId  );

                    }
                }
                else if (memberRes.Data.MemberType == MemberType.Vehicle)
                {
                    var vehicleMemberRes = manger.GetByMemberType_TeamId(teamId, (int)memberRes.Data.MemberType);
                    var vehicleRes = vehicleManager.Get(memberRes.Data.MemberParentId);
                    // there is Vehicle in this team
                    if (vehicleMemberRes != null && vehicleMemberRes.Data != null)
                    {
                        return ProcessResultViewModelHelper.Failed(false, SharedResource.AnotherVehicThisTeam);
                    }
                    else
                    {
                        // no Vehicle in team 
                        if (teamRes != null && teamRes.Data != null && vehicleRes != null && vehicleRes.Data != null)
                        {
                            var entityResult = manger.AssignedMemberToTeam(teamId, memberId,defaultMemberId == memberId);
                            if (entityResult.Data)
                            {
                                 
                                vehicleRes.Data.Isassigned = true;
                                vehicleRes.Data.TeamId = teamId;
                                var vehicleUpdateRes = vehicleManager.Update(vehicleRes.Data);
                            }
                            result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
                        }
                        else
                        {
                            return ProcessResultViewModelHelper.Failed(false, SharedResource.NoTeamThisId +SharedResource.General_OR +SharedResource.NoVehicleThisId);
                        }
                    }
                }
            }
            return result;
        }

        private async Task<ProcessResultViewModel<string>> NotifyTeamMembers(bool assign, int teamId, string teamName, List<string> teamMembers)
        {
            ProcessResultViewModel<string> notificationRes = null;
            List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
            if (teamMembers != null && teamMembers.Count > 0)
            {
                foreach (var item in teamMembers)
                {
                    var identity = await identityUserService.GetById(item);

                    notificationModels.Add(new SendNotificationViewModel()
                    {
                        UserId = item,
                        Title = assign ? "Assign To New Team" : "Unassign from Team",
                        Body = assign ? $"You have been assinged to team {teamName}" : $"You have been unassinged from team {teamName}",
                        Data = new { TeamId = assign ? teamId : 0, NotificationType = "Assign Member To Team" }
                    });
                    //notificationCenterManageManager.Add(new NotificationCenter()
                    //{
                    //    RecieverId = item,
                    //    Title = "New Order",
                    //    Body = $"Dispatcher {identity.Data?.FullName} assigned New order No.",
                    //    Data = teamId.ToString(),
                    //    NotificationType = "New Order",
                    //    IsRead = false
                    //});
                }

                notificationRes = await notificationService.SendToMultiUsers(notificationModels);
            }
            return notificationRes;
        }

        [HttpPut]
        [Authorize(Roles = StaticRoles.Admin)]
        [Route("UnassignedMember")]
        public ProcessResultViewModel<bool> ReassignTeamToDispatcher([FromQuery] int memberId)
        {
            ProcessResultViewModel<bool> result = null; ;

            var memberRes = manger.Get(memberId);
            #region LogTeamMember
            TeamMembersHistory logMember = new TeamMembersHistory();
            logMember.PrevTeamId = memberRes.Data.TeamId;
            logMember.TeamId = null;
            logMember.UserId = memberRes.Data.UserId;
            logMember.UserDetailId = memberRes.Data.MemberParentId;
            logMember.UserTypeId = memberRes.Data.UserTypeId;
            if (memberRes.Data.MemberType == MemberType.Vehicle)
            {
                logMember.VehicleId = memberRes.Data.MemberParentId;
            }
            _ITeamMembersHistoryManager.Add(logMember);
            #endregion
            if (memberRes != null && memberRes.Data != null)
            {
                memberRes.Data.TeamId = null;

                if (memberRes.Data.MemberType == MemberType.Technician || memberRes.Data.MemberType == MemberType.Driver)
                {
                    var technicianMemberRes = technicianManager.Get(memberRes.Data.MemberParentId);
                    if (technicianMemberRes != null && technicianMemberRes.Data != null)
                    {
                        technicianMemberRes.Data.TeamId = null;
                        var vehicleResult = technicianManager.Update(technicianMemberRes.Data);
                    }
                }
                else
                {
                    memberRes.Data.TeamId = null;
                    var vehicleMemberRes = vehicleManager.Get(memberRes.Data.MemberParentId);

                    vehicleMemberRes.Data.Isassigned = false;
                    vehicleMemberRes.Data.TeamId = null;
                    var vehicleResult = vehicleManager.Update(vehicleMemberRes.Data);
                }

                var entityResult = manger.Update(memberRes.Data);
                result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            }

            return result;
        }

        [HttpPut]
        [Route("UnassignedMembers")]
        [Authorize(Roles = StaticRoles.Admin)]
        public ProcessResultViewModel<bool> UnassignedMembers([FromBody] List<int> membersIds)
            => _teamMembersService.UnassignedMembers(membersIds);

        [HttpPut]
        [Route("SortMemeber/{teamId}")]
        public ProcessResultViewModel<bool> SortMemeber([FromRoute]int teamId, [FromBody] List<TeamMemberRanks> teamMembers)
        {
            ProcessResultViewModel<bool> result;
            var entityResult = manger.SortMemeber(teamId, teamMembers);
            result = processResultMapper.Map<bool, bool>(entityResult, (ProcessResultViewModel<bool>)null);
            return result;
        }

        [HttpGet]
        [Route("SearchVec")]
        public ProcessResultViewModel<List<TeamMemberViewModel>> SearchVec([FromQuery]string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return ProcessResultViewModelHelper.Succedded(new List<TeamMemberViewModel>(), SharedResource.WordCannotEmpty);
            }
            else
            {
                var entityResult = manger.SearchVec(word);
                var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
                return result;
            }
        }


        [HttpGet]
        [Route("SearchTech")]
        [Authorize(Roles = StaticRoles.Admin)]
        public ProcessResultViewModel<List<TeamMemberViewModel>> SearchTech([FromQuery]string word, [FromQuery]int division)
        {
            var entityResult = manger.SearchTech(word, division);
            var result = processResultMapper.Map<List<TeamMember>, List<TeamMemberViewModel>>(entityResult);
            return result;
        }

        [HttpGet]
        [Route("Deprecated")]
        [Authorize(Roles = StaticRoles.Admin)]
        public override ProcessResultViewModel<List<TeamMemberViewModel>> Get()
        {
            return base.Get();
        }

        [HttpGet]
        [Route("GetAll")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetAsync()
        {
            var users = base.Get();
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }

        [HttpGet]
        [Route("WithBlockedDeprecated")]
        [Authorize(Roles = StaticRoles.Admin)]
        public override ProcessResultViewModel<List<TeamMemberViewModel>> GetAllWithBlocked()
        {
            return base.GetAllWithBlocked();
        }

        [HttpGet]
        [Route("GetAllWithBlocked")]
        [Authorize(Roles = StaticRoles.Admin)]
        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetAllWithBlockedAsync()
        {
            var users = base.GetAllWithBlocked();
            var identity = await identityUserService.GetByUserIds(users.Data.Select(x => x.UserId).ToList());

            foreach (var item in users.Data)
            {
                item.FullName = identity.Data.Where(x => x.Id == item.UserId).Select(q => q.FullName).FirstOrDefault();
            }

            return users;
        }
    }
}