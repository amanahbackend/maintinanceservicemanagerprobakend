﻿using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.API.Utilities.HangFireServices
{
    public interface IHangFireJobService
    {
        //ProcessResult<bool> SendIdeleBreakAlert();
       // Task<ProcessResult<bool>> Hangfire();
        Task<ProcessResult<bool>> RegisterIdleBreakNotifications();
        
    }
}
