﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CommonEnum;
using Ghanim.API.ServiceCommunications.Notification;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ServiceCommunications.UserManagementService.EngineerService;
using Ghanim.API.ServiceCommunications.UserManagementService.ForemanService;
using Ghanim.API.ServiceCommunications.UserManagementService.IdentityService;
using Ghanim.API.ServiceCommunications.UserManagementService.ManagerService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamMembersService;
using Ghanim.API.ServiceCommunications.UserManagementService.TeamService;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianService;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Entities;
using Ghanim.API.Helper;
using Ghanim.API.ServiceCommunications.UserManagementService.TechnicianStateService;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using Hangfire;
using Utilites.ProcessingResult;
using Ghanim.Models.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.OData.Query.SemanticAst;
using CommonEnums;

namespace Ghanim.API.Utilities.HangFireServices
{

    public class HangFireJobService : IHangFireJobService
    {
        public IOrderSettingManager settingManager;
        public IOrderActionManager actionManager;

        public readonly IMapper mapper;
        private readonly IHostingEnvironment _hostingEnv;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        IDispatcherService dispatcherService;
        IForemanService foremanService;
        ISupervisorService supervisorService;
        ITeamMembersService teamMembersService;
        INotificationService notificationService;
        ISMSService smsService;
        ITeamService teamService;
        IIdentityService identityService;
        IForemanService formanService;
        IEngineerService engineerService;
        IManagerService managerService;
        IArchivedOrderFileManager archivedOrderFileManager;
        ITechnicianService technicianService;
        IAPIHelper helper;
        ITechnicianStateService technicianStateService;
        INotificationCenterManager notificationCenterManageManager;
        private readonly ApplicationDbContext _context;
        public HangFireJobService(IServiceProvider serviceprovider, IHostingEnvironment hostingEnv, IOrderManager _manager,
            IOrderSettingManager _settingManager,

            IOrderActionManager _actionManager,
            IAPIHelper _helper,
            IMapper _mapper,
            INotificationCenterManager _notificationCenterManageManager,
            IProcessResultMapper _processResultMapper,
            IProcessResultPaginatedMapper _processResultPaginatedMapper,
            ITeamMembersService _teamMembersService,
            IDispatcherService _dispatcherService,
            IForemanService _foremanService,
            INotificationService _notificationService,
            ITeamService _teamService,
            IIdentityService _identityService,
            ISupervisorService _supervisorService,
            IForemanService _formanService,
            IEngineerService _engineerService,
            IManagerService _managerService,
            IArchivedOrderFileManager _archivedOrderFileManager,
            ITechnicianService _technicianService,
            ITechnicianStateService _technicianStateService,
             ApplicationDbContext context,
            ISMSService _smsService
            )
        {
            _hostingEnv = hostingEnv;
            settingManager = _settingManager;
            actionManager = _actionManager;
            mapper = _mapper;
            helper = _helper;
            notificationCenterManageManager = _notificationCenterManageManager;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;
            dispatcherService = _dispatcherService;
            foremanService = _foremanService;
            teamMembersService = _teamMembersService;
            notificationService = _notificationService;
            teamService = _teamService;
            supervisorService = _supervisorService;
            identityService = _identityService;
            formanService = _formanService;
            engineerService = _engineerService;
            managerService = _managerService;
            technicianService = _technicianService;
            technicianStateService = _technicianStateService;
            smsService = _smsService;
            archivedOrderFileManager = _archivedOrderFileManager;
            _context = context;
        }
        //public ProcessResult<bool> SendIdeleBreakAlert()
        //{
        //    var jobId = BackgroundJob.Schedule(
        //    () => Hangfire(), TimeSpan.FromSeconds(2));
        //    //var result = await Hangfire();
        //    return ProcessResultHelper.Succedded<bool>(true);
        //}

        //public async Task<ProcessResult<bool>> Hangfire()
        //{
        //    var orderActions = actionManager.GetAll();
        //    var descOrderActions = orderActions.Data.Where(x => x.TeamId != 0).ToList();
        //    //var descOrderActions = orderActions.Data.Where(x => x.WorkingTypeName=="Idle"&&x.TeamId!=0).ToList();
        //    List<OrderActionViewModel> OrderActionViewModelLst = new List<OrderActionViewModel>();
        //    //var xx = descOrderActions.GroupBy(x=>x.TeamId).ToList();
        //    var group = descOrderActions.GroupBy(x => x.TeamId).Select(grp => new { GroupID = grp.Key, TeamList = grp.ToList() }).ToList();
        //    foreach (var item in group)
        //    {
        //        var lastAction = mapper.Map<OrderAction, OrderActionViewModel>(item.TeamList.LastOrDefault());
        //        for (var i = 0; i < item.TeamList.Count; i++)
        //        {
        //            var PreviousAction = item.TeamList[item.TeamList.Count - 1];
        //            //var LastTime = new TimeSpan(lastAction.ActionTime.Value.Add(TimeSpan.FromDays(lastAction.ActionTimeDays != null ? lastAction.ActionTimeDays.Value : 0)).Ticks);
        //            var AlertTime = TimeSpan.FromMinutes(30);
        //            if (lastAction.WorkingTypeId == (int)TimeSheetType.Idle || lastAction.WorkingTypeId == (int)TimeSheetType.Break)
        //            {
        //                var LastTime = DateTime.Now - PreviousAction.ActionDate;
        //                if (LastTime > AlertTime)
        //                {
        //                    var NotificationRes = await NotifyDispatcherWithTechnicianLate(lastAction.TeamId.Value);
        //                }
        //                return ProcessResultHelper.Succedded<bool>(true);
        //            }
        //        }
        //        //foreach (var groupItem in item.TeamList)
        //        //{
        //        //    var lastAction = mapper.Map<OrderAction, OrderActionViewModel>(actions.Data.LastOrDefault());
        //        //}
        //    }
        //    //for (int i = 0; i < group.Count; i++)
        //    //{
        //    //    var AlertTime = TimeSpan.FromMinutes(7);
        //    //    if (OrderActionViewModelLst[i].ActionTime != null)
        //    //    {
        //    //        var LastTime = new TimeSpan(OrderActionViewModelLst[i].ActionTime.Value.Add(TimeSpan.FromDays(OrderActionViewModelLst[i].ActionTimeDays != null ? OrderActionViewModelLst[i].ActionTimeDays.Value : 0)).Ticks);
        //    //        if (LastTime > AlertTime)
        //    //            if (OrderActionViewModelLst[i].WorkingTypeId == (int)TimeSheetType.Idle || OrderActionViewModelLst[i].WorkingTypeId == (int)TimeSheetType.Break)
        //    //            {
        //    //                var order = Get(OrderActionViewModelLst[i].OrderId).Data;
        //    //                var NotificationRes = await NotifyDispatcherWithTechnicianLate(22);
        //    //                return ProcessResultHelper.Succedded<bool>(true);
        //    //            }
        //    //    }
        //    //    //var Time = action.ActionTime.Value.Subtract(TimeSpan.FromMinutes());

        //    //}

        //    return ProcessResultHelper.Succedded<bool>(true);
        //}
        //New HangFire Implementation Refactored
        public async Task<ProcessResult<bool>> RegisterIdleBreakNotifications()
        {
            //Console.Write($"Starting The RegisterIdleBreakNotifications{DateTime.Now:F}");

            return ProcessResultHelper.Succedded<bool>(true);
            var descOrderActions = _context.OrderActions.Where(x=>x.TeamId !=null &&x.TeamId!=0).ToList();
            var OrderActionsPerTeamgroup = descOrderActions.GroupBy(x => x.TeamId).Select(grp => new { TeamId = grp.Key, TeamList = grp.ToList() }).ToList();
            foreach (var teamItem in OrderActionsPerTeamgroup)
            {
                var lastAction = teamItem.TeamList.LastOrDefault();
                var AlertTime = TimeSpan.FromMinutes(30);               
                if (lastAction != null)
                {
                    if (lastAction.WorkingTypeId == (int)TimeSheetType.Idle || lastAction.WorkingTypeId == (int)TimeSheetType.Break)
                    {
                        var LastTime = lastAction.ActionTime == null ?
                                       new TimeSpan((DateTime.Now - lastAction.ActionDate).Ticks)
                                  : new TimeSpan(lastAction.ActionTime.Value.Add(TimeSpan.FromDays(lastAction.ActionTimeDays != null ? lastAction.ActionTimeDays.Value : 0)).Ticks);
                        // var LastTime = T.Minutes;
                        if (LastTime > AlertTime)
                        {
                            var NotificationRes = await NotifyDispatcherWithTechnicianLate(teamItem.TeamId.Value);
                        }
                    }
                }            
            }
           
            return ProcessResultHelper.Succedded<bool>(true);
        }



        //HangFire Notifications       
        private async Task<ProcessResultViewModel<string>> NotifyDispatcherWithTechnicianLate(int teamID)
        {
            ProcessResultViewModel<string> notificationRes = null;
            var TeamRes = teamService.GetTeamById(teamID);
            //var orderRes = manager.Get(11);
            if(TeamRes != null)
            {
                var dispatcher = dispatcherService.GetDispatcherById(TeamRes.Data.DispatcherId ?? 0);
                List<SendNotificationViewModel> notificationModels = new List<SendNotificationViewModel>();
                //SendNotificationViewModel notificationModel;
                var DispIdentity = await identityService.GetById(dispatcher.Data.UserId);

                //var userId = dispatcher.Data.UserId;
                //var DispatchIdentity = await identityService.GetById(userId);
                //var jsonOrder = JsonConvert.SerializeObject(TeamRes.Data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                // var teamMembers = await teamMembersService.GetMemberUsersByTeamId(teamID);
                var orderTeam =
              _context.Members.Include(x => x.User).ThenInclude(x => x.Technician)
                     .Where(x => x.TeamId == teamID && (x.MemberType == MemberType.Technician || x.MemberType == MemberType.Driver)).ToList();
                var teamMembers = orderTeam;
                if (teamMembers != null && teamMembers.Count > 0)
                {
                    foreach (var item in teamMembers)
                    {
                        var techIdentity = await identityService.GetById(item.UserId);
                        notificationModels.Add(new SendNotificationViewModel()
                        {
                            UserId = dispatcher.Data.UserId,
                            Title = "Technician Late",
                            Body = $"Dear {DispIdentity.Data?.FullName}  Technicain {techIdentity.Data?.FullName} Is Idle Or Break more than 30 Minutes.",
                            Data = new { OrderId = TeamRes.Data.Id, NotificationType = "Technician Late State" },
                            //Data = new { order = JsonConvert.DeserializeObject(jsonOrder), NotificationType = "Technician State" },
                            TeamId = teamID
                        });
                        notificationCenterManageManager.Add(new NotificationCenter()
                        {
                            RecieverId = dispatcher.Data.UserId,
                            Title = "Technician Late",
                            Body = $"Dear {DispIdentity.Data?.FullName}  Technicain {techIdentity.Data?.FullName} Is Idle Or Break more than 30 Minutes.",
                            //Body = $"Technician {techIdentity.Data?.FullName} requests changing the priority of order No. {orderRes.Data.Code} to be able to work on it.",
                            Data = JsonConvert.SerializeObject(teamID),
                            NotificationType = "Technician Late",
                            IsRead = false,
                            TeamId = teamID,
                            CurrentUserId = techIdentity.Data?.UserName
                        });

                        notificationRes = await notificationService.SendToMultiUsers(notificationModels);
                    }
                }
            }
           
            return notificationRes;
        }

    }

}
