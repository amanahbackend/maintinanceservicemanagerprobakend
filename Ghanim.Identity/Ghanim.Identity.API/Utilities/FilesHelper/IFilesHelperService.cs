﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities.Utilites.GenericListToExcel;

namespace Ghanim.API.Helper
{
    public interface IFilesHelperService
    {
         string GetUrlOfFile();
    }


}
