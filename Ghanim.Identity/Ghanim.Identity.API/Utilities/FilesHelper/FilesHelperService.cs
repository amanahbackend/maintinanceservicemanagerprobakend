﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;
using Utilities.Utilites.GenericListToExcel;

namespace Ghanim.API.Helper
{
    public class FilesHelperService: IFilesHelperService
    {
        private readonly IHostingEnvironment _hostingEnv;

        public FilesHelperService(IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;


        }
        public string GetUrlOfFile()
        {
            string fileNameInWWWRoute = "StaticFiles\\defaultProfilePicture.png";
            string newFile = $"{ _hostingEnv.WebRootPath}\\{fileNameInWWWRoute}";
            int ExcelSheetindex = newFile.IndexOf("\\StaticFiles\\");
            var url = new Uri(newFile);
            var absolute = url.AbsoluteUri.Substring(ExcelSheetindex);
            return absolute;
        }
    }


}
