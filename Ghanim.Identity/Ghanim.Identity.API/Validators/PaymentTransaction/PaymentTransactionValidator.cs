﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using FluentValidation;
using Ghanim.API.ViewModels.PaymentTransaction;
using Ghanim.Models.Payment.Enums;
using Ghanim.Models.Payment.Entities;
namespace Ghanim.API.Validators.PaymentTransaction
{
    public class PaymentTransactionValidator : AbstractValidator<PaymentTransactionViewModel>
    {
        public PaymentTransactionValidator()
        {
            RuleFor(x => x.PaymentMode).IsInEnum().WithMessage("Must Select PaymentMode");
            RuleFor(x => x.PaymentStatus).IsInEnum().WithMessage("Must Select PaymentStatus");
            RuleFor(x => x.Payment).NotEqual(0).WithMessage("Must Enter Payment");
            RuleFor(x => x.OrderId).NotEqual(0).WithMessage("Must Enter Order");
            RuleFor(x => x.ServiceTypeId).NotEqual(0).WithMessage("Must Enter ServiceType");


            //RuleFor(x => x.).NotEmpty();
            //RuleFor(x => x.Forename).NotEmpty().WithMessage("Please specify a first name");
            //RuleFor(x => x.Discount).NotEqual(0).When(x => x.HasDiscount);
            //RuleFor(x => x.Address).Length(20, 250);
            //RuleFor(x => x.Postcode).Must(BeAValidPostcode).WithMessage("Please specify a valid postcode");

        }
    }
}
