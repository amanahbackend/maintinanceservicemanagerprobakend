﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class ContractTypeCached : ICached<ContractTypesViewModel>
    {
        private readonly ICachedTables _cashestables;
        public ContractTypeCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }
        public List<ContractTypesViewModel> CachedGetAll()
        {
            return _cashestables.CachedContractTypes.Where(x => !x.IsDeleted).ToList();
        }

        public List<ContractTypesViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedContractTypes;
        }

        public ContractTypesViewModel CachedGetByID(int nId)
        {
            ContractTypesViewModel ContractType = _cashestables.CachedContractTypes.FirstOrDefault(c => c.Id == nId);
            if (ContractType != null)
            {
                return ContractType.Clone<ContractTypesViewModel>();
            }
            return null;
        }

        public List<ContractTypesViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedContractTypes.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public List<ContractTypesViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedContractTypes.Where(x => ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearContractTypes();
        }
    }
}
