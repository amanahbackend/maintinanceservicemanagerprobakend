﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class CostCenterCached : ICached<CostCenterViewModel>
    {
        private readonly ICachedTables _cashestables;
        public CostCenterCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }
        public List<CostCenterViewModel> CachedGetAll()
        {
            return _cashestables.CachedCostCenters.Where(x => !x.IsDeleted).ToList();
        }

        public List<CostCenterViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedCostCenters;
        }

        public CostCenterViewModel CachedGetByID(int nId)
        {
            CostCenterViewModel CostCenter = _cashestables.CachedCostCenters.FirstOrDefault(c => c.Id == nId);
            if (CostCenter != null)
            {
                return CostCenter.Clone<CostCenterViewModel>();
            }
            return null;
        }

        public List<CostCenterViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedCostCenters.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public List<CostCenterViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedCostCenters.Where(x => ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearCostCenters();
        }
    }
}
