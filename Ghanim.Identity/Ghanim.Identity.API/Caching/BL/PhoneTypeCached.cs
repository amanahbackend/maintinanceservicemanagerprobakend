﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class PhoneTypeCached : ICached<PhoneTypeViewModel>
    {
        //injections
        private readonly ICachedTables _cashestables;
        public PhoneTypeCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }


        public PhoneTypeViewModel CachedGetByID(int nId)
        {
            PhoneTypeViewModel type = _cashestables.CachedPhoneTypes.FirstOrDefault(c => c.Id == nId);
            if (type != null)
            {
                return type.Clone<PhoneTypeViewModel>();
            }
            return null;
        }

        public List<PhoneTypeViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedPhoneTypes.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearPhoneTypes();
        }



        public List<PhoneTypeViewModel> CachedGetAll()
        {
            return _cashestables.CachedPhoneTypes.Where(x => !x.IsDeleted).ToList();
        }

        public List<PhoneTypeViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedPhoneTypes;
        }

        public List<PhoneTypeViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedPhoneTypes.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
