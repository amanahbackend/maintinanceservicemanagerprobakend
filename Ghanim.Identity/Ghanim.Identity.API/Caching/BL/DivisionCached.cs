﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.Models.Entities;
using IdentityServer4.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class DivisionCached : ICached<DivisionViewModel>
    {
        private readonly ICachedTables _cashestables;
        public DivisionCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;


        }
        public List<DivisionViewModel> CachedGetAll()
        {
            return _cashestables.CachedDivisions.Where(x => !x.IsDeleted).ToList();
        }

        public List<DivisionViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedDivisions;
        }

        public DivisionViewModel CachedGetByID(int nId)
        {
            DivisionViewModel Division = _cashestables.CachedDivisions.FirstOrDefault(c => c.Id == nId);
            if (Division != null)
            {
                return Division.Clone<DivisionViewModel>();
            }
            return null;
        }

        public List<DivisionViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedDivisions.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public List<DivisionViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedDivisions.Where(x => ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearOrderStatus();
        }
    }
}
