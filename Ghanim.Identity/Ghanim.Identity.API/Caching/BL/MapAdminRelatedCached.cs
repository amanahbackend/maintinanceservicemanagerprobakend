﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Caching.Abstracts;
using Ghanim.API.Caching.CachedTablesContainers;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.API.Caching.BL
{
    public class MapAdminRelatedCached : ICached<MapAdminRelatedViewModel> 
    {
        //injections
        private readonly ICachedTables _cashestables;
        public MapAdminRelatedCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }


        public MapAdminRelatedViewModel CachedGetByID(int nId)
        {
            MapAdminRelatedViewModel MapAdminRelated = _cashestables.CachedMapAdminRelateds.FirstOrDefault(c => c.Id == nId);
            if (MapAdminRelated != null)
            {
                return MapAdminRelated.Clone<MapAdminRelatedViewModel>();
            }
            return null;
        }

      

        public List<MapAdminRelatedViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedMapAdminRelateds.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearMapAdminRelateds();
        }



        public List<MapAdminRelatedViewModel> CachedGetAll()
        {
            return _cashestables.CachedMapAdminRelateds.Where(x => !x.IsDeleted).ToList();
        }

        public List<MapAdminRelatedViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedMapAdminRelateds;
        }

        public List<MapAdminRelatedViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedMapAdminRelateds.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
