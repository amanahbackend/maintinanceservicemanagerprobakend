﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Caching.Abstracts;
using Ghanim.API.Caching.CachedTablesContainers;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.API.Caching.BL
{
    public class AreaCached : ICached<AreasViewModel>
    {
        //injections
        private readonly ICachedTables _cashestables;
        public AreaCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }


        public AreasViewModel CachedGetByID(int nId)
        {
            AreasViewModel area = _cashestables.CachedAreas.FirstOrDefault(c => c.Id == nId);
            if (area != null)
            {
                return area.Clone<AreasViewModel>();
            }
            return null;
        }

        public AreasViewModel CachedGetByCurCode(int CurCode)
        {
            AreasViewModel area = _cashestables.CachedAreas.FirstOrDefault(c => c.Area_No == CurCode);
            if (area != null)
            {
                area.Clone<AreasViewModel>();
                return area;
            }
            return null;
        }

        public List<AreasViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedAreas.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearAreas();
        }



        public List<AreasViewModel> CachedGetAll()
        {
            return _cashestables.CachedAreas.Where(x => !x.IsDeleted).ToList();
        }

        public List<AreasViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedAreas;
        }

        public List<AreasViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedAreas.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
