﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Caching.Abstracts;
using Ghanim.API.Caching.CachedTablesContainers;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.API.Caching.BL
{
    public class OrderProblemCached : ICached<OrderProblemViewModel> 
    {
        //injections
        private readonly ICachedTables _cashestables;
        public OrderProblemCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }


        public OrderProblemViewModel CachedGetByID(int nId)
        {
            OrderProblemViewModel OrderProblem = _cashestables.CachedOrderProblems.FirstOrDefault(c => c.Id == nId);
            if (OrderProblem != null)
            {
                return OrderProblem.Clone<OrderProblemViewModel>();
            }
            return null;
        }

      

        public List<OrderProblemViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedOrderProblems.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearOrderProblems();
        }



        public List<OrderProblemViewModel> CachedGetAll()
        {
            return _cashestables.CachedOrderProblems.Where(x => !x.IsDeleted).ToList();
        }

        public List<OrderProblemViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedOrderProblems;
        }

        public List<OrderProblemViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedOrderProblems.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
