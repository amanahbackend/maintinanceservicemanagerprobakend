﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.Models.Entities;
using IdentityServer4.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class CompanyCodeCached : ICached<CompanyCodeViewModel>
    {
        private readonly ICachedTables _cashestables;
        public CompanyCodeCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;


        }
        public List<CompanyCodeViewModel> CachedGetAll()
        {
            return _cashestables.CachedCompanyCodes.Where(x => !x.IsDeleted).ToList();
        }

        public List<CompanyCodeViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedCompanyCodes;
        }

        public CompanyCodeViewModel CachedGetByID(int nId)
        {
            CompanyCodeViewModel CompanyCode = _cashestables.CachedCompanyCodes.FirstOrDefault(c => c.Id == nId);
            if (CompanyCode != null)
            {
                return CompanyCode.Clone<CompanyCodeViewModel>();
            }
            return null;
        }

        public List<CompanyCodeViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedCompanyCodes.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public List<CompanyCodeViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedCompanyCodes.Where(x => ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearOrderStatus();
        }
    }
}
