﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class CustomerTypeCached : ICached<CustomerTypeViewModel>
    {
        private readonly ICachedTables _cashestables;
        public CustomerTypeCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }

        public List<CustomerTypeViewModel> CachedGetAll()
        {
            return _cashestables.CachedCustomerTypes.Where(x => !x.IsDeleted).ToList();
        }

        public List<CustomerTypeViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedCustomerTypes;
        }

        public CustomerTypeViewModel CachedGetByID(int nId)
        {
            CustomerTypeViewModel type = _cashestables.CachedCustomerTypes.FirstOrDefault(c => c.Id == nId);
            if (type != null)
            {
                return type.Clone<CustomerTypeViewModel>();
            }
            return null;
        }

        public List<CustomerTypeViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedCustomerTypes.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();

        }

        public List<CustomerTypeViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedCustomerTypes.Where(x => ids.Contains(x.Id)).ToList();

        }

        public void ClearCashed()
        {
            _cashestables.ClearCustomerTypes();

        }
    }
}
