﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Caching.Abstracts;
using Ghanim.API.Caching.CachedTablesContainers;
using Ghanim.BLL.Caching.Abstracts;

namespace Ghanim.API.Caching.BL
{
    public class OrderStatusCached : ICached<OrderStatusViewModel> 
    {
        //injections
        private readonly ICachedTables _cashestables;
        public OrderStatusCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }


        public OrderStatusViewModel CachedGetByID(int nId)
        {
            OrderStatusViewModel OrderStatus = _cashestables.CachedOrderStatus.FirstOrDefault(c => c.Id == nId);
            if (OrderStatus != null)
            {
                return OrderStatus.Clone<OrderStatusViewModel>();
            }
            return null;
        }

      

        public List<OrderStatusViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedOrderStatus.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearOrderStatus();
        }



        public List<OrderStatusViewModel> CachedGetAll()
        {
            return _cashestables.CachedOrderStatus.Where(x => !x.IsDeleted).ToList();
        }

        public List<OrderStatusViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedOrderStatus;
        }

        public List<OrderStatusViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedOrderStatus.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
