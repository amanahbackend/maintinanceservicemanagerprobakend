﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class WorkingTypeCached : ICached<WorkingTypeMiniViewModel>
    {
        //injections
        private readonly ICachedTables _cashestables;
        public WorkingTypeCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }

        public List<WorkingTypeMiniViewModel> CachedGetAll()
        {
            return _cashestables.CachedWorkingTypes.Where(x => !x.IsDeleted).ToList();
        }

        public List<WorkingTypeMiniViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedWorkingTypes;
        }

        public WorkingTypeMiniViewModel CachedGetByID(int nId)
        {
            WorkingTypeMiniViewModel WorkingType = _cashestables.CachedWorkingTypes.FirstOrDefault(c => c.Id == nId);
            if (WorkingType != null)
            {
                return WorkingType.Clone<WorkingTypeMiniViewModel>();
            }
            return null;
        }

        public List<WorkingTypeMiniViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedWorkingTypes.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public List<WorkingTypeMiniViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedWorkingTypes.Where(x => ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearOrderStatus();
        }
    }
}
