﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class MachineTypeCached : ICached<MachineTypeViewModel>
    {
        //injections
        private readonly ICachedTables _cashestables;
        public MachineTypeCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }


        public MachineTypeViewModel CachedGetByID(int nId)
        {
            MachineTypeViewModel type = _cashestables.CachedMachineTypes.FirstOrDefault(c => c.Id == nId);
            if (type != null)
            {
                return type.Clone<MachineTypeViewModel>();
            }
            return null;
        }

        public List<MachineTypeViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedMachineTypes.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearMachineTypes();
        }



        public List<MachineTypeViewModel> CachedGetAll()
        {
            return _cashestables.CachedMachineTypes.Where(x => !x.IsDeleted).ToList();
        }

        public List<MachineTypeViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedMachineTypes;
        }

        public List<MachineTypeViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedMachineTypes.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}
