﻿using Ghanim.API.Caching.Abstracts;
using Ghanim.BLL.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.Caching.BL
{
    public class OrderTypeCached : ICached<OrderTypeViewModel>
    {
        //injections
        private readonly ICachedTables _cashestables;
        public OrderTypeCached(ICachedTables cashestables)
        {
            _cashestables = cashestables;
        }

        public List<OrderTypeViewModel> CachedGetAll()
        {
            return _cashestables.CachedOrderTypes.Where(x => !x.IsDeleted).ToList();
        }

        public List<OrderTypeViewModel> CachedGetAllWithBlock()
        {
            return _cashestables.CachedOrderTypes;
        }

        public OrderTypeViewModel CachedGetByID(int nId)
        {
            OrderTypeViewModel OrderType = _cashestables.CachedOrderTypes.FirstOrDefault(c => c.Id == nId);
            if (OrderType != null)
            {
                return OrderType.Clone<OrderTypeViewModel>();
            }
            return null;
        }

        public List<OrderTypeViewModel> CachedGetByIds(List<int> ids)
        {
            return _cashestables.CachedOrderTypes.Where(x => !x.IsDeleted && ids.Contains(x.Id)).ToList();
        }

        public List<OrderTypeViewModel> CachedGetByIdsWithBlocked(List<int> ids)
        {
            return _cashestables.CachedOrderTypes.Where(x => ids.Contains(x.Id)).ToList();
        }

        public void ClearCashed()
        {
            _cashestables.ClearOrderStatus();
        }
    }
}
