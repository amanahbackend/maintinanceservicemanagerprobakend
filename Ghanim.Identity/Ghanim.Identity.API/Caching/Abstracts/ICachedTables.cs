﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Caching.BL;

namespace Ghanim.API.Caching.Abstracts
{
    public interface ICachedTables
    {
        List<AreasViewModel> CachedAreas { get; }
        void ClearAreas();

        List<OrderStatusViewModel> CachedOrderStatus { get; }
        void ClearOrderStatus();

        List<OrderProblemViewModel> CachedOrderProblems { get; }
        void ClearOrderProblems();

        List<MapAdminRelatedViewModel> CachedMapAdminRelateds { get; }
        void ClearMapAdminRelateds();

        List<OrderTypeViewModel> CachedOrderTypes { get; }
        void ClearOrderTypes();

        List<DivisionViewModel> CachedDivisions { get; }
        void ClearDivisions();

        List<CompanyCodeViewModel> CachedCompanyCodes { get; }
        void ClearCompanyCodes();

        List<CostCenterViewModel> CachedCostCenters { get; }
        void ClearCostCenters();

        List<WorkingTypeMiniViewModel> CachedWorkingTypes { get; }
        void ClearWorkingTypes();

        List<ContractTypesViewModel> CachedContractTypes{ get; }
        void ClearContractTypes();

        List<CustomerTypeViewModel> CachedCustomerTypes { get; }
        void ClearCustomerTypes();
        List<MachineTypeViewModel> CachedMachineTypes { get; }
        void ClearMachineTypes();
        List<PhoneTypeViewModel> CachedPhoneTypes { get; }
        void ClearPhoneTypes();
    }
}
