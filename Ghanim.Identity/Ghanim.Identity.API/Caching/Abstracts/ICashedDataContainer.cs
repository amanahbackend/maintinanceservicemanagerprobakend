﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Caching.BL;

namespace Ghanim.API.Caching.CachedTablesContainers
{
    public interface ICashedDataContainer
    {
        List<AreasViewModel> CachedAreasContainer { get; set; }
        List<OrderStatusViewModel> CachedOrderStatusContainer { get; set; }
        List<OrderProblemViewModel> CachedOrderProblemContainer { get; set; }
        List<MapAdminRelatedViewModel> CachedMapAdminRelatedContainer { get; set; }
        List<OrderTypeViewModel> CachedOrderTypesContainer { get; set; }
        List<DivisionViewModel> CachedDivisionsContainer { get; set; }
        List<CompanyCodeViewModel> CachedCompanyCodesContainer { get; set; }
        
        List<CostCenterViewModel> CachedCostCentersContainer { get; set; }
        List<ContractTypesViewModel> CachedContractTypesContainer { get; set; }
        List<WorkingTypeMiniViewModel> CachedWorkingTypesContainer { get;  set;}

        List<CustomerTypeViewModel> CachedCustomerTypesContainer { get; set; }
        List<MachineTypeViewModel> CachedMachineTypesContainer { get; set; }
        List<PhoneTypeViewModel> CachedPhoneTypesContainer { get; set; }


    }
}
