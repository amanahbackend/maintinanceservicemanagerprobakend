﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Ghanim.API.Caching.Abstracts;
using Ghanim.API.Caching.BL;
using Ghanim.API.Caching.CachedTablesContainers;
using Ghanim.BLL.IManagers;
using Ghanim.Models.Context;
using Ghanim.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.API.Caching
{
    public class CachedTables : ICachedTables
    {
        private readonly ICashedDataContainer _cashedDataContainer;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;
        //private readonly IServiceProvider _provider;
        //private readonly IMapper _mapper;
        private readonly object padlock = new object();

        //private ApplicationDbContext _context()
        //{

        //    var dbContext = _provider.GetRequiredService<ApplicationDbContext>();
        //    return dbContext;
        //}


        //private IMapper _mapper()
        //{
        //    var mapper = _provider.GetRequiredService<IMapper>();
        //    return mapper;

        //}
        //IServiceProvider provider,
        public CachedTables(IMapper mapper, ApplicationDbContext context, ICashedDataContainer cashedDataContainer)
        {
            //_provider = provider;
            _mapper = mapper;
            _context = context;
            _cashedDataContainer = cashedDataContainer;
        }

        public List<AreasViewModel> CachedAreas
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedAreasContainer == null)
                    {
                        var list = _context.Areas.ToList();
                        _cashedDataContainer.CachedAreasContainer = _mapper.Map<List<Areas>, List<AreasViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedAreasContainer;
            }
        }

        public void ClearAreas()
        {
            _cashedDataContainer.CachedAreasContainer = null;
        }

        public List<OrderStatusViewModel> CachedOrderStatus
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedOrderStatusContainer == null)
                    {
                        var list = _context.OrderStatuses.ToList();
                        _cashedDataContainer.CachedOrderStatusContainer = _mapper.Map<List<OrderStatus>, List<OrderStatusViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedOrderStatusContainer;
            }
        }

        public void ClearOrderStatus()
        {
            _cashedDataContainer.CachedOrderStatusContainer = null;
        }

        public List<OrderProblemViewModel> CachedOrderProblems
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedOrderProblemContainer == null)
                    {
                        var list = _context.OrderProblems.ToList();
                        _cashedDataContainer.CachedOrderProblemContainer = _mapper.Map<List<OrderProblem>, List<OrderProblemViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedOrderProblemContainer;
            }
        }


        public void ClearOrderProblems()
        {
            _cashedDataContainer.CachedOrderProblemContainer = null;
        }

        public List<OrderTypeViewModel> CachedOrderTypes
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedOrderTypesContainer == null)
                    {
                        var list = _context.OrderTypes.ToList();
                        _cashedDataContainer.CachedOrderTypesContainer = _mapper.Map<List<OrderType>, List<OrderTypeViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedOrderTypesContainer;
            }
        }

        public void ClearOrderTypes()
        {
            _cashedDataContainer.CachedOrderTypesContainer = null;
        }

        public List<DivisionViewModel> CachedDivisions
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedDivisionsContainer == null)
                    {
                        var list = _context.Division.ToList();
                        _cashedDataContainer.CachedDivisionsContainer = _mapper.Map<List<Division>, List<DivisionViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedDivisionsContainer;
            }
        }

        public void ClearDivisions()
        {
            _cashedDataContainer.CachedDivisionsContainer = null;
        }

        public List<CostCenterViewModel> CachedCostCenters
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedCostCentersContainer == null)
                    {
                        var list = _context.CostCenter.ToList();
                        _cashedDataContainer.CachedCostCentersContainer = _mapper.Map<List<CostCenter>, List<CostCenterViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedCostCentersContainer;
            }
        }

        public void ClearCostCenters()
        {
            _cashedDataContainer.CachedCostCentersContainer = null;
        }

        public List<MapAdminRelatedViewModel> CachedMapAdminRelateds
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedMapAdminRelatedContainer == null)
                    {
                        var list = _context.MapAdminRelated.ToList();
                        _cashedDataContainer.CachedMapAdminRelatedContainer = _mapper.Map<List<MapAdminRelated>, List<MapAdminRelatedViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedMapAdminRelatedContainer;
            }
        }

        public void ClearMapAdminRelateds()
        {
            _cashedDataContainer.CachedMapAdminRelatedContainer = null;
        }


        public List<CompanyCodeViewModel> CachedCompanyCodes
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedCompanyCodesContainer == null)
                    {
                        var list = _context.CompanyCode.ToList();
                        _cashedDataContainer.CachedCompanyCodesContainer = _mapper.Map<List<CompanyCode>, List<CompanyCodeViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedCompanyCodesContainer;
            }
        }


        public void ClearCompanyCodes()
        {
            _cashedDataContainer.CachedCompanyCodesContainer = null;
        }

        public List<ContractTypesViewModel> CachedContractTypes
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedContractTypesContainer == null)
                    {
                        var list = _context.ContractTypes.ToList();
                        _cashedDataContainer.CachedContractTypesContainer = _mapper.Map<List<ContractTypes>, List<ContractTypesViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedContractTypesContainer;
            }
        }

        public void ClearContractTypes()
        {
            _cashedDataContainer.CachedContractTypesContainer = null;
        }


        public List<WorkingTypeMiniViewModel> CachedWorkingTypes
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedWorkingTypesContainer == null)
                    {
                        var list = _context.WorkingTypes.ToList();
                        _cashedDataContainer.CachedWorkingTypesContainer = _mapper.Map<List<WorkingType>, List<WorkingTypeMiniViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedWorkingTypesContainer;
            }
        }


        public void ClearWorkingTypes()
        {
            _cashedDataContainer.CachedWorkingTypesContainer = null;
        }


        public List<CustomerTypeViewModel> CachedCustomerTypes
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedCustomerTypesContainer == null)
                    {
                        var list = _context.CustomerType.ToList();
                        _cashedDataContainer.CachedCustomerTypesContainer = _mapper.Map<List<CustomerType>, List<CustomerTypeViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedCustomerTypesContainer;
            }
        }

        public void ClearCustomerTypes()
        {
            _cashedDataContainer.CachedCustomerTypesContainer = null;
        }


        public List<MachineTypeViewModel> CachedMachineTypes
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedMachineTypesContainer == null)
                    {
                        var list = _context.MachineType.ToList();
                        _cashedDataContainer.CachedMachineTypesContainer = _mapper.Map<List<MachineType>, List<MachineTypeViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedMachineTypesContainer;
            }
        }

        public void ClearMachineTypes()
        {
            _cashedDataContainer.CachedMachineTypesContainer = null;
        }



        public List<PhoneTypeViewModel> CachedPhoneTypes
        {
            get
            {
                lock (padlock)
                {
                    if (_cashedDataContainer.CachedPhoneTypesContainer == null)
                    {
                        var list = _context.PhoneType.ToList();
                        _cashedDataContainer.CachedPhoneTypesContainer = _mapper.Map<List<PhoneType>, List<PhoneTypeViewModel>>(list);
                    }
                }
                return _cashedDataContainer.CachedPhoneTypesContainer;
            }
        }

        public void ClearPhoneTypes()
        {
            _cashedDataContainer.CachedPhoneTypesContainer = null;
        }
    }
}
