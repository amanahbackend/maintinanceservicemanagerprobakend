﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Caching.BL;

namespace Ghanim.API.Caching.CachedTablesContainers
{
    public class CashedDataContainer : ICashedDataContainer
    {
        public List<AreasViewModel> CachedAreasContainer { get; set; }
        public List<OrderStatusViewModel> CachedOrderStatusContainer { get; set; }
        public List<OrderProblemViewModel> CachedOrderProblemContainer { get; set; }
        public List<MapAdminRelatedViewModel> CachedMapAdminRelatedContainer { get; set; }
        public List<OrderTypeViewModel> CachedOrderTypesContainer { get; set; }
        public List<DivisionViewModel> CachedDivisionsContainer { get; set; }
        public List<CostCenterViewModel> CachedCostCentersContainer { get; set; }
        public List<ContractTypesViewModel> CachedContractTypesContainer { get; set; }



        public List<CompanyCodeViewModel> CachedCompanyCodesContainer { get; set; }
       public List<WorkingTypeMiniViewModel> CachedWorkingTypesContainer { get;  set;}

       public List<CustomerTypeViewModel> CachedCustomerTypesContainer { get; set; }
       public List<MachineTypeViewModel> CachedMachineTypesContainer { get; set; }
       public List<PhoneTypeViewModel> CachedPhoneTypesContainer { get; set; }
    }
}
