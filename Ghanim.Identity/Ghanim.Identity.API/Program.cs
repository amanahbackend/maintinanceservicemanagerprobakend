﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.SerilogExtensions;
using Ghanim.Models.Context;
using Ghanim.API.Seed;
using Ghanim.BLL.IManagers;
using Dispatching.Identity.API.Seed;
using Ghanim.API.Seed;
using Serilog;

namespace Ghanim.API
{
    public class Program
    {
        public static void Main(string[] args)
        {

            Log.Logger = new LoggerConfiguration()
                  .Enrich.With<CustomExceptionEnricher>()
                  .MinimumLevel.Error()
                  .Enrich.FromLogContext()
                  .WriteTo.File("logs/log_.csv", rollingInterval: RollingInterval.Day,
                  outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz},[{Level}],{Message},{ExceptionMessage},{ExceptionSource},{ExceptionType},{ExceptionStackTrace},{NewLine}")
                  .CreateLogger();

            BuildWebHost(args)
                //.MigrateDbContext<PersistedGrantDbContext>((_, __) => { })
                //.MigrateDbContext<ApplicationDbContext>((context, services) =>
                //{
                //    var env = services.GetService<IHostingEnvironment>();
                //    var logger = services.GetService<ILogger<ApplicationDbContextSeed>>();
                //    var appUserManager = services.GetService<IApplicationUserManager>();
                //    var appRoleManager = services.GetService<IApplicationRoleManager>();

                //    new ApplicationDbContextSeed(appUserManager, appRoleManager)
                //        .SeedAsync(context, env, logger)
                //        .Wait();
                //})
                //.MigrateDbContext<ConfigurationDbContext>((context, services) =>
                //{
                //    var configuration = services.GetService<IConfiguration>();

                //    new ConfigurationDbContextSeed()
                //        .SeedAsync(context, configuration)
                //        .Wait();
                //})
                .MigrateDbContext<ApplicationDbContext>((context, services) =>
                {
                    var env = services.GetService<IHostingEnvironment>();
                    var logger = services.GetService<ILogger<OrderSeedForApplicationDbContext>>();
                    new OrderSeedForApplicationDbContext()
                        .SeedAsync(context, env, logger)
                        .Wait();
                })




                .Run();
                
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel(o => { o.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(10); })
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseSetting("detailedErrors", "true")
                .CaptureStartupErrors(true)
                .UseSerilog()
                .ConfigureLogging((hostingContext, logging) =>
                {
                    // Requires `using Microsoft.Extensions.Logging;`
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                    logging.AddEventSourceLogger();
                })
                .Build();
    }
}
