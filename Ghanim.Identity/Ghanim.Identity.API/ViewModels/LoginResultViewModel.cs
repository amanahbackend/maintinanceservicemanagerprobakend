﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonEnum;
namespace Ghanim.API
{
    public class LoginResultViewModel
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public List<string> Roles { get; set; }
        public string UserId { get; set; }
        public int TeamId { get; set; }
        public int Id { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public int LanguageId { get; set; }
        public bool IsFirstLogin { get; set; }
        public TechnicianState LastState { get; set; }
        public string LanguageCode { get; set; }
    }
}
