﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using FluentValidation.Attributes;
using Ghanim.API.Validators.PaymentTransaction;
using Ghanim.Models.Payment.Enums;

namespace Ghanim.API.ViewModels.PaymentTransaction
{
    [Validator(typeof(PaymentTransactionValidator))]
    public class PaymentTransactionViewModel : BaseEntity 
    {
        public PaymentModeEnum PaymentMode { get; set; }
        //Status in case online: (the online payment status to come from KNPAY)
        public PaymentStatusEnum PaymentStatus { get; set; }
        public string PaymentMethod { get; set; }

        //the money that customer payed on this payment
        public decimal Payment { get; set; }


        public string Receipt { get; set; }

        //Manual receipt number* (entered manually by technician)
        public string ManualReceiptNumber { get; set; }


        public decimal OfferDiscount { get; set; }


        public int OrderId { get; set; }

        //exuter userId
        //ExcuterUser // created by

        public string CreatedBy { get; set; }

        //Dispatcher id during payment
        public int? CurrentDispatcherId { get; set; }
        public string CurrentDispatcher { get; set; }

        //supervisor id during payment
        public int? CurrentSupervisorId { get; set; }
        public string CurrentSupervisor { get; set; }

        //last tech had interaction with order before payment
        public int? CurrentTechnicianId { get; set; }
        public string CurrentTechnician { get; set; }

        //foreman at payment time
        public int? CurrentForemanId { get; set; }
        public string CurrentForeman { get; set; }
        //master data
        public int ServiceTypeId { get; set; }
        public string ServiceType { get; set; }


        public string KNPAYPaymentRefNumber { get; set; }
        public string KNPAYAuthorizationCode { get; set; }
        public string KNPAYTransactionID { get; set; }
        public string KNPAYTrackID { get; set; }

        public int OrderHistoryStatusId { get; set; }
        public  string OrderHistoryStatus { get; set; }




        ///props needed in View Model
        public string OrderCode { get; set; }
        public string OrderType { get; set; }
        public string OrderContractNom { get; set; }
        public string OrderContractType { get; set; }


        public string OrderAreaNumber { get; set; }
        public string OrderAreaDescription { get; set; }

        public string CompanyCode { get; set; }
        public string CompanySAPId { get; set; }
        public string CompanyName { get; set; }

        public string OrderCustomerName { get; set; }
        public string OrderCustomerSAPID { get; set; }
        public string OrderCustomerPhone { get; set; }

        public string PaymentSource { get; set; }

        public string OrderProblemCode { get; set; }
        public string OrderProblemName { get; set; }

        public string OrderCurrentStatus { get; set; }

    }
}
