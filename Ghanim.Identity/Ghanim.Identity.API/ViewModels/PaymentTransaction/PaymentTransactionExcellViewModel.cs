﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using FluentValidation.Attributes;
using Ghanim.API.Validators.PaymentTransaction;
using Ghanim.Models.Payment.Enums;

namespace Ghanim.API.ViewModels.PaymentTransaction
{
    public class PaymentTransactionExcellViewModel
    {
        public string Order_Id { get; set; }
        public string Amount_Collected { get; set; }
        public string System_Receipt_No { get; set; }
        public string Manual_Receipt_No { get; set; }
        public string Order_Creation_Date { get; set; }
        public string Order_Type { get; set; }
        public string Contract_No { get; set; }
        public string Contract_Type { get; set; }
        public string Order_Expiry_Date { get; set; }
        public string Collector_Name { get; set; }
        public string Collector_Pf { get; set; }
        public string Collector_User_Type { get; set; }
        public string Payment_Mode { get; set; }
        public string Payment_Method { get; set; }
        public string Payment_Date { get; set; }
        public string Order_Area_Number { get; set; }
        public string Order_Area_Description { get; set; }
        public string Service_Type { get; set; }
    }
}
