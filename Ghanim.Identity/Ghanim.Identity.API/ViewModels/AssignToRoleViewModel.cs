﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class AssignToRoleViewModel
    {
        public string UserId { get; set; }
        public string[] Roles { get; set; }
    }
}
