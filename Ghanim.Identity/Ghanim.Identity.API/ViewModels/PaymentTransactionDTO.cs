﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.Models.Payment.Enums;

namespace Ghanim.API.ViewModels
{
    public class PaymentTransactionDTO
    {
        public PaymentModeEnum PaymentMode { get; set; }
        //Status in case online: (the online payment status to come from KNPAY)
        public PaymentStatusEnum PaymentStatus { get; set; }

        //the money that customer payed on this payment
        public decimal Payment { get; set; }


        public string Receipt { get; set; }

        //Manual receipt number* (entered manually by technician)
        public string ManualReceiptNumber { get; set; }


        public decimal OfferDiscount { get; set; }


        public int OrderId { get; set; }

        public int ServiceTypeId { get; set; }
    }
}
