﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonEnums;

namespace Ghanim.API
{
    public partial class TeamMemberViewModel : BaseEntity
    {
        public int? TeamId { get; set; }
        public string MemberName { get; set; }
        public string MemberPhone { get; set; }
        public int MemberParentId { get; set; }
        public string MemberParentName { get; set; }
        public int Rank { get; set; }
        public int MemberType { get; set; }
        public string MemberTypeName { get; set; }
        public string PF { get; set; }
        public string UserId { get; set; }
        public int? DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int? CostCenterId { get; set; }
        public string CostCenter { get; set; }
        public bool IsDriver { get; set; }
        public string FullName { get; set; }
        public  bool IsDefault { get; set; }
    }
}
