﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class FinanceViewModel : BaseEntity
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string PF { get; set; }
       
        public int FK_SupportedLanguages_ID { get; set; }
        public string FullName { get; set; }
    }
}
