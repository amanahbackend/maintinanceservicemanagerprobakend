﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class AllDispatcherSettingsViewModel : BaseEntity
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int OrderProblemId { get; set; }
        public string OrderProblemName { get; set; }
        public int? AreaId { get; set; }
        public int GroupId { get; set; }
        public string AreaName { get; set; }
        public string FullName { get; set; }
        public string PF { get; set; }
        public Division division { get; set; }
        public Dispatcher dispatcher { get; set; }
        public OrderProblemViewModel OrderProblemObj { get; set; }
       public List<int> AssignedAreasForProblem { get; set; }
        public List<AreasViewModel> UnAssignedAreasForProblem { get; set; }

    }
    public class AreasWithAssignedFlag
    {
        public int? AreaId { get; set; }
        public string name { get; set; }
        public bool? IsAssigned { get; set; } = false;
    }

   
}
