﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class AttendenceViewModel :BaseEntity
    {
        public string AttendedId { get; set; }
        public string AttendedName { get; set; }
        public int StatusId { get; set; }
        public int StatusName { get; set; }
        public DateTime AttendedDate { get; set; }
    }
}
