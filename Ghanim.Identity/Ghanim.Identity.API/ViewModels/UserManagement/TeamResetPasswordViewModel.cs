﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class TeamResetPasswordViewModel
    {
        [Required]
        public int TeamId { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}
