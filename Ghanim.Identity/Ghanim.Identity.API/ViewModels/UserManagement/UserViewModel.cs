﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.UserManagement
{
    public class UserViewModel
    {
        public ApplicationUserViewModel BaseUser { get; set; }
        public DispatcherViewModel DispatcherViewModel { get; set; }
        public EngineerViewModel EngineerViewModel { get; set; }
        public ForemanViewModel ForemanViewModel { get; set; }
        public ManagerViewModel ManagerViewModel { get; set; }
        public SupervisorViewModel SupervisorViewModel { get; set; }
        public TechnicianViewModel TechnicianViewModel { get; set; }
        public FinanceViewModel FinanceViewModel { get; set; }
    }
}
