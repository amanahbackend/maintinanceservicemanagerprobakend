﻿using Ghanim.BLL.NotMappedModels;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.UserManagement
{
    public class DispatcherSettingsInputsViewModel
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }

        public int DivisionId { get; set; }
        //  public List<Areas> Areas { get; set; }
        public int OrderProblemId { get; set; }
        public List<int> Areas { get; set; }



    }
}
