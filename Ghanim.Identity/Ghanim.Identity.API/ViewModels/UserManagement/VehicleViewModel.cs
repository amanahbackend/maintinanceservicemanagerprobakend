﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class VehicleViewModel :BaseEntity
    {
        
        public string Plate_no { get; set; }
        public bool Isassigned { get; set; }
        public int TeamId { get; set; }
       
    }
}
