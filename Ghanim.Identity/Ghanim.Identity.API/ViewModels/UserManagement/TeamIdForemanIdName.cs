﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class TeamIdForemanIdNameViewModel
    {
        public int? ForemanId { get; set; }
        public string ForemanName { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
