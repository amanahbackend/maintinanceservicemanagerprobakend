﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class SubmitLanguageViewModel
    {
        public string UserId { get; set; }
        public int LanguageId { get; set; }
        //public bool IsFirstLogin { get; set; }
    }
}
