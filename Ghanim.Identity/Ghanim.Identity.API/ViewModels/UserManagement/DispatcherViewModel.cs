﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class DispatcherViewModel : BaseEntity
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public string PF { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
    }
    
    public class DispatcherViewModelWithCounts : DispatcherViewModel 
    {
        public int ForeManCount { get; set; }
        public int OrderCount { get; set; }
    }


    
}
