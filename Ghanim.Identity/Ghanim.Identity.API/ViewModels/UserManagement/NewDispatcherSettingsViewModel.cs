﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.UserManagement
{
    public class NewDispatcherSettingsViewModel: BaseEntity
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int OrderProblemId { get; set; }
        public string OrderProblemName { get; set; }
        public int? AreaId { get; set; }
        public int GroupId { get; set; }
        public string AreaName { get; set; }
        public string FullName { get; set; }
        public string PF { get; set; }
        public List<Areas> Areas { get; set; }

    }
}

