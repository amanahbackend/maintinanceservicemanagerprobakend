﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class CustomerPhoneBookViewModel : BaseEntity 
    {
        public int FK_Customer_Id { get; set; }

        public string Customer_Name { get; set; }

        public string Phone { get; set; }

        public int FK_PhoneType_Id { get; set; }
        public string PhoneType_Name { get; set; }


    }
}
