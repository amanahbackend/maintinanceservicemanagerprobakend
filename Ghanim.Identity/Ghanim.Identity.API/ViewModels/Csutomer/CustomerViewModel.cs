﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class CustomerViewModel : BaseEntity
    {

        public string CuatomerId { get; set; }

        public string Name { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public string Email { get; set; }

        public string AccountNo { get; set; }

        public string CustomerSiteName { get; set; }

        public string SiteCode { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public string CustomerType_Name { get; set; }

        public int FK_MachineType_Id { get; set; }
        public int MachineType_Name { get; set; }

        public List<LocationDataViewModel> Locations { get; set; }
        public List<CustomerPhoneBookViewModel> CustomerPhoneBook { get; set; }



    }
}
