﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class Lang_MachineTypeViewModel : BaseEntity
    {
        public int FK_MachineType_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public MachineType MachineType { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
