﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.API
{
    public class Lang_CompanyCodeViewModel : RepoistryBaseEntity
    {
        public int FK_CompanyCode_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public CompanyCode CompanyCode { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
