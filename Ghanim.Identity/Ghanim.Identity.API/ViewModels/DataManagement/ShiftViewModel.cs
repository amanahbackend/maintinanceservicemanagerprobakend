﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.API
{
    public class ShiftViewModel : RepoistryBaseEntity
    {
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public string Name => $"{FromTime:hh:mm tt} - {ToTime:hh:mm tt}";
        public bool IsDeleted { get; set; }
    }
}
