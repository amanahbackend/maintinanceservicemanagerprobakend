﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.API
{
   public class BuildingTypesViewModel : RepoistryBaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
