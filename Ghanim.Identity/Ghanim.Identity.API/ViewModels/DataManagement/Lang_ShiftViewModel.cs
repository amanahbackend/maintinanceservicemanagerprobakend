﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.API
{
    public class Lang_ShiftViewModel : RepoistryBaseEntity
    {
        public int FK_Shift_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        public string Name { get; set; }
        public Shift Shift { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
