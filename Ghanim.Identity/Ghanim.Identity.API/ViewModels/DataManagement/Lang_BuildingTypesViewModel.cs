﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.API
{
    public class Lang_BuildingTypesViewModel : RepoistryBaseEntity
    {
        public int FK_BuildingTypes_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public BuildingTypes BuildingTypes { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
