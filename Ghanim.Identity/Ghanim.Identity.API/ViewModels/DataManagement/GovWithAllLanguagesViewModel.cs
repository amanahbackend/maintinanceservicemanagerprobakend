﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class GovWithAllLanguagesViewModel : RepoistryBaseEntity
    {
        public int Gov_No { get; set; }
        public List<LanguagesDictionaryViewModel> languagesDictionaries { get; set; }
        public bool IsDeleted { get; set; }
    }
}
