﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class TeamModeViewModel
    {
        public string Mode { get; set; }
        public int ? ModeId { get; set; }
        public int? WorkingTypeId { get; set; }
    }
}
