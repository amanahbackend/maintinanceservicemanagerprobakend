﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.ViewModels.Base;
using Ghanim.Models.Payment.Enums;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaymentTransactionFilters : BaseFilter
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public List<PaymentModeEnum> PaymentMode { get; set; }
        //Status in case online: (the online payment status to come from KNPAY)
        public List<PaymentStatusEnum> PaymentStatus { get; set; }

        public string OrderCode { get; set; }
        public List<int> OrderHistoryStatusId { get; set; }
        public List<int> OrderCurrentStatusId { get; set; }
        public List<string> FK_CreatedBy_Id { get; set; }

        public List<int> CurrentDispatcherId { get; set; }

        public List<int> CurrentSupervisorId { get; set; }

        public List<int> CurrentTechnicianId { get; set; }
        public List<int> CurrentForemanId { get; set; }

        public List<int> ServiceTypeId { get; set; }


        public List<int> OrderTypeId { get; set; }
        public List<int> OrderContractTypeId { get; set; }
        public List<int> OrderAreaId { get; set; }
        public List<int> OrderCompanyId { get; set; }
        public string OrderCustomerCode { get; set; }
        public List<int> OrderProblemId { get; set; }

    }
}
