﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaginatedFilterOrderReportViewModel
    {
        public List<OrderManagementReportViewModel> Filters { get; set; }
        //public PaginatedItemsViewModel<OrderManagementReportOutputVieweModel> PageInfo { get; set; } = null;
    }
}
