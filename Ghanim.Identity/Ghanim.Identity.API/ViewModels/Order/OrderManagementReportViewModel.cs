﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class OrderManagementReportViewModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string CustomerCode { get; set; }
        public string OrderId { get; set; }
        public List<int> StatusId { get; set; }
        public List<int> ProblemId { get; set; }
        public List<int> DivisionId { get; set; }
        public List<int> DispatcherId { get; set; }
        //public List<int> TeamId { get; set; }
        
        
        //userIds
        public List<int> Techs { get; set; }
        public List<int> AreaId { get; set; }
        public List<int> OrderTypeId { get; set; }
        public List<int> CompanyCode { get; set; }
        public bool Closed { get; set; }
        public bool IncludeProgressData { get; set; }
        public bool OnHoldOnly { get; set; }
        public bool AllProgress { get; set; }
        public bool OnHoldLastProgress { get; set; }
        public bool LastProgress { get; set; }
        public PaginatedItemsViewModel<OrderViewModel> PageInfo { get; set; } = null;
    }
}
