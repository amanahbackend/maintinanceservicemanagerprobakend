﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Order
{
    public class VisitSlotViewModel : BaseEntity
    {
        public string Code { get; set; }
        public string SlotDescription { get; set; }
    }
}
