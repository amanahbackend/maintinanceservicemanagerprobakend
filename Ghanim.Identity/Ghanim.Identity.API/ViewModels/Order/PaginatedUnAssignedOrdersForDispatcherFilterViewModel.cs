﻿using Ghanim.API.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaginatedUnAssignedOrdersForDispatcherFilterViewModel
    {
        public int DispatcherId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string OrderCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPhone { get; set; }
        public List<int> AreaIds { get; set; }
        public string BlockName { get; set; }
        public List<int> ProblemIds { get; set; }
        public List<int> StatusIds { get; set; }
        public int Rejection { get; set; }
        public bool ReturnAssigned { get; set; } = true;
        public bool ReturnUnAssigned { get; set; } = true;

       // public PaginatedItemsViewModel<OrderViewModel> PageInfo { get; set; } = null;
        public PaginatedItemsViewModel<OrderCardViewModel> PageInfo { get; set; } = null;
    }
}
