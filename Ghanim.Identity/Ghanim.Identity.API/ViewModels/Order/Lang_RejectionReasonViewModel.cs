﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class Lang_RejectionReasonViewModel : BaseEntity
    {
        public int FK_RejectionReason_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        public Lang_RejectionReason RejectionReason { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
