﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.API.Enums;
using Ghanim.Models.Order.Enums;

namespace Ghanim.API
{
    public class ChangeStatusViewModel
    {
        public PlatformType PlatformType { get; set; }

        public int? OrderId { get; set; }
        public int? StatusId { get; set; }
        //public string StatusName { get; set; }
        public int? SubStatusId { get; set; }
        public string SubStatusName { get; set; }
        public string ServeyReport { get; set; }
        public int? TeamId { get; set; }
        public string TeamMode { get; set; }

    }
}
