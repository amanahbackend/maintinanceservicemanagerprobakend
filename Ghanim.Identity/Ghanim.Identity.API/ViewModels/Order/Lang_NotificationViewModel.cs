﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class Lang_NotificationViewModel : BaseEntity
    {
        public int FK_Notification_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public Notification Notification { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
