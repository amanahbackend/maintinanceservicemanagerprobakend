﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{    
    public class AssignDispatcherOrderViewModel : BaseEntity
    {
        // Order info
        public string code { get; set; }
        public int typeId { get; set; }
        public string typeName { get; set; }
        public int statusId { get; set; }
        public string statusName { get; set; }
        public int subStatusId { get; set; }
        public string subStatusName { get; set; }
        public int problemId { get; set; }
        public string problemName { get; set; }
        public int priorityId { get; set; }
        public string priorityName { get; set; }
        public int companyCodeId { get; set; }
        public string companyCodeName { get; set; }
        public string orderTypeCode { get; set; }
        public int divisionId { get; set; }
        public string divisionName { get; set; }
        public string icAgentNote { get; set; }
        public string dispatcherNote { get; set; }
        public string cancellationReason { get; set; }
        public string generalNote { get; set; }
        public Nullable<DateTime> saP_CreatedDate { get; set; }
        public string orderDescription { get; set; }
        public string serveyReport { get; set; }

        // Customer info
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string phoneOne { get; set; }
        public string phoneTwo { get; set; }
        public string caller_ID { get; set; }
        // Address info
        //// SAP address
        public string saP_PACI { get; set; }
        public string saP_HouseKasima { get; set; }
        public string saP_Floor { get; set; }
        public string saP_AppartmentNo { get; set; }
        public string saP_StreetName { get; set; }
        public string saP_BlockName { get; set; }
        public string saP_AreaName { get; set; }
        public string saP_GovName { get; set; }
        //// PACI address
        public string pACI { get; set; }
        public string functionalLocation { get; set; }
        public string houseKasima { get; set; }
        public string floor { get; set; }
        public string appartmentNo { get; set; }
        public int streetId { get; set; }
        public string streetName { get; set; }
        public int blockId { get; set; }
        public string blockName { get; set; }
        public int areaId { get; set; }
        public string areaName { get; set; }
        public int govId { get; set; }
        public string govName { get; set; }
        public string addressNote { get; set; }
        public decimal Long { get; set; }
        public decimal Lat { get; set; }
        public int buildingTypeId { get; set; }
        public string buildingTypeName { get; set; }
        // Contract info
        public string contractCode { get; set; }
        public int contractTypeId { get; set; }
        public string contractTypeName { get; set; }
        public Nullable<DateTime> contractStartDate { get; set; }
        public Nullable<DateTime> contractExpiryDate { get; set; }
        // Reading info
        public Nullable<DateTime> insertionDate { get; set; }
        public string fileName { get; set; }
        // Assigning info
        public int supervisorId { get; set; }
        public string supervisorName { get; set; }
        public int dispatcherId { get; set; }
        public string dispatcherName { get; set; }
        public int? rankInDispatcher { get; set; }
        public int teamId { get; set; }
        public int prevTeamId { get; set; }
        public int? rankInTeam { get; set; }
        public AcceptenceType acceptanceFlag { get; set; }
        public int rejectionReasonId { get; set; }
        public string rejectionReason { get; set; }
        public AccomplishType isAccomplish { get; set; }
        public int onHoldCount { get; set; }
        // Repeated call
        public bool isRepeatedCall { get; set; }
        // Is Exceed time
        public bool isExceedTime { get; set; }
        public int? techId { get; set; }
        public string techName { get; set; }
        //public int? TeamId { get; set; }
        public string teamName { get; set; }
        public string techNotes { get; set; }
        public string pin { get; set; }
        public string currentUserId { get; set; }
        public string fK_CreatedBy_Id { get; set; }
        public string fK_UpdatedBy_Id { get; set; }
        public string fK_DeletedBy_Id { get; set; }
        public Nullable<DateTime> updatedDate { get; set; }
        public Nullable<DateTime> createdDate { get; set; }

        //
        //


    }
}
