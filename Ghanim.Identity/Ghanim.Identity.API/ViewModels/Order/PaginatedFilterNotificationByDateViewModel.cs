﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaginatedFilterNotificationByDateViewModel
    {
        public string  RecieverId { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string orderCode { get; set; }
        public List<string> tecs { get; set; }
        public bool IsActionTaken { get; set; }
        public PaginatedItemsViewModel<NotificationCenterViewModel> PageInfo { get; set; } = null;
    }
}
