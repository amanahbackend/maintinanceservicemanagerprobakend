﻿
using Ghanim.Models.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class WorkingTypesWithAllLanguagesViewModel
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int CategoryCode { get; set; }
        public List<LanguagesDictionaryViewModel> languagesDictionaries { get; set; }
        public bool IsDeleted { get; set; }
        public int FK_WorkingType_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public int StateId { get; set; }
        public int TypeId { get; set; }
        public string StateName { get; set; }
        public IEnumerable<WorkingType> WorkingTypes { get; set; }       
        public WorkingType WorkingType { get; set; }
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
