﻿using Ghanim.API.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class TeamOrdersViewModel
    {
        public int ForemanId { get; set; }
        public string ForemanName { get; set; }
        public string Phone { get; set; }
        public int? TeamId { get; set; }
        public string TeamName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
       public IEnumerable<OrderCardViewModel> Orders { get; set; }
    }
    
}
