﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ghanim.Models.Entities;

namespace Ghanim.API
{
    public class OrderManagementReportExportOutputViewModelWithProgreassColomns : OrderManagementReportExportOutputViewModel
    {

        public override string Order_ID { get; set; }
        public override string Created_Date { get; set; }
        public override string Created_Time { get; set; }
        public override string Current_Order_Status { get; set; }
        public override string Order_Type { get; set; }
        public override string Dispatcher_Notes { get; set; }
        public override string IC_Agent_Notes { get; set; }
        public override string Order_Description { get; set; }
        public override string Problem_Type { get; set; }
        public override string Division { get; set; }
        public override string Current_Supervisor_Name { get; set; }
        public override string Current_Dispatcher { get; set; }
        public override string Current_Foreman_Name { get; set; }
        public override string Current_Technician { get; set; }
        public override string Company_Code { get; set; }
        public override string Customer_ID { get; set; }
        public override string Customer_Tel_1 { get; set; }
        public override string Customer_Tel_2 { get; set; }
        public override string Customer_Caller_Phone { get; set; }
        public override string Functional_Location { get; set; }
        public override string PACI_No { get; set; }
        public override string PACI_Governorate { get; set; } 
        public override string SAP_Area { get; set; }// was paci but changed as per client request 
        public override string SAP_Street { get; set; }// was paci but changed as per client request  
        public override string SAP_Block { get; set; }// was paci but  changed as per client request  
        public override string SAP_Building { get; set; }
        public override string SAP_Floor { get; set; }
        public override string SAP_Apartment { get; set; }
        public override string Contract_Number { get; set; }
        public override string Contract_Type { get; set; }
        public override string Contract_Creation_Date { get; set; }
        public override string Contract_Expiration_Date { get; set; }
        public override string Closed_Date { get; set; }
        public override string Closed_Time { get; set; }
        public string Progress_Date_History { get; set; }
        public string Progress_Time_History { get; set; }
        public string Executor_History { get; set; }
        public string Action_Type_Name_History { get; set; }
        public string Progress_Status_History { get; set; }
        public string Progress_Sub_Status_History { get; set; }
        public string Technician_Name_History { get; set; }
        public string Dispatcher_Name_History { get; set; }
        public string Survey_Report_History { get; set; }
 
        
        public OrderManagementReportExportOutputViewModelWithProgreassColomns()
        {

        }

        public OrderManagementReportExportOutputViewModelWithProgreassColomns
            (OrderManagementReportItem itm, Dictionary<int?, string> teamIdDefaultTechName):base(itm, teamIdDefaultTechName)
        {
            DateTime? progress = null;
            if (itm.ProgressDate != null)
                progress = itm.ProgressDate?.AddHours(3);

            Progress_Date_History = progress == null ? "" : ((DateTime)progress).ToString("dd-MM-yyyy") ?? "";
            Progress_Time_History = progress == null ? "" : ((DateTime)progress).ToString("hh:mm tt") ?? "";
            Executor_History = itm.Executer ?? "";
            Action_Type_Name_History = itm.ActionTypeName ?? "";
            Progress_Status_History = itm.ProgressStatus ?? "";
            Progress_Sub_Status_History = itm.ProgressSubStatus ?? "";
            Technician_Name_History = itm.TechnicianName ?? "";
            Dispatcher_Name_History = itm.DispatcherName ?? "";
            Survey_Report_History = itm.TechnicianNotes ?? "";
        }
    }



    public class OrderManagementReportExportOutputViewModel
    {
        public virtual string Order_ID { get; set; }
        public virtual string Created_Date { get; set; }
        public virtual string Created_Time { get; set; }
        public virtual string Current_Order_Status { get; set; }
        public virtual string Order_Type { get; set; }
        public virtual string Dispatcher_Notes { get; set; }
        public virtual string IC_Agent_Notes { get; set; }
        public virtual string Order_Description { get; set; }
        public virtual string Problem_Type { get; set; }
        public virtual string Division { get; set; }
        public virtual string Current_Supervisor_Name { get; set; }
        public virtual string Current_Dispatcher { get; set; }
        public virtual string Current_Foreman_Name { get; set; }
        public virtual string Current_Technician { get; set; }
        public virtual string Company_Code { get; set; }
        public virtual string Customer_ID { get; set; }
        public virtual string Customer_Tel_1 { get; set; }
        public virtual string Customer_Tel_2 { get; set; }
        public virtual string Customer_Caller_Phone { get; set; }
        public virtual string Functional_Location { get; set; }
        public virtual string PACI_No { get; set; }
        public virtual string PACI_Governorate { get; set; } 
        public virtual string SAP_Area { get; set; }// was paci but changed as per client request 
        public virtual string SAP_Street { get; set; }// was paci but changed as per client request  
        public virtual string SAP_Block { get; set; }// was paci but  changed as per client request  
        public virtual string SAP_Building { get; set; }
        public virtual string SAP_Floor { get; set; }
        public virtual string SAP_Apartment { get; set; }
        public virtual string Contract_Number { get; set; }
        public virtual string Contract_Type { get; set; }
        public virtual string Contract_Creation_Date { get; set; }
        public virtual string Contract_Expiration_Date { get; set; }
        public virtual string Closed_Date { get; set; }
        public virtual string Closed_Time { get; set; }




        public OrderManagementReportExportOutputViewModel()
        {

        }
    
        public OrderManagementReportExportOutputViewModel(OrderManagementReportItem itm)
        {
            DateTime? closed = null;
            if (itm.ClosedDate != null)
                closed = itm.ClosedDate?.AddHours(3);

            Order_ID = itm.OrderCode ?? "";
            Created_Date = itm.CreatedDate == DateTime.MinValue ? "" : itm?.CreatedDate.ToString("dd-MM-yyyy") ?? "";
            Created_Time = itm.CreatedDate == DateTime.MinValue ? "" : itm?.CreatedDate.ToString("hh:mm tt") ?? "";
            Current_Order_Status = itm.Status ?? "";
            Order_Type = itm.OrderType ?? "";
            Dispatcher_Notes = itm.DispatcherNotes ?? "";
            IC_Agent_Notes = itm.ICAgentNotes != null ? Regex.Replace(itm.ICAgentNotes, @"\t|\n|\r", " ") : "";
            Order_Description = itm.OrderDesc ?? "";
            Problem_Type = itm.ProblemType ?? "";
            Division = itm.Division ?? "";
            Current_Supervisor_Name = itm.Supervisor ?? "";
            Current_Dispatcher = itm.Dispatcher ?? "";
            Current_Foreman_Name = itm.ForeMan ?? "";
            Current_Technician = itm.Technician ?? "";
            Company_Code = itm.CompanyCode ?? "";
            Customer_ID = itm.CustomerID;
            Customer_Tel_1 = itm.CustomerFirstPhone ?? "";
            Customer_Tel_2 = itm.CustomerSecondPhone ?? "";
            Customer_Caller_Phone = $"+{itm.CustomerCallerPhone ?? ""}";
            Functional_Location = itm.FunctionalLocation ?? "";
            PACI_No = itm.PACINum ?? "";
            PACI_Governorate = itm.GovName ?? "";
            SAP_Area = itm.Area ?? "";
            SAP_Street = itm.Street ?? "";
            SAP_Block = itm.Block ?? "";
            SAP_Building = itm.SAPBuilding ?? "";
            SAP_Floor = itm.SAPFloor ?? "";
            SAP_Apartment = itm.SAPApartmentNo ?? "";
            Contract_Number = itm.ContractNo ?? "";
            Contract_Type = itm.ContractType ?? "";
            Contract_Creation_Date = itm?.ContractCreationDate?.ToString("dd-MM-yyyy");
            Contract_Expiration_Date = itm?.ContractExpirationDate?.ToString("dd-MM-yyyy");
            Closed_Date = closed == null ? "" : ((DateTime)closed).ToString("dd-MM-yyyy") ?? "";
            Closed_Time = closed == null ? "" : ((DateTime)closed).ToString("hh:mm tt") ?? "";
            //TeamId = itm.TeamId == 0 ? "" : itm.TeamId.ToString();
        }


        public OrderManagementReportExportOutputViewModel
            (OrderManagementReportItem itm, Dictionary<int?, string> teamIdDefaultTechName) : this(itm)
        {
            if (itm.TeamId != null )
            {
                try
                {
                    string dummy = "";
                    teamIdDefaultTechName.TryGetValue(itm.TeamId, out dummy);
                    Current_Technician = dummy;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

    }



}
