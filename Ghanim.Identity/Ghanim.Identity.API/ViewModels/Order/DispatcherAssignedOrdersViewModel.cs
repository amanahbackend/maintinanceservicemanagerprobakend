﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class DispatcherAssignedOrdersViewModel : BaseEntity
    {
        public int? ForemanId{ get; set; }
        public string ForemanName{ get; set; }
        public string ForemanPhone { get; set; }
        public IEnumerable<TeamOrdersViewModel> Teams { get; set; }
    }
}
