﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class ChangeRankInTeamViewModel
    {
        public int TeamId { get; set; }
        public Dictionary<int,int> Orders { get; set; }
        public int? orderId { get; set; }
    }
}
