﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.Models.Entities;
using Ghanim.Models.Order.Enums;
using Utilites.ProcessingResult;

namespace Ghanim.API
{
    public class OrderManagementReportItem : BaseEntity
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int OrderId { get; set; }
        public string OrderCode { get; set; }
        public string Status { get; set; }
        public string ActionTypeName { get; set; }
        public string ProblemType { get; set; }
        public string OrderType { get; set; }
        public string DispatcherNotes { get; set; }
        public string OrderDesc { get; set; }
        public string Area { get; set; }
        public string Street { get; set; }
        public string Block { get; set; }
        public string PACINum { get; set; }
        public string Division { get; set; }
        public string Dispatcher { get; set; }
        public string Technician { get; set; }
        public string Supervisor { get; set; }
        public int? TeamId { get; set; }

        public string CompanyCode { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string ICAgentNotes { get; set; }
        public DateTime? ProgressDate { get; set; }
        public TimeSpan? ProgressTime { get; set; }
        public string ProgressStatus { get; set; }
        public string ProgressSubStatus { get; set; }
        public string TechnicianNotes { get; set; }
        public string TechnicianName { get; set; }
        public string DispatcherName { get; set; }
        public string SupervisorName { get; set; }
        public string Executer { get; internal set; }
        public string CustomerID { get; internal set; }
        public string CustomerFirstPhone { get; internal set; }
        public string CustomerSecondPhone { get; internal set; }
        public string CustomerCallerPhone { get; internal set; }
        public string GovName { get; internal set; }
        public string FunctionalLocation { get; internal set; }
        public string SAPBuilding { get; internal set; }
        public string SAPFloor { get; internal set; }
        public string SAPApartmentNo { get; internal set; }
        public string ContractNo { get; internal set; }
        public string ContractType { get; internal set; }
        public DateTime? ContractCreationDate { get; internal set; }
        public DateTime? ContractExpirationDate { get; internal set; }
        public string ForeMan { get; internal set; }
        public int? CreatedUserId { get; set; }
        public string CreatedUser { get; set; }



        public OrderManagementReportItem()
        {

        }

        public OrderManagementReportItem(OrderAction x) : this(x.Order)
        {
            try
            {
                if (x.PlatformTypeSource != PlatformType.Ftp)
                    ProgressDate = x.ActionDate;

                ActionTypeName = x.ActionTypeName;

                Executer = x.ExcuterUser?.FullName;//TODO Excuter is not entered
                ProgressStatus = x?.Status?.Name;
                ProgressSubStatus = x.SubStatus?.Name;
                ProgressTime = x.ActionTimeDays == 0 || x.ActionTimeDays == null
                    ? x.ActionTime
                    : x.ActionTime.Value.Add(TimeSpan.FromDays(x.ActionTimeDays.Value));
                TechnicianNotes = x.ServeyReport;
                TechnicianName = x.Technician?.User.FullName;
                DispatcherName = x.Dispatcher?.User?.FullName;
                SupervisorName = x.Supervisor?.User?.FullName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public OrderManagementReportItem(OrderObject x)
        {

            try
            {
                DispatcherName = x.Dispatcher?.User?.FullName;
                OrderId = x.Id;
                OrderCode = x.Code;
                Area = x.SAP_AreaName;
                Block = x.SAP_BlockName;
                Street = x.SAP_StreetName;
                PACINum = x.SAP_PACI;
                CompanyCode = x.CompanyCode.Code;
                DispatcherNotes = x.DispatcherNote;
                CustomerID = x.CustomerCode;
                CustomerFirstPhone = x.PhoneOne;
                CustomerSecondPhone = x.PhoneTwo;
                CustomerCallerPhone = x.Caller_ID;
                GovName = x.GovName;
                FunctionalLocation = x.FunctionalLocation;
                SAPBuilding = x.SAP_HouseKasima;
                SAPFloor = x.SAP_Floor;
                SAPApartmentNo = x.SAP_AppartmentNo;
                ContractNo = x.ContractCode;
                ContractType = x.ContractType?.Name;
                ContractCreationDate = x.ContractStartDate;
                ContractExpirationDate = x.ContractExpiryDate;
                Dispatcher = x.Dispatcher?.User?.FullName;
                Supervisor = x.Supervisor?.User?.FullName;
                ForeMan = x.Team?.Foreman?.User?.FullName;
                TeamId = x.TeamId;
                //Technician = x.Team?.Technicians?.FirstOrDefault()?.User?.FullName ?? "";
                ICAgentNotes = x.ICAgentNote;
                Division = x.Division?.Name;
                Status = x?.Status?.Name;
                OrderDesc = x.OrderDescription;
                OrderType = x.Type?.Name;
                ProblemType = x?.Problem?.Name;
                ClosedDate = x.StatusId == 7 || x.StatusId == 8 ? x.UpdatedDate : (DateTime?)null;
                CreatedDate = x.SAP_CreatedDate == null ? default(DateTime) : x.SAP_CreatedDate;
                UpdatedDate = x.UpdatedDate == DateTime.MinValue ? x.SAP_CreatedDate : x.UpdatedDate;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public OrderManagementReportItem Clone()
        {
            return (OrderManagementReportItem)MemberwiseClone();
        }

    }
}