﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class FilterOrderViewModel
    {
        public List<Filter> Filters { get; set; }
        public bool IncludeUnAssign { get; set; }
    }
    public class Filter
    {
        public string PropertyName { get; set; }
        public object Value { get; set; }
    }
}
