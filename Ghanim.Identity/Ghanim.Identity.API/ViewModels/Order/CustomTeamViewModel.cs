﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Order
{
    public class CustomTeamViewModel
    {
        public CustomTeamViewModel()
        {
            this.Orders = new List<OrderCardViewModel>();
        }
        public int TeamId { get; set; }
        public List<OrderCardViewModel> Orders { get; set; }
        public string  TeamName { get; set; }
        public int StatusId { get; set; }
    }
}
