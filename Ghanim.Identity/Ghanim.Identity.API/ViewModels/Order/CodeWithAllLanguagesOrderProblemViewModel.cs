﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class CodeWithAllLanguagesOrderProblemViewModel : RepoistryBaseEntity
    {
        public string Code { get; set; }
        public List<LanguagesDictionariesViewModel> languagesDictionaries { get; set; }
        public bool IsDeleted { get; set; }
    }
}
