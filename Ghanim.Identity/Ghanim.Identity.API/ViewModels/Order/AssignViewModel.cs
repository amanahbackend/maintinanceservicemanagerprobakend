﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class AssignViewModel
    {
        public int OrderId { get; set; }
        public int TeamId { get; set; }
    }
}
