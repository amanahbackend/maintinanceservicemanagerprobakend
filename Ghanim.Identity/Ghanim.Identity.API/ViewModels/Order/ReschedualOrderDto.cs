﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class ReschedualOrderDto
    {
        public string OrderCode { get; set; }
        public string VisitSlotCode { get; set; }
        public DateTime Visitdate { get; set; }
        public string CustomerCode { get; set; }

}
}
