﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class PaginationContainer<T>
    {
        public long Count { get; set; }
        public List<T> Items { get; set; }

        public PaginationContainer()
        {
            Items = new List<T>();
        }
    }
}