﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class WorkingTypeGroupViewModel
    {
        public int StateId { get; set; }
        public int TypeId { get; set; }
        public string StateName { get; set; }
        public IEnumerable<WorkingType> WorkingTypes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
