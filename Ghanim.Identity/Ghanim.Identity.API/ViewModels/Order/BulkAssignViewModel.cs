﻿using CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class BulkAssignViewModel
    {
        public int OrderId { get; set; }
        public bool IsTeam { get; set; }
        public int? TeamId { get; set; }
        public int? DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public int? SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
