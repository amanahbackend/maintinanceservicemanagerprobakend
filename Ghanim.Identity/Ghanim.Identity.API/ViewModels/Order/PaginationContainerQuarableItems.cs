﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class PaginationContainerQuarableItems<T>
    {
        public long Count { get; set; }
        public IQueryable<T> Items { get; set; }
    }
}