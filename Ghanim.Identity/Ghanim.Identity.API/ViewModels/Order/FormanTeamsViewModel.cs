﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Order
{
    public class FormanTeamsViewModel
    {
        public int ForemanId { get; set; }
        public string ForemanName { get; set; }
        public string ForemanPhone { get; set; }
        public IQueryable<CustomTeamViewModel> Teams { get; set; }
    }
}
