﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class OrderFilterViewModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public List<int> StatusId { get; set; }
        public List<int> ProblemId { get; set; }
        public List<int> DivisionId { get; set; }
        public List<int> DispatcherId { get; set; }
        public List<int> TechnicianId { get; set; }
        public List<int> AreaId { get; set; }
        public List<int> OrderTypeId { get; set; }
        public int Assigned{ get; set; }
    }
    public class Coordinate
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
    public class OrdersMapBoundaryViewModel : OrderFilterViewModel
    {
        public Coordinate SouthWest{get;set; }
        public Coordinate NorthEast { get; set; }
        public List<int> TeamId { get; set; }
    }
    public class OrdersMapViewModel
    {
        public int Id{ get; set; }
        public string Code { get; set; }
        public decimal Long { get; set; }
        public decimal Lat { get; set; }
        public string Pin{ get; set; }
        public int OnHoldCount { get; set; }
        public string TypeName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ProblemName { get; set; }
        public DateTime SAP_CreatedDate { get; set; }
    }
    public class OrdersMapViewCustomModel
    {
        public int OnHoldCount { get; set; }
        public string Code { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
        public string Pin { get; set; }

    }
}
