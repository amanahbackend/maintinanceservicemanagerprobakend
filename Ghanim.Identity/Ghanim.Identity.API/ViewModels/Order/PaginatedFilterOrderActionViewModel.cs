﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaginatedFilterOrderActionViewModel
    {
            public List<Filter> Filters { get; set; }
            public PaginatedItemsViewModel<OrderActionViewModel> PageInfo { get; set; } = null;
    }
}
