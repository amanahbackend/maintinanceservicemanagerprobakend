﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Order
{
    
    public class TeamOrdersForMobileViewModel : BaseEntity
    {
        // Order info
        public string Code { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int SubStatusId { get; set; }
        public string SubStatusName { get; set; }
        //public int? ProblemId { get; set; }
        public string ProblemName { get; set; }
       
        public string CompanyCodeName { get; set; }
        public string OrderTypeCode { get; set; }
       
        public string ICAgentNote { get; set; }
        public string DispatcherNote { get; set; }
        
        public Nullable<DateTime> SAP_CreatedDate { get; set; }
        public string OrderDescription { get; set; }
        public string ServeyReport { get; set; }

        // Customer info
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string PhoneOne { get; set; }
        public string PhoneTwo { get; set; }
        public string Caller_ID { get; set; }
        // Address info
        //// SAP address
        public string SAP_PACI { get; set; }
        public string SAP_HouseKasima { get; set; }
        public string SAP_Floor { get; set; }
        public string SAP_AppartmentNo { get; set; }
        public string SAP_StreetName { get; set; }
        public string SAP_BlockName { get; set; }
        public string SAP_AreaName { get; set; }
        public string SAP_GovName { get; set; }
        //// PACI address
        public string PACI { get; set; }
        public string FunctionalLocation { get; set; }
        
        public string AreaName { get; set; }
        
        public string AddressNote { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
       
        // Contract info
        public string ContractCode { get; set; }
      
        public string ContractTypeName { get; set; }
        public Nullable<DateTime> ContractStartDate { get; set; }
        public Nullable<DateTime> ContractExpiryDate { get; set; }
        
        public int? TeamId { get; set; }
        public int? PrevTeamId { get; set; }
        public int? RankInTeam { get; set; }
        public AcceptenceType AcceptanceFlag { get; set; }
        public int? RejectionReasonId { get; set; }
        public string RejectionReason { get; set; }
        

    }
}
