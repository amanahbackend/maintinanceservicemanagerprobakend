﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class PACIViewModel : BaseEntity
    {
        public string PACINumber { get; set; }
        public string AreaName { get; set; }
        public int AreaId { get; set; }
        public string GovernorateName { get; set; }
        public int GovernorateId { get; set; }
        public string BlockName { get; set; }
        public int BlockId { get; set; }
        public string StreetName { get; set; }
        public int StreetId { get; set; }
        public decimal Longtiude { get; set; }
        public decimal Latitude { get; set; }
        public string AppartementNumber { get; set; }
        public string FloorNumber { get; set; }
        public string BuildingNumber { get; set; }
    }
}
