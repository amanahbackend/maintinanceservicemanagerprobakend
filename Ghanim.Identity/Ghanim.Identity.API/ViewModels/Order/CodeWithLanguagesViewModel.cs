﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class CodeWithLanguagesViewModel : RepoistryBaseEntity
    {
        public string Code { get; set; }
        public int StatusId { get; set; }
        public int StatusName { get; set; }
        public List<LanguagesDictionariesViewModel> languagesDictionaries { get; set; }
        public bool IsDeleted { get; set; }
        ///
        public int StateId { get; set; }
        public int TypeId { get; set; }
        public string StateName { get; set; }
        public IEnumerable<WorkingType> WorkingTypes { get; set; }
        
    }
}
