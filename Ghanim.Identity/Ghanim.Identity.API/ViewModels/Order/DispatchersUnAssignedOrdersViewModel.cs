﻿using Ghanim.API.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class DispatchersUnAssignedOrdersViewModel
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public string DispatcherPhone { get; set; }
        public IEnumerable<OrderCardViewModel> Orders { get; set; }
    }
}
