﻿using Ghanim.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class PrintOutResultViewModel
    {
        public int DispatcherId { get; set; }       
        
        public List<ForemenWithOrders> foremenWithOrders { get; set; }
    }

    public class ForemenWithOrders
    {
      public int AreaId { get; set; }
      public string AreaName { get; set; }
      public int PrevTeamId { get; set; }
      public int? ForemanId { get; set; }
      public string ForemanName { get; set; }
      public IEnumerable<OrderViewModel> Orders { get; set; }
       // public List<OrderViewModel> Orderss { get; set; }

    }
}
