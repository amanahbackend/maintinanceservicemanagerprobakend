﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class PrintOutFilterViewModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int DispatcherId { get; set; }
        public List<int> teamsId { get; set; }
        public bool IncludeOpenOrders { get; set; }
    }
}
