﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public enum OrderUpdateResponseStatus
    {
        NoStatus = 0,
        NoTeam = 1,
        Completed = 2,
    }
}
