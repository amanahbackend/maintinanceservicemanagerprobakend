﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class RejectionReasonViewModel : BaseEntity
    {
        public string Name { get; set; }
    }
}
