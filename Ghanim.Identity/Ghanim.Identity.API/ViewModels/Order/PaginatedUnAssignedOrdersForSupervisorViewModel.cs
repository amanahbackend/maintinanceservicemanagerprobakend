﻿using Ghanim.API.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaginatedUnAssignedOrdersForSupervisorViewModel
    {
        public int SupervisrId { get; set; }
        public string OrderCode { get; set; }
        public PaginatedItemsViewModel<OrderViewModel> PageInfo { get; set; } = null;
        public PaginatedItemsViewModel<OrderCardViewModel> PageData { get; set; } = null;
    }
}
