﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class OrderManagementReportExportViewModel
    {
        public string OrderId { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedTime { get; set; }
        public string CurrentOrderStatus { get; set; }
        public string OrderType { get; set; }
        public string DispatcherNotes { get; set; }
        public string ICAgentNotes { get; set; }
        public string OrderDescription { get; set; }
        public string ProblemType { get; set; }
        public string Area { get; set; }
        public string Street { get; set; }
        public string Block { get; set; }
        public string PACINum { get; set; }
        public string Tech { get; set; }
        public string Disp { get; set; }
        public string Division { get; set; }
        public string CompanyCode { get; set; }
        public string ClosedDate { get; set; }
    }
}