﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaginatedFilterNotificationViewModel
    {
        public string RecieverId  { get; set; }
        public bool? IsRead { get; set; }
        //public bool? IsActionTaken { get; set; }
        public PaginatedItemsViewModel<NotificationCenterViewModel> PageInfo { get; set; } = null;
    }
}
