﻿using Newtonsoft.Json.Serialization;

namespace Ghanim.API
{
    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            //return propertyName.GetType().Name.ToCamelCaseString()
            return propertyName.ToLower();
        }

    }
}



