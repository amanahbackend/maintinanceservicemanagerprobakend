﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class OrderManagementReportPaginatedViewModel
    {
        public long Count { get; set; }
        public List<OrderManagementReportItem> Result { get; set; }
    }
}
