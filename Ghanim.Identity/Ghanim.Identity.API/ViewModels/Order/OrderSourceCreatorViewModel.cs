﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Order
{
    public class OrderSourceCreatorViewModel : BaseEntity
    {
        public string UserName { get; set; }
        public string ImgPath { get; set; }
    }
}
