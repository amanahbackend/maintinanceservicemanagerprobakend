﻿using CommonEnum;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Order
{
    public class OrderCardViewModel : BaseEntity
    {
        public double Long { get; set; }
        public double Lat { get; set; }
        public string Code { get; set; }
        public string CustomerName { get; set; }
        public string StatusName { get; set; }
        public string SAP_BlockName { get; set; }
        public string SAP_AreaName { get; set; }
        public string OrderTypeCode { get; set; }
        public string PriorityName { get; set; }
        public Nullable<DateTime> SAP_CreatedDate { get; set; }
        public bool IsExceedTime { get; set; }
        public bool IsRepeatedCall { get; set; }
        public AcceptenceType AcceptanceFlag { get; set; }
        public string RejectionReason { get; set; }
        public int? RankInTeam { get; set; }
        public int? TeamId { get; set; }
        public int? RankInDispatcher { get; set; }
        public int? SubStatusId { get; set; }
        public string SubStatusName { get; set; }
        public int? DispatcherId { get; set; }
        public int? PrevTeamId { get; set; }
    }
}
