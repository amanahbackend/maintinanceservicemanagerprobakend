﻿using Ghanim.API.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItemsViewModel;

namespace Ghanim.API
{
    public class PaginatedUnAssignedOrdersForDispatcherViewModel
    {
        public int DispatcherId { get; set; }
        public string OrderCode { get; set; }
        public PaginatedItemsViewModel<OrderCardViewModel> PageInfo { get; set; } = null;
    }
}
