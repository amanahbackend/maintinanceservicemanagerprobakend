﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.Models.Order.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Order
{
    public class OrderRemoteRequestsOnProcessViewModel : BaseEntity
    {
        public string Code { get; set; }
        public DateTime? VisitDate { get; set; }
        public DateTime? VisitCode { get; set; }
        public OrderRequestTypeEnum OrderRequestTypeId { get; set; }
    }
}
