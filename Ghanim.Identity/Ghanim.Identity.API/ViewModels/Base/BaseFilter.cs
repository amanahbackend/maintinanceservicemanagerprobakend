﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API.ViewModels.Base
{
    public class BaseFilter
    {
        public int PageNo { get; set; }

        public int PageSize { get; set; }

    }
}
