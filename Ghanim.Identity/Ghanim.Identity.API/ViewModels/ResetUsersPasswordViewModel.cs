﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.API
{
    public class ResetUsersPasswordViewModel
    {
        [Required]
        public List<string> Usernames { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}
