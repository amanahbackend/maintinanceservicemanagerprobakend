﻿using CommonEnum;
using Ghanim.API.Controllers;
using Ghanim.API.ServiceCommunications.DataManagementService.BuildingTypesService;
using Ghanim.API.ServiceCommunications.DataManagementService.CompanyCodeService;
using Ghanim.API.ServiceCommunications.DataManagementService.ContractTypesService;
using Ghanim.API.ServiceCommunications.DataManagementService.DivisionService;
using Ghanim.API.ServiceCommunications.DataManagementService.GovernoratesService;
using Ghanim.API.ServiceCommunications.GoogleApi;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherService;
using Ghanim.API.ServiceCommunications.UserManagementService.DispatcherSettingsService;
using Ghanim.API.ServiceCommunications.UserManagementService.SupervisorService;

using Ghanim.BLL.ExcelSettings;
using Ghanim.BLL.Managers;
using Ghanim.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Ghanim.API.Helper;
using Ghanim.BLL.Caching.Abstracts;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Services.FlixyApi;
using Ghanim.BLL.Settings;
using Ghanim.Models.Context;
using Microsoft.Extensions.Options;
using Utilites.ExcelToGenericList;
using Utilites.PACI;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Microsoft.Extensions.Localization;
using Ghanim.ResourceLibrary;

namespace Ghanim.API.Schedulers.Scheduling
{

    //Todo refactor Issue read from id colomns 
    public class ReadOrdersWorkFlow //: IReadOrderManager
    {
         
        OrderTypeManager orderTypeManager;
        OrderStatusManager orderStatusManager;
        OrderSubStatusManager orderSubStatusManager;
        OrderPriorityManager orderPriorityManager;
        OrderProblemManager orderProblemManager;
        OrderManager orderManager;
        ArchivedOrderFileManager archivedOrderFileManager;
        PACIManager paciManager;
        IContractTypesService contractTypesService;
        IAreasManager areaManager;
        IGeoLocationService geoLocationService;
        IBuildingTypesService buildingTypesService;
        ICompanyCodeService companyCodeService;
        IGovernoratesService governoratesService;
        IDivisionService divisionService;
         ICached<OrderStatusViewModel> cashedForOrderStatus; 
         ICached<OrderProblemViewModel> cashedForOrderProblem; 
       OrderRowDataManager orderRowDataManager;
        IDispatcherService dispatcherService;
        IDispatcherSettingsService dispatcherSettingsService;
        ISupervisorService supervisorService;
        ApplicationDbContext context;
        ILogger<ReadOrdersWorkFlow> logger;
        private readonly IHostingEnvironment _hostingEnv;
        public ReadOrdersWorkFlow(IServiceProvider serviceProvider, IHostingEnvironment hostingEnv )
        {
            _hostingEnv = hostingEnv;
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                cashedForOrderStatus = serviceProvider.GetService<ICached<OrderStatusViewModel>>();

                context = FileUploadSettings.GetDbContext();

                orderTypeManager = new OrderTypeManager(context);
                orderStatusManager = new OrderStatusManager(context, cashedForOrderStatus);
                orderSubStatusManager = new OrderSubStatusManager(context);
                orderPriorityManager = new OrderPriorityManager(context);
                archivedOrderFileManager = new ArchivedOrderFileManager(context);
                paciManager = new PACIManager(context);
                divisionService = serviceProvider.GetService<IDivisionService>();
                contractTypesService = serviceProvider.GetService<IContractTypesService>();
                areaManager = serviceProvider.GetService<IAreasManager>();
                geoLocationService = serviceProvider.GetService<IGeoLocationService>();
                buildingTypesService = serviceProvider.GetService<IBuildingTypesService>();
                governoratesService = serviceProvider.GetService<IGovernoratesService>();
                companyCodeService = serviceProvider.GetService<ICompanyCodeService>();
               var orderActionManager = serviceProvider.GetService<IOrderActionManager>();
                orderProblemManager = new OrderProblemManager(context, cashedForOrderProblem);
                orderRowDataManager = new OrderRowDataManager(context);
                logger = serviceProvider.GetService<ILogger<ReadOrdersWorkFlow>>();



                var helper = serviceProvider.GetService<IAPIHelper>();
                var flixyService = serviceProvider.GetService<IFlixyService>();

                var fileService = serviceProvider.GetService<IOptions<StaticFilesSettings>>();
                orderManager = new OrderManager(context, orderActionManager, helper, flixyService, fileService);
                dispatcherService = serviceProvider.GetService<IDispatcherService>();
                dispatcherSettingsService = serviceProvider.GetService<IDispatcherSettingsService>();
                supervisorService = serviceProvider.GetService<ISupervisorService>();
            }
            PACIHelper.Intialization(FileUploadSettings.proxyUrl,
                FileUploadSettings.paciServiceUrl,
                FileUploadSettings.paciNumberFieldName,
                FileUploadSettings.blockServiceUrl,
                FileUploadSettings.blockNameFieldNameBlockService,
                FileUploadSettings.nhoodNameFieldName,
                FileUploadSettings.streetServiceUrl,
                FileUploadSettings.blockNameFieldNameStreetService,
                FileUploadSettings.streetNameFieldName);

        }

        private FtpWebResponse DownloadFile(string ftpPath, string fileName)
        {
            try
            {
                FtpWebRequest ftpRequestRead = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath + "/" + fileName));
                //ftpRequestRead.EnableSsl = true;
                ftpRequestRead.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                //ftpRequestRead.UsePassive = true;
                //ftpRequestRead.Timeout = 30000;
                //ftpRequestRead.ReadWriteTimeout = 30000;
                ftpRequestRead.Method = WebRequestMethods.Ftp.DownloadFile;
                FtpWebResponse responseRead = (FtpWebResponse)ftpRequestRead.GetResponse();
                return responseRead;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Download Error");
                Console.WriteLine(ex.Message);
                logger.LogError("Download file: " + ex.Message, ex.InnerException?.Message);
                return null;
            }
        }
        private async Task MoveFile(string fileName)
        {
            try
            {
                var result = await archivedOrderFileManager.AddFileName(new ArchivedOrderFile { FileName = fileName });
                if (result?.IsSucceeded == false)
                {
                    logger.LogError("Moving file: " + result?.Exception?.Message, result?.Exception?.InnerException?.Message);
                }

                //if (result.IsSucceeded)
                //{
                //logger.LogError("file added");
                var sourceFolder = FileUploadSettings.SourceFilePath;
                var targetFolder = FileUploadSettings.TargetFilePath;

                Uri uriSource = new Uri(FileUploadSettings.HostName + "/" + sourceFolder + "/" + fileName);
                Uri uriDestination = new Uri(FileUploadSettings.HostName + "/" + targetFolder + "/" + fileName);

                Uri targetUriRelative = uriSource.MakeRelativeUri(uriDestination);

                //var url = new Uri(FileUploadSettings.HostName + "/" + sourceFolder + "/" + fileName);
                FtpWebRequest ftpRequestMove = (FtpWebRequest)WebRequest.Create(uriSource.AbsoluteUri);
                //ftpRequestMove.EnableSsl = true;
                ftpRequestMove.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                //ftpRequestMove.UsePassive = true;
                //ftpRequestMove.Timeout = 30000;
                //ftpRequestMove.ReadWriteTimeout = 30000;
                ftpRequestMove.Method = WebRequestMethods.Ftp.Rename;
                ftpRequestMove.RenameTo = Uri.UnescapeDataString(targetUriRelative.OriginalString);
                var responseMove = (FtpWebResponse)ftpRequestMove.GetResponse();
                responseMove.Dispose();
                //}
                //logger.LogError("file not moved");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Move Error");
                Console.WriteLine(ex.Message);
                logger.LogError("Moving file: " + ex.Message, ex.InnerException?.Message);
            }
        }
        private void DeleteFile(string ftpPath, string file)
        {
            try
            {
                FtpWebRequest ftpRequestDelete = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath + "/" + file));
                //ftpRequestDelete.EnableSsl = true;
                ftpRequestDelete.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                //ftpRequestDelete.UsePassive = true;
                //ftpRequestDelete.Timeout = 30000;
                //ftpRequestDelete.ReadWriteTimeout = 30000;
                ftpRequestDelete.Method = WebRequestMethods.Ftp.DeleteFile;
                FtpWebResponse responseDelete = (FtpWebResponse)ftpRequestDelete.GetResponse();
                responseDelete.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Delete Error");
                Console.WriteLine(ex.Message);
                logger.LogError("Delete file: " + ex.Message, ex.InnerException?.Message);
            }
        }

        private OrderRowData GenerateOrderRowData(string[] values)
        {
            OrderRowData orderRow = new OrderRowData();

            orderRow.AppartmentNo = values[(int)ExcelSheetProperties.ApartmentNo].ToInt32();
            orderRow.AreaCode = values[(int)ExcelSheetProperties.Area].ToInt32();
            orderRow.AreaDescription = values[(int)ExcelSheetProperties.AreaDescription].ToString();
            orderRow.Block = values[(int)ExcelSheetProperties.Block].ToString();
            orderRow.CompanyCode = values[(int)ExcelSheetProperties.OrderCompanyCode].ToString();
            if (values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != null && values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != "")
            {
                orderRow.ContactExpiraion = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractExpirationDate].ToString(), "dd.MM.yyyy", null);
            }
            if (values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != null && values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != "")
            {
                orderRow.ContractDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractValidFrom].ToString(), "dd.MM.yyyy", null);
            }
            orderRow.ContractNo = values[(int)ExcelSheetProperties.ContractNo].ToString();
            orderRow.ContractType = values[(int)ExcelSheetProperties.ContractType].ToString();
            orderRow.ContractTypeDescription = values[(int)ExcelSheetProperties.ContractTypeDescription].ToString();
            orderRow.CustomerName = values[(int)ExcelSheetProperties.CustomerName].ToString();
            orderRow.CustomerNo = values[(int)ExcelSheetProperties.CustomerId].ToString();
            orderRow.Division = values[(int)ExcelSheetProperties.OrderDivision].ToInt32();
            orderRow.DivisionDescription = values[(int)ExcelSheetProperties.OrderDivisionDescription].ToString();
            orderRow.Floor = values[(int)ExcelSheetProperties.Floor].ToString();
            orderRow.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
            orderRow.House = values[(int)ExcelSheetProperties.HouseKasima].ToString();
            if (values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != null && values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != "")
            {
                orderRow.OrderDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString(), "dd.MM.yyyy HH:mm:ss", null);
            }
            orderRow.OrderNo = values[(int)ExcelSheetProperties.OrderNo].ToString();
            orderRow.OrderNoteAgent = values[(int)ExcelSheetProperties.OrderNoteICAgent].ToString();
            orderRow.OrderPriority = values[(int)ExcelSheetProperties.OrderPriority].ToInt32();
            orderRow.OrderPriorityDescription = values[(int)ExcelSheetProperties.OrderPriorityDescription].ToString();
            orderRow.OrderStatus = values[(int)ExcelSheetProperties.OrderStatusDescription].ToString();
            orderRow.OrderType = values[(int)ExcelSheetProperties.OrderType].ToString();
            orderRow.OrderTypeDescription = values[(int)ExcelSheetProperties.OrderTypeDescription].ToString();
            orderRow.PACI = values[(int)ExcelSheetProperties.Paci].ToString();
            orderRow.PhoneOne = values[(int)ExcelSheetProperties.Phone1].ToString();
            orderRow.PhoneTwo = values[(int)ExcelSheetProperties.Phone2].ToString();
            orderRow.Problem = values[(int)ExcelSheetProperties.OrderProblem1].ToString();
            orderRow.ProblemDescription = values[(int)ExcelSheetProperties.OrderProblem1Description].ToString();
            orderRow.Street = values[(int)ExcelSheetProperties.StreetJaddah].ToString();
            orderRow.Caller_ID = values[(int)ExcelSheetProperties.Caller_ID].ToString();
            orderRow.OrderDescription = values[(int)ExcelSheetProperties.OrderDescription].ToString();

            return orderRow;
        }

        private async Task<OrderObject> GenerateOrderObject(string[] values, string fileName)
        {
            OrderObject temp = new OrderObject();

            #region OrderViewModelInitialization
            DateTime? cDate = null;
            if (values[5] != "")
            {
                var date = values[5].ToString();
                int len = date.Length;
                cDate = DateTime.ParseExact(values[5].ToString(), "dd.MM.yyyy HH:mm:ss", null);
            }

            temp.Code = values[(int)ExcelSheetProperties.OrderNo].ToString();
            var TypeName = values[(int)ExcelSheetProperties.OrderTypeDescription].ToString();
            temp.OrderTypeCode = values[(int)ExcelSheetProperties.OrderType].ToString();
            temp.Caller_ID = values[(int)ExcelSheetProperties.Caller_ID].ToString();
            temp.OrderDescription = values[(int)ExcelSheetProperties.OrderDescription].ToString();

            try
            {
                var type = orderTypeManager.Get(x => x.Name == TypeName).Data;
                temp.TypeId = (type != null) ? type.Id : 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in type binding");
            }

            //temp.StatusName = StaticAppSettings.InitialStatus.Name;
            temp.StatusId = StaticAppSettings.InitialStatus.Id;
            temp.SubStatusId = StaticAppSettings.InitialSubStatus.Id;
            var stringsubstatus = StaticAppSettings.InitialSubStatus.Name;
            var stringPriority = values[(int)ExcelSheetProperties.OrderPriorityDescription].ToString();

            try
            {
                var priority = orderPriorityManager.Get(x => x.Name == stringPriority).Data;
                temp.PriorityId = (priority != null) ? priority.Id : 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in priority binding");
            }

          var Companycode = values[(int)ExcelSheetProperties.OrderCompanyCode].ToString();

            try
            {
                var companyCodeRes = await companyCodeService.GetByCode(Companycode);
                if (companyCodeRes != null && companyCodeRes.Data != null)
                {
                    var companyCode = companyCodeRes.Data;
                    temp.CompanyCodeId = (companyCode != null) ? companyCode.Id : 0;
                }

            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in company code binding");
            }
            if (values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != null && values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString() != "")
            {
                temp.SAP_CreatedDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.OrderCreatedDateTime].ToString(), "dd.MM.yyyy HH:mm:ss", null);
            }
           var divisionName = values[(int)ExcelSheetProperties.OrderDivisionDescription].ToString();

            try
            {
                var divisionRes = await divisionService.GetByName(divisionName);
                if (divisionRes != null && divisionRes.Data != null)
                {
                    var division = divisionRes.Data;
                    temp.DivisionId = (division != null) ? division.Id : 0;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in division binding");
            }

            string proplem = values[(int)ExcelSheetProperties.OrderProblem1Description].ToString();
            try
            {
                var problem = orderProblemManager.Get(x => x.Name == proplem).Data;
                temp.ProblemId = (problem != null) ? problem.Id : 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in problem binding");
            }

            temp.ICAgentNote = values[(int)ExcelSheetProperties.OrderNoteICAgent].ToString();
            temp.ContractCode = values[(int)ExcelSheetProperties.ContractNo].ToString();

            string contactName = values[(int)ExcelSheetProperties.ContractTypeDescription].ToString();

            try
            {
                var contractRes = await contractTypesService.GetByName(contactName);
                if (contractRes != null && contractRes.Data != null)
                {
                    var contract = contractRes.Data;
                    temp.ContractTypeId = (contract != null) ? contract.Id : 0;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in contract binding");
            }
            if (values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != null && values[(int)ExcelSheetProperties.ContractValidFrom].ToString() != "")
            {
                temp.ContractStartDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractValidFrom].ToString(), "dd.MM.yyyy", null);
            }
            else
            {
                temp.ContractStartDate = null;
            }
            if (values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != null && values[(int)ExcelSheetProperties.ContractExpirationDate].ToString() != "")
            {
                temp.ContractExpiryDate = DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractExpirationDate].ToString(), "dd.MM.yyyy", null);
            }
            else
            {
                temp.ContractExpiryDate = null;
            }
            temp.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
            temp.CustomerCode = values[(int)ExcelSheetProperties.CustomerId].ToString();
            temp.CustomerName = values[(int)ExcelSheetProperties.CustomerName].ToString();
            temp.PhoneOne = values[(int)ExcelSheetProperties.Phone1].ToString();
            temp.PhoneTwo = values[(int)ExcelSheetProperties.Phone2].ToString();
            temp.SAP_PACI = values[(int)ExcelSheetProperties.Paci].ToString();
            temp.SAP_AreaName = values[(int)ExcelSheetProperties.AreaDescription].ToString();

            //try
            //{
            //    var area = areasManager.Get(x => x.Name == temp.AreaName).Data;
            //    temp.AreaId = (area != null) ? area.Id : 0;
            //}
            //catch (Exception ex)
            //{
            //    logger.LogError(ex.Message, "error in area binding");
            //}

            //temp.BlockId = values[(int)ExcelSheetProperties.Block].ToInt32();
            temp.SAP_BlockName = values[(int)ExcelSheetProperties.Block].ToString();
            temp.SAP_StreetName = values[(int)ExcelSheetProperties.StreetJaddah].ToString();
            temp.SAP_HouseKasima = values[(int)ExcelSheetProperties.HouseKasima].ToString();
            temp.SAP_Floor = values[(int)ExcelSheetProperties.Floor].ToString();
            temp.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].ToString();
            temp.SAP_AppartmentNo = values[(int)ExcelSheetProperties.ApartmentNo].ToString();

            try
            {
                string name = values[(int)ExcelSheetProperties.BuildingType].ToString();
                var buildingTypeRes = await buildingTypesService.GetByName(name);
                if (buildingTypeRes != null && buildingTypeRes.Data != null)
                {
                    var buildingType = buildingTypeRes.Data;
                    temp.BuildingTypeId = (buildingType != null) ? buildingType.Id : 0;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message + " " + ex.StackTrace + " " + ex.InnerException?.Message);
            }
            temp.FileName = fileName;
            temp.InsertionDate = DateTime.Now;
            temp.AcceptanceFlag = AcceptenceType.NoAction;
            temp.IsAccomplish = AccomplishType.Working;
            #endregion OrderViewModelInitialization

            //Address intialization from PACI
            #region AddressIntialization
            if (!string.IsNullOrEmpty(temp.SAP_PACI))
            {
                var paciData = new PACIHelper.PACIInfo();
                var savedPaci = paciManager.Get(x => x.PACINumber == temp.SAP_PACI).Data;
                if (savedPaci != null)
                {
                    temp.PACI = temp.SAP_PACI;
                    //temp.AreaName = savedPaci.AreaName;
                    temp.GovName = savedPaci.GovernorateName;
                    temp.Lat = (double)savedPaci.Latitude;
                    temp.Long = (double)savedPaci.Longtiude;
                    temp.StreetName = savedPaci.StreetName;
                    temp.StreetId = savedPaci.StreetId;
                    temp.BlockName = savedPaci.BlockName;
                    temp.BlockId = savedPaci.BlockId;
                    temp.AppartmentNo = savedPaci.AppartementNumber;
                    temp.Floor = savedPaci.FloorNumber;
                    temp.HouseKasima = savedPaci.BuildingNumber;
                }
                else
                {
                    int round = 1;
                    while (round < 3 && paciData.Latitude == null && paciData.Longtiude == null)
                    {
                        paciData = PACIHelper.GetLocationByPACI(int.Parse(temp.SAP_PACI));
                        if (paciData == null)
                            break;
                        round++;
                    }
                    if (paciData != null)
                    {
                        if (paciData.Latitude != null && paciData.Longtiude != null)
                        {
                            temp.PACI = temp.SAP_PACI;
                            //temp.AreaName = paciData.Area?.Name;
                            temp.GovName = paciData.Governorate?.Name;
                            temp.Lat = (double)paciData.Latitude;
                            temp.Long = (double)paciData.Longtiude;
                            temp.StreetName = paciData.Street;
                            temp.StreetId = 0;
                            temp.BlockName = paciData.Block;
                            temp.BlockId = 0;
                            temp.AppartmentNo = paciData.AppartementNumber;
                            temp.Floor = paciData.FloorNumber;
                            temp.HouseKasima = paciData.BuildingNumber;

                            paciManager.AddRow(new PACI
                            {
                                PACINumber = temp.SAP_PACI,
                                AreaName = paciData.Area?.Name,
                                AreaId = int.TryParse(paciData.Area?.Id, out int c) ? int.Parse(paciData.Area?.Id) : 0,
                                GovernorateName = paciData.Governorate?.Name,
                                GovernorateId = int.TryParse(paciData.Governorate?.Id, out int d) ? int.Parse(paciData.Governorate?.Id) : 0,
                                BlockName = paciData.Block,
                                BlockId = 0,
                                StreetName = paciData.Street,
                                StreetId = 0,
                                Longtiude = (decimal)paciData.Longtiude,
                                Latitude = (decimal)paciData.Latitude,
                                FloorNumber = paciData.FloorNumber,
                                AppartementNumber = paciData.AppartementNumber,
                                BuildingNumber = paciData.BuildingNumber
                            });

                            //try
                            //{
                            //    var areaRes = await areaService.GetByName(paciData.Area.Name);
                            //    if (areaRes != null && areaRes.Data != null)
                            //    {
                            //        var area = areaRes.Data;
                            //        if (area == null)
                            //        {
                            //            areaRes = await areaService.Add(new AreasViewModel() { Name = paciData.Area.Name });
                            //            area = areaRes.Data;
                            //        }
                            //        temp.AreaName = paciData.Area?.Name;
                            //        temp.AreaId = (area != null) ? area.Id : 0;
                            //    }
                            //    else
                            //    {
                            //        var areaViewModel = new AreasViewModel()
                            //        {
                            //            Name = paciData.Area.Name
                            //        };

                            //        areaRes = await areaService.Add(areaViewModel);

                            //        if (areaRes != null && areaRes.Data != null)
                            //        {
                            //            var area = areaRes.Data;
                            //            if (area != null)
                            //            {
                            //                temp.AreaName = paciData.Area?.Name;
                            //                temp.AreaId = (area != null) ? area.Id : 0;
                            //            }
                            //        }
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    logger.LogError(ex.Message, "error in area binding");
                            //}

                            //try
                            //{
                            //    var govRes = await governoratesService.GetByName(paciData.Governorate.Name);
                            //    var gov = govRes.Data;
                            //    temp.GovName = (gov != null) ? gov.Name : null;
                            //    temp.GovId = (gov != null) ? gov.Id : 0;
                            //}
                            //catch (Exception ex)
                            //{
                            //    logger.LogError(ex.Message, "error in gov binding");
                            //}
                        }
                    }
                    //}

                    //else
                    //{
                    //temp.PACI = temp.SAP_PACI;
                    //temp.Lat = 0;
                    //temp.Long = 0;
                    //temp.StreetName = temp.SAP_StreetName;
                    //temp.StreetId = 0;
                    //temp.BlockName = temp.SAP_BlockName;
                    //temp.BlockId = 0;
                    //temp.AppartmentNo = temp.SAP_AppartmentNo;
                    //temp.Floor = temp.SAP_Floor;
                    //temp.HouseKasima = temp.SAP_HouseKasima;
                    //}
                }
            }
            try
            {
                if ((decimal)temp.Lat == decimal.Zero || (decimal)temp.Long == decimal.Zero)
                {
                    var address = temp.SAP_AreaName.Trim();
                    if (!string.IsNullOrEmpty(temp.SAP_BlockName))
                    {
                        address += "+block" + temp.SAP_BlockName.Trim();
                    }
                    if (!string.IsNullOrEmpty(temp.SAP_StreetName) && !temp.SAP_StreetName.ToLower().Trim().Contains("no street"))
                    {
                        if (int.TryParse(temp.SAP_StreetName.Trim(), out int n))
                        {
                            address += "+street " + temp.SAP_StreetName.Trim();
                        }
                        else
                        {
                            address += "+" + temp.SAP_StreetName.Trim();
                        }
                    }

                    var addressencoded = Uri.EscapeUriString(address);
                    var response = await geoLocationService.GetGeoLocation(addressencoded);
                    if (response.Data != null)
                    {
                        JArray items = (JArray)response.Data.results;
                        if (items.Count > 0)
                        {
                            temp.Lat = response.Data.results[0].geometry.location.lat;
                            temp.Long = response.Data.results[0].geometry.location.lng;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message + " " + ex.StackTrace + " " + ex.InnerException?.Message);
            }

            try
            {
                if (string.IsNullOrEmpty(temp.SAP_AreaName))
                {
                    //temp.SAP_AreaName = temp.AreaName;
                }

                if (!string.IsNullOrEmpty(temp.SAP_AreaName))
                {
                    var areaRes = areaManager.GetByName(temp.SAP_AreaName);
                    if (areaRes != null)
                    {
                        var area = areaRes.Data;
                        if (area == null)
                        {
                            areaRes = areaManager.Add(new Areas() { Name = temp.SAP_AreaName });
                            area = areaRes.Data;
                        }

                        //temp.AreaName = temp.SAP_AreaName;
                        temp.AreaId = area?.Id ?? 0;
                    }
                    else
                    {
                        var areaViewModel = new Areas()
                        {
                            Name = temp.SAP_AreaName
                        };

                        areaRes = areaManager.Add(areaViewModel);

                        if (areaRes != null)
                        {
                            var area = areaRes.Data;
                            if (area != null)
                            {
                                //temp.AreaName = temp.SAP_AreaName;
                                temp.AreaId = area.Id;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in area binding");
            }

            try
            {
                if (!string.IsNullOrEmpty(temp.SAP_GovName))
                {
                    var govRes = await governoratesService.GetByName(temp.SAP_GovName);
                    if (govRes != null)
                    {
                        var gov = govRes.Data;
                        //temp.GovName = (gov != null) ? gov.Name : temp.SAP_GovName;
                        temp.GovId = (gov != null) ? gov.Id : 0;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, "error in gov binding");
            }
            #endregion AddressIntialization

            //Assign to Dispatcher
            #region AssignToDispatcher
            SettingsViewModel orderSettings = new SettingsViewModel()
            {
                AreaId = temp.AreaId,
                DivisionId = temp.DivisionId,
                ProblemId = temp.ProblemId
            };
            try
            {
                var dispatchers = dispatcherSettingsService.GetDispatcherSettingsByOrder(orderSettings);
                if (dispatchers.IsSucceeded && dispatchers.Data != null)
                {
                    Random randNum = new Random();

                    temp.DispatcherId = dispatchers.Data[randNum.Next(dispatchers.Data.Count)].DispatcherId;

                    var dispatcherDetails = dispatcherService.GetDispatcherById(temp.DispatcherId ?? 0);//TODO Refactor issue
                    if (dispatcherDetails.IsSucceeded && dispatcherDetails.Data != null)
                    {
                        //temp.DispatcherName = dispatcherDetails.Data.Name;
                        temp.SupervisorId = dispatcherDetails.Data.SupervisorId;
                        //temp.SupervisorName = dispatcherDetails.Data.SupervisorName;
                    }
                }
                else
                {
                    //Handle assign to supervisor
                    var supervisors = supervisorService.GetSupervisorsByDivisionId(temp.DivisionId ?? 0);//TODO Refactor issue
                    if (supervisors.IsSucceeded && supervisors.Data.Count > 0)
                    {
                        Random randNum = new Random();

                        temp.SupervisorId = supervisors.Data[randNum.Next(supervisors.Data.Count)].Id;
                        //temp.SupervisorName = supervisors.Data[randNum.Next(supervisors.Data.Count)].Name;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message + " " + ex.StackTrace + " " + ex.InnerException?.Message);
            }
            #endregion AssignToDispatcher
            return temp;
        }

        private byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public async void ReadFile()
        {
            //Start:
            try
            {
                var ftpPath = FileUploadSettings.HostName + "/" + FileUploadSettings.SourceFilePath;
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath));
                //ftpRequest.EnableSsl = true;
                ftpRequest.Credentials = new NetworkCredential(FileUploadSettings.Username, FileUploadSettings.Password);
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream());

                string file = string.Empty;
                string streamLine = streamReader.ReadLine();
                while (!string.IsNullOrEmpty(streamLine))
                {
                    if (streamLine.Contains(FileUploadSettings.FileExtention))
                    {
                        file = streamLine;
                        break;
                    }
                    streamLine = streamReader.ReadLine();
                }
                streamReader.Close();
                response.Dispose();

                if (!string.IsNullOrEmpty(file))
                {
                    var archivedFiles = archivedOrderFileManager.GetAll(x => x.FileName == file);

                    if (archivedFiles.Data != null)
                    {
                        if (archivedFiles.Data.Count() > 0)
                        {
                            //logger.LogError("file exist");
                            DeleteFile(ftpPath, file);
                            //goto Start;
                        }
                    }

                    //logger.LogError("file not exist");
                    if (archivedOrderFileManager.Count().Data > 200)
                    {
                        archivedOrderFileManager.DeleteAll();
                    }

                    var responseRead = DownloadFile(ftpPath, file);
                    if (responseRead != null)
                    {
                        var stream = responseRead.GetResponseStream();
                        var bytes = ReadFully(stream);
                        var memoryStream = new MemoryStream(bytes);
                        stream.Dispose();
                        var reader = new StreamReader(memoryStream);
                        List<OrderObject> orderList = new List<OrderObject>();
                        List<OrderRowData> orderRowDataList = new List<OrderRowData>();
                        bool isFirst = true;
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(',');

                            if (isFirst == false)
                            {
                                if (values != null && values.Length > 0)
                                {

                                    try
                                    {
                                        var orderObject = await GenerateOrderObject(values, file);
                                        orderList.Add(orderObject);

                                        var orderRow = GenerateOrderRowData(values);
                                        orderRowDataList.Add(orderRow);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("Entity of type \"{0}\" with error \"{1}\"", e.GetType(), e.Message);
                                        logger.LogError(e.Message + " " + e.StackTrace + " " + e.InnerException?.Message);
                                    }
                                }
                            }
                            else
                            {
                                isFirst = false;
                            }
                        }

                        try
                        {
                            orderManager.Add(orderList);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            logger.LogError(ex.Message + " " + ex.StackTrace + " " + ex.InnerException?.Message);
                        }

                        orderRowDataManager.Add(orderRowDataList);

                        Console.WriteLine(string.Format("End of file {0}", file));
                        reader.Close();
                        memoryStream.Dispose();
                        //logger.LogError("about to move file");
                        await MoveFile(file);
                        Console.WriteLine(" \r \n Hi  \r \n  Task Finish press any key to close. \r \n Thanks");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("ISSUE");
                logger.LogError(ex.Message + " " + ex.StackTrace + " " + ex.InnerException?.Message);
            }
        }
    }
}
