﻿using AutoMapper;
using Ghanim.BLL.Managers;
using Ghanim.Models.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Ghanim.API.Helper;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Services.FlixyApi;
using Ghanim.BLL.Settings;
using Ghanim.Models.Context;
using Microsoft.Extensions.Options;

namespace Ghanim.API.Schedulers.TaskFlow
{
    public class ArchiveOrderFlow
    {
        ApplicationDbContext context;
        private readonly ILogger<ArchiveOrderFlow> logger;

        private readonly new IMapper mapper;

        ArchivedOrderManager archivedOrderManager;
        OrderManager orderManager;
        OrderSettingManager orderSettingManager;

        public ArchiveOrderFlow(IServiceProvider _serviceProvider, IMapper _mapper)
        {
            context = StaticAppSettings.GetDbContext();
            archivedOrderManager = new ArchivedOrderManager(context);
            var orderActionManager = _serviceProvider.GetService<IOrderActionManager>();
            var helper = _serviceProvider.GetService<IAPIHelper>();
            var flixyService = _serviceProvider.GetService<IFlixyService>();
            var fileService = _serviceProvider.GetService<IOptions<StaticFilesSettings>>();

            orderManager = new OrderManager(context, orderActionManager, helper, flixyService, fileService);
            orderSettingManager = new OrderSettingManager(context);
            mapper = _mapper;
        }

        public async void Archive()
        {
            var archiveAfterDays = orderSettingManager.GetAllQuerable().Data.Where(x => x.Name == StaticAppSettings.ArchiveAfterDays).FirstOrDefault().Value;
            int daysAfter = Convert.ToInt32(archiveAfterDays);

            DateTime currentTime = DateTime.Now;

            var orders = orderManager.GetAllQuerable().Data.Where(x => StaticAppSettings.ArchivedStatusId.Contains(x.StatusId) && (currentTime - x.CreatedDate).TotalDays >= daysAfter).ToList();

            if (orders.Count > 0)
            {
                var archivedOrders = mapper.Map<List<OrderObject>, List<ArchivedOrder>>(orders);
                var archiveResult = archivedOrderManager.Add(archivedOrders);
                if (archiveResult.IsSucceeded)
                {
                    orderManager.Delete(orders);
                }
            }
        }

    }
}
