﻿using Ghanim.API.Schedulers.TaskFlow;
using Ghanim.BLL.IManagers;
using Ghanim.BLL.Managers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ghanim.Models.Context;

namespace Ghanim.API.Schedulers
{
    public class ArchiveOrderTask : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private Timer _timer;
        private ArchiveOrderFlow _archiveOrderFlow;

        private OrderSettingManager _orderSettingManager;
        ApplicationDbContext _context; 

        public ArchiveOrderTask(ILogger<ArchiveOrderTask> logger,
            IServiceProvider serviceProvider)
        {
            _logger = logger;

            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                _context = StaticAppSettings.GetDbContext();
                _archiveOrderFlow = scope.ServiceProvider.GetRequiredService<ArchiveOrderFlow>();
                _orderSettingManager = new OrderSettingManager(_context);
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");

            // to run at mid-night
            int atTime = 24 - DateTime.Now.Hour;

            var ArchiveAfter = _orderSettingManager.GetAllQuerable().Data.Where(x => x.Name == StaticAppSettings.ArchiveAfterDays).FirstOrDefault().Value;
            int fromDays = Convert.ToInt32(ArchiveAfter);

            _timer = new Timer(DoWork, null, TimeSpan.FromHours(atTime), //TimeSpan.Zero,
                TimeSpan.FromDays(fromDays));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _logger.LogInformation("Timed Background Service is working.");
            _archiveOrderFlow.Archive();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
