﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.API.ViewModels.Order;
using System.Collections.Generic;
using Utilites.PaginatedItemsViewModel;
using Utilites.ProcessingResult;

namespace Ghanim.API.Models
{
    public class DispatcherAllOrdersViewModel
    {
        public List< DispatcherAssignedOrdersViewModel> AssignedOrders { get; set; }
        public PaginatedItemsViewModel<OrderCardViewModel> UnAssignedOrders { get; set; }
    }
}
