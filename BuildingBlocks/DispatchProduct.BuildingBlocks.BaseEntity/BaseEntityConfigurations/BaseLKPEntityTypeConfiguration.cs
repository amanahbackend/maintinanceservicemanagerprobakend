﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations
{
    public class BaseLKPEntityTypeConfiguration<T>: BaseEntityTypeConfiguration<T>
        where T : class, IBaseLKPEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> BaseEntityConfiguration)
        {
            base.Configure(BaseEntityConfiguration);
            BaseEntityConfiguration.Property(o => o.NameAR).IsRequired();
            BaseEntityConfiguration.Property(o => o.NameEN).IsRequired();
            BaseEntityConfiguration.Property(o => o.IsSystemKey).IsRequired(false);
        }
    }
}
