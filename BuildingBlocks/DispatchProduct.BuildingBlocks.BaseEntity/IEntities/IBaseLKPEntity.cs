﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.BuildingBlocks.BaseEntities.IEntities
{
    public interface IBaseLKPEntity : IBaseEntity
    {
        string NameAR { get; set; }
        string NameEN { get; set; }
        bool? IsSystemKey { get; set; }
    }
}
