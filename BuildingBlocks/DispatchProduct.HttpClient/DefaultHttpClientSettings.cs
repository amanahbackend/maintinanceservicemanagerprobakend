﻿using DnsClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchProduct.HttpClient
{
    public class DefaultHttpClientSettings : IHttpClientSettings
    {

        public  virtual string Uri
        {
            get; set;
        }
        public virtual string Version
        {
            get; set;
        }
        public virtual string Controller
        {
            get; set;
        }
        public bool UsingIIS { get; set; }
        public async Task<string> GetUri(IDnsQuery dnsQuery)
        {
            string result = null;
            if (UsingIIS)
            {
                result= $"{Uri}/{Version}/{Controller}";
            }
            //else
            //{
            //    result = $"{await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName)}/{Version}/{Controller}";
            //}
            return result;
        }
        public string ServiceName { get; set; }
        public virtual string GetVerb
        {
            get
            {
                return "Get";
            }
        }
        public virtual string GetByIdsVerb
        {
            get
            {
                return "GetByIds";
            }
        }
        public virtual string GetAllVerb
        {
            get {
                return "GetAll";
            }
        }
        public virtual string PutVerb
        {
            get
            {
                return "Update";
            }
        }
        public virtual string PostVerb
        {
            get
            {
                return "Add";
            }
        }
        public virtual string DeleteVerb
        {
            get
            {
                return "Delete";
            }
        }
        public virtual string PatchVerb
        {
            get
            {
                return "";
            }
        }
    }
}
