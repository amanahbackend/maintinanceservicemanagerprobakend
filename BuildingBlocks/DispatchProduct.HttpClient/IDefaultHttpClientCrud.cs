﻿using DispatchProduct.Api.HttpClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.HttpClient
{
    public interface IDefaultHttpClientCrud<T,TIn, TOut> where T: IHttpClientSettings
    {

        Task<ProcessResultViewModel<List<TOut>>> GetList(string auth = "");
        Task<ProcessResultViewModel<List<TOut>>> GetList(string requestUri, string auth = "");
        Task<ProcessResultViewModel<List<TOutCustomize>>> GetListCustomized<TOutCustomize>(string requestUri, string auth = "");

        Task<ProcessResultViewModel<TOut>> GetItem(string id, string auth = "");
        Task<ProcessResultViewModel<TOut>> Get(string requesturi, string auth = "");
        Task<ProcessResultViewModel<TOutCustomize>> GetCustomized<TOutCustomize>(string requesturi, string auth = "");

        Task<ProcessResultViewModel<List<TOut>>> GetByIds<TInCustomize>(TInCustomize model, string auth = "");
        Task<ProcessResultViewModel<List<TOut>>> GetByIds<TInCustomize>(string requestUri, TInCustomize model, string auth = "");

        Task<ProcessResultViewModel<TOut>> Post(TIn model, string auth = "");
        Task<ProcessResultViewModel<TOut>> Post(string requesturi, TIn model, string auth = "");
        Task<ProcessResultViewModel<TOutCustomize>> PostCustomize<TInCustomize, TOutCustomize>(string requesturi, TInCustomize model, string auth = "");

        Task<ProcessResultViewModel<TOut>> Put(TIn model, string id, string auth = "");
        Task<ProcessResultViewModel<TOut>> Put(string requesturi, TIn model, string auth = "");
        Task<ProcessResultViewModel<TOutCustomize>> PutCustomize<TInCustomize, TOutCustomize>(string requesturi, TInCustomize model, string id, string auth = "");



        Task<ProcessResultViewModel<TOut>> GetByUri(string requesturi, string auth = "");
        Task<ProcessResultViewModel<TOutCustomize>> GetByUriCustomized<TOutCustomize>(string requesturi, string auth = "");

        Task Patch(string id, string auth = "");

        Task Delete(string id, string auth = "");
        Task Delete(string uri, string id, string auth = "");

        Task<ProcessResultViewModel<List<TOut>>> GetListByUri(string requesturi, string auth = "");
        Task<ProcessResultViewModel<List<TOutCustomize>>> GetListByUriCustomize<TOutCustomize>(string requesturi, string auth = "");
    }
}
