﻿using DnsClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchProduct.HttpClient
{
    public interface IHttpClientSettings
    {
        string Uri { get; }
        string GetVerb { get; }
        string GetByIdsVerb { get; }
        string GetAllVerb { get; }
        string PutVerb { get; }
        string PostVerb { get; }
        string DeleteVerb { get; }
        string PatchVerb { get; }
        Task<string> GetUri(IDnsQuery dnsQuery);
    }
}
