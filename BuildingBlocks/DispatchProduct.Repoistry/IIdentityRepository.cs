﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace DispatchProduct.RepositoryModule
{
    public interface IIdentityRepository<TEntity> where TEntity : class
    {
        ProcessResult<IQueryable<TEntity>> GetAllQuerable();
        ProcessResult<List<TEntity>> GetAll();
        ProcessResult<TEntity> Get(params object[] id);
        ProcessResult<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        ProcessResult<TEntity> Add(TEntity entity);
        ProcessResult<List<TEntity>> Add(List<TEntity> entityLst);
        ProcessResult<bool> Update(TEntity entity);
        ProcessResult<bool> Delete(TEntity entity);
        ProcessResult<bool> Delete(List<TEntity> entitylst);
        ProcessResult<bool> DeleteById(params object[] id);
        int SaveChanges();
    }
}
