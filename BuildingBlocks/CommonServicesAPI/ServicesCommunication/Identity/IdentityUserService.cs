﻿using CommonServicesAPI.ServicesSettings.Identity;
using CommonServicesAPI.ServicesViewModels.Identity;
using DispatchProduct.Api.HttpClient;
using DispatchProduct.HttpClient;
using DnsClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace CommonServicesAPI.ServicesCommunication.Identity
{
    public class IdentityUserService
       : DefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>,
       IIdentityUserService
    {
        IdentityServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public IdentityUserService(IOptions<IdentityServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> AddCustomerUser(string version, ApplicationUserViewModel user)
        {
            ProcessResultViewModel<ApplicationUserViewModel> result = null;
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.AddUserAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Customer" };
                result = await Post(url, user);
            }
            return result;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerUser(string version, ApplicationUserViewModel user)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.UpdateUserAction;
                var url = $"{baseUrl}/{version}/{requestedAction}";
                user.RoleNames = new List<string>() { "Customer" };
                return await Put(url, user);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string version, string userId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetUserById;
                var url = $"{baseUrl}/{version}/{requestedAction}?id={userId}";
                return await Get(url);
            }
            return null;
        }

        public async Task DeleteById(string version, string userId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.DeleteUserById;
                var url = $"{baseUrl}/{version}/{requestedAction}?id={userId}";
                await HttpRequestFactory.Delete(url);
            }
        }
    }
}
