﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CommonServicesAPI.ServicesViewModels.Identity 
{
    public class ApplicationUserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string PicturePath { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public string Password { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public List<string> RoleNames { get; set; }
        public string Picture { get; set; }
        public int? Fk_Country_Id { get; set; }
        public string CountryCode { get; set; }
    }
}
