﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.RepositoryModule;
using AutoMapper;
using Utilites.ProcessingResult;
using Utilites.PaginatedItems;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Utilites.PaginatedItemsViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Controllers
{

    public class HealthCheckBaseController : Controller
    {
        public HealthCheckBaseController()
        {
        }

        public virtual IActionResult Ping()
        {
            return Ok("I'm fine");
        }
    }
}
