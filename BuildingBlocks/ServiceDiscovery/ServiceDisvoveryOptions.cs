﻿using System;
using System.Linq;
using System.Net;

namespace BuildingBlocks.ServiceDiscovery
{
    public class ServiceDisvoveryOptions
    {
        public string ServiceName { get; set; }

        public ConsulOptions Consul { get; set; }

        public string HealthCheckTemplate { get; set; }

        public string[] Endpoints { get; set; }
    }

    public class ConsulOptions
    {
        public string HttpEndpoint { get; set; }

        public DnsEndpoint DnsEndpoint { get; set; }
    }

    public class DnsEndpoint
    {
        public string Address { get; set; }

        public int Port { get; set; }

        public IPEndPoint ToIPEndPoint()
        {
            var ips = Dns.GetHostAddresses(Address);
            var ip = ips[0].ToString();
            return new IPEndPoint(IPAddress.Parse(ip), Port);
        }
    }
}
