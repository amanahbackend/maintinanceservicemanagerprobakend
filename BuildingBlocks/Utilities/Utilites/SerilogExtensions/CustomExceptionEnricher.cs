﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Utilites.SerilogExtensions
{
    public class CustomExceptionEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var exception = logEvent.Exception;
            if (exception == null) return;

            var message = propertyFactory.CreateProperty("ExceptionMessage", exception.Message, destructureObjects: true);
            logEvent.AddPropertyIfAbsent(message);

            var source = propertyFactory.CreateProperty("ExceptionSource", exception.Source, destructureObjects: true);
            logEvent.AddPropertyIfAbsent(source);

            var type = propertyFactory.CreateProperty("ExceptionType", exception.GetType().Name, destructureObjects: true);
            logEvent.AddPropertyIfAbsent(type);

            var stackTrace = propertyFactory.CreateProperty("ExceptionStackTrace", exception.StackTrace.Replace(Environment.NewLine, " ").Replace(",", ";"), destructureObjects: true);
            logEvent.AddPropertyIfAbsent(stackTrace);

        }
    }
}
