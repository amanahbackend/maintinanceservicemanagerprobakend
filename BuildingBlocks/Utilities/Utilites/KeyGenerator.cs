﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Utilites.UniqueKey
{
    public class KeyGenerator
    {
        private static Random random = new Random();
        public static string GetUniqueKey(int maxSize,bool numbersOnly=false)
        {
            char[] chars = new char[62];
            if (numbersOnly)
            {
                chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            }
            else
            {
                chars =
                "1234567890".ToCharArray();
            }
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        public static string RandomString(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}