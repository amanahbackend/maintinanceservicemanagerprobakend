﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Utilites.UploadFile
{
    public interface IUploadExcelFileManager:IUploadFileManager
    {
        ProcessResult<string> AddFile(UploadFile file, string filePath);
    }
}
