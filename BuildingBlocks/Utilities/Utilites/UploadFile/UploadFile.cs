﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utilites.UploadFile
{
    public class UploadFile:BaseEntity, IUploadFile
    {
        public string FileContent { get; set; }
        public string FileName { get; set; }
        public string FileRelativePath { get; set; }
    }
}
