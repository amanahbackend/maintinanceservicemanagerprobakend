﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Utilites;
using Utilites.ProcessingResult;

namespace Utilites.UploadFile
{
    public interface IUploadFileManager
    {
         ProcessResult<string> AddFile(UploadFile file, string path);
         ProcessResult<List<string>> AddFiles(List<UploadFile> files, string path);
         ProcessResult<string> ISFileNull(UploadFile file);
    }
}
