﻿using AutoMapper;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Utilites
{


    public static class AutoMapperExtensions
    {
        public static IMappingExpression<TSource, TDestination> IgnoreBaseEntityProperties<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
         where TSource : IRepoistryBaseEntity
         where TDestination : IBaseEntity
        {
            mapping.ForMember(e => e.FK_CreatedBy_Id, c => c.Ignore());
            mapping.ForMember(e => e.FK_UpdatedBy_Id, c => c.Ignore());
            mapping.ForMember(e => e.FK_DeletedBy_Id, c => c.Ignore());
            mapping.ForMember(e => e.CreatedDate, c => c.Ignore());
            mapping.ForMember(e => e.UpdatedDate, c => c.Ignore());
            mapping.ForMember(e => e.DeletedDate, c => c.Ignore());
            mapping.ForMember(e => e.IsDeleted, c => c.Ignore());
            mapping.ForMember(e => e.CurrentUserId, c => c.Ignore());
            return mapping;
        }

        public static IMappingExpression<TSource, TDestination> IgnoreIdentityBaseEntityProperties<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
         where TDestination : IdentityBaseEntity
        {
            mapping.ForMember(e => e.FK_CreatedBy_Id, c => c.Ignore());
            mapping.ForMember(e => e.FK_UpdatedBy_Id, c => c.Ignore());
            mapping.ForMember(e => e.FK_DeletedBy_Id, c => c.Ignore());
            mapping.ForMember(e => e.CreatedDate, c => c.Ignore());
            mapping.ForMember(e => e.UpdatedDate, c => c.Ignore());
            mapping.ForMember(e => e.DeletedDate, c => c.Ignore());
            mapping.ForMember(e => e.IsDeleted, c => c.Ignore());

            return mapping;
        }

        public static IMappingExpression<TSource, TDestination> MapBaseEntityProperties<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
         where TSource : BaseEntity
         where TDestination : BaseEntity
        {
            mapping.ForMember(e => e.Id, c => c.MapFrom(src=>src.Id));
            mapping.ForMember(e => e.FK_CreatedBy_Id, c => c.MapFrom(src => src.FK_CreatedBy_Id));
            mapping.ForMember(e => e.FK_UpdatedBy_Id, c => c.MapFrom(src => src.FK_UpdatedBy_Id));
            mapping.ForMember(e => e.FK_DeletedBy_Id, c => c.MapFrom(src => src.FK_DeletedBy_Id));
            mapping.ForMember(e => e.CreatedDate, c => c.MapFrom(src => src.CreatedDate));
            mapping.ForMember(e => e.UpdatedDate, c => c.MapFrom(src => src.UpdatedDate));
            mapping.ForMember(e => e.DeletedDate, c => c.MapFrom(src => src.DeletedDate));
            mapping.ForMember(e => e.IsDeleted, c => c.MapFrom(src => src.IsDeleted));
            mapping.ForMember(e => e.CurrentUserId, c => c.MapFrom(src => src.CurrentUserId));
            return mapping;
        }
    }
}
