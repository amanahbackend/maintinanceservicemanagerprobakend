﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.ProcessingResult;

namespace Utilites.ProcessingResult
{
    public interface IProcessResultStatus
    {
         ProcessResultStatusCode Code { get; set; }
         int SubCode { get; set; }
        string Message { get; set; }
    }
}
