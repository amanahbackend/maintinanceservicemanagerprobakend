﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Utilites.NewtonsoftHelper;

namespace Utilites.ProcessingResult
{
    public class ProcessResultViewModel<T>:IProcessResultViewModel<T>
    {
        public ProcessResultViewModel(string methodName)
        {
            MethodName = methodName;
        }
        public ProcessResultViewModel()
        {

        }
        public bool IsSucceeded { get; set; }
        public string MethodName { get; set; }
        public ProcessResultStatus Status { get; set; }
        public T Data { get; set; }
    }
    }
