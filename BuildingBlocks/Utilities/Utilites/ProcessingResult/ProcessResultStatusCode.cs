﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.ProcessingResult
{
    public enum ProcessResultStatusCode
    {
        DefaultValue = 0,
        Succeded = 1,
        Failed = 2,
        NotFound = 3,
        MissingArguments = 4,
        NullArguments=5,
        NotSupportedExtension=6,
        InvalidValue=6,
        Dublicated = 7,
        HasChilds = 9,
        AssignedForWork = 10,
        UserHasTeam=11,
        AssignedForWorkAndHasUsers=12
    };
}