﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnum
{
    public enum AcceptenceType
    {
        NoAction = 1,
        Accepted = 2,
        Rejected = 3
    }
}
