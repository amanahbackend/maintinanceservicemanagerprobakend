﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnums
{
    public enum DeliveryType
    {
        PickUp = 1,
        Home = 2,
        Donation = 3
    }
}
