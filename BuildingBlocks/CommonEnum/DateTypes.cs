﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnums
{
    public enum DateTypes
    {
        CustomerDate=1,
        EidDates=2,
        PickUpDates=3,
        DonationDates = 4,
    }
}
