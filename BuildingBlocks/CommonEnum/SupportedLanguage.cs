﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnums
{
    public enum SupportedLanguage
    {
        English=1,
        Arabic=2,
        NotSet=0
    }
}
