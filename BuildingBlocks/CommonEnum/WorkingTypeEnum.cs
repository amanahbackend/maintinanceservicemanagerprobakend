﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnum
{
    public enum  WorkingTypeEnum
    {
        AcceptAndsetFirstOrderOnTravel=1,
        Idle=3,
        Break=4,
        RejectFirstOrder=8,
        ContinueWorking =9,
        EndWork=10,
        SetFirstOrderOnTravel = 13

    }
}
