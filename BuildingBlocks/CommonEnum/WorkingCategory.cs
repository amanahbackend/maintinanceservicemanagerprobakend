﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnum
{
    public enum WorkingCategory
    {
        KuwaitCashOnDelivery = 1,
        UAECashOnDelivery = 2,
        KNet = 3,
        MasterCard = 4,
        Wallet = 5
    }
}
