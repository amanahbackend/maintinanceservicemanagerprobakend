﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CommonEnums
{
    public enum SystemCategoryType
    {
        [Description("أفراد")]
        Individual = 1,
        [Description("مبيعات")]
        Sales = 2
    }
}
