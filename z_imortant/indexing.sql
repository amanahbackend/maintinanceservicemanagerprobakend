/****** Object:  Index [IX_Orders_BuildingTypeId]    Script Date: 10/27/2020 5:09:30 PM ******/
DROP INDEX [IX_Orders_BuildingTypeId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_BuildingTypeId]    Script Date: 10/27/2020 5:09:30 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_BuildingTypeId] ON [dbo].[Orders]
(
	[BuildingTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO


/****** Object:  Index [IX_Orders_ContractTypeId]    Script Date: 10/27/2020 5:09:57 PM ******/
DROP INDEX [IX_Orders_ContractTypeId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_ContractTypeId]    Script Date: 10/27/2020 5:09:57 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_ContractTypeId] ON [dbo].[Orders]
(
	[ContractTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Orders_DispatcherId]    Script Date: 10/27/2020 5:10:35 PM ******/
DROP INDEX [IX_Orders_DispatcherId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_DispatcherId]    Script Date: 10/27/2020 5:10:35 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_DispatcherId] ON [dbo].[Orders]
(
	[DispatcherId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Orders_DivisionId]    Script Date: 10/27/2020 5:11:19 PM ******/
DROP INDEX [IX_Orders_DivisionId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_DivisionId]    Script Date: 10/27/2020 5:11:19 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_DivisionId] ON [dbo].[Orders]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_LastActorTechUserId]    Script Date: 10/27/2020 5:11:49 PM ******/
DROP INDEX [IX_Orders_LastActorTechUserId] ON [dbo].[Orders]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Orders_LastActorTechUserId]    Script Date: 10/27/2020 5:11:49 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_LastActorTechUserId] ON [dbo].[Orders]
(
	[LastActorTechUserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Orders_ProblemId]    Script Date: 10/27/2020 5:12:15 PM ******/
DROP INDEX [IX_Orders_ProblemId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_ProblemId]    Script Date: 10/27/2020 5:12:15 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_ProblemId] ON [dbo].[Orders]
(
	[ProblemId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Orders_StatusId]    Script Date: 10/27/2020 5:12:38 PM ******/
DROP INDEX [IX_Orders_StatusId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_StatusId]    Script Date: 10/27/2020 5:12:38 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_StatusId] ON [dbo].[Orders]
(
	[StatusId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Orders_SupervisorId]    Script Date: 10/27/2020 5:13:15 PM ******/
DROP INDEX [IX_Orders_SupervisorId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_SupervisorId]    Script Date: 10/27/2020 5:13:15 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_SupervisorId] ON [dbo].[Orders]
(
	[SupervisorId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Orders_TeamId]    Script Date: 10/27/2020 5:13:49 PM ******/
DROP INDEX [IX_Orders_TeamId] ON [dbo].[Orders]
GO

/****** Object:  Index [IX_Orders_TeamId]    Script Date: 10/27/2020 5:13:49 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_TeamId] ON [dbo].[Orders]
(
	[TeamId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_CostCenterId]    Script Date: 10/27/2020 5:15:06 PM ******/
DROP INDEX [IX_OrderActions_CostCenterId] ON [dbo].[OrderActions]
GO

/****** Object:  Index [IX_OrderActions_CostCenterId]    Script Date: 10/27/2020 5:15:06 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_CostCenterId] ON [dbo].[OrderActions]
(
	[CostCenterId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_DispatcherId]    Script Date: 10/27/2020 5:15:30 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_DispatcherId] ON [dbo].[OrderActions]
(
	[DispatcherId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_OrderActions_FK_CreatedBy_Id]    Script Date: 10/27/2020 5:15:51 PM ******/
DROP INDEX [IX_OrderActions_FK_CreatedBy_Id] ON [dbo].[OrderActions]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_OrderActions_FK_CreatedBy_Id]    Script Date: 10/27/2020 5:15:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_FK_CreatedBy_Id] ON [dbo].[OrderActions]
(
	[FK_CreatedBy_Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_OrderActions_ForemanId]    Script Date: 10/27/2020 5:16:13 PM ******/
DROP INDEX [IX_OrderActions_ForemanId] ON [dbo].[OrderActions]
GO

/****** Object:  Index [IX_OrderActions_ForemanId]    Script Date: 10/27/2020 5:16:13 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_ForemanId] ON [dbo].[OrderActions]
(
	[ForemanId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_OrderId]    Script Date: 10/27/2020 5:16:34 PM ******/
DROP INDEX [IX_OrderActions_OrderId] ON [dbo].[OrderActions]
GO

/****** Object:  Index [IX_OrderActions_OrderId]    Script Date: 10/27/2020 5:16:34 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_OrderId] ON [dbo].[OrderActions]
(
	[OrderId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_StatusId]    Script Date: 10/27/2020 5:16:53 PM ******/
DROP INDEX [IX_OrderActions_StatusId] ON [dbo].[OrderActions]
GO

/****** Object:  Index [IX_OrderActions_StatusId]    Script Date: 10/27/2020 5:16:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_StatusId] ON [dbo].[OrderActions]
(
	[StatusId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_OrderActions_SupervisorId]    Script Date: 10/27/2020 5:17:09 PM ******/
DROP INDEX [IX_OrderActions_SupervisorId] ON [dbo].[OrderActions]
GO

/****** Object:  Index [IX_OrderActions_SupervisorId]    Script Date: 10/27/2020 5:17:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_SupervisorId] ON [dbo].[OrderActions]
(
	[SupervisorId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_OrderActions_TeamId]    Script Date: 10/27/2020 5:17:37 PM ******/
DROP INDEX [IX_OrderActions_TeamId] ON [dbo].[OrderActions]
GO

/****** Object:  Index [IX_OrderActions_TeamId]    Script Date: 10/27/2020 5:17:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_TeamId] ON [dbo].[OrderActions]
(
	[TeamId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_TechnicianId]    Script Date: 10/27/2020 5:18:01 PM ******/
DROP INDEX [IX_OrderActions_TechnicianId] ON [dbo].[OrderActions]
GO

/****** Object:  Index [IX_OrderActions_TechnicianId]    Script Date: 10/27/2020 5:18:01 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_TechnicianId] ON [dbo].[OrderActions]
(
	[TechnicianId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Supervisors_CostCenterId]    Script Date: 10/27/2020 5:19:10 PM ******/
DROP INDEX [IX_Supervisors_CostCenterId] ON [dbo].[Supervisors]
GO

/****** Object:  Index [IX_Supervisors_CostCenterId]    Script Date: 10/27/2020 5:19:10 PM ******/
CREATE NONCLUSTERED INDEX [IX_Supervisors_CostCenterId] ON [dbo].[Supervisors]
(
	[CostCenterId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Supervisors_DivisionId]    Script Date: 10/27/2020 5:19:27 PM ******/
DROP INDEX [IX_Supervisors_DivisionId] ON [dbo].[Supervisors]
GO

/****** Object:  Index [IX_Supervisors_DivisionId]    Script Date: 10/27/2020 5:19:27 PM ******/
CREATE NONCLUSTERED INDEX [IX_Supervisors_DivisionId] ON [dbo].[Supervisors]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Supervisors_ManagerId]    Script Date: 10/27/2020 5:20:00 PM ******/
DROP INDEX [IX_Supervisors_ManagerId] ON [dbo].[Supervisors]
GO

/****** Object:  Index [IX_Supervisors_ManagerId]    Script Date: 10/27/2020 5:20:00 PM ******/
CREATE NONCLUSTERED INDEX [IX_Supervisors_ManagerId] ON [dbo].[Supervisors]
(
	[ManagerId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Supervisors_UserId]    Script Date: 10/27/2020 5:20:18 PM ******/
DROP INDEX [IX_Supervisors_UserId] ON [dbo].[Supervisors]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Supervisors_UserId]    Script Date: 10/27/2020 5:20:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Supervisors_UserId] ON [dbo].[Supervisors]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Teams_DispatcherId]    Script Date: 10/27/2020 5:20:58 PM ******/
DROP INDEX [IX_Teams_DispatcherId] ON [dbo].[Teams]
GO

/****** Object:  Index [IX_Teams_DispatcherId]    Script Date: 10/27/2020 5:20:58 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_DispatcherId] ON [dbo].[Teams]
(
	[DispatcherId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Teams_DivisionId]    Script Date: 10/27/2020 5:21:17 PM ******/
DROP INDEX [IX_Teams_DivisionId] ON [dbo].[Teams]
GO

/****** Object:  Index [IX_Teams_DivisionId]    Script Date: 10/27/2020 5:21:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_DivisionId] ON [dbo].[Teams]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_EngineerId]    Script Date: 10/27/2020 5:21:34 PM ******/
DROP INDEX [IX_Teams_EngineerId] ON [dbo].[Teams]
GO

/****** Object:  Index [IX_Teams_EngineerId]    Script Date: 10/27/2020 5:21:34 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_EngineerId] ON [dbo].[Teams]
(
	[EngineerId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Teams_ForemanId]    Script Date: 10/27/2020 5:21:51 PM ******/
DROP INDEX [IX_Teams_ForemanId] ON [dbo].[Teams]
GO

/****** Object:  Index [IX_Teams_ForemanId]    Script Date: 10/27/2020 5:21:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_ForemanId] ON [dbo].[Teams]
(
	[ForemanId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_ShiftId]    Script Date: 10/27/2020 5:22:14 PM ******/
DROP INDEX [IX_Teams_ShiftId] ON [dbo].[Teams]
GO

/****** Object:  Index [IX_Teams_ShiftId]    Script Date: 10/27/2020 5:22:14 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_ShiftId] ON [dbo].[Teams]
(
	[ShiftId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_StatusId]    Script Date: 10/27/2020 5:22:31 PM ******/
DROP INDEX [IX_Teams_StatusId] ON [dbo].[Teams]
GO

/****** Object:  Index [IX_Teams_StatusId]    Script Date: 10/27/2020 5:22:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_StatusId] ON [dbo].[Teams]
(
	[StatusId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Teams_SupervisorId]    Script Date: 10/27/2020 5:22:45 PM ******/
DROP INDEX [IX_Teams_SupervisorId] ON [dbo].[Teams]
GO

/****** Object:  Index [IX_Teams_SupervisorId]    Script Date: 10/27/2020 5:22:45 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_SupervisorId] ON [dbo].[Teams]
(
	[SupervisorId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Technicians_CostCenterId]    Script Date: 10/27/2020 5:23:17 PM ******/
DROP INDEX [IX_Technicians_CostCenterId] ON [dbo].[Technicians]
GO

/****** Object:  Index [IX_Technicians_CostCenterId]    Script Date: 10/27/2020 5:23:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Technicians_CostCenterId] ON [dbo].[Technicians]
(
	[CostCenterId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Technicians_DivisionId]    Script Date: 10/27/2020 5:23:36 PM ******/
DROP INDEX [IX_Technicians_DivisionId] ON [dbo].[Technicians]
GO

/****** Object:  Index [IX_Technicians_DivisionId]    Script Date: 10/27/2020 5:23:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_Technicians_DivisionId] ON [dbo].[Technicians]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Technicians_TeamId]    Script Date: 10/27/2020 5:24:04 PM ******/
DROP INDEX [IX_Technicians_TeamId] ON [dbo].[Technicians]
GO

/****** Object:  Index [IX_Technicians_TeamId]    Script Date: 10/27/2020 5:24:04 PM ******/
CREATE NONCLUSTERED INDEX [IX_Technicians_TeamId] ON [dbo].[Technicians]
(
	[TeamId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Technicians_UserId]    Script Date: 10/27/2020 5:24:25 PM ******/
DROP INDEX [IX_Technicians_UserId] ON [dbo].[Technicians]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Technicians_UserId]    Script Date: 10/27/2020 5:24:25 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Technicians_UserId] ON [dbo].[Technicians]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Dispatchers_CostCenterId]    Script Date: 10/27/2020 5:26:21 PM ******/
DROP INDEX [IX_Dispatchers_CostCenterId] ON [dbo].[Dispatchers]
GO

/****** Object:  Index [IX_Dispatchers_CostCenterId]    Script Date: 10/27/2020 5:26:21 PM ******/
CREATE NONCLUSTERED INDEX [IX_Dispatchers_CostCenterId] ON [dbo].[Dispatchers]
(
	[CostCenterId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Dispatchers_DivisionId]    Script Date: 10/27/2020 5:26:38 PM ******/
DROP INDEX [IX_Dispatchers_DivisionId] ON [dbo].[Dispatchers]
GO

/****** Object:  Index [IX_Dispatchers_DivisionId]    Script Date: 10/27/2020 5:26:38 PM ******/
CREATE NONCLUSTERED INDEX [IX_Dispatchers_DivisionId] ON [dbo].[Dispatchers]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Dispatchers_SupervisorId]    Script Date: 10/27/2020 5:26:55 PM ******/
DROP INDEX [IX_Dispatchers_SupervisorId] ON [dbo].[Dispatchers]
GO

/****** Object:  Index [IX_Dispatchers_SupervisorId]    Script Date: 10/27/2020 5:26:55 PM ******/
CREATE NONCLUSTERED INDEX [IX_Dispatchers_SupervisorId] ON [dbo].[Dispatchers]
(
	[SupervisorId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Dispatchers_UserId]    Script Date: 10/27/2020 5:27:10 PM ******/
DROP INDEX [IX_Dispatchers_UserId] ON [dbo].[Dispatchers]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Dispatchers_UserId]    Script Date: 10/27/2020 5:27:10 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Dispatchers_UserId] ON [dbo].[Dispatchers]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_DispatcherSettings_AreaId]    Script Date: 10/27/2020 5:28:42 PM ******/
DROP INDEX [IX_DispatcherSettings_AreaId] ON [dbo].[DispatcherSettings]
GO

/****** Object:  Index [IX_DispatcherSettings_AreaId]    Script Date: 10/27/2020 5:28:42 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_AreaId] ON [dbo].[DispatcherSettings]
(
	[AreaId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_DispatcherSettings_DispatcherId]    Script Date: 10/27/2020 5:28:57 PM ******/
DROP INDEX [IX_DispatcherSettings_DispatcherId] ON [dbo].[DispatcherSettings]
GO

/****** Object:  Index [IX_DispatcherSettings_DispatcherId]    Script Date: 10/27/2020 5:28:57 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_DispatcherId] ON [dbo].[DispatcherSettings]
(
	[DispatcherId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_DispatcherSettings_DivisionId]    Script Date: 10/27/2020 5:29:17 PM ******/
DROP INDEX [IX_DispatcherSettings_DivisionId] ON [dbo].[DispatcherSettings]
GO

/****** Object:  Index [IX_DispatcherSettings_DivisionId]    Script Date: 10/27/2020 5:29:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_DivisionId] ON [dbo].[DispatcherSettings]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_DispatcherSettings_OrderProblemId]    Script Date: 10/27/2020 5:29:35 PM ******/
DROP INDEX [IX_DispatcherSettings_OrderProblemId] ON [dbo].[DispatcherSettings]
GO

/****** Object:  Index [IX_DispatcherSettings_OrderProblemId]    Script Date: 10/27/2020 5:29:35 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_OrderProblemId] ON [dbo].[DispatcherSettings]
(
	[OrderProblemId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Engineers_CostCenterId]    Script Date: 10/27/2020 5:30:20 PM ******/
DROP INDEX [IX_Engineers_CostCenterId] ON [dbo].[Engineers]
GO

/****** Object:  Index [IX_Engineers_CostCenterId]    Script Date: 10/27/2020 5:30:20 PM ******/
CREATE NONCLUSTERED INDEX [IX_Engineers_CostCenterId] ON [dbo].[Engineers]
(
	[CostCenterId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Engineers_DispatcherId]    Script Date: 10/27/2020 5:30:34 PM ******/
DROP INDEX [IX_Engineers_DispatcherId] ON [dbo].[Engineers]
GO

/****** Object:  Index [IX_Engineers_DispatcherId]    Script Date: 10/27/2020 5:30:34 PM ******/
CREATE NONCLUSTERED INDEX [IX_Engineers_DispatcherId] ON [dbo].[Engineers]
(
	[DispatcherId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Engineers_DivisionId]    Script Date: 10/27/2020 5:30:55 PM ******/
DROP INDEX [IX_Engineers_DivisionId] ON [dbo].[Engineers]
GO

/****** Object:  Index [IX_Engineers_DivisionId]    Script Date: 10/27/2020 5:30:55 PM ******/
CREATE NONCLUSTERED INDEX [IX_Engineers_DivisionId] ON [dbo].[Engineers]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Engineers_UserId]    Script Date: 10/27/2020 5:31:08 PM ******/
DROP INDEX [IX_Engineers_UserId] ON [dbo].[Engineers]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Engineers_UserId]    Script Date: 10/27/2020 5:31:08 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Engineers_UserId] ON [dbo].[Engineers]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Foremans_CostCenterId]    Script Date: 10/27/2020 5:31:52 PM ******/
DROP INDEX [IX_Foremans_CostCenterId] ON [dbo].[Foremans]
GO

/****** Object:  Index [IX_Foremans_CostCenterId]    Script Date: 10/27/2020 5:31:52 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_CostCenterId] ON [dbo].[Foremans]
(
	[CostCenterId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Foremans_DispatcherId]    Script Date: 10/27/2020 5:32:05 PM ******/
DROP INDEX [IX_Foremans_DispatcherId] ON [dbo].[Foremans]
GO

/****** Object:  Index [IX_Foremans_DispatcherId]    Script Date: 10/27/2020 5:32:05 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_DispatcherId] ON [dbo].[Foremans]
(
	[DispatcherId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Foremans_DivisionId]    Script Date: 10/27/2020 5:32:20 PM ******/
DROP INDEX [IX_Foremans_DivisionId] ON [dbo].[Foremans]
GO

/****** Object:  Index [IX_Foremans_DivisionId]    Script Date: 10/27/2020 5:32:20 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_DivisionId] ON [dbo].[Foremans]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Foremans_EngineerId]    Script Date: 10/27/2020 5:32:33 PM ******/
DROP INDEX [IX_Foremans_EngineerId] ON [dbo].[Foremans]
GO

/****** Object:  Index [IX_Foremans_EngineerId]    Script Date: 10/27/2020 5:32:33 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_EngineerId] ON [dbo].[Foremans]
(
	[EngineerId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Foremans_UserId]    Script Date: 10/27/2020 5:32:47 PM ******/
DROP INDEX [IX_Foremans_UserId] ON [dbo].[Foremans]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Foremans_UserId]    Script Date: 10/27/2020 5:32:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Foremans_UserId] ON [dbo].[Foremans]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Managers_CostCenterId]    Script Date: 10/27/2020 5:33:35 PM ******/
DROP INDEX [IX_Managers_CostCenterId] ON [dbo].[Managers]
GO

/****** Object:  Index [IX_Managers_CostCenterId]    Script Date: 10/27/2020 5:33:35 PM ******/
CREATE NONCLUSTERED INDEX [IX_Managers_CostCenterId] ON [dbo].[Managers]
(
	[CostCenterId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Managers_UserId]    Script Date: 10/27/2020 5:33:49 PM ******/
DROP INDEX [IX_Managers_UserId] ON [dbo].[Managers]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Managers_UserId]    Script Date: 10/27/2020 5:33:49 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Managers_UserId] ON [dbo].[Managers]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Members_DivisionId]    Script Date: 10/27/2020 5:34:28 PM ******/
DROP INDEX [IX_Members_DivisionId] ON [dbo].[Members]
GO

/****** Object:  Index [IX_Members_DivisionId]    Script Date: 10/27/2020 5:34:28 PM ******/
CREATE NONCLUSTERED INDEX [IX_Members_DivisionId] ON [dbo].[Members]
(
	[DivisionId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_Members_UserId]    Script Date: 10/27/2020 5:34:42 PM ******/
DROP INDEX [IX_Members_UserId] ON [dbo].[Members]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_Members_UserId]    Script Date: 10/27/2020 5:34:42 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Members_UserId] ON [dbo].[Members]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO










































































































