USE [master]
GO
/****** Object:  Database [custend-sandbox-db-2]    Script Date: 10/25/2020 12:43:15 PM ******/
CREATE DATABASE [custend-sandbox-db-2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'custend-sandbox-db-2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\custend-sandbox-db-2.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'custend-sandbox-db-2_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\custend-sandbox-db-2_log.ldf' , SIZE = 139264KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [custend-sandbox-db-2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [custend-sandbox-db-2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET ARITHABORT OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [custend-sandbox-db-2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [custend-sandbox-db-2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET  ENABLE_BROKER 
GO
ALTER DATABASE [custend-sandbox-db-2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [custend-sandbox-db-2] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [custend-sandbox-db-2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET RECOVERY FULL 
GO
ALTER DATABASE [custend-sandbox-db-2] SET  MULTI_USER 
GO
ALTER DATABASE [custend-sandbox-db-2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [custend-sandbox-db-2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [custend-sandbox-db-2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [custend-sandbox-db-2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [custend-sandbox-db-2] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'custend-sandbox-db-2', N'ON'
GO
ALTER DATABASE [custend-sandbox-db-2] SET QUERY_STORE = OFF
GO
USE [custend-sandbox-db-2]
GO
/****** Object:  Schema [HangFire]    Script Date: 10/25/2020 12:43:16 PM ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationUserHistory]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationUserHistory](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](max) NOT NULL,
	[LoginDate] [datetime2](7) NOT NULL,
	[LogoutDate] [datetime2](7) NULL,
	[Token] [nvarchar](max) NOT NULL,
	[UserType] [nvarchar](max) NOT NULL,
	[DeveiceId] [nvarchar](max) NULL,
 CONSTRAINT [PK_ApplicationUserHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArchivedOrderFile]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArchivedOrderFile](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](450) NULL,
	[Trials] [int] NOT NULL,
 CONSTRAINT [PK_ArchivedOrderFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArchivedOrders]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArchivedOrders](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[OrderId] [int] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[TypeId] [int] NOT NULL,
	[TypeName] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[StatusName] [nvarchar](max) NULL,
	[SubStatusId] [int] NOT NULL,
	[SubStatusName] [nvarchar](max) NULL,
	[ProblemId] [int] NOT NULL,
	[ProblemName] [nvarchar](max) NULL,
	[PriorityId] [int] NOT NULL,
	[PriorityName] [nvarchar](max) NULL,
	[CompanyCodeId] [int] NOT NULL,
	[CompanyCodeName] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[DivisionName] [nvarchar](max) NULL,
	[ICAgentNote] [nvarchar](max) NULL,
	[DispatcherNote] [nvarchar](max) NULL,
	[CancellationReason] [nvarchar](max) NULL,
	[GeneralNote] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[OrderDescription] [nvarchar](max) NULL,
	[CustomerCode] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](max) NULL,
	[PhoneOne] [nvarchar](max) NULL,
	[PhoneTwo] [nvarchar](max) NULL,
	[Caller_ID] [nvarchar](max) NULL,
	[PACI] [nvarchar](max) NULL,
	[FunctionalLocation] [nvarchar](max) NULL,
	[HouseKasima] [nvarchar](max) NULL,
	[Floor] [nvarchar](max) NULL,
	[AppartmentNo] [nvarchar](max) NULL,
	[StreetId] [int] NOT NULL,
	[StreetName] [nvarchar](max) NULL,
	[BlockId] [int] NOT NULL,
	[BlockName] [nvarchar](max) NULL,
	[AreaId] [int] NOT NULL,
	[AreaName] [nvarchar](max) NULL,
	[GovId] [int] NOT NULL,
	[GovName] [nvarchar](max) NULL,
	[AddressNote] [nvarchar](max) NULL,
	[Long] [decimal](18, 2) NOT NULL,
	[Lat] [decimal](18, 2) NOT NULL,
	[BuildingTypeId] [int] NOT NULL,
	[BuildingTypeName] [nvarchar](max) NULL,
	[ContractCode] [nvarchar](max) NULL,
	[ContractTypeId] [int] NOT NULL,
	[ContractTypeName] [nvarchar](max) NULL,
	[ContractStartDate] [datetime2](7) NOT NULL,
	[ContractExpiryDate] [datetime2](7) NOT NULL,
	[InsertionDate] [datetime2](7) NOT NULL,
	[FileName] [nvarchar](max) NULL,
	[SupervisorId] [int] NOT NULL,
	[SupervisorName] [nvarchar](max) NULL,
	[DispatcherId] [int] NOT NULL,
	[DispatcherName] [nvarchar](max) NULL,
	[TeamId] [int] NOT NULL,
	[AcceptanceFlag] [int] NOT NULL,
	[RejectionReasonId] [int] NOT NULL,
	[RejectionReason] [nvarchar](max) NULL,
	[IsAccomplish] [int] NOT NULL,
	[IsRepeatedCall] [bit] NOT NULL,
	[IsExceedTime] [bit] NOT NULL,
 CONSTRAINT [PK_ArchivedOrders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Areas]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Areas](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Area_No] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Fk_Governorate_Id] [int] NULL,
 CONSTRAINT [PK_Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[PF] [nvarchar](max) NULL,
	[Phone1] [nvarchar](max) NULL,
	[Phone2] [nvarchar](max) NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Deactivated] [bit] NOT NULL,
	[PicturePath] [nvarchar](max) NULL,
	[IsFirstLogin] [bit] NOT NULL,
	[LanguageId] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AttendanceStates]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttendanceStates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_AttendanceStates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attendences]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendences](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[AttendedId] [nvarchar](max) NULL,
	[AttendedName] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[StatusName] [int] NOT NULL,
	[AttendedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Attendences] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Availability]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Availability](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Availability] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BuildingTypes]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuildingTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_BuildingTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CompanyCode]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyCode](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CompanyCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractTypes]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_ContractTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CostCenter]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CostCenter](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CostCenter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dispatchers]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dispatchers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[SupervisorId] [int] NOT NULL,
 CONSTRAINT [PK_Dispatchers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DispatcherSettings]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DispatcherSettings](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[DispatcherId] [int] NOT NULL,
	[DivisionId] [int] NULL,
	[OrderProblemId] [int] NOT NULL,
	[AreaId] [int] NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_DispatcherSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Division]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Division](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Drivers]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drivers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[DivisionName] [nvarchar](max) NULL,
	[CostCenterId] [int] NOT NULL,
	[CostCenterName] [nvarchar](max) NULL,
	[VehicleNo] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
 CONSTRAINT [PK_Drivers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Engineers]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Engineers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[DispatcherId] [int] NULL,
 CONSTRAINT [PK_Engineers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Foremans]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Foremans](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[Name] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[SupervisorId] [int] NOT NULL,
	[DispatcherId] [int] NULL,
	[EngineerId] [int] NULL,
 CONSTRAINT [PK_Foremans] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Governorates]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Governorates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Gov_No] [int] NOT NULL,
 CONSTRAINT [PK_Governorates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JunkUser]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JunkUser](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[NormalizedUserName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[NormalizedEmail] [nvarchar](max) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[Phone1] [nvarchar](max) NOT NULL,
	[Phone2] [nvarchar](max) NULL,
	[PicturePath] [nvarchar](max) NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Deactivated] [bit] NOT NULL,
 CONSTRAINT [PK_JunkUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Areas]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Areas](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Area_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[AreasId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_AttendanceStates]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_AttendanceStates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_AttendanceStates_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[AttendanceStatesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_AttendanceStates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Availability]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Availability](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Availability_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[AvailabilityId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Availability] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_BuildingTypes]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_BuildingTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_BuildingTypes_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[BuildingTypesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_BuildingTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_CompanyCode]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_CompanyCode](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_CompanyCode_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CompanyCodeId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_CompanyCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_ContractTypes]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_ContractTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_ContractTypes_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[ContractTypesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_ContractTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_CostCenter]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_CostCenter](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_CostCenter_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CostCenterId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_CostCenter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Division]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Division](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Division_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DivisionId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Division] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Governorates]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Governorates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Governorates_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[GovernoratesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Governorates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Notification]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Notification](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Notification_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderPriority]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderPriority](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderPriority_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderPriority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderProblem]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderProblem](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderProblem_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderProblem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderStatus]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderStatus_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderSubStatus]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderSubStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderSubStatus_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderSubStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderType]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderType_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_RejectionReason]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_RejectionReason](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_RejectionReason_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Lang_RejectionReason] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Shift]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Shift](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Shift_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[FromTime] [time](7) NOT NULL,
	[ToTime] [time](7) NOT NULL,
	[FromTimeValue] [datetime2](7) NOT NULL,
	[ToTimeValue] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Lang_Shift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_WorkingType]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_WorkingType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_WorkingType_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_WorkingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Managers]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Managers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[Name] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
	[CostCenterId] [int] NOT NULL,
 CONSTRAINT [PK_Managers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MapAdminRelated]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapAdminRelated](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mobile_Api_Key] [nvarchar](max) NULL,
	[Web_Api_Key] [nvarchar](max) NULL,
	[Refresh_Rate] [int] NOT NULL,
 CONSTRAINT [PK_MapAdminRelated] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaterialControllers]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialControllers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[DivisionName] [nvarchar](max) NULL,
	[CostCenterId] [int] NOT NULL,
	[CostCenterName] [nvarchar](max) NULL,
 CONSTRAINT [PK_MaterialControllers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Members]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Members](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[TeamId] [int] NULL,
	[MemberParentId] [int] NOT NULL,
	[MemberParentName] [nvarchar](max) NULL,
	[Rank] [int] NOT NULL,
	[MemberType] [int] NOT NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NULL,
 CONSTRAINT [PK_Members] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationCenter]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationCenter](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[RecieverId] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
	[Data] [nvarchar](max) NULL,
	[NotificationType] [nvarchar](max) NULL,
	[IsRead] [bit] NOT NULL,
	[TeamId] [int] NULL,
	[IsActionTaken] [bit] NULL,
	[OrderCode] [nvarchar](max) NULL,
 CONSTRAINT [PK_NotificationCenter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifications](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderActions]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderActions](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](450) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[OrderId] [int] NULL,
	[StatusId] [int] NULL,
	[SubStatusId] [int] NULL,
	[SubStatusName] [nvarchar](max) NULL,
	[ActionDate] [datetime2](7) NOT NULL,
	[ActionDay] [datetime2](7) NOT NULL,
	[ActionTime] [time](7) NULL,
	[ActionTimeDays] [int] NULL,
	[CreatedUserId] [int] NULL,
	[CreatedUser] [nvarchar](max) NULL,
	[ActionTypeId] [int] NULL,
	[ActionTypeName] [nvarchar](max) NULL,
	[CostCenterId] [int] NULL,
	[WorkingTypeId] [int] NULL,
	[WorkingTypeName] [nvarchar](max) NULL,
	[Reason] [nvarchar](max) NULL,
	[ServeyReport] [nvarchar](max) NULL,
	[ActionDistance] [float] NOT NULL,
	[SupervisorId] [int] NULL,
	[DispatcherId] [int] NULL,
	[ForemanId] [int] NULL,
	[TeamId] [int] NULL,
	[TechnicianId] [int] NULL,
	[PlatformTypeSource] [int] NOT NULL,
	[WorkingSubReason] [nvarchar](max) NULL,
	[WorkingSubReasonId] [int] NULL,
 CONSTRAINT [PK_OrderActions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderPriority]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderPriority](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderPriority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderProblem]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProblem](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[ExceedHours] [int] NOT NULL,
 CONSTRAINT [PK_OrderProblem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderRowData]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderRowData](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerNo] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](max) NULL,
	[PhoneOne] [nvarchar](max) NULL,
	[PhoneTwo] [nvarchar](max) NULL,
	[ContractNo] [nvarchar](max) NULL,
	[ContractType] [nvarchar](max) NULL,
	[ContractTypeDescription] [nvarchar](max) NULL,
	[ContractDate] [datetime2](7) NULL,
	[ContactExpiraion] [datetime2](7) NULL,
	[FunctionalLocation] [nvarchar](max) NULL,
	[PACI] [nvarchar](max) NULL,
	[Governorate] [int] NULL,
	[AreaCode] [int] NULL,
	[AreaDescription] [nvarchar](max) NULL,
	[Block] [nvarchar](max) NULL,
	[Street] [nvarchar](max) NULL,
	[House] [nvarchar](max) NULL,
	[Floor] [nvarchar](max) NULL,
	[AppartmentNo] [int] NULL,
	[AddressNote] [nvarchar](max) NULL,
	[OrderNo] [nvarchar](max) NULL,
	[OrderType] [nvarchar](max) NULL,
	[OrderTypeDescription] [nvarchar](max) NULL,
	[CompanyCode] [nvarchar](max) NULL,
	[OrderDate] [datetime2](7) NULL,
	[Division] [int] NULL,
	[DivisionDescription] [nvarchar](max) NULL,
	[OrderPriority] [int] NULL,
	[OrderPriorityDescription] [nvarchar](max) NULL,
	[Problem] [nvarchar](max) NULL,
	[ProblemDescription] [nvarchar](max) NULL,
	[OrderDescription] [nvarchar](max) NULL,
	[OrderStatus] [nvarchar](max) NULL,
	[OrderNoteAgent] [nvarchar](max) NULL,
	[Caller_ID] [nvarchar](max) NULL,
	[FileName] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderRowData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 10/25/2020 12:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[TypeId] [int] NOT NULL,
	[TypeName] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[SubStatusId] [int] NOT NULL,
	[SubStatusName] [nvarchar](max) NULL,
	[ProblemId] [int] NULL,
	[PriorityId] [int] NOT NULL,
	[PriorityName] [nvarchar](max) NULL,
	[CompanyCodeId] [int] NOT NULL,
	[CompanyCodeName] [nvarchar](max) NULL,
	[DivisionId] [int] NULL,
	[ICAgentNote] [nvarchar](max) NULL,
	[DispatcherNote] [nvarchar](max) NULL,
	[CancellationReason] [nvarchar](max) NULL,
	[GeneralNote] [nvarchar](max) NULL,
	[SAP_CreatedDate] [datetime2](7) NOT NULL,
	[OrderDescription] [nvarchar](max) NULL,
	[ServeyReport] [nvarchar](max) NULL,
	[OrderTypeCode] [nvarchar](max) NULL,
	[CustomerCode] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](max) NULL,
	[PhoneOne] [nvarchar](max) NULL,
	[PhoneTwo] [nvarchar](max) NULL,
	[Caller_ID] [nvarchar](max) NULL,
	[SAP_PACI] [nvarchar](max) NULL,
	[SAP_HouseKasima] [nvarchar](max) NULL,
	[SAP_Floor] [nvarchar](max) NULL,
	[SAP_AppartmentNo] [nvarchar](max) NULL,
	[SAP_StreetName] [nvarchar](max) NULL,
	[SAP_BlockName] [nvarchar](max) NULL,
	[SAP_AreaName] [nvarchar](max) NULL,
	[SAP_GovName] [nvarchar](max) NULL,
	[PACI] [nvarchar](max) NULL,
	[FunctionalLocation] [nvarchar](max) NULL,
	[HouseKasima] [nvarchar](max) NULL,
	[Floor] [nvarchar](max) NULL,
	[AppartmentNo] [nvarchar](max) NULL,
	[StreetId] [int] NOT NULL,
	[StreetName] [nvarchar](max) NULL,
	[BlockId] [int] NOT NULL,
	[BlockName] [nvarchar](max) NULL,
	[AreaId] [int] NOT NULL,
	[AreaName] [nvarchar](max) NULL,
	[GovId] [int] NOT NULL,
	[GovName] [nvarchar](max) NULL,
	[AddressNote] [nvarchar](max) NULL,
	[Long] [decimal](18, 2) NOT NULL,
	[Lat] [decimal](18, 2) NOT NULL,
	[BuildingTypeId] [int] NULL,
	[ContractCode] [nvarchar](max) NULL,
	[ContractTypeId] [int] NULL,
	[ContractStartDate] [datetime2](7) NULL,
	[ContractExpiryDate] [datetime2](7) NULL,
	[InsertionDate] [datetime2](7) NOT NULL,
	[FileName] [nvarchar](max) NULL,
	[SupervisorId] [int] NULL,
	[DispatcherId] [int] NULL,
	[RankInDispatcher] [int] NULL,
	[TeamId] [int] NULL,
	[PrevTeamId] [int] NULL,
	[RankInTeam] [int] NULL,
	[AcceptanceFlag] [int] NOT NULL,
	[RejectionReasonId] [int] NULL,
	[RejectionReason] [nvarchar](max) NULL,
	[IsAccomplish] [int] NOT NULL,
	[OnHoldCount] [int] NOT NULL,
	[IsRepeatedCall] [bit] NOT NULL,
	[IsExceedTime] [bit] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderSubStatus]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderSubStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[StatusName] [int] NOT NULL,
 CONSTRAINT [PK_OrderSubStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PACI]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PACI](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PACINumber] [nvarchar](450) NULL,
	[AreaName] [nvarchar](max) NULL,
	[AreaId] [int] NOT NULL,
	[GovernorateName] [nvarchar](max) NULL,
	[GovernorateId] [int] NOT NULL,
	[BlockName] [nvarchar](max) NULL,
	[BlockId] [int] NOT NULL,
	[StreetName] [nvarchar](max) NULL,
	[StreetId] [int] NOT NULL,
	[Longtiude] [decimal](18, 6) NOT NULL,
	[Latitude] [decimal](18, 6) NOT NULL,
	[AppartementNumber] [nvarchar](max) NULL,
	[FloorNumber] [nvarchar](max) NULL,
	[BuildingNumber] [nvarchar](max) NULL,
 CONSTRAINT [PK_PACI] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PasswordTokenPin]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordTokenPin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Token] [nvarchar](max) NOT NULL,
	[Pin] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_PasswordTokenPin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Privilege]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Privilege](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Privilege] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RejectionReason]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RejectionReason](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_RejectionReason] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePrivilege]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePrivilege](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Fk_ApplicationRole_Id] [nvarchar](max) NOT NULL,
	[Fk_Privilege_Id] [int] NOT NULL,
 CONSTRAINT [PK_RolePrivilege] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shift]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shift](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FromTime] [time](7) NOT NULL,
	[ToTime] [time](7) NOT NULL,
 CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supervisors]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supervisors](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NULL,
	[CostCenterId] [int] NULL,
	[ManagerId] [int] NULL,
 CONSTRAINT [PK_Supervisors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupportedLanguages]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupportedLanguages](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_SupportedLanguages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teams]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[VehicleNo] [nvarchar](max) NULL,
	[DivisionId] [int] NULL,
	[ForemanId] [int] NULL,
	[EngineerId] [int] NULL,
	[DispatcherId] [int] NOT NULL,
	[SupervisorId] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[ShiftId] [int] NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Technicians]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Technicians](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[IsDriver] [bit] NOT NULL,
	[TeamId] [int] NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
 CONSTRAINT [PK_Technicians] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TechniciansStates]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TechniciansStates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[TechnicianId] [int] NOT NULL,
	[TeamId] [int] NOT NULL,
	[ActionDate] [datetime2](7) NOT NULL,
	[Long] [decimal](18, 2) NOT NULL,
	[Lat] [decimal](18, 2) NOT NULL,
	[State] [int] NOT NULL,
 CONSTRAINT [PK_TechniciansStates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDevice]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDevice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Fk_AppUser_Id] [nvarchar](max) NOT NULL,
	[DeveiceId] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_UserDevice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehicles]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicles](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Plate_no] [nvarchar](max) NULL,
	[Isassigned] [bit] NOT NULL,
	[TeamId] [int] NULL,
 CONSTRAINT [PK_Vehicles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkingType]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkingType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
	[CategoryCode] [int] NOT NULL,
 CONSTRAINT [PK_WorkingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[ExpireAt] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [CX_HangFire_Counter]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE CLUSTERED INDEX [CX_HangFire_Counter] ON [HangFire].[Counter]
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Job]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StateId] [bigint] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Queue] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[List]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Server]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Set]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[State]    Script Date: 10/25/2020 12:43:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181001135134_init', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181015121521_init', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181015124737_delete-code-from-supportedlanguage', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181016102923_add-code-to-supportedlanguages', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181017105001_add-order-tables', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181018154513_init', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181018160333_change-team-id', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181024140805_order-row-data-table-added', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181025105550_init', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181028124930_add-assigning-info', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181028131949_add-deipatchersettings', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181029110606_creatdispatchersettings', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181029114422_add-rejection-reason', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181029140219_fix-rejection-reason', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181030122145_add-dispatcher-name-and-super-name', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181030133030_add-division-to-dispatcherSettings', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181031104248_add-repeated-call-and-exceed-time', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181031143248_order-setting-table-added', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181031155857_add-archived-order-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181101121904_add-exceeded-hours-per-problem', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181103210758_add-applicationuserhistory-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181104161141_add-order-rank-per-team-and-dispatcher-columns', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181104161933_fix-rank-nullability', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181105085327_order-action-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181105093348_add-order-action-type', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181107134641_add-assigning-in-order-action', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181115122538_add-userDevice-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181118112503_add-vehicle-no-to-driver', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181118112955_add-foreman-to-team', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181118113516_add-member-type-name-and-pf-to-team-member', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181118114829_add-pf-to-driver-and-technician', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181121110839_add-device-id-to-applicationuserhistory', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181204095152_add-userId-to-teammember', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181216142958_make-phone1-nullable', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181217094537__add-manager-and-materialcontroller', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181217095040__add-manager-and-materialcontrollers', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181217112729_add-isdriver-and-team-to-technical-and-delete-driver-from-team', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181218121112__add-TechniciansState', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181218144850_make-technicianId-and-teamId-to-be-int', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181218160207_change-technician-state-property-name', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181220125350_add-costcenterId-and-costcenterName-to-orderAction', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181224172024_fix-database', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181224183223_removeGovAreaRelation', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181224183733_addGovAreaRelation', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181227101132_add-table-working-type', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181230080846_change-order-action', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181230115813_change-working-type', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181231095405_remove-drive-data-from-orderAction-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20181231133118_add-action-time-property-to-order-action', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190101102852_add-vehicle-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190103075505_add-divisionId-divisionName_and-IsDriver-toteammember', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190105075339_add-SAP-location-data-to-current-order', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190105075905_fix-naming-of-SAP-location-data-to-current-order', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190108190430_add-groupId-to-dispatcherSettings', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190108214631_add-device-type-column', N'2.1.4-rtm-31024')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190109001337_add-action-day-in-order-actions', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190109003858_add-reason-in-order-action', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190110115242_add-engineer-details-in-forman', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190114110402_add-notificationCenterTable-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190114115321_add-IsRead-to-notificationCenterTable', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190121145602_set-data-in-notificationCenter-to-int', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190206132044_add-caller_Id-to-orderRowData-and-orderDescription', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190206132202_add-caller_Id-and-orderDescription-to-order', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190210154613_make-data-string-in-notificationCenter', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190212130549_add-PF-to_orderAction', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190213091046_remove-pf-from-orderaction', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190218094958_add-mapadminrelated-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190220094441_add-OnHoldCount-to-ordertable', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190224083838_add-supportedLanguage-to-technician', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190225224140_add-orderLookup-tables', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190225224522_remove-orderLookup-tables', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190226112800_add-lang_workingType-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190228115748_add-notification-tables', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190306142812_remove-name-from-shift', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190306143336_add-shiftId-to-team-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190307083454_use-timespan-in-shift', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190307092728_add-serveyreport-to-order-and-orderAction', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190308235545_add_prevTeamId-to-order', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190317101643_add-PF-field-to-all-users', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190317141326_add-PF-to-application-users', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190325102500_add-action-time-days-field-to-order-actions-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190408144811_add-name-and-day-to-shift-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190409155029_remove-day-from-shift-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190418141957_add-archived-order-files-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190502135821_add-sap-creation-date-to-order-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190506115731_update-contract-start-and-expiration-time-to-nullable', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190512124651_add-paci-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190512140759_add-blockid-and-streetid-to-paci-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190513142717_add-areaid-to-paci-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190513145016_modify-paci-column-types', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190514125727_add-gov-to-paci-table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190527110042_add-building-to-paci', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190721104521_add-file-name-to-order-row-data', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190721135553_add-trials-to-archived-files', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200314145057_initforCodeFirst', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200315094430_fixLang_ShiftTimeSpanIddueFromAndToDates', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200315095953_fixsonArchivedOrderFileAndFileNameInOrderRow', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200320200906_fixsomeUSersRelations', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200324033215_remove_name_Pf_from_Supervisor', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200324035439_remove_name_Pf_from_Dispatcher', N'2.1.14-servicing-32113')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200326223436_teamIssues', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200327152315_restore_Old_Technician_props', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200327160127_remove_Old_Technician_props_Add_Fix_Its_relations', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200329103809_managementClassesFixes', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200330223959_make_some_ids_in_order_nullable', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200331134219_make_Supervisor_nullable', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200331143603_fix_Order_Relations', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200404234151_orderActionsProps', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200405001852_orderActionsRelations', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200405193913_relationBetweenTechnicianAndOrderAction', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200405234239_relationBetweenTechnicianAndOrderActionReFixsssss', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200405234537_relationBetweenTechnicianAndOrderActionReFixsssssRefixednames', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200416133337_fixTechAndMemberRelations', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200420163630_addCostCenterRelationToOrderAction', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200420174527_addplatformForOrderAction', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200422113431_removeStatusNameFromOrderAndOrderAction', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200422115908_removeStatusNameFromOrderAndOrderActionv2', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200422120456_removeStatusNameFromOrderAndOrderActionv3', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200423155738_fixExxuterAndTech', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200430230110_removeNameFromShift', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200603161735_add_WorkingSubReason_To_OrderAction', N'2.1.14-servicing-32113')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200603162527_add_WorkingSubReasonId_To_OrderAction', N'2.1.14-servicing-32113')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (N'161b4121-f4ed-4438-8c1a-35d9828fcf16', N'Engineer', N'ENGINEER', N'd3d2892c-d037-4add-9e89-51613a7d8dda', NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (N'16222f92-3b27-4204-9ba5-90ead9820d53', N'Supervisor', N'SUPERVISOR', N'0433e5df-64ea-444b-98f0-0e178c3a2217', NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (N'3296129a-9e4a-41f5-860d-3a3957882b05', N'Admin', N'ADMIN', N'5a2bf3ba-d7bb-4c31-a900-32722fc7636a', NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (N'94a0662e-d0f1-48f5-890e-e56f09659de5', N'Foreman', N'FOREMAN', N'466fb960-5611-4559-9dfa-0b68bdc46235', NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (N'9c36bde2-0940-4f58-9328-9b558c2a2d64', N'Technician', N'TECHNICIAN', N'f7eeae05-6faa-4c64-8f1e-d24acb4ad084', NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (N'def02a06-aedd-4a74-b0d3-99677755560f', N'Dispatcher', N'DISPATCHER', N'6b33b89c-0b9b-4bab-b160-a758e22bf7bc', NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (N'ed0de298-3435-494e-9156-53581b3afd66', N'Manager', N'MANAGER', N'af2501b2-6c9f-4ea6-ac1a-492708cd5168', NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'558e6917-6b9e-4e9a-a73f-e0c23f8f47a4', N'161b4121-f4ed-4438-8c1a-35d9828fcf16')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'57a8a3b5-4cc2-4515-884f-bed0affe0a57', N'161b4121-f4ed-4438-8c1a-35d9828fcf16')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ee990494-bac0-4f51-b3cb-b58803d6e49b', N'161b4121-f4ed-4438-8c1a-35d9828fcf16')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a96899ab-a09a-492a-be3e-6c751d62a3e3', N'16222f92-3b27-4204-9ba5-90ead9820d53')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c105727c-cb48-4375-8dd3-d9d9d305dcb0', N'16222f92-3b27-4204-9ba5-90ead9820d53')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd815b64b-fdb2-4ed0-8502-7e760804882d', N'16222f92-3b27-4204-9ba5-90ead9820d53')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b9bfeb80-82a7-426e-86df-52cb39e0e31d', N'3296129a-9e4a-41f5-860d-3a3957882b05')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'01bd52d9-12a3-4e0b-9034-d99e70ead630', N'94a0662e-d0f1-48f5-890e-e56f09659de5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'10d5ff37-0fd3-49e2-aeb0-13ea736f13c6', N'94a0662e-d0f1-48f5-890e-e56f09659de5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'164c4db7-ac0b-48b8-b513-a6bbb3500c27', N'94a0662e-d0f1-48f5-890e-e56f09659de5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cf90a295-3432-4b5c-8a23-b98601d2abc8', N'94a0662e-d0f1-48f5-890e-e56f09659de5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'db3f0f10-2ad0-48cd-b938-c545b5a513f3', N'94a0662e-d0f1-48f5-890e-e56f09659de5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'dc2f2a13-2c48-4a56-9c50-1e2d8bf57bea', N'94a0662e-d0f1-48f5-890e-e56f09659de5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'000a317b-64b4-4d1c-bf63-3b5b392770f0', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'038c0f73-f669-4d43-bf23-cca622a4d0a6', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'04f68fbe-0c3c-4115-b378-9efdfeb1e476', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'0564f1d3-0514-4806-b84c-d08b03e0287c', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'06f3b398-c69c-44b1-8dcd-dd62adcd52cc', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'0b9eabc7-e819-4348-88b9-2eec7b0e608d', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'0ff8d62c-71bb-4caa-b6b1-84d2276b7901', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'10bc85dd-395b-4a92-aa64-f6fa39c88b56', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'13d54f2f-b11b-49c9-a77f-4937ccbc514d', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'158d996b-f18f-4ea4-a412-6e5caa97f160', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'16023cd4-c0b7-42ef-9633-e269a5cfafa4', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'16ad66b4-1d81-42cc-b904-4397b9bbbad8', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1b770ebf-141c-4402-915c-846f6798eccc', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'20085b35-cc40-4f3f-a7a5-ac9b3de3249c', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'235b3571-7988-4420-903b-3854515dbe2c', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'23be8e97-8fd6-41f6-a819-6d0933ed90b5', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'246e6434-1496-431b-a7d1-22bb8357b63c', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2474bc21-28fe-4e66-83a4-cfdb9eeb4c5e', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'24bd41b8-4558-4b3c-ab32-6138c6a4db6f', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'266480d5-f158-4fe9-9a43-e1fed36bb75c', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2695057f-960d-4afd-a5b1-4f2e09b145e5', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'27b0bad4-1bfb-496a-be53-aaeb57a6776b', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2b650e74-0250-435f-a82a-da932f5eb567', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2c5a30c4-d144-40fe-abea-7e249d0c856c', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2df7a03a-3c2a-40d0-a127-4fdb3a17922d', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2f5414dc-e240-400e-9dd8-104b10b054b1', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'33649fea-39c2-4e41-addd-ccdad739c393', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'33e097e0-96d2-4fe1-9e74-7ac7f8333004', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'35991b0b-81d2-4591-9ff4-4e96d45fad5a', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'35d796d5-faaf-4821-95b0-24e1e69319d2', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3889d25c-3039-4326-bbeb-0748ba7fbcc7', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3b8481c8-8f34-42d3-a296-cd01289fc67e', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3c6fa9fb-2784-4c0d-a63d-dfd4a4a09ed1', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3db7ca38-8d5a-4b02-a91d-137c8718a39d', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3dd7ccc6-a183-48eb-be3d-5deaa6b1e82e', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3fcd316f-08e0-45d1-937f-93673f39788b', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'43e99446-8dd2-4c04-8c78-d91286892e11', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'46f2d590-ff89-4fb4-85c8-8a634828d0cc', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'48a503f5-a9e4-4111-bf4d-e0d199fdc5ff', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'493214b1-bcc1-4e3c-b1e1-e3da8c4359ed', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'4f39836b-90c6-47b1-ba23-0f19d4e5ec60', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'53f0bf60-5ab0-4cd2-aa8e-3ca584a86780', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'56c758e0-735e-4752-86a7-c706c98b36ab', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'5751cd8d-1739-4cbe-9839-3444e734aab9', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'5b86ecbd-5985-43d4-8396-a88443a61e62', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'5d6cffcb-0014-45ff-9594-356b4897acd8', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'63383e41-654c-4cce-b13e-28fb45ba80fc', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'635ff621-aa25-4a00-adad-c3d4ff6847ea', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'63c63fee-74ec-4dda-a16b-ef322278b3da', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'64cbdd75-bcb9-4fa5-a4b6-a1d570464262', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6cd6c499-9475-470f-8a18-422dbc3e2808', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6e84b1df-2e53-4dcd-80e4-eb1e38b3c5d6', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'75d0ddd1-83e0-4afb-b73c-061fd9243f1e', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'79822834-1a8e-40be-a641-37057b6bbc35', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7e1717f7-d3ac-40b1-8787-e2065b3d008e', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'852c05d7-587c-4721-8e64-6be639b6231e', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'87768de1-0fae-459b-8375-815a90faab62', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8c5a9297-6012-4cbb-8d53-949cf82e3b55', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'90914d65-b7c5-4216-ac5e-be1f50d45881', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'927619c0-8ed7-4c57-939a-f8514c5fe52f', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'92d373b4-f47f-403d-8801-c6c72b0751f6', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'952fa1f1-d649-4cec-a906-3064f4dd62b6', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'96eb03a6-f007-4db6-9e78-d817a935596c', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'97fc3a53-a767-467e-bc31-c980370ae6f2', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'998a2fc9-4e93-44ca-b0d2-f739e4f61abc', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9bcece5e-c683-40bd-b4e0-a35771c2293d', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9cdcc628-ccbb-4997-a8d8-7233508d9fa1', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9d88226b-d07e-40d0-b0b4-12fecb7bf421', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9ee03b17-2fef-4590-a8d9-d7946d86d78f', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9ffae888-6364-4451-b359-7ba04d86aef4', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a222328d-6c13-4051-bd4b-0df7aed264c1', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a300dc9a-3768-41f2-b332-361d1241804d', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a77aedcc-bfdf-4353-8f88-3ab5ef990e0f', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a960cf7e-2c10-45d9-9f26-196ce0c9e992', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a9623355-1470-4581-a202-a229248d9b11', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a9c8ad71-a110-4c2e-b2fb-12a5580d6079', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'aa289112-2f40-492d-aea6-193dcf3c224e', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'aafbe55f-52a5-4459-9391-02094e59b500', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ab5b9a9c-a074-472a-ac8f-227466c5e493', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'aec2fd65-16b4-4d08-b924-5f79a9a87453', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'af3cb0f7-32ad-4c14-9c1a-883ae0b1b187', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'afe405d5-1b87-4217-9ef6-a4ba66d95a2d', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b1c1da7d-a756-4847-a611-2db0395a46c4', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b3226544-b994-49df-bb49-6b7a9b954027', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b473da25-cab6-434e-8717-b85a103125cb', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b958d3c5-4cc2-43c0-bbbf-558e96c36144', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c1600511-1a23-4cbb-93ea-2b8ec57dddd9', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ccf408b3-e167-4d5a-a900-87b67e250ffd', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cdbfd60c-ce7a-4d2c-b6ad-2a726de7b0c2', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cdf76519-0c37-4af0-a903-88c17832d308', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cee58283-c665-4791-8d08-995e9a292f47', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd2e93446-fa51-45dd-b4a1-dda71c70dd83', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd460f733-c3e6-4b4d-8779-30fc50b54042', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd5d40bcd-8d76-4ba7-9244-459861e36759', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd850f3a1-9e14-4c9c-bfbc-28dcadbc40eb', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'dcaf1fe7-c3b6-4c31-a188-fa1cbc3c0920', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e00740c5-2af2-4701-bd8a-f2aa3a6f06f6', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e2a234ee-6d31-4637-9a00-2bc3f3c7b044', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e487796e-904c-4888-b45c-374274521a31', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e580ff1c-b712-4598-977b-198780694f62', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e6bcdedd-53eb-4446-8152-20400926621b', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e7398c5c-a467-4b3d-b756-c18be32329d5', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e7836a11-7b57-443c-954e-15c43de81c6f', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e8baae6f-015e-4bcb-9791-351a377d9dd3', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ecee45c5-b4ab-4560-bd49-9ef32f90a3e8', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ee422e75-8221-40c9-a119-669a01d04e77', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f39af81f-9bf2-4409-b508-a9477648f5b0', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f3a33b9c-450e-464b-8f7b-e82b543aa2e5', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f5c7cf43-4744-4d32-b72b-caf0f1e879b8', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f79d0f47-a14a-4021-90cd-366e5724b227', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f8fd0e57-ac2a-4cdb-997e-7daeacf412bc', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'fa134161-de79-4821-af01-18900598e218', N'9c36bde2-0940-4f58-9328-9b558c2a2d64')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2ff47812-46d6-4ffc-8e2e-57ac1a028f81', N'def02a06-aedd-4a74-b0d3-99677755560f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'339a9de7-ded6-4f21-843c-ce95fb45d9a3', N'def02a06-aedd-4a74-b0d3-99677755560f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'50a7de65-83b8-4908-aca1-80e6fc84bae8', N'def02a06-aedd-4a74-b0d3-99677755560f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7138922f-dc6f-4c59-a6a3-538453e866d1', N'def02a06-aedd-4a74-b0d3-99677755560f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7bc5720c-d981-4b45-82b5-f52b6d06f75c', N'def02a06-aedd-4a74-b0d3-99677755560f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd1f45cee-a195-4662-9284-4a6d61f4fa57', N'def02a06-aedd-4a74-b0d3-99677755560f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e339c4af-9a9e-4b7b-ba39-f8262726c7b7', N'def02a06-aedd-4a74-b0d3-99677755560f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3065c679-5495-4031-9a68-478f9932ba64', N'ed0de298-3435-494e-9156-53581b3afd66')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'000a317b-64b4-4d1c-bf63-3b5b392770f0', N'16374', N'16374', N'16374@microsoft.com', N'16374@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEPWrKrWJCyXnt/95Kpda1pGfPwJKMJUlIVENOw7HGSufsS5H7PpvJ7EfgdWUVCkeyQ==', N'A2D7EMUEHKKI7SPTGO5KBITXSSFOJ5FB', N'8128c4da-da7b-483d-93d5-a3dd65f0af85', NULL, 1, 0, NULL, 1, 0, N'muhassab', N'MSB', N'16374', N'60749301', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'01bd52d9-12a3-4e0b-9034-d99e70ead630', N'6384', N'6384', N'6384@microsoft.com', N'6384@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEE5v+1tX4HZNV/NZQLWAe4Mqcley90AzZJrYpWrK8c4E3xjCVzRH80TeVKuBqDMZ4A==', N'GVFROVXPZPQI3AJAUKRZY2F4QJSDUJE3', N'8a8b542f-d781-49ce-ae71-e02d7839190a', NULL, 1, 0, NULL, 1, 0, N'Shabaz', N'Ahmed', N'6384', N'66766051', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'038c0f73-f669-4d43-bf23-cca622a4d0a6', N'26105', N'26105', N'26105@microsoft.com', N'26105@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEB5Wyjbzx5uYQ6FqciwWELXPKX7FUMMguwdgWL/bZd9+Z5UpufYFikJCF+SeRfuBUg==', N'46HZR3AX7AMFR75BUBQQ5BGN63HUKY75', N'50a62830-a120-462d-b0f1-ad49aeae0099', NULL, 1, 0, NULL, 1, 0, N'Ghoss', N'Gs', N'26105', N'55588899', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'04f68fbe-0c3c-4115-b378-9efdfeb1e476', N'23279', N'23279', N'23279@microsoft.com', N'23279@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDAJdBHxNazqeBeRK5rCEXb3aFOGp0BaiQEbxtOko1HFEB7PKEyFRRr7O2ngeMp76w==', N'BJRZJKWHRVXN4AVQVZAGBEPYNHGP7ZN4', N'4e96e091-9909-4468-a77b-dd3344fea7c5', NULL, 1, 0, NULL, 1, 0, N'vijay', N'VJ', N'23279', N'66722268', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'0564f1d3-0514-4806-b84c-d08b03e0287c', N'7', N'7', N'7@microsoft.com', N'7@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIT71DaL3LFl8Y0B2TGgKsE9F9Y219enCIA0TjkAjnDi3jjLqF1e1/TMywbFGf4MVg==', N'LLEVKVDTAT37LHVQ5YPZPDS75LUEGQEG', N'315bdbf2-a2e1-48f8-a774-216b95375391', NULL, 1, 0, NULL, 1, 0, N'7', N'7', N'7', N'7', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'06f3b398-c69c-44b1-8dcd-dd62adcd52cc', N'21493', N'21493', N'21493@microsoft.com', N'21493@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEFO4VQnaJlzD4/V+aILJ5tZf8nJ+9GbOMYvHtiFrVtUmv34u/hbPbnv2TmHQPf3+Uw==', N'IOWSU64RBDZOHBNHLH325YSKGSBZVWFL', N'fbf0aebb-e509-475a-bfc7-1fe27df4ff72', NULL, 1, 0, NULL, 1, 0, N'Arfan', N'ARF', N'21493', N'55566689', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'0b9eabc7-e819-4348-88b9-2eec7b0e608d', N'7436', N'7436', N'7436@microsoft.com', N'7436@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEEC1YOuATSaeQFMulxkimT/erTQ2VTPbFhEcuul51ztZfnRvhYi/xlVhZRsXWvS1fw==', N'KOJUPNFQP4ZWEIQJNEEIU3QVFX26MX3E', N'80babfd9-bfbb-4df4-a723-deeacb7532ed', NULL, 1, 0, NULL, 1, 0, N'Mohammad Afzal', N'Inayattullah', N'7436', N'12345123', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'0ff8d62c-71bb-4caa-b6b1-84d2276b7901', N'5642', N'5642', N'5642@microsoft.com', N'5642@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEJgJK3xXCotUNSLMR2qc01xWYP+fmOxCpQWtphzV3vh8r2ywTp4PCXrdTdCw2uHoGw==', N'WXF256BPUCRJYJW6JGUZRT3LV6FQUR2Y', N'4ea02b9e-b143-406b-b8a2-16ebdfbcf3c4', NULL, 1, 0, NULL, 1, 0, N'ahmed ', N'hassan', N'5642', N'66153961', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'10bc85dd-395b-4a92-aa64-f6fa39c88b56', N'29694', N'29694', N'29694@microsoft.com', N'29694@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEEFTRidAunl769CfecUHlTuNLch7ZGFl9+MrBmZVh/W5LMeyTPYh+y240pGTh4eNPw==', N'RVJ5V2CSHXR5XHTTWURZAMW6BJLQHZT4', N'9b57ae58-2861-40a8-99a0-92f3a237eb7e', NULL, 1, 0, NULL, 1, 0, N'Tanwer', N'TN', N'29694', N'88855524', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'10d5ff37-0fd3-49e2-aeb0-13ea736f13c6', N'6054', N'6054', N'6054@microsoft.com', N'6054@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEJ0bzZ27G7rvZgWeSKG8RG/pE/G7cUh97rj/cfy0lGoR6tQLhbksgaj2t4tq+95WFg==', N'W3LMVAVZK4Z5FMBC4L3COIYMMHRL7FQQ', N'15857cc6-b97d-4334-8b66-45d0c2a3c2b4', NULL, 1, 0, NULL, 1, 0, N'Zagham', N'Habib Rajput', N'6054', N'17777777', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'13d54f2f-b11b-49c9-a77f-4937ccbc514d', N'4402', N'4402', N'4402@microsoft.com', N'4402@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAELu7uSIH/AdVKOFJNL3l0QHnbbajMe5QqUt2oEtMfeM5WEedz+9u9ANHZZZsudOxdg==', N'4FE4Y77L5IELZADIRRX62SBCQB42QGX6', N'bbc95cd7-78be-42fb-9c92-92ef35cc98b9', NULL, 1, 0, NULL, 1, 0, N'Ajmal', N'AL', N'4402', N'55587345', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'158d996b-f18f-4ea4-a412-6e5caa97f160', N'7279', N'7279', N'7279@microsoft.com', N'7279@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDoWiaSewNFpR3FscH1yMxYo5pQwRmhQC9QuS+hEAydXocjl7suASDWxIwcPP7fa6Q==', N'HQPKQ4Q5DMAIJ74YZG4HSZTRROHACJ3Q', N'9c84ceac-ab64-472d-950f-a81e62e608ca', NULL, 1, 0, NULL, 1, 0, N'parumal', N'PR', N'7279', N'65129531', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'16023cd4-c0b7-42ef-9633-e269a5cfafa4', N'5259', N'5259', N'5259@microsoft.com', N'5259@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEK091Z9Y2ytH/celpbyJA0NxiEoFzZnvkfBWy+OnSTIGYKAjiEbL0nP0KE3uM7r7Dw==', N'7F6M3YBUYFKED2HQZDS6PWUWALXQGHMX', N'cdfc00d2-cdbb-41f5-a84c-6acbcbd3d9fb', NULL, 1, 0, NULL, 1, 0, N'Irfan', N'Thilge', N'5259', N'99426124', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'164c4db7-ac0b-48b8-b513-a6bbb3500c27', N'3889', N'3889', N'3889@microsoft.com', N'3889@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEL4AjK7qU3cjjoZoPhRO/gCGK35xDploaFhUf6r99qNPWCD7ziycZ8gq3PwouwC8Ag==', N'2WFRD2RIRENHJKMISYSWNO2TCJXJNK6M', N'ee2814a3-2367-4073-9874-15f6e2d71a28', NULL, 1, 0, NULL, 1, 0, N'El sayed', N'Mahmoud', N'3889', N'55588896', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'16ad66b4-1d81-42cc-b904-4397b9bbbad8', N'47275', N'47275', N'47275@microsoft.com', N'47275@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEECpI/6FpOKYJv9ioriMDfKAXn5a33ARxgQr2ovXhBIwRKvTNtGlbhzKCNqg5FuHnw==', N'6ZMLDWGAXR23UOMIPIZK4NIEGSX3QXW2', N'be7354e4-688a-49b5-9c2b-b095cf577b71', NULL, 1, 0, NULL, 1, 0, N'Abdellatif', N'Rezk', N'47275', N'12341234', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'1b770ebf-141c-4402-915c-846f6798eccc', N'22647', N'22647', N'22647@microsoft.com', N'22647@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEE+6b99Stxn0V9O3a6Jv/qIyLOIQlUVUktcE1rzad96GDAFCu83WJSN5VPXAdAs1kg==', N'EMK5C3NJS56TS4AQVSD5XCYJPRPVRHLT', N'91469b59-0aff-4d32-81a5-361d0774ecb2', NULL, 1, 0, NULL, 1, 0, N'Abbas', N'Kutty', N'22647', N'66390892', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'20085b35-cc40-4f3f-a7a5-ac9b3de3249c', N'27371', N'27371', N'27371@microsoft.com', N'27371@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIFeN19eHJJQdNyw+eQecmbImyEGi0EaMvQej/t5IV3H23g0DznHbFnaePQzTCibkQ==', N'6KI6ON462JFHDF4OY2KECKNOOZPIKEVG', N'6152998b-df6a-46ac-8640-23e36ca77548', NULL, 1, 0, NULL, 1, 0, N'Ejaz', N'EJ', N'27371', N'66699985', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'235b3571-7988-4420-903b-3854515dbe2c', N'25993', N'25993', N'25993@microsoft.com', N'25993@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAELZhOSWVigsTELzq1kqgFvcE5toXJgT84/G4cnxUO8zGoLDYLtcoQX2D7nL8NcQnxA==', N'FS47QYNSX2HJUQMC7ZTR7SPNUD75VR55', N'ae274eea-53f9-4916-8a2b-5708a928569d', NULL, 1, 0, NULL, 1, 0, N'Anantharaj', N'Balaraman', N'25993', N'60908343', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'23be8e97-8fd6-41f6-a819-6d0933ed90b5', N'31960', N'31960', N'31960@microsoft.com', N'31960@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEMcjx9i3lV9CszNIVepQ3RH37/B6pRJmPcNVNuq6/JnIhEH+Su6W9qHfD5RydrZsKA==', N'RK6N6Y6427MKTUNTB5FGBZNRY5UF474O', N'841776f8-09c6-4a4c-ad6d-bcbe5ff2850c', NULL, 1, 0, NULL, 1, 0, N'Sadhamhussain', N'Liyakat Ali', N'31960', N'33333332', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'246e6434-1496-431b-a7d1-22bb8357b63c', N'47574', N'47574', N'47574@microsoft.com', N'47574@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEKQb/b2stSYdJNKXjE1oxJsvTc0gfKtqNrDhMiSxcOVjZ1le1ja3u8yvnNB6DC0PpQ==', N'G4L6FBKY5MTROQZ25WKWAZ7LADZEISQ2', N'e748e9ab-8dd5-4d61-acdc-1b4c66db07d6', NULL, 1, 0, NULL, 1, 0, N'Mohamed Kamel', N'Mohamed', N'47574', N'33333335', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'2474bc21-28fe-4e66-83a4-cfdb9eeb4c5e', N'5', N'5', N'5@microsoft.com', N'5@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEB137L+YS7SFt/RyVZZr9/ZQoSho3HsyGK1P4s76nEV3wtTlMYfIvCtcewqr3yzgnw==', N'2F4TE6L7WVL54PSGJVKF7LTKDC22OXT7', N'6b61e099-ede6-4d21-b7cd-345c27fb0549', NULL, 1, 0, NULL, 1, 0, N'5', N'5', N'5', N'5', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'24bd41b8-4558-4b3c-ab32-6138c6a4db6f', N'29133', N'29133', N'29133@microsoft.com', N'29133@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEAQFmXvCX8uGqOs9lfsyKR7vbuVbhPA6ffZ1KDMqP7XcQ+iiwdDmTRROKMFgo/xcBg==', N'66Z4SIFRURLZYHDFAUH7VMGHYIJAHBBX', N'9838fa37-d157-4cdc-917f-35a0306c7dd9', NULL, 1, 0, NULL, 1, 0, N'Sohail', N'Anjum', N'29133', N'50222724', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'266480d5-f158-4fe9-9a43-e1fed36bb75c', N'1', N'1', N'1@microsoft.com', N'1@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEPitlBLmGDrV/RQKKygeqAZ7xxUd/omScryrCneEIzHzn4zNPgHWagR60etsHjhEIA==', N'PGPNAKOSANXQL22ZHFMGSLGBPU34NX63', N'aad0eca7-cd64-4241-aaa2-95a8b09aa61f', NULL, 1, 0, NULL, 1, 0, N'1', N'1', N'1', N'1', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'2695057f-960d-4afd-a5b1-4f2e09b145e5', N'31973', N'31973', N'31973@microsoft.com', N'31973@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEF+9YetTtEr5mTf2j4LltmPbysvGL5Gr8IVMwDHleEYYIu3qKXJcfwlEZGJkzlvHww==', N'BZNYNA4I5VZCXXXSJA7NPJPENIGY2CXT', N'55ce4f00-c4d0-4ca6-a1dc-e08e71cb5785', NULL, 1, 0, NULL, 1, 0, N'Srinivasan', N'Shanmugam', N'31973', N'13333333', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'27b0bad4-1bfb-496a-be53-aaeb57a6776b', N'22155', N'22155', N'22155@microsoft.com', N'22155@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEBEU4UiLhSCzOBE1ZgYCtv95cm3cJE8bbNQg83UfRj+qWZT+Is/PL4PjiFHExsIb7w==', N'FBBWXLNAA6AH3IMMEKRZ25NQVL43GQNO', N'6ac1834b-0d8d-4b6f-807d-3467b969b4ce', NULL, 1, 0, NULL, 1, 0, N'Ryaz', N'RZ', N'22155', N'88877745', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'2b650e74-0250-435f-a82a-da932f5eb567', N'43312', N'43312', N'43312@microsoft.com', N'43312@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEABSXYQEML5Bdi4cM4WxEmlmGVZHq5otk8Igp+lJoa55V6X14oHRy6/VnhsWU0Jeog==', N'G7WXPUWX2TELO6UCHKIZDMZP2I4XKMNR', N'8d205ec7-2d69-4c82-ad4a-c833d9ba6cfd', NULL, 1, 0, NULL, 1, 0, N'A.Alrahem ', N'Suhail', N'43312', N'22255587', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'2c5a30c4-d144-40fe-abea-7e249d0c856c', N'3', N'3', N'3@microsoft.com', N'3@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAECa3VvVaR69zzcu34if1ENB1etrfbBdrPm8248OR3fmjExd72jBYivvRgFtv/ojcQw==', N'GZA3JVBHQK5FRVK3WD4JRNBVQUO6UB3U', N'1043bd83-b106-4b83-8cd2-86adab9c5e2c', NULL, 1, 0, NULL, 1, 0, N'3', N'3', N'3', N'3', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'2df7a03a-3c2a-40d0-a127-4fdb3a17922d', N'47533', N'47533', N'47533@microsoft.com', N'47533@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAELShgVw21PYh+5ROU+RrD+g8oFDHnGEsDCQgUtX9VK04j2PJ1as/xL8n2q1JD8qSqQ==', N'EQAESCZK44O6L6F3MD3TP5OVOEWJSG22', N'851c7ac9-ae58-474d-9fb9-943168563fdb', NULL, 1, 0, NULL, 1, 0, N'Mohamed Awad', N'Abbady', N'47533', N'12121212', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'2f5414dc-e240-400e-9dd8-104b10b054b1', N'43587', N'43587', N'43587@microsoft.com', N'43587@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIBOK2IyyuUlr2C+Mweu2cuHKCGzbjCqS3AawsV6Ac0/7CW+0oRGjJJISp1YSj18pg==', N'HPGNCM7ZMSIPW67ZVHHESMZZAE5K4BCR', N'5fe21f6c-ed13-4b6f-aa17-a808dd8106d1', NULL, 1, 0, NULL, 1, 0, N'Thinamani', N'Chidambaram', N'43587', N'12312321', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'2ff47812-46d6-4ffc-8e2e-57ac1a028f81', N'16503', N'16503', N'16503@microsoft.com', N'16503@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEBuK734GVbTrWVJjYLAR2JS8mOIzvr/zcOVArhEF3x0AINrd+ahUHPscJojCbrYrRA==', N'JIFT7CGFTHNTX2UANQLSYTVLJKOAE2LB', N'0e8a19da-d3b3-4b57-a052-c99d6e103cdc', NULL, 1, 0, NULL, 1, 0, N'Emam', N'Emam', N'16503', N'44455569', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'3065c679-5495-4031-9a68-478f9932ba64', N'5681', N'5681', N'5681@microsoft.com', N'5681@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDYmMK1g1meILAiuIm6u56R7yAEjxlXbnGfSKO7Q5pRigjQ20XDG/ZQcTQwDARvzKQ==', N'YTIWKJJ6S5SOFGNJO37FUFQGOAR27HFV', N'935aaf50-5f8b-4e9e-941d-ec5fa40b829e', NULL, 1, 0, NULL, 1, 0, N'Rana ', N'Nasrallah', N'5681', N'96524962000', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'33649fea-39c2-4e41-addd-ccdad739c393', N'30707', N'30707', N'30707@microsoft.com', N'30707@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEE8jPNqqapjy2JXbtf4yDfKkdeSPhuYfBME6jrNuSmpDbsrdhiEzBK4ZC5R+0w7UtQ==', N'2M725INU3P4TTU3OSAOKWS5GAJHG2UAR', N'5fd700a2-77eb-40b8-af7f-a464114b2b87', NULL, 1, 0, NULL, 1, 0, N'Mohamed', N'Baroun', N'30707', N'97236647', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'339a9de7-ded6-4f21-843c-ce95fb45d9a3', N'123454', N'123454', N'123454@microsoft.com', N'123454@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEMjbFIrbnagsbf3O1GSvzgYiwnyT3QkQNi29vF8W+Ne12Z2RHcnBlU7h4XKFO6Fc6A==', N'NDEXWKTTNF5ZBU4BJDJ73VVQ4EQKCVTG', N'a06e3f13-75a7-49bd-a344-45cf094fa9b0', NULL, 1, 0, NULL, 1, 0, N'devvv', N'sevvvvv', N'123454', N'123454', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'33e097e0-96d2-4fe1-9e74-7ac7f8333004', N'15433', N'15433', N'15433@microsoft.com', N'15433@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEOPTMAW9N3th2HBLxwtxPzTbo9hb9D3FRD08H44j2OXfY6QQ6UbKdKbMHF8TU5S7HA==', N'NNI3EQ3OXHAXG2X7P7356QHBOR4IGYIS', N'c0fb594f-c6b1-439d-b3b7-f68e635f0e48', NULL, 1, 0, NULL, 1, 0, N'Shahbaz Gul', N'Ahmed', N'15433', N'66238487', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'35991b0b-81d2-4591-9ff4-4e96d45fad5a', N'4', N'4', N'4@microsoft.com', N'4@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEALRX+1Acw+JLMxe8Wp2nVWq0tMelyJJCjuNShx/OgRG9Iex2YPEnrnx9aNqtVnB5A==', N'VMI4UIC4SZJXGEP4VEWBGHC62HOMKV5Q', N'a2988071-fa67-4b75-9117-19f9a972114b', NULL, 1, 0, NULL, 1, 0, N'4', N'4', N'4', N'4', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'35d796d5-faaf-4821-95b0-24e1e69319d2', N'999', N'999', N'999@microsoft.com', N'999@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENE0Za+PRuc04a6AUq8pEm/a6AoCuj+3hZVEq9QeaOAABFCBsL0EEVOFxdFTyhlDqw==', N'6EO3NIPDGC35KV3J7AQMLEQZG6T6IDP6', N'0fe2ac8f-08ac-4e62-a3d0-40ad3d5209f6', NULL, 1, 0, NULL, 1, 0, N'darsh', N'Tech999', N'999', N'999', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'3889d25c-3039-4326-bbeb-0748ba7fbcc7', N'32573', N'32573', N'32573@microsoft.com', N'32573@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEKJTqTXceDWErY0PT1nOcq8Kd0jKvjUHbMoClLnASdU2Emn8LUBRhCHSBQg9bx0nmg==', N'4HIWSN6XNES6EVWUFMG2BCSPTT2RPIZT', N'2622d62f-cab1-4eaf-8fb9-d01325d6fd4a', NULL, 1, 0, NULL, 1, 0, N'mijahed', N'MJD', N'32573', N'55588823', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'3b8481c8-8f34-42d3-a296-cd01289fc67e', N'21756', N'21756', N'21756@microsoft.com', N'21756@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEP9Xsgw+u92LumVRe1xP0R3fKqe88uJC4eA59zbt+GKsheE7dJ+hDd5dhgS5SYxnlw==', N'PINTX5BAJQ6LTPFYSNRCZBW64P2RUF5H', N'4fb1ff0b-6a09-43f9-971e-445f720acf9a', NULL, 1, 0, NULL, 1, 0, N'shahid', N'SHd', N'21756', N'69987267', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'3c6fa9fb-2784-4c0d-a63d-dfd4a4a09ed1', N'33737', N'33737', N'33737@microsoft.com', N'33737@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEMLl4G7+EdA6Z+61ZXqudU4W+7QhcH9jLbm1djzoamfu6t+zcx8GYVQ1USWjU/cXlg==', N'6SPI4MAQALTWY3KHH6ACTOPUKOFOS5JL', N'bbb7c2b6-e2f0-4fbb-bf36-431d23d19b76', NULL, 1, 0, NULL, 1, 0, N'A.Alrahman ', N'Mohamed', N'33737', N'98555519', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'3db7ca38-8d5a-4b02-a91d-137c8718a39d', N'47276', N'47276', N'47276@microsoft.com', N'47276@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDKnW2MNJ+LTO3Ez352S0LVOMo1qAF2OkkKpq1aIlssb9lKCIGhjCTgigOCsC6yWZQ==', N'7QSFV6AZ56UM6MA5VPFA5L5ZJHEBXPLM', N'a4d7c959-1bb6-407b-9955-21fe169979e9', NULL, 1, 0, NULL, 1, 0, N'Alaa', N'AL', N'47276', N'55566633', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'3dd7ccc6-a183-48eb-be3d-5deaa6b1e82e', N'997', N'997', N'997@microsoft.com', N'997@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENrsUKbL5vsovDh/GbXsOtBNkc3lFq5izDqARF9CYzLA4HEB9BTIqB4ISbPerLPk/g==', N'Q74HSECPNFAUQRQ7LBEFGF4HAEFV4C7I', N'a863a131-d108-4862-9259-42398a9cf029', NULL, 1, 0, NULL, 1, 0, N'Darsh2', N'Tech997', N'997', N'98', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'3fcd316f-08e0-45d1-937f-93673f39788b', N'6', N'6', N'6@microsoft.com', N'6@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEN657KVjt6Xw1MDwAPqtWypzlbYTTupd/FZ8hW+O7JN7VPmHOyufrqTDDGtQxzi6Wg==', N'CNIFKBMGBY6J3QPNJT6N3AZ52LNLTALS', N'fbb07887-24d8-45b7-ac1f-55295ed1960a', NULL, 1, 0, NULL, 1, 0, N'6', N'6', N'6', N'6', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'43e99446-8dd2-4c04-8c78-d91286892e11', N'12489', N'12489', N'12489@microsoft.com', N'12489@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIyG89EqMVzM/iIjaAUhwrOJWumPfjQZX+UsVkepPIk8oGQOYwK2YiooHguJPdsNoA==', N'S4WV72LGE76KE62NOP6AG5WVP7PD4JTQ', N'227e4e0c-2ff2-426b-be12-dc42a5ac2182', NULL, 1, 0, NULL, 1, 0, N'AZZAM', N'AZ', N'12489', N'96762934', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'46f2d590-ff89-4fb4-85c8-8a634828d0cc', N'43323', N'43323', N'43323@microsoft.com', N'43323@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEC3OxuEFr2aRiq6kE4ijUKg/i2+0YjxUzvs4V1I19DIYV6zkjqqpDjtuGyRVt9Kvbw==', N'5SUVQSY7CMVE62PFRQC7BHLU32NXR6U3', N'c8fc7928-ef10-44b7-b43a-857a97c52962', NULL, 1, 0, NULL, 1, 0, N'Bijo', N'BJ', N'43323', N'55588814', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'48a503f5-a9e4-4111-bf4d-e0d199fdc5ff', N'6481', N'6481', N'6481@microsoft.com', N'6481@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEPK7p9vpHERFPMO9jwXa0QO9LtiBcL4MHnQ3D6zY6wV1FJ2kAbQdV1CaweV84rXr6w==', N'RHHJHW7XOG3GA7W4FUKLTZD2DMOPU6OK', N'6c17326e-ef2a-4409-b2dc-0d3795d479f3', NULL, 1, 0, NULL, 1, 0, N'Sarun', N'Sa', N'6481', N'50277844', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'493214b1-bcc1-4e3c-b1e1-e3da8c4359ed', N'16802', N'16802', N'16802@microsoft.com', N'16802@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAECfrYXaWNur3zJ1NZNo+tglrzQeJBe5guAUnvU+BN+MUDDTenn/ZhzXnWsj4BvoxpQ==', N'YEMZMAMWEJHJH53EYRQTOKOP5QXAAYYN', N'bf58c692-02b4-40db-8a57-e473fe6159f4', NULL, 1, 0, NULL, 1, 0, N'Nirmal', N'Biswas', N'16802', N'66197132', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'4f39836b-90c6-47b1-ba23-0f19d4e5ec60', N'32082', N'32082', N'32082@microsoft.com', N'32082@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEH6+4GxnCxday1EoizSZVJcab1MW0KtqEWh+OVBu2mqIfd8tEGhW/7C3edVDHOOuWA==', N'QBU2QWGO57E5CLVUUWFFOEQ3QPH4D4E5', N'820629f4-9ab0-4016-9457-5e727528778f', NULL, 1, 0, NULL, 1, 0, N'jaya ', N'kumar', N'32082', N'65616228', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'50a7de65-83b8-4908-aca1-80e6fc84bae8', N'45123', N'45123', N'45123@microsoft.com', N'45123@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEAiqhC0haWqc9vgX+BRHgCTC5UBcdpzbs+8eilSjS0KoJFX3Bv2NOrBG82sL0TqwgQ==', N'XULSTHV4LZVIQULQKSII52I6ONHDWPP3', N'934ca915-211b-41ee-baf1-097bb77174f6', NULL, 1, 0, NULL, 1, 0, N'Alaa', N'Abdesattar', N'45123', N'55588896', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'53f0bf60-5ab0-4cd2-aa8e-3ca584a86780', N'26988', N'26988', N'26988@microsoft.com', N'26988@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEKo0l2M+TSrCNhG4yuJ6R78VtddzWE2KgqFBa76Z+ZR9ATEVNcP49T5Vt6I2YFZazg==', N'DP7OVSYLU4DAU2QVIEVY3WCLKTZP5QXB', N'a61ccb90-364d-4ab8-ab2b-d7447ed18382', NULL, 1, 0, NULL, 1, 0, N'Ahmad', N'Saad', N'26988', N'50892272', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'558e6917-6b9e-4e9a-a73f-e0c23f8f47a4', N'23989', N'23989', N'23989@microsoft.com', N'23989@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEHFrfhbAilMztPmdPiw3rjiViqZ/yPvVZz3C9gnKGrGSx2NzD/JsSwgEoi9mbJo2cA==', N'4CDDV77RGNE2AF6TGEYWFBYSEYIJ7I5M', N'd171defe-22d7-4487-a447-46a814459d7e', NULL, 1, 0, NULL, 1, 0, N'Laith', N'Abo Shamma', N'23989', N'66766054', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'56c758e0-735e-4752-86a7-c706c98b36ab', N'6461', N'6461', N'6461@microsoft.com', N'6461@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEM/n9geE/QaW4ZI17I9kFPGHfDYWW/WDk3xhURPOdan73JxRmQHZYsf5ACt4jbIizQ==', N'7RBOQ7EPCJUJBILHT6POHHE56MBOYNEO', N'0a09de61-b401-4c28-8830-e5d9f0eff954', NULL, 1, 0, NULL, 1, 0, N'Majid', N'Khakhor', N'6461', N'50388706', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'5751cd8d-1739-4cbe-9839-3444e734aab9', N'998', N'998', N'998@microsoft.com', N'998@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEG8yldJqtHBnDyBltZSp2cg3iIhFf7yLf+8+4rSGW7SUTKuYoaNuoy+yIwLJ+54IdQ==', N'2EQLWAG2X3A6LQXLTGGKUASKVGIR3FFP', N'bb0d4dd3-0729-42b7-8ace-9fa2b74b5c9c', NULL, 1, 0, NULL, 1, 0, N'darsh3', N'Driver', N'998', N'9098', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'57a8a3b5-4cc2-4515-884f-bed0affe0a57', N'eng1', N'ENG1', N'eng1@microsoft.com', N'ENG1@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEJdoBgqBbtMlh2d3zBXXx/XI4eixu4yf6fIKfDB+fGjcV465AxlCg322IYhoDmrjVA==', N'FXIRYEL2UVC6FJXSZAH6AYHXDUYMZU7V', N'1eea982b-9f50-4c0e-8123-96082b450b46', NULL, 1, 0, NULL, 1, 0, N'mohamed', N'elshref', N'13489', N'106968528', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'5b86ecbd-5985-43d4-8396-a88443a61e62', N'4507', N'4507', N'4507@microsoft.com', N'4507@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEMOewu1Fl8W4X00h0qrpm7z0yvgEjA0BMkOpYnzRysAXqPGy8taNUb6PJuCpgD4iPw==', N'CPA44GYUQFRJEMZO6GXCFZFPB452SOVJ', N'8ea7fceb-0fc0-482a-bdb8-50051b657b35', NULL, 1, 0, NULL, 1, 0, N'Hamed', N'HMD', N'4507', N'55522241', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'5d6cffcb-0014-45ff-9594-356b4897acd8', N'16332', N'16332', N'16332@microsoft.com', N'16332@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEFyzBYUtGYaiI+/0MATbCKrVEj1bQxXH1P8tGCFFCiSB6zHI7aEm5X/QolpStMAPkw==', N'VZGP7P5DYMJKPMITKFLBECVOLBY4FE6Y', N'6a1d46d8-f45d-439c-bdf3-86ed88d7997d', NULL, 1, 0, NULL, 1, 0, N'Asghar', N'ASG', N'16332', N'99858289', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'63383e41-654c-4cce-b13e-28fb45ba80fc', N'32385', N'32385', N'32385@microsoft.com', N'32385@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEInKYOkL2wBl4IWN228fHDrlRR6FF8YJ+Q03KGtyVV4jnJpBgNjrGPDRU2vDCutfhQ==', N'Y5E6VZ4CTTCNUIW77CLAYMRGVGQ5S2YT', N'2a218b25-4c77-4a3f-8676-b7a54874cc12', NULL, 1, 0, NULL, 1, 0, N'Santhan', N'Santhan', N'32385', N'55522236', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'635ff621-aa25-4a00-adad-c3d4ff6847ea', N'12345', N'12345', N'12345@microsoft.com', N'12345@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEOClWs4jlbe6VeoSTW2Z4j5ET++Ddf6QC+UXn4+N8LnYzoqu/yrm33c1pgJe6i83yQ==', N'FUZRCZP4D35YGY2VQQCURDPNJOHBCDF6', N'e98caf33-2700-4c59-bc3b-27bbb7c59151', NULL, 1, 0, NULL, 1, 0, N'one', N'two', N'12345', N'99996666', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'63c63fee-74ec-4dda-a16b-ef322278b3da', N'8761', N'8761', N'8761@microsoft.com', N'8761@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEPXcctMGfo0kBxDqh0ARkCM88wzw+fp5lnv7ka/8H8QhyeSN9aHnGRLc7GeS2qv8Uw==', N'KFSCZ7H7KTSJXZQSOD3F526VFENDYAW4', N'a6377a06-72ad-4940-9563-161a6b32edb5', NULL, 1, 0, NULL, 1, 0, N'Mohammed Arsalan', N'Mughal', N'8761', N'55720069', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'64cbdd75-bcb9-4fa5-a4b6-a1d570464262', N'44675', N'44675', N'44675@microsoft.com', N'44675@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDcxcX8+LHvLWQg112YGwE7kebrHshGkat7mCzHFcPsTHLvwIxGEvDvwzIog5you0Q==', N'DC2ZXF7L7CTLYZ4YZT7HVT5ELOPMTR2Z', N'b9390f9b-e893-46a0-96b8-40cbd006bd4e', NULL, 1, 0, NULL, 1, 0, N'HARBI', N'HR', N'44675', N'66699952', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'6cd6c499-9475-470f-8a18-422dbc3e2808', N'11111', N'11111', N'11111@microsoft.com', N'11111@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAELu2JZMFoI7S/Dpn37sDPG+0D2DvPNTVKZzN0GKBLsk5YucFAf2hOuvlBWlyN9pseA==', N'XMTTLVFGNOSJTFISXWAI2QE45HLGWWUE', N'4e491457-a5f6-4be9-8a98-9a1f98b7990c', NULL, 1, 0, NULL, 1, 0, N'11111', N'11111', N'11111', N'11111', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'6e84b1df-2e53-4dcd-80e4-eb1e38b3c5d6', N'21497', N'21497', N'21497@microsoft.com', N'21497@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDlX/2t+Y75H04HtoLx17PAV3snviscRozJRtT7PSb2zVzDQAR2WdKF+ZslAKCKN2w==', N'BL67ANAWNUXZCQY6CTT2O6M6SHOCYLHQ', N'9562c36c-097c-41d3-aae0-e93edc025dc0', NULL, 1, 0, NULL, 1, 0, N'Satesh ', N'Sa', N'21497', N'50796568', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'7138922f-dc6f-4c59-a6a3-538453e866d1', N'16828', N'16828', N'16828@microsoft.com', N'16828@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEP0myvIk99QJmuVOKYtoVNBU28ataLHMPICJpi1KKiUJeyoHCJ/w75buvxwIDNgvtw==', N'DUWWOXYW7R54BV47YXX6POQ3CRWIOYXJ', N'a06bfcbc-da9f-445d-b1b1-082d6cf0f38e', NULL, 1, 0, NULL, 1, 0, N'Atef', N'Atef', N'16828', N'44455566', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'75d0ddd1-83e0-4afb-b73c-061fd9243f1e', N'43276', N'43276', N'43276@microsoft.com', N'43276@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEJT4Cq8oslTiiGHZo4yGOb5p4YAoKUWOiIEwfjdqaT5LLzojuRYTLun7/a0eyJmUTA==', N'5VXZBT7MFG2P2LWLFGBIDPKK5PQ24WKC', N'd9b751e0-d5d0-471c-989d-d91e8a15b59b', NULL, 1, 0, NULL, 1, 0, N'Imtyaz ', N'Khan', N'43276', N'44411123', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'79822834-1a8e-40be-a641-37057b6bbc35', N'6169', N'6169', N'6169@microsoft.com', N'6169@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEEMhmH0ZSQipit19VlSt6oWzTNNu8nioaNAh0PzR3d/lSsasL1NbYjWUwQw1UIwI5Q==', N'DRF6VMO257OETE44NUB5KNFATOBL35GK', N'ad83a3c9-3312-49e1-85d0-5dc21ec3dbdb', NULL, 1, 0, NULL, 1, 0, N'Arif', N'Khan', N'6169', N'23452345', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'7bc5720c-d981-4b45-82b5-f52b6d06f75c', N'Test', N'TEST', N'Test@microsoft.com', N'TEST@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEGyHEkg1svAmIWrrI4EdOiN0J+OFAEnCjOV5h9IFzk7l4rkrumUP6+EqRLqdYjKKPQ==', N'4DEBKUMLZAUQLGREJZ6NXDPZGGICUM7T', N'f1f67a83-08a3-4edb-9edf-6c3898044499', NULL, 1, 0, NULL, 1, 0, N'Test', N'Test', N'123456', N'123456789', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'7e1717f7-d3ac-40b1-8787-e2065b3d008e', N'40665', N'40665', N'40665@microsoft.com', N'40665@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEF4FK8ILe6G8wVLeke9+/+FRArKz2CAnruP3V8JJRUS9RF/0EO4kib+hixMCBMQbCg==', N'37FP2PUF765OA4BBMO23CLIXURX2I5OU', N'ba9d36f7-aeb3-4e1a-8444-80a2c02a7e5e', NULL, 1, 0, NULL, 1, 0, N'SHABBIR ', N'SHB', N'40665', N'66443931', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'852c05d7-587c-4721-8e64-6be639b6231e', N'22860', N'22860', N'22860@microsoft.com', N'22860@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIRXDsGUg1L6xK3Pcvrw9JjDPS9AxIpsamwJlfUuNIhuV3B+3hFFdzg6KPuj3mkQvQ==', N'NY3T4NQMPOHZ6EB4T4WUFRRUVQP4IEBL', N'b6a7c9d9-938f-477b-939f-c84e9f211749', NULL, 1, 0, NULL, 1, 0, N'Imran', N'IM', N'22860', N'55741775', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'87768de1-0fae-459b-8375-815a90faab62', N'5600', N'5600', N'5600@microsoft.com', N'5600@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEEf0phviyNHRwM2qJtskFT5/Pm+AkUd7Sn0GBoverJlJDd1tQCKSKwYfglLLi2Bb9g==', N'XLWHFJMAH5T4UDRCJF4QUXPHGNK3MX22', N'e09b50b8-01dd-48fa-a18d-7056f174ba5f', NULL, 1, 0, NULL, 1, 0, N'Mohamed', N'Nor El Din', N'5600', N'65626273', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'8c5a9297-6012-4cbb-8d53-949cf82e3b55', N'21495', N'21495', N'21495@microsoft.com', N'21495@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEFrHii3EQRBt/w8XsE9klY3f8uj9P46UJFyrqH9afOwwuwfSypnZ0gaxhjE3s4MIOQ==', N'DTRYQEXBRHBA4CTSAJT6NZY47HAGIDKF', N'd4d20f2e-7705-4cd6-b609-990ffe748a9a', NULL, 1, 0, NULL, 1, 0, N'George', N'Paulose', N'21495', N'55342126', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'90914d65-b7c5-4216-ac5e-be1f50d45881', N'33680', N'33680', N'33680@microsoft.com', N'33680@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEE772GQ+biLEKson/fZ3QDJv/bcoPemJNQ/TyDZ0Wz7frbuRaklxWJjgivKSAwKyNQ==', N'GW2V5WCOFNS4UUOIZJGI74DEDDIKQIUJ', N'bf827691-3273-48fa-a1df-ad9a9a0e9709', NULL, 1, 0, NULL, 1, 0, N'Abdul', N'Kabeer', N'33680', N'33333337', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'927619c0-8ed7-4c57-939a-f8514c5fe52f', N'24529', N'24529', N'24529@microsoft.com', N'24529@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIYYVFF379TEwmkRvSEr6I17B32DmxqzWvO+2tlRqO+2ittn0D4oHgo8bTOJGD/paQ==', N'SHYJPVQZD2HFQILL3YDHEY7FTNIV44S5', N'2420bb9e-408b-4c41-afa5-457582b2253c', NULL, 1, 0, NULL, 1, 0, N'Nizar', N'NZ', N'24529', N'55588823', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'92d373b4-f47f-403d-8801-c6c72b0751f6', N'123456', N'123456', N'123456@microsoft.com', N'123456@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDTe/KKROdWle1dKOZ8WOISufGMYDqlZNJbfdIhrIYQfY45t0B9u2L6s405x/DxqoA==', N'2BO66E5VZXKZ34FGF4ZAYBTDGT6C7PCX', N'13f9cd6c-5176-418f-81d8-c61cd0320f56', NULL, 1, 0, NULL, 1, 0, N'Alaa', N'Abdo', N'123456', N'9876543', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'952fa1f1-d649-4cec-a906-3064f4dd62b6', N'32102', N'32102', N'32102@microsoft.com', N'32102@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEPLe0aoLPddhv6H0t4Egl9N21hIZ3PP0xa0ksoZtSL/zTNaNW8ozjxMdwCXxqf7ryA==', N'BVERYI5LB4MK7SKPJPNFU7GWQKZBJBAI', N'34746251-74fe-4a9d-ad70-8672a1af20b3', NULL, 1, 0, NULL, 1, 0, N'sunil', N'SN', N'32102', N'55588823', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'96eb03a6-f007-4db6-9e78-d817a935596c', N'47266', N'47266', N'47266@microsoft.com', N'47266@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAELWtm+j1g689cnkgVpE4wOKvtjjWLxpGoqWfXe7wp/r69/Mps1JV+8tTHIQZg67VJg==', N'AXOHBWHZ35A6VVA6YOGHIJ7GT53ZA5L5', N'ff680b4b-c3af-4611-b30e-f222123b2718', NULL, 1, 0, NULL, 1, 0, N'Zeeshan ', N'Iqbal', N'47266', N'33333339', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'97fc3a53-a767-467e-bc31-c980370ae6f2', N'16223', N'16223', N'16223@microsoft.com', N'16223@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEBi1NrvO2uTXOKCCO6x3LzqX+RRaUN2N8n9FQrgXq8yj6fMC0cFK6sb6R2dYcMfSmQ==', N'Z3DBMOFJJNB56D6QXVGX4WATEITJTNGR', N'677785dc-467d-4ba4-8742-8e34aa8a5f31', NULL, 1, 0, NULL, 1, 0, N'Haroon', N'	Mohammed', N'16223', N'60620517', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'998a2fc9-4e93-44ca-b0d2-f739e4f61abc', N'37279', N'37279', N'37279@microsoft.com', N'37279@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEGrUBcMfdIRYuYI6T4wzqATubJ3qqU5MBIcWHqhqBiZXK9cbyPdic8YTk5QwFlXocA==', N'WRADDXO6QI44XCZWFSX2RWA5V4P2XB6M', N'4e50089c-b5de-4043-bdca-a43d84e6ae3c', NULL, 1, 0, NULL, 1, 0, N'Ihtisham', N'Anjum', N'37279', N'33333331', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'9bcece5e-c683-40bd-b4e0-a35771c2293d', N'38340', N'38340', N'38340@microsoft.com', N'38340@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDzpOGn1TasYKP/eG/i7jisq6N+T/IyewM1m0FLMpJsZG9cXKbIpG4G4zTSh1VZHTg==', N'7ZISUR7KWIT3BXXPS2YR4DDLQK7OVQ3Q', N'd234ed42-bd68-4494-a410-92923d647968', NULL, 1, 0, NULL, 1, 0, N'Abdur', N'Rashid', N'38340', N'99379677', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'9cdcc628-ccbb-4997-a8d8-7233508d9fa1', N'43458', N'43458', N'43458@microsoft.com', N'43458@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEOb5s0vJWHvHNqcg5bevp2hn1zSoknyHd6EaTIggx3BbHbAGW96rbxef6A+4TC6Suw==', N'X5JPDBUP3VPIRK6GHJMEQAIGQZJTUZP4', N'3cabfaa7-dcc5-432f-92b5-3000e84efc78', NULL, 1, 0, NULL, 1, 0, N'Ansil', N'Thajudeen', N'43458', N'43214321', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'9d88226b-d07e-40d0-b0b4-12fecb7bf421', N'43288', N'43288', N'43288@microsoft.com', N'43288@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDZOZOY1lTrrCHMYL2MhL79VYnRGPRDirk0gwhOd4CNLVERhRY41mFt1enSJS14gNw==', N'C3HK46UABHG2PXKLYRZDY43355YPYF6H', N'4131e514-21cc-436a-80f3-f9253542fbd9', NULL, 1, 0, NULL, 1, 0, N'Pujary', N'PJ', N'43288', N'55588823', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'9ee03b17-2fef-4590-a8d9-d7946d86d78f', N'32401', N'32401', N'32401@microsoft.com', N'32401@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEL+46xlxUVeUGcS+LYQxbECqNdRBXOYJue36x2cijiw50muKDWALdKhOboPY1Vqa3w==', N'M4BBO6F5JQST4P6PKZYTAWMAC3NHYU6N', N'd3878587-5514-4702-ac44-affaba196961', NULL, 1, 0, NULL, 1, 0, N'Baiju Varghese', N'Cheruvathur', N'32401', N'14444444', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'9ffae888-6364-4451-b359-7ba04d86aef4', N'32270', N'32270', N'32270@microsoft.com', N'32270@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEK3eZBf1lX8oekOJXtpnX5lWKvMKqXe7RP5Ud1ya0Kd4ZEtu56cZY7x1UknWikIX3w==', N'D2TASR42QJVHENOWW4MKIUDK2YXKLX3V', N'ff5189e0-cb04-4980-a912-a3bc4944e437', NULL, 1, 0, NULL, 1, 0, N'Ashraf', N'Ash', N'32270', N'66633325', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'a222328d-6c13-4051-bd4b-0df7aed264c1', N'40135', N'40135', N'40135@microsoft.com', N'40135@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEJn8wy8ZCwVEFPUHrt1+jSyRjR/FhUjviQZrWKpcg63gmhzIGbnn8kyPi1PgmRhhGw==', N'JR3YPR6HW55T4WRLRIX4OKS7TFURAYPS', N'281b94d6-d302-481a-9fa6-14b94499a85e', NULL, 1, 0, NULL, 1, 0, N'Ramzan Ahmed', N'Shaikh', N'40135', N'94769347', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'a300dc9a-3768-41f2-b332-361d1241804d', N'5282', N'5282', N'5282@microsoft.com', N'5282@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEEgh4E25KOTLCgen++hrQN0U7KPkCEH/YQbLBMUW6DLVXwuBZ9gxlEnZUIyS6wfFKQ==', N'TBTMFLKPNEPPXMG2TSO7DKUPEJPYZA52', N'51104f0d-17aa-4b27-9885-13fb893c70eb', NULL, 1, 0, NULL, 1, 0, N'PALAM', N'PL', N'5282', N'67613998', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'a77aedcc-bfdf-4353-8f88-3ab5ef990e0f', N'30847', N'30847', N'30847@microsoft.com', N'30847@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEH5O6g5NG/prFYMDfYX5etKJ9nb5LsAbpjZvDVWyz8/2pmLHevHrtNZOIMgNzNrX5w==', N'LZ3XLBOQXEQMYFWFHJCGEU65E46XQJCD', N'06622aa7-8ce9-4f72-892c-a110612da59f', NULL, 1, 0, NULL, 1, 0, N'Suresh', N'Kumar', N'30847', N'44444445', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'a960cf7e-2c10-45d9-9f26-196ce0c9e992', N'30705', N'30705', N'30705@microsoft.com', N'30705@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEEs/db79LYvM/3M+WA03TcfLuf+aEHCHsf/OP+d23Uimz5j6Qc57qzOjxSRcybw1GQ==', N'MWO7DKPPBIZZYLEHAMVQOR2WGWUCXHWA', N'8600540b-b4e4-4566-85fa-d792caf22bda', NULL, 1, 0, NULL, 1, 0, N'Hamad', N'KHALID', N'30705', N'55993987', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'a9623355-1470-4581-a202-a229248d9b11', N'56789', N'56789', N'56789@microsoft.com', N'56789@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEK25izVhr1nM5M3mh2WwIpa1+F61eWwIv/gQp08VuV6J8Mate2v2qYdrKm5ctNyvHg==', N'26BLBLI7TTMWGG7MUZKAQXFVYYWBFRFP', N'49964ac5-5783-48ea-9a7e-116376935c8d', NULL, 1, 0, NULL, 1, 0, N'three', N'four', N'56789', N'66655566', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'a96899ab-a09a-492a-be3e-6c751d62a3e3', N'1234098', N'1234098', N'1234098@microsoft.com', N'1234098@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEBoKi28BUt9oH5PeRvss+JiiKwQZ8J+kPgUZL7B9INJvPX4HLiunJI8ZQp77Xnxovw==', N'AGCGYSIP4UOD3OYV3GQPCLCQXSPNBLZ6', N'a5f101e5-d1e3-406a-b41b-bdee3c3bc070', NULL, 1, 0, NULL, 1, 0, N'123', N'123', N'1234098', N'213123', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'a9c8ad71-a110-4c2e-b2fb-12a5580d6079', N'31964', N'31964', N'31964@microsoft.com', N'31964@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEKKPx5MUVgLuhLsj/HVKgo2gAxbG1FYzBlnuHBlpMK5P3un5Z39PAQ7/v1rfzE3Qpw==', N'HYEXRI5M4AWVAVC5YRD3MRIE5B5O3T7C', N'3e83acd5-4d7e-42f1-82aa-0e7113ff409b', NULL, 1, 0, NULL, 1, 0, N'Seeni Mohamed', N'Nalla Muzaber Gani', N'31964', N'44444441', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'aa289112-2f40-492d-aea6-193dcf3c224e', N'32310', N'32310', N'32310@microsoft.com', N'32310@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEFRpjH/ebg3z+Gssoisu1D451cct6IlturI9kAr55V4Te9aRRlOUgsdI4L9Nz8z1VQ==', N'FA7WN3AAFMFJF5RD27S2MB4NUGTVUS6N', N'e77cdcb1-c303-4d23-8004-58cd5a1e9549', NULL, 1, 0, NULL, 1, 0, N'NIZAMUDEEN', N'NZD', N'32310', N'66633398', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'aafbe55f-52a5-4459-9391-02094e59b500', N'31928', N'31928', N'31928@microsoft.com', N'31928@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEINM63A0hByrSPlQ8blHP766bOsOzht32KKFQAfnx0kXLxLwfjIUy6admrXCTP3qMw==', N'TJKTNJ7FEOFKAQDKERI5KPA3HK5EH2LO', N'6f64a78f-1fdd-4269-9d6b-1a3e0baafe4d', NULL, 1, 0, NULL, 1, 0, N'Syam Bhasi', N'Kurup', N'31928', N'33333338', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'ab5b9a9c-a074-472a-ac8f-227466c5e493', N'26111', N'26111', N'26111@microsoft.com', N'26111@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEMm6AdZUt7lAGnQon39r9aGQPcLTSLXglRMprERT8JdpKrSPeDdCBxiO2hVXsTUNVA==', N'SJJPEUJUAUAPOMA5LQ4ROQQBEETDMDGC', N'634bafb1-02a1-40cb-b9d3-4a4271524fdc', NULL, 1, 0, NULL, 1, 0, N'Ansari', N'Ans', N'26111', N'55588896', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'aec2fd65-16b4-4d08-b924-5f79a9a87453', N'47391', N'47391', N'47391@microsoft.com', N'47391@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEAZ7BiB+lf0xZ1x1UW+gVucap7jj7Cg45tnfNB0v1S8ClmW3cPGdI/aKVj0Or8c6LA==', N'3YVNCBOXXNAFMBQYMTVAWWNLUT5FYBL7', N'd2e4e55a-e4c5-48e6-a48f-2ab7160b7ac5', NULL, 1, 0, NULL, 1, 0, N'Akhtar', N'Ahmed', N'47391', N'65140305', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'af3cb0f7-32ad-4c14-9c1a-883ae0b1b187', N'6150', N'6150', N'6150@microsoft.com', N'6150@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEBhpclmbn66GCapHZSaInpUY7vN47y01V1YzdrPlQfGgKaf6i//O0us5qdk26dmQpw==', N'RAIZALIX6TPVSIOOJKPEXGKGPNQBW2H3', N'501c8546-7604-463a-8aac-7adc36c4e8c0', NULL, 1, 0, NULL, 1, 0, N'Moideenkutty', N'Mohammed Kutty', N'6150', N'66273046', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'afe405d5-1b87-4217-9ef6-a4ba66d95a2d', N'25601', N'25601', N'25601@microsoft.com', N'25601@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEB8EVU1aRAL6wEtd0qdautPNINkyf/xmEPYilqL48U0Edr4fF53gI2P710KWPCnU2Q==', N'3RIEW5V5BMQ6FNKEPA6BSXWN53CUC2EK', N'5e73cd7c-67cf-4371-ae52-73cb4c65253f', NULL, 1, 0, NULL, 1, 0, N'Asif Qasim', N'Duste', N'25601', N'44444442', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'b1c1da7d-a756-4847-a611-2db0395a46c4', N'39750', N'39750', N'39750@microsoft.com', N'39750@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAED7rpYULTsmjZUHaJ30vd6jTnZYu8EVT4rZTlqys+B2SNEpK17N4Ph3QqMDBpZ/VZg==', N'BIMOVYE3NMG4FXMAXXJUB3NP5AA46CCZ', N'b588976f-c8a5-4908-a4d0-56215af2a2d3', NULL, 1, 0, NULL, 1, 0, N'Pradeep', N'Thadathil Raveendran', N'39750', N'65860948', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'b3226544-b994-49df-bb49-6b7a9b954027', N'22582', N'22582', N'22582@microsoft.com', N'22582@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEAkQaG+ZO/98WLgPAX4bLO5GeExR7bRQz2dck/FYUYjO7l/9Jm4omlZ5zTqMBeDiFg==', N'MIWYUORJUNUNFQLK2JQHWMNGZDMCCGJD', N'797e75b0-b34f-489e-9043-fc37d7175956', NULL, 1, 0, NULL, 1, 0, N'Tausif Abdul Aziz', N'Dalvi', N'22582', N'44444446', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'b473da25-cab6-434e-8717-b85a103125cb', N'2', N'2', N'2@microsoft.com', N'2@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEAokWpc1FKYwmTwkOT+Sq5lsHlz71lWUQbvnV5a1d+ZrEU9PM8Ca/EzmvkZlE7zk4A==', N'SX2WBCBXJVVU5VSL6QAQZ4LHO4PZ4I4Z', N'306afac2-a0fa-4e9a-b69c-aa4427d4f92f', NULL, 1, 0, NULL, 1, 0, N'2', N'2', N'2', N'2', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'b958d3c5-4cc2-43c0-bbbf-558e96c36144', N'31866', N'31866', N'31866@microsoft.com', N'31866@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAELPWe0wXGzdDxT3BCDr4QJdJkVWTa7xzKp/csr59YF4znIVmbj5In70SU650uoBLNg==', N'W4LZMBOEL2PXGYRP6R7SG6XOTZ6YWYBI', N'38173c7b-5db3-4446-b1be-59c16da77b2b', NULL, 1, 0, NULL, 1, 0, N'Mariyadhasan', N'Santhanakrishnan', N'31866', N'55704439', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'b9bfeb80-82a7-426e-86df-52cb39e0e31d', N'admin', N'ADMIN', N'admin@microsoft.com', N'ADMIN@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEHAvp/N09cAHPgvbKTf32o4EP+oLn+D1aTSQGk9lmscZ68NVJmDiQrRzDQhypHdPCg==', N'5KIXBHAOVEIUEX75DMPF7QR5T5BKEJTF', N'89eba453-6faf-490f-a877-77606767698c', NULL, 1, 0, NULL, 1, 0, N'admin', N'admin', NULL, N'12345678', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'c105727c-cb48-4375-8dd3-d9d9d305dcb0', N'1234', N'1234', N'supervisor@microsoft.com', N'SUPERVISOR@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEKcu4RlE91g/BxR1mvmDw80S7f+GBFES2oKOv+nPXIEp0mC71N7yHixLfiK0dEfAPQ==', N'U3RC6WTTWJVJTZUTFWXO64YXUFFF7VXP', N'1055b032-46e1-48cd-9f6f-76c87725b505', NULL, 1, 0, NULL, 1, 0, N'Ezzat', N'Ezzat', N'1234', N'111111111', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'c1600511-1a23-4cbb-93ea-2b8ec57dddd9', N'31971', N'31971', N'31971@microsoft.com', N'31971@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEHd6V+HFbUZNZaz4VVpZVR+FyMuOABFbtNQP3GYVf3DZ07bzVBTIFZwBTaRMH6fRow==', N'NMHY2TFYPHLJOTNTNDI6TMDTXPTXPYTD', N'b924645c-9976-4a5f-ab0e-232fb1d37d16', NULL, 1, 0, NULL, 1, 0, N'Senthil Kumar', N'Radha Krishnan', N'31971', N'33333338', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'ccf408b3-e167-4d5a-a900-87b67e250ffd', N'16337', N'16337', N'16337@microsoft.com', N'16337@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEJr10LLHmlumJVPoQKVhINdlfk1vAFqAMRkv3wU8gfHWGQDEL6sbZUN9XM+/wM4XRA==', N'OKOFNPC2T2CYTXIRT5E4AWHAC3YGZ77N', N'df3a960b-eed9-4258-90c7-c06e4511c13d', NULL, 1, 0, NULL, 1, 0, N'khalid ', N'Nadem', N'16337', N'66068736', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'cdbfd60c-ce7a-4d2c-b6ad-2a726de7b0c2', N'8708', N'8708', N'8708@microsoft.com', N'8708@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEOcCFA1vabp2tpZ563MhTsmwz0TH10oBY1MmCdTIC2Mo5Uyll0qjazrvTa+GlXKwZA==', N'H43UPC2TKTZFBX63TLEZVERSE3OVYLKK', N'd4a0d0f7-1c2b-4328-9e13-520d1228c5cb', NULL, 1, 0, NULL, 1, 0, N'yousaf ', N'Kuti', N'8708', N'97606264', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'cdf76519-0c37-4af0-a903-88c17832d308', N'32308', N'32308', N'32308@microsoft.com', N'32308@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENUs4JmrLfnJsdQZBqo6gJ7SsTPRJpp5KmjlemVHyflO2C61Zhi6Q/6SRmi4AVIRQA==', N'HTRN5AAM52SB32UONFI2GFG7BDBHZ73X', N'ea7bc211-8263-45e2-9d93-b3dfb411f382', NULL, 1, 0, NULL, 1, 0, N'Ajiesh', N'AJ', N'32308', N'22255587', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'cee58283-c665-4791-8d08-995e9a292f47', N'25973', N'25973', N'25973@microsoft.com', N'25973@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEPco2rdYTWyWQldQJbP8j4nkxdS6xC/L9qMR8NkTErPOSlrekxbSw71m2YPa/HZ/pA==', N'COLJZ2VHYC5ICMZ5EV2QKRVDUYMGE2U6', N'348c39c2-701a-40ae-8289-3c2df1fe3367', NULL, 1, 0, NULL, 1, 0, N'Imtiyaz Ahmed', N'Khaleel Ahmed', N'25973', N'60085428', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'cf90a295-3432-4b5c-8a23-b98601d2abc8', N'2589', N'TRAY1', N'tray1@microsoft.com', N'TRAY1@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEObCiRwb38bbcJh+qvih/HK2lqXk2/zXD2JslLElUleu0U6nUNmv2g8tGh+UJOka6Q==', N'CUZEHZ4YF6RZPAATMZY3X5U6HBJD3H7Y', N'f0ba8902-9254-4087-8f6d-a80fddf5c8df', NULL, 1, 0, NULL, 1, 0, N'tray1', N'tray1', N'2589', N'44411155', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'd1f45cee-a195-4662-9284-4a6d61f4fa57', N'4128', N'TRAY', N'tray@microsoft.com', N'TRAY@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENaqAyMbZ8SWaJLt4wHKIZN7cajIue7V7ti1C1B010FKSDIXzgzxjQLtPQoNHhkflQ==', N'ITSURO5V6DF466VTVRUGSC5D6ZPYC4QF', N'27ac0203-3139-4ef4-8067-2ae84991bfdb', NULL, 1, 0, NULL, 1, 0, N'tray', N'tray', N'4128', N'66699988', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'd2e93446-fa51-45dd-b4a1-dda71c70dd83', N'29948', N'29948', N'29948@microsoft.com', N'29948@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEOVIr/SgQfAYad9642V+pVkFUCKau65APqTIolLOtplEIyf4CnfOSe15uJ1f43LLdg==', N'HQV4GSJJDU4EZ4QDN2O576KVQT3YSMVY', N'1ab03c85-e175-4784-a5ed-f62e242cab91', NULL, 1, 0, NULL, 1, 0, N'Yaseen', N'Ismail Bate', N'29948', N'12222222', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'd460f733-c3e6-4b4d-8779-30fc50b54042', N'32479', N'32479', N'32479@microsoft.com', N'32479@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAED2lj/9iKdLES04nwubNCAQet0M6QA4x+ksp+uv3s5Qyoi2IAGSYu1P2begNX8l6eQ==', N'M2YJ36P5RW4QHLIBVGZIXGIMHYW5TIP6', N'ce11a7b4-07c0-4b8f-99f5-6333ad77e50b', NULL, 1, 0, NULL, 1, 0, N'Senthilkumar', N'Pachayappan', N'32479', N'65993140', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'd5d40bcd-8d76-4ba7-9244-459861e36759', N'41151', N'41151', N'41151@microsoft.com', N'41151@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEFXfK5UeKb00yH/hz3wLXvT4bVoLQewqHLMhkFZu06SgLx8FDJFLtTRrob+X8rcVng==', N'Q7MO734WNKKHQKNHNBYF2QFAWHKVKMUM', N'a2701d39-d3bf-4a8b-b66e-57c71b7e6cf2', NULL, 1, 0, NULL, 1, 0, N'Devi Lal', N'Patidar', N'41151', N'54321543', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'd815b64b-fdb2-4ed0-8502-7e760804882d', N'5966', N'5966', N'supervisor2@microsoft.com', N'SUPERVISOR2@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENAdNChhnZk2TidFZbAeExBub30I3lX8eElNla97nmawe0RuozOpL7oW/27AHqOlgA==', N'WYWWD73R7O5AHP35B4I7J7JKHYI2CZKE', N'248f6ed7-49bd-47f2-b0c3-2603eaf59ab0', NULL, 1, 0, NULL, 1, 0, N'Walid', N'El seman', N'5966', N'22255536', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'd850f3a1-9e14-4c9c-bfbc-28dcadbc40eb', N'33210', N'33210', N'33210@microsoft.com', N'33210@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIqB0mFnP7aISwmnFALVd/hQcUHaW6K9ImgTag3aY7t/vP4zKUziGBRhwxoLHFR/hA==', N'RCDAQMASYWXPQLCV5CSWQXS6LI4J5C6W', N'b490d0bf-f2a0-439a-8109-cc5e144a2fe1', NULL, 1, 0, NULL, 1, 0, N'Mohamed Iqbal', N'Mohamed Yashin', N'33210', N'33333336', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'db3f0f10-2ad0-48cd-b938-c545b5a513f3', N'1234258', N'1234258', N'1234258@microsoft.com', N'1234258@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEF+BAFxKlBfCMLzaPLi6zEiyhNrG7mwNhPVPU6alzcMtsVW1DKIskciDCxn4jbkpIw==', N'LZTTOL5WU6YGYBOEXRG6QGEY4I5HGZT7', N'afe288fa-fba5-4275-8fe9-ca6c4e9c6270', NULL, 1, 0, NULL, 1, 0, N'FormanForAlaaDispatecher', N'lastname ', N'1234258', N'123123123', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'dc2f2a13-2c48-4a56-9c50-1e2d8bf57bea', N'3725', N'3725', N'3725@microsoft.com', N'3725@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAECmqtWYp7wkCfvESJ0qvGowTsnRtIsWC4yibWFlvV4yjvcB5k7q+ZuBHMe6ltl1w1A==', N'4KISZU5SNAG7UA2OBGRY6MSKQACHFYZ5', N'94537e7f-fa59-44c6-bb94-db0f355f6797', NULL, 1, 0, NULL, 1, 0, N'Nassir', N'Iqbal', N'3725', N'67069138', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'dcaf1fe7-c3b6-4c31-a188-fa1cbc3c0920', N'45403', N'45403', N'45403@microsoft.com', N'45403@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEOwryTecbXnHPBLpqx/ySNAgRvDFtMF3NqEt37Pic7X4+O+rrrjGhKMff/ZFLMlB2Q==', N'J7CTMSCYTTAXQEXKKS73W3MZTRSS66GE', N'ee51151a-6847-4c02-aa4a-07ae9e402a97', NULL, 1, 0, NULL, 1, 0, N'Amer', N'Allam', N'45403', N'33333335', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e00740c5-2af2-4701-bd8a-f2aa3a6f06f6', N'11641', N'11641', N'11641@microsoft.com', N'11641@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAECbOEZLDgKqE7fEJcZxVy6ZYwQvLPueSBf5plq96a/h0AQBOeJAbUnzxjm3C7mQEdw==', N'VR4KDXAYQFYIK2HO3NY3W3UGBY5TXH45', N'88ccac92-8db0-4f66-aa40-a656306847af', NULL, 1, 0, NULL, 1, 0, N'Shaik', N'Kareemulla', N'11641', N'44444449', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e2a234ee-6d31-4637-9a00-2bc3f3c7b044', N'11441', N'11441', N'11441@microsoft.com', N'11441@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEFy4HE7WjT6Sw/C+EQOR9Gz1bwbDQUf3PfaEioNBGkIB+lAqmzSxJssCd2WvAdWGyQ==', N'ZTBEX43YYAURBJU6GKIO5OPUHILVNAOJ', N'5501328f-fc7b-4d74-804e-9246b2f63772', NULL, 1, 0, NULL, 1, 0, N'Faisal', N'Ahmed', N'11441', N'66400725', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e339c4af-9a9e-4b7b-ba39-f8262726c7b7', N'9147', N'9147', N'9147@microsoft.com', N'9147@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEDJS06SII39aqNC+TSsZ4IEGtOMB2QAN06O1IxLwO1/dTJkkVL18drh3eG9VV4qCcQ==', N'QWI6YE5OSBVWQYG2WMCYTLTHB45JMAFQ', N'1eb00233-a59c-474e-9b3d-ac93c862cde9', NULL, 1, 0, NULL, 1, 0, N'Mahmoud', N'Salah', N'9147', N'55588847', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 0, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e487796e-904c-4888-b45c-374274521a31', N'4005', N'4005', N'4005@microsoft.com', N'4005@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEMhjLSuuHynG8UHQK3TLKt65gzmeCeJF4uw7barVUEt9L/FLnfKl9VURPRWpzBy1hA==', N'JLIMSUPUD4VTGIWCKDGDIP7LJSRURGHY', N'02747121-870e-4416-af35-267c534bd114', NULL, 1, 0, NULL, 1, 0, N'Mahmoud', N' Hussen', N'4005', N'66025129', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e580ff1c-b712-4598-977b-198780694f62', N'32137', N'32137', N'32137@microsoft.com', N'32137@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEErHisvDefYF24TO5/SEZnP20RwdNoWAkEAOnjbG3tM8AXljuBzd59fCHr3lCNr0yg==', N'SSDOKXB6NMPOI7V4L5YCGZC473PDGT2E', N'8e5ab316-2fa5-4836-a6d8-208301aeac7c', NULL, 1, 0, NULL, 1, 0, N'SHEIK ', N'ALAUDEEN', N'32137', N'65914994', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e6bcdedd-53eb-4446-8152-20400926621b', N'5067', N'5067', N'5067@microsoft.com', N'5067@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEL2VcIdnCoKyZof7TypuW8nSbTmfF986KWVq8HkT7a2SwszO/8lqB7M+ppUi9y9ZgA==', N'R3X7JBF4MSQ3PEBZEH4CMPTVH2M5CZTK', N'7f20cfdb-49e4-4ef7-b94c-c57335c4c776', NULL, 1, 0, NULL, 1, 0, N'Ramish', N'Ramish', N'5067', N'88855523', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e7398c5c-a467-4b3d-b756-c18be32329d5', N'17913', N'17913', N'17913@microsoft.com', N'17913@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENSy95kZCQDZFLP5q/NczAARULkxxeSPXNcCKFNegikqbGh1TXoZN9osUSMGMaV0Zw==', N'U6YBRBBEZIR6HBC4CTP2EKZ5L6UIDBFO', N'e225f1ba-36d3-4be3-a4d0-715e6c8ae79c', NULL, 1, 0, NULL, 1, 0, N'TANWEER', N'TN', N'17913', N'44455566', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e7836a11-7b57-443c-954e-15c43de81c6f', N'996', N'996', N'996@microsoft.com', N'996@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEKIjaLUWzApwvVj4TRyneOxlel1b/bB7RFmZf2RDVjtXmOBqI/k3Qdo6FUtKKk1DTg==', N'3GT56TK2JW7POHECSKEUFRBCZWIXMVEM', N'139432c6-3d43-4524-80a8-9e8ad3923247', NULL, 1, 0, NULL, 1, 0, N'darsh', N'Tech', N'996', N'232', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'e8baae6f-015e-4bcb-9791-351a377d9dd3', N'31564', N'31564', N'31564@microsoft.com', N'31564@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENTCdgcemi8cR2Kfm8pu496w+02jHcbSg/nxdlnRSYHKa3ix01+BAm0WtxZgg2rTWw==', N'TTG5SMPB3VNSSID4WEIN2ZRQ7KKOD5R7', N'9713ad10-5734-4e64-8ec3-a899c7bb0586', NULL, 1, 0, NULL, 1, 0, N'Balakrishnan', N'Chandran', N'31564', N'32132112', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'ecee45c5-b4ab-4560-bd49-9ef32f90a3e8', N'32997', N'32997', N'32997@microsoft.com', N'32997@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEJHpskNY7wUWq6X6U6SiYvtX/uk/pSKMGKOpp0SbGxAcVmMnqXrlIju1b9z6fRe2cQ==', N'JPDW62ATHBTQW5JCDWFBFNFX46XRX7UQ', N'90826955-9232-40c6-b5b0-de0c0f1b2c1c', NULL, 1, 0, NULL, 1, 0, N'Nasir', N'Basel Karel', N'32997', N'133333333', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'ee422e75-8221-40c9-a119-669a01d04e77', N'43329', N'43329', N'43329@microsoft.com', N'43329@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEGho2eO+yrxx3AMlPowpR4ZvnqEyMwQFlOQpDYrAuJFkhSgWN2Lzud6cEBpMqMxpwQ==', N'NENP26XJT4IE6YP5WPKT6H7M7VJAINAP', N'ffbab16c-2bfd-4b0f-a3be-7bf802f52222', NULL, 1, 0, NULL, 1, 0, N'Erulappa', N'ERL', N'43329', N'99988832', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'ee990494-bac0-4f51-b3cb-b58803d6e49b', N'18450', N'18450', N'18450@microsoft.com', N'18450@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEHai0ObMdhiWqpMADO6zejU8u1nwCzlfZoWNLO+Tahj/jPz8vO6D08pSUKgYth5htg==', N'YPHTTH5S3ERBIKE5PCY4HQNA4X5HP45P', N'd1849a0b-d5b9-4b22-953f-d5da31aefa5b', NULL, 1, 0, NULL, 1, 0, N'Mohamed', N'Tawil', N'18450', N'66947747', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'f39af81f-9bf2-4409-b508-a9477648f5b0', N'8740', N'8740', N'8740@microsoft.com', N'8740@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEBVrF7E696b97+ukINsD/Q7JbeMCXgXfxLNzYRUXfE9xPsmXJuyJW8ixMg1o5ZYaGA==', N'27EPRWTRCYW2CDMRYTG3LSJRRJ4AMG4M', N'0e5f94fb-d141-4958-89d3-8e61446fb783', NULL, 1, 0, NULL, 1, 0, N'Nadeem', N'N', N'8740', N'65140305', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'f3a33b9c-450e-464b-8f7b-e82b543aa2e5', N'31970', N'31970', N'31970@microsoft.com', N'31970@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEAaR2WuDU1qfkldtCo+qAt71kNsVgQ3BdzNlM+v/oR3O0HDwxpdHuw1N8I9fyx2HrQ==', N'MYTHPZCJSYZZGQH6O2FUX5TYD5GK7HLP', N'f4121617-b1b0-4954-869f-2b64dc395377', NULL, 1, 0, NULL, 1, 0, N'Naeem Ali', N'Firfire', N'31970', N'65009199', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'f5c7cf43-4744-4d32-b72b-caf0f1e879b8', N'16113', N'16113', N'16113@microsoft.com', N'16113@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEIwfWiDzUC1poQq25v5I2QJfHj6qLQYtK9lLEMrzEGsB3hP3JQhp2rjlmJa1CqoooQ==', N'SWWJ6LY6D3WRXOODL4CYZEURYXTNOYT7', N'9140ad37-164d-485b-9571-fbf77a9a418e', NULL, 1, 0, NULL, 1, 0, N'Manoj', N'Ravindran', N'16113', N'65161573', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'f79d0f47-a14a-4021-90cd-366e5724b227', N'20094', N'20094', N'20094@microsoft.com', N'20094@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEO7Inz6AEoWhCjwSBRaGNMO8fOpmIk/iVYQskfyCBDKJaJ8Uc4SzgQlHHhqlmlYJoA==', N'WIJQFUXWWREY4STO2P7ZW35ZTDOY37YE', N'b20f7cdb-f4a6-4e07-80f0-c390bb69a3bc', NULL, 1, 0, NULL, 1, 0, N'Pirmal', N'PRL', N'20094', N'97179530', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'f8fd0e57-ac2a-4cdb-997e-7daeacf412bc', N'16046', N'16046', N'16046@microsoft.com', N'16046@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAENY5uorY6hggFJmvA1i0UGBIHkp0Y8H1vD/ryj8z7fxs+7R4bZt3s4RykKkADB+B+w==', N'VAUZKAQPM6HHOOTSA3UWWPN6VHKTEIXR', N'44da5d6d-1a6e-4893-8ec8-dfb55a027c05', NULL, 1, 0, NULL, 1, 0, N'Sulayman', N'SLY', N'16046', N'97832805', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName], [PF], [Phone1], [Phone2], [FK_CreatedBy_Id], [FK_UpdatedBy_Id], [FK_DeletedBy_Id], [IsDeleted], [CreatedDate], [UpdatedDate], [DeletedDate], [Deactivated], [PicturePath], [IsFirstLogin], [LanguageId]) VALUES (N'fa134161-de79-4821-af01-18900598e218', N'27136', N'27136', N'27136@microsoft.com', N'27136@MICROSOFT.COM', 1, N'AQAAAAEAACcQAAAAEHFnyboXHUsnBDupg1PTP2jFAC3+dbhxo5a7C1lG/bYuOwj4OpiCYlbXhUBbcHgvBQ==', N'DA4NMTYZYGM2OOAMBWPO46EQXPPOS7YE', N'22797842-7106-4e91-8784-d707a2b750d1', NULL, 1, 0, NULL, 1, 0, N'Ahmed', N'Rajab', N'27136', N'65805877', NULL, NULL, NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, 1, 2)
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (188, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (188, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (188, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (188, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (188, N'Time', N'1586058400')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (189, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (189, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (189, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (189, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (189, N'Time', N'1586112660')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (190, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (190, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (190, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (190, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (190, N'Time', N'1586113233')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (191, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (191, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (191, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (191, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (191, N'Time', N'1586114420')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (192, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (192, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (192, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (192, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (192, N'Time', N'1586115677')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (193, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (193, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (193, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (193, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (193, N'Time', N'1586122867')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (194, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (194, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (194, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (194, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (194, N'Time', N'1586124087')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (195, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (195, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (195, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (195, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (195, N'Time', N'1586125274')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (196, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (196, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (196, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (196, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (196, N'Time', N'1586127551')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (197, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (197, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (197, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (197, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (197, N'Time', N'1586127645')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (198, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (198, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (198, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (198, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (198, N'Time', N'1586128805')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (199, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (199, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (199, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (199, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (199, N'Time', N'1586130214')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (200, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (200, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (200, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (200, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (200, N'Time', N'1586135187')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (201, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (201, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (201, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (201, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (201, N'Time', N'1586136001')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (202, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (202, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (202, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (202, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (202, N'Time', N'1586137204')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (203, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (203, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (203, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (203, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (203, N'Time', N'1586149068')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (204, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (204, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (204, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (204, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (204, N'Time', N'1586161061')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (205, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (205, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (205, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (205, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (205, N'Time', N'1586161201')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (206, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (206, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (206, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (206, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (206, N'Time', N'1586162408')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (207, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (207, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (207, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (207, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (207, N'Time', N'1586163603')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (208, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (208, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (208, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (208, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (208, N'Time', N'1586194503')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (209, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (209, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (209, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (209, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (209, N'Time', N'1586194848')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (210, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (210, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (210, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (210, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (210, N'Time', N'1586201344')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (211, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (211, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (211, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (211, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (211, N'Time', N'1586202006')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (212, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (212, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (212, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (212, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (212, N'Time', N'1586203209')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (213, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (213, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (213, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (213, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (213, N'Time', N'1586204412')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (214, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (214, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (214, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (214, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (214, N'Time', N'1586288008')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (215, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (215, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (215, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (215, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (215, N'Time', N'1586288450')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (216, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (216, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (216, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (216, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (216, N'Time', N'1586289611')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (217, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (217, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (217, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (217, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (217, N'Time', N'1586290801')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (218, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (218, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (218, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (218, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (218, N'Time', N'1586293025')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (219, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (219, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (219, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (219, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (219, N'Time', N'1586293210')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (220, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (220, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (220, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (220, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (220, N'Time', N'1586294477')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (221, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (221, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (221, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (221, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (221, N'Time', N'1586295601')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (222, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (222, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (222, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (222, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (222, N'Time', N'1586296807')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (223, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (223, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (223, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (223, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (223, N'Time', N'1586298012')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (224, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (224, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (224, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (224, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (224, N'Time', N'1586299202')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (225, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (225, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (225, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (225, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (225, N'Time', N'1586300461')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (226, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (226, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (226, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (226, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (226, N'Time', N'1586301600')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (227, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (227, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (227, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (227, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (227, N'Time', N'1586302805')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (228, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (228, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (228, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (228, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (228, N'Time', N'1586304040')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (229, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (229, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (229, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (229, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (229, N'Time', N'1586305205')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (230, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (230, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (230, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (230, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (230, N'Time', N'1586306411')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (231, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (231, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (231, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (231, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (231, N'Time', N'1586307601')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (232, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (232, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (232, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (232, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (232, N'Time', N'1586308807')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (233, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (233, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (233, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (233, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (233, N'Time', N'1586319939')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (234, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (234, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (234, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (234, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (234, N'Time', N'1586332842')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (235, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (235, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (235, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (235, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (235, N'Time', N'1586334545')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (236, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (236, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (236, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (236, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (236, N'Time', N'1586335214')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (237, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (237, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (237, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (237, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (237, N'Time', N'1586336404')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (238, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (238, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (238, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (238, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (238, N'Time', N'1586337609')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (239, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (239, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (239, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (239, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (239, N'Time', N'1586338800')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (240, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (240, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (240, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (240, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (240, N'Time', N'1586340006')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (241, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (241, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (241, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (241, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (241, N'Time', N'1586341212')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (242, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (242, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (242, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (242, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (242, N'Time', N'1586343308')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (243, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (243, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (243, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (243, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (243, N'Time', N'1586343610')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (244, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (244, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (244, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (244, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (244, N'Time', N'1586344801')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (245, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (245, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (245, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (245, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (245, N'Time', N'1586346007')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (246, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (246, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (246, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (246, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (246, N'Time', N'1586347212')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (247, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (247, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (247, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (247, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (247, N'Time', N'1586348402')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (248, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (248, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (248, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (248, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (248, N'Time', N'1586349611')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (249, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (249, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (249, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (249, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (249, N'Time', N'1586350801')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (250, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (250, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (250, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (250, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (250, N'Time', N'1586352002')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (251, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (251, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (251, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (251, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (251, N'Time', N'1586353207')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (252, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (252, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (252, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (252, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (252, N'Time', N'1586354412')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (253, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (253, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (253, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (253, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (253, N'Time', N'1586362468')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (254, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (254, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (254, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (254, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (254, N'Time', N'1586362803')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (255, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (255, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (255, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (255, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (255, N'Time', N'1586364008')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (256, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (256, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (256, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (256, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (256, N'Time', N'1586365214')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (257, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (257, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (257, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (257, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (257, N'Time', N'1586366405')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (258, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (258, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (258, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (258, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (258, N'Time', N'1586367610')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (259, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (259, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (259, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (259, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (259, N'Time', N'1586368801')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (260, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (260, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (260, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (260, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (260, N'Time', N'1586370006')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (261, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (261, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (261, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (261, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (261, N'Time', N'1586371211')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (262, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (262, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (262, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (262, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (262, N'Time', N'1586372402')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (263, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (263, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (263, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (263, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (263, N'Time', N'1586373607')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (264, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (264, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (264, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (264, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (264, N'Time', N'1586399520')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (265, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (265, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (265, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (265, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (265, N'Time', N'1586400006')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (266, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (266, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (266, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (266, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (266, N'Time', N'1586401428')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (267, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (267, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (267, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (267, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (267, N'Time', N'1586403949')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (268, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (268, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (268, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (268, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (268, N'Time', N'1586404811')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (269, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (269, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (269, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (269, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (269, N'Time', N'1586406014')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (270, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (270, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (270, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (270, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (270, N'Time', N'1586407207')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (271, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (271, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (271, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (271, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (271, N'Time', N'1586408685')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (272, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (272, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (272, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (272, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (272, N'Time', N'1586409726')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (273, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (273, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (273, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (273, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (273, N'Time', N'1586411477')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (274, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (274, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (274, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (274, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (274, N'Time', N'1586412006')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (275, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (275, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (275, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (275, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (275, N'Time', N'1586413202')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (276, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (276, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (276, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (276, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (276, N'Time', N'1586414405')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (277, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (277, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (277, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (277, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (277, N'Time', N'1586420806')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (278, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (278, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (278, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (278, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (278, N'Time', N'1586421607')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (279, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (279, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (279, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (279, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (279, N'Time', N'1586422812')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (280, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (280, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (280, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (280, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (280, N'Time', N'1586429127')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (281, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (281, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (281, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (281, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (281, N'Time', N'1586430013')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (282, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (282, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (282, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (282, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (282, N'Time', N'1586440870')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (283, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (283, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (283, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (283, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (283, N'Time', N'1586449797')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (284, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (284, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (284, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (284, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (284, N'Time', N'1586450400')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (285, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (285, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (285, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (285, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (285, N'Time', N'1586451600')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (286, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (286, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (286, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (286, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (286, N'Time', N'1586452800')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (287, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (287, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (287, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (287, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (287, N'Time', N'1586454001')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (288, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (288, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (288, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (288, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (288, N'Time', N'1586455201')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (289, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (289, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (289, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (289, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (289, N'Time', N'1586456450')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (290, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (290, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (290, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (290, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (290, N'Time', N'1586457609')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (291, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (291, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (291, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (291, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (291, N'Time', N'1586463253')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (292, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (292, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (292, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (292, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (292, N'Time', N'1586465482')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (293, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (293, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (293, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (293, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (293, N'Time', N'1586466071')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (294, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (294, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (294, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (294, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (294, N'Time', N'1586467388')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (295, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (295, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (295, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (295, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (295, N'Time', N'1586468410')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (296, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (296, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (296, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (296, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (296, N'Time', N'1586469600')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (297, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (297, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (297, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (297, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (297, N'Time', N'1586470803')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (298, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (298, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (298, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (298, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (298, N'Time', N'1586472003')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (299, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (299, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (299, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (299, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (299, N'Time', N'1586473200')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (300, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (300, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (300, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (300, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (300, N'Time', N'1586474836')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (301, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (301, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (301, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (301, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (301, N'Time', N'1586475613')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (302, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (302, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (302, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (302, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (302, N'Time', N'1586476810')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (303, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (303, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (303, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (303, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (303, N'Time', N'1586478000')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (304, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (304, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (304, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (304, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (304, N'Time', N'1586479205')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (305, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (305, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (305, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (305, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (305, N'Time', N'1586480410')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (306, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (306, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (306, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (306, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (306, N'Time', N'1586481706')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (307, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (307, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (307, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (307, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (307, N'Time', N'1586482803')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (308, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (308, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (308, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (308, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (308, N'Time', N'1586484006')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (309, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (309, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (309, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (309, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (309, N'Time', N'1586485212')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (310, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (310, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (310, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (310, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (310, N'Time', N'1586486402')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (311, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (311, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (311, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (311, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (311, N'Time', N'1586487607')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (312, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (312, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (312, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (312, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (312, N'Time', N'1586488813')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (313, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (313, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (313, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (313, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (313, N'Time', N'1586490003')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (314, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (314, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (314, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (314, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (314, N'Time', N'1586491209')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (315, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (315, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (315, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (315, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (315, N'Time', N'1586492414')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (316, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (316, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (316, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (316, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (316, N'Time', N'1586493604')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (317, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (317, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (317, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (317, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (317, N'Time', N'1586494810')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (318, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (318, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (318, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (318, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (318, N'Time', N'1586496000')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (319, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (319, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (319, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (319, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (319, N'Time', N'1586497206')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (320, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (320, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (320, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (320, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (320, N'Time', N'1586498411')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (321, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (321, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (321, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (321, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (321, N'Time', N'1586499602')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (322, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (322, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (322, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (322, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (322, N'Time', N'1586511484')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (323, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (323, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (323, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (323, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (323, N'Time', N'1586529501')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (324, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (324, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (324, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (324, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (324, N'Time', N'1586529609')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (325, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (325, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (325, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (325, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (325, N'Time', N'1586530800')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (326, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (326, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (326, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (326, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (326, N'Time', N'1586532005')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (327, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (327, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (327, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (327, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (327, N'Time', N'1586533212')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (328, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (328, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (328, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (328, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (328, N'Time', N'1586534403')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (329, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (329, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (329, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (329, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (329, N'Time', N'1586535609')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (330, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (330, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (330, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (330, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (330, N'Time', N'1586536800')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (331, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (331, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (331, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (331, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (331, N'Time', N'1586538006')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (332, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (332, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (332, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (332, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (332, N'Time', N'1586539211')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (333, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (333, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (333, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (333, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (333, N'Time', N'1586540402')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (334, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (334, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (334, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (334, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (334, N'Time', N'1586541607')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (335, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (335, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (335, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (335, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (335, N'Time', N'1586542812')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (336, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (336, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (336, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (336, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (336, N'Time', N'1586544003')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (337, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (337, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (337, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (337, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (337, N'Time', N'1586545208')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (338, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (338, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (338, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (338, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (338, N'Time', N'1586546414')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (339, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (339, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (339, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (339, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (339, N'Time', N'1586547604')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (340, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (340, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (340, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (340, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (340, N'Time', N'1586548809')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (341, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (341, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (341, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (341, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (341, N'Time', N'1586550000')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (342, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (342, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (342, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (342, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (342, N'Time', N'1586551206')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (343, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (343, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (343, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (343, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (343, N'Time', N'1586552412')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (344, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (344, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (344, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (344, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (344, N'Time', N'1586553606')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (345, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (345, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (345, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (345, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (345, N'Time', N'1586554807')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (346, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (346, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (346, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (346, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (346, N'Time', N'1586556008')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (347, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (347, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (347, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (347, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (347, N'Time', N'1586557200')
GO
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (348, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (348, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (348, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (348, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (348, N'Time', N'1586558401')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (349, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (349, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (349, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (349, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (349, N'Time', N'1586603893')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (350, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (350, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (350, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (350, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (350, N'Time', N'1586604013')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (351, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (351, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (351, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (351, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (351, N'Time', N'1586605213')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (352, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (352, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (352, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (352, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (352, N'Time', N'1586611075')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (353, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (353, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (353, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (353, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (353, N'Time', N'1586611211')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (354, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (354, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (354, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (354, N'RetryCount', N'10')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (354, N'Time', N'1586612411')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (699, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (699, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (699, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (699, N'Time', N'1592387112')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (700, N'CurrentCulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (700, N'CurrentUICulture', N'"en-US"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (700, N'RecurringJobId', N'"HangFireJobService.Hangfire"')
INSERT [HangFire].[JobParameter] ([JobId], [Name], [Value]) VALUES (700, N'Time', N'1592388000')
INSERT [HangFire].[Schema] ([Version]) VALUES (7)
INSERT [HangFire].[Server] ([Id], [Data], [LastHeartbeat]) VALUES (N'desktop-a21k4fj:20028:68411393-3e14-479a-9e9d-e38795cae8f9', N'{"WorkerCount":20,"Queues":["default"],"StartedAt":"2020-06-17T09:44:53.6872726Z"}', CAST(N'2020-06-17T10:14:24.737' AS DateTime))
INSERT [HangFire].[Set] ([Key], [Score], [Value], [ExpireAt]) VALUES (N'recurring-jobs', 1592389200, N'HangFireJobService.Hangfire', NULL)
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ArchivedOrderFile_FileName]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ArchivedOrderFile_FileName] ON [dbo].[ArchivedOrderFile]
(
	[FileName] ASC
)
WHERE ([FileName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Areas_Fk_Governorate_Id]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Areas_Fk_Governorate_Id] ON [dbo].[Areas]
(
	[Fk_Governorate_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Dispatchers_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Dispatchers_CostCenterId] ON [dbo].[Dispatchers]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Dispatchers_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Dispatchers_DivisionId] ON [dbo].[Dispatchers]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Dispatchers_SupervisorId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Dispatchers_SupervisorId] ON [dbo].[Dispatchers]
(
	[SupervisorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Dispatchers_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Dispatchers_UserId] ON [dbo].[Dispatchers]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DispatcherSettings_AreaId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_AreaId] ON [dbo].[DispatcherSettings]
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DispatcherSettings_DispatcherId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_DispatcherId] ON [dbo].[DispatcherSettings]
(
	[DispatcherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DispatcherSettings_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_DivisionId] ON [dbo].[DispatcherSettings]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DispatcherSettings_OrderProblemId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_DispatcherSettings_OrderProblemId] ON [dbo].[DispatcherSettings]
(
	[OrderProblemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Engineers_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Engineers_CostCenterId] ON [dbo].[Engineers]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Engineers_DispatcherId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Engineers_DispatcherId] ON [dbo].[Engineers]
(
	[DispatcherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Engineers_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Engineers_DivisionId] ON [dbo].[Engineers]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Engineers_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Engineers_UserId] ON [dbo].[Engineers]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Foremans_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_CostCenterId] ON [dbo].[Foremans]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Foremans_DispatcherId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_DispatcherId] ON [dbo].[Foremans]
(
	[DispatcherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Foremans_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_DivisionId] ON [dbo].[Foremans]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Foremans_EngineerId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Foremans_EngineerId] ON [dbo].[Foremans]
(
	[EngineerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Foremans_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Foremans_UserId] ON [dbo].[Foremans]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Areas_AreasId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Areas_AreasId] ON [dbo].[Lang_Areas]
(
	[AreasId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Areas_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Areas_SupportedLanguagesId] ON [dbo].[Lang_Areas]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_AttendanceStates_AttendanceStatesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_AttendanceStates_AttendanceStatesId] ON [dbo].[Lang_AttendanceStates]
(
	[AttendanceStatesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_AttendanceStates_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_AttendanceStates_SupportedLanguagesId] ON [dbo].[Lang_AttendanceStates]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Availability_AvailabilityId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Availability_AvailabilityId] ON [dbo].[Lang_Availability]
(
	[AvailabilityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Availability_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Availability_SupportedLanguagesId] ON [dbo].[Lang_Availability]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_BuildingTypes_BuildingTypesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_BuildingTypes_BuildingTypesId] ON [dbo].[Lang_BuildingTypes]
(
	[BuildingTypesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_BuildingTypes_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_BuildingTypes_SupportedLanguagesId] ON [dbo].[Lang_BuildingTypes]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_CompanyCode_CompanyCodeId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_CompanyCode_CompanyCodeId] ON [dbo].[Lang_CompanyCode]
(
	[CompanyCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_CompanyCode_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_CompanyCode_SupportedLanguagesId] ON [dbo].[Lang_CompanyCode]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_ContractTypes_ContractTypesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_ContractTypes_ContractTypesId] ON [dbo].[Lang_ContractTypes]
(
	[ContractTypesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_ContractTypes_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_ContractTypes_SupportedLanguagesId] ON [dbo].[Lang_ContractTypes]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_CostCenter_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_CostCenter_CostCenterId] ON [dbo].[Lang_CostCenter]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_CostCenter_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_CostCenter_SupportedLanguagesId] ON [dbo].[Lang_CostCenter]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Division_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Division_DivisionId] ON [dbo].[Lang_Division]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Division_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Division_SupportedLanguagesId] ON [dbo].[Lang_Division]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Governorates_GovernoratesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Governorates_GovernoratesId] ON [dbo].[Lang_Governorates]
(
	[GovernoratesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Lang_Governorates_SupportedLanguagesId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Lang_Governorates_SupportedLanguagesId] ON [dbo].[Lang_Governorates]
(
	[SupportedLanguagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Managers_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Managers_CostCenterId] ON [dbo].[Managers]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Managers_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Managers_UserId] ON [dbo].[Managers]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Members_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Members_DivisionId] ON [dbo].[Members]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Members_TeamId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Members_TeamId] ON [dbo].[Members]
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Members_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Members_UserId] ON [dbo].[Members]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_CostCenterId] ON [dbo].[OrderActions]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_DispatcherId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_DispatcherId] ON [dbo].[OrderActions]
(
	[DispatcherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_OrderActions_FK_CreatedBy_Id]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_FK_CreatedBy_Id] ON [dbo].[OrderActions]
(
	[FK_CreatedBy_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_ForemanId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_ForemanId] ON [dbo].[OrderActions]
(
	[ForemanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_OrderId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_OrderId] ON [dbo].[OrderActions]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_StatusId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_StatusId] ON [dbo].[OrderActions]
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_SupervisorId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_SupervisorId] ON [dbo].[OrderActions]
(
	[SupervisorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_TeamId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_TeamId] ON [dbo].[OrderActions]
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderActions_TechnicianId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_OrderActions_TechnicianId] ON [dbo].[OrderActions]
(
	[TechnicianId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_BuildingTypeId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_BuildingTypeId] ON [dbo].[Orders]
(
	[BuildingTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_ContractTypeId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_ContractTypeId] ON [dbo].[Orders]
(
	[ContractTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_DispatcherId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_DispatcherId] ON [dbo].[Orders]
(
	[DispatcherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_DivisionId] ON [dbo].[Orders]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_ProblemId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_ProblemId] ON [dbo].[Orders]
(
	[ProblemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_StatusId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_StatusId] ON [dbo].[Orders]
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_SupervisorId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_SupervisorId] ON [dbo].[Orders]
(
	[SupervisorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_TeamId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Orders_TeamId] ON [dbo].[Orders]
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_PACI_PACINumber]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_PACI_PACINumber] ON [dbo].[PACI]
(
	[PACINumber] ASC
)
WHERE ([PACINumber] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Supervisors_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Supervisors_CostCenterId] ON [dbo].[Supervisors]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Supervisors_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Supervisors_DivisionId] ON [dbo].[Supervisors]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Supervisors_ManagerId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Supervisors_ManagerId] ON [dbo].[Supervisors]
(
	[ManagerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Supervisors_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Supervisors_UserId] ON [dbo].[Supervisors]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_DispatcherId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_DispatcherId] ON [dbo].[Teams]
(
	[DispatcherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_DivisionId] ON [dbo].[Teams]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_EngineerId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_EngineerId] ON [dbo].[Teams]
(
	[EngineerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_ForemanId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_ForemanId] ON [dbo].[Teams]
(
	[ForemanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_ShiftId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_ShiftId] ON [dbo].[Teams]
(
	[ShiftId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_StatusId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_StatusId] ON [dbo].[Teams]
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Teams_SupervisorId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Teams_SupervisorId] ON [dbo].[Teams]
(
	[SupervisorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Technicians_CostCenterId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Technicians_CostCenterId] ON [dbo].[Technicians]
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Technicians_DivisionId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Technicians_DivisionId] ON [dbo].[Technicians]
(
	[DivisionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Technicians_TeamId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_Technicians_TeamId] ON [dbo].[Technicians]
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Technicians_UserId]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Technicians_UserId] ON [dbo].[Technicians]
(
	[UserId] ASC
)
WHERE ([UserId] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_AggregatedCounter_ExpireAt]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_AggregatedCounter_ExpireAt] ON [HangFire].[AggregatedCounter]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Hash_ExpireAt]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Job_ExpireAt]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[StateName]) 
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Job_StateName]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job]
(
	[StateName] ASC
)
WHERE ([StateName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_List_ExpireAt]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Server_LastHeartbeat]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Server_LastHeartbeat] ON [HangFire].[Server]
(
	[LastHeartbeat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Set_ExpireAt]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_ExpireAt] ON [HangFire].[Set]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Set_Score]    Script Date: 10/25/2020 12:43:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_Score] ON [HangFire].[Set]
(
	[Key] ASC,
	[Score] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ArchivedOrderFile] ADD  DEFAULT ((0)) FOR [Trials]
GO
ALTER TABLE [dbo].[Lang_Shift] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [FromTimeValue]
GO
ALTER TABLE [dbo].[Lang_Shift] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [ToTimeValue]
GO
ALTER TABLE [dbo].[OrderActions] ADD  DEFAULT ((0)) FOR [PlatformTypeSource]
GO
ALTER TABLE [dbo].[Areas]  WITH CHECK ADD  CONSTRAINT [FK_Areas_Governorates_Fk_Governorate_Id] FOREIGN KEY([Fk_Governorate_Id])
REFERENCES [dbo].[Governorates] ([Id])
GO
ALTER TABLE [dbo].[Areas] CHECK CONSTRAINT [FK_Areas_Governorates_Fk_Governorate_Id]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_Division_DivisionId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_Areas_AreaId] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_Areas_AreaId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_Division_DivisionId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_OrderProblem_OrderProblemId] FOREIGN KEY([OrderProblemId])
REFERENCES [dbo].[OrderProblem] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_OrderProblem_OrderProblemId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_Division_DivisionId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_Division_DivisionId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_Engineers_EngineerId] FOREIGN KEY([EngineerId])
REFERENCES [dbo].[Engineers] ([Id])
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_Engineers_EngineerId]
GO
ALTER TABLE [dbo].[Lang_Areas]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Areas_Areas_AreasId] FOREIGN KEY([AreasId])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[Lang_Areas] CHECK CONSTRAINT [FK_Lang_Areas_Areas_AreasId]
GO
ALTER TABLE [dbo].[Lang_Areas]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Areas_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Areas] CHECK CONSTRAINT [FK_Lang_Areas_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_AttendanceStates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_AttendanceStates_AttendanceStates_AttendanceStatesId] FOREIGN KEY([AttendanceStatesId])
REFERENCES [dbo].[AttendanceStates] ([Id])
GO
ALTER TABLE [dbo].[Lang_AttendanceStates] CHECK CONSTRAINT [FK_Lang_AttendanceStates_AttendanceStates_AttendanceStatesId]
GO
ALTER TABLE [dbo].[Lang_AttendanceStates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_AttendanceStates_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_AttendanceStates] CHECK CONSTRAINT [FK_Lang_AttendanceStates_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_Availability]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Availability_Availability_AvailabilityId] FOREIGN KEY([AvailabilityId])
REFERENCES [dbo].[Availability] ([Id])
GO
ALTER TABLE [dbo].[Lang_Availability] CHECK CONSTRAINT [FK_Lang_Availability_Availability_AvailabilityId]
GO
ALTER TABLE [dbo].[Lang_Availability]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Availability_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Availability] CHECK CONSTRAINT [FK_Lang_Availability_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_BuildingTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_BuildingTypes_BuildingTypes_BuildingTypesId] FOREIGN KEY([BuildingTypesId])
REFERENCES [dbo].[BuildingTypes] ([Id])
GO
ALTER TABLE [dbo].[Lang_BuildingTypes] CHECK CONSTRAINT [FK_Lang_BuildingTypes_BuildingTypes_BuildingTypesId]
GO
ALTER TABLE [dbo].[Lang_BuildingTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_BuildingTypes_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_BuildingTypes] CHECK CONSTRAINT [FK_Lang_BuildingTypes_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_CompanyCode]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CompanyCode_CompanyCode_CompanyCodeId] FOREIGN KEY([CompanyCodeId])
REFERENCES [dbo].[CompanyCode] ([Id])
GO
ALTER TABLE [dbo].[Lang_CompanyCode] CHECK CONSTRAINT [FK_Lang_CompanyCode_CompanyCode_CompanyCodeId]
GO
ALTER TABLE [dbo].[Lang_CompanyCode]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CompanyCode_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_CompanyCode] CHECK CONSTRAINT [FK_Lang_CompanyCode_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_ContractTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_ContractTypes_ContractTypes_ContractTypesId] FOREIGN KEY([ContractTypesId])
REFERENCES [dbo].[ContractTypes] ([Id])
GO
ALTER TABLE [dbo].[Lang_ContractTypes] CHECK CONSTRAINT [FK_Lang_ContractTypes_ContractTypes_ContractTypesId]
GO
ALTER TABLE [dbo].[Lang_ContractTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_ContractTypes_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_ContractTypes] CHECK CONSTRAINT [FK_Lang_ContractTypes_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_CostCenter]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CostCenter_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
GO
ALTER TABLE [dbo].[Lang_CostCenter] CHECK CONSTRAINT [FK_Lang_CostCenter_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Lang_CostCenter]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CostCenter_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_CostCenter] CHECK CONSTRAINT [FK_Lang_CostCenter_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_Division]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Division_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Lang_Division] CHECK CONSTRAINT [FK_Lang_Division_Division_DivisionId]
GO
ALTER TABLE [dbo].[Lang_Division]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Division_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Division] CHECK CONSTRAINT [FK_Lang_Division_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_Governorates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Governorates_Governorates_GovernoratesId] FOREIGN KEY([GovernoratesId])
REFERENCES [dbo].[Governorates] ([Id])
GO
ALTER TABLE [dbo].[Lang_Governorates] CHECK CONSTRAINT [FK_Lang_Governorates_Governorates_GovernoratesId]
GO
ALTER TABLE [dbo].[Lang_Governorates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Governorates_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Governorates] CHECK CONSTRAINT [FK_Lang_Governorates_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Managers]  WITH CHECK ADD  CONSTRAINT [FK_Managers_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Managers] CHECK CONSTRAINT [FK_Managers_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Managers]  WITH CHECK ADD  CONSTRAINT [FK_Managers_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Managers] CHECK CONSTRAINT [FK_Managers_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Members]  WITH CHECK ADD  CONSTRAINT [FK_Members_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Members] CHECK CONSTRAINT [FK_Members_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Members]  WITH CHECK ADD  CONSTRAINT [FK_Members_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Members] CHECK CONSTRAINT [FK_Members_Division_DivisionId]
GO
ALTER TABLE [dbo].[Members]  WITH CHECK ADD  CONSTRAINT [FK_Members_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Members] CHECK CONSTRAINT [FK_Members_Teams_TeamId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_AspNetUsers_FK_CreatedBy_Id] FOREIGN KEY([FK_CreatedBy_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_AspNetUsers_FK_CreatedBy_Id]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Foremans_ForemanId] FOREIGN KEY([ForemanId])
REFERENCES [dbo].[Foremans] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Foremans_ForemanId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Orders_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Orders_OrderId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_OrderStatus_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_OrderStatus_StatusId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Teams_TeamId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Technicians_TechnicianId] FOREIGN KEY([TechnicianId])
REFERENCES [dbo].[Technicians] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Technicians_TechnicianId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_BuildingTypes_BuildingTypeId] FOREIGN KEY([BuildingTypeId])
REFERENCES [dbo].[BuildingTypes] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_BuildingTypes_BuildingTypeId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_ContractTypes_ContractTypeId] FOREIGN KEY([ContractTypeId])
REFERENCES [dbo].[ContractTypes] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_ContractTypes_ContractTypeId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Division_DivisionId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderProblem_ProblemId] FOREIGN KEY([ProblemId])
REFERENCES [dbo].[OrderProblem] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderProblem_ProblemId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderStatus_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderStatus_StatusId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Teams_TeamId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_Division_DivisionId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_Managers_ManagerId] FOREIGN KEY([ManagerId])
REFERENCES [dbo].[Managers] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_Managers_ManagerId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Availability_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Availability] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Availability_StatusId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Division_DivisionId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Engineers_EngineerId] FOREIGN KEY([EngineerId])
REFERENCES [dbo].[Engineers] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Engineers_EngineerId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Foremans_ForemanId] FOREIGN KEY([ForemanId])
REFERENCES [dbo].[Foremans] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Foremans_ForemanId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Shift_ShiftId] FOREIGN KEY([ShiftId])
REFERENCES [dbo].[Shift] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Shift_ShiftId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_Division_DivisionId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_Teams_TeamId]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
USE [master]
GO
ALTER DATABASE [custend-sandbox-db-2] SET  READ_WRITE 
GO
