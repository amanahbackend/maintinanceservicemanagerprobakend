
/****** Object:  Table [dbo].[WorkingType]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[WorkingType]
GO
/****** Object:  Table [dbo].[Vehicles]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Vehicles]
GO
/****** Object:  Table [dbo].[UserDevice]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[UserDevice]
GO
/****** Object:  Table [dbo].[TechniciansStates]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[TechniciansStates]
GO
/****** Object:  Table [dbo].[Technicians]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Technicians]
GO
/****** Object:  Table [dbo].[Teams]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Teams]
GO
/****** Object:  Table [dbo].[SupportedLanguages]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[SupportedLanguages]
GO
/****** Object:  Table [dbo].[Supervisors]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Supervisors]
GO
/****** Object:  Table [dbo].[Shift]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Shift]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Settings]
GO
/****** Object:  Table [dbo].[RolePrivilege]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[RolePrivilege]
GO
/****** Object:  Table [dbo].[RejectionReason]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[RejectionReason]
GO
/****** Object:  Table [dbo].[Privilege]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Privilege]
GO
/****** Object:  Table [dbo].[PasswordTokenPin]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[PasswordTokenPin]
GO
/****** Object:  Table [dbo].[PACI]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[PACI]
GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[OrderType]
GO
/****** Object:  Table [dbo].[OrderSubStatus]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[OrderSubStatus]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[OrderStatus]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Orders]
GO
/****** Object:  Table [dbo].[OrderRowData]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[OrderRowData]
GO
/****** Object:  Table [dbo].[OrderProblem]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[OrderProblem]
GO
/****** Object:  Table [dbo].[OrderPriority]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[OrderPriority]
GO
/****** Object:  Table [dbo].[OrderActions]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[OrderActions]
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Notifications]
GO
/****** Object:  Table [dbo].[NotificationCenter]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[NotificationCenter]
GO
/****** Object:  Table [dbo].[Members]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Members]
GO
/****** Object:  Table [dbo].[MaterialControllers]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[MaterialControllers]
GO
/****** Object:  Table [dbo].[MapAdminRelated]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[MapAdminRelated]
GO
/****** Object:  Table [dbo].[Managers]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Managers]
GO
/****** Object:  Table [dbo].[Lang_WorkingType]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Lang_WorkingType]
GO
/****** Object:  Table [dbo].[Lang_Shift]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Lang_Shift]
GO
/****** Object:  Table [dbo].[Lang_RejectionReason]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Lang_RejectionReason]
GO
/****** Object:  Table [dbo].[Lang_OrderType]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Lang_OrderType]
GO
/****** Object:  Table [dbo].[Lang_OrderSubStatus]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Lang_OrderSubStatus]
GO
/****** Object:  Table [dbo].[Lang_OrderStatus]    Script Date: 2020-05-17 1:54:45 PM ******/
DROP TABLE [dbo].[Lang_OrderStatus]
GO
/****** Object:  Table [dbo].[Lang_OrderProblem]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_OrderProblem]
GO
/****** Object:  Table [dbo].[Lang_OrderPriority]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_OrderPriority]
GO
/****** Object:  Table [dbo].[Lang_Notification]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_Notification]
GO
/****** Object:  Table [dbo].[Lang_Governorates]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_Governorates]
GO
/****** Object:  Table [dbo].[Lang_Division]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_Division]
GO
/****** Object:  Table [dbo].[Lang_CostCenter]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_CostCenter]
GO
/****** Object:  Table [dbo].[Lang_ContractTypes]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_ContractTypes]
GO
/****** Object:  Table [dbo].[Lang_CompanyCode]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_CompanyCode]
GO
/****** Object:  Table [dbo].[Lang_BuildingTypes]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_BuildingTypes]
GO
/****** Object:  Table [dbo].[Lang_Availability]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_Availability]
GO
/****** Object:  Table [dbo].[Lang_AttendanceStates]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_AttendanceStates]
GO
/****** Object:  Table [dbo].[Lang_Areas]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Lang_Areas]
GO
/****** Object:  Table [dbo].[JunkUser]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[JunkUser]
GO
/****** Object:  Table [dbo].[Governorates]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Governorates]
GO
/****** Object:  Table [dbo].[Foremans]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Foremans]
GO
/****** Object:  Table [dbo].[Engineers]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Engineers]
GO
/****** Object:  Table [dbo].[Drivers]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Drivers]
GO
/****** Object:  Table [dbo].[Division]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Division]
GO
/****** Object:  Table [dbo].[DispatcherSettings]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[DispatcherSettings]
GO
/****** Object:  Table [dbo].[Dispatchers]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Dispatchers]
GO
/****** Object:  Table [dbo].[CostCenter]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[CostCenter]
GO
/****** Object:  Table [dbo].[ContractTypes]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[ContractTypes]
GO
/****** Object:  Table [dbo].[CompanyCode]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[CompanyCode]
GO
/****** Object:  Table [dbo].[BuildingTypes]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[BuildingTypes]
GO
/****** Object:  Table [dbo].[Availability]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Availability]
GO
/****** Object:  Table [dbo].[Attendences]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Attendences]
GO
/****** Object:  Table [dbo].[AttendanceStates]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[AttendanceStates]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[AspNetUserTokens]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[AspNetUserRoles]
GO

DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[AspNetUserClaims]
GO

/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[AspNetRoleClaims]
GO
/****** Object:  Table [dbo].[Areas]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[Areas]
GO
/****** Object:  Table [dbo].[ArchivedOrders]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[ArchivedOrders]
GO
/****** Object:  Table [dbo].[ArchivedOrderFile]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[ArchivedOrderFile]
GO
/****** Object:  Table [dbo].[ApplicationUserHistory]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[ApplicationUserHistory]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 2020-05-17 1:54:46 PM ******/
DROP TABLE [dbo].[__EFMigrationsHistory]
GO

