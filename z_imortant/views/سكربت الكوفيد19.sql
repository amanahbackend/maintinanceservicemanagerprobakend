USE [CodeFirst-db]
GO

/****** Object:  View [dbo].[View_Excel_MR]    Script Date: 6/23/2020 8:18:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_Excel_MR]
AS
SELECT        o.Code AS Order_ID, o.SAP_CreatedDate AS Created_Date, ISNULL(oStatus.Name, '') AS Current_Order_Status, ISNULL(o.TypeName, '') AS Order_Type, ISNULL(o.DispatcherNote, '') AS Dispatcher_Notes, ISNULL(o.ICAgentNote, 
                         '') AS IC_Agent_Notes, ISNULL(o.OrderDescription, '') AS Order_Description, ISNULL(oProblem.Name, '') AS Problem_Type, ISNULL(oDivision.Name, '') AS Division, ISNULL(oDispatcherUser.FirstName, '') 
                         + ' ' + ISNULL(oDispatcherUser.LastName, '') AS Current_Dispatcher, ISNULL(oSupervisorUser.FirstName, '') + ' ' + ISNULL(oSupervisorUser.LastName, '') AS Current_Supervisor_Name, ISNULL(oTeamForemanUser.FirstName, 
                         '') + ' ' + ISNULL(oTeamForemanUser.LastName, '') AS Current_Foreman_Name, ISNULL(oCompanyCode.Code, '') AS Company_Code, ISNULL(o.CustomerCode, '') AS Customer_ID, ISNULL(o.PhoneOne, '') AS Customer_Tel_1, 
                         ISNULL(o.PhoneTwo, '') AS Customer_Tel_2, ISNULL(o.Caller_ID, '') AS Customer_Caller_Phone, ISNULL(o.SAP_PACI, '') AS PACI_No, ISNULL(o.GovName, N'') AS PACI_Governorate, ISNULL(o.SAP_AreaName, '') AS PACI_Area, 
                         ISNULL(o.SAP_StreetName, '') AS PACI_Street, ISNULL(o.SAP_BlockName, '') AS PACI_Block, ISNULL(o.SAP_AppartmentNo, '') AS SAP_Apartment, ISNULL(o.SAP_HouseKasima, '') AS SAP_Building, ISNULL(o.SAP_Floor, '') 
                         AS SAP_Floor, ISNULL(o.ContractCode, '') AS Contract_Number, ISNULL(oContractType.Name, '') AS Contract_Type, o.ContractStartDate AS Contract_Creation_Date, o.ContractExpiryDate AS Contract_Expiration_Date, 
                         o.UpdatedDate AS Closed_Date, ac.ActionDate AS Progress_Date_History, ISNULL(acExcuterUser.FirstName, '') + ' ' + ISNULL(acExcuterUser.LastName, '') AS Executor_History, ISNULL(ac.ActionTypeName, '') 
                         AS Action_Type_Name_History, ISNULL(acStatus.Name, '') AS Progress_Status_History, ISNULL(ac.SubStatusName, '') AS Progress_Sub_Status_History, ISNULL(acTechnicianUser.FirstName, '') 
                         + ' ' + ISNULL(acTechnicianUser.LastName, '') AS Technician_Name_History, ISNULL(acDispatcherUser.FirstName, '') + ' ' + ISNULL(acDispatcherUser.LastName, '') AS Dispatcher_Name_History, ISNULL(ac.ServeyReport, '') 
                         AS Survey_Report_History, o.FunctionalLocation AS Functional_Location, o.AreaId, o.DispatcherId, o.DivisionId, o.TypeId, o.ProblemId, o.StatusId, o.CompanyCodeId, o.Id, ac.WorkingTypeId, 
                         acTechnician.IsDriver AS acTechnicianIsDriver, acTechnician.Id AS acTechnicianId, ac.PlatformTypeSource, ac.StatusId AS acStatusId, ac.Id AS acId
FROM            dbo.Orders AS o LEFT OUTER JOIN
                         dbo.OrderActions AS ac ON o.Id = ac.OrderId LEFT OUTER JOIN
                         dbo.OrderStatus AS oStatus ON oStatus.Id = o.StatusId LEFT OUTER JOIN
                         dbo.Dispatchers AS oDispatcher ON o.DispatcherId = oDispatcher.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oDispatcherUser ON oDispatcher.UserId = oDispatcherUser.Id LEFT OUTER JOIN
                         dbo.Division AS oDivision ON o.DivisionId = oDivision.Id LEFT OUTER JOIN
                         dbo.OrderProblem AS oProblem ON o.ProblemId = oProblem.Id LEFT OUTER JOIN
                         dbo.Supervisors AS oSupervisor ON o.SupervisorId = oSupervisor.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oSupervisorUser ON oSupervisor.UserId = oSupervisorUser.Id LEFT OUTER JOIN
                         dbo.Teams AS oTeam ON o.TeamId = oTeam.Id LEFT OUTER JOIN
                         dbo.Foremans AS oTeamForeman ON oTeam.ForemanId = oTeamForeman.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oTeamForemanUser ON oTeamForeman.UserId = oTeamForemanUser.Id LEFT OUTER JOIN
                         dbo.ContractTypes AS oContractType ON o.ContractTypeId = oContractType.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS acExcuterUser ON ac.FK_CreatedBy_Id = acExcuterUser.Id LEFT OUTER JOIN
                         dbo.Dispatchers AS acDispatcher ON ac.DispatcherId = acDispatcher.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS acDispatcherUser ON acDispatcher.UserId = acDispatcherUser.Id LEFT OUTER JOIN
                         dbo.OrderStatus AS acStatus ON ac.StatusId = acStatus.Id LEFT OUTER JOIN
                         dbo.Technicians AS acTechnician ON ac.TechnicianId = acTechnician.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS acTechnicianUser ON acTechnician.UserId = acTechnicianUser.Id LEFT OUTER JOIN
                         dbo.CostCenter AS acCostCenter ON ac.CostCenterId = acCostCenter.Id LEFT OUTER JOIN
                         dbo.CompanyCode AS oCompanyCode ON oCompanyCode.Id = o.CompanyCodeId
 