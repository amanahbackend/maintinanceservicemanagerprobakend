USE [CodeFirst-db]
GO

/****** Object:  View [dbo].[View_OrderPrintOut]    Script Date: 7/12/2020 11:11:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_OrderPrintOut]
AS
SELECT        o.Id, o.Code, o.TypeId, o.TypeName, o.StatusId, oStatus.Name AS StatusName, o.SubStatusId, o.ProblemId, o.SubStatusName, OProblem.Name AS problemName, o.PriorityId, o.PriorityName, o.CompanyCodeId, 
                         o.CompanyCodeName, o.OrderTypeCode, o.DivisionId, oDivision.Name AS DivisionName, o.ICAgentNote, o.DispatcherNote, o.CancellationReason, o.GeneralNote, o.SAP_CreatedDate, o.OrderDescription, o.ServeyReport, 
                         o.CustomerCode, o.CustomerName, o.PhoneOne, o.PhoneTwo, o.Caller_ID, o.SAP_PACI, o.SAP_HouseKasima, o.SAP_Floor, o.SAP_AppartmentNo, o.SAP_StreetName, o.SAP_BlockName, o.SAP_AreaName, o.SAP_GovName, 
                         o.PACI, o.FunctionalLocation, o.HouseKasima, o.Floor, o.AppartmentNo, o.StreetId, o.StreetName, o.BlockId, o.BlockName, o.AreaId, oArea.Name AS AreaName, o.GovId, o.GovName, o.AddressNote, o.Long, o.Lat, 
                         o.BuildingTypeId, dbo.BuildingTypes.Name AS buildingTypeName, o.ContractCode, o.ContractTypeId, oContractTypes.Name AS contractTypeName, o.ContractStartDate, o.ContractExpiryDate, o.InsertionDate, o.FileName, 
                         o.SupervisorId, ISNULL(oSupervisorUser.FirstName, N'') + ' ' + ISNULL(oSupervisorUser.LastName, N'') AS supervisorName, o.DispatcherId, ISNULL(oDispatcherUser.FirstName, N'') + ' ' + ISNULL(oDispatcherUser.LastName, 
                         N'') AS dispatcherName, o.RankInDispatcher, o.TeamId, o.PrevTeamId, o.RankInTeam, o.AcceptanceFlag, o.RejectionReasonId, o.RejectionReason, o.IsAccomplish, o.OnHoldCount, o.IsRepeatedCall, o.IsExceedTime, 
                         o.CurrentUserId, o.FK_CreatedBy_Id, o.FK_UpdatedBy_Id, o.FK_DeletedBy_Id, o.CreatedDate, o.UpdatedDate, o.DeletedDate, o.IsDeleted, ac.FK_CreatedBy_Id AS acFK_CreatedBy_Id, 
                         ac.PlatformTypeSource AS acPlatformTypeSource, ac.Id AS acId
FROM            dbo.Orders AS o LEFT OUTER JOIN
                         dbo.OrderActions AS ac ON o.Id = ac.OrderId LEFT OUTER JOIN
                         dbo.Supervisors AS oSupervisor ON o.SupervisorId = oSupervisor.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oSupervisorUser ON oSupervisor.UserId = oSupervisorUser.Id LEFT OUTER JOIN
                         dbo.Dispatchers AS oDispatcher ON o.DispatcherId = oDispatcher.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oDispatcherUser ON oDispatcher.UserId = oDispatcherUser.Id LEFT OUTER JOIN
                         dbo.OrderStatus AS oStatus ON o.StatusId = oStatus.Id LEFT OUTER JOIN
                         dbo.BuildingTypes ON o.BuildingTypeId = dbo.BuildingTypes.Id LEFT OUTER JOIN
                         dbo.ContractTypes AS oContractTypes ON o.ContractTypeId = oContractTypes.Id LEFT OUTER JOIN
                         dbo.Division AS oDivision ON o.DivisionId = oDivision.Id LEFT OUTER JOIN
                         dbo.OrderProblem AS OProblem ON o.ProblemId = OProblem.Id LEFT OUTER JOIN
                         dbo.Areas AS oArea ON o.AreaId = oArea.Id
 