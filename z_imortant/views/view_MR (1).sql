USE [CodeFirst-db]
GO

/****** Object:  View [dbo].[View_Excel_MR]    Script Date: 6/20/2020 11:41:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_Excel_MR]
AS
SELECT        TOP (100) PERCENT o.Code AS Order_ID, o.SAP_CreatedDate AS Created_Date, oStatus.Name AS Current_Order_Status, oOrderType.Name AS Order_Type, o.DispatcherNote AS Dispatcher_Notes, 
                         o.ICAgentNote AS IC_Agent_Notes, o.OrderDescription AS Order_Description, oProblem.Name AS Problem_Type, oDivision.Name AS Division, oDispatcherUser.FirstName AS Current_Dispatcher, 
                         oDispatcherUser.LastName AS Current_Dispatcher2, oSupervisorUser.FirstName AS Current_Supervisor_Name, oSupervisorUser.LastName AS Current_Supervisor_Name2, 
                         oTeamForemanUser.FirstName AS Current_Foreman_Name, oTeamForemanUser.LastName AS Current_Foreman_Name2, oCompanyCode.Code AS Company_Code, o.CustomerCode AS Customer_ID, 
                         o.PhoneOne AS Customer_Tel_1, o.PhoneTwo AS Customer_Tel_2, o.Caller_ID AS Customer_Caller_Phone, o.SAP_PACI AS PACI_No, o.SAP_GovName AS PACI_Governorate, o.SAP_AreaName AS PACI_Area, 
                         o.SAP_StreetName AS PACI_Street, o.SAP_BlockName AS PACI_Block, o.SAP_AppartmentNo, o.SAP_HouseKasima AS SAP_Building, o.SAP_Floor, o.ContractCode AS Contract_Number, oContractType.Name AS Contract_Type, 
                         o.ContractStartDate AS Contract_Creation_Date, o.ContractExpiryDate AS Contract_Expiration_Date, o.UpdatedDate AS Closed_Date, ac.ActionDate AS Progress_Date_History, acExcuterUser.FirstName AS Executor_History, 
                         acExcuterUser.LastName AS Executor_History2, ac.ActionTypeName AS Action_Type_Name_History, acStatus.Name AS Progress_Status_History, ac.SubStatusName AS Progress_Sub_Status_History, 
                         acTechnicianUser.FirstName AS Technician_Name_History, acTechnicianUser.LastName AS Technician_Name_History2, acDispatcherUser.FirstName AS Dispatcher_Name_History, 
                         acDispatcherUser.LastName AS Dispatcher_Name_History2, ac.ServeyReport AS Survey_Report_History
FROM            dbo.Orders AS o LEFT OUTER JOIN
                         dbo.OrderActions AS ac ON o.Id = ac.OrderId LEFT OUTER JOIN
                         dbo.OrderStatus AS oStatus ON oStatus.Id = o.StatusId LEFT OUTER JOIN
                         dbo.OrderType AS oOrderType ON oOrderType.Id = o.TypeId LEFT OUTER JOIN
                         dbo.Dispatchers AS oDispatcher ON o.DispatcherId = oDispatcher.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oDispatcherUser ON oDispatcher.UserId = oDispatcherUser.Id LEFT OUTER JOIN
                         dbo.Division AS oDivision ON o.DivisionId = oDivision.Id LEFT OUTER JOIN
                         dbo.OrderProblem AS oProblem ON o.ProblemId = oProblem.Id LEFT OUTER JOIN
                         dbo.Supervisors AS oSupervisor ON o.SupervisorId = oSupervisor.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oSupervisorUser ON oSupervisor.UserId = oSupervisorUser.Id LEFT OUTER JOIN
                         dbo.Teams AS oTeam ON o.TeamId = oTeam.Id LEFT OUTER JOIN
                         dbo.Foremans AS oTeamForeman ON oTeam.ForemanId = oTeamForeman.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS oTeamForemanUser ON oTeamForeman.UserId = oTeamForemanUser.Id LEFT OUTER JOIN
                         dbo.ContractTypes AS oContractType ON o.ContractTypeId = oContractType.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS acExcuterUser ON ac.FK_CreatedBy_Id = acExcuterUser.Id LEFT OUTER JOIN
                         dbo.Dispatchers AS acDispatcher ON ac.DispatcherId = acDispatcher.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS acDispatcherUser ON acDispatcher.UserId = acDispatcherUser.Id LEFT OUTER JOIN
                         dbo.OrderStatus AS acStatus ON ac.StatusId = acStatus.Id LEFT OUTER JOIN
                         dbo.Technicians AS acTechnician ON ac.TechnicianId = acTechnician.Id LEFT OUTER JOIN
                         dbo.AspNetUsers AS acTechnicianUser ON acTechnician.UserId = acTechnicianUser.Id LEFT OUTER JOIN
                         dbo.CostCenter AS acCostCenter ON ac.CostCenterId = acCostCenter.Id LEFT OUTER JOIN
                         dbo.CompanyCode AS oCompanyCode ON oCompanyCode.Id = o.CompanyCodeId