/****** Object:  View [dbo].[View_Default_Order_Action]    Script Date: 11/16/2020 11:43:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_Default_Order_Action]
AS
SELECT MAX(OrderActions.Id) AS DefaultActionID
FROM     dbo.OrderActions
WHERE  (OrderId IS NOT NULL)
GROUP BY OrderActions.ActionDate
GO

