/****** Object:  View [dbo].[View_Last_Order_Action]    Script Date: 11/16/2020 11:41:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_Last_Order_Action]
AS
SELECT OrderActions.OrderId, MAX(OrderActions.Id) AS LastActionID
FROM     dbo.OrderActions
WHERE  (OrderId IS NOT NULL)
GROUP BY OrderActions.OrderId
GO

