/****** Object:  View [dbo].[View_Excel_MR_LastAction]    Script Date: 11/16/2020 11:41:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_Excel_MR_LastAction]
AS
SELECT dbo.View_Excel_MR.Order_ID, dbo.View_Excel_MR.Created_Date, dbo.View_Excel_MR.Current_Order_Status, dbo.View_Excel_MR.Order_Type, dbo.View_Excel_MR.Dispatcher_Notes, dbo.View_Excel_MR.IC_Agent_Notes, 
                  dbo.View_Excel_MR.Order_Description, dbo.View_Excel_MR.Problem_Type, dbo.View_Excel_MR.Division, dbo.View_Excel_MR.Current_Dispatcher, dbo.View_Excel_MR.Current_Supervisor_Name, 
                  dbo.View_Excel_MR.Current_Foreman_Name, dbo.View_Excel_MR.Company_Code, dbo.View_Excel_MR.Customer_ID, dbo.View_Excel_MR.Customer_Tel_1, dbo.View_Excel_MR.Customer_Tel_2, 
                  dbo.View_Excel_MR.Customer_Caller_Phone, dbo.View_Excel_MR.PACI_No, dbo.View_Excel_MR.PACI_Governorate, dbo.View_Excel_MR.PACI_Area, dbo.View_Excel_MR.PACI_Street, dbo.View_Excel_MR.PACI_Block, 
                  dbo.View_Excel_MR.SAP_Apartment, dbo.View_Excel_MR.SAP_Building, dbo.View_Excel_MR.SAP_Floor, dbo.View_Excel_MR.Contract_Number, dbo.View_Excel_MR.Contract_Type, dbo.View_Excel_MR.Contract_Creation_Date, 
                  dbo.View_Excel_MR.Contract_Expiration_Date, dbo.View_Excel_MR.Closed_Date, dbo.View_Excel_MR.Progress_Date_History, dbo.View_Excel_MR.Executor_History, dbo.View_Excel_MR.Action_Type_Name_History, 
                  dbo.View_Excel_MR.Progress_Status_History, dbo.View_Excel_MR.Progress_Sub_Status_History, dbo.View_Excel_MR.Technician_Name_History, dbo.View_Excel_MR.Dispatcher_Name_History, 
                  dbo.View_Excel_MR.Survey_Report_History, dbo.View_Excel_MR.Functional_Location, dbo.View_Excel_MR.AreaId, dbo.View_Excel_MR.DispatcherId, dbo.View_Excel_MR.DivisionId, dbo.View_Excel_MR.TypeId, 
                  dbo.View_Excel_MR.ProblemId, dbo.View_Excel_MR.StatusId, dbo.View_Excel_MR.CompanyCodeId, dbo.View_Excel_MR.Id, dbo.View_Excel_MR.WorkingTypeId, dbo.View_Excel_MR.acTechnicianIsDriver, 
                  dbo.View_Excel_MR.acTechnicianId, dbo.View_Excel_MR.PlatformTypeSource, dbo.View_Excel_MR.acStatusId, dbo.View_Excel_MR.acId, dbo.View_Excel_MR.Current_Technician, dbo.View_Last_Order_Action.LastActionID
FROM     dbo.View_Excel_MR LEFT OUTER JOIN
                  dbo.View_Last_Order_Action ON View_Excel_MR.id = View_Last_Order_Action.orderId
GO

