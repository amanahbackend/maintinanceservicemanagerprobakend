/****** Object:  View [dbo].[View_Excel_MR_DefaultAction]    Script Date: 11/16/2020 11:42:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_Excel_MR_DefaultAction]
AS
SELECT dbo.View_Excel_MR_LastAction.Order_ID, dbo.View_Excel_MR_LastAction.Created_Date, dbo.View_Excel_MR_LastAction.Current_Order_Status, dbo.View_Excel_MR_LastAction.Order_Type, 
                  dbo.View_Excel_MR_LastAction.Dispatcher_Notes, dbo.View_Excel_MR_LastAction.IC_Agent_Notes, dbo.View_Excel_MR_LastAction.Order_Description, dbo.View_Excel_MR_LastAction.Problem_Type, 
                  dbo.View_Excel_MR_LastAction.Division, dbo.View_Excel_MR_LastAction.Current_Dispatcher, dbo.View_Excel_MR_LastAction.Current_Supervisor_Name, dbo.View_Excel_MR_LastAction.Current_Foreman_Name, 
                  dbo.View_Excel_MR_LastAction.Company_Code, dbo.View_Excel_MR_LastAction.Customer_ID, dbo.View_Excel_MR_LastAction.Customer_Tel_1, dbo.View_Excel_MR_LastAction.Customer_Tel_2, 
                  dbo.View_Excel_MR_LastAction.Customer_Caller_Phone, dbo.View_Excel_MR_LastAction.PACI_No, dbo.View_Excel_MR_LastAction.PACI_Governorate, dbo.View_Excel_MR_LastAction.PACI_Area, 
                  dbo.View_Excel_MR_LastAction.PACI_Street, dbo.View_Excel_MR_LastAction.PACI_Block, dbo.View_Excel_MR_LastAction.SAP_Apartment, dbo.View_Excel_MR_LastAction.SAP_Building, dbo.View_Excel_MR_LastAction.SAP_Floor, 
                  dbo.View_Excel_MR_LastAction.Contract_Number, dbo.View_Excel_MR_LastAction.Contract_Type, dbo.View_Excel_MR_LastAction.Contract_Creation_Date, dbo.View_Excel_MR_LastAction.Contract_Expiration_Date, 
                  dbo.View_Excel_MR_LastAction.Closed_Date, dbo.View_Excel_MR_LastAction.Progress_Date_History, dbo.View_Excel_MR_LastAction.Executor_History, dbo.View_Excel_MR_LastAction.Action_Type_Name_History, 
                  dbo.View_Excel_MR_LastAction.Progress_Status_History, dbo.View_Excel_MR_LastAction.Progress_Sub_Status_History, dbo.View_Excel_MR_LastAction.Technician_Name_History, 
                  dbo.View_Excel_MR_LastAction.Dispatcher_Name_History, dbo.View_Excel_MR_LastAction.Survey_Report_History, dbo.View_Excel_MR_LastAction.Functional_Location, dbo.View_Excel_MR_LastAction.AreaId, 
                  dbo.View_Excel_MR_LastAction.DispatcherId, dbo.View_Excel_MR_LastAction.DivisionId, dbo.View_Excel_MR_LastAction.TypeId, dbo.View_Excel_MR_LastAction.ProblemId, dbo.View_Excel_MR_LastAction.StatusId, 
                  dbo.View_Excel_MR_LastAction.CompanyCodeId, dbo.View_Excel_MR_LastAction.Id, dbo.View_Excel_MR_LastAction.WorkingTypeId, dbo.View_Excel_MR_LastAction.acTechnicianIsDriver, 
                  dbo.View_Excel_MR_LastAction.acTechnicianId, dbo.View_Excel_MR_LastAction.PlatformTypeSource, dbo.View_Excel_MR_LastAction.acStatusId, dbo.View_Excel_MR_LastAction.acId, 
                  dbo.View_Excel_MR_LastAction.Current_Technician, dbo.View_Excel_MR_LastAction.LastActionID, dbo.View_Default_Order_Action.DefaultActionID
FROM     dbo.View_Excel_MR_LastAction LEFT OUTER JOIN
                  dbo.View_Default_Order_Action ON View_Excel_MR_LastAction.acId = View_Default_Order_Action.DefaultActionID
GO

