/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationUserHistory]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationUserHistory](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](max) NOT NULL,
	[LoginDate] [datetime2](7) NOT NULL,
	[LogoutDate] [datetime2](7) NULL,
	[Token] [nvarchar](max) NOT NULL,
	[UserType] [nvarchar](max) NOT NULL,
	[DeveiceId] [nvarchar](max) NULL,
 CONSTRAINT [PK_ApplicationUserHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArchivedOrderFile]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArchivedOrderFile](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](450) NULL,
	[Trials] [int] NOT NULL,
 CONSTRAINT [PK_ArchivedOrderFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ArchivedOrders]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArchivedOrders](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[OrderId] [int] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[TypeId] [int] NOT NULL,
	[TypeName] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[StatusName] [nvarchar](max) NULL,
	[SubStatusId] [int] NOT NULL,
	[SubStatusName] [nvarchar](max) NULL,
	[ProblemId] [int] NOT NULL,
	[ProblemName] [nvarchar](max) NULL,
	[PriorityId] [int] NOT NULL,
	[PriorityName] [nvarchar](max) NULL,
	[CompanyCodeId] [int] NOT NULL,
	[CompanyCodeName] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[DivisionName] [nvarchar](max) NULL,
	[ICAgentNote] [nvarchar](max) NULL,
	[DispatcherNote] [nvarchar](max) NULL,
	[CancellationReason] [nvarchar](max) NULL,
	[GeneralNote] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[OrderDescription] [nvarchar](max) NULL,
	[CustomerCode] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](max) NULL,
	[PhoneOne] [nvarchar](max) NULL,
	[PhoneTwo] [nvarchar](max) NULL,
	[Caller_ID] [nvarchar](max) NULL,
	[PACI] [nvarchar](max) NULL,
	[FunctionalLocation] [nvarchar](max) NULL,
	[HouseKasima] [nvarchar](max) NULL,
	[Floor] [nvarchar](max) NULL,
	[AppartmentNo] [nvarchar](max) NULL,
	[StreetId] [int] NOT NULL,
	[StreetName] [nvarchar](max) NULL,
	[BlockId] [int] NOT NULL,
	[BlockName] [nvarchar](max) NULL,
	[AreaId] [int] NOT NULL,
	[AreaName] [nvarchar](max) NULL,
	[GovId] [int] NOT NULL,
	[GovName] [nvarchar](max) NULL,
	[AddressNote] [nvarchar](max) NULL,
	[Long] [decimal](18, 2) NOT NULL,
	[Lat] [decimal](18, 2) NOT NULL,
	[BuildingTypeId] [int] NOT NULL,
	[BuildingTypeName] [nvarchar](max) NULL,
	[ContractCode] [nvarchar](max) NULL,
	[ContractTypeId] [int] NOT NULL,
	[ContractTypeName] [nvarchar](max) NULL,
	[ContractStartDate] [datetime2](7) NOT NULL,
	[ContractExpiryDate] [datetime2](7) NOT NULL,
	[InsertionDate] [datetime2](7) NOT NULL,
	[FileName] [nvarchar](max) NULL,
	[SupervisorId] [int] NOT NULL,
	[SupervisorName] [nvarchar](max) NULL,
	[DispatcherId] [int] NOT NULL,
	[DispatcherName] [nvarchar](max) NULL,
	[TeamId] [int] NOT NULL,
	[AcceptanceFlag] [int] NOT NULL,
	[RejectionReasonId] [int] NOT NULL,
	[RejectionReason] [nvarchar](max) NULL,
	[IsAccomplish] [int] NOT NULL,
	[IsRepeatedCall] [bit] NOT NULL,
	[IsExceedTime] [bit] NOT NULL,
 CONSTRAINT [PK_ArchivedOrders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Areas]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Areas](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Area_No] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Fk_Governorate_Id] [int] NULL,
 CONSTRAINT [PK_Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[PF] [nvarchar](max) NULL,
	[Phone1] [nvarchar](max) NULL,
	[Phone2] [nvarchar](max) NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Deactivated] [bit] NOT NULL,
	[PicturePath] [nvarchar](max) NULL,
	[IsFirstLogin] [bit] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[UserTypeId] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AttendanceStates]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttendanceStates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_AttendanceStates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attendences]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendences](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[AttendedId] [nvarchar](max) NULL,
	[AttendedName] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[StatusName] [int] NOT NULL,
	[AttendedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Attendences] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Availability]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Availability](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Availability] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BuildingTypes]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuildingTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_BuildingTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CompanyCode]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyCode](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CompanyCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractTypes]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_ContractTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CostCenter]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CostCenter](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CostCenter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dispatchers]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dispatchers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[SupervisorId] [int] NOT NULL,
 CONSTRAINT [PK_Dispatchers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DispatcherSettings]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DispatcherSettings](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[DispatcherId] [int] NOT NULL,
	[DivisionId] [int] NULL,
	[OrderProblemId] [int] NOT NULL,
	[AreaId] [int] NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_DispatcherSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Division]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Division](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Drivers]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drivers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[DivisionName] [nvarchar](max) NULL,
	[CostCenterId] [int] NOT NULL,
	[CostCenterName] [nvarchar](max) NULL,
	[VehicleNo] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
 CONSTRAINT [PK_Drivers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Engineers]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Engineers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[DispatcherId] [int] NULL,
 CONSTRAINT [PK_Engineers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Foremans]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Foremans](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[Name] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[SupervisorId] [int] NOT NULL,
	[DispatcherId] [int] NULL,
	[EngineerId] [int] NULL,
 CONSTRAINT [PK_Foremans] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Governorates]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Governorates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Gov_No] [int] NOT NULL,
 CONSTRAINT [PK_Governorates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JunkUser]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JunkUser](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[NormalizedUserName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[NormalizedEmail] [nvarchar](max) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[Phone1] [nvarchar](max) NOT NULL,
	[Phone2] [nvarchar](max) NULL,
	[PicturePath] [nvarchar](max) NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Deactivated] [bit] NOT NULL,
 CONSTRAINT [PK_JunkUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Areas]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Areas](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Area_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[AreasId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_AttendanceStates]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_AttendanceStates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_AttendanceStates_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[AttendanceStatesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_AttendanceStates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Availability]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Availability](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Availability_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[AvailabilityId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Availability] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_BuildingTypes]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_BuildingTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_BuildingTypes_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[BuildingTypesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_BuildingTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_CompanyCode]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_CompanyCode](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_CompanyCode_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CompanyCodeId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_CompanyCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_ContractTypes]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_ContractTypes](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_ContractTypes_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[ContractTypesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_ContractTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_CostCenter]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_CostCenter](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_CostCenter_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CostCenterId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_CostCenter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Division]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Division](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Division_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DivisionId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Division] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Governorates]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Governorates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Governorates_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[GovernoratesId] [int] NULL,
	[SupportedLanguagesId] [int] NULL,
 CONSTRAINT [PK_Lang_Governorates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Notification]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Notification](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Notification_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderPriority]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderPriority](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderPriority_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderPriority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderProblem]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderProblem](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderProblem_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderProblem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderStatus]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderStatus_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderSubStatus]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderSubStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderSubStatus_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderSubStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_OrderType]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_OrderType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_OrderType_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_OrderType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_RejectionReason]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_RejectionReason](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_RejectionReason_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Lang_RejectionReason] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_Shift]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_Shift](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_Shift_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[FromTime] [time](7) NOT NULL,
	[ToTime] [time](7) NOT NULL,
	[FromTimeValue] [datetime2](7) NOT NULL,
	[ToTimeValue] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Lang_Shift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lang_WorkingType]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lang_WorkingType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FK_WorkingType_ID] [int] NOT NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lang_WorkingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Managers]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Managers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[Name] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
	[CostCenterId] [int] NOT NULL,
 CONSTRAINT [PK_Managers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MapAdminRelated]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapAdminRelated](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mobile_Api_Key] [nvarchar](max) NULL,
	[Web_Api_Key] [nvarchar](max) NULL,
	[Refresh_Rate] [int] NOT NULL,
 CONSTRAINT [PK_MapAdminRelated] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaterialControllers]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialControllers](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[PF] [nvarchar](max) NULL,
	[DivisionId] [int] NOT NULL,
	[DivisionName] [nvarchar](max) NULL,
	[CostCenterId] [int] NOT NULL,
	[CostCenterName] [nvarchar](max) NULL,
 CONSTRAINT [PK_MaterialControllers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Members]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Members](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[TeamId] [int] NULL,
	[MemberParentId] [int] NOT NULL,
	[MemberParentName] [nvarchar](max) NULL,
	[Rank] [int] NOT NULL,
	[MemberType] [int] NOT NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NULL,
	[UserTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Members] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationCenter]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationCenter](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[RecieverId] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
	[Data] [nvarchar](max) NULL,
	[NotificationType] [nvarchar](max) NULL,
	[IsRead] [bit] NOT NULL,
	[TeamId] [int] NULL,
	[IsActionTaken] [bit] NULL,
	[OrderCode] [nvarchar](max) NULL,
 CONSTRAINT [PK_NotificationCenter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifications](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderActions]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderActions](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](450) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[OrderId] [int] NULL,
	[StatusId] [int] NULL,
	[SubStatusId] [int] NULL,
	[SubStatusName] [nvarchar](max) NULL,
	[ActionDate] [datetime2](7) NOT NULL,
	[ActionDay] [datetime2](7) NOT NULL,
	[ActionTime] [time](7) NULL,
	[ActionTimeDays] [int] NULL,
	[CreatedUserId] [int] NULL,
	[CreatedUser] [nvarchar](max) NULL,
	[ActionTypeId] [int] NULL,
	[ActionTypeName] [nvarchar](max) NULL,
	[CostCenterId] [int] NULL,
	[WorkingTypeId] [int] NULL,
	[WorkingTypeName] [nvarchar](max) NULL,
	[Reason] [nvarchar](max) NULL,
	[ServeyReport] [nvarchar](max) NULL,
	[ActionDistance] [float] NOT NULL,
	[SupervisorId] [int] NULL,
	[DispatcherId] [int] NULL,
	[ForemanId] [int] NULL,
	[TeamId] [int] NULL,
	[TechnicianId] [int] NULL,
	[PlatformTypeSource] [int] NOT NULL,
	[WorkingSubReason] [nvarchar](max) NULL,
	[WorkingSubReasonId] [int] NULL,
 CONSTRAINT [PK_OrderActions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderHistoryTeams]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderHistoryTeams](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[OrderId] [int] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[TeamId] [int] NULL,
	[PrevTeamId] [int] NULL,
 CONSTRAINT [PK_OrderHistoryTeams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderPriority]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderPriority](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderPriority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderProblem]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProblem](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[ExceedHours] [int] NOT NULL,
 CONSTRAINT [PK_OrderProblem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderRowData]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderRowData](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerNo] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](max) NULL,
	[PhoneOne] [nvarchar](max) NULL,
	[PhoneTwo] [nvarchar](max) NULL,
	[ContractNo] [nvarchar](max) NULL,
	[ContractType] [nvarchar](max) NULL,
	[ContractTypeDescription] [nvarchar](max) NULL,
	[ContractDate] [datetime2](7) NULL,
	[ContactExpiraion] [datetime2](7) NULL,
	[FunctionalLocation] [nvarchar](max) NULL,
	[PACI] [nvarchar](max) NULL,
	[Governorate] [int] NULL,
	[AreaCode] [int] NULL,
	[AreaDescription] [nvarchar](max) NULL,
	[Block] [nvarchar](max) NULL,
	[Street] [nvarchar](max) NULL,
	[House] [nvarchar](max) NULL,
	[Floor] [nvarchar](max) NULL,
	[AppartmentNo] [int] NULL,
	[AddressNote] [nvarchar](max) NULL,
	[OrderNo] [nvarchar](max) NULL,
	[OrderType] [nvarchar](max) NULL,
	[OrderTypeDescription] [nvarchar](max) NULL,
	[CompanyCode] [nvarchar](max) NULL,
	[OrderDate] [datetime2](7) NULL,
	[Division] [int] NULL,
	[DivisionDescription] [nvarchar](max) NULL,
	[OrderPriority] [int] NULL,
	[OrderPriorityDescription] [nvarchar](max) NULL,
	[Problem] [nvarchar](max) NULL,
	[ProblemDescription] [nvarchar](max) NULL,
	[OrderDescription] [nvarchar](max) NULL,
	[OrderStatus] [nvarchar](max) NULL,
	[OrderNoteAgent] [nvarchar](max) NULL,
	[Caller_ID] [nvarchar](max) NULL,
	[FileName] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderRowData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[TypeId] [int] NOT NULL,
	[TypeName] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[SubStatusId] [int] NOT NULL,
	[SubStatusName] [nvarchar](max) NULL,
	[ProblemId] [int] NULL,
	[PriorityId] [int] NOT NULL,
	[PriorityName] [nvarchar](max) NULL,
	[CompanyCodeId] [int] NOT NULL,
	[CompanyCodeName] [nvarchar](max) NULL,
	[DivisionId] [int] NULL,
	[ICAgentNote] [nvarchar](max) NULL,
	[DispatcherNote] [nvarchar](max) NULL,
	[CancellationReason] [nvarchar](max) NULL,
	[GeneralNote] [nvarchar](max) NULL,
	[SAP_CreatedDate] [datetime2](7) NOT NULL,
	[OrderDescription] [nvarchar](max) NULL,
	[ServeyReport] [nvarchar](max) NULL,
	[OrderTypeCode] [nvarchar](max) NULL,
	[CustomerCode] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](max) NULL,
	[PhoneOne] [nvarchar](max) NULL,
	[PhoneTwo] [nvarchar](max) NULL,
	[Caller_ID] [nvarchar](max) NULL,
	[SAP_PACI] [nvarchar](max) NULL,
	[SAP_HouseKasima] [nvarchar](max) NULL,
	[SAP_Floor] [nvarchar](max) NULL,
	[SAP_AppartmentNo] [nvarchar](max) NULL,
	[SAP_StreetName] [nvarchar](max) NULL,
	[SAP_BlockName] [nvarchar](max) NULL,
	[SAP_AreaName] [nvarchar](max) NULL,
	[SAP_GovName] [nvarchar](max) NULL,
	[PACI] [nvarchar](max) NULL,
	[FunctionalLocation] [nvarchar](max) NULL,
	[HouseKasima] [nvarchar](max) NULL,
	[Floor] [nvarchar](max) NULL,
	[AppartmentNo] [nvarchar](max) NULL,
	[StreetId] [int] NOT NULL,
	[StreetName] [nvarchar](max) NULL,
	[BlockId] [int] NOT NULL,
	[BlockName] [nvarchar](max) NULL,
	[AreaId] [int] NOT NULL,
	[AreaName] [nvarchar](max) NULL,
	[GovId] [int] NOT NULL,
	[GovName] [nvarchar](max) NULL,
	[AddressNote] [nvarchar](max) NULL,
	[Long] [float] NOT NULL,
	[Lat] [float] NOT NULL,
	[BuildingTypeId] [int] NULL,
	[ContractCode] [nvarchar](max) NULL,
	[ContractTypeId] [int] NULL,
	[ContractStartDate] [datetime2](7) NULL,
	[ContractExpiryDate] [datetime2](7) NULL,
	[InsertionDate] [datetime2](7) NOT NULL,
	[FileName] [nvarchar](max) NULL,
	[SupervisorId] [int] NULL,
	[DispatcherId] [int] NULL,
	[RankInDispatcher] [int] NULL,
	[TeamId] [int] NULL,
	[PrevTeamId] [int] NULL,
	[RankInTeam] [int] NULL,
	[AcceptanceFlag] [int] NOT NULL,
	[RejectionReasonId] [int] NULL,
	[RejectionReason] [nvarchar](max) NULL,
	[IsAccomplish] [int] NOT NULL,
	[OnHoldCount] [int] NOT NULL,
	[IsRepeatedCall] [bit] NOT NULL,
	[IsExceedTime] [bit] NOT NULL,
	[LastActorTechUserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderSubStatus]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderSubStatus](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[StatusName] [int] NOT NULL,
 CONSTRAINT [PK_OrderSubStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PACI]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PACI](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PACINumber] [nvarchar](450) NULL,
	[AreaName] [nvarchar](max) NULL,
	[AreaId] [int] NOT NULL,
	[GovernorateName] [nvarchar](max) NULL,
	[GovernorateId] [int] NOT NULL,
	[BlockName] [nvarchar](max) NULL,
	[BlockId] [int] NOT NULL,
	[StreetName] [nvarchar](max) NULL,
	[StreetId] [int] NOT NULL,
	[Longtiude] [decimal](18, 6) NOT NULL,
	[Latitude] [decimal](18, 6) NOT NULL,
	[AppartementNumber] [nvarchar](max) NULL,
	[FloorNumber] [nvarchar](max) NULL,
	[BuildingNumber] [nvarchar](max) NULL,
 CONSTRAINT [PK_PACI] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PasswordTokenPin]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordTokenPin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Token] [nvarchar](max) NOT NULL,
	[Pin] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_PasswordTokenPin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Privilege]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Privilege](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Privilege] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RejectionReason]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RejectionReason](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_RejectionReason] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePrivilege]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePrivilege](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Fk_ApplicationRole_Id] [nvarchar](max) NOT NULL,
	[Fk_Privilege_Id] [int] NOT NULL,
 CONSTRAINT [PK_RolePrivilege] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shift]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shift](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FromTime] [time](7) NOT NULL,
	[ToTime] [time](7) NOT NULL,
 CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supervisors]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supervisors](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NULL,
	[CostCenterId] [int] NULL,
	[ManagerId] [int] NULL,
 CONSTRAINT [PK_Supervisors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupportedLanguages]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupportedLanguages](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_SupportedLanguages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeamMembersHistory]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeamMembersHistory](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](max) NULL,
	[UserDetailId] [int] NULL,
	[TeamId] [int] NULL,
	[PrevTeamId] [int] NULL,
	[UserTypeId] [int] NOT NULL,
 CONSTRAINT [PK_TeamMembersHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teams]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[VehicleNo] [nvarchar](max) NULL,
	[DivisionId] [int] NULL,
	[ForemanId] [int] NULL,
	[EngineerId] [int] NULL,
	[DispatcherId] [int] NOT NULL,
	[SupervisorId] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[ShiftId] [int] NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Technicians]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Technicians](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DivisionId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[IsDriver] [bit] NOT NULL,
	[TeamId] [int] NULL,
	[FK_SupportedLanguages_ID] [int] NOT NULL,
 CONSTRAINT [PK_Technicians] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TechniciansStates]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TechniciansStates](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[TechnicianId] [int] NOT NULL,
	[TeamId] [int] NOT NULL,
	[ActionDate] [datetime2](7) NOT NULL,
	[Long] [decimal](18, 2) NOT NULL,
	[Lat] [decimal](18, 2) NOT NULL,
	[State] [int] NOT NULL,
 CONSTRAINT [PK_TechniciansStates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDevice]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDevice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Fk_AppUser_Id] [nvarchar](max) NOT NULL,
	[DeveiceId] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_UserDevice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehicles]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicles](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrentUserId] [nvarchar](max) NULL,
	[Plate_no] [nvarchar](max) NULL,
	[Isassigned] [bit] NOT NULL,
	[TeamId] [int] NULL,
 CONSTRAINT [PK_Vehicles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkingType]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkingType](
	[IsDeleted] [bit] NOT NULL,
	[FK_CreatedBy_Id] [nvarchar](max) NULL,
	[FK_UpdatedBy_Id] [nvarchar](max) NULL,
	[FK_DeletedBy_Id] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
	[CategoryCode] [int] NOT NULL,
 CONSTRAINT [PK_WorkingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[ExpireAt] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Field] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Job]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StateId] [bigint] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Queue] ASC,
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[List]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Server]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Set]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Value] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[State]    Script Date: 10/25/2020 4:19:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT ((0)) FOR [UserTypeId]
GO
ALTER TABLE [dbo].[Lang_Shift] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [FromTimeValue]
GO
ALTER TABLE [dbo].[Lang_Shift] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [ToTimeValue]
GO
ALTER TABLE [dbo].[Members] ADD  DEFAULT ((0)) FOR [UserTypeId]
GO
ALTER TABLE [dbo].[OrderActions] ADD  DEFAULT ((0)) FOR [PlatformTypeSource]
GO
ALTER TABLE [dbo].[Areas]  WITH CHECK ADD  CONSTRAINT [FK_Areas_Governorates_Fk_Governorate_Id] FOREIGN KEY([Fk_Governorate_Id])
REFERENCES [dbo].[Governorates] ([Id])
GO
ALTER TABLE [dbo].[Areas] CHECK CONSTRAINT [FK_Areas_Governorates_Fk_Governorate_Id]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_Division_DivisionId]
GO
ALTER TABLE [dbo].[Dispatchers]  WITH CHECK ADD  CONSTRAINT [FK_Dispatchers_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dispatchers] CHECK CONSTRAINT [FK_Dispatchers_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_Areas_AreaId] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_Areas_AreaId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_Division_DivisionId]
GO
ALTER TABLE [dbo].[DispatcherSettings]  WITH CHECK ADD  CONSTRAINT [FK_DispatcherSettings_OrderProblem_OrderProblemId] FOREIGN KEY([OrderProblemId])
REFERENCES [dbo].[OrderProblem] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DispatcherSettings] CHECK CONSTRAINT [FK_DispatcherSettings_OrderProblem_OrderProblemId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Engineers]  WITH CHECK ADD  CONSTRAINT [FK_Engineers_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Engineers] CHECK CONSTRAINT [FK_Engineers_Division_DivisionId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_Division_DivisionId]
GO
ALTER TABLE [dbo].[Foremans]  WITH CHECK ADD  CONSTRAINT [FK_Foremans_Engineers_EngineerId] FOREIGN KEY([EngineerId])
REFERENCES [dbo].[Engineers] ([Id])
GO
ALTER TABLE [dbo].[Foremans] CHECK CONSTRAINT [FK_Foremans_Engineers_EngineerId]
GO
ALTER TABLE [dbo].[Lang_Areas]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Areas_Areas_AreasId] FOREIGN KEY([AreasId])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[Lang_Areas] CHECK CONSTRAINT [FK_Lang_Areas_Areas_AreasId]
GO
ALTER TABLE [dbo].[Lang_Areas]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Areas_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Areas] CHECK CONSTRAINT [FK_Lang_Areas_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_AttendanceStates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_AttendanceStates_AttendanceStates_AttendanceStatesId] FOREIGN KEY([AttendanceStatesId])
REFERENCES [dbo].[AttendanceStates] ([Id])
GO
ALTER TABLE [dbo].[Lang_AttendanceStates] CHECK CONSTRAINT [FK_Lang_AttendanceStates_AttendanceStates_AttendanceStatesId]
GO
ALTER TABLE [dbo].[Lang_AttendanceStates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_AttendanceStates_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_AttendanceStates] CHECK CONSTRAINT [FK_Lang_AttendanceStates_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_Availability]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Availability_Availability_AvailabilityId] FOREIGN KEY([AvailabilityId])
REFERENCES [dbo].[Availability] ([Id])
GO
ALTER TABLE [dbo].[Lang_Availability] CHECK CONSTRAINT [FK_Lang_Availability_Availability_AvailabilityId]
GO
ALTER TABLE [dbo].[Lang_Availability]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Availability_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Availability] CHECK CONSTRAINT [FK_Lang_Availability_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_BuildingTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_BuildingTypes_BuildingTypes_BuildingTypesId] FOREIGN KEY([BuildingTypesId])
REFERENCES [dbo].[BuildingTypes] ([Id])
GO
ALTER TABLE [dbo].[Lang_BuildingTypes] CHECK CONSTRAINT [FK_Lang_BuildingTypes_BuildingTypes_BuildingTypesId]
GO
ALTER TABLE [dbo].[Lang_BuildingTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_BuildingTypes_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_BuildingTypes] CHECK CONSTRAINT [FK_Lang_BuildingTypes_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_CompanyCode]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CompanyCode_CompanyCode_CompanyCodeId] FOREIGN KEY([CompanyCodeId])
REFERENCES [dbo].[CompanyCode] ([Id])
GO
ALTER TABLE [dbo].[Lang_CompanyCode] CHECK CONSTRAINT [FK_Lang_CompanyCode_CompanyCode_CompanyCodeId]
GO
ALTER TABLE [dbo].[Lang_CompanyCode]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CompanyCode_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_CompanyCode] CHECK CONSTRAINT [FK_Lang_CompanyCode_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_ContractTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_ContractTypes_ContractTypes_ContractTypesId] FOREIGN KEY([ContractTypesId])
REFERENCES [dbo].[ContractTypes] ([Id])
GO
ALTER TABLE [dbo].[Lang_ContractTypes] CHECK CONSTRAINT [FK_Lang_ContractTypes_ContractTypes_ContractTypesId]
GO
ALTER TABLE [dbo].[Lang_ContractTypes]  WITH CHECK ADD  CONSTRAINT [FK_Lang_ContractTypes_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_ContractTypes] CHECK CONSTRAINT [FK_Lang_ContractTypes_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_CostCenter]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CostCenter_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
GO
ALTER TABLE [dbo].[Lang_CostCenter] CHECK CONSTRAINT [FK_Lang_CostCenter_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Lang_CostCenter]  WITH CHECK ADD  CONSTRAINT [FK_Lang_CostCenter_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_CostCenter] CHECK CONSTRAINT [FK_Lang_CostCenter_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_Division]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Division_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Lang_Division] CHECK CONSTRAINT [FK_Lang_Division_Division_DivisionId]
GO
ALTER TABLE [dbo].[Lang_Division]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Division_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Division] CHECK CONSTRAINT [FK_Lang_Division_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Lang_Governorates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Governorates_Governorates_GovernoratesId] FOREIGN KEY([GovernoratesId])
REFERENCES [dbo].[Governorates] ([Id])
GO
ALTER TABLE [dbo].[Lang_Governorates] CHECK CONSTRAINT [FK_Lang_Governorates_Governorates_GovernoratesId]
GO
ALTER TABLE [dbo].[Lang_Governorates]  WITH CHECK ADD  CONSTRAINT [FK_Lang_Governorates_SupportedLanguages_SupportedLanguagesId] FOREIGN KEY([SupportedLanguagesId])
REFERENCES [dbo].[SupportedLanguages] ([Id])
GO
ALTER TABLE [dbo].[Lang_Governorates] CHECK CONSTRAINT [FK_Lang_Governorates_SupportedLanguages_SupportedLanguagesId]
GO
ALTER TABLE [dbo].[Managers]  WITH CHECK ADD  CONSTRAINT [FK_Managers_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Managers] CHECK CONSTRAINT [FK_Managers_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Managers]  WITH CHECK ADD  CONSTRAINT [FK_Managers_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Managers] CHECK CONSTRAINT [FK_Managers_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Members]  WITH CHECK ADD  CONSTRAINT [FK_Members_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Members] CHECK CONSTRAINT [FK_Members_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Members]  WITH CHECK ADD  CONSTRAINT [FK_Members_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Members] CHECK CONSTRAINT [FK_Members_Division_DivisionId]
GO
ALTER TABLE [dbo].[Members]  WITH CHECK ADD  CONSTRAINT [FK_Members_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Members] CHECK CONSTRAINT [FK_Members_Teams_TeamId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_AspNetUsers_FK_CreatedBy_Id] FOREIGN KEY([FK_CreatedBy_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_AspNetUsers_FK_CreatedBy_Id]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Foremans_ForemanId] FOREIGN KEY([ForemanId])
REFERENCES [dbo].[Foremans] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Foremans_ForemanId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Orders_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Orders_OrderId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_OrderStatus_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_OrderStatus_StatusId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Teams_TeamId]
GO
ALTER TABLE [dbo].[OrderActions]  WITH CHECK ADD  CONSTRAINT [FK_OrderActions_Technicians_TechnicianId] FOREIGN KEY([TechnicianId])
REFERENCES [dbo].[Technicians] ([Id])
GO
ALTER TABLE [dbo].[OrderActions] CHECK CONSTRAINT [FK_OrderActions_Technicians_TechnicianId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_AspNetUsers_LastActorTechUserId] FOREIGN KEY([LastActorTechUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_AspNetUsers_LastActorTechUserId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_BuildingTypes_BuildingTypeId] FOREIGN KEY([BuildingTypeId])
REFERENCES [dbo].[BuildingTypes] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_BuildingTypes_BuildingTypeId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_ContractTypes_ContractTypeId] FOREIGN KEY([ContractTypeId])
REFERENCES [dbo].[ContractTypes] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_ContractTypes_ContractTypeId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Division_DivisionId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderProblem_ProblemId] FOREIGN KEY([ProblemId])
REFERENCES [dbo].[OrderProblem] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderProblem_ProblemId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderStatus_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderStatus_StatusId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Teams_TeamId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_Division_DivisionId]
GO
ALTER TABLE [dbo].[Supervisors]  WITH CHECK ADD  CONSTRAINT [FK_Supervisors_Managers_ManagerId] FOREIGN KEY([ManagerId])
REFERENCES [dbo].[Managers] ([Id])
GO
ALTER TABLE [dbo].[Supervisors] CHECK CONSTRAINT [FK_Supervisors_Managers_ManagerId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Availability_StatusId] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Availability] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Availability_StatusId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Dispatchers_DispatcherId] FOREIGN KEY([DispatcherId])
REFERENCES [dbo].[Dispatchers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Dispatchers_DispatcherId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Division_DivisionId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Engineers_EngineerId] FOREIGN KEY([EngineerId])
REFERENCES [dbo].[Engineers] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Engineers_EngineerId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Foremans_ForemanId] FOREIGN KEY([ForemanId])
REFERENCES [dbo].[Foremans] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Foremans_ForemanId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Shift_ShiftId] FOREIGN KEY([ShiftId])
REFERENCES [dbo].[Shift] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Shift_ShiftId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Supervisors_SupervisorId] FOREIGN KEY([SupervisorId])
REFERENCES [dbo].[Supervisors] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Supervisors_SupervisorId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_CostCenter_CostCenterId] FOREIGN KEY([CostCenterId])
REFERENCES [dbo].[CostCenter] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_CostCenter_CostCenterId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_Division_DivisionId] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_Division_DivisionId]
GO
ALTER TABLE [dbo].[Technicians]  WITH CHECK ADD  CONSTRAINT [FK_Technicians_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Technicians] CHECK CONSTRAINT [FK_Technicians_Teams_TeamId]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
