﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ghanim.Order.Reading.containers
{
    public class OrderContainerData
    {
        //public string StatusName { get; set; }
        public string DivisionCode { get; set; }
        public string ProblemCode { get; set; }
        public string ContractTypeCode { get; set; }
        public string BuildingTypeName { get; internal set; }
        public Order order { get; set; }
    }
}