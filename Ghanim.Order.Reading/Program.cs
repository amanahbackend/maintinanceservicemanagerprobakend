﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ghanim.Order.Reading;
using Microsoft.Azure.WebJobs;

namespace Ghanim.Reading
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            //var config = new JobHostConfiguration();

            //if (config.IsDevelopment)
            //{
            //    config.UseDevelopmentSettings();
            //}

            //var host = new JobHost(config);
            //// The following code ensures that the WebJob will be running continuously
            //host.Call(typeof(Functions).GetMethod("ReadFileTask"));

            Entities context = new Entities();
            Functions job = new Functions(context);
            var task = Task.Run(() => job.ReadFileTask());
            task.Wait();
        }
    }
}
