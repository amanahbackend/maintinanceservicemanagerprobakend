﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ghanim.Order.Reading;
using Ghanim.Reading.PACIHelpers;

namespace Ghanim.Reading
{
    public static class Helpers
    {


        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public static async Task<string> GetGeoLocation(string address)
        {
            try
            {
                string baseUrl = ConfigurationManager.AppSettings["GoogleApiService"];
                if (!String.IsNullOrEmpty(baseUrl))
                {
                    string requestedAction = ConfigurationManager.AppSettings["GetGeoLocation"];
                    string apiKey = ConfigurationManager.AppSettings["ApiKey"];
                    var url = $"{requestedAction}?address={address}&key={apiKey}";

                    using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                    {
                        client.BaseAddress = new Uri(baseUrl);
                        var response = await client.GetAsync(url);
                        //response.EnsureSuccessStatusCode();
                        if (response.IsSuccessStatusCode)
                        {
                            return await response.Content.ReadAsStringAsync();
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool isDebuggingMod = true;
        public static void ConsoleErrorWriteLine(string msg)
        {
            if (isDebuggingMod)
                Console.Error.WriteLine(msg);
        }
        public static void ConsoleErrorWriteLine(string format, object arg0)
        {
            if (isDebuggingMod)
                Console.Error.WriteLine(format, arg0);
        }

        public static void ConsoleWriteLine(string msg)
        {
            if (isDebuggingMod)
                Console.WriteLine(msg);
        }


    }
}
