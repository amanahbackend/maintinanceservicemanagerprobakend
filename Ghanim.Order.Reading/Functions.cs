﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ghanim.Reading.ExcelSettings;
using System.Configuration;
using System.Data.Entity;
using Ghanim.Order.Reading;
using Ghanim.Order.Reading.containers;
using Newtonsoft.Json.Linq;
using Ghanim.Reading.PACIHelpers;
using Newtonsoft.Json;

namespace Ghanim.Reading
{
    using Order = Order.Reading.Order;

    public class Functions
    {
        private readonly Entities _context;

        private string notFoundvalue = "NotFound";
        static readonly object _object = new object();


        public Functions(Entities context)
        {
            _context = context;
        }

        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        //public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        //{
        //    log.WriteLine(message);
        //}

        public async Task ReadFileTask()
        {
            //Start:
            try
            {
                string fileExtension = ConfigurationManager.AppSettings["FileExtension"];
                int trials = int.Parse(ConfigurationManager.AppSettings["Trials"]);

                var response = FTPManager.GetListOfFiles();

                StreamReader streamReader = new StreamReader(response.GetResponseStream());

                string file = string.Empty;
                string streamLine = await streamReader.ReadLineAsync();
                while (!string.IsNullOrEmpty(streamLine))
                {
                    if (streamLine.Contains(fileExtension))
                    {
                        file = streamLine;
                        break;
                    }
                    streamLine = await streamReader.ReadLineAsync();
                }
                streamReader.Close();
                response.Dispose();

                bool isPaciOnline = PACIHelper.IsPaciOnline(file);
                Helpers.ConsoleWriteLine(
                    $"isPaciOnline is {isPaciOnline}");
                if (!string.IsNullOrEmpty(file))
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{file} , Checking file existence");

                    var archFile = _context.ArchivedOrderFiles.FirstOrDefault(x => x.FileName == file);
                    if (archFile != null)
                    {
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{file} , FILE FOUND WITH READ TRIALS {archFile.Trials}");
                        if (archFile.Trials >= trials)
                        {

                            Helpers.ConsoleErrorWriteLine(
                                $"ERROR::> Processing File:{file} , File Already In Archived With Trials {archFile.Trials} ,Moving File To Invalid.");
                            FTPManager.MoveInvalidFile(file);
                            return;
                        }

                        try
                        {
                            Helpers.ConsoleWriteLine(
                                 $"INFO::> Processing File:{file} , UPDATING  file TRIALS with");

                            try
                            {
                                Helpers.ConsoleWriteLine(
                                     $"INFO::> Processing File:{file} , UPDATING  file TRIALS with {archFile.Trials}");

                                archFile.Trials += 1;
                                archFile.UpdatedDate = DateTime.Now;

                                await _context.SaveChangesAsync();
                            }
                            catch (Exception ex)
                            {
                                Helpers.ConsoleErrorWriteLine(
                                    $"ERROR::> Processing File:{file} , UPDATING FILE TRIALS ERROR VAL  = {archFile.Trials}",
                                    ex.Message);
                            }

                            //return;
                        }
                        catch (Exception ex)
                        {
                            Helpers.ConsoleErrorWriteLine(
                                $"ERROR::> Processing File:{file} , SAVING FILE TRIALS > {archFile.Trials} , ERROR",
                                ex.Message);
                        }
                    }


                    var responseRead = FTPManager.DownloadFile(file);
                    if (responseRead != null)
                    {
                        var stream = responseRead.GetResponseStream();
                        var bytes = Helpers.ReadFully(stream);
                        var memoryStream = new MemoryStream(bytes);
                        stream.Dispose();
                        var reader = new StreamReader(memoryStream);
                        List<Order> orderList = new List<Order>();
                        List<OrderRowData> orderRowDataList = new List<OrderRowData>();
                        bool isFirst = true;
                        int ordersFound = 0;

                        PACIHelper.Intialization(ConfigurationManager.AppSettings["ProxyUrl"],
                            ConfigurationManager.AppSettings["PACIServiceUrl"],
                            ConfigurationManager.AppSettings["PACIFieldName"],
                            ConfigurationManager.AppSettings["BlockServiceUrl"],
                            ConfigurationManager.AppSettings["BlockNameFieldName"],
                            ConfigurationManager.AppSettings["AreaNameFieldName"],
                            ConfigurationManager.AppSettings["StreetServiceUrl"],
                            ConfigurationManager.AppSettings["BlockNameFieldName"],
                            ConfigurationManager.AppSettings["StreetNameFieldName"]);

                        int alreadyExistedOrders = 0;

                        while (!reader.EndOfStream)
                        {
                            var line = await reader.ReadLineAsync();
                            var values = line.Split(',');

                            if (isFirst == false)
                            {
                                if (values.Length > 0)
                                {
                                    ordersFound++;
                                    try
                                    {
                                        var orderRow = GenerateOrderRowData(values, file);
                                        orderRowDataList.Add(orderRow);
                                        if (!_context.Orders.Any(o => o.Code == orderRow.OrderNo))
                                        {
                                            var orderObject = await GenerateOrderObject(values, file, isPaciOnline);
                                            orderList.Add(orderObject);
                                        }
                                        else
                                        {
                                            alreadyExistedOrders++;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Helpers.ConsoleErrorWriteLine(
                                            $"ERROR::> Processing File:{file} , GENERATING ORDER DATA ERROR > ",
                                            ex.Message);
                                    }
                                }
                            }
                            else
                            {
                                isFirst = false;
                            }
                        }

                        Helpers.ConsoleWriteLine("-------------------------------------------------------------------------");
                        Helpers.ConsoleWriteLine($"INFO::> Processing File:{file} , FOUND {ordersFound} ORDERS IN THE FILE");

                        lock (_object)
                        {
                            SaveData(file, orderList, orderRowDataList, alreadyExistedOrders);
                        }

                        Helpers.ConsoleWriteLine($"INFO::> Processing File:{file} , END Processing File.");
                        reader.Close();
                        memoryStream.Dispose();

                        Helpers.ConsoleWriteLine("-------------------------------------------------------------------------");
                    }
                }
            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> READING FILE TASK ERROR > ", ex.Message);
            }
        }



        /// <summary>
        /// saving theorders that been read to the database 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="orderList"></param>
        /// <param name="orderRowDataList"></param>
        /// <returns></returns>
        private void SaveData(string file, List<Order> orderList, List<OrderRowData> orderRowDataList, int alreadyExistedOrders)
        {
            using (DbContextTransaction dbTransaction = _context.Database.BeginTransaction())
            {
                int successOrders = 0;
                try
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{file} , Start Database Transaction [SAVING {orderList.Count()} ORDERS]");
                    //_context.Orders.AddRange(orderList);

                    foreach (var order in orderList)
                    {
                        if (!_context.Orders.Any(o => o.Code == order.Code))
                        {
                            Order orderToAdd = new Order();
                            orderToAdd = (Order)order;
                            _context.Orders.Add(orderToAdd);
                            _context.SaveChanges();
                            _context.OrderActions.Add(new OrderAction()
                            {
                                OrderId = order.Id,
                                ActionDate = DateTime.Now,
                                PlatformTypeSource = 1
                            });
                            _context.SaveChanges();

                            successOrders += 1;
                            Helpers.ConsoleWriteLine(
                                 $"INFO::> Processing File:{file} , #{successOrders} - ORDER {order.Code} SAVED INTO DB");
                        }
                    }

                    if (successOrders + alreadyExistedOrders == orderRowDataList.Count)
                    {
                        Helpers.ConsoleWriteLine(
                             $"ERROR::> Processing File:{file} ,MOVING FILE TO ARCHIVED success:{successOrders} / duplicates:{alreadyExistedOrders} ");
                        FTPManager.MoveFile(file);
                    }


                    dbTransaction.Commit();

                    if (alreadyExistedOrders > 0)
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{file} " +
                             $", END Database Transaction [{alreadyExistedOrders} OF {orderList.Count()} ORDERS ARE FOUND DUPLICATED]");

                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{file} " +
                         $", END Database Transaction [{successOrders} OF {orderList.Count()} ORDERS SAVED SUCCESSFULLY]");



                    try
                    {
                        Helpers.ConsoleWriteLine($"INFO::> Processing File:{file} " + ", Saving into ArchivedOrderFiles & OrderRowDatas");
                        _context.ArchivedOrderFiles.Add(new ArchivedOrderFile
                        { FileName = file, Trials = 0, CreatedDate = DateTime.Now });
                        _context.OrderRowDatas.AddRange(orderRowDataList);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Helpers.ConsoleWriteLine($"INFO::> Processing File:{file} " + $", Failed To Save into ArchivedOrderFiles or OrderRowDatas as {e.InnerException.Message}");
                    }

                }
                catch (Exception ex)
                {
                    Helpers.ConsoleErrorWriteLine(
                        $"ERROR::> Processing File:{file} , END Database Transaction [SAVING ORDERS FAILED] > ",
                        ex.Message);
                    Helpers.ConsoleErrorWriteLine(
                        $"error msg:: #Exception{ex.Message} #InnerException {ex.InnerException?.Message ?? ""}  #InnerExceptionInnerException {ex.InnerException?.InnerException?.Message ?? ""}",
                        ex.Message);
                    dbTransaction.Rollback();
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{file} , END Database Transaction [CHANGES ROLLBACK]");
                }
                finally
                {
                    // DELETE THE FILE WHEN ALL ORDERS INSERTED BEFORE > DUPLICATE FILE CASE
                    if (alreadyExistedOrders > 0 && alreadyExistedOrders == orderList.Count)
                    {
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{file} " +
                             $", DUPLICATE ORDERS [{alreadyExistedOrders} OF {orderList.Count()}  > DUPLICATE FILE > MOVING FILE TO INVALID ");
                        FTPManager.MoveInvalidFile(file);
                    }
                }
            }
        }

        private OrderRowData GenerateOrderRowData(string[] values, string file)
        {
            OrderRowData orderRow = new OrderRowData();
            int outNum;

            orderRow.FileName = file;
            orderRow.CreatedDate = DateTime.Now;
            orderRow.AppartmentNo =
                int.TryParse(values[(int)ExcelSheetProperties.ApartmentNo], out outNum) ? outNum : 0;
            orderRow.AreaCode = int.TryParse(values[(int)ExcelSheetProperties.Area], out outNum) ? outNum : 0;
            orderRow.AreaDescription = values[(int)ExcelSheetProperties.AreaDescription].Trim();
            orderRow.Block = values[(int)ExcelSheetProperties.Block].Trim();
            orderRow.CompanyCode = values[(int)ExcelSheetProperties.OrderCompanyCode].Trim();
            if (!string.IsNullOrEmpty(values[(int)ExcelSheetProperties.ContractExpirationDate].Trim()))
            {
                orderRow.ContactExpiraion =
                    DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractExpirationDate].Trim(),
                        "dd.MM.yyyy", null);
            }

            if (!string.IsNullOrEmpty(values[(int)ExcelSheetProperties.ContractValidFrom].Trim()))
            {
                orderRow.ContractDate =
                    DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractValidFrom].Trim(), "dd.MM.yyyy",
                        null);
            }

            orderRow.ContractNo = values[(int)ExcelSheetProperties.ContractNo].Trim();
            orderRow.ContractType = values[(int)ExcelSheetProperties.ContractType].Trim();
            orderRow.ContractTypeDescription = values[(int)ExcelSheetProperties.ContractTypeDescription].Trim();
            orderRow.CustomerName = values[(int)ExcelSheetProperties.CustomerName].Trim();
            orderRow.CustomerNo = values[(int)ExcelSheetProperties.CustomerId].Trim();
            orderRow.Division = int.TryParse(values[(int)ExcelSheetProperties.OrderDivision], out outNum) ? outNum : 0;
            orderRow.DivisionDescription = values[(int)ExcelSheetProperties.OrderDivisionDescription].Trim();
            orderRow.Floor = values[(int)ExcelSheetProperties.Floor].Trim();
            orderRow.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].Trim();
            orderRow.House = values[(int)ExcelSheetProperties.HouseKasima].Trim();
            if (!string.IsNullOrEmpty(values[(int)ExcelSheetProperties.OrderCreatedDateTime].Trim()))
            {
                orderRow.OrderDate =
                    DateTime.ParseExact(values[(int)ExcelSheetProperties.OrderCreatedDateTime].Trim(),
                        "dd.MM.yyyy HH:mm:ss", null);
            }

            orderRow.OrderNo = values[(int)ExcelSheetProperties.OrderNo].Trim();
            orderRow.OrderNoteAgent = values[(int)ExcelSheetProperties.OrderNoteICAgent].Trim();
            orderRow.OrderPriority = int.TryParse(values[(int)ExcelSheetProperties.OrderPriority], out outNum) ? outNum : 0;
            orderRow.OrderPriorityDescription = values[(int)ExcelSheetProperties.OrderPriorityDescription].Trim();
            orderRow.OrderStatus = values[(int)ExcelSheetProperties.OrderStatusDescription].Trim();
            orderRow.OrderType = values[(int)ExcelSheetProperties.OrderType].Trim();
            orderRow.OrderTypeDescription = values[(int)ExcelSheetProperties.OrderTypeDescription].Trim();
            orderRow.PACI = values[(int)ExcelSheetProperties.Paci].Trim();
            orderRow.PhoneOne = values[(int)ExcelSheetProperties.Phone1].Trim();
            orderRow.PhoneTwo = values[(int)ExcelSheetProperties.Phone2].Trim();
            orderRow.Problem = values[(int)ExcelSheetProperties.OrderProblem1].Trim();
            orderRow.ProblemDescription = values[(int)ExcelSheetProperties.OrderProblem1Description].Trim();
            orderRow.Street = values[(int)ExcelSheetProperties.StreetJaddah].Trim();
            orderRow.Caller_ID = values[(int)ExcelSheetProperties.Caller_ID].Trim();
            orderRow.OrderDescription = values[(int)ExcelSheetProperties.OrderDescription].Trim();

            return orderRow;
        }

        private async Task<Order> GenerateOrderObject(string[] values, string fileName, bool IsPaciOnline)
        {
            Helpers.ConsoleWriteLine("--------------------------------------------------------------------------");
            OrderContainerData orderContainer = new OrderContainerData();
            orderContainer.order = new Order();

            MapOrderFromExcel(values, fileName, orderContainer);
            Order orderObject = orderContainer.order;

            Helpers.ConsoleWriteLine($"Start Processing File {fileName} , OrderCode {orderObject.Code}");

            // EXTRACTING ORDER TYPE
            try
            {
                if (!string.IsNullOrEmpty(orderObject.OrderTypeCode))
                {
                    var type = _context.OrderTypes.FirstOrDefault(x => x.Code == orderObject.OrderTypeCode);

                    if (type == null)
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , TypeName: {orderObject.OrderTypeCode} IS NOT FOUND IN DB");

                    orderObject.TypeId = type?.Id ?? 0;
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , OrderTypeCode IS EMPTY");
                }

            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , TypeName: {orderObject.OrderTypeCode}",
                    ex.Message);
            }

            // EXTRACTING PRIORITY
            try
            {
                int priorityId = TryParse(ExcelSheetProperties.OrderPriority, values);
                if (priorityId != 0)
                {
                    var priority = _context.OrderPriorities.FirstOrDefault(x => x.Id == priorityId);
                    orderObject.PriorityName = priority?.Name ?? "";
                    if (priority == null)
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , PriorityName: {orderObject.PriorityName} IS NOT FOUND IN DB");
                    orderObject.PriorityId = priority?.Id ?? 0;
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , PriorityName IS EMPTY");
                }

            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , PriorityName: {orderObject.PriorityName}",
                    ex.Message);
            }

            // EXTRACTING COMPANY CODE
            try
            {
                orderObject.CompanyCodeName = values[(int)ExcelSheetProperties.OrderCompanyCode];
                if (!string.IsNullOrEmpty(orderObject.CompanyCodeName))
                {
                    var companyCodeRes = _context.CompanyCodes.FirstOrDefault(x => x.Code == orderObject.CompanyCodeName);
                    orderObject.CompanyCodeId = companyCodeRes?.Id ?? 0;
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , CompanyCodeName IS EMPTY");
                }

            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , CompanyCodeName: {orderObject.CompanyCodeName}",
                    ex.Message);
            }

            // EXTRACTING CREATION TIME
            if (!string.IsNullOrEmpty(values[(int)ExcelSheetProperties.OrderCreatedDateTime]))
            {
                orderObject.SAP_CreatedDate = DateTime.ParseExact(
                    values[(int)ExcelSheetProperties.OrderCreatedDateTime],
                    "dd.MM.yyyy HH:mm:ss", null);
            }

            // EXTRACTING DIVISION
            try
            {
                if (!string.IsNullOrEmpty(orderContainer.DivisionCode))
                {
                    var division = _context.Divisions.FirstOrDefault(x => x.code == orderContainer.DivisionCode);
                    if (division == null)
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , DivisionCode: {orderContainer.DivisionCode} IS NOT FOUND IN DB");

                    orderObject.DivisionId = division?.Id;
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , DivisionName IS EMPTY");
                }

            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , DivisionCode: {orderContainer.DivisionCode}",
                    ex.Message);
            }


            // EXTRACTING PROBLEM NAME
            try
            {

                int priorityId = TryParse(ExcelSheetProperties.OrderPriority, values);



                if (!string.IsNullOrEmpty(orderContainer.ProblemCode))
                {
                    var problem = _context.OrderProblems.FirstOrDefault(x => x.Code == orderContainer.ProblemCode);
                    if (problem == null)
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , ProblemCode: {orderContainer.ProblemCode} IS NOT FOUND IN DB");
                    orderObject.ProblemId = problem?.Id;
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , ProblemName IS EMPTY");
                }

            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , ProblemCode: {orderContainer.ProblemCode}",
                    ex.Message);
            }


            // EXTRACTING CONTRACT TYPE
            try
            {
                if (!string.IsNullOrEmpty(orderContainer.ContractTypeCode))
                {
                    var contractType = _context.ContractTypes.FirstOrDefault(x => x.Code == orderContainer.ContractTypeCode);
                    if (contractType == null)
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , ContractTypeCode: {orderContainer.ContractTypeCode} IS NOT FOUND IN DB");
                    orderObject.ContractTypeId = contractType?.Id;
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , ContractTypeName IS EMPTY");
                }

            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , ContractTypeName: {notFoundvalue}",
                    ex.Message);
            }

            if (!string.IsNullOrEmpty(values[(int)ExcelSheetProperties.ContractValidFrom]))
                orderObject.ContractStartDate =
                    DateTime.ParseExact(values[(int)ExcelSheetProperties.ContractValidFrom], "dd.MM.yyyy",
                        null);
            else
                orderObject.ContractStartDate = null;

            if (!string.IsNullOrEmpty(values[(int)ExcelSheetProperties.ContractExpirationDate]))
                orderObject.ContractExpiryDate = DateTime.ParseExact(
                    values[(int)ExcelSheetProperties.ContractExpirationDate],
                    "dd.MM.yyyy", null);
            else
                orderObject.ContractExpiryDate = null;

            // EXTRACTING BUILDING TYPE
            try
            {
                if (!string.IsNullOrEmpty(orderContainer.BuildingTypeName))
                {
                    var buildingType = _context.BuildingTypes.FirstOrDefault(x => x.Name == orderContainer.BuildingTypeName);
                    if (buildingType == null)
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , BuildingTypeName: {notFoundvalue} IS NOT FOUND IN DB");
                    orderObject.BuildingTypeId = buildingType?.Id;
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , BuildingTypeName IS EMPTY");
                }
            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , BuildingTypeName: {notFoundvalue}",
                    ex.Message);
            }

            // SETTING ADDRESS INFORMATION
            if (orderObject.SAP_PACI != "77777777")
            {
                GetPaciDetails(fileName, orderObject, IsPaciOnline);
            }
            else
            {
                Helpers.ConsoleErrorWriteLine(
                     $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , PACI NO {orderObject.SAP_PACI} INVALID PACI NO ");
            }

            // GEOCODING FROM GOOGLE API ONLY WHEN AREA & BLOCK & STREET HAS VALUES
            if ((decimal)orderObject.Lat == decimal.Zero || (decimal)orderObject.Long == decimal.Zero)
            {
                if (
                    !string.IsNullOrEmpty(orderObject.SAP_AreaName) &&
                    !string.IsNullOrEmpty(orderObject.SAP_BlockName) &&
                    !string.IsNullOrEmpty(orderObject.SAP_StreetName)
                    )
                {
                    await GeocodeOrderAddress(fileName, orderObject);

                }
                else
                    Helpers.ConsoleWriteLine(
                        $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} " +
                        $", MISSING ADDRESS PARAMS > {orderObject.SAP_AreaName} - {orderObject.StreetName} - {orderObject.SAP_BlockName}");
            }


            // EXTRACTING SAP AREA NAME
            try
            {
                var areaNo = int.TryParse(values[(int)ExcelSheetProperties.Area], out var outNum) ? outNum : 0;
                if (areaNo != 0)
                {
                    var areaRes = _context.Areas.FirstOrDefault(x => x.Area_No == areaNo);
                    if (areaRes != null)
                        orderObject.AreaId = areaRes?.Id ?? 0;
                    else
                    {
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAP_AreaName: {orderObject.SAP_AreaName} IS NOT FOUND IN DB");

                        var newArea = _context.Areas.Add(new Area { Name = orderObject.SAP_AreaName, Area_No = areaNo });

                        if (_context.SaveChanges() > 0)
                        {
                            Helpers.ConsoleWriteLine(
                                 $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAP_AreaName: {orderObject.SAP_AreaName} CREATED & INSERTED IN DB");
                            orderObject.AreaId = newArea?.Id ?? 0;
                        }
                    }
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAP_AREA {orderObject.SAP_AreaName} , SAP_AreaID IS EMPTY");
                }
            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAP_AreaName:{orderObject.SAP_AreaName} ",
                    ex.Message);
            }

            // EXTRACTING SAP GOV NAME
            try
            {
                if (!string.IsNullOrEmpty(orderObject.SAP_GovName))
                {
                    var govRes = _context.Governorates.FirstOrDefault(x => x.Name == orderObject.SAP_GovName);
                    if (govRes != null)
                        orderObject.GovId = govRes?.Id ?? 0;
                    else
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAP_GovName: {orderObject.SAP_GovName} IS NOT FOUND IN DB");
                }
                else
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAP_GovName IS EMPTY");
                }
            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAP_GovName:{orderObject.SAP_GovName} ",
                    ex.Message);
            }

            //Assign to Dispatcher

            AssignOrder(fileName, orderObject);


            return orderObject;
        }

        private void MapOrderFromExcel(string[] values, string fileName, OrderContainerData container)
        {
            container.order = container.order ?? new Order();
            container.order.CreatedDate = DateTime.Now;
            container.order.Code = values[(int)ExcelSheetProperties.OrderNo].Trim();
            container.order.OrderTypeCode = values[(int)ExcelSheetProperties.OrderType].Trim();

            container.order.TypeName = values[(int)ExcelSheetProperties.OrderTypeDescription].Trim();
            if (container.order.TypeName == "Cash Compressor")
            {
                container.order.OrderTypeCode = "ZS99";
            }
            container.order.Caller_ID = values[(int)ExcelSheetProperties.Caller_ID].Trim();
            container.order.OrderDescription = values[(int)ExcelSheetProperties.OrderDescription].Trim();
            //container.StatusName = ConfigurationManager.AppSettings["InitialStatusName"];
            container.order.StatusId = int.Parse(ConfigurationManager.AppSettings["InitialId"]);
            container.order.SubStatusId = int.Parse(ConfigurationManager.AppSettings["InitialId"]);
            container.order.SubStatusName = ConfigurationManager.AppSettings["InitialSubStatusName"];
            container.DivisionCode = values[(int)ExcelSheetProperties.OrderDivision].Trim();
            container.ProblemCode = values[(int)ExcelSheetProperties.OrderProblem1].Trim();
            container.order.ICAgentNote = values[(int)ExcelSheetProperties.OrderNoteICAgent].Trim();
            container.order.ContractCode = values[(int)ExcelSheetProperties.ContractNo].Trim();
            container.ContractTypeCode = values[(int)ExcelSheetProperties.ContractType].Trim();

            container.order.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].Trim();
            container.order.CustomerCode = values[(int)ExcelSheetProperties.CustomerId].Trim();
            container.order.CustomerName = values[(int)ExcelSheetProperties.CustomerName].Trim();
            container.order.PhoneOne = values[(int)ExcelSheetProperties.Phone1].Trim();
            container.order.PhoneTwo = values[(int)ExcelSheetProperties.Phone2].Trim();
            container.order.SAP_PACI = values[(int)ExcelSheetProperties.Paci].Trim();
            container.order.SAP_AreaName = values[(int)ExcelSheetProperties.AreaDescription].Trim();


            container.order.SAP_BlockName = values[(int)ExcelSheetProperties.Block].Trim();
            container.order.SAP_StreetName = values[(int)ExcelSheetProperties.StreetJaddah].Trim();
            container.order.SAP_HouseKasima = values[(int)ExcelSheetProperties.HouseKasima].Trim();
            container.order.SAP_Floor = values[(int)ExcelSheetProperties.Floor].Trim();
            container.order.FunctionalLocation = values[(int)ExcelSheetProperties.FunctionalLocation].Trim();
            container.order.SAP_AppartmentNo = values[(int)ExcelSheetProperties.ApartmentNo].Trim();
            container.BuildingTypeName = values[(int)ExcelSheetProperties.BuildingType].Trim();

            container.order.FileName = fileName;
            container.order.InsertionDate = DateTime.Now;
            container.order.AcceptanceFlag = int.Parse(ConfigurationManager.AppSettings["InitialId"]);
            container.order.IsAccomplish = int.Parse(ConfigurationManager.AppSettings["InitialId"]);
        }

        private void AssignOrder(string fileName, Order orderObject)
        {
            try
            {
                var dispatchers = _context.DispatcherSettings.Where(x =>
                    x.AreaId == orderObject.AreaId && x.DivisionId == orderObject.DivisionId &&
                    x.OrderProblemId == orderObject.ProblemId).ToList();
                if (dispatchers.Count > 0)
                {
                    Random randNum = new Random();

                    orderObject.DispatcherId = dispatchers[randNum.Next(dispatchers.Count)].DispatcherId;

                    var dispatcherDetails = _context.Dispatchers.FirstOrDefault(x => x.Id == orderObject.DispatcherId);
                    if (dispatcherDetails != null)
                    {
                        orderObject.SupervisorId = dispatcherDetails.SupervisorId;
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} " +
                             $", ORDER ASSIGNED TO [DISPATCHER] DispatcherId:{orderObject.DispatcherId} " +
                             $", Dispatcher:{notFoundvalue}, Supervisor:{notFoundvalue}");
                    }
                }
                else
                {
                    //Handle assign to supervisor
                    var supervisors = _context.Supervisors.Where(x => x.DivisionId == orderObject.DivisionId).ToList();
                    if (supervisors.Count > 0)
                    {
                        Random randNum = new Random();

                        orderObject.SupervisorId = supervisors[randNum.Next(supervisors.Count)].Id;

                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} " +
                             $", ORDER ASSIGNED TO [SUPERVISOR] SupervisorId:{orderObject.SupervisorId} " +
                             $", Supervisor:{notFoundvalue}");
                    }
                }
            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , ORDER ASSIGNING ERROR ",
                    ex.Message);
            }
        }

        private void GetPaciDetails(string fileName, Order orderObject, bool isPasiOnline = true)
        {
            if (!string.IsNullOrEmpty(orderObject.SAP_PACI))
            {
                var savedPaci = _context.PACIs.FirstOrDefault(x => x.PACINumber == orderObject.SAP_PACI);
                if (savedPaci != null)
                {
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , PACI NO {orderObject.SAP_PACI} FOUND IN DB");

                    orderObject.PACI = orderObject.SAP_PACI;
                    orderObject.AreaName = savedPaci.AreaName;
                    orderObject.GovName = savedPaci.GovernorateName;
                    orderObject.Lat = (double)savedPaci.Latitude;
                    orderObject.Long = (double)savedPaci.Longtiude;
                    orderObject.StreetName = savedPaci.StreetName;
                    orderObject.StreetId = savedPaci.StreetId;
                    orderObject.BlockName = savedPaci.BlockName;
                    orderObject.BlockId = savedPaci.BlockId;
                    orderObject.AppartmentNo = savedPaci.AppartementNumber;
                    orderObject.Floor = savedPaci.FloorNumber;
                    orderObject.HouseKasima = savedPaci.BuildingNumber;
                }
                else if (isPasiOnline)
                {


                    var paciData = new PACIHelper.PACIInfo();

                    paciData = PACIHelper.GetLocationByPACI(int.Parse(orderObject.SAP_PACI));
                    var round = 0;
                    var paci = new PACI();

                    while (round < 3 && (paciData?.Latitude == null || paciData?.Longtiude == null))
                    {
                        paciData = PACIHelper.GetLocationByPACI(int.Parse(orderObject.SAP_PACI));
                        round++;
                    }

                    if (paciData?.Longtiude != null && paciData?.Latitude != null)
                    {
                        orderObject.PACI = orderObject.SAP_PACI;
                        orderObject.AreaName = paciData.Area?.Name;
                        orderObject.GovName = paciData.Governorate?.Name;
                        orderObject.Lat = (double)paciData.Latitude;
                        orderObject.Long = (double)paciData.Longtiude;
                        orderObject.AppartmentNo = paciData.AppartementNumber;
                        orderObject.Floor = paciData.FloorNumber;
                        orderObject.HouseKasima = paciData.BuildingNumber;
                        orderObject.StreetName = paciData.Street;
                        orderObject.StreetId = 0;
                        orderObject.BlockName = paciData.Block;
                        orderObject.BlockId = 0;

                        paci.PACINumber = orderObject.SAP_PACI;
                        paci.AreaName = paciData.Area?.Name;
                        paci.AreaId = int.TryParse(paciData.Area?.Id, out int c) ? int.Parse(paciData.Area?.Id) : 0;
                        paci.GovernorateName = paciData.Governorate?.Name;
                        paci.GovernorateId = int.TryParse(paciData.Governorate?.Id, out int d)
                                ? int.Parse(paciData.Governorate?.Id)
                                : 0;
                        paci.Longtiude = (decimal)paciData.Longtiude;
                        paci.Latitude = (decimal)paciData.Latitude;
                        paci.FloorNumber = paciData.FloorNumber;
                        paci.AppartementNumber = paciData.AppartementNumber;
                        paci.BuildingNumber = paciData.BuildingNumber;
                        paci.CreatedDate = DateTime.Now;
                        paci.BlockName = paciData.Block;
                        paci.BlockId = 0;
                        paci.StreetName = paciData.Street;
                        paci.StreetId = 0;

                        _context.PACIs.Add(paci);

                        try
                        {
                            _context.SaveChanges();
                            Helpers.ConsoleWriteLine(
                                 $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAVING PACI DATA {orderObject.SAP_PACI} TO DB ");
                        }
                        catch (Exception ex)
                        {
                            Helpers.ConsoleErrorWriteLine(
                                $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , SAVING PACI DATA {orderObject.SAP_PACI} TO DB Error >",
                                ex.Message);
                        }
                    }
                    else
                    {
                        Helpers.ConsoleErrorWriteLine(
                            $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , PACI NO {orderObject.SAP_PACI} DATA FROM WS HAS NO LAT/LONG ");
                    }

                    //if (paciData?.Longtiude != null && paciData?.Latitude != null)
                    //{
                    //    orderObject.PACI = orderObject.SAP_PACI;
                    //    orderObject.AreaName = paciData.Area?.Name;
                    //    orderObject.GovName = paciData.Governorate?.Name;
                    //    orderObject.Lat = (decimal)paciData.Latitude;
                    //    orderObject.Long = (decimal)paciData.Longtiude;
                    //    orderObject.StreetName = paciData.Street?.Name;
                    //    orderObject.StreetId =
                    //        int.TryParse(paciData.Street?.Id, out int y) ? int.Parse(paciData.Street?.Id) : 0;
                    //    orderObject.BlockName = paciData.Block?.Name;
                    //    orderObject.BlockId = int.TryParse(paciData.Block?.Id, out int n) ? int.Parse(paciData.Block?.Id) : 0;
                    //    orderObject.AppartmentNo = paciData.AppartementNumber;
                    //    orderObject.Floor = paciData.FloorNumber;
                    //    orderObject.HouseKasima = paciData.BuildingNumber;
                    //}
                    //else
                    //{
                    //    Helpers.ConsoleErrorWriteLine(
                    //        $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , PACI NO {orderObject.SAP_PACI} DATA FROM WS HAS NO LAT/LONG ");
                    //}
                }
            }
        }

        private static async Task GeocodeOrderAddress(string fileName, Order orderObject)
        {
            try
            {
                Helpers.ConsoleWriteLine(
                     $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} ,START GEOCODING ORDER ADDRESS BY GOOGLE ");
                var address = orderObject.SAP_AreaName.Trim();
                if (!string.IsNullOrEmpty(orderObject.SAP_BlockName))
                {
                    address += "+block" + orderObject.SAP_BlockName.Trim();
                }

                if (!string.IsNullOrEmpty(orderObject.SAP_StreetName) &&
                    !orderObject.SAP_StreetName.ToLower().Trim().Contains("no street"))
                {
                    if (int.TryParse(orderObject.SAP_StreetName.Trim(), out int n))
                    {
                        address += "+street " + orderObject.SAP_StreetName.Trim();
                    }
                    else
                    {
                        address += "+" + orderObject.SAP_StreetName.Trim();
                    }
                }

                Helpers.ConsoleWriteLine(
                     $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , START Address Resolving {address}");
                var addressencoded = Uri.EscapeUriString(address);
                var stringResponse = await Helpers.GetGeoLocation(addressencoded);
                dynamic response = JsonConvert.DeserializeObject(stringResponse);

                if (response != null)
                {
                    JArray items = (JArray)response.results;
                    if (items.Count > 0)
                    {
                        orderObject.Lat = response.results[0].geometry.location.lat;
                        orderObject.Long = response.results[0].geometry.location.lng;
                        Helpers.ConsoleWriteLine(
                             $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , END Address Resolving {response.results[0].geometry.location.ToString()} ");
                    }
                }
                else
                    Helpers.ConsoleWriteLine(
                         $"INFO::> Processing File:{fileName} , OrderCode:{orderObject.Code} , END Address Resolving NO RESPONSE!!");

            }
            catch (Exception ex)
            {
                Helpers.ConsoleErrorWriteLine(
                    $"ERROR::> Processing File:{fileName} , OrderCode:{orderObject.Code} , Address Resolving Error ",
                    ex.Message);
            }
        }



        private static int TryParse(ExcelSheetProperties prop, string[] values)
        {
            try
            {
                int num;
                num = int.TryParse(values[(int)prop], out num) ? num : 0;
                return num;
            }
            catch (Exception e)
            {
                return 0;
            }
        }


    }
}
