﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Ghanim.Reading
{
    public static class FTPManager
    {
        public static FtpWebResponse GetListOfFiles()
        {
            try
            {
                string HostName = ConfigurationManager.AppSettings["HostName"];
                string SourceFilePath = ConfigurationManager.AppSettings["SourceFilePath"];
                string Username = ConfigurationManager.AppSettings["Username"];
                string Password = ConfigurationManager.AppSettings["Password"];

                var ftpPath = HostName + "/" + SourceFilePath;
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath));
                //ftpRequest.EnableSsl = true;
                ftpRequest.Credentials = new NetworkCredential(Username, Password);
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                return response;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(
                    $"ERROR::> GetListOfFiles() > Get List Of FTP Files ERROR > ", ex);
                return null;
            }
        }
        public static FtpWebResponse DownloadFile(string fileName)
        {
            try
            {
                string HostName = ConfigurationManager.AppSettings["HostName"];
                string SourceFilePath = ConfigurationManager.AppSettings["SourceFilePath"];
                string Username = ConfigurationManager.AppSettings["Username"];
                string Password = ConfigurationManager.AppSettings["Password"];
                var ftpPath = HostName + "/" + SourceFilePath;

                FtpWebRequest ftpRequestRead = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath + "/" + fileName));
                //ftpRequestRead.EnableSsl = true;
                ftpRequestRead.Credentials = new NetworkCredential(Username, Password);
                //ftpRequestRead.UsePassive = true;
                //ftpRequestRead.Timeout = 30000;
                //ftpRequestRead.ReadWriteTimeout = 30000;
                ftpRequestRead.Method = WebRequestMethods.Ftp.DownloadFile;
                FtpWebResponse responseRead = (FtpWebResponse)ftpRequestRead.GetResponse();
                return responseRead;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(
                    $"ERROR::> DownloadFile() > Error While Downloading {fileName} > ", ex);
                return null;
            }
        }
        public static void MoveFile(string fileName)
        {
            try
            {
                string Username = ConfigurationManager.AppSettings["Username"];
                string Password = ConfigurationManager.AppSettings["Password"];
                string SourceFilePath = ConfigurationManager.AppSettings["SourceFilePath"];
                string TargetFilePath = ConfigurationManager.AppSettings["TargetFilePath"];
                string HostName = ConfigurationManager.AppSettings["HostName"];

                Uri uriSource = new Uri(HostName + "/" + SourceFilePath + "/" + fileName);
                Uri uriDestination = new Uri(HostName + "/" + TargetFilePath + "/" + fileName);

                Uri targetUriRelative = uriSource.MakeRelativeUri(uriDestination);

                //var url = new Uri(FileUploadSettings.HostName + "/" + sourceFolder + "/" + fileName);
                FtpWebRequest ftpRequestMove = (FtpWebRequest)WebRequest.Create(uriSource.AbsoluteUri);
                //ftpRequestMove.EnableSsl = true;
                ftpRequestMove.Credentials = new NetworkCredential(Username, Password);
                //ftpRequestMove.UsePassive = true;
                //ftpRequestMove.Timeout = 30000;
                //ftpRequestMove.ReadWriteTimeout = 30000;
                ftpRequestMove.Method = WebRequestMethods.Ftp.Rename;
                ftpRequestMove.RenameTo = Uri.UnescapeDataString(targetUriRelative.OriginalString);
                var responseMove = (FtpWebResponse)ftpRequestMove.GetResponse();
                responseMove.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(
                    $"ERROR::> MoveFile() > Error While Moving {fileName} > ", ex);
            }
        }
        public static void MoveInvalidFile(string fileName)
        {
            try
            {
                string Username = ConfigurationManager.AppSettings["Username"];
                string Password = ConfigurationManager.AppSettings["Password"];
                string SourceFilePath = ConfigurationManager.AppSettings["SourceFilePath"];
                string InvalidFilePath = ConfigurationManager.AppSettings["InvalidFiles"];
                string HostName = ConfigurationManager.AppSettings["HostName"];

                Uri uriSource = new Uri(HostName + "/" + SourceFilePath + "/" + fileName);
                Uri uriDestination = new Uri(HostName + "/" + InvalidFilePath + "/" + fileName);

                Uri targetUriRelative = uriSource.MakeRelativeUri(uriDestination);

                //var url = new Uri(FileUploadSettings.HostName + "/" + sourceFolder + "/" + fileName);
                FtpWebRequest ftpRequestMove = (FtpWebRequest)WebRequest.Create(uriSource.AbsoluteUri);
                //ftpRequestMove.EnableSsl = true;
                ftpRequestMove.Credentials = new NetworkCredential(Username, Password);
                //ftpRequestMove.UsePassive = true;
                //ftpRequestMove.Timeout = 30000;
                //ftpRequestMove.ReadWriteTimeout = 30000;
                ftpRequestMove.Method = WebRequestMethods.Ftp.Rename;
                ftpRequestMove.RenameTo = Uri.UnescapeDataString(targetUriRelative.OriginalString);
                var responseMove = (FtpWebResponse)ftpRequestMove.GetResponse();
                responseMove.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(
                    $"ERROR::> MoveFile() > Error While Moving {fileName} > ", ex.Message);
                DeleteFile(fileName);
                Console.WriteLine(
                    $"ERROR::> MoveFile() > Error While Moving {fileName} > File DELETED", ex.Message);
            }
        }
        public static void DeleteFile(string file)
        {
            try
            {
                string HostName = ConfigurationManager.AppSettings["HostName"];
                string SourceFilePath = ConfigurationManager.AppSettings["SourceFilePath"];
                string Username = ConfigurationManager.AppSettings["Username"];
                string Password = ConfigurationManager.AppSettings["Password"];
                var ftpPath = HostName + "/" + SourceFilePath;

                FtpWebRequest ftpRequestDelete = (FtpWebRequest)WebRequest.Create(new Uri(ftpPath + "/" + file));
                //ftpRequestDelete.EnableSsl = true;
                ftpRequestDelete.Credentials = new NetworkCredential(Username, Password);
                //ftpRequestDelete.UsePassive = true;
                //ftpRequestDelete.Timeout = 30000;
                //ftpRequestDelete.ReadWriteTimeout = 30000;
                ftpRequestDelete.Method = WebRequestMethods.Ftp.DeleteFile;
                FtpWebResponse responseDelete = (FtpWebResponse)ftpRequestDelete.GetResponse();
                responseDelete.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(
                    $"ERROR::> DeleteFile() > Error While Deleting {file} > ", ex);
            }
        }
    }
}
