//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ghanim.Order.Reading
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrderAction
    {
        public bool IsDeleted { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public System.DateTime DeletedDate { get; set; }
        public int Id { get; set; }
        public string CurrentUserId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> StatusId { get; set; }
        public Nullable<int> SubStatusId { get; set; }
        public string SubStatusName { get; set; }
        public System.DateTime ActionDate { get; set; }
        public System.DateTime ActionDay { get; set; }
        public Nullable<System.TimeSpan> ActionTime { get; set; }
        public Nullable<int> ActionTimeDays { get; set; }
        public Nullable<int> CreatedUserId { get; set; }
        public string CreatedUser { get; set; }
        public Nullable<int> ActionTypeId { get; set; }
        public string ActionTypeName { get; set; }
        public Nullable<int> CostCenterId { get; set; }
        public Nullable<int> WorkingTypeId { get; set; }
        public string WorkingTypeName { get; set; }
        public string Reason { get; set; }
        public string ServeyReport { get; set; }
        public double ActionDistance { get; set; }
        public Nullable<int> SupervisorId { get; set; }
        public Nullable<int> DispatcherId { get; set; }
        public Nullable<int> ForemanId { get; set; }
        public Nullable<int> TeamId { get; set; }
        public Nullable<int> TechnicianId { get; set; }
        public int PlatformTypeSource { get; set; }
        public string WorkingSubReason { get; set; }
        public Nullable<int> WorkingSubReasonId { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual CostCenter CostCenter { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }
        public virtual Foreman Foreman { get; set; }
        public virtual Order Order { get; set; }
        public virtual OrderStatu OrderStatu { get; set; }
        public virtual Supervisor Supervisor { get; set; }
        public virtual Team Team { get; set; }
        public virtual Technician Technician { get; set; }
    }
}
