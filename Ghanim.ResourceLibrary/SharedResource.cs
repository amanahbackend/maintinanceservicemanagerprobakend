﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.ResourceLibrary
{
    public interface ISharedResourceService
    {
    }
    public class SharedResourceService : ISharedResourceService
    {
        private readonly IStringLocalizer _localizer;

        public SharedResourceService(IStringLocalizer<SharedResourceService> localizer)
        {
            _localizer = localizer;
        }

        public string this[string index]
        {
            get
            {
                return _localizer[index];
            }
        }
    }
}
