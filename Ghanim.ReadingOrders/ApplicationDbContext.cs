﻿using Ghanim.Models.Entities;
using Ghanim.Models.EntitiesConfiguration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ghanim.EFCore.MSSQL.EntitiesConfiguration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace Ghanim.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {  }

        //identity DbSets 
        public DbSet<PasswordTokenPin> PasswordTokenPins { get; set; }
        public DbSet<ApplicationUserHistory> ApplicationUserHistory { get; set; }
        public DbSet<JunkUser> JunkUsers { get; set; }
        public DbSet<Privilege> Privileges { get; set; }
        public DbSet<RolePrivilege> RolePrivileges { get; set; }
        public DbSet<UserDevice> UserDevices { get; set; }

        //UserManagement DbSets 
        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Dispatcher> Dispatchers { get; set; }
        public DbSet<Engineer> Engineers { get; set; }
        public DbSet<Foreman> Foremans { get; set; }
        public DbSet<Technician> Technicians { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<TeamMember> Members { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Attendence> Attendences { get; set; }
        public DbSet<DispatcherSettings> DispatcherSettings { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<MaterialController> MaterialControllers { get; set; }
        public DbSet<TechniciansState> TechniciansStates { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }


        //DataManagement DbSets 
        public DbSet<Areas> Areas { get; set; }
        public DbSet<AttendanceStates> AttendanceStates { get; set; }
        public DbSet<Availability> Availability { get; set; }
        public DbSet<BuildingTypes> BuildingTypes { get; set; }
        public DbSet<CompanyCode> CompanyCode { get; set; }
        public DbSet<ContractTypes> ContractTypes { get; set; }
        public DbSet<Division> Division { get; set; }
        public DbSet<Governorates> Governorates { get; set; }
        public DbSet<Shift> Shift { get; set; }
        public DbSet<Lang_Areas> Lang_Areas { get; set; }
        public DbSet<Lang_AttendanceStates> Lang_AttendanceStates { get; set; }
        public DbSet<CostCenter> CostCenter { get; set; }
        public DbSet<Lang_CostCenter> Lang_CostCenter { get; set; }
        public DbSet<Lang_Availability> Lang_Availability { get; set; }
        public DbSet<Lang_BuildingTypes> Lang_BuildingTypes { get; set; }
        public DbSet<Lang_CompanyCode> Lang_CompanyCode { get; set; }
        public DbSet<Lang_ContractTypes> Lang_ContractTypes { get; set; }
        public DbSet<Lang_Division> Lang_Division { get; set; }
        public DbSet<Lang_Governorates> Lang_Governorates { get; set; }
        public DbSet<Lang_Shift> Lang_Shift { get; set; }
        public DbSet<SupportedLanguages> SupportedLanguages { get; set; }
        public DbSet<MapAdminRelated> MapAdminRelated { get; set; }


        //Order DbSets 
        public DbSet<OrderObject> Orders { get; set; }
        public DbSet<ArchivedOrderFile> ArchivedOrderFile { get; set; }
        public DbSet<OrderSetting> Settings { get; set; }
        public DbSet<ArchivedOrder> ArchivedOrders { get; set; }
        public DbSet<OrderAction> OrderActions { get; set; }
        public DbSet<NotificationCenter> NotificationCenter { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Lang_OrderStatus> Lang_OrderStatuses { get; set; }
        public DbSet<OrderSubStatus> OrderSubStatuses { get; set; }
        public DbSet<Lang_OrderSubStatus> Lang_OrderSubStatuses { get; set; }
        public DbSet<OrderType> OrderTypes { get; set; }
        public DbSet<Lang_OrderType> Lang_OrderTypes { get; set; }
        public DbSet<OrderPriority> OrderPriorities { get; set; }
        public DbSet<Lang_OrderPriority> Lang_OrderPriorities { get; set; }
        public DbSet<OrderProblem> OrderProblems { get; set; }
        public DbSet<Lang_OrderProblem> Lang_OrderProblems { get; set; }
        public DbSet<RejectionReason> RejectionReasons { get; set; }
        public DbSet<Lang_RejectionReason> Lang_RejectionReasons { get; set; }
        public DbSet<WorkingType> WorkingTypes { get; set; }
        public DbSet<OrderRowData> OrderRowData { get; set; }
        public DbSet<Lang_WorkingType> Lang_WorkingTypes { get; set; }
        public DbSet<Lang_Notification> Lang_Notifications { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<PACI> PACI { get; set; }




        public async Task SeedAsync(ApplicationDbContext context, IHostingEnvironment env,
            ILogger<ApplicationDbContext> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {

            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;
                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");
                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // identity config and mapping
            builder.ApplyConfiguration(new JunkUserTypeConfiguration());
            builder.ApplyConfiguration(new PasswordTokenPinTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationUserTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationUserHistoryTypeConfiguration());
            builder.ApplyConfiguration(new ApplicationRoleTypeConfiguration());
            builder.ApplyConfiguration(new PrivilegeTypeConfiguration());
            builder.ApplyConfiguration(new RolePrivilegeTypeConfiguration());
            builder.ApplyConfiguration(new UserDeviceTypeConfiguration());

            // user management config and mapping
            builder.ApplyConfiguration(new SupervisorTypeConfiguration());
            builder.ApplyConfiguration(new DispatcherSettingsTypeConfiguration());
            builder.ApplyConfiguration(new DispatcherTypeConfiguration());
            builder.ApplyConfiguration(new EngineerTypeConfiguration());
            builder.ApplyConfiguration(new ForemanTypeConfiguration());
            builder.ApplyConfiguration(new TechnicianTypeConfiguration());
            builder.ApplyConfiguration(new DriverTypeConfiguration());
            builder.ApplyConfiguration(new VehicleTypeConfiguration());
            builder.ApplyConfiguration(new TeamMemberTypeConfiguration());
            builder.ApplyConfiguration(new TeamTypeConfiguration());


            //data management config and mapping
            builder.ApplyConfiguration(new AreasConfiguration());
            builder.ApplyConfiguration(new AttendanceStatesConfiguration());
            builder.ApplyConfiguration(new AvailabilityConfiguration());
            builder.ApplyConfiguration(new BuildingTypesConfiguration());
            builder.ApplyConfiguration(new CompanyCodeConfiguration());
            builder.ApplyConfiguration(new ContractTypesConfiguration());
            builder.ApplyConfiguration(new DivisionConfiguration());
            builder.ApplyConfiguration(new GovernoratesConfiguration());
            builder.ApplyConfiguration(new ShiftConfiguration());
            builder.ApplyConfiguration(new CostCenterConfiguration());
            builder.ApplyConfiguration(new Lang_CostCenterConfiguration());
            builder.ApplyConfiguration(new Lang_AreasConfiguration());
            builder.ApplyConfiguration(new Lang_AttendanceStatesConfiguration());
            builder.ApplyConfiguration(new Lang_AvailabilityConfiguration());
            builder.ApplyConfiguration(new Lang_BuildingTypesConfiguration());
            builder.ApplyConfiguration(new Lang_CompanyCodeConfiguration());
            builder.ApplyConfiguration(new Lang_ContractTypesConfiguration());
            builder.ApplyConfiguration(new Lang_DivisionConfiguration());
            builder.ApplyConfiguration(new Lang_GovernoratesConfiguration());
            builder.ApplyConfiguration(new Lang_ShiftConfiguration());
            builder.ApplyConfiguration(new SupportedLanguagesConfiguration());
            builder.ApplyConfiguration(new MapAdminRelatedConfiguration());


            //order config and mapping
            builder.ApplyConfiguration(new OrderConfiguration());
            builder.ApplyConfiguration(new ArchivedOrderFileConfiguration());
            builder.ApplyConfiguration(new OrderSettingConfiguration());
            builder.ApplyConfiguration(new ArchivedOrderConfiguration());
            builder.ApplyConfiguration(new NotificationCenterConfiguration());
            builder.ApplyConfiguration(new NotificationConfiguration());
            builder.ApplyConfiguration(new OrderActionConfiguration());
            builder.ApplyConfiguration(new Lang_OrderProblemConfiguration());
            builder.ApplyConfiguration(new OrderProblemConfiguration());
            builder.ApplyConfiguration(new Lang_OrderStatusConfiguraion());
            builder.ApplyConfiguration(new OrderStatusConfiguration());
            builder.ApplyConfiguration(new Lang_OrderSubStatusConfiguration());
            builder.ApplyConfiguration(new OrderSubStatusConfiguration());
            builder.ApplyConfiguration(new Lang_OrderPriorityConfiguration());
            builder.ApplyConfiguration(new OrderPriorityConfiguration());
            builder.ApplyConfiguration(new Lang_OrderTypeConfiguration());
            builder.ApplyConfiguration(new OrderTypeConfiguration());
            builder.ApplyConfiguration(new RejectionReasonConfiguration());
            builder.ApplyConfiguration(new Lang_RejectionReasonConfiguration());
            builder.ApplyConfiguration(new OrderRowDataConfiguration());
            builder.ApplyConfiguration(new WorkingTypeConfiguration());
            builder.ApplyConfiguration(new Lang_WorkingTypeConfiguration());
            builder.ApplyConfiguration(new Lang_NotificationConfiguration());
            builder.ApplyConfiguration(new PACIConfiguration());
        }
    }
}
